#!/bin/bash
defaultJMXPort=8082 		# Default JMX Port 
defaultJMXHost='localhost' 	# Default JMX Host 
defaultPoolSize=20
[ -z $JMX_PORT ] && JMX_PORT=$defaultJMXPort || JMX_PORT=$JMX_PORT

#echo "Port : '$JMX_PORT'";

[ -z $JMX_HOST ] && JMX_HOST=$defaultJMXHost || JMX_HOST=$JMX_HOST

#echo "Port : '$JMX_HOST'";

[ -z $CUSTOM_COMMON_POOL_SIZE ] && CUSTOM_COMMON_POOL_SIZE=$defaultPoolSize || CUSTOM_COMMON_POOL_SIZE=$CUSTOM_COMMON_POOL_SIZE

export CATALINA_OPTS="-Xms3072m -Xmx4096m -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=$JMX_PORT -Dcom.sun.management.jmxremote.rmi.port=$JMX_PORT -Djava.util.concurrent.ForkJoinPool.common.parallelism=${CUSTOM_COMMON_POOL_SIZE} -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname=$JMX_HOST"