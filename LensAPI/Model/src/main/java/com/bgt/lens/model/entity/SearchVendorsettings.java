package com.bgt.lens.model.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.BatchSize;

/**
 * SearchVendorsettings
 */
@Entity
@Table(name = "search_vendorsettings")
public class SearchVendorsettings  implements java.io.Serializable {


     private Integer id;
     private CoreLenssettings coreLenssettings;
     private String vendorName;
     private String type;
     private int maxDocumentCount;
     private int registeredDocumentCount;
     private boolean isBGVendor;
   //  private String docServer;
     //private String hyperCubeServer;
     private String status;
     /**
     * Client created date
     */
    private Date createdOn;

    /**
     * Client updated date
     */
    private Date updatedOn;
    private Set<CoreClient> coreClients = new HashSet<CoreClient>(0);

    /**
     *
     */
    public SearchVendorsettings() {
    }

    /**
     *
     * @param coreLenssettings
     * @param vendorName
     * @param type
     * @param maxDocumentCount
     * @param registeredDocumentCount
     * @param isBGVendor
     * @param docServer
     * @param status
     * @param createdOn
     * @param updatedOn
     */
    public SearchVendorsettings(CoreLenssettings coreLenssettings, String vendorName, String type, int maxDocumentCount, int registeredDocumentCount,boolean isBGVendor, String status, Date createdOn, Date updatedOn) {
        this.coreLenssettings = coreLenssettings;
        this.vendorName = vendorName;
        this.type = type;
        this.maxDocumentCount = maxDocumentCount;
        this.registeredDocumentCount = registeredDocumentCount;
        this.isBGVendor = isBGVendor;
       // this.docServer = docServer;
        this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
        this.updatedOn = updatedOn == null ? null : new Date(updatedOn.getTime());
        this.status = status;
    }

    /**
     *
     * @param coreLenssettings
     * @param vendorName
     * @param type
     * @param maxDocumentCount
     * @param registeredDocumentCount
     * @param docServer
     * @param hyperCubeServer
     * @param status
     * @param createdOn
     * @param updatedOn
     * @param coreClients
     */
    public SearchVendorsettings(CoreLenssettings coreLenssettings, String vendorName, String type, int maxDocumentCount, int registeredDocumentCount, String status, Date createdOn, Date updatedOn, Set<CoreClient> coreClients) {
       this.coreLenssettings = coreLenssettings;
       this.vendorName = vendorName;
       this.type = type;
       this.maxDocumentCount = maxDocumentCount;
       this.registeredDocumentCount = registeredDocumentCount;
       this.isBGVendor = isBGVendor;
     //  this.docServer = docServer;
      // this.hyperCubeServer = hyperCubeServer;
       this.status = status;
       this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
       this.updatedOn = updatedOn == null ? null : new Date(updatedOn.getTime());
       this.coreClients = coreClients;
    }
    
    /**
    * Get Vendor Id
    * @return 
    */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    /**
     * Set vendor Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * Get LENS settings Id
     * @return 
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LensSettingsId", nullable = false)
    public CoreLenssettings getCoreLenssettings() {
        return this.coreLenssettings;
    }
    
    /**
     * Set LENS settings
     * @param coreLenssettings 
     */
    public void setCoreLenssettings(CoreLenssettings coreLenssettings) {
        this.coreLenssettings = coreLenssettings;
    }
    
    /**
     * Get vendor name
     * @return 
     */
    @Column(name = "VendorName", nullable = false, length = 50)
    public String getVendorName() {
        return this.vendorName;
    }
    
    /**
     * Set vendor name
     * @param vendorName 
     */
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }
    
    /**
     * Get type
     * @return 
     */
    @Column(name = "Type", nullable = false, length = 50)
    public String getType() {
        return this.type;
    }
    
    /**
     * Set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }
    
    /**
     * Get maximum document count
     * @return 
     */
    @Column(name = "MaxDocumentCount", nullable = false)
    public int getMaxDocumentCount() {
        return this.maxDocumentCount;
    }
    
    /**
     * Set maximum document count
     * @param maxDocumentCount 
     */
    public void setMaxDocumentCount(int maxDocumentCount) {
        this.maxDocumentCount = maxDocumentCount;
    }
    
    /**
     * Get registered document count
     * @return 
     */
    @Column(name = "RegisteredDocumentCount", nullable = false)
    public int getRegisteredDocumentCount() {
        return this.registeredDocumentCount;
    }
    
    /**
     * Set registered document count
     * @param registeredDocumentCount 
     */
    public void setRegisteredDocumentCount(int registeredDocumentCount) {
        this.registeredDocumentCount = registeredDocumentCount;
    }
    
    /**
     * Get doc server name
     * @return 
     */
//    @Column(name = "DocServer", nullable = false, length = 50)
//    public String getDocServer() {
//        return this.docServer;
//    }
//    
//    /**
//     * Set doc server
//     * @param docServer 
//     */
//    public void setDocServer(String docServer) {
//        this.docServer = docServer;
//    }
    
    /**
     * Get hypercube server
     * @return 
     */
//    @Column(name = "HyperCubeServer", nullable = true, length = 50)
//    public String getHyperCubeServer() {
//        return this.hyperCubeServer;
//    }
    
    /**
//     * Set hypercube server
//     * @param hyperCubeServer 
//     */
//    public void setHyperCubeServer(String hyperCubeServer) {
//        this.hyperCubeServer = hyperCubeServer;
//    }
//    
    /**
     * Get status
     * @return 
     */
    @Column(name = "Status", nullable = false)
    public String getStatus() {
        return this.status;
    }
    
    /**
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    /**
     * Get client created date
     *
     * @return
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedOn", nullable = false, length = 19)
    public Date getCreatedOn() {
        return createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Set client creation date
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Get client updated date
     *
     * @return
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UpdatedOn", nullable = false, length = 19)
    public Date getUpdatedOn() {
        return updatedOn == null ? null : new Date(updatedOn.getTime());
    }

    /**
     * Set client updated date
     *
     * @param updatedOn
     */
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn == null ? null : new Date(updatedOn.getTime());
    }
    
    /**
     * Get Core Clients
     * 
     * @return
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "search_clientvendorsettings", joinColumns = {
        @JoinColumn(name = "VendorId", nullable = false, updatable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "ClientId", nullable = false, updatable = false)})
    @BatchSize(size = 20)
    public Set<CoreClient> getCoreClients() {
        return this.coreClients;
    }
    
    /**
     * Set Core Clients
     *
     * @param coreClients
     */
    public void setCoreClients(Set<CoreClient> coreClients) {
        this.coreClients = coreClients;
    }

    /**
     * IsBGVendor
     *
     * @return
     */
    @Column(name = "IsBGVendor", nullable = false)
    public boolean isIsBGVendor() {
        return isBGVendor;
    }

    /**
     * SetBGVendor
     *
     * @param isBGVendor
     */
    public void setIsBGVendor(boolean isBGVendor) {
        this.isBGVendor = isBGVendor;
    }

    
}


