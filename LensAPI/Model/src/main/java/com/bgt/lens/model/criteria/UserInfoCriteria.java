// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

/**
 * User Criteria to get data from database
 */
public class UserInfoCriteria {

    /**
     * User ID
     */
    public Integer userId;

    /**
     * Email
     */
    public String email;

    /**
     * Get User ID
     *
     * @return
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * Set User ID
     *
     * @param userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * Get Email
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set Email
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Deleted on
     */
    public boolean deletedOn;
    
    /**
     * Is approved
     */
    public boolean isApproved;

    /**
     * Get deleted on
     * @return 
     */
    public boolean isDeletedOn() {
        return deletedOn;
    }

    /**
     * Set deleted on
     * @param DeletedOn 
     */
    public void setDeletedOn(boolean DeletedOn) {
        this.deletedOn = DeletedOn;
    }
    
    /**
     * Get is approved
     * @return 
     */
    public boolean isIsApproved() {
        return isApproved;
    }

    /**
     * Set is approved
     * @param isApproved 
     */
    public void setIsApproved(boolean isApproved) {
        this.isApproved = isApproved;
    }
}
