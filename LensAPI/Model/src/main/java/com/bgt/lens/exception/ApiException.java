// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.exception;

import org.springframework.http.HttpStatus;

/**
 *
 * API customized exception
 */
public class ApiException extends RuntimeException {

    /**
     * Exception message
     */
    private final String message;

    /**
     * HTTP status code
     */
    private final HttpStatus statusCode;

    /**
     * Exception trace log
     */
    private final String TraceLog;

    /**
     * Set API Exception
     *
     * @param traceLog
     * @param message
     * @param statusCode
     */
    public ApiException(String traceLog, String message, HttpStatus statusCode) {
        this.message = message;
        this.statusCode = statusCode;
        this.TraceLog = traceLog;
    }

    /**
     * Get exception message
     *
     * @return
     */
    @Override
    public String getMessage() {
        return this.message;
    }

    /**
     * Get HTTP status code
     *
     * @return
     */
    public HttpStatus getStatusCode() {
        return this.statusCode;
    }

    /**
     * Get exception trace log
     *
     * @return
     */
    public String getTraceLog() {
        return TraceLog;
    }

}
