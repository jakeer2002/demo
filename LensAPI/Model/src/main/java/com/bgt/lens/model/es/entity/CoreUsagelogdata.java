// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.es.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;


public class CoreUsagelogdata implements java.io.Serializable {
    /**
     * Consumer key
     */
    private String consumerKey;
    
    /**
     * Api
     */
    private String api;
    
    /**
     * Usage log Id
     */
    private Integer usageLogId;
    
    /**
     * Usage log
     */
    private CoreUsagelog coreUsagelog;
    
    /**
     * Usage log data id
     */
    private Integer id;
    
    /**
     * Request content
     */
    private String requestContent;
    
    /**
     * Response content
     */
    private String responseContent;
    
    /**
     * Request in time
     */
    private Date inTime;

    /**
     * Response sent time
     */
    private Date outTime;
    
    /**
     * Headers
     */
    private String headers;

    public CoreUsagelogdata() {
    }

    public CoreUsagelogdata(CoreUsagelog coreUsagelog, int id) {
        this.coreUsagelog = coreUsagelog;
        this.id = id;
    }

    public CoreUsagelogdata(CoreUsagelog coreUsagelog, int id, String requestContent, String responseContent, String headers) {
        this.coreUsagelog = coreUsagelog;
        this.id = id;
        this.requestContent = requestContent;
        this.responseContent = responseContent;
        this.headers = headers;
    }
    
    /**
     * Get consumer key
     * @return 
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get api
     * @return 
     */
    public String getApi() {
        return api;
    }

    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * Get usage log Id
     * @return 
     */
    public Integer getUsageLogId() {
        return this.usageLogId;
    }

    /**
     * Set usage log Id
     * @param usageLogId 
     */
    public void setUsageLogId(Integer usageLogId) {
        this.usageLogId = usageLogId;
    }

    /**
     * Get usage log
     * @return 
     */
    public CoreUsagelog getCoreUsagelog() {
        return this.coreUsagelog;
    }

    /**
     * Set usage log
     * @param coreUsagelog 
     */
    public void setCoreUsagelog(CoreUsagelog coreUsagelog) {
        this.coreUsagelog = coreUsagelog;
    }

    /**
     * Get Id
     * @return 
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Set Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get request content
     * @return 
     */
    public String getRequestContent() {
        return this.requestContent;
    }

    /**
     * Set request content
     * @param requestContent 
     */
    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    /**
     * Get response content
     * @return 
     */
    public String getResponseContent() {
        return this.responseContent;
    }

    /**
     * Set response content
     * @param responseContent 
     */
    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

    /**
     * Get headers
     * @return 
     */
    public String getHeaders() {
        return this.headers;
    }

    /**
     * Set headers
     * @param headers 
     */
    public void setHeaders(String headers) {
        this.headers = headers;
    }

    /**
     * Get in time
     * @return 
     */
    public Date getInTime() {
        return inTime;
    }

    /**
     * Set in time
     * @param inTime 
     */
    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }

    /**
     * Get out time
     * @return 
     */
    public Date getOutTime() {
        return outTime;
    }

    /**
     * Set out time
     * @param outTime 
     */
    public void setOutTime(Date outTime) {
        this.outTime = outTime;
    }
    
}
