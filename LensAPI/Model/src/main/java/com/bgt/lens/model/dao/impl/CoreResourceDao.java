// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.ResourceCriteria;
import com.bgt.lens.model.dao.ICoreResourceDao;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreResources;

/**
 *
 * Resource table data management
 */
public class CoreResourceDao implements ICoreResourceDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize resource Dao
     */
    public CoreResourceDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create resource
     *
     * @param coreResources
     */
    @Override
    public void save(CoreResources coreResources) {
        helperDao.save(sessionFactory, coreResources);
    }

    /**
     * Update resource
     *
     * @param coreResources
     */
    @Override
    public void update(CoreResources coreResources) {
        helperDao.update(sessionFactory, coreResources);
    }

    /**
     * Delete resource
     *
     * @param coreResources
     */
    @Override
    public void delete(CoreResources coreResources) {
        helperDao.delete(sessionFactory, coreResources);
    }

    /**
     * Get resource by Id
     *
     * @param coreResourceId
     * @return
     */
    @Override
    public CoreResources getResourcesById(int coreResourceId) {
        return (CoreResources) helperDao.getById(sessionFactory, CoreResources.class, coreResourceId);
    }
    
    /**
     * Get resource by criteria
     *
     * @param resourceCriteria
     * @return
     */
    @Override
    public List<CoreResources> getResourcesByCriteria(ResourceCriteria resourceCriteria) {
        List<CoreResources> coreResourcesList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreResources> criteriaQuery = cb.createQuery(CoreResources.class);
            Root<CoreResources> coreResourcesRoot = criteriaQuery.from(CoreResources.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (!_helper.isNullOrEmpty(resourceCriteria.getResourceName())) {
            	conditions.add(cb.equal(coreResourcesRoot.get("resource"), resourceCriteria.getResourceName()));
            }

            if (_helper.isNotNull(resourceCriteria.getApi())) {
            	conditions.add(cb.equal(coreResourcesRoot.get("coreApi"), resourceCriteria.getApi()));
            }

            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreResources> query = session.createQuery(criteriaQuery);
            List<CoreResources> results = query.getResultList();
            if (results != null && results.size() > 0) {
                coreResourcesList = results;

                Set<CoreResources> coreClientSet = new HashSet<>();
                coreClientSet.addAll(coreResourcesList);

                coreResourcesList.clear();
                coreResourcesList.addAll(coreClientSet);
            }
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreResourcesList;
    }

    /**
     * Get resource by criteria
     *
     * @param resourceCriteria
     * @return
     */
    @Override
    public List<CoreResources> getResourcesWithoutApiByCriteria(ResourceCriteria resourceCriteria) {
        List<CoreResources> coreResourcesList = new ArrayList<>();
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreResources> criteriaQuery = cb.createQuery(CoreResources.class);
            Root<CoreResources> coreResourcesRoot = criteriaQuery.from(CoreResources.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (!_helper.isNullOrEmpty(resourceCriteria.getResourceName())) {
            	conditions.add(cb.equal(coreResourcesRoot.get("resource"), resourceCriteria.getResourceName()));
            }

            if (_helper.isNotNull(resourceCriteria.getApi())) {
            	conditions.add(cb.equal(coreResourcesRoot.get("coreApi"), resourceCriteria.getApi()));
            }

            if (_helper.isNotNull(resourceCriteria.getCoreClient())) {
            	Join<CoreResources, CoreClient> childJoin = coreResourcesRoot.join( "coreClients" );
            	conditions.add(cb.equal(childJoin.get("id"), resourceCriteria.getCoreClient().getId()));
            }
            
            criteriaQuery.multiselect(coreResourcesRoot.get("id"),
            		coreResourcesRoot.get("resource")
            		).distinct(true);
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreResources> query = session.createQuery(criteriaQuery);
            List<CoreResources> results = query.getResultList();
            if (results != null && results.size() > 0) {
                coreResourcesList = results;

                Set<CoreResources> coreClientSet = new HashSet<>();
                coreClientSet.addAll(coreResourcesList);

                coreResourcesList.clear();
                coreResourcesList.addAll(coreClientSet);
            }
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreResourcesList;
    }

}
