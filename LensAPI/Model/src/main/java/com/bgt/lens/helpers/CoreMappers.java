// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.helpers;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.config.SearchSettings;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Enum.Fields;
import com.bgt.lens.helpers.Enum.InstanceType;
import com.bgt.lens.helpers.Enum.ParsingInstanceType;
import com.bgt.lens.helpers.Enum.docType;
import com.bgt.lens.helpers.Enum.orderResultsBy;
import com.bgt.lens.helpers.Enum.registerDuplicate;
import com.bgt.lens.helpers.Enum.registerLogType;
import com.bgt.lens.helpers.Enum.sortDirection;
import com.bgt.lens.helpers.Enum.vendorStatus;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.GetApiRequest;
import com.bgt.lens.model.adminservice.request.GetClientsRequest;
import com.bgt.lens.model.adminservice.request.GetErrorLogsRequest;
import com.bgt.lens.model.adminservice.request.GetLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.GetUsageCounterRequest;
import com.bgt.lens.model.adminservice.request.GetUsageLogsRequest;
import com.bgt.lens.model.adminservice.request.UpdateApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.UpdateResourceRequest;
import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ErrorLogCriteria;
import com.bgt.lens.model.criteria.LensSettingsCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.criteria.SearchRegisterLogCriteria;
import com.bgt.lens.model.criteria.UsageLogCriteria;
import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreErrorlog;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.CoreResources;
import com.bgt.lens.model.entity.CoreUsagecounter;
import com.bgt.lens.model.entity.CoreUsagelog;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchRegisterlog;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Candidate;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.CustomSkillsList;
import com.bgt.lens.model.rest.response.CustomXpathData;
import com.bgt.lens.model.rest.response.DefaultLocaleList;
import com.bgt.lens.model.rest.response.Distribution;
import com.bgt.lens.model.rest.response.EducationHistory;
import com.bgt.lens.model.rest.response.EmploymentHistory;
import com.bgt.lens.model.rest.response.ErrorLog;
import com.bgt.lens.model.rest.response.FacetDistribution;
import com.bgt.lens.model.rest.response.FacetDistributionList;
import com.bgt.lens.model.rest.response.FacetFilters;
import com.bgt.lens.model.rest.response.FacetFiltersList;
import com.bgt.lens.model.rest.response.FetchJob;
import com.bgt.lens.model.rest.response.FetchResume;
import com.bgt.lens.model.rest.response.Getdocids;
import com.bgt.lens.model.rest.response.FilterSettings;
import com.bgt.lens.model.rest.response.Job;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.License;
import com.bgt.lens.model.rest.response.LocationHistory;
import com.bgt.lens.model.rest.response.MatchExplanation;
import com.bgt.lens.model.rest.response.RegisterDocument;
import com.bgt.lens.model.rest.response.RegisterLog;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.SearchCommandDump;
import com.bgt.lens.model.rest.response.SearchResult;
import com.bgt.lens.model.rest.response.Skill;
import com.bgt.lens.model.rest.response.UsageCounter;
import com.bgt.lens.model.rest.response.UsageLog;
import com.bgt.lens.model.rest.response.Vendor;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xerces.dom.ElementImpl;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.http.HttpStatus;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * Core mapper
 */
public class CoreMappers {

    private final Helper _helper = new Helper();
    private final int internal = 1;
    private final int external = 2;
    private final String localeNotAvailable = "NA";
    private static final Logger LOGGER = LogManager.getLogger(CoreMappers.class);

    /**
     * Create API criteria
     *
     * @param api
     * @return
     */
    public ApiCriteria createApiCriteria(GetApiRequest api) {
        ApiCriteria apiCriteria = new ApiCriteria();
        apiCriteria.setApiKey(api.getApiKey());
        apiCriteria.setName(api.getApiName());
        return apiCriteria;
    }

    /**
     * Create client criteria
     *
     * @param client
     * @return
     */
    public ClientCriteria createClientCriteria(GetClientsRequest client) {
        ClientCriteria clientCriteria = new ClientCriteria();
        clientCriteria.setClientKey(client.getClientKey());
        clientCriteria.setName(client.getClientName());
        clientCriteria.setDumpStatus(client.isDumpStatus());
        return clientCriteria;
    }

    /**
     * Create Lens settings criteria
     *
     * @param settings
     * @return
     */
    public LensSettingsCriteria createLensSettingsCriteria(GetLensSettingsRequest settings) {
        LensSettingsCriteria settingsCriteria = new LensSettingsCriteria();
        settingsCriteria.setCharacterSet(settings.getCharSet());
        settingsCriteria.setHost(settings.getHost());
        settingsCriteria.setInstanceType(settings.getInstanceType());
        settingsCriteria.setHrxmlVersion(settings.getHRXMLVersion());
        settingsCriteria.setHrxmlFileName(settings.getHRXMLFileName());
        settingsCriteria.setLensVersion(settings.getLensVersion());
        settingsCriteria.setLocale(settings.getLocale());
        settingsCriteria.setCountry(settings.getCountry());
        settingsCriteria.setLanguage(settings.getLanguage());
        settingsCriteria.setPort(_helper.isNotNull(settings.getPort()) ? settings.getPort().intValue() : 0);
        settingsCriteria.setStatus(settings.isStatus());
        settingsCriteria.setTimeout(_helper.isNotNull(settings.getTimeOut()) ? settings.getTimeOut().intValue() : 0);
        return settingsCriteria;
    }

    /**
     * Create error log criteria
     *
     * @param errorLogRequest
     * @param clientId
     * @param apiId
     * @return
     */
    public ErrorLogCriteria createErrorLogsCriteria(GetErrorLogsRequest errorLogRequest, Integer clientId, String consumerKey, Integer apiId, String apiKey) {
        ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
        errorLogCriteria.setApiId(_helper.isNotNull(apiId) ? apiId : null);
        errorLogCriteria.setClientId(_helper.isNotNull(clientId) ? clientId : null);
        errorLogCriteria.setApi(_helper.isNotNullOrEmpty(apiKey) ? apiKey : null);
        errorLogCriteria.setConsumerKey(_helper.isNotNullOrEmpty(consumerKey) ? consumerKey : null);
        errorLogCriteria.setRequestId(_helper.isNotNull(errorLogRequest.getRequestId()) ? errorLogRequest.getRequestId() : null);
        errorLogCriteria.setStatusCode(_helper.isNotNull(errorLogRequest.getStatusCode()) ? errorLogRequest.getStatusCode().intValue() : null);
        return errorLogCriteria;
    }

    /**
     * Create usage log criteria
     *
     * @param usageLogCriteria
     * @param usageLogRequest
     * @param clientId
     * @param apiId
     * @return
     * @throws ParseException
     */
    public UsageLogCriteria createUsageLogsCriteria(UsageLogCriteria usageLogCriteria, GetUsageLogsRequest usageLogRequest, Integer clientId, String consumerKey, Integer apiId, String apiKey) throws ParseException {

        usageLogCriteria.setApiId(_helper.isNotNull(apiId) ? apiId : null);
        usageLogCriteria.setClientId(_helper.isNotNull(clientId) ? clientId : null);
        usageLogCriteria.setApi(_helper.isNotNullOrEmpty(apiKey) ? apiKey : null);
        usageLogCriteria.setConsumerKey(_helper.isNotNullOrEmpty(consumerKey) ? consumerKey : null);
        //usageLogCriteria.setTo(_helper.isNotNullOrEmpty(usageLogRequest.getTo()) ? _helper.parseDate(usageLogRequest.getTo()) : null);
        usageLogCriteria.setMethod(usageLogRequest.getMethod());
        usageLogCriteria.setResource(usageLogRequest.getResource());
        usageLogCriteria.setInstanceType(usageLogRequest.getInstanceType());
        usageLogCriteria.setLocale(usageLogRequest.getLocale());
        usageLogCriteria.setParseVariant(usageLogRequest.getParseVariant());
        usageLogCriteria.setRaisedError(usageLogRequest.isIsError());
        return usageLogCriteria;
    }

    /**
     * Create usage counter criteria
     *
     * @param usageCounterRequest
     * @param clientId
     * @param apiId
     * @return
     * @throws ParseException
     */
    public UsagecounterCriteria createUsageCounterCriteria(GetUsageCounterRequest usageCounterRequest, Integer clientId, Integer apiId) throws ParseException {
        UsagecounterCriteria usageCounterCriteria = new UsagecounterCriteria();
        usageCounterCriteria.setApiId(_helper.isNotNull(apiId) ? apiId : null);
        usageCounterCriteria.setClientId(_helper.isNotNull(clientId) ? clientId : null);
        usageCounterCriteria.setFromDate(_helper.isNotNullOrEmpty(usageCounterRequest.getFrom()) ? _helper.parseDate(usageCounterRequest.getFrom()) : null);
        usageCounterCriteria.setToDate(_helper.isNotNullOrEmpty(usageCounterRequest.getTo()) ? _helper.parseDate(usageCounterRequest.getTo()) : null);
        usageCounterCriteria.setMethod(usageCounterRequest.getMethod());
        usageCounterCriteria.setResource(usageCounterRequest.getResource());
        return usageCounterCriteria;
    }

    /**
     * Create resource entity
     *
     * @param resourceRequest
     * @param api
     * @return
     */
    public CoreResources createResourceEntity(AddResourceRequest resourceRequest, CoreApi api) {
        CoreResources resource = new CoreResources();
        resource.setResource(resourceRequest.getResourceName());
        resource.setCoreApi(api);
        return resource;
    }

    /**
     * Update resource entity
     *
     * @param resourceRequest
     * @param api
     * @param resourceId
     * @return
     */
    public CoreResources updateResourceEntity(UpdateResourceRequest resourceRequest, CoreApi api, int resourceId) {
        CoreResources resource = new CoreResources();
        resource.setId(resourceId);
        resource.setResource(resourceRequest.getResourceName());
        resource.setCoreApi(api);
        return resource;
    }

    /**
     * Create Lens setting entity
     *
     * @param settingsRequest
     * @param apiConfig
     * @return
     * @throws Exception
     */
    public CoreLenssettings createLensSettingsEntity(AddLensSettingsRequest settingsRequest, ApiConfiguration apiConfig) throws Exception {
        CoreLenssettings settings = new CoreLenssettings();

        //settings.setCharacterSet(_helper.IsNull(settingsRequest.getCharSet()) || _helper.IsEmpty(settingsRequest.getCharSet()) ? "utf-8" : settingsRequest.getCharSet());
        settings.setCreatedOn(Date.from(Instant.now()));
        settings.setHost(settingsRequest.getHost());

        if (_helper.isNotNullOrEmpty(settingsRequest.getInstanceType())) {
            String instanceType = settingsRequest.getInstanceType();
            if (!_helper.isValidInstanceType(instanceType)) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            } else {
                settings.setInstanceType(instanceType);
            }
        } else {
            settings.setInstanceType(apiConfig.getDefaultInstanceType());
        }

        if (_helper.isNotNullOrEmpty(settingsRequest.getCharSet())) {
            String charset = settingsRequest.getCharSet();
            if (_helper.isValidEncoding(charset)) {
                settings.setCharacterSet(settingsRequest.getCharSet());
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ENCODING_FORMAT), HttpStatus.BAD_REQUEST);
            }
        } else {
            settings.setCharacterSet(apiConfig.getDefaultEncoding());
        }

        // Check the validity of locale
        if (_helper.isNotNullOrEmpty(settingsRequest.getLocale())) {
            String locale = settingsRequest.getLocale();
            if (_helper.isValidLocale(locale) || "NA".equalsIgnoreCase(locale)) {
                settings.setLocale(settingsRequest.getLocale());
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_LENS_LOCALE), HttpStatus.BAD_REQUEST);
            }
        } else {
            settings.setLocale(apiConfig.getDefaultLocale());
        }

        List<CoreLenssettings> coreLensSettingsList = new ArrayList<>();
        Set<CoreLenssettings> coreLensSettingsSet = new HashSet<>();

        if (_helper.isNotNull(settingsRequest.getFailoverLensSettings()) && _helper.isNotNull(settingsRequest.getFailoverLensSettings().getFailoverLensSettingsIdList())) {

            settingsRequest.getFailoverLensSettings().getFailoverLensSettingsIdList().forEach(failoverInstanceId -> {
                CoreLenssettings coreLensSettings = new CoreLenssettings();
                coreLensSettings.setId(failoverInstanceId.intValue());
                coreLensSettingsList.add(coreLensSettings);
            });

            coreLensSettingsSet.addAll(coreLensSettingsList);
        }

        //Validate Custom filter configuration
        if (_helper.isNotNull(settingsRequest.getFilterSettings()) && _helper.isNotNull(settingsRequest.getFilterSettings().getCustomFilters())) {

            if (_helper.isNotNull(settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters()) || _helper.isNotNull(settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters())) {
                boolean addCustomFilters = false;
                if (settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().size() > 0) {
                    addCustomFilters = true;
                    List<String> filterKey = new ArrayList<>();
                    List<String> filterValue = new ArrayList<>();
                    settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().forEach(filter -> {
                        if (_helper.isNullOrEmpty(filter.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(filter.getName())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterKey.contains(filter.getKey().trim())) {
                            filterKey.add(filter.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterValue.contains(filter.getName().trim())) {
                            filterValue.add(filter.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });

                }

                if (settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().size() > 0) {
                    addCustomFilters = true;
                    List<String> filterKey = new ArrayList<>();
                    List<String> filterValue = new ArrayList<>();

                    settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().forEach(filter -> {
                        if (_helper.isNullOrEmpty(filter.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(filter.getName())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterKey.contains(filter.getKey().trim())) {
                            filterKey.add(filter.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterValue.contains(filter.getName().trim())) {
                            filterValue.add(filter.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });

                }

                if (addCustomFilters) {
                    if (settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.LENSSEARCH.toString()) || settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.SPECTRUM.toString())) {
                        //settings.setCustomFilterSettings(_helper.getJsonStringFromObject(settingsRequest.getFilterSettings()));
                    } else {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_CUSTOM_FILTER_CONFIG), HttpStatus.BAD_REQUEST);
                    }
                }
            }
        }

        //Validate facet filter configuration
        if (_helper.isNotNull(settingsRequest.getFilterSettings()) && _helper.isNotNull(settingsRequest.getFilterSettings().getFacetFilters())) {

            if (_helper.isNotNull(settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters()) || _helper.isNotNull(settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters())) {
                boolean addFacetFilters = false;
                if (settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().size() > 0) {
                    addFacetFilters = true;
                    List<String> filterKey = new ArrayList<>();
                    List<String> filterValue = new ArrayList<>();

                    settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().forEach(filter -> {
                        if (_helper.isNullOrEmpty(filter.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_FACET_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(filter.getName())) {

                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_FACET_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterKey.contains(filter.getKey().trim())) {
                            filterKey.add(filter.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FACET_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterValue.contains(filter.getName().trim())) {
                            filterValue.add(filter.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FACET_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });
                }

                if (settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().size() > 0) {
                    addFacetFilters = true;
                    List<String> filterKey = new ArrayList<>();
                    List<String> filterValue = new ArrayList<>();

                    settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().forEach(filter -> {
                        if (_helper.isNullOrEmpty(filter.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_FACET_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(filter.getName())) {

                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_FACET_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterKey.contains(filter.getKey().trim())) {
                            filterKey.add(filter.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FACET_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterValue.contains(filter.getName().trim())) {
                            filterValue.add(filter.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FACET_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });

                }

                if (addFacetFilters) {
                    if (settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.LENSSEARCH.toString()) || settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.SPECTRUM.toString())) {
                        //settings.setCustomFilterSettings(_helper.getJsonStringFromObject(settingsRequest.getFilterSettings()));
                    } else {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_FACET_FILTER_CONFIG), HttpStatus.BAD_REQUEST);
                    }
                }
            }
        }

        List<CoreCustomskillsettings> coreCustomSkillSettingsList = new ArrayList<>();
        Set<CoreCustomskillsettings> coreCustomSkillSettingsSet = new HashSet<>();

        //Validate custom key settings configuration
        if (_helper.isNotNull(settingsRequest.getCustomSkillsSettings())) {

            if (_helper.isNotNull(settingsRequest.getCustomSkillsSettings().getCustomSkillsList())) {
                boolean addCustomSkills = false;
                if (settingsRequest.getCustomSkillsSettings().getCustomSkillsList().size() > 0) {
                    addCustomSkills = true;
                    List<String> customSkillKey = new ArrayList<>();
                    List<String> customSkillValue = new ArrayList<>();

                    settingsRequest.getCustomSkillsSettings().getCustomSkillsList().forEach(customSkill -> {
                        if (_helper.isNullOrEmpty(customSkill.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_SKILL_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(customSkill.getName())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_SKILL_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!_helper.isAlphaNumeric(customSkill.getKey())){
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CUSTOMSKILL_KEY), HttpStatus.BAD_REQUEST);
                        }                        
                        if (!_helper.isAlphaNumeric(customSkill.getName())){
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CUSTOMSKILL_NAME), HttpStatus.BAD_REQUEST);
                        }                        
                        if (!customSkillKey.contains(customSkill.getKey().trim())) {
                            customSkillKey.add(customSkill.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_SKILL_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!customSkillValue.contains(customSkill.getName().trim())) {
                            customSkillValue.add(customSkill.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_SKILL_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });
                }

                if (addCustomSkills) {
                    if (settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.LD.toString())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_CUSTOM_SKILL_CONFIG), HttpStatus.BAD_REQUEST);
                    } else {
                        settingsRequest.getCustomSkillsSettings().getCustomSkillsList().forEach(customSkill -> {
                            CoreCustomskillsettings coreCustomskillsettings = new CoreCustomskillsettings();
                            coreCustomskillsettings.setCustomSkillCode(customSkill.getKey().trim());
                            coreCustomskillsettings.setCustomSkillName(customSkill.getName().trim());
                            coreCustomskillsettings.setCoreLenssettings(settings);
                            coreCustomSkillSettingsList.add(coreCustomskillsettings);
                        });

                        coreCustomSkillSettingsSet.addAll(coreCustomSkillSettingsList);
                    }
                }
            }
        }

        if (_helper.isNotNull(settingsRequest.getFilterSettings())
                && ((_helper.isNotNull(settingsRequest.getFilterSettings().getCustomFilters())
                && ((_helper.isNotNull(settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters())
                && settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().size() > 0)
                || (_helper.isNotNull(settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters())
                && settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().size() > 0)))
                || (_helper.isNotNull(settingsRequest.getFilterSettings().getFacetFilters())
                && ((_helper.isNotNull(settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters())
                && settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().size() > 0)
                || (_helper.isNotNull(settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters())
                && settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().size() > 0))))) {
            settings.setCustomFilterSettings(_helper.getJsonStringFromObject(settingsRequest.getFilterSettings()));
            if (_helper.isNotNullOrEmpty(settings.getCustomFilterSettings())) {
                FilterSettings filterSettings = (FilterSettings) _helper.getObjectFromJson(settings.getCustomFilterSettings(), new FilterSettings());
                if (_helper.isNotNull(filterSettings)) {
                    if (_helper.isNotNull(filterSettings.getCustomFilters()) && (_helper.isNotNull(filterSettings.getCustomFilters().getResumeFilters()))) {
                        if (filterSettings.getCustomFilters().getResumeFilters().size() <= 0) {
                            filterSettings.getCustomFilters().setResumeFilters(null);
                        }
                    }
                    if (_helper.isNotNull(filterSettings.getCustomFilters()) && (_helper.isNotNull(filterSettings.getCustomFilters().getPostingFilters()))) {
                        if (filterSettings.getCustomFilters().getPostingFilters().size() <= 0) {
                            filterSettings.getCustomFilters().setPostingFilters(null);
                        }
                    }

                    if (_helper.isNotNull(filterSettings.getCustomFilters()) && _helper.isNull(filterSettings.getCustomFilters().getResumeFilters()) && _helper.isNull(filterSettings.getCustomFilters().getPostingFilters())) {
                        filterSettings.setCustomFilters(null);
                    }

                    if (_helper.isNotNull(filterSettings.getFacetFilters()) && (_helper.isNotNull(filterSettings.getFacetFilters().getResumeFilters()))) {
                        if (filterSettings.getFacetFilters().getResumeFilters().size() <= 0) {
                            filterSettings.getFacetFilters().setResumeFilters(null);
                        }
                    }
                    if (_helper.isNotNull(filterSettings.getFacetFilters()) && (_helper.isNotNull(filterSettings.getFacetFilters().getPostingFilters()))) {
                        if (filterSettings.getFacetFilters().getPostingFilters().size() <= 0) {
                            filterSettings.getFacetFilters().setPostingFilters(null);
                        }
                    }

                    if (_helper.isNotNull(filterSettings.getFacetFilters()) && _helper.isNull(filterSettings.getFacetFilters().getResumeFilters()) && _helper.isNull(filterSettings.getFacetFilters().getPostingFilters())) {
                        filterSettings.setFacetFilters(null);
                    }

                    if (_helper.isNull(filterSettings.getCustomFilters()) && _helper.isNull(filterSettings.getFacetFilters())) {
                        filterSettings = null;
                    }
                }
                settings.setCustomFilterSettings(_helper.getJsonStringFromObject(filterSettings));
            } else {
                settingsRequest.setFilterSettings(null);
            }

        }

        if (_helper.isNotNullOrEmpty(settingsRequest.getInstanceType()) && settingsRequest.getInstanceType().equalsIgnoreCase(InstanceType.LD.toString())) {
            settings.setLanguage(localeNotAvailable);
            settings.setCountry(localeNotAvailable);
            settings.setLocale(localeNotAvailable);
        } else {
            settings.setLanguage(settingsRequest.getLanguage());
            settings.setCountry(settingsRequest.getCountry());
            settings.setHrxmlcontent(settingsRequest.getHRXMLContent());
            settings.setHrxmlversion(settingsRequest.getHRXMLVersion());

            //added by sic
            settings.setDocServerResume(settingsRequest.getDocServerResume());
            settings.setDocServerPostings(settingsRequest.getDocServerPostings());
            settings.setHypercubeServerResume(settingsRequest.getHypercubeServerResume());
            settings.setHypercubeServerPostings(settingsRequest.getHypercubeServerPostings());
            settings.setHrxmlfilename(settingsRequest.getHRXMLFileName());
            settings.setLocale(settingsRequest.getLocale());
        }
        settings.setLensVersion(settingsRequest.getVersion());
        settings.setPort(settingsRequest.getPort().intValue());
        settings.setStatus(settingsRequest.isStatus());
        settings.setTimeout(_helper.isNull(settingsRequest.getTimeOut()) || settingsRequest.getTimeOut().intValue() <= 0 ? apiConfig.getDefaultTimeout() : settingsRequest.getTimeOut().intValue());
        settings.setCoreLenssettingsesForFailoverInstanceId(coreLensSettingsSet);
        settings.setCoreCustomskillsettings(coreCustomSkillSettingsSet);

        return settings;
    }

    /**
     * Create API entity
     *
     * @param apiRequest
     * @return
     * @throws Exception
     */
    public CoreApi createApiEntity(CreateApiRequest apiRequest) throws Exception {
        CoreApi api = new CoreApi();

        api.setApiKey(apiRequest.getKey());
        api.setName(apiRequest.getName());
        api.setCreatedOn(Date.from(Instant.now()));
        api.setUpdatedOn(Date.from(Instant.now()));

        return api;
    }

    /**
     * Delete API response
     *
     * @param apiId
     * @return
     */
    public Api deleteApiResponse(int apiId) {
        Api api = new Api();
        api.setId(apiId);
        return api;
    }

    /**
     * Delete Lens setting response
     *
     * @param settingsId
     * @return
     */
    public LensSettings deleteLensSettingsResponse(int settingsId) {
        LensSettings lensSettings = new LensSettings();
        lensSettings.setId(settingsId);
        lensSettings.setPort(null);
        lensSettings.setTimeout(null);
        lensSettings.setStatus(null);
        return lensSettings;
    }

    /**
     * Delete resource response
     *
     * @param resources
     * @return
     */
    public Resources deleteResourceResponse(CoreResources resources) {
        Resources resource = new Resources();
        resource.setResourceId(resources.getId());
        return resource;
    }

    /**
     * Create resource response
     *
     * @param resources
     * @return
     */
    public Resources createResourceResponse(CoreResources resources) {
        Resources resource = new Resources();
        resource.setResourceId(resources.getId());
        resource.setApiKey(resources.getCoreApi().getApiKey());
        resource.setResourceName(resources.getResource());
        return resource;
    }

    /**
     * Create resource response
     *
     * @param coreResourceList
     * @return
     */
    public List<Resources> createResourceResponse(List<CoreResources> coreResourceList) {
        List<Resources> resourceList = new ArrayList<>();

        coreResourceList.forEach(coreResource -> {
            Resources resource = new Resources();
            resource.setResourceId(coreResource.getId());
            resource.setApiKey(coreResource.getCoreApi().getApiKey());
            resource.setResourceName(coreResource.getResource());
            resourceList.add(resource);
        });

        return resourceList;
    }

    /**
     * Create Lens settings response
     *
     * @param coreLenssettings
     * @return
     * @throws java.io.IOException
     */
    public LensSettings createLensSettingsResponse(CoreLenssettings coreLenssettings) throws IOException {
        LensSettings settings = new LensSettings();
        settings.setId(coreLenssettings.getId());
        settings.setCharset(coreLenssettings.getCharacterSet());
        settings.setHost(coreLenssettings.getHost());
        settings.setPort(coreLenssettings.getPort());
        settings.setInstanceType(coreLenssettings.getInstanceType());
        settings.setVersion(coreLenssettings.getLensVersion());
        settings.setLanguage(coreLenssettings.getLanguage());
        settings.setCountry(coreLenssettings.getCountry());
        settings.setHrxmlContent(coreLenssettings.getHrxmlcontent());
        settings.setHrxmlVersion(coreLenssettings.getHrxmlversion());
        settings.setHrxmlFileName(coreLenssettings.getHrxmlfilename());
        settings.setLocale(coreLenssettings.getLocale());
        settings.setStatus(coreLenssettings.isStatus());
        settings.setTimeout(coreLenssettings.getTimeout());

        settings.setDocServerResume(coreLenssettings.getDocServerResume());
        settings.setDocServerPostings(coreLenssettings.getDocServerPostings());
        settings.setHypercubeServerResume(coreLenssettings.getHypercubeServerResume());
        settings.setHypercubeServerPostings(coreLenssettings.getHypercubeServerPostings());

        if (_helper.isNotNullOrEmpty(coreLenssettings.getCustomFilterSettings())) {
            settings.setFilterSettings((FilterSettings) _helper.getObjectFromJson(coreLenssettings.getCustomFilterSettings(), new FilterSettings()));
        }

        List<CoreLenssettings> lensSettingsList = new ArrayList<>();
        lensSettingsList.addAll(coreLenssettings.getCoreLenssettingsesForFailoverInstanceId());
        List<Integer> instancesList = new ArrayList<>();

        lensSettingsList.forEach(coreLensSettings -> {
            instancesList.add(coreLensSettings.getId());
        });

        settings.setFailoverLensSettings(instancesList);

        com.bgt.lens.model.rest.response.CoreCustomskillsettings coreCustomskillsettings = new com.bgt.lens.model.rest.response.CoreCustomskillsettings();
        List<CustomSkillsList> customSkillsList = new ArrayList();

        if (_helper.isNotNull(coreLenssettings.getCoreCustomskillsettings())) {
            coreLenssettings.getCoreCustomskillsettings().forEach(customSkillsetting -> {
                CustomSkillsList customSkills = new CustomSkillsList();
                customSkills.setId(customSkillsetting.getId());
                customSkills.setKey(customSkillsetting.getCustomSkillCode());
                customSkills.setName(customSkillsetting.getCustomSkillName());
                customSkillsList.add(customSkills);                
            });
            coreCustomskillsettings.setCustomSkillsList(customSkillsList);
            
            settings.setCustomSkillsSettings(coreCustomskillsettings);
        }

        return settings;
    }

    /**
     * Create Lens settings response
     *
     * @param coreLenssettingsList
     * @return
     * @throws java.io.IOException
     */
    public List<LensSettings> createLensSettingsResponse(List<CoreLenssettings> coreLenssettingsList) throws IOException {
        List<LensSettings> lensSettingsList = new ArrayList<>();

        for (CoreLenssettings coreLenssettings : coreLenssettingsList) {
            LensSettings settings = new LensSettings();
            settings.setId(coreLenssettings.getId());
            settings.setCharset(coreLenssettings.getCharacterSet());
            settings.setHost(coreLenssettings.getHost());
            settings.setPort(coreLenssettings.getPort());
            settings.setInstanceType(coreLenssettings.getInstanceType());
            settings.setVersion(coreLenssettings.getLensVersion());
            settings.setLocale(coreLenssettings.getLocale());
            settings.setLanguage(coreLenssettings.getLanguage());
            settings.setCountry(coreLenssettings.getCountry());
            settings.setHrxmlContent(coreLenssettings.getHrxmlcontent());
            settings.setHrxmlVersion(coreLenssettings.getHrxmlversion());
            settings.setHrxmlFileName(coreLenssettings.getHrxmlfilename());
            settings.setStatus(coreLenssettings.isStatus());
            settings.setTimeout(coreLenssettings.getTimeout());
            List<CoreLenssettings> coreLensSettingsList = new ArrayList<>();
            coreLensSettingsList.addAll(coreLenssettings.getCoreLenssettingsesForFailoverInstanceId());
            List<Integer> instancesList = new ArrayList<>();

            coreLensSettingsList.forEach(coreLensSettings -> {
                instancesList.add(coreLensSettings.getId());
            });

            settings.setFailoverLensSettings(instancesList);

            if (_helper.isNotNullOrEmpty(coreLenssettings.getCustomFilterSettings())) {
                settings.setFilterSettings((FilterSettings) _helper.getObjectFromJson(coreLenssettings.getCustomFilterSettings(), new FilterSettings()));
            }

            lensSettingsList.add(settings);
        }
        return lensSettingsList;
    }

    /**
     * Create Usage log response
     *
     * @param coreUsageLog
     * @return
     */
    public UsageLog createUsageLogResponse(CoreUsagelog coreUsageLog) {
        UsageLog usageLog = new UsageLog();
//        usageLog.setApiId(coreUsageLog.getApiId());
//        usageLog.setClientId(coreUsageLog.getClientId());
        usageLog.setApi(coreUsageLog.getApi());
        usageLog.setConsumerKey(coreUsageLog.getConsumerKey());
        usageLog.setId(coreUsageLog.getId());
        usageLog.setMethod(coreUsageLog.getMethod());
//        usageLog.setHeaders(coreUsageLog.getHeaders());
        usageLog.setInTime(coreUsageLog.getInTime());
        usageLog.setOutTime(coreUsageLog.getOutTime());
        usageLog.setRaisedError(coreUsageLog.isRaisedError());
        usageLog.setResource(coreUsageLog.getResource());
        usageLog.setRequestId(coreUsageLog.getRequestId());
//        usageLog.setRequestContent(coreUsageLog.getRequestContent());
//        usageLog.setResponseContent(coreUsageLog.getResponseContent());
        if (coreUsageLog.getMilliseconds() != null) {
            usageLog.setTimeElapsed(coreUsageLog.getMilliseconds());
        }
        usageLog.setUri(coreUsageLog.getUri());
        return usageLog;
    }

    /**
     * Create usage log response
     *
     * @param coreUsageLogList
     * @return
     */
    public List<UsageLog> createUsageLogResponse(List<CoreUsagelog> coreUsageLogList) {

        List<UsageLog> usageLogList = new ArrayList<>();

        coreUsageLogList.forEach(coreUsageLog -> {

            UsageLog usageLog = new UsageLog();
//            usageLog.setApiId(coreUsageLog.getApiId());
//            usageLog.setClientId(coreUsageLog.getClientId());
            usageLog.setApi(coreUsageLog.getApi());
            usageLog.setConsumerKey(coreUsageLog.getConsumerKey());
            usageLog.setId(coreUsageLog.getId());
            usageLog.setMethod(coreUsageLog.getMethod());
//            usageLog.setHeaders(coreUsageLog.getHeaders());
            usageLog.setInTime(coreUsageLog.getInTime());
            usageLog.setOutTime(coreUsageLog.getOutTime());
            usageLog.setRaisedError(coreUsageLog.isRaisedError());
            usageLog.setResource(coreUsageLog.getResource());
            usageLog.setRequestId(coreUsageLog.getRequestId());
//            usageLog.setRequestContent(coreUsageLog.getRequestContent());
//            usageLog.setResponseContent(coreUsageLog.getResponseContent());
            if (coreUsageLog.getMilliseconds() != null) {
                usageLog.setTimeElapsed(coreUsageLog.getMilliseconds());
            }
            usageLog.setUri(coreUsageLog.getUri());
            usageLogList.add(usageLog);

        });
        return usageLogList;
    }

    /**
     * Create Register Log response
     *
     * @param searchRegisterLog
     * @return
     */
    public RegisterLog createRegisterLogResponse(SearchRegisterlog searchRegisterLog) {
        RegisterLog registerLog = new RegisterLog();
        registerLog.setClientId(searchRegisterLog.getClientId());
        registerLog.setId(searchRegisterLog.getId());
        registerLog.setLensSettingsId(searchRegisterLog.getLensSettingsId());
        registerLog.setDocId(searchRegisterLog.getDocId());
        registerLog.setInstanceType(searchRegisterLog.getInstanceType());
        registerLog.setDocumentType(docType.resume.toString());
        registerLog.setLocale(searchRegisterLog.getLocale());
        registerLog.setCreatedOn(searchRegisterLog.getCreatedOn());
        registerLog.setRequestId(searchRegisterLog.getRequestId());
        registerLog.setUpdatedOn(searchRegisterLog.getUpdatedOn());
        registerLog.setFailoverUpdateStatus(searchRegisterLog.getFailoverUpdateStatus());
        registerLog.setOverWrite(searchRegisterLog.getOverWrite());
        registerLog.setVendorId(searchRegisterLog.getVendorId());

        if (searchRegisterLog.getRegisterOrUnregister()) {
            registerLog.setRegisterOrUnregister(registerLogType.Register.toString());
        } else if (!searchRegisterLog.getRegisterOrUnregister()) {
            registerLog.setRegisterOrUnregister(registerLogType.Unregister.toString());
        }

        return registerLog;
    }

    /**
     * Create register log response
     *
     * @param searchRegisterLogList
     * @param docType
     * @return
     */
    public List<RegisterLog> createRegisterLogResponse(List<SearchRegisterlog> searchRegisterLogList, String docType) {

        List<RegisterLog> registerLogList = new ArrayList<>();

        for (SearchRegisterlog searchRegisterLog : searchRegisterLogList) {
            RegisterLog registerLog = new RegisterLog();

            if (_helper.isNotNullOrEmpty(docType)) {
                registerLog.setDocumentType(docType);
            }
            registerLog.setClientId(searchRegisterLog.getClientId());
            registerLog.setId(searchRegisterLog.getId());
            registerLog.setDocId(searchRegisterLog.getDocId());
            registerLog.setLocale(searchRegisterLog.getLocale());
            registerLog.setDocumentType(searchRegisterLog.getDocumentType());
            registerLog.setInstanceType(searchRegisterLog.getInstanceType());
            registerLog.setLensSettingsId(searchRegisterLog.getLensSettingsId());
            registerLog.setCreatedOn(searchRegisterLog.getCreatedOn());
            registerLog.setRequestId(searchRegisterLog.getRequestId());
            registerLog.setUpdatedOn(searchRegisterLog.getUpdatedOn());
            registerLog.setFailoverUpdateStatus(searchRegisterLog.getFailoverUpdateStatus());
            registerLog.setOverWrite(searchRegisterLog.getOverWrite());
            registerLog.setVendorId(searchRegisterLog.getVendorId());

            if (searchRegisterLog.getRegisterOrUnregister()) {
                registerLog.setRegisterOrUnregister(registerLogType.Register.toString());
            } else if (!searchRegisterLog.getRegisterOrUnregister()) {
                registerLog.setRegisterOrUnregister(registerLogType.Unregister.toString());
            }

            registerLogList.add(registerLog);

        }
        return registerLogList;
    }

    /**
     * Create Search Command Dump response
     *
     * @param searchCommandDump
     * @return
     */
    public SearchCommandDump createSearchCommandDumpResponse(SearchCommanddump searchCommandDump) {
        SearchCommandDump commandDump = new SearchCommandDump();
        commandDump.setClientId(searchCommandDump.getClientId());
        commandDump.setId(searchCommandDump.getId());
        commandDump.setRequestId(searchCommandDump.getRequestId());
        commandDump.setLensSettingsId(searchCommandDump.getLensSettingsId());
        commandDump.setVendorId(searchCommandDump.getVendorId());
        commandDump.setCreatedOn(searchCommandDump.getCreatedOn());
        commandDump.setSearchCommand(searchCommandDump.getSearchCommand());

        return commandDump;
    }

    /**
     * Create search command dunp response
     *
     * @param searchCommandDumpList
     * @param docType
     * @return
     */
    public List<SearchCommandDump> createSearchCommandDumpResponse(List<SearchCommanddump> searchCommandDumpList, String docType) {

        List<SearchCommandDump> searchCommandList = new ArrayList<>();

        for (SearchCommanddump searchCommandDump : searchCommandDumpList) {

            SearchCommandDump commandDump = new SearchCommandDump();
            commandDump.setClientId(searchCommandDump.getClientId());
            commandDump.setRequestId(searchCommandDump.getRequestId());
            commandDump.setId(searchCommandDump.getId());
            commandDump.setLensSettingsId(searchCommandDump.getLensSettingsId());
            commandDump.setVendorId(searchCommandDump.getVendorId());
            commandDump.setCreatedOn(searchCommandDump.getCreatedOn());
            commandDump.setSearchCommand(searchCommandDump.getSearchCommand());
            if (_helper.isNotNullOrEmpty(docType)) {
                commandDump.setDocType(docType);
            }

            searchCommandList.add(commandDump);

        }
        return searchCommandList;
    }

    /**
     * Create facet filter list response
     *
     * @param lensSettingsList
     * @param clientId
     * @param docType
     * @return
     */
    public FacetFiltersList createFacetFilterListResponse(List<CoreLenssettings> lensSettingsList, BigInteger clientId, String docType) throws IOException {

        FacetFiltersList facetFilterList = new FacetFiltersList();
        facetFilterList.setFacetFiltersList(new ArrayList<FacetFilters>());
        for (CoreLenssettings lenssettings : lensSettingsList) {
            FacetFilters facetFilters = null;
            if (_helper.isNotNullOrEmpty(lenssettings.getCustomFilterSettings())) {
                FilterSettings filters = (FilterSettings) _helper.getObjectFromJson(lenssettings.getCustomFilterSettings(), new FilterSettings());
                if (_helper.isNotNull(filters) && _helper.isNotNull(filters.getFacetFilters())) {
                    if (_helper.isNullOrEmpty(docType)) {
                        if ((filters.getFacetFilters().getPostingFilters() != null && filters.getFacetFilters().getPostingFilters().size() > 0)
                                || (filters.getFacetFilters().getResumeFilters() != null && filters.getFacetFilters().getResumeFilters().size() > 0)) {
                            facetFilters = filters.getFacetFilters();
                        }
                    } else if (docType.equalsIgnoreCase(Enum.docType.resume.toString())) {
                        if ((filters.getFacetFilters().getResumeFilters() != null && filters.getFacetFilters().getResumeFilters().size() > 0)) {
                            filters.getFacetFilters().setPostingFilters(null);
                            facetFilters = filters.getFacetFilters();
                        }
                    } else if (docType.equalsIgnoreCase(Enum.docType.posting.toString())) {
                        if ((filters.getFacetFilters().getPostingFilters() != null && filters.getFacetFilters().getPostingFilters().size() > 0)) {
                            filters.getFacetFilters().setResumeFilters(null);
                            facetFilters = filters.getFacetFilters();
                        }
                    }
                }
                if (_helper.isNotNull(facetFilters)) {
                    if (_helper.isNotNull(clientId)) {
                        facetFilters.setClientId(clientId.intValue());
                    }
                    facetFilters.setInstanceType(lenssettings.getInstanceType());
                    facetFilters.setLocale(lenssettings.getLocale());
                    facetFilters.setLensSettingsId(lenssettings.getId());

                    facetFilterList.getFacetFiltersList().add(facetFilters);
                }
            }
        }
        return facetFilterList;
    }

    /**
     * Create error log response
     *
     * @param coreErrorLogList
     * @return
     */
    public List<ErrorLog> createErrorLogResponse(List<CoreErrorlog> coreErrorLogList) {

        List<ErrorLog> errorLogList = new ArrayList<>();

        coreErrorLogList.forEach(coreErrorLog -> {

            ErrorLog errorLog = new ErrorLog();
//            errorLog.setApiId(coreErrorLog.getApiId());
//            errorLog.setClientId(coreErrorLog.getClientId());
            errorLog.setApi(errorLog.getApi());
            errorLog.setConsumerKey(errorLog.getConsumerKey());
            errorLog.setId(coreErrorLog.getId());
            errorLog.setRequestId(coreErrorLog.getRequestId());
            errorLog.setStatusCode(coreErrorLog.getStatusCode());
            errorLog.setUserMessage(coreErrorLog.getUserMessage());
            errorLog.setException(coreErrorLog.getException());
            errorLog.setId(coreErrorLog.getId());
            errorLog.setCreatedOn(coreErrorLog.getCreatedOn());
            errorLog.setTraceLog(coreErrorLog.getTraceLog());
            errorLogList.add(errorLog);

        });

        return errorLogList;
    }

    /**
     * Create error log response
     *
     * @param coreErrorLog
     * @return
     */
    public ErrorLog createErrorLogResponse(CoreErrorlog coreErrorLog) {
        ErrorLog errorLog = new ErrorLog();
//        errorLog.setApiId(coreErrorLog.getApiId());
//        errorLog.setClientId(coreErrorLog.getClientId());
        errorLog.setId(coreErrorLog.getId());
        errorLog.setApi(coreErrorLog.getApi());
        errorLog.setConsumerKey(coreErrorLog.getConsumerKey());
        errorLog.setRequestId(coreErrorLog.getRequestId());
        errorLog.setStatusCode(coreErrorLog.getStatusCode());
        errorLog.setUserMessage(coreErrorLog.getUserMessage());
        errorLog.setException(coreErrorLog.getException());
        errorLog.setId(coreErrorLog.getId());
        errorLog.setCreatedOn(coreErrorLog.getCreatedOn());
        errorLog.setTraceLog(coreErrorLog.getTraceLog());
        return errorLog;
    }

    /**
     * Create usage counter response
     *
     * @param coreUsageCounter
     * @return
     */
    public UsageCounter createUSageCounterResponse(CoreUsagecounter coreUsageCounter) {
        UsageCounter usageCounter = new UsageCounter();
        usageCounter.setApiId(coreUsageCounter.getApiId());
        usageCounter.setClientId(coreUsageCounter.getClientId());
        usageCounter.setCreatedOn(coreUsageCounter.getCreatedOn());
        usageCounter.setUpdatedOn(coreUsageCounter.getUpdatedOn());
        usageCounter.setId(coreUsageCounter.getId());
        usageCounter.setMethod(coreUsageCounter.getMethod());
        usageCounter.setResource(coreUsageCounter.getResource());
        usageCounter.setUsageCount(coreUsageCounter.getUsageCount());
        return usageCounter;
    }

    /**
     * Create usage counter response
     *
     * @param coreUsageCounterList
     * @return
     */
    public List<UsageCounter> createUsageCounterResponse(List<CoreUsagecounter> coreUsageCounterList) {

        List<UsageCounter> usageCounterList = new ArrayList<>();

        coreUsageCounterList.forEach(coreUsageCounter -> {
            UsageCounter usageCounter = new UsageCounter();
            usageCounter.setApiId(coreUsageCounter.getApiId());
            usageCounter.setClientId(coreUsageCounter.getClientId());
            usageCounter.setCreatedOn(coreUsageCounter.getCreatedOn());
            usageCounter.setUpdatedOn(coreUsageCounter.getUpdatedOn());
            usageCounter.setId(coreUsageCounter.getId());
            usageCounter.setMethod(coreUsageCounter.getMethod());
            usageCounter.setResource(coreUsageCounter.getResource());
            usageCounter.setUsageCount(coreUsageCounter.getUsageCount());
            usageCounterList.add(usageCounter);
        });
        return usageCounterList;
    }

    /**
     * Create API response
     *
     * @param coreApi
     * @return
     */
    public Api createApiResponse(CoreApi coreApi) {
        Api api = new Api();

        api.setId(coreApi.getId());
        api.setKey(coreApi.getApiKey());
        api.setName(coreApi.getName());
        api.setCreatedOn(coreApi.getCreatedOn());
        api.setUpdatedOn(coreApi.getUpdatedOn());

        return api;
    }

    /**
     * Create API response
     *
     * @param coreApiList
     * @return
     */
    public List<Api> createApiResponse(List<CoreApi> coreApiList) {
        List<Api> apiList = new ArrayList<>();

        coreApiList.forEach(coreApi -> {
            Api api = new Api();

            api.setId(coreApi.getId());
            api.setKey(coreApi.getApiKey());
            api.setName(coreApi.getName());
            api.setCreatedOn(coreApi.getCreatedOn());
            api.setUpdatedOn(coreApi.getUpdatedOn());

            apiList.add(api);
        });

        return apiList;
    }

    /**
     * Update Lens settings entity
     *
     * @param settingsRequest
     * @param apiConfig
     * @param lensSettingsId
     * @return
     * @throws Exception
     */
    public CoreLenssettings updateLensSettingsEntity(Integer lensSettingsId, UpdateLensSettingsRequest settingsRequest, ApiConfiguration apiConfig) throws Exception {
        CoreLenssettings settings = new CoreLenssettings();

        //Validate custom filter settings
        if (_helper.isNotNull(settingsRequest.getFilterSettings()) && _helper.isNotNull(settingsRequest.getFilterSettings().getCustomFilters())) {

            if (_helper.isNotNull(settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters()) || _helper.isNotNull(settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters())) {
                boolean addCustomFilters = false;
                if (settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().size() > 0) {
                    addCustomFilters = true;
                    List<String> filterKey = new ArrayList<>();
                    List<String> filterValue = new ArrayList<>();

                    settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().forEach(filter -> {
                        if (_helper.isNullOrEmpty(filter.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(filter.getName())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterKey.contains(filter.getKey().trim())) {
                            filterKey.add(filter.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterValue.contains(filter.getName().trim())) {
                            filterValue.add(filter.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });
                }

                if (settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().size() > 0) {
                    addCustomFilters = true;
                    List<String> filterKey = new ArrayList<>();
                    List<String> filterValue = new ArrayList<>();

                    settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().forEach(filter -> {
                        if (_helper.isNullOrEmpty(filter.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(filter.getName())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterKey.contains(filter.getKey().trim())) {
                            filterKey.add(filter.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterValue.contains(filter.getName().trim())) {
                            filterValue.add(filter.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });
                }

                if (addCustomFilters) {
                    if (settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.LENSSEARCH.toString()) || settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.SPECTRUM.toString())) {
                        settings.setCustomFilterSettings(_helper.getJsonStringFromObject(settingsRequest.getFilterSettings()));
                    } else {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_CUSTOM_FILTER_CONFIG), HttpStatus.BAD_REQUEST);
                    }
                }
            }
        }

        //Validate facet filter configuration
        if (_helper.isNotNull(settingsRequest.getFilterSettings()) && _helper.isNotNull(settingsRequest.getFilterSettings().getFacetFilters())) {

            if (_helper.isNotNull(settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters()) || _helper.isNotNull(settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters())) {
                boolean addFacetFilters = false;
                if (settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().size() > 0) {
                    addFacetFilters = true;
                    List<String> filterKey = new ArrayList<>();
                    List<String> filterValue = new ArrayList<>();

                    settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().forEach(filter -> {
                        if (_helper.isNullOrEmpty(filter.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_FACET_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(filter.getName())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_FACET_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterKey.contains(filter.getKey().trim())) {
                            filterKey.add(filter.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FACET_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterValue.contains(filter.getName().trim())) {
                            filterValue.add(filter.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FACET_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });
                }

                if (settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().size() > 0) {
                    addFacetFilters = true;
                    List<String> filterKey = new ArrayList<>();
                    List<String> filterValue = new ArrayList<>();

                    settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().forEach(filter -> {
                        if (_helper.isNullOrEmpty(filter.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_FACET_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(filter.getName())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_FACET_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterKey.contains(filter.getKey().trim())) {
                            filterKey.add(filter.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FACET_FILTER_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!filterValue.contains(filter.getName().trim())) {
                            filterValue.add(filter.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FACET_FILTER_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });
                }

                if (addFacetFilters) {
                    if (settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.LENSSEARCH.toString()) || settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.SPECTRUM.toString())) {
                        settings.setCustomFilterSettings(_helper.getJsonStringFromObject(settingsRequest.getFilterSettings()));
                    } else {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_FACET_FILTER_CONFIG), HttpStatus.BAD_REQUEST);
                    }
                }
            }
        }

        if (_helper.isNotNull(settingsRequest.getFilterSettings())) {
            settings.setCustomFilterSettings(_helper.getJsonStringFromObject(settingsRequest.getFilterSettings()));
        }

        List<CoreLenssettings> coreLensSettingsList = new ArrayList<>();
        Set<CoreLenssettings> coreLensSettingsSet = new HashSet<>();

        if (_helper.isNotNull(settingsRequest.getFailoverLensSettings()) && _helper.isNotNull(settingsRequest.getFailoverLensSettings().getFailoverLensSettingsIdList())) {

            Set<BigInteger> failoverInstanceSet = new HashSet<>();
            failoverInstanceSet.addAll(settingsRequest.getFailoverLensSettings().getFailoverLensSettingsIdList());

            if (failoverInstanceSet.size() != settingsRequest.getFailoverLensSettings().getFailoverLensSettingsIdList().size()) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FAILOVER_ID_LIST), HttpStatus.BAD_REQUEST);
            }
            if (failoverInstanceSet.contains(BigInteger.valueOf(lensSettingsId))) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.FAILOVER_TO_SAME_INSTANCE), HttpStatus.BAD_REQUEST);
            }

            settingsRequest.getFailoverLensSettings().getFailoverLensSettingsIdList().forEach(failoverInstanceId -> {
                CoreLenssettings coreLensSettings = new CoreLenssettings();
                coreLensSettings.setId(failoverInstanceId.intValue());
                coreLensSettingsList.add(coreLensSettings);
            });

            coreLensSettingsSet.addAll(coreLensSettingsList);
        }

        List<CoreCustomskillsettings> coreCustomSkillSettingsList = new ArrayList<>();
        Set<CoreCustomskillsettings> coreCustomSkillSettingsSet = new HashSet<>();

        //Validate custom key settings configuration
        if (_helper.isNotNull(settingsRequest.getCustomSkillsSettings())) {

            if (_helper.isNotNull(settingsRequest.getCustomSkillsSettings().getCustomSkillsList())) {
                boolean addCustomSkills = false;
                if (settingsRequest.getCustomSkillsSettings().getCustomSkillsList().size() > 0) {
                    addCustomSkills = true;
                    List<String> customSkillKey = new ArrayList<>();
                    List<String> customSkillValue = new ArrayList<>();

                    settingsRequest.getCustomSkillsSettings().getCustomSkillsList().forEach(customSkill -> {
                        if (_helper.isNullOrEmpty(customSkill.getKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_SKILL_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(customSkill.getName())) {

                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CUSTOM_SKILL_NAME), HttpStatus.BAD_REQUEST);
                        }
                        if (!_helper.isAlphaNumeric(customSkill.getKey())){
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CUSTOMSKILL_KEY), HttpStatus.BAD_REQUEST);
                        }                        
                        if (!_helper.isAlphaNumeric(customSkill.getName())){
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CUSTOMSKILL_NAME), HttpStatus.BAD_REQUEST);
                        } 
                        if (!customSkillKey.contains(customSkill.getKey().trim())) {
                            customSkillKey.add(customSkill.getKey().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_SKILL_KEY), HttpStatus.BAD_REQUEST);
                        }
                        if (!customSkillValue.contains(customSkill.getName().trim())) {
                            customSkillValue.add(customSkill.getName().trim());
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CUSTOM_SKILL_NAME), HttpStatus.BAD_REQUEST);
                        }
                    });
                }

                if (addCustomSkills) {
                    if (settingsRequest.getInstanceType().equalsIgnoreCase(ParsingInstanceType.LD.toString())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_FACET_FILTER_CONFIG), HttpStatus.BAD_REQUEST);
                    } else {
                        settingsRequest.getCustomSkillsSettings().getCustomSkillsList().forEach(customSkill -> {
                            CoreCustomskillsettings coreCustomskillsettings = new CoreCustomskillsettings();
                            coreCustomskillsettings.setCustomSkillCode(customSkill.getKey().trim());
                            coreCustomskillsettings.setCustomSkillName(customSkill.getName().trim());
                            coreCustomskillsettings.setCoreLenssettings(settings);
                            coreCustomSkillSettingsList.add(coreCustomskillsettings);
                        });

                        coreCustomSkillSettingsSet.addAll(coreCustomSkillSettingsList);
                    }
                }
            }
        }

        settings.setCharacterSet(settingsRequest.getCharSet());
        settings.setHost(settingsRequest.getHost());
        settings.setInstanceType(settingsRequest.getInstanceType());

        if (_helper.isNotNullOrEmpty(settingsRequest.getInstanceType()) && settingsRequest.getInstanceType().equalsIgnoreCase(InstanceType.LD.toString())) {
            settings.setLanguage(localeNotAvailable);
            settings.setCountry(localeNotAvailable);
            settings.setLocale(localeNotAvailable);
        } else {
            settings.setLanguage(settingsRequest.getLanguage());
            settings.setCountry(settingsRequest.getCountry());
            settings.setHrxmlcontent(settingsRequest.getHRXMLContent());
            settings.setHrxmlversion(settingsRequest.getHRXMLVersion());
            settings.setHrxmlfilename(settingsRequest.getHRXMLFileName());
            settings.setLocale(settingsRequest.getLocale());
            settings.setDocServerResume(settingsRequest.getDocServerResume());
            settings.setDocServerPostings(settingsRequest.getDocServerPostings());
            settings.setHypercubeServerResume(settingsRequest.getHypercubeServerResume());
            settings.setHypercubeServerPostings(settingsRequest.getHypercubeServerPostings());
        }
        settings.setLensVersion(settingsRequest.getVersion());
        if (_helper.isNotNull(settingsRequest.getPort())) {
            settings.setPort(settingsRequest.getPort().intValue());
        }
        settings.setStatus(settingsRequest.isStatus());
        settings.setTimeout(_helper.isNull(settingsRequest.getTimeOut()) || settingsRequest.getTimeOut().intValue() <= 0 ? apiConfig.getDefaultTimeout() : settingsRequest.getTimeOut().intValue());
        settings.setCoreLenssettingsesForFailoverInstanceId(coreLensSettingsSet);
        settings.setCoreCustomskillsettings(coreCustomSkillSettingsSet);

        return settings;
    }

    /**
     * Update API entity
     *
     * @param apiRequest
     * @return
     * @throws Exception
     */
    public CoreApi updateApiEntity(UpdateApiRequest apiRequest) throws Exception {
        CoreApi api = new CoreApi();

        api.setApiKey(apiRequest.getKey());
        api.setName(apiRequest.getName());
        //api.setCreatedOn(Date.from(Instant.now()));
        api.setUpdatedOn(Date.from(Instant.now()));

        return api;
    }

    /**
     * Create consumer entity
     *
     * @param request
     * @param apiConfig
     * @return
     * @throws ParseException
     */
    public CoreClient createConsumerAsEntity(CreateConsumerRequest request, ApiConfiguration apiConfig) throws ParseException, JsonProcessingException, IOException {
        Date date = Date.from(Instant.now());
        CoreClient client = new CoreClient();
        List<CoreClientapi> clientApiList = new ArrayList<>();
        List<CoreResources> coreResourceList = new ArrayList<>();

        request.getClientApiList().getClientApi().forEach(api -> {

            CoreClientapi clientApi = new CoreClientapi();
            clientApi.setSecret(api.getSecret());

            if (_helper.isNullOrEmpty(api.getActivationDate()) && _helper.isNullOrEmpty(api.getExpiryDate()) && _helper.isNull(api.getAllowedTransactions())) {
                LocalDate today = new LocalDate();
                LocalDate today30 = today.plusDays(apiConfig.getDefaultExpiryDateInterval());
                clientApi.setActivationDate(today.toDateTimeAtStartOfDay().toDate());
                clientApi.setExpiryDate(today30.toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
                clientApi.setAllowedTransactions(apiConfig.getDefaultTransactionCount());
            } else if (_helper.isNotNullOrEmpty(api.getActivationDate()) && _helper.isNotNullOrEmpty(api.getExpiryDate())) {
                clientApi.setActivationDate(LocalDate.parse(api.getActivationDate()).toDateTimeAtStartOfDay().toDate());
                clientApi.setExpiryDate(LocalDate.parse(api.getExpiryDate()).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
            } else if (_helper.isNotNullOrEmpty(api.getActivationDate())) {
                LocalDate today30 = LocalDate.parse(api.getActivationDate()).plusDays(apiConfig.getDefaultExpiryDateInterval());
                clientApi.setActivationDate(LocalDate.parse(api.getActivationDate()).toDateTimeAtStartOfDay().toDate());
                clientApi.setExpiryDate(today30.toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
            } else if (_helper.isNotNullOrEmpty(api.getExpiryDate())) {
                LocalDate today = new LocalDate();
                clientApi.setActivationDate(today.toDateTimeAtStartOfDay().toDate());
                clientApi.setExpiryDate(LocalDate.parse(api.getExpiryDate()).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
            } else {
                LocalDate today = new LocalDate();
                LocalDate today30 = today.plusDays(apiConfig.getDefaultExpiryDateInterval());
                clientApi.setActivationDate(today.toDateTimeAtStartOfDay().toDate());
                clientApi.setExpiryDate(today30.toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
            }

            if (clientApi.getActivationDate().after(clientApi.getExpiryDate())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
            }

            clientApi.setAllowedTransactions((_helper.isNull(api.getAllowedTransactions()) || api.getAllowedTransactions().intValue() <= 0) ? apiConfig.getDefaultTransactionCount() : api.getAllowedTransactions().intValue());
            clientApi.setEnabled(api.isEnabled());

            clientApi.setRemainingTransactions(clientApi.getAllowedTransactions());
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getApiId().intValue());
            clientApi.setCoreApi(coreApi);

            if (_helper.isNotNull(api.getLicensedResources()) && _helper.isNotNull(api.getLicensedResources().getResourceList())) {

                api.getLicensedResources().getResourceList().forEach(resourceId -> {
                    CoreResources coreResources = new CoreResources();
                    coreResources.setId(resourceId.intValue());
                    coreResources.setCoreApi(coreApi);
                    coreResourceList.add(coreResources);
                });
                Set<CoreResources> coreResourcesSet = new HashSet<>();
                coreResourcesSet.addAll(coreResourceList);
                client.setCoreResourceses(coreResourcesSet);
            }
            clientApiList.add(clientApi);
        });

        Set<CoreClientapi> coreClientApiSet = new HashSet<>();
        coreClientApiSet.addAll(clientApiList);

        if (_helper.isNotNull(request.getDefaultLocaleList())) {

            List<com.bgt.lens.model.adminservice.request.Locale> localeList = request.getDefaultLocaleList().getLocale();
            localeList.forEach(locale -> {
                if (_helper.isNullOrEmpty(locale.getLanguage())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LANGUAGE), HttpStatus.BAD_REQUEST);
                } else if (_helper.isNullOrEmpty(locale.getCountry())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_COUNTRY), HttpStatus.BAD_REQUEST);
                }
            });
        }

        String defaultLocaleList = _helper.getJsonStringFromObject(request.getDefaultLocaleList());

        String searchSettings = null;
        if (_helper.isNotNull(request.getVendorSettings()) && _helper.isNotNull(request.getVendorSettings().getVendorIdList()) && request.getVendorSettings().getVendorIdList().size() > 0) {
            if (_helper.isNotNull(request.getSearchSettings())) {
                if (_helper.isNull(request.getSearchSettings().getMaxSearchCount())) {
                    request.getSearchSettings().setMaxSearchCount((long) apiConfig.getDefaultMaxSearchCount());
                }

                searchSettings = _helper.getJsonStringFromObject(request.getSearchSettings());

                SearchSettings searchSettingsObj = (SearchSettings) _helper.getObjectFromJson(searchSettings, new SearchSettings());

                if (_helper.isNotNull(searchSettingsObj)) {
                    if (_helper.isNull(searchSettingsObj.getMaxSearchCount())) {
                        searchSettingsObj.setMaxSearchCount(apiConfig.getDefaultMaxSearchCount());
                    }
                    if (_helper.isNull(searchSettingsObj.isEnableClarify())) {
                        searchSettingsObj.setEnableClarify(false);
                    }
                    if (!(_helper.isNotNull(searchSettingsObj.getResume()) && searchSettingsObj.getResume().size() > 0)) {
                        searchSettingsObj.setResume(null);
                    }
                    if (!(_helper.isNotNull(searchSettingsObj.getPosting()) && searchSettingsObj.getPosting().size() > 0)) {
                        searchSettingsObj.setPosting(null);
                    }
                    searchSettings = _helper.getJsonStringFromObject(searchSettingsObj);
                } else {
                    searchSettings = null;
                }
//                if((_helper.isNotNull(request.getSearchSettings().getResume()) && request.getSearchSettings().getResume().size() > 0 ) || ( _helper.isNotNull(request.getSearchSettings().getPosting()) && request.getSearchSettings().getPosting().size() > 0 ) ){
//                    
//                }
            }
        } else if (_helper.isNotNull(request.getVendorSettings())) {
            if (_helper.isNull(request.getVendorSettings().getVendorIdList())
                    || request.getVendorSettings().getVendorIdList().size() <= 0) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_VENDOR_SETTINGS_ID_LIST), HttpStatus.BAD_REQUEST);
            }
        }

        //CoreLenssettings coreLensSettings = new CoreLenssettings();
        List<CoreClientlenssettings> coreClientLensSettingslist = new ArrayList<>();

        request.getLensSettings().getInstanceList().forEach(instance -> {
            //coreLensSettings.setId(instanceId.intValue());
            CoreClientlenssettings clientLensSettings = new CoreClientlenssettings();
            CoreLenssettings coreLensSettings = new CoreLenssettings();
            coreLensSettings.setId(instance.getInstanceId().intValue());
            if (instance.isRateLimitEnabled()) {
                clientLensSettings.setRateLimitEnabled(instance.isRateLimitEnabled());
                clientLensSettings.setRateLimitTimePeriod(instance.getRateLimitTimePeriod().intValue());
                clientLensSettings.setRateLimitCount(instance.getRateLimitCount().intValue());
            } else {
                clientLensSettings.setRateLimitEnabled(Boolean.FALSE);
            }
            clientLensSettings.setCoreLenssettings(coreLensSettings);

            if (_helper.isNotNull(instance.getCustomSkillsId())) {
                CoreCustomskillsettings coreCustomskillsettings = new CoreCustomskillsettings();
                coreCustomskillsettings.setId(instance.getCustomSkillsId().intValue());
                clientLensSettings.setCoreCustomskillsettings(coreCustomskillsettings);
            }
            coreClientLensSettingslist.add(clientLensSettings);
        });

        Set<CoreClientlenssettings> coreLensSettingsSet = new HashSet<>();
        coreLensSettingsSet.addAll(coreClientLensSettingslist);

        List<SearchVendorsettings> searchVendorsettingsList = new ArrayList<>();

        if (_helper.isNotNull(request.getVendorSettings()) && _helper.isNotNull(request.getVendorSettings().getVendorIdList())) {
            request.getVendorSettings().getVendorIdList().forEach(vendorId -> {
                //coreLensSettings.setId(instanceId.intValue());
                SearchVendorsettings searchVendorsettings = new SearchVendorsettings();
                searchVendorsettings.setId(vendorId.intValue());
                searchVendorsettingsList.add(searchVendorsettings);
            });

            Set<SearchVendorsettings> searchVendorsettingsSet = new HashSet<>();
            searchVendorsettingsSet.addAll(searchVendorsettingsList);

            client.setSearchVendorsettingses(searchVendorsettingsSet);
        }

        client.setDumpStatus(request.isDumpStatus());
        client.setDocStoreService(request.isDocStoreService());
        client.setDefaultLocale(defaultLocaleList);
        client.setSearchSettings(searchSettings);
        client.setEnableLocaleDetection(request.isEnableLocaleDetection());
        client.setAuthenticationType(request.getAuthenticationType());
        client.setCoreClientapis(coreClientApiSet);
        client.setCoreClientlenssettingses(coreLensSettingsSet);
        client.setClientKey(request.getKey());
        client.setCreatedOn(date);
        client.setUpdatedOn(date);
        client.setName(request.getName());
        client.setSecret(request.getSecret());

        // Rate Limit
        client.setRateLimitEnabled(request.isRateLimitEnabled());
        if (request.isRateLimitEnabled()) {
            client.setRateLimitTimePeriod(Integer.valueOf(request.getRateLimitTimePeriod().toString()));
            client.setRateLimitCount(Integer.valueOf(request.getRateLimitCount().toString()));
        }
        if (_helper.isNotNull(request.getTier())) {
            client.setTier(Integer.valueOf(request.getTier().toString()));
        } else {
            client.setTier(external);
        }

        return client;
    }

    /**
     * Create update consumer entity
     *
     * @param request
     * @return
     * @throws ParseException
     */
    public CoreClient updateConsumerAsEntity(UpdateConsumerRequest request) throws ParseException {
        Date date = Date.from(Instant.now());
        CoreClient client = new CoreClient();

        List<CoreClientapi> clientApiList = new ArrayList<>();
        List<CoreResources> coreResourceList = new ArrayList<>();

        if (request.getClientApiList() != null) {
            request.getClientApiList().getClientApi().forEach(api -> {
                CoreClientapi clientApi = new CoreClientapi();
                clientApi.setSecret(api.getSecret());
                clientApi.setActivationDate(_helper.isNotNullOrEmpty(api.getActivationDate()) ? LocalDate.parse(api.getActivationDate()).toDateTimeAtStartOfDay().toDate() : null);
                clientApi.setExpiryDate(_helper.isNotNullOrEmpty(api.getExpiryDate()) ? LocalDate.parse(api.getExpiryDate()).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate() : null);
                clientApi.setAllowedTransactions(_helper.isNotNull(api.getAllowedTransactions()) ? api.getAllowedTransactions().intValue() : 0);
                clientApi.setEnabled(api.isEnabled());

                clientApi.setRemainingTransactions(_helper.isNotNull(api.getAllowedTransactions()) ? api.getAllowedTransactions().intValue() : 0);
                CoreApi coreApi = new CoreApi();
                coreApi.setId(api.getApiId().intValue());
                clientApi.setCoreApi(coreApi);

                if (_helper.isNotNull(api.getLicensedResources()) && _helper.isNotNull(api.getLicensedResources().getResourceList())) {

                    api.getLicensedResources().getResourceList().forEach(resourceId -> {
                        //coreLensSettings.setId(instanceId.intValue());
                        CoreResources coreResources = new CoreResources();
                        coreResources.setId(resourceId.intValue());
                        coreResources.setCoreApi(coreApi);
                        coreResourceList.add(coreResources);
                    });
                }

                clientApiList.add(clientApi);
            });
        }

        Set<CoreClientapi> coreClientApiSet = new HashSet<>();
        coreClientApiSet.addAll(clientApiList);

        //CoreLenssettings coreLensSettings = new CoreLenssettings();
        ApiContext apiContext = _helper.getApiContext();
        CoreClient coreClient = apiContext.getClient();
        //  Set<CoreClientlenssettings> clientlenssettingses = coreClient.getCoreClientlenssettingses();

        List<CoreClientlenssettings> coreClientLensSettingslist = new ArrayList<>();
        if (_helper.isNotNull(request.getLensSettings())) {

            request.getLensSettings().getInstanceList().forEach(instance -> {

                //CoreLenssettings coreLensSettings = new CoreLenssettingsDao().getLensSettingsById(instance.getInstanceId().intValue());
                //coreLensSettings.setId(instanceId.intValue());
                CoreClientlenssettings clientLensSettings = new CoreClientlenssettings();
                CoreLenssettings coreLensSettings = new CoreLenssettings();
                coreLensSettings.setId(instance.getInstanceId().intValue());
                if (instance.isRateLimitEnabled()) {
                    clientLensSettings.setRateLimitEnabled(instance.isRateLimitEnabled());
                    clientLensSettings.setRateLimitTimePeriod(instance.getRateLimitTimePeriod().intValue());
                    clientLensSettings.setRateLimitCount(instance.getRateLimitCount().intValue());
                } else {
                    clientLensSettings.setRateLimitEnabled(Boolean.FALSE);
                }
                if (_helper.isNotNull(instance.getCustomSkillsId())) {
                    CoreCustomskillsettings coreCustomskillsettings = new CoreCustomskillsettings();
                    coreCustomskillsettings.setId(instance.getCustomSkillsId().intValue());
                    clientLensSettings.setCoreCustomskillsettings(coreCustomskillsettings);
                }
                clientLensSettings.setCoreClient(coreClient);
                clientLensSettings.setCoreLenssettings(coreLensSettings);
                coreClientLensSettingslist.add(clientLensSettings);
            });
        }

        Set<CoreClientlenssettings> coreLensSettingsSet = new HashSet<>();
        coreLensSettingsSet.addAll(coreClientLensSettingslist);

        client.setCoreClientlenssettingses(coreLensSettingsSet);

        List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();

        if (request.getVendorSettings() != null) {
            request.getVendorSettings().getVendorIdList().forEach(vendorId -> {
                //coreLensSettings.setId(instanceId.intValue());
                SearchVendorsettings searchVendorSettings = new SearchVendorsettings();
                searchVendorSettings.setId(vendorId.intValue());
                searchVendorSettingsList.add(searchVendorSettings);
            });
        }

        Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
        searchVendorSettingsSet.addAll(searchVendorSettingsList);

        Set<CoreResources> coreResourcesSet = new HashSet<>();
        coreResourcesSet.addAll(coreResourceList);

        if (_helper.isNotNull(request.getTier())) {
            client.setTier(request.getTier().intValue());
        }
        client.setDumpStatus(request.isDumpStatus());
        client.setDocStoreService(request.isDocStoreService());
        client.setCoreClientapis(coreClientApiSet);
        //client.setCoreClientLensSettingses(coreLensSettingsSet);
        client.setSearchVendorsettingses(searchVendorSettingsSet);
        client.setCoreResourceses(coreResourcesSet);
        client.setClientKey(request.getKey());
        //client.setCreatedOn(date);
        client.setUpdatedOn(date);
        client.setName(request.getName());
        client.setSecret(request.getSecret());
        client.setAuthenticationType(request.getAuthenticationType());

        // Rate Limit
        client.setRateLimitEnabled(request.isRateLimitEnabled());
        if (request.isRateLimitEnabled()) {
            if (_helper.isNotNull(request.getRateLimitTimePeriod())) {
                client.setRateLimitTimePeriod(Integer.valueOf(request.getRateLimitTimePeriod().toString()));
            }
            if (_helper.isNotNull(request.getRateLimitCount())) {
                client.setRateLimitCount(Integer.valueOf(request.getRateLimitCount().toString()));
            }
        }

        return client;
    }

    /**
     * Delete client response
     *
     * @param clientId
     * @return
     */
    public Client deleteClientResponse(int clientId) {
        Client client = new Client();
        client.setId(clientId);
        client.setDumpStatus(null);
        return client;
    }

    /**
     * Create client response
     *
     * @param coreClient
     * @return
     * @throws java.io.IOException
     */
    public Client createClientResponse(CoreClient coreClient) throws IOException {
        Client client = new Client();

        client.setId(coreClient.getId());
        client.setKey(coreClient.getClientKey());
        client.setSecret(coreClient.getSecret());
        client.setDumpStatus(coreClient.isDumpStatus());

        List<CoreClientlenssettings> lensSettingsList = new ArrayList<>();
        lensSettingsList.addAll(coreClient.getCoreClientlenssettingses());

        List<CoreResources> resourceList = new ArrayList<>();
        resourceList.addAll(coreClient.getCoreResourceses());

//        List<Integer> instancesList = new ArrayList<>();
//        for (CoreClientlenssettings settings : lensSettingsList) {
//            instancesList.add(settings.getCoreLenssettings().getId());
//        }
        //client.setInstances(instancesList);
        List<CoreClientlenssettings> clientlenssettingses = new ArrayList<>();
        clientlenssettingses.addAll(coreClient.getCoreClientlenssettingses());

        List<com.bgt.lens.model.rest.response.ClientLensSettings> clientLensSettings = new ArrayList<>();

        clientlenssettingses.forEach(clientlenssettings -> {
            com.bgt.lens.model.rest.response.ClientLensSettings cls = new com.bgt.lens.model.rest.response.ClientLensSettings();
            cls.setInstanceId(clientlenssettings.getCoreLenssettings().getId());
            cls.setRateLimitEnabled(clientlenssettings.isRateLimitEnabled());
            if (clientlenssettings.isRateLimitEnabled()) {
                cls.setRateLimitTimePeriod(clientlenssettings.getRateLimitTimePeriod());
                cls.setRateLimitCount(clientlenssettings.getRateLimitCount());
            }
            if (_helper.isNotNull(clientlenssettings.getCoreCustomskillsettings())) {
                cls.setCustomSkillsId(clientlenssettings.getCoreCustomskillsettings().getId());
            }
            clientLensSettings.add(cls);
        });

        List<SearchVendorsettings> vendorSettingsList = new ArrayList<>();
        vendorSettingsList.addAll(coreClient.getSearchVendorsettingses());

        List<Integer> vendorsList = new ArrayList<>();

        vendorSettingsList.forEach(settings -> {
            vendorsList.add(settings.getId());
        });

        client.setVendors(vendorsList);

        List<Integer> resourcesList = new ArrayList<>();
        resourceList.forEach(resources -> {
            resourcesList.add(resources.getId());
        });
        client.setLicensedResources(resourcesList);

        List<CoreClientapi> clientApiList = new ArrayList<>();
        clientApiList.addAll(coreClient.getCoreClientapis());

        List<com.bgt.lens.model.rest.response.ClientApi> clientApisList = new ArrayList<>();
        clientApiList.forEach(clientApi -> {
            com.bgt.lens.model.rest.response.ClientApi api = new com.bgt.lens.model.rest.response.ClientApi();
            //api.Id = clientApi.getId_1();
            api.setId(null);
            api.setApiId(clientApi.getCoreApi().getId());
            api.setApiKey(clientApi.getCoreApi().getApiKey());
            api.setEnabled(clientApi.isEnabled());
            api.setSecret(clientApi.getSecret());
            api.setActivationDate(clientApi.getActivationDate());
            api.setExpiryDate(clientApi.getExpiryDate());
            api.setAllowedTransactions(clientApi.getAllowedTransactions());
            api.setRemainingTransactions(clientApi.getRemainingTransactions());
            clientApisList.add(api);
        });

        DefaultLocaleList defaultLocaleList = _helper.isNotNull(coreClient.getDefaultLocale()) ? (DefaultLocaleList) _helper.getObjectFromJson(coreClient.getDefaultLocale(), new DefaultLocaleList()) : null;

        SearchSettings searchSettings = _helper.isNotNull(coreClient.getSearchSettings()) ? (SearchSettings) _helper.getObjectFromJson(coreClient.getSearchSettings(), new SearchSettings()) : null;

        client.setClientLensSettings(clientLensSettings);

        client.setSearchSettings(searchSettings);

        client.setDefaultLocales(defaultLocaleList);

        client.setEnableLocaleDetection(coreClient.isEnableLocaleDetection());

        client.setAuthenticationType(coreClient.getAuthenticationType());

        client.setClientApi(clientApisList);

        client.setRateLimitEnabled(coreClient.isRateLimitEnabled());

        client.setRateLimitTimePeriod(_helper.isNotNull(coreClient.getRateLimitTimePeriod()) ? coreClient.getRateLimitTimePeriod() : null);

        client.setRateLimitCount(_helper.isNotNull(coreClient.getRateLimitCount()) ? coreClient.getRateLimitCount() : null);

        return client;
    }

    /**
     * Create client response
     *
     * @param coreClientList
     * @return
     */
    public List<Client> createClientResponse(List<CoreClient> coreClientList) {
        List<Client> clientList = new ArrayList<>();

        coreClientList.forEach(coreClient -> {
            Client client = new Client();

            client.setId(coreClient.getId());
            client.setKey(coreClient.getClientKey());
            client.setSecret(coreClient.getSecret());
            client.setDumpStatus(coreClient.isDumpStatus());

            List<CoreLenssettings> lensSettingsList = new ArrayList<>();
            List<CoreClientlenssettings> clientLensSettingsList = new ArrayList<>();
            //lensSettingsList.addAll(coreClient.getCoreClientLensSettingses().);
            //lensSettingsList.addAll(coreClient.getCoreLenssettingses());

            List<CoreResources> resourceList = new ArrayList<>();
            resourceList.addAll(coreClient.getCoreResourceses());

//            List<Integer> instancesList = new ArrayList<>();
//            for (CoreLenssettings settings : lensSettingsList) {
//                instancesList.add(settings.getId());
//            }
            //client.setInstances(instancesList);
            List<CoreClientlenssettings> clientlenssettingses = new ArrayList<>();
            clientlenssettingses.addAll(coreClient.getCoreClientlenssettingses());

            List<com.bgt.lens.model.rest.response.ClientLensSettings> clientLensSettings = new ArrayList<>();

            clientlenssettingses.forEach(clientlenssettings -> {
                com.bgt.lens.model.rest.response.ClientLensSettings cls = new com.bgt.lens.model.rest.response.ClientLensSettings();
                cls.setInstanceId(clientlenssettings.getCoreLenssettings().getId());
                cls.setRateLimitEnabled(clientlenssettings.isRateLimitEnabled());
                if (clientlenssettings.isRateLimitEnabled()) {
                    cls.setRateLimitTimePeriod(clientlenssettings.getRateLimitTimePeriod());
                    cls.setRateLimitCount(clientlenssettings.getRateLimitCount());
                }
                clientLensSettings.add(cls);
            });

            List<Integer> resourcesList = new ArrayList<>();
            resourceList.forEach(resources -> {
                resourcesList.add(resources.getId());
            });

            client.setLicensedResources(resourcesList);

            List<CoreClientapi> clientApiList = new ArrayList<>();
            clientApiList.addAll(coreClient.getCoreClientapis());

            List<com.bgt.lens.model.rest.response.ClientApi> clientApisList = new ArrayList<>();

            clientApiList.forEach(clientApi -> {
                com.bgt.lens.model.rest.response.ClientApi api = new com.bgt.lens.model.rest.response.ClientApi();
                //api.Id = clientApi.getId_1();
                api.setId(null);
                api.setApiId(clientApi.getCoreApi().getId());
                api.setApiKey(clientApi.getCoreApi().getApiKey());
                api.setEnabled(clientApi.isEnabled());
                api.setSecret(clientApi.getSecret());
                api.setActivationDate(clientApi.getActivationDate());
                api.setExpiryDate(clientApi.getExpiryDate());
                api.setAllowedTransactions(clientApi.getAllowedTransactions());
                api.setRemainingTransactions(clientApi.getRemainingTransactions());
                clientApisList.add(api);
            });
            client.setClientApi(clientApisList);
            clientList.add(client);
        });
        return clientList;
    }

    /**
     * Create License response
     *
     * @param coreClientList
     * @return
     */
    public List<License> createLicenseResponse(List<CoreClient> coreClientList) {
        List<License> licenseList = new ArrayList<>();

        for (CoreClient client : coreClientList) {
            License license = new License();

            license.setClientId(client.getId());

            List<com.bgt.lens.model.rest.response.ClientApi> clientApiList = new ArrayList<>();

            for (CoreClientapi clientApi : client.getCoreClientapis()) {
                com.bgt.lens.model.rest.response.ClientApi api = new com.bgt.lens.model.rest.response.ClientApi();
                api.setApiId(clientApi.getCoreApi().getId());
                api.setApiKey(clientApi.getCoreApi().getApiKey());
                api.setActivationDate(clientApi.getActivationDate());
                api.setExpiryDate(clientApi.getExpiryDate());
                api.setAllowedTransactions(clientApi.getAllowedTransactions());
                api.setRemainingTransactions(clientApi.getRemainingTransactions());
                api.setEnabled(clientApi.isEnabled());
                clientApiList.add(api);
            }

            clientApiList = _helper.sortClientAPI(clientApiList);
            license.setClientApi(clientApiList);

            licenseList.add(license);
        }

        licenseList = _helper.sortLicenseByClientId(licenseList);

        return licenseList;
    }

    /**
     * Create License response
     *
     * @param coreClient
     * @return
     */
    public License createLicenseResponse(CoreClient coreClient) {
        License license = new License();

        license.setClientId(coreClient.getId());

        List<com.bgt.lens.model.rest.response.ClientApi> clientApiList = new ArrayList<>();

        for (CoreClientapi clientApi : coreClient.getCoreClientapis()) {
            com.bgt.lens.model.rest.response.ClientApi api = new com.bgt.lens.model.rest.response.ClientApi();
            api.setApiId(clientApi.getCoreApi().getId());
            api.setApiKey(clientApi.getCoreApi().getApiKey());
            api.setActivationDate(clientApi.getActivationDate());
            api.setExpiryDate(clientApi.getExpiryDate());
            api.setAllowedTransactions(clientApi.getAllowedTransactions());
            api.setRemainingTransactions(clientApi.getRemainingTransactions());
            api.setEnabled(clientApi.isEnabled());
            clientApiList.add(api);
        }

        clientApiList = _helper.sortClientAPI(clientApiList);
        license.setClientApi(clientApiList);

        return license;
    }

    /**
     * Create Vendor Response
     *
     * @param searchVendorSettings
     * @return
     * @throws IOException
     */
    public Vendor createVendorResponse(SearchVendorsettings searchVendorSettings) throws IOException {
        Vendor vendor = new Vendor();
        if (_helper.isNotNull(searchVendorSettings)) {
            if (_helper.isNotNull(searchVendorSettings.getCoreLenssettings()) && (_helper.isNotNull(searchVendorSettings.getCoreLenssettings().getId()))) {
                vendor.setInstanceId(searchVendorSettings.getCoreLenssettings().getId());
                vendor.setInstanceType(searchVendorSettings.getCoreLenssettings().getInstanceType());
                vendor.setLocale(searchVendorSettings.getCoreLenssettings().getLocale());
            }
            vendor.setVendorId(searchVendorSettings.getId());
            vendor.setStatus(searchVendorSettings.getStatus());
            vendor.setVendor(searchVendorSettings.getVendorName());
            vendor.setType(searchVendorSettings.getType());
            vendor.setIsBGVendor(searchVendorSettings.isIsBGVendor());
            vendor.setMaxDocumentCount(searchVendorSettings.getMaxDocumentCount());
            vendor.setRegisteredDocumentCount(searchVendorSettings.getRegisteredDocumentCount());
            vendor.setCreatedOn(searchVendorSettings.getCreatedOn());
            vendor.setUpdatedOn(searchVendorSettings.getUpdatedOn());
        }
        return vendor;
    }

    /**
     * Create Vendor Response
     *
     * @param searchVendorSettings
     * @return
     * @throws IOException
     */
    public Vendor createDeleteVendorResponse(SearchVendorsettings searchVendorSettings) throws IOException {
        Vendor vendor = new Vendor();
        if (_helper.isNotNull(searchVendorSettings)) {
            if (_helper.isNotNull(searchVendorSettings.getCoreLenssettings()) && (_helper.isNotNull(searchVendorSettings.getCoreLenssettings().getId()))) {
                vendor.setInstanceId(searchVendorSettings.getCoreLenssettings().getId());
                vendor.setInstanceType(searchVendorSettings.getCoreLenssettings().getInstanceType());
                vendor.setLocale(searchVendorSettings.getCoreLenssettings().getLocale());
            }
            vendor.setVendorId(searchVendorSettings.getId());
            vendor.setStatus(searchVendorSettings.getStatus());
            vendor.setVendor(searchVendorSettings.getVendorName());
            vendor.setType(searchVendorSettings.getType());
            vendor.setMaxDocumentCount(searchVendorSettings.getMaxDocumentCount());
            vendor.setRegisteredDocumentCount(searchVendorSettings.getRegisteredDocumentCount());
            //vendor.setCreatedOn(searchVendorSettings.getCreatedOn());
            //vendor.setUpdatedOn(searchVendorSettings.getUpdatedOn());
        }
        return vendor;
    }

    public List<Vendor> createVendorResponseList(List<SearchVendorsettings> searchVendorSettingsList) throws IOException {
        List<Vendor> vendorList = new ArrayList<>();

        searchVendorSettingsList.forEach(searchVendorSettings -> {
            Vendor vendor = new Vendor();
            if (_helper.isNotNull(searchVendorSettings)) {
                if (_helper.isNotNull(searchVendorSettings.getCoreLenssettings()) && (_helper.isNotNull(searchVendorSettings.getCoreLenssettings().getId()))) {
                    vendor.setInstanceId(searchVendorSettings.getCoreLenssettings().getId());
                    vendor.setInstanceType(searchVendorSettings.getCoreLenssettings().getInstanceType());
                    vendor.setLocale(searchVendorSettings.getCoreLenssettings().getLocale());
                }
                vendor.setVendorId(searchVendorSettings.getId());
                vendor.setStatus(searchVendorSettings.getStatus());
                vendor.setVendor(searchVendorSettings.getVendorName());
                vendor.setType(searchVendorSettings.getType());
                vendor.setMaxDocumentCount(searchVendorSettings.getMaxDocumentCount());
                vendor.setRegisteredDocumentCount(searchVendorSettings.getRegisteredDocumentCount());
                vendor.setCreatedOn(searchVendorSettings.getCreatedOn());
                vendor.setUpdatedOn(searchVendorSettings.getUpdatedOn());
            }
            vendorList.add(vendor);
        });
        return vendorList;
    }

    /**
     * Order search results
     *
     * @param searchResult
     * @param orderResult
     * @return
     * @throws Exception
     */
    public SearchResult orderSearchResults(SearchResult searchResult, String orderResult, final String sort) throws Exception {
        List<Candidate> candidateList = searchResult.getCandidates();
        if (candidateList != null && candidateList.size() > 0) {
            if (orderResult.equalsIgnoreCase(orderResultsBy.id.toString())) {
                candidateList.sort(Comparator.comparing(Candidate::getResumeId, Comparator.nullsLast(Comparator.naturalOrder())));
            } else if (orderResult.equalsIgnoreCase(orderResultsBy.name.toString())) {
                candidateList.sort(Comparator.comparing(Candidate::getName, Comparator.nullsLast(Comparator.naturalOrder())));
            } else if (orderResult.equalsIgnoreCase(orderResultsBy.experience.toString())) {
                candidateList.sort(Comparator.comparing(Candidate::getYrsExp, Comparator.nullsLast(Comparator.naturalOrder())));
            }
            searchResult.setSortDirection(sortDirection.ASC.toString());
            if (sort.equalsIgnoreCase(sortDirection.DESC.toString())) {
                Collections.reverse(candidateList);
                searchResult.setSortDirection(sortDirection.DESC.toString());
            }
            searchResult.setCandidates(candidateList);
        }
        return searchResult;
    }

    /**
     * Clarify result
     *
     * @param clarifyResponse
     * @return
     */
    public List<String> parseClarifyResult(String clarifyResponse) throws XPathExpressionException {
        List<String> clarifyList = new ArrayList<>();
        if (_helper.isNotNullOrEmpty(clarifyResponse)) {
            InputSource is = new InputSource(new StringReader(clarifyResponse));
            XPathFactory factory = XPathFactory.newInstance();
            XPath xPath = factory.newXPath();
            NodeList clarifyResultSection = (NodeList) xPath.evaluate("/bgtres/clarify/word", is, XPathConstants.NODESET);

            if (clarifyResultSection != null && clarifyResultSection.getLength() > 0) {
                for (int temp = 0; temp < clarifyResultSection.getLength(); temp++) {
                    Node nNode = clarifyResultSection.item(temp);
                    clarifyList.add(nNode.getTextContent());
                }
            }
        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CLARIFY_RESPONSE), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return clarifyList;
    }

    public ApiResponse parseResumeFields(ApiResponse response, List<String> fields) throws Exception {
        try {
            ApiContext apiContext = new ApiContext();
            apiContext = _helper.getApiContext();

            if (fields.size() <= 0 || fields.contains(Fields.All.toString().toLowerCase(Locale.ENGLISH))) {

                if (_helper.isNotNull(apiContext.getAccept()) && apiContext.getAccept().equalsIgnoreCase(Enum.Headers.ApplicationJSON.toString())) {
                    // Remove empty nodes
                    DocumentBuilder dBuilder = null;
                    Document doc = null;
                    dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(((String) response.responseData).getBytes("UTF-8")));
                    doc = dBuilder.parse(new ByteArrayInputStream(((String) response.responseData).getBytes(xmlStreamReader.getCharacterEncodingScheme())));
                    xmlStreamReader.close();
                    String header = "";
                    if (((String) response.responseData).contains("<?xml version='1.0' encoding='iso-8859-1'?>")) {
                        header = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    } else if (((String) response.responseData).contains("<?xml version='1.0' encoding='utf-8'?>")) {
                        header = "<?xml version='1.0' encoding='utf-8'?>";
                    } else {
                        header = "<?xml version='1.0' encoding='utf-8'?>";
                    }

                    _helper.removeEmptyNodes(doc);
                    TransformerFactory tf = TransformerFactory.newInstance();
                    Transformer trans = tf.newTransformer();
                    trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                    StreamResult result = new StreamResult(new StringWriter());
                    trans.transform(new DOMSource(doc), result);
                    response.responseData = header + result.getWriter().toString();

                    try {
                        XMLSerializer xmlSerializer = new XMLSerializer();
                        xmlSerializer.setForceTopLevelObject(true);
                        JSON out = xmlSerializer.read((String) response.responseData);
                        String update = out.toString().replaceAll("\":null", "\":{}");
                        JSONObject JSO = JSONObject.fromObject(update);

                        response.responseData = JSO;
                        return response;
                    } catch (Exception ex) {
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.JSON_TRANSFORM_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                } else {
                    return response;
                }
            }
            if (_helper.isNotNull(response) && _helper.isNotNull(response.responseData)) {

                InputSource is = new InputSource(new StringReader((String) response.responseData));

                String responseData = (String) response.responseData;
                String combinedXml = "";

                if (responseData.contains("<?xml version='1.0' encoding='iso-8859-1'?>")) {
                    combinedXml += "<?xml version='1.0' encoding='iso-8859-1'?>";
                } else if (responseData.contains("<?xml version='1.0' encoding='utf-8'?>")) {
                    combinedXml += "<?xml version='1.0' encoding='utf-8'?>";
                } else {
                    combinedXml += "<?xml version='1.0' encoding='utf-8'?>";
                }

                combinedXml += "<ResDoc>";

                XPathFactory factory = XPathFactory.newInstance();
                XPath xPath = factory.newXPath();

                // Get fields from resume section
                combinedXml += "<resume";

                NodeList resumeSection = (NodeList) xPath.evaluate("/ResDoc/resume", is, XPathConstants.NODESET);

                for (int temp = 0; temp < 1; temp++) {
                    Node nNode = resumeSection.item(temp);
                    NamedNodeMap nMap = nNode.getAttributes();
                    for (int temp1 = 0; temp1 < nMap.getLength(); temp1++) {
                        combinedXml += " " + nMap.item(temp1).getNodeName() + "=\"" + nMap.item(temp1).getNodeValue() + "\"";
                    }
                }
                combinedXml += ">";

                // Get Contact section
                if (fields.contains(Fields.Contact.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList contactSection = (NodeList) xPath.evaluate("/ResDoc/resume/contact", is, XPathConstants.NODESET);
                    String contact = getOuterXml(contactSection);
                    if (_helper.isNotNullOrEmpty(contact)) {
                        combinedXml += contact;
                    } else {
                        combinedXml += "<contact></contact>";
                    }
                }

                // Get Education section
                if (fields.contains(Fields.Education.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList educationSection = (NodeList) xPath.evaluate("/ResDoc/resume/education", is, XPathConstants.NODESET);
                    String education = getOuterXml(educationSection);
                    if (_helper.isNotNullOrEmpty(education)) {
                        combinedXml += education;
                    } else {
                        combinedXml += "<education></education>";
                    }
                }

                // Get Experience section
                if (fields.contains(Fields.Experience.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList experienceSection = (NodeList) xPath.evaluate("/ResDoc/resume/experience", is, XPathConstants.NODESET);
                    String exp = getOuterXml(experienceSection);
                    if (_helper.isNotNullOrEmpty(exp)) {
                        combinedXml += exp;
                    } else {
                        combinedXml += "<experience></experience>";
                    }
                }

                // Get Professional section
                if (fields.contains(Fields.Professional.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList professionalSection = (NodeList) xPath.evaluate("/ResDoc/resume/professional", is, XPathConstants.NODESET);
                    String prof = getOuterXml(professionalSection);
                    if (_helper.isNotNullOrEmpty(prof)) {
                        combinedXml += prof;
                    } else {
                        combinedXml += "<professional></professional>";
                    }
                }

                // Get References section
                if (fields.contains(Fields.References.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList referencesSection = (NodeList) xPath.evaluate("/ResDoc/resume/references", is, XPathConstants.NODESET);
                    String ref = getOuterXml(referencesSection);
                    if (_helper.isNotNullOrEmpty(ref)) {
                        combinedXml += ref;
                    } else {
                        combinedXml += "<references></references>";
                    }
                }

                // Get Skills section
                if (fields.contains(Fields.Skills.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList skillsSection = (NodeList) xPath.evaluate("/ResDoc/resume/skills", is, XPathConstants.NODESET);
                    String skills = getOuterXml(skillsSection);
                    if (_helper.isNotNullOrEmpty(skills)) {
                        combinedXml += skills;
                    } else {
                        combinedXml += "<skills></skills>";
                    }
                }

                // Get Statements section
                if (fields.contains(Fields.Statements.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList statementsSection = (NodeList) xPath.evaluate("/ResDoc/resume/statements", is, XPathConstants.NODESET);
                    String statements = getOuterXml(statementsSection);
                    if (_helper.isNotNullOrEmpty(statements)) {
                        combinedXml += statements;
                    } else {
                        combinedXml += "<statements></statements>";
                    }
                }

                // Get Summary section
                if (fields.contains(Fields.Summary.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList summarySection = (NodeList) xPath.evaluate("/ResDoc/resume/summary", is, XPathConstants.NODESET);
                    String summary = getOuterXml(summarySection);
                    if (_helper.isNotNullOrEmpty(summary)) {
                        combinedXml += summary;
                    } else {
                        combinedXml += "<summary></summary>";
                    }
                }

                combinedXml += "</resume>";

                // Get Skillrollup section
                if (fields.contains(Fields.SkillRollUp.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList skillrollupSection = (NodeList) xPath.evaluate("/ResDoc/skillrollup", is, XPathConstants.NODESET);
                    String skillrollup = getOuterXml(skillrollupSection);
                    if (_helper.isNotNullOrEmpty(skillrollup)) {
                        combinedXml += skillrollup;
                    } else {
                        combinedXml += "<skillrollup></skillrollup>";
                    }
                }

                // Get DataElementsRollup section
                if (fields.contains(Fields.DataElementsRollup.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList dataElementsRollupSection = (NodeList) xPath.evaluate("/ResDoc/DataElementsRollup", is, XPathConstants.NODESET);
                    String dataElementsRollup = getOuterXml(dataElementsRollupSection);
                    if (_helper.isNotNullOrEmpty(dataElementsRollup)) {
                        combinedXml += dataElementsRollup;
                    } else {
                        combinedXml += "<DataElementsRollup></DataElementsRollup>";
                    }
                }

                combinedXml += "</ResDoc>";

                if (_helper.isNotNull(apiContext.getAccept()) && apiContext.getAccept().equalsIgnoreCase(Enum.Headers.ApplicationJSON.toString())) {
                    // Remove empty nodes
                    DocumentBuilder dBuilder = null;
                    Document doc = null;
                    dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(((String) response.responseData).getBytes("UTF-8")));
                    doc = dBuilder.parse(new ByteArrayInputStream(combinedXml.getBytes(xmlStreamReader.getCharacterEncodingScheme())));

                    String header = "";
                    if (combinedXml.contains("<?xml version='1.0' encoding='iso-8859-1'?>")) {
                        header = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    } else if (combinedXml.contains("<?xml version='1.0' encoding='utf-8'?>")) {
                        header = "<?xml version='1.0' encoding='utf-8'?>";
                    } else {
                        header = "<?xml version='1.0' encoding='utf-8'?>";
                    }

                    _helper.removeEmptyNodes(doc);
                    TransformerFactory tf = TransformerFactory.newInstance();
                    Transformer trans = tf.newTransformer();
                    trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    StreamResult result = new StreamResult(new OutputStreamWriter(bos, xmlStreamReader.getCharacterEncodingScheme()));
                    trans.transform(new DOMSource(doc), result);
                    combinedXml = header + new String(bos.toByteArray());
                    try {
                        XMLSerializer xmlSerializer = new XMLSerializer();
                        xmlSerializer.setForceTopLevelObject(true);
                        JSON out = xmlSerializer.read(combinedXml);
//                        String update = out.toString().replaceAll("\":null", "\":{}");
//                        JSONObject JSO = JSONObject.fromObject(update);
                        response.responseData = out;
                        return response;
                    } catch (Exception ex) {
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.JSON_TRANSFORM_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                } else {
                    response.responseData = combinedXml;
                    return response;
                }

            }
        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESUME_EXTRACT_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return response;
    }

    /**
     * Parse posting fields
     *
     * @param response
     * @param fields
     * @return
     * @throws Exception
     */
    public ApiResponse parseJobFields(ApiResponse response, List<String> fields) throws Exception {
        try {
            ApiContext apiContext = new ApiContext();
            apiContext = _helper.getApiContext();
            if (fields.size() <= 0 || fields.contains(Fields.All.toString().toLowerCase(Locale.ENGLISH))) {
                if (_helper.isNotNull(apiContext.getAccept()) && apiContext.getAccept().equalsIgnoreCase(Enum.Headers.ApplicationJSON.toString())) {
                    // Remove empty nodes
                    DocumentBuilder dBuilder = null;
                    Document doc = null;
                    dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(((String) response.responseData).getBytes("UTF-8")));
                    doc = dBuilder.parse(new ByteArrayInputStream(((String) response.responseData).getBytes(xmlStreamReader.getCharacterEncodingScheme())));

                    String header = "";
                    if (((String) response.responseData).contains("<?xml version='1.0' encoding='iso-8859-1'?>")) {
                        header = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    } else if (((String) response.responseData).contains("<?xml version='1.0' encoding='utf-8'?>")) {
                        header = "<?xml version='1.0' encoding='utf-8'?>";
                    } else {
                        header = "<?xml version='1.0' encoding='utf-8'?>";
                    }

                    _helper.removeEmptyNodes(doc);
                    TransformerFactory tf = TransformerFactory.newInstance();
                    Transformer trans = tf.newTransformer();
                    trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    StreamResult result = new StreamResult(new OutputStreamWriter(bos));
                    trans.transform(new DOMSource(doc), result);
                    response.responseData = header + new String(bos.toByteArray());

                    try {
                        XMLSerializer xmlSerializer = new XMLSerializer();
                        xmlSerializer.setForceTopLevelObject(true);
                        JSON out = xmlSerializer.read((String) response.responseData);
//                        String update = out.toString().replaceAll("\":null", "\":{}");
//                        JSONObject JSO = JSONObject.fromObject(update);
                        response.responseData = out;
                        return response;
                    } catch (Exception ex) {
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.JSON_TRANSFORM_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                } else {
                    return response;
                }
            }
            if (_helper.isNotNull(response) && _helper.isNotNull(response.responseData)) {

                InputSource is = new InputSource(new StringReader((String) response.responseData));

                String responseData = (String) response.responseData;
                String combinedXml = "";

                if (responseData.contains("<?xml version='1.0' encoding='iso-8859-1'?>")) {
                    combinedXml += "<?xml version='1.0' encoding='iso-8859-1'?>";
                } else if (responseData.contains("<?xml version='1.0' encoding='utf-8'?>")) {
                    combinedXml += "<?xml version='1.0' encoding='utf-8'?>";
                } else {
                    combinedXml += "<?xml version='1.0' encoding='utf-8'?>";
                }

                combinedXml += "<JobDoc>";

                XPathFactory factory = XPathFactory.newInstance();
                XPath xPath = factory.newXPath();

                //Get fields from posting section
                combinedXml += "<posting";

                NodeList resumeSection = (NodeList) xPath.evaluate("/JobDoc/posting", is, XPathConstants.NODESET);

                for (int temp = 0; temp < 1; temp++) {
                    Node nNode = resumeSection.item(temp);
                    NamedNodeMap nMap = nNode.getAttributes();
                    for (int temp1 = 0; temp1 < nMap.getLength(); temp1++) {
                        combinedXml += " " + nMap.item(temp1).getNodeName() + "=\"" + nMap.item(temp1).getNodeValue() + "\"";
                    }
                }
                combinedXml += ">";

                // Get Duties section 
                if (fields.contains(Fields.Duties.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList dutiesSection = (NodeList) xPath.evaluate("/JobDoc/posting/duties", is, XPathConstants.NODESET);
                    String duties = getOuterXml(dutiesSection);
                    if (_helper.isNotNullOrEmpty(duties)) {
                        combinedXml += duties;
                    } else {
                        combinedXml += "<duties></duties>";
                    }
                }

                // Get Contact section 
                if (fields.contains(Fields.Contact.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList contactSection = (NodeList) xPath.evaluate("/JobDoc/posting/contact", is, XPathConstants.NODESET);
                    String contact = getOuterXml(contactSection);
                    if (_helper.isNotNullOrEmpty(contact)) {
                        combinedXml += contact;
                    } else {
                        combinedXml += "<contact></contact>";
                    }
                }

                // Get BackGround section 
                if (fields.contains(Fields.BackGround.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList backgroundSection = (NodeList) xPath.evaluate("/JobDoc/posting/background", is, XPathConstants.NODESET);
                    String background = getOuterXml(backgroundSection);
                    if (_helper.isNotNullOrEmpty(background)) {
                        combinedXml += background;
                    } else {
                        combinedXml += "<background></background>";
                    }
                }

                combinedXml += "</posting>";

                // Get SkillRollUp section
                if (fields.contains(Fields.SkillRollUp.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList skillrollupSection = (NodeList) xPath.evaluate("/JobDoc/skillrollup", is, XPathConstants.NODESET);
                    String skillrollup = getOuterXml(skillrollupSection);
                    if (_helper.isNotNullOrEmpty(skillrollup)) {
                        combinedXml += skillrollup;
                    } else {
                        combinedXml += "<skillrollup></skillrollup>";
                    }
                }

                // Get DataElementsRollup section
                if (fields.contains(Fields.DataElementsRollup.toString().toLowerCase(Locale.ENGLISH))) {
                    is = new InputSource(new StringReader((String) response.responseData));
                    NodeList dataElementsRollupSection = (NodeList) xPath.evaluate("/JobDoc/DataElementsRollup", is, XPathConstants.NODESET);
                    String dataElementsRollup = getOuterXml(dataElementsRollupSection);
                    if (_helper.isNotNullOrEmpty(dataElementsRollup)) {
                        combinedXml += dataElementsRollup;
                    } else {
                        combinedXml += "<DataElementsRollup></DataElementsRollup>";
                    }
                }

                combinedXml += "</JobDoc>";

                if (_helper.isNotNull(apiContext.getAccept()) && apiContext.getAccept().equalsIgnoreCase(Enum.Headers.ApplicationJSON.toString())) {
                    // Remove empty nodes
                    DocumentBuilder dBuilder = null;
                    Document doc = null;
                    dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(((String) response.responseData).getBytes("UTF-8")));
                    doc = dBuilder.parse(new ByteArrayInputStream(combinedXml.getBytes(xmlStreamReader.getCharacterEncodingScheme())));

                    String header = "";
                    if (combinedXml.contains("<?xml version='1.0' encoding='iso-8859-1'?>")) {
                        header = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    } else if (combinedXml.contains("<?xml version='1.0' encoding='utf-8'?>")) {
                        header = "<?xml version='1.0' encoding='utf-8'?>";
                    } else {
                        header = "<?xml version='1.0' encoding='utf-8'?>";
                    }

                    _helper.removeEmptyNodes(doc);
                    TransformerFactory tf = TransformerFactory.newInstance();
                    Transformer trans = tf.newTransformer();
                    trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    StreamResult result = new StreamResult(new OutputStreamWriter(bos, xmlStreamReader.getCharacterEncodingScheme()));
                    trans.transform(new DOMSource(doc), result);
                    combinedXml = header + new String(bos.toByteArray());
                    try {
                        XMLSerializer xmlSerializer = new XMLSerializer();
                        xmlSerializer.setForceTopLevelObject(true);
                        JSON out = xmlSerializer.read(combinedXml);
//                        String update = out.toString().replaceAll("\":null", "\":{}");
//                        JSONObject JSO = JSONObject.fromObject(update);
//                        org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
//                        JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
                        response.responseData = out;
                        return response;
                    } catch (Exception ex) {
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.JSON_TRANSFORM_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                } else {
                    response.responseData = combinedXml;
                    return response;
                }

            }
        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESUME_EXTRACT_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return response;
    }

    public String getOuterXml(NodeList nodeList) throws TransformerConfigurationException, TransformerException {
        String response = "";
        if (nodeList != null && nodeList.getLength() > 0) {
            for (int i = 0; i < nodeList.getLength(); ++i) {
                Node nNode = nodeList.item(i);
                StreamResult xmlOutput = new StreamResult(new StringWriter());
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.transform(new DOMSource(nNode), xmlOutput);
                response += xmlOutput.getWriter().toString();
            }
        }
        return response;
    }

    public NodeList getNodeListFromXPath(String inputString, String xpath) throws XPathExpressionException {
        InputSource is = new InputSource(new StringReader(inputString));
        XPathFactory factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();
        NodeList nodeList = (NodeList) xPath.evaluate(xpath, is, XPathConstants.NODESET);
        return nodeList;
    }

    /**
     * Get OuterXML form Document object
     *
     * @param document
     * @return
     * @throws Exception
     */
    public String getOuterXml(Document document) throws Exception {
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        DOMSource source = new DOMSource(document);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        StreamResult result = new StreamResult(outStream);
        transformer.transform(source, result);
        String resultString = new String(outStream.toByteArray());
        return resultString;
    }

    public String getOuterXml(Node node)
            throws TransformerConfigurationException, TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(node), new StreamResult(writer));
        return writer.toString();
    }

    /**
     * Order Job search results
     *
     * @param searchResult
     * @param orderResult
     * @return
     * @throws Exception
     */
    public SearchResult orderJobSearchResults(SearchResult searchResult, String orderResult, final String sort) throws Exception {
        List<Job> jobList = searchResult.getJobs();
        if (jobList != null && jobList.size() > 0) {
            if (orderResult.equalsIgnoreCase(orderResultsBy.id.toString())) {
                jobList.sort(Comparator.comparing(Job::getJobId, Comparator.nullsLast(Comparator.naturalOrder())));
            }
            searchResult.setSortDirection(sortDirection.ASC.toString());
            if (sort.equalsIgnoreCase(sortDirection.DESC.toString())) {
                Collections.reverse(jobList);
                searchResult.setSortDirection(sortDirection.DESC.toString());
            }
            searchResult.setJobs(jobList);
        }
        return searchResult;
    }    
}
