// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.entity.CoreClient;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * Client Dao Interface
 */
public interface ICoreClientDao {

    /**
     * Create client
     *
     * @param coreClient
     */
    public void save(CoreClient coreClient);

    /**
     * Update client
     *
     * @param coreClient
     * @param updatedCoreClient
     */
    public void update(CoreClient coreClient, CoreClient updatedCoreClient);
    
     /**
     * Update client
     *
     * @param id
     * @param coreClient
     * @param updatedCoreClient
     */
    public void update(Set<BigInteger> id, Map<BigInteger,CoreClient>coreClient,Map<BigInteger,CoreClient> updatedCoreClient);

    /**
     * Delete client
     *
     * @param coreClient
     */
    public void delete(CoreClient coreClient);

    /**
     * Get client by id
     *
     * @param clientId
     * @return
     */
    public CoreClient getClientById(int clientId);

    /**
     * Get client by criteria
     *
     * @param clientCriteria
     * @return
     */
    public List<CoreClient> getClientByCriteria(ClientCriteria clientCriteria);

}
