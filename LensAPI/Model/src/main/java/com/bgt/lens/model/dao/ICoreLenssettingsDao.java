// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.LensSettingsCriteria;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreFailoverlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import java.util.List;
import java.util.Set;

/**
 *
 * Lens settings Dao Interface
 */
public interface ICoreLenssettingsDao {

    /**
     * Create Lens settings
     *
     * @param coreLenssettings
     */
    public void save(CoreLenssettings coreLenssettings);

    /**
     * Update Lens settings
     *
     * @param coreLenssettings
     * @param deleteCoreCustomskillsettings
     */
    public void update(CoreLenssettings coreLenssettings, Set<CoreCustomskillsettings> deleteCoreCustomskillsettings);

    /**
     * Delete Lens settings
     *
     * @param coreLenssettings
     */
    public void delete(CoreLenssettings coreLenssettings);

    /**
     * Get Lens settings by id
     *
     * @param lensSettingId
     * @return
     */
    public CoreLenssettings getLensSettingsById(int lensSettingId);
    
        /**
     * Get Lens settings by id
     *
     * @param lensSettingId
     * @return
     */
    public CoreFailoverlenssettings getFailoverLensSettingsById(int lensSettingId);

    /**
     * Get Lens settings by criteria
     *
     * @param lensSettingsCriteria
     * @return
     */
    public List<CoreLenssettings> getLensSettingsByCriteria(LensSettingsCriteria lensSettingsCriteria);
}
