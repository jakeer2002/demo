// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Employment history
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class EmploymentHistory {
    /**
     * Job title
     */
    @XmlElement(name = "JobTitle", required = false)
    protected String jobTitle;
    
    /**
     * Employer
     */
    @XmlElement(name = "Employer", required = false)
    protected String employer;
    
    /**
     * Start date
     */
    @XmlElement(name = "Start", required = false)
    protected String start;
    
    /**
     * End date
     */
    @XmlElement(name = "End", required = false)
    protected String end;

    /**
     * Get job title
     * @return 
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Set job title
     * @param jobTitle 
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * Get employer
     * @return 
     */
    public String getEmployer() {
        return employer;
    }

    /**
     * Set employer
     * @param employer 
     */
    public void setEmployer(String employer) {
        this.employer = employer;
    }

    /**
     * Get start
     * @return 
     */
    public String getStart() {
        return start;
    }

    /**
     * Set start
     * @param start 
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * Get end
     * @return 
     */
    public String getEnd() {
        return end;
    }

    /**
     * Set end
     * @param end 
     */
    public void setEnd(String end) {
        this.end = end;
    }
}
