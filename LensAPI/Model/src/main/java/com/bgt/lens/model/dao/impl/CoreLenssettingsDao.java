// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.LensSettingsCriteria;
import com.bgt.lens.model.dao.ICoreLenssettingsDao;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreFailoverlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * Lens settings table data management
 */
@Transactional
public class CoreLenssettingsDao implements ICoreLenssettingsDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize Lens settings Dao
     */
    public CoreLenssettingsDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set Session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create Len settings
     *
     * @param coreLenssettings
     */
    @Override
    public void save(CoreLenssettings coreLenssettings) {
        helperDao.save(sessionFactory, coreLenssettings);
    }

    /**
     * Update Lens settings
     *
     * @param coreLenssettings
     */
    @Override
    public void update(CoreLenssettings coreLenssettings, Set<CoreCustomskillsettings> deleteCoreCustomskillsettings) {
        //helperDao.update(sessionFactory, coreLenssettings);
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            session.saveOrUpdate(coreLenssettings);
            session.flush();

            for (CoreCustomskillsettings coreCustomskillsettings : deleteCoreCustomskillsettings) {
                session.delete(coreCustomskillsettings);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Delete Lens settings
     *
     * @param coreLenssettings
     */
    @Override
    public void delete(CoreLenssettings coreLenssettings) {
        coreLenssettings.getCoreLenssettingsesForFailoverInstanceId().clear();
        helperDao.delete(sessionFactory, coreLenssettings);
    }

    /**
     * Get Lens settings by id
     *
     * @param lensSettingId
     * @return
     */
    @Override
    public CoreLenssettings getLensSettingsById(int lensSettingId) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            CoreLenssettings coreLenssettings = (CoreLenssettings) session.get(CoreLenssettings.class, lensSettingId);

            if (coreLenssettings != null) {
                Hibernate.initialize(coreLenssettings.getCoreLenssettingsesForFailoverInstanceId());
                Hibernate.initialize(coreLenssettings.getCoreLenssettingsesForLensSettingsId());
                Hibernate.initialize(coreLenssettings.getCoreClientlenssettingses());
                Hibernate.initialize(coreLenssettings.getCoreCustomskillsettings());
            }
            tx.commit();
            LOGGER.info("Transaction Commit");
            return coreLenssettings;
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        //return (CoreLenssettings) helperDao.getById(sessionFactory, CoreLenssettings.class, lensSettingId);
    }

    /**
     * Get Lens settings by id
     *
     * @param lensSettingId
     * @return
     */
    @Override
    public CoreFailoverlenssettings getFailoverLensSettingsById(int lensSettingId) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            CoreFailoverlenssettings coreLenssettings = (CoreFailoverlenssettings) session.get(CoreFailoverlenssettings.class, lensSettingId);

            tx.commit();
            LOGGER.info("Transaction Commit");
            return coreLenssettings;
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        //return (CoreLenssettings) helperDao.getById(sessionFactory, CoreLenssettings.class, lensSettingId);
    }

    /**
     * Get Lens settings by criteria
     *
     * @param lensSettingsCriteria
     * @return
     */
    @Override
    public List<CoreLenssettings> getLensSettingsByCriteria(LensSettingsCriteria lensSettingsCriteria) {
        List<CoreLenssettings> coreLensSettingsList = new ArrayList<>();
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Object> criteriaQuery = cb.createQuery(Object.class);
            Root<CoreLenssettings> coreLenssettingsRoot = criteriaQuery.from(CoreLenssettings.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            Join<?,?> joins = null;

            if (!_helper.isNull(lensSettingsCriteria.getId())) {
            	conditions.add(cb.equal(coreLenssettingsRoot.get("id"), lensSettingsCriteria.getId()));
            }

            if (!_helper.isNullOrEmpty(lensSettingsCriteria.getHost())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("host"), lensSettingsCriteria.getHost()));
            }

            if (!_helper.isNullOrEmpty(lensSettingsCriteria.getCharacterSet())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("characterSet"), lensSettingsCriteria.getCharacterSet()));
            }

            if (!_helper.isNullOrEmpty(lensSettingsCriteria.getLocale())) {
                conditions.add(cb.equal(cb.lower(coreLenssettingsRoot.get("locale")), lensSettingsCriteria.getLocale().toLowerCase()));
            }

            if (!_helper.isNullOrEmpty(lensSettingsCriteria.getCountry())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("country"), lensSettingsCriteria.getCountry()));
            }

            if (!_helper.isNullOrEmpty(lensSettingsCriteria.getLanguage())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("language"), lensSettingsCriteria.getLanguage()));
            }
            // Added HRXML version in get criteria
            if (!_helper.isNullOrEmpty(lensSettingsCriteria.getHrxmlVersion())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("hrxmlversion"), lensSettingsCriteria.getHrxmlVersion()));
            }

            // Added HRXML file name in get criteria
            if (!_helper.isNullOrEmpty(lensSettingsCriteria.getHrxmlFileName())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("hrxmlfilename"), lensSettingsCriteria.getHrxmlFileName()));
            }

            if (!_helper.isNullOrEmpty(lensSettingsCriteria.getLensVersion())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("lensVersion"), lensSettingsCriteria.getLensVersion()));
            }

            if (!_helper.isNullOrEmpty(lensSettingsCriteria.getInstanceType())) {
                conditions.add(cb.equal(cb.lower(coreLenssettingsRoot.get("instanceType")), lensSettingsCriteria.getInstanceType().toLowerCase()));
            }

            if (_helper.isNotNull(lensSettingsCriteria.getTimeout()) && lensSettingsCriteria.getTimeout() > 0) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("timeout"), lensSettingsCriteria.getTimeout()));
            }

            if (_helper.isNotNull(lensSettingsCriteria.getPort()) && lensSettingsCriteria.getPort() > 0) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("port"), lensSettingsCriteria.getPort()));
            }

            //added by sic
            if (_helper.isNotNull(lensSettingsCriteria.getDocServerResume())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("docServerResume"), lensSettingsCriteria.getDocServerResume()));
            }
            if (_helper.isNotNull(lensSettingsCriteria.getDocServerPostings())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("docServerPostings"), lensSettingsCriteria.getDocServerPostings()));
            }
            if (_helper.isNotNull(lensSettingsCriteria.getHypercubeServerResume())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("hypercubeServerResume"), lensSettingsCriteria.getHypercubeServerResume()));
            }
            if (_helper.isNotNull(lensSettingsCriteria.getHypercubeServerPostings())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("hypercubeServerPostings"), lensSettingsCriteria.getHypercubeServerPostings()));
            }

            if (_helper.isNotNull(lensSettingsCriteria.isStatus())) {
                conditions.add(cb.equal(coreLenssettingsRoot.get("status"), lensSettingsCriteria.isStatus()));
            }

            if (_helper.isNotNull(lensSettingsCriteria.getCoreClient())) {
                Join<CoreLenssettings, CoreClientlenssettings> childJoin = coreLenssettingsRoot.join( "coreClientlenssettingses" );
                joins = childJoin;
                conditions.add(cb.equal(childJoin.get("coreClient").get("id"), lensSettingsCriteria.getCoreClient().getId()));
            }
            
            // ProjectionList
            List<Selection<?>> selectlist = new ArrayList<Selection<?>>();
            selectlist.add(coreLenssettingsRoot.get("id"));
            selectlist.add(coreLenssettingsRoot.get("host"));
            selectlist.add(coreLenssettingsRoot.get("port"));
            selectlist.add(coreLenssettingsRoot.get("characterSet"));
            selectlist.add(coreLenssettingsRoot.get("timeout"));
            selectlist.add(coreLenssettingsRoot.get("locale"));
            
            selectlist.add(coreLenssettingsRoot.get("language"));
            selectlist.add(coreLenssettingsRoot.get("country"));

            if (_helper.isNotNull(lensSettingsCriteria.isIsHRXML())) {
                if (lensSettingsCriteria.isIsHRXML()) {
                    selectlist.add(coreLenssettingsRoot.get("hrxmlcontent"));
                }
            }
            // Added HRXML version to get
            selectlist.add(coreLenssettingsRoot.get("hrxmlversion"));
            selectlist.add(coreLenssettingsRoot.get("hrxmlfilename"));
            selectlist.add(coreLenssettingsRoot.get("lensVersion"));
            selectlist.add(coreLenssettingsRoot.get("instanceType"));
            selectlist.add(coreLenssettingsRoot.get("customFilterSettings"));
            selectlist.add(coreLenssettingsRoot.get("status"));
            selectlist.add(coreLenssettingsRoot.get("createdOn"));
            
            //added by sic
            selectlist.add(coreLenssettingsRoot.get("docServerResume"));
            selectlist.add(coreLenssettingsRoot.get("docServerPostings"));
            selectlist.add(coreLenssettingsRoot.get("hypercubeServerResume"));
            selectlist.add(coreLenssettingsRoot.get("hypercubeServerPostings"));

            if (_helper.isNotNull(lensSettingsCriteria.isFailover())) {
                if (lensSettingsCriteria.isFailover()) {
                    selectlist.add(coreLenssettingsRoot.get("coreLenssettingsesForFailoverInstanceId"));
                }
            }
            if (_helper.isNotNull(lensSettingsCriteria.getCoreClient())) {
                selectlist.add(joins.get("rateLimitEnabled"));
                selectlist.add(joins.get("rateLimitTimePeriod"));
                selectlist.add(joins.get("rateLimitCount"));
                Join<CoreClientlenssettings,CoreCustomskillsettings> childJoin= joins.join("coreCustomskillsettings",JoinType.LEFT);
                selectlist.add(childJoin);
            }

            criteriaQuery.multiselect(selectlist).distinct(true);
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<Object> query = session.createQuery(criteriaQuery);
            List<Object> results = query.getResultList();
            
            // Custom Result set transformer
            // Not safe - regression may occur 
            // TODO : validate data & Format
            if (results != null && results.size() > 0) {
                //List<CoreLenssettings> coreLenssettingses = new ArrayList<>();
                Iterator<Object> iterator = results.iterator();
                for (int i = 0; i < results.size(); i++) {
                    CoreLenssettings coreLenssettings = new CoreLenssettings();

                    CoreClientlenssettings clientlenssettings = new CoreClientlenssettings();
                    Set<CoreClientlenssettings> clientlenssettingses = new HashSet<>();
                    Object obj[] = (Object[]) iterator.next();

                    if (obj.length == 23) {
                        // Set Core Lens settings
                        coreLenssettings.setId((int) (obj[0] != null ? obj[0] : 0));
                        coreLenssettings.setHost((String) obj[1]);
                        coreLenssettings.setPort((int) (obj[2] != null ? obj[2] : 0));
                        coreLenssettings.setCharacterSet((String) obj[3]);
                        coreLenssettings.setTimeout((int) (obj[4] != null ? obj[4] : 0));
                        coreLenssettings.setLocale((String) obj[5]);
                        coreLenssettings.setLanguage((String) obj[6]);
                        coreLenssettings.setCountry((String) obj[7]);
                        // coreLenssettings.setHrxmlcontent((String)obj[8]);
                        coreLenssettings.setHrxmlversion((String) obj[8]);
                        coreLenssettings.setHrxmlfilename((String) obj[9]);
                        coreLenssettings.setLensVersion((String) obj[10]);
                        coreLenssettings.setInstanceType((String) obj[11]);
                        coreLenssettings.setCustomFilterSettings((String) obj[12]);
                        coreLenssettings.setStatus((boolean) obj[13]);
                        coreLenssettings.setCreatedOn((Date) obj[14]);
                        //added by sic
                        coreLenssettings.setDocServerResume((String) obj[15]);
                        coreLenssettings.setDocServerPostings((String) obj[16]);
                        coreLenssettings.setHypercubeServerResume((String) obj[17]);
                        coreLenssettings.setHypercubeServerPostings((String) obj[18]);

                        clientlenssettings.setRateLimitEnabled((boolean) obj[19]);
                        clientlenssettings.setRateLimitTimePeriod((Integer) obj[20]);
                        clientlenssettings.setRateLimitCount((Integer) obj[21]);
                        clientlenssettings.setCoreCustomskillsettings((CoreCustomskillsettings) obj[22]);

                        clientlenssettingses.add(clientlenssettings);

                        coreLenssettings.setCoreClientlenssettingses(clientlenssettingses);
                        coreLensSettingsList.add(coreLenssettings);
                    } else if (obj.length == 24) {
                        // Set Core Lens settings
                        coreLenssettings.setId((int) (obj[0] != null ? obj[0] : 0));
                        coreLenssettings.setHost((String) obj[1]);
                        coreLenssettings.setPort((int) (obj[2] != null ? obj[2] : 0));
                        coreLenssettings.setCharacterSet((String) obj[3]);
                        coreLenssettings.setTimeout((int) (obj[4] != null ? obj[4] : 0));
                        coreLenssettings.setLocale((String) obj[5]);
                        coreLenssettings.setLanguage((String) obj[6]);
                        coreLenssettings.setCountry((String) obj[7]);
                        coreLenssettings.setHrxmlcontent((String) obj[8]);
                        coreLenssettings.setHrxmlversion((String) obj[9]);
                        coreLenssettings.setHrxmlfilename((String) obj[10]);
                        coreLenssettings.setLensVersion((String) obj[11]);
                        coreLenssettings.setInstanceType((String) obj[12]);
                        coreLenssettings.setCustomFilterSettings((String) (obj[13] != null ? obj[13] : ""));
                        coreLenssettings.setStatus((boolean) (obj[14] != null ? obj[14] : false));
                        coreLenssettings.setCreatedOn((Date) obj[15]);
                        //added by sic
                        coreLenssettings.setDocServerResume((String) obj[16]);
                        coreLenssettings.setDocServerPostings((String) obj[17]);
                        coreLenssettings.setHypercubeServerResume((String) obj[18]);
                        coreLenssettings.setHypercubeServerPostings((String) obj[19]);

                        clientlenssettings.setRateLimitEnabled((boolean) obj[20]);
                        clientlenssettings.setRateLimitTimePeriod((Integer) obj[21]);
                        clientlenssettings.setRateLimitCount((Integer) obj[22]);
                        clientlenssettings.setCoreCustomskillsettings((CoreCustomskillsettings) obj[23]);

                        clientlenssettingses.add(clientlenssettings);

                        coreLenssettings.setCoreClientlenssettingses(clientlenssettingses);
                        coreLensSettingsList.add(coreLenssettings);
                    } else if (obj.length == 20) {
                        // Set Core Lens settings
                        coreLenssettings.setId((int) (obj[0] != null ? obj[0] : 0));
                        coreLenssettings.setHost((String) obj[1]);
                        coreLenssettings.setPort((int) (obj[2] != null ? obj[2] : 0));
                        coreLenssettings.setCharacterSet((String) obj[3]);
                        coreLenssettings.setTimeout((int) (obj[4] != null ? obj[4] : 0));
                        coreLenssettings.setLocale((String) obj[5]);
                        coreLenssettings.setLanguage((String) obj[6]);
                        coreLenssettings.setCountry((String) obj[7]);
                        coreLenssettings.setHrxmlcontent((String) obj[8]);
                        coreLenssettings.setHrxmlversion((String) obj[9]);
                        coreLenssettings.setHrxmlfilename((String) (obj[10] != null ? obj[10] : new String()));
                        coreLenssettings.setLensVersion((String) obj[11]);
                        coreLenssettings.setInstanceType((String) obj[12]);

                        coreLenssettings.setCustomFilterSettings((String) obj[13]);
                        coreLenssettings.setStatus((boolean) obj[14]);
                        coreLenssettings.setCreatedOn((Date) obj[15]);

                        //added by sic
                        coreLenssettings.setDocServerResume((String) obj[16]);
                        coreLenssettings.setDocServerPostings((String) obj[17]);
                        coreLenssettings.setHypercubeServerResume((String) obj[18]);
                        coreLenssettings.setHypercubeServerPostings((String) obj[19]);

                        coreLensSettingsList.add(coreLenssettings);
                    } else {

                        coreLenssettings.setId((int) (obj[0] != null ? obj[0] : 0));
                        coreLenssettings.setHost((String) obj[1]);
                        coreLenssettings.setPort((int) (obj[2] != null ? obj[2] : 0));
                        coreLenssettings.setCharacterSet((String) obj[3]);
                        coreLenssettings.setTimeout((int) (obj[4] != null ? obj[4] : 0));
                        coreLenssettings.setLocale((String) obj[5]);
                        coreLenssettings.setLanguage((String) obj[6]);
                        coreLenssettings.setCountry((String) obj[7]);
                        // coreLenssettings.setHrxmlcontent((String) obj[8]);
                        coreLenssettings.setHrxmlversion((String) obj[8]);
                        coreLenssettings.setHrxmlfilename((String) (obj[9] != null ? obj[9] : new String()));
                        coreLenssettings.setLensVersion((String) obj[10]);
                        coreLenssettings.setInstanceType((String) obj[11]);

                        coreLenssettings.setCustomFilterSettings((String) obj[12]);
                        coreLenssettings.setStatus((boolean) obj[13]);
                        coreLenssettings.setCreatedOn((Date) obj[14]);

                        //added by sic
                        coreLenssettings.setDocServerResume((String) obj[15]);
                        coreLenssettings.setDocServerPostings((String) obj[16]);
                        coreLenssettings.setHypercubeServerResume((String) obj[17]);
                        coreLenssettings.setHypercubeServerPostings((String) obj[18]);

                        coreLensSettingsList.add(coreLenssettings);
                    }
                }
            }

//            if (results != null && results.size() > 0) {
//                coreLensSettingsList = (List<CoreLenssettings>) (List<?>) results;
//                Set<CoreLenssettings> coreLenssettingsSet = new HashSet<>();
//                coreLenssettingsSet.addAll(coreLensSettingsList);
//                coreLensSettingsList.clear();
//                coreLensSettingsList.addAll(coreLenssettingsSet);
//
//                for (CoreLenssettings coreLenssettings : coreLensSettingsList) {
//                    Hibernate.initialize(coreLenssettings.getCoreLenssettingsesForFailoverInstanceId());
//                }
//            }
            if (coreLensSettingsList.size() > 0) {
                for (CoreLenssettings coreLenssettings : coreLensSettingsList) {
                    for (CoreClientlenssettings coreClientlenssettings : coreLenssettings.getCoreClientlenssettingses()) {
                        Hibernate.initialize(coreClientlenssettings.getCoreCustomskillsettings());
                    }
                }
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                tx.rollback();
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreLensSettingsList;
    }
}
