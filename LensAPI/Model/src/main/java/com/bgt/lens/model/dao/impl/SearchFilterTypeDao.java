// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.dao.impl;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.SearchFilterTypeCriteria;
import com.bgt.lens.model.dao.ISearchFilterTypeDao;
import com.bgt.lens.model.entity.SearchFilterType;
import com.bgt.lens.model.entity.SearchVendorsettings;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import javax.persistence.criteria.Expression;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;


public class SearchFilterTypeDao implements ISearchFilterTypeDao {
     /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize Lens settings Dao
     */
    public SearchFilterTypeDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set Session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(SearchFilterType searchFilterType) {
       helperDao.save(sessionFactory, searchFilterType);
    }

    @Override
    public void update(SearchFilterType searchFilterType) {
       helperDao.update(sessionFactory, searchFilterType);
    }

    @Override
    public void delete(SearchFilterType searchFilterType) {
       helperDao.delete(sessionFactory, searchFilterType);
    }
    
    @Override
    public List<SearchFilterType> getFilterIDByName(SearchFilterTypeCriteria searchFilterTypeCriteria){
        
        List<SearchFilterType> searchFilterTypeList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<SearchFilterType> criteriaQuery = cb.createQuery(SearchFilterType.class);
            Root<SearchFilterType> searchFilterTypeRoot = criteriaQuery.from(SearchFilterType.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(searchFilterTypeCriteria.getFilterType())) {
            	conditions.add(searchFilterTypeRoot.get("filterType").in(searchFilterTypeCriteria.getFilterType().toArray()));
            }

            criteriaQuery.select(searchFilterTypeRoot.get("id")).distinct(true);
            criteriaQuery.orderBy(cb.asc(searchFilterTypeRoot.get("id")));
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<SearchFilterType> query = session.createQuery(criteriaQuery);
            List<SearchFilterType> results = query.getResultList();
            
            if (results != null && results.size() > 0) {
                searchFilterTypeList = results;

                Set<SearchFilterType> searchFilterTypeSet = new HashSet<>();
                searchFilterTypeSet.addAll(searchFilterTypeList);

                searchFilterTypeList.clear();
                searchFilterTypeList.addAll(searchFilterTypeSet);
                
            }
                       
            tx.commit();
            LOGGER.info("Transaction Commit");
        } 
        catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
       
        return searchFilterTypeList;
    }

}
