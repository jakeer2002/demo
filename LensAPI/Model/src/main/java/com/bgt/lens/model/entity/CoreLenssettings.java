// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * CoreLenssettings generated by hbm2java
 */
@Entity
@Table(name = "core_lenssettings")
public class CoreLenssettings implements java.io.Serializable {

    /**
     * Lens Settings Id
     */
    private Integer id;

    /**
     * Lens host
     */
    private String host;

    /**
     * Lens port
     */
    private int port;

    /**
     * Lens Encoding
     */
    private String characterSet;

    /**
     * Connection timeout
     */
    private int timeout;

    /**
     * Locale of the instance
     */
    private String locale;

    /**
     * Language
     */
    private String language;

    /**
     * Country
     */
    private String country;

    /**
     * HRXML Content
     */
    private String hrxmlcontent;

    /**
     * HRXML Version
     */
    private String hrxmlversion;

    /**
     * HRXML FileName
     */
    private String hrxmlfilename;

    /**
     * Lens version
     */
    private String lensVersion;

    /**
     * Instance type
     */
    private String instanceType;

    /**
     * Custom filter Settings
     */
    private String customFilterSettings;

    /**
     * Instance status
     */
    private boolean status;

    /**
     * Settings created date
     */
    private Date createdOn;
    
    private String docServerResume;
    private String docServerPostings;
    private String hypercubeServerResume;
    private String hypercubeServerPostings;

    /**
     * Client details
     */
//    private Set<CoreClient> coreClients = new HashSet<>(0);

    /**
     * Primary LENS settings
     */
    private Set<CoreLenssettings> coreLenssettingsesForLensSettingsId = new HashSet<>(0);

    /**
     * Search vendor settings
     */
    private Set<SearchVendorsettings> searchVendorsettingses = new HashSet<>(0);
   
    private Set<CoreLenssettings> coreLenssettingsesForFailoverInstanceId = new HashSet<>(0);

    private Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<CoreClientlenssettings>(0);

    private Set<CoreCustomskillsettings> coreCustomskillsettings = new HashSet<>(0);
    
    /**
     * Initialize Lens settings
     */
    public CoreLenssettings() {
    }

    /**
     * Initialize Lens settings
     *
     * @param host
     * @param port
     * @param characterSet
     * @param timeout
     * @param locale
     * @param instanceType
     * @param status
     * @param createdOn
     */
    public CoreLenssettings(String host, int port, String characterSet, int timeout, String locale, String instanceType, boolean status, Date createdOn) {
        this.host = host;
        this.port = port;
        this.characterSet = characterSet;
        this.timeout = timeout;
        this.locale = locale;
        this.instanceType = instanceType;
        this.status = status;
        this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Initialize Lens settings
     *
     * @param host
     * @param port
     * @param characterSet
     * @param timeout
     * @param locale
     * @param language
     * @param country
     * @param hrxmlversion
     * @param hrxmlfilename
     * @param hrxmlcontent
     * @param hrxmlversion
     * @param lensVersion
     * @param instanceType
     * @param customFilterSettings
     * @param status
     * @param createdOn
     * @param searchVendorsettingses
     */
    public CoreLenssettings(String host, int port, String characterSet, int timeout, String locale, String language, String country, String hrxmlversion, String hrxmlfilename, String hrxmlcontent, String lensVersion, String instanceType, String customFilterSettings, boolean status, String docServerResume, String docServerPostings, String hypercubeServerResume, String hypercubeServerPostings, Date createdOn,  Set<SearchVendorsettings> searchVendorsettingses) {

        this.host = host;
        this.port = port;
        this.characterSet = characterSet;
        this.timeout = timeout;
        this.locale = locale;
        this.language = language;
        this.country = country;
        this.hrxmlcontent = hrxmlcontent;
        this.hrxmlversion = hrxmlversion;
        this.hrxmlfilename = hrxmlfilename;
        this.lensVersion = lensVersion;
        this.instanceType = instanceType;
        this.customFilterSettings = customFilterSettings;
        this.status = status;
        this.docServerResume = docServerResume;
        this.docServerPostings = docServerPostings;
        this.hypercubeServerResume = hypercubeServerResume;
        this.hypercubeServerPostings = hypercubeServerPostings;
        this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
//        this.coreClients = coreClients;
    }

    /**
     * Get Lens settings Id
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    /**
     * Set Lens settings Id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get Lens host
     *
     * @return
     */
    @Column(name = "Host", nullable = true, length = 20)
    public String getHost() {
        return this.host;
    }

    /**
     * Set Lens host
     *
     * @param host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Get Lens port
     *
     * @return
     */
    @Column(name = "Port", nullable = false)
    public int getPort() {
        return this.port;
    }

    /**
     * Set Lens port
     *
     * @param port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Get Lens encoding
     *
     * @return
     */
    @Column(name = "CharacterSet", nullable = true, length = 10)
    public String getCharacterSet() {
        return this.characterSet;
    }

    /**
     * Set lens Encoding
     *
     * @param characterSet
     */
    public void setCharacterSet(String characterSet) {
        this.characterSet = characterSet;
    }

    /**
     * Get connection timeout interval
     *
     * @return
     */
    @Column(name = "Timeout", nullable = false)
    public int getTimeout() {
        return this.timeout;
    }

    /**
     * Set connection timeout interval
     *
     * @param timeout
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * Get locale of the instance
     *
     * @return
     */
    @Column(name = "Locale", nullable = true, length = 20)
    public String getLocale() {
        return this.locale;
    }

    /**
     * Set locale of the instance
     *
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get Language of the instance
     *
     * @return
     */
    @Column(name = "Language", length = 100)
    public String getLanguage() {
        return this.language;
    }

    /**
     * Set Language of the instance
     *
     * @param language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Get Country of the instance
     *
     * @return
     */
    @Column(name = "Country", length = 100)
    public String getCountry() {
        return this.country;
    }

    /**
     * Set Country of the instance
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Get HRXMLVersion of the instance
     *
     * @return
     */
    @Column(name = "HRXMLVersion")
    public String getHrxmlversion() {
        return hrxmlversion;
    }

    /**
     * Set HRXMLVersion of the instance
     *
     * @param hrxmlversion
     */
    public void setHrxmlversion(String hrxmlversion) {
        this.hrxmlversion = hrxmlversion;
    }

    /**
     * Get HRXMLFileName
     *
     * @return
     */
    @Column(name = "HRXMLFileName")
    public String getHrxmlfilename() {
        return hrxmlfilename;
    }

    /**
     * Set HRXMLVersion of the instance
     *
     * @param hrxmlfilename
     */
    public void setHrxmlfilename(String hrxmlfilename) {
        this.hrxmlfilename = hrxmlfilename;
    }

    /**
     * Get HRXMLContent of the instance
     *
     * @return
     */
    @Column(name = "HRXMLContent")
    public String getHrxmlcontent() {
        return this.hrxmlcontent;
    }

    /**
     * Set HRXMLContent of the instance
     *
     * @param hrxmlcontent
     */
    public void setHrxmlcontent(String hrxmlcontent) {
        this.hrxmlcontent = hrxmlcontent;
    }

    /**
     * Get Lens version
     *
     * @return
     */
    @Column(name = "LensVersion", length = 100)
    public String getLensVersion() {
        return this.lensVersion;
    }

    /**
     * Set Lens version
     *
     * @param lensVersion
     */
    public void setLensVersion(String lensVersion) {
        this.lensVersion = lensVersion;
    }

    /**
     * Get instance type
     *
     * @return
     */
    @Column(name = "InstanceType", nullable = true, length = 10)
    public String getInstanceType() {
        return this.instanceType;
    }

    /**
     * Set instance type
     *
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get custom filter settings
     * @return 
     */
    @Column(name = "CustomFilterSettings")
    public String getCustomFilterSettings() {
        return customFilterSettings;
    }

    /**
     * Set custom filter settings
     * @param customFilterSettings 
     */
    public void setCustomFilterSettings(String customFilterSettings) {
        this.customFilterSettings = customFilterSettings;
    }

    /**
     * Get the instance status
     *
     * @return
     */
    @Column(name = "Status", nullable = false)
    public boolean isStatus() {
        return this.status;
    }

    /**
     * Set instance status
     *
     * @param status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Get instance created date
     *
     * @return
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedOn", nullable = false, length = 19)
    public Date getCreatedOn() {
        return createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Set instance created date
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
    }

    @Column(name = "DocServerResume")
    public String getDocServerResume() {
        return this.docServerResume;
    }

    public void setDocServerResume(String docServerResume) {
        this.docServerResume = docServerResume;
    }

    @Column(name = "DocServerPostings")
    public String getDocServerPostings() {
        return this.docServerPostings;
    }

    public void setDocServerPostings(String docServerPostings) {
        this.docServerPostings = docServerPostings;
    }

    @Column(name = "HypercubeServerResume")
    public String getHypercubeServerResume() {
        return this.hypercubeServerResume;
    }

    public void setHypercubeServerResume(String hypercubeServerResume) {
        this.hypercubeServerResume = hypercubeServerResume;
    }

    @Column(name = "HypercubeServerPostings")
    public String getHypercubeServerPostings() {
        return this.hypercubeServerPostings;
    }

    public void setHypercubeServerPostings(String hypercubeServerPostings) {
        this.hypercubeServerPostings = hypercubeServerPostings;
    }

    /**
     * Get clients
     *
     * @return
     */
//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "core_clientlenssettings", joinColumns = {
//        @JoinColumn(name = "LensSettingId", nullable = false, updatable = false)}, inverseJoinColumns = {
//        @JoinColumn(name = "ClientId", nullable = false, updatable = false)})
//    public Set<CoreClient> getCoreClients() {
//        return this.coreClients;
//    }
//
//    /**
//     * Set clients
//     *
//     * @param coreClients
//     */
//    public void setCoreClients(Set<CoreClient> coreClients) {
//        this.coreClients = coreClients;
//    }

    /**
     * Get Vendor details
     *
     * @return
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "coreLenssettings")
    @Cascade(CascadeType.REMOVE)
    @BatchSize(size = 20)
    public Set<SearchVendorsettings> getSearchVendorsettingses() {
        return this.searchVendorsettingses;
    }

    /**
     * Set vendor details
     *
     * @param searchVendorsettingses
     */
    public void setSearchVendorsettingses(Set<SearchVendorsettings> searchVendorsettingses) {
        this.searchVendorsettingses = searchVendorsettingses;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "core_failoverlenssettings", joinColumns = {
        @JoinColumn(name = "FailoverInstanceId", nullable = false, updatable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "LensSettingsId", nullable = false, updatable = false)})
    public Set<CoreLenssettings> getCoreLenssettingsesForLensSettingsId() {
        return this.coreLenssettingsesForLensSettingsId;
    }

    public void setCoreLenssettingsesForLensSettingsId(Set<CoreLenssettings> coreLenssettingsesForLensSettingsId) {
        this.coreLenssettingsesForLensSettingsId = coreLenssettingsesForLensSettingsId;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "core_failoverlenssettings", joinColumns = {
        @JoinColumn(name = "LensSettingsId", nullable = false, updatable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "FailoverInstanceId", nullable = false, updatable = false)})
    @Cascade(CascadeType.REMOVE)
    public Set<CoreLenssettings> getCoreLenssettingsesForFailoverInstanceId() {
        return this.coreLenssettingsesForFailoverInstanceId;
    }

    public void setCoreLenssettingsesForFailoverInstanceId(Set<CoreLenssettings> coreLenssettingsesForFailoverInstanceId) {
        this.coreLenssettingsesForFailoverInstanceId = coreLenssettingsesForFailoverInstanceId;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "coreLenssettings", orphanRemoval = true, cascade = javax.persistence.CascadeType.ALL)
    @BatchSize(size = 20)
    public Set<CoreClientlenssettings> getCoreClientlenssettingses() {
        return this.coreClientlenssettingses;
    }

    public void setCoreClientlenssettingses(Set<CoreClientlenssettings> coreClientlenssettingses) {
        this.coreClientlenssettingses = coreClientlenssettingses;
    }
    
     /**
     * Get Custom Skill Settings
     *
     * @return
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "coreLenssettings")             
    @BatchSize(size = 20)        
    @Cascade(CascadeType.ALL)
    public Set<CoreCustomskillsettings> getCoreCustomskillsettings() {
        return this.coreCustomskillsettings;
    }

    /**
     * Set Custom Skill Settings
     *
     * @param coreCustomskillsettings
     */
    public void setCoreCustomskillsettings(Set<CoreCustomskillsettings> coreCustomskillsettings) {
        this.coreCustomskillsettings = coreCustomskillsettings;
    }
}
