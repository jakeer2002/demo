// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.authentication;

/**
 *
 * @author prashanth
 */
public class BasicAuthHeader {

    /**
     * Basic Auth Consumer key
     */
    protected String consumerKey;

    /**
     * Basic Auth Consumer Secret
     */
    protected String consumerSecret;
    /**
     * Basic Auth Access Token
     */
    protected String accessToken;
    /**
     * Basic Auth Access Token Secret
     */
    protected String accessTokenSecret;

    /**
     * Get consumerKey
     *
     * @return
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumerKey
     *
     * @param consumerKey
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get consumerSecret
     *
     * @return
     */
    public String getConsumerSecret() {
        return consumerSecret;
    }

    /**
     * Set consumerSecret
     *
     * @param consumerSecret
     */
    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    /**
     * Get accessToken
     *
     * @return
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * Set accessToken
     *
     * @param accessToken
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * Get accessTokenSecret
     *
     * @return
     */
    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    /**
     * Set accessTokenSecret
     *
     * @param accessTokenSecret
     */
    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }

}
