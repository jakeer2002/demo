// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * CoreUsagelogdata
 */
@Entity
@Table(name = "core_usagelogdata", uniqueConstraints = @UniqueConstraint(columnNames = "Id")
)
public class CoreUsagelogdata implements java.io.Serializable {

    /**
     * Usage log Id
     */
    private Integer usageLogId;
    
    /**
     * Usage log
     */
    private CoreUsagelog coreUsagelog;
    
    /**
     * Usage log data id
     */
    private Integer id;
    
    /**
     * Request content
     */
    private String requestContent;
    
    /**
     * Response content
     */
    private String responseContent;
    
    /**
     * Headers
     */
    private String headers;

    public CoreUsagelogdata() {
    }

    public CoreUsagelogdata(CoreUsagelog coreUsagelog, int id) {
        this.coreUsagelog = coreUsagelog;
        this.id = id;
    }

    public CoreUsagelogdata(CoreUsagelog coreUsagelog, int id, String requestContent, String responseContent, String headers) {
        this.coreUsagelog = coreUsagelog;
        this.id = id;
        this.requestContent = requestContent;
        this.responseContent = responseContent;
        this.headers = headers;
    }

    /**
     * Get usage log Id
     * @return 
     */
    public Integer getUsageLogId() {
        return this.usageLogId;
    }

    /**
     * Set usage log Id
     * @param usageLogId 
     */
    public void setUsageLogId(Integer usageLogId) {
        this.usageLogId = usageLogId;
    }

    /**
     * Get usage log
     * @return 
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UsageLogId", nullable = false, insertable = false, updatable = false)
    public CoreUsagelog getCoreUsagelog() {
        return this.coreUsagelog;
    }

    /**
     * Set usage log
     * @param coreUsagelog 
     */
    public void setCoreUsagelog(CoreUsagelog coreUsagelog) {
        this.coreUsagelog = coreUsagelog;
    }

    /**
     * Get Id
     * @return 
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false, insertable = false, updatable = false)
    public Integer getId() {
        return this.id;
    }

    /**
     * Set Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get request content
     * @return 
     */
    @Column(name = "RequestContent")
    public String getRequestContent() {
        return this.requestContent;
    }

    /**
     * Set request content
     * @param requestContent 
     */
    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    /**
     * Get response content
     * @return 
     */
    @Column(name = "ResponseContent")
    public String getResponseContent() {
        return this.responseContent;
    }

    /**
     * Set response content
     * @param responseContent 
     */
    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

    /**
     * Get headers
     * @return 
     */
    @Column(name = "Headers")
    public String getHeaders() {
        return this.headers;
    }

    /**
     * Set headers
     * @param headers 
     */
    public void setHeaders(String headers) {
        this.headers = headers;
    }

}
