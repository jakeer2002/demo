// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * *
 * Get License list
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class LicenseList {

    /**
     * *
     * Get license list
     */
    @XmlElement(name = "License", required = false)
    public List<License> license;

    /**
     * *
     * Get license list
     *
     * @return
     */
    public List<License> getLicense() {
        return license;
    }

    /**
     * *
     * Set license list
     *
     * @param license
     */
    public void setLicense(List<License> license) {
        this.license = license;
    }
}
