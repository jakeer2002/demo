// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Search Filter Log
 */
@Entity
@Table(name = "Search_FilterLog")
public class SearchFilterLog implements java.io.Serializable {

    /**
     * Id
     */
    private int id;

     /**
     * Search Command Dump
     */
    private SearchCommanddump searchCommanddump;   
    
    /**
     * Search Filter Type
     */
    
    private SearchFilterType searchFilterType;    

    public SearchFilterLog() {
    }

    public SearchFilterLog(int id, SearchCommanddump searchCommanddump, SearchFilterType searchFilterType) {
        this.id = id;
        this.searchCommanddump = searchCommanddump;
        this.searchFilterType = searchFilterType;
    }

    public SearchFilterLog(SearchCommanddump searchCommanddump, SearchFilterType searchFilterType) {
        this.searchCommanddump = searchCommanddump;
        this.searchFilterType = searchFilterType;;
    }

    /**
     * Get Id
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    /**
     * Set Id
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get Search Command Dump
     *
     * @return
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CommandDumpId", nullable = false)
    public SearchCommanddump getSearchCommanddump() {
        return this.searchCommanddump;
    }

    /**
     * Set Search Command Dump
     *
     * @param searchCommanddump
     */
    public void setSearchCommanddump(SearchCommanddump searchCommanddump) {
        this.searchCommanddump = searchCommanddump;
    }

    /**
     * Get Search Filter Type
     *
     * @return
     */
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="FilterId", nullable=false)
    public SearchFilterType getSearchFilterType() {
        return this.searchFilterType;
    }
    
    /**
     * Set Search Filter Type
     *
     * @param searchFilterType
     */
    public void setSearchFilterType(SearchFilterType searchFilterType) {
        this.searchFilterType = searchFilterType;
    }

}
