// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.AggregatedLensDocumentCriteria;
import com.bgt.lens.model.entity.AggregatedCoreLensDocument;
import com.bgt.lens.model.entity.CoreLensdocument;
import java.util.List;

/**
 *
 * Lens document Dao
 */
public interface ICoreLensdocumentDao {

    /**
     * Create Lens Document
     *
     * @param coreLensDocument
     */
    public void save(CoreLensdocument coreLensDocument);

    /**
     * Update Lens document
     *
     * @param coreLensDocument
     */
    public void update(CoreLensdocument coreLensDocument);

    /**
     * Delete Lens Document
     *
     * @param coreLensDocument
     */
    public void delete(CoreLensdocument coreLensDocument);

    /**
     * Get Lens document by Id
     *
     * @param lensDocumentId
     * @return
     */
    public CoreLensdocument getLensDocumentById(int lensDocumentId);
    
    /**
     * Update document dump count
     * @param aggLensDocumentCriteria
     * @return 
     */
    public void updateDocumentDumpCount(AggregatedCoreLensDocument aggLensDocument);
}
