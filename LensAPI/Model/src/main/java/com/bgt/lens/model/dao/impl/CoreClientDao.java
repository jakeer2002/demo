// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.dao.ICoreClientDao;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientapiId;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreClientlenssettingsId;

/**
 *
 * Client table data management
 */
public class CoreClientDao implements ICoreClientDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize Client Dao
     */
    public CoreClientDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create client
     *
     * @param coreClient
     */
    @Override
    public void save(CoreClient coreClient) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            session.save(coreClient);
            Set<CoreClientapi> coreClientApiSet = coreClient.getCoreClientapis();
            Set<CoreClientlenssettings> clientlenssettingsSet = coreClient.getCoreClientlenssettingses();
            for (CoreClientapi coreClientApi : coreClientApiSet) {
                coreClientApi.setCoreClient(coreClient);
                CoreClientapiId clientApiId = new CoreClientapiId();
                clientApiId.setApiId(coreClientApi.getCoreApi().getId());
                clientApiId.setClientId(coreClient.getId());
                coreClientApi.setId(clientApiId);

                session.save(coreClientApi);
            }

            for (CoreClientlenssettings clientlenssettings : clientlenssettingsSet) {                
                clientlenssettings.setCoreClient(coreClient);                
                CoreClientlenssettingsId clientlenssettingsId = new CoreClientlenssettingsId();
                clientlenssettingsId.setLensSettingId(clientlenssettings.getCoreLenssettings().getId());
                clientlenssettingsId.setClientId(coreClient.getId());   
                        
                clientlenssettings.setId(clientlenssettingsId);                                
                session.save(clientlenssettings);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                tx.rollback();
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Update client
     *
     * @param coreClient
     * @param updatedCoreClient
     */
    @Override
    public void update(CoreClient coreClient, CoreClient updatedCoreClient) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            session.update(updatedCoreClient);

            Set<CoreClientapi> coreClientApiSet = updatedCoreClient.getCoreClientapis();
            Set<CoreClientlenssettings> clientlenssettingsSet = updatedCoreClient.getCoreClientlenssettingses();
            for (CoreClientapi coreClientApi : coreClientApiSet) {
                coreClientApi.setCoreClient(coreClient);
                CoreClientapiId clientApiId = new CoreClientapiId();
                clientApiId.setApiId(coreClientApi.getCoreApi().getId());
                clientApiId.setClientId(coreClient.getId());
                coreClientApi.setId(clientApiId);
                session.saveOrUpdate(coreClientApi);
            }
  
            for (CoreClientlenssettings clientlenssettings : clientlenssettingsSet) {
                clientlenssettings.setCoreClient(coreClient);
                CoreClientlenssettingsId clientlenssettingsId = new CoreClientlenssettingsId();
                clientlenssettingsId.setLensSettingId(clientlenssettings.getCoreLenssettings().getId());
                clientlenssettingsId.setClientId(coreClient.getId());
                clientlenssettings.setId(clientlenssettingsId);

                session.saveOrUpdate(clientlenssettings);
            }
            session.flush();
            // Find unused settings and delete it
            // TODO : Optimize this
            for(CoreClientlenssettings clientlenssettings : coreClient.getCoreClientlenssettingses()) {
                // CoreLenssettings cl = clientlenssettings.getCoreLenssettings();
                boolean isSettingsAvailable = false;
                for(CoreClientlenssettings updateClientlenssettings : clientlenssettingsSet) {
                    if((clientlenssettings.getCoreLenssettings().getId())
                            .equals(updateClientlenssettings.getCoreLenssettings().getId())) {
                      isSettingsAvailable = true;
                      break;
                    }
                } 
                if(!isSettingsAvailable) {
                    session.delete(clientlenssettings);
                }
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Update client
     *
     * @param coreClient
     * @param updatedCoreClient
     */
    @Override
    public void update(Set<BigInteger> id, Map<BigInteger, CoreClient> _coreClient, Map<BigInteger, CoreClient> _updatedCoreClient) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            for (BigInteger cliendId : id) {
                CoreClient updatedCoreClient = new CoreClient();
                 CoreClient coreClient = new CoreClient();
                 updatedCoreClient = (CoreClient) _updatedCoreClient.get(cliendId);
                 coreClient = (CoreClient) _coreClient.get(cliendId);
                 LOGGER.info("Update consumer data to database. Key - " + updatedCoreClient.getClientKey());
                session.update(updatedCoreClient);
                Set<CoreClientapi> coreClientApiSet = updatedCoreClient.getCoreClientapis();
                Set<CoreClientlenssettings> clientlenssettingsSet = updatedCoreClient.getCoreClientlenssettingses();
                for (CoreClientapi coreClientApi : coreClientApiSet) {
                    coreClientApi.setCoreClient(coreClient);
                    CoreClientapiId clientApiId = new CoreClientapiId();
                    clientApiId.setApiId(coreClientApi.getCoreApi().getId());
                    clientApiId.setClientId(coreClient.getId());
                    coreClientApi.setId(clientApiId);
                    session.saveOrUpdate(coreClientApi);
                }
                for (CoreClientlenssettings clientlenssettings : clientlenssettingsSet) {
                    clientlenssettings.setCoreClient(coreClient);
                    CoreClientlenssettingsId clientlenssettingsId = new CoreClientlenssettingsId();
                    clientlenssettingsId.setLensSettingId(clientlenssettings.getCoreLenssettings().getId());
                    clientlenssettingsId.setClientId(coreClient.getId());
                    clientlenssettings.setId(clientlenssettingsId);

                    session.saveOrUpdate(clientlenssettings);
                }
                session.flush();
                // Find unused settings and delete it
                // TODO : Optimize this
                for (CoreClientlenssettings clientlenssettings : coreClient.getCoreClientlenssettingses()) {
                    // CoreLenssettings cl = clientlenssettings.getCoreLenssettings();
                    boolean isSettingsAvailable = false;
                    for (CoreClientlenssettings updateClientlenssettings : clientlenssettingsSet) {
                        if ((clientlenssettings.getCoreLenssettings().getId())
                                .equals(updateClientlenssettings.getCoreLenssettings().getId())) {
                            isSettingsAvailable = true;
                            break;
                        }
                    }
                    if (!isSettingsAvailable) {
                        session.delete(clientlenssettings);
                    }
                }
            }
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Delete client
     *
     * @param coreClient
     */
    @Override
    public void delete(CoreClient coreClient) {
        helperDao.delete(sessionFactory, coreClient);
    }

    /**
     * Get client by Id
     *
     * @param clientId
     * @return
     */
    @Override
    public CoreClient getClientById(int clientId) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            CoreClient coreClient = (CoreClient) session.get(CoreClient.class, clientId);

            if (coreClient != null) {
                Hibernate.initialize(coreClient.getCoreResourceses());
                Hibernate.initialize(coreClient.getCoreClientlenssettingses());
                Hibernate.initialize(coreClient.getSearchVendorsettingses());
                Hibernate.initialize(coreClient.getCoreClientapis());
            }
            tx.commit();
            LOGGER.info("Transaction Commit");
            return coreClient;
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Get client by criteria
     *
     * @param clientCriteria
     * @return
     */
    @Override
    public List<CoreClient> getClientByCriteria(ClientCriteria clientCriteria) {
        List<CoreClient> coreClientList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreClient> criteria = cb.createQuery(CoreClient.class);
            Root<CoreClient> coreClientRoot = criteria.from(CoreClient.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (!_helper.isNullOrEmpty(clientCriteria.getClientKey())) {
            	conditions.add(cb.equal(coreClientRoot.get("clientKey"), clientCriteria.getClientKey()));
            }

            if (!_helper.isNullOrEmpty(clientCriteria.getName())) {
            	conditions.add(cb.equal(coreClientRoot.get("name"), clientCriteria.getName()));
            }

            if (_helper.isNotNull(clientCriteria.isDumpStatus())) {
            	conditions.add(cb.equal(coreClientRoot.get("dumpStatus"), clientCriteria.isDumpStatus()));
            }

            // to get users who are only active
            conditions.add(cb.isNull(coreClientRoot.get("deletedOn")));

            criteria.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreClient> query = session.createQuery(criteria);
            List<CoreClient> results = query.getResultList();

            if (results != null && results.size() > 0) {
                coreClientList = results;

                Set<CoreClient> coreClientSet = new HashSet<>();
                coreClientSet.addAll(coreClientList);

                coreClientList.clear();
                coreClientList.addAll(coreClientSet);

                // This will force SQL to execute the query that will join with other associations
                for (CoreClient coreClient : coreClientList) {
                    //Hibernate.initialize(coreClient.getCoreResourceses());
                    Hibernate.initialize(coreClient.getCoreClientlenssettingses());
                    Hibernate.initialize(coreClient.getCoreClientapis());
                }
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreClientList;
    }

}
