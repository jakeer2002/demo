// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreClientapi;
import java.util.List;

/**
 *
 * Client API Dao
 */
public interface ICoreClientapiDao {

    /**
     * Create client API
     *
     * @param coreClientApi
     */
    public void save(CoreClientapi coreClientApi);

    /**
     * Update client API
     *
     * @param coreClientApi
     */
    public void update(CoreClientapi coreClientApi);

    /**
     * Delete client API
     *
     * @param coreClientApi
     */
    public void delete(CoreClientapi coreClientApi);

    /**
     * Get client API by Id
     *
     * @param clientApiId
     * @return
     */
    public CoreClientapi getClientApiById(int clientApiId);

    /**
     * Get client API by criteria
     *
     * @param clientapiCriteria
     * @return
     */
    public List<CoreClientapi> getClientApiByCriteria(ClientapiCriteria clientapiCriteria);

}
