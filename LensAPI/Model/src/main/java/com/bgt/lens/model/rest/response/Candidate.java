// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Candidate
 */
@XmlRootElement(name = "Candidate")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class Candidate {
    /**
     * Document Id
     */
    @XmlElement(name = "ResumeId", required = false)
    protected String resumeId;
    
    /**
     * Candidate name
     */
    @XmlElement(name = "Name", required = false)
    protected String name;
    
    /**
     * Score
     */
    @XmlElement(name = "Score", required = false)
    protected Integer score;
    
    /**
     * Years of experience
     */
    @XmlElement(name = "YrsExp", required = false)
    protected Integer yrsExp;
    
    /**
     * List of employment history
     */
    @XmlElement(name = "EmploymentHistory", required = false)
    protected List<EmploymentHistory> employmentHistory;
    
    /**
     * List of education history
     */
    @XmlElement(name = "EducationHistory", required = false)
    protected List<EducationHistory> educationHistory;
    
    /**
     * List of location history
     */
    @XmlElement(name = "LocationHistory", required = false)
    protected List<LocationHistory> locationHistory;

    /**
     * List of location history
     */
    @XmlElement(name = "CustomFilters", required = false)
    protected List<Filter> customFilters;
    
    /**
     * List of custom xpath data
     */
    @XmlElement(name = "CustomXpathData", required = false)
    protected List<CustomXpathData> customXpathData;
    
    /**
     * Match explanation
     */
    @XmlElement(name = "MatchExplanation", required = false)
    protected MatchExplanation matchExplanation;

    /**
     * Get document id
     * @return 
     */
    public String getResumeId() {
        return resumeId;
    }

    /**
     * Set document id
     * @param resumeId 
     */
    public void setResumeId(String resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * Get name
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * Set name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get score
     * @return 
     */
    public Integer getScore() {
        return score;
    }

    /**
     * Set score
     * @param score 
     */
    public void setScore(Integer score) {
        this.score = score;
    }
    
    /**
     * Get years of experience
     * @return 
     */
    public Integer getYrsExp() {
        return yrsExp;
    }

    /**
     * Set years of experience
     * @param yrsExp 
     */
    public void setYrsExp(Integer yrsExp) {
        this.yrsExp = yrsExp;
    }

    /**
     * Get employment histories
     * @return 
     */
    public List<EmploymentHistory> getEmploymentHistory() {
        return employmentHistory;
    }

    /**
     * Set employment histories
     * @param employmentHistory 
     */
    public void setEmploymentHistory(List<EmploymentHistory> employmentHistory) {
        this.employmentHistory = employmentHistory;
    }

    /**
     * Get education histories
     * @return 
     */
    public List<EducationHistory> getEducationHistory() {
        return educationHistory;
    }

    /**
     * Set education histories
     * @param educationHistory 
     */
    public void setEducationHistory(List<EducationHistory> educationHistory) {
        this.educationHistory = educationHistory;
    }

    /**
     * Get location histories
     * @return 
     */
    public List<LocationHistory> getLocationHistory() {
        return locationHistory;
    }

    /**
     * Set location histories
     * @param locationHistory 
     */
    public void setLocationHistory(List<LocationHistory> locationHistory) {
        this.locationHistory = locationHistory;
    }
    
    /**
     * Get custom filters
     * @return 
     */
    public List<Filter> getCustomFilters() {
        return customFilters;
    }

    /**
     * Set custom filters
     * @param customFilters 
     */
    public void setCustomFilters(List<Filter> customFilters) {
        this.customFilters = customFilters;
    }
    
    /**
     * Get list of custom xpath data
     * @return 
     */
    public List<CustomXpathData> getCustomXpathData() {
        return customXpathData;
    }

    /**
     * Set list of custom xpath data
     * @param customXpathData 
     */
    public void setCustomXpathData(List<CustomXpathData> customXpathData) {
        this.customXpathData = customXpathData;
    }
    
    /**
     * Get match explanation
     * @return 
     */
    public MatchExplanation getMatchExplanation() {
        return matchExplanation;
    }

    /**
     * Set match explanation
     * @param matchExplanation 
     */
    public void setMatchExplanation(MatchExplanation matchExplanation) {
        this.matchExplanation = matchExplanation;
    }
}
