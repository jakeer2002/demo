// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Custom Xpath Data
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class CustomXpathData {
    /**
     * Custom xpath
     */
    @XmlElement(name = "xpath", required = false)
    protected String xpath;
    
    /**
     * Custom xpath data
     */
    @XmlElement(name = "xpathData", required = false)
    protected Object xpathData;

    /**
     * Get custom xpath
     * @return 
     */
    public String getXpath() {
        return xpath;
    }

    /**
     * Set custom xpath
     * @param xpath 
     */
    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    /**
     * Get xpath data
     * @return 
     */
    public Object getXpathData() {
        return xpathData;
    }

    /**
     * Set xpath data
     * @param xpathData 
     */
    public void setXpathData(Object xpathData) {
        this.xpathData = xpathData;
    }
}
