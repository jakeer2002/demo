// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.UsageLogCriteria;
import com.bgt.lens.model.entity.CoreUsagelog;
import java.util.List;

/**
 *
 * Usage log Dao interface
 */
public interface ICoreUsagelogDao {

    /**
     * Create usage log
     *
     * @param coreUsageLog
     */
    public void save(CoreUsagelog coreUsageLog);

    /**
     * Update usage log
     *
     * @param coreUsageLog
     */
    public void update(CoreUsagelog coreUsageLog);

    /**
     * Delete usage log
     *
     * @param coreUsageLog
     */
    public void delete(CoreUsagelog coreUsageLog);

    /**
     * Get usage log by Id
     *
     * @param usageLogId
     * @return
     */
    public CoreUsagelog getUsageLogById(String usageLogId);

    /**
     * Get usage log by id and client
     *
     * @param usageLogId
     * @param ClientId
     * @return
     */
    public CoreUsagelog getUsageLogById(String usageLogId, int ClientId);

    /**
     * Get usage log by criteria
     *
     * @param usageLogCriteria
     * @return
     */
    public List<CoreUsagelog> getUsageLogByCriteria(UsageLogCriteria usageLogCriteria);
}
