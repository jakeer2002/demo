// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.dao.ISearchFilterLogDao;
import com.bgt.lens.model.entity.SearchFilterLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;


public class SearchFilterLogDao implements ISearchFilterLogDao {
    
    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize Lens settings Dao
     */
    public SearchFilterLogDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set Session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void save(SearchFilterLog searchFilterType) {
       helperDao.save(sessionFactory, searchFilterType);
    }

    @Override
    public void update(SearchFilterLog searchFilterType) {
        helperDao.update(sessionFactory, searchFilterType);
    }

    @Override
    public void delete(SearchFilterLog searchFilterType) {
        helperDao.delete(sessionFactory, searchFilterType);
    }
    
}
