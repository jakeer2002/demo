// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Custom Filter Settings
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class CustomFilters {
    
    /**
     * List of Resume Filters
     */
    @XmlElement(name = "ResumeFilters", required = false)
    protected List<Filter> resumeFilters;
    
    
    /**
     * List of posting filters
     */
    @XmlElement(name = "PostingFilters", required = false)
    protected List<Filter> postingFilters;

    /**
     * Get Resume filters
     * @return 
     */
    public List<Filter> getResumeFilters() {
        return resumeFilters;
    }

    /**
     * Set Resume filters
     * @param resumeFilters 
     */
    public void setResumeFilters(List<Filter> resumeFilters) {
        this.resumeFilters = resumeFilters;
    }

    /**
     * Get posting filters
     * @return 
     */
    public List<Filter> getPostingFilters() {
        return postingFilters;
    }

    /**
     * Set posting filters
     * @param postingFilters 
     */
    public void setPostingFilters(List<Filter> postingFilters) {
        this.postingFilters = postingFilters;
    }
}
