// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * *
 * Error log
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class ErrorLog {

    /**
     * *
     * Error log Id
     */
    @XmlElement(name = "Id", required = false)
    protected Integer id;

    /**
     * *
     * Request Id
     */
    @XmlElement(name = "RequestId", required = false)
    protected String requestId;

    /**
     * *
     * Client id
     */
    @XmlElement(name = "ClientId", required = false)
    protected Integer clientId;

    /**
     * *
     * API Id
     */
    @XmlElement(name = "ApiId", required = false)
    protected Integer apiId;
    
    /**
     * *
     * Consumer key
     */
    @XmlElement(name = "ConsumerKey", required = false)
    protected String consumerKey;

    /**
     * *
     * API
     */
    @XmlElement(name = "Api", required = false)
    protected String api;

    /**
     * *
     * Status code
     */
    @XmlElement(name = "StatusCode", required = false)
    protected Integer statusCode;

    /**
     * *
     * User message
     */
    @XmlElement(name = "UserMessage", required = false)
    protected String userMessage;

    /**
     * *
     * Exception message
     */
    @XmlElement(name = "Exception", required = false)
    protected String exception;

    /**
     * *
     * Trace log
     */
    @XmlElement(name = "TraceLog", required = false)
    protected String traceLog;

    /**
     * *
     * Error log creation date
     */
    @XmlElement(name = "CreatedOn", required = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    protected Date createdOn;

    /**
     * *
     * Get error log id
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * *
     * Set error log Id
     *
     * @param Id
     */
    public void setId(Integer Id) {
        this.id = Id;
    }

    /**
     * *
     * get request Id
     *
     * @return
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * *
     * Set request id
     *
     * @param RequestId
     */
    public void setRequestId(String RequestId) {
        this.requestId = RequestId;
    }

    /**
     * *
     * Get client Id
     *
     * @return
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * *
     * Set client Id
     *
     * @param ClientId
     */
    public void setClientId(Integer ClientId) {
        this.clientId = ClientId;
    }

    /**
     * *
     * Get API Id
     *
     * @return
     */
    public Integer getApiId() {
        return apiId;
    }

    /**
     * *
     * Set API Id
     *
     * @param ApiId
     */
    public void setApiId(Integer ApiId) {
        this.apiId = ApiId;
    }
    
    /**
     * Get consumer key
     * @return 
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get api
     * @return 
     */
    public String getApi() {
        return api;
    }

    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * *
     * Get status code
     *
     * @return
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * *
     * Set status code
     *
     * @param StatusCode
     */
    public void setStatusCode(Integer StatusCode) {
        this.statusCode = StatusCode;
    }

    /**
     * *
     * Get user message
     *
     * @return
     */
    public String getUserMessage() {
        return userMessage;
    }

    /**
     * *
     * Set user message
     *
     * @param UserMessage
     */
    public void setUserMessage(String UserMessage) {
        this.userMessage = UserMessage;
    }

    /**
     * *
     * Get exception
     *
     * @return
     */
    public String getException() {
        return exception;
    }

    /**
     * *
     * Set exception
     *
     * @param Exception
     */
    public void setException(String Exception) {
        this.exception = Exception;
    }

    /**
     * *
     * Get trace log
     *
     * @return
     */
    public String getTraceLog() {
        return traceLog;
    }

    /**
     * *
     * Set trace log
     *
     * @param TraceLog
     */
    public void setTraceLog(String TraceLog) {
        this.traceLog = TraceLog;
    }

    /**
     * *
     * Get created date
     *
     * @return
     */
    public Date getCreatedOn() {
        return createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * *
     * Set created dated
     *
     * @param CreatedOn
     */
    public void setCreatedOn(Date CreatedOn) {
        this.createdOn = CreatedOn == null ? null : new Date(CreatedOn.getTime());
    }

}
