// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.config;

/**
 *
 * API configuration
 */
public class ApiConfiguration {

    /**
     * Default transaction count
     */
    protected int defaultTransactionCount;

    /**
     * Default expiry date time interval
     */
    protected int defaultExpiryDateInterval;

    /**
     * Default maximum allowed transaction count
     */
    protected int maxTransactionCount;

    /**
     * Default Lens session timeout
     */
    protected int defaultTimeout;

    /**
     * Default instance type
     */
    protected String defaultInstanceType;

    /**
     * Default encoding
     */
    protected String defaultEncoding;

    /**
     * Default doc type
     */
    protected String defaultDocType;

    /**
     * Default extension
     */
    protected String defaultExtension;

    /**
     * Default Result Limit
     */
    protected int defaultResultLimit;

    /**
     * *
     * Default usage log response limit
     */
    protected int defaultUsageLogResponseDataSize;
    
    /**
     * Default maximum search count
     */
    protected int defaultMaxDocumentCountPerVendor;
    
    /**
     * Default max document count per user
     */
    protected int defaultMaxSearchCount;
    
    /**
     * Default search document count
     */
    protected int defaultSearchCount;
    
    /**
     * Default search document count for Gel All Document Ids
     */
    protected int defaultSearchCountGetAllDocIds;
    
    /**
     * Default minimum LENS score
     */
    protected int defaultSearchMinScore;
    
    /**
     * Default Vendor Count
     */
    protected int defaultVendorCount;
    
    /**
     * Default facet distribution count
     */
    protected int defaultDistributionCount;

    /**
     * *
     * Get default usage log response size
     *
     * @return
     */
    public int getDefaultUsageLogResponseDataSize() {
        return defaultUsageLogResponseDataSize;
    }

    /**
     * *
     * Set default usage log response size
     *
     * @param defaultUsageLogResponseDataSize
     */
    public void setDefaultUsageLogResponseDataSize(int defaultUsageLogResponseDataSize) {
        this.defaultUsageLogResponseDataSize = defaultUsageLogResponseDataSize;
    }

    /**
     * Get default result limit
     *
     * @return
     */
    public int getDefaultResultLimit() {
        return defaultResultLimit;
    }

    /**
     * Set default result limit
     *
     * @param defaultResultLimit
     */
    public void setDefaultResultLimit(int defaultResultLimit) {
        this.defaultResultLimit = defaultResultLimit;
    }

    /**
     * Get default extension
     *
     * @return
     */
    public String getDefaultExtension() {
        return defaultExtension;
    }

    /**
     * Set default extension
     *
     * @param defaultExtension
     */
    public void setDefaultExtension(String defaultExtension) {
        this.defaultExtension = defaultExtension;
    }

    /**
     * Default locale
     */
    protected String defaultLocale;

    /**
     * Get default locale
     *
     * @return
     */
    public String getDefaultLocale() {
        return defaultLocale;
    }

    /**
     * Set default locale
     *
     * @param defaultLocale
     */
    public void setDefaultLocale(String defaultLocale) {
        this.defaultLocale = defaultLocale;
    }

    /**
     * Default allowable Request Delay Seconds
     */
    protected int allowableRequestDelaySeconds;

    /**
     * Set default transaction count
     *
     * @param defaultTransactionCount
     */
    public void setDefaultTransactionCount(int defaultTransactionCount) {
        this.defaultTransactionCount = defaultTransactionCount;
    }

    /**
     * Get default transaction count
     *
     * @return
     */
    public int getDefaultTransactionCount() {
        return this.defaultTransactionCount;
    }

    /**
     * Set default expiry date interval
     *
     * @param defaultExpiryDateInterval
     */
    public void setDefaultExpiryDateInterval(int defaultExpiryDateInterval) {
        this.defaultExpiryDateInterval = defaultExpiryDateInterval;
    }

    /**
     * Get default expiry date interval
     *
     * @return
     */
    public int getDefaultExpiryDateInterval() {
        return this.defaultExpiryDateInterval;
    }

    /**
     * Set Max transaction count
     *
     * @param maxTransactionCount
     */
    public void setMaxTransactionCount(int maxTransactionCount) {
        this.maxTransactionCount = maxTransactionCount;
    }

    /**
     * Get Max trasaction count
     *
     * @return
     */
    public int getMaxTransactionCount() {
        return this.maxTransactionCount;
    }

    /**
     * Set default timeout
     *
     * @param defaultTimeout
     */
    public void setDefaultTimeout(int defaultTimeout) {
        this.defaultTimeout = defaultTimeout;
    }

    /**
     * Get default timeout
     *
     * @return
     */
    public int getDefaultTimeout() {
        return this.defaultTimeout;
    }

    /**
     * Set default instance type
     *
     * @param defaultInstanceType
     */
    public void setDefaultInstanceType(String defaultInstanceType) {
        this.defaultInstanceType = defaultInstanceType;
    }

    /**
     * Get default instance type
     *
     * @return
     */
    public String getDefaultInstanceType() {
        return this.defaultInstanceType;
    }

    /**
     * Set default encoding
     *
     * @param defaultEncoding
     */
    public void setDefaultEncoding(String defaultEncoding) {
        this.defaultEncoding = defaultEncoding;
    }

    /**
     * Get default encoding
     *
     * @return
     */
    public String getDefaultEncoding() {
        return this.defaultEncoding;
    }

    /**
     * Set allowable request delay seconds
     *
     * @param allowableRequestDelaySeconds
     */
    public void setAllowableRequestDelaySeconds(int allowableRequestDelaySeconds) {
        this.allowableRequestDelaySeconds = allowableRequestDelaySeconds;
    }

    /**
     * Get allowable request delay seconds
     *
     * @return
     */
    public int getAllowableRequestDelaySeconds() {
        return this.allowableRequestDelaySeconds;
    }
    
    /**
     * Get default document type
     * @return 
     */
    public String getDefaultDocType() {
        return defaultDocType;
    }

    /**
     * Set default document type
     * @param defaultDocType 
     */
    public void setDefaultDocType(String defaultDocType) {
        this.defaultDocType = defaultDocType;
    }

    /**
     * Get default search document count
     * @return 
     */
    public int getDefaultMaxDocumentCountPerVendor() {
        return defaultMaxDocumentCountPerVendor;
    }

    /**
     * Set default search document count
     * @param defaultMaxDocumentCountPerVendor 
     */
    public void setDefaultMaxDocumentCountPerVendor(int defaultMaxDocumentCountPerVendor) {
        this.defaultMaxDocumentCountPerVendor = defaultMaxDocumentCountPerVendor;
    }
    
    /**
     * Get default search document count
     * @return 
     */
    public int getDefaultMaxSearchCount() {
        return defaultMaxSearchCount;
    }

    /**
     * Set default search document count
     * @param defaultSearchDocumentCount 
     */
    public void setDefaultMaxSearchCount(int defaultSearchDocumentCount) {
        this.defaultMaxSearchCount = defaultSearchDocumentCount;
    }
    
    /**
     * Get default search maximum document count
     * @return 
     */
    public int getDefaultSearchCount() {
        return defaultSearchCount;
    }

    /**
     * Set default search maximum document count
     * @param defaultSearchMaxDocumentCount 
     */
    public void setDefaultSearchCount(int defaultSearchMaxDocumentCount) {
        this.defaultSearchCount = defaultSearchMaxDocumentCount;
    }

    /**
     * Get default minimum score
     * @return 
     */
    public int getDefaultSearchMinScore() {
        return defaultSearchMinScore;
    }

    /**
     * Set default minimum score
     * @param defaultSearchMinScore 
     */
    public void setDefaultSearchMinScore(int defaultSearchMinScore) {
        this.defaultSearchMinScore = defaultSearchMinScore;
    }
    
    /**
     * Get vendor count
     * @return 
     */
    public int getDefaultVendorCount() {
        return defaultVendorCount;
    }

    /**
     * Set vendor count
     * @param defaultVendorCount 
     */
    public void setDefaultVendorCount(int defaultVendorCount) {
        this.defaultVendorCount = defaultVendorCount;
    }
    
    /**
     * Get facet distribution count
     * @return 
     */
    public int getDefaultDistributionCount() {
        return defaultDistributionCount;
    }

    /**
     * Set facet distribution count
     * @param defaultDistributionCount 
     */
    public void setDefaultDistributionCount(int defaultDistributionCount) {
        this.defaultDistributionCount = defaultDistributionCount;
    }

    /**
     * Get get all document ids search count
     * @return 
     */
    public int getDefaultSearchCountGetAllDocIds() {
        return defaultSearchCountGetAllDocIds;
    }

    /**
     * Set get all document ids search count
     * @param defaultSearchCountGetAllDocIds 
     */
    public void setDefaultSearchCountGetAllDocIds(int defaultSearchCountGetAllDocIds) {
        this.defaultSearchCountGetAllDocIds = defaultSearchCountGetAllDocIds;
    }
    
    
}
