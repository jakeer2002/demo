package com.bgt.lens.model.dao;

// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

import com.bgt.lens.model.criteria.SearchFilterTypeCriteria;
import com.bgt.lens.model.entity.SearchFilterType;
import java.util.List;

public interface ISearchFilterTypeDao {
    /**
     * Create Filter Type
     *
     * @param searchFilterType
     */
    public void save(SearchFilterType searchFilterType);

    /**
     * Update Filter Type
     *
     * @param searchFilterType
     */
    public void update(SearchFilterType searchFilterType);

    /**
     * Delete Filter Type
     *
     * @param searchFilterType
     */
    public void delete(SearchFilterType searchFilterType);
    
    /**
     * Get Filter Types
     * @param searchFilterTypeCriteria     
     * @return 
     */
    public List<SearchFilterType> getFilterIDByName(SearchFilterTypeCriteria searchFilterTypeCriteria);    
    
}
