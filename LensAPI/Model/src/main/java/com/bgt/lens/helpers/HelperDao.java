// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.helpers;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.event.service.spi.EventListenerGroup;
import org.hibernate.internal.SessionImpl;

/**
 *
 * Helper dao to communicate with database
 */
public class HelperDao {

    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Create a new record into database
     *
     * @param sessionFactory
     * @param obj
     */
    public void save(SessionFactory sessionFactory, Object obj) {
         LOGGER.info("Get Current Session from SessionFactory");
           Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            session.save(obj);
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Update a record
     *
     * @param sessionFactory
     * @param obj
     */
    public void update(SessionFactory sessionFactory, Object obj) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            session.update(obj);
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Delete a record
     *
     * @param sessionFactory
     * @param obj
     */
    public void delete(SessionFactory sessionFactory, Object obj) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            session.delete(obj);
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Get a record by Id
     *
     * @param sessionFactory
     * @param obj
     * @param id
     * @return
     */
    public Object getById(SessionFactory sessionFactory, Class obj, int id) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            Object object = session.get(obj, id);
            tx.commit();
            LOGGER.info("Transaction Commit");
            return object;
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

}
