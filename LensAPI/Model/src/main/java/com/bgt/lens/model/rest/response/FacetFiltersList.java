// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Facet filter List
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class FacetFiltersList {
    @XmlElement(name = "FacetFiltersList", required = false)
    protected List<FacetFilters> facetFiltersList;

    /**
     * Get facet filter list
     * @return 
     */
    public List<FacetFilters> getFacetFiltersList() {
        return facetFiltersList;
    }

    /**
     * Set facet filter list
     * @param facetFiltersList 
     */
    public void setFacetFiltersList(List<FacetFilters> facetFiltersList) {
        this.facetFiltersList = facetFiltersList;
    }
    
}
