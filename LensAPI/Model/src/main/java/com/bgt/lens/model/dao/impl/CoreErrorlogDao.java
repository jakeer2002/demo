// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.ErrorLogCriteria;
import com.bgt.lens.model.dao.ICoreErrorlogDao;
import com.bgt.lens.model.entity.CoreErrorlog;
import com.bgt.lens.model.entity.CoreUsagelog;

/**
 *
 * Error log table data management
 */
public class CoreErrorlogDao implements ICoreErrorlogDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(CoreErrorlogDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize error log Dao
     */
    public CoreErrorlogDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create error log
     *
     * @param coreErrorlog
     */
    @Override
    public void save(CoreErrorlog coreErrorlog) {
        helperDao.save(sessionFactory, coreErrorlog);
    }

    /**
     * Update error log
     *
     * @param coreErrorlog
     */
    @Override
    public void update(CoreErrorlog coreErrorlog) {
        helperDao.update(sessionFactory, coreErrorlog);
    }

    /**
     * Delete error log
     *
     * @param coreErrorlog
     */
    @Override
    public void delete(CoreErrorlog coreErrorlog) {
        helperDao.delete(sessionFactory, coreErrorlog);
    }

    /**
     * Get error log by Id
     *
     * @param errorLogId
     * @return
     */
    @Override
    public CoreErrorlog getErrorLogById(String errorLogId) {
        CoreErrorlog coreErrorlog = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreErrorlog> criteriaQuery = cb.createQuery(CoreErrorlog.class);
            Root<CoreErrorlog> coreErrorlogRoot = criteriaQuery.from(CoreErrorlog.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(errorLogId)) {
            	conditions.add(cb.equal(coreErrorlogRoot.get("requestId"), errorLogId));
            }

            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreErrorlog> query = session.createQuery(criteriaQuery);
            query.setMaxResults(1);
            List<CoreErrorlog> results=query.getResultList();
            if (results != null && results.size() > 0) {
            	coreErrorlog = results.get(0);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreErrorlog;
    }

    /**
     * Get error log by Id and client
     *
     * @param errorLogId
     * @param clientId
     * @return
     */
    @Override
    public CoreErrorlog getErrorLogById(String errorLogId, int clientId) {
        CoreErrorlog coreErrorlog = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreErrorlog> criteriaQuery = cb.createQuery(CoreErrorlog.class);
            Root<CoreErrorlog> coreErrorlogRoot = criteriaQuery.from(CoreErrorlog.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(errorLogId)) {
            	conditions.add(cb.equal(coreErrorlogRoot.get("requestId"), errorLogId));
            }

            if (_helper.isNotNull(clientId)) {
            	conditions.add(cb.equal(coreErrorlogRoot.get("clientId"), clientId));
            }

            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreErrorlog> query = session.createQuery(criteriaQuery);
            query.setMaxResults(1);
            List<CoreErrorlog> results=query.getResultList();
            if (results != null && results.size() > 0) {
            	coreErrorlog = results.get(0);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreErrorlog;
    }

    /**
     * Get error log by criteria
     *
     * @param errorLogCriteria
     * @return
     */
    @Override
    public List<CoreErrorlog> getErrorLogByCriteria(ErrorLogCriteria errorLogCriteria) {
        List<CoreErrorlog> coreErrorlogList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreErrorlog> criteriaQuery = cb.createQuery(CoreErrorlog.class);
            Root<CoreErrorlog> coreErrorlogRoot = criteriaQuery.from(CoreErrorlog.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (!_helper.isNullOrEmpty(errorLogCriteria.getRequestId())) {
            	conditions.add(cb.equal(coreErrorlogRoot.get("requestId"), errorLogCriteria.getRequestId()));
            }

            if (_helper.isNotNull(errorLogCriteria.getClientId()) && errorLogCriteria.getClientId() > 0) {
            	conditions.add(cb.equal(coreErrorlogRoot.get("clientId"), errorLogCriteria.getClientId()));
            }

            if (_helper.isNotNull(errorLogCriteria.getApiId()) && errorLogCriteria.getApiId() > 0) {
            	conditions.add(cb.equal(coreErrorlogRoot.get("apiId"), errorLogCriteria.getApiId()));
            }

            if (_helper.isNotNull(errorLogCriteria.getStatusCode()) && errorLogCriteria.getStatusCode() > 0) {
            	conditions.add(cb.equal(coreErrorlogRoot.get("statusCode"), errorLogCriteria.getStatusCode()));
            }

            criteriaQuery.orderBy(cb.desc(coreErrorlogRoot.get("id")));
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreErrorlog> query = session.createQuery(criteriaQuery);
            query.setMaxResults(errorLogCriteria.getDefaultResultLimit());
            List<CoreErrorlog> results = query.getResultList();

            // ToDo : Need to check duplicate in list
            if (results != null && results.size() > 0) {
                coreErrorlogList = results;

                Set<CoreErrorlog> coreUsageLogSet = new HashSet<>();
                coreUsageLogSet.addAll(coreErrorlogList);

                coreErrorlogList.clear();
                coreErrorlogList.addAll(coreUsageLogSet);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreErrorlogList;
    }

}
