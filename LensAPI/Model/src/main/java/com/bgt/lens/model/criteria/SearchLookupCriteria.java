// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import java.util.List;

/**
 * Search lookup criteria
 */
public class SearchLookupCriteria {
        
    /**
     * lookup type
     */
    protected String type;
    
    /**
     * lookup key
     */
    protected String lookupKey;
    
    /**
     * Document Type
     */
    protected Integer Id;

    /**
     * Get type
     * @return 
     */
    public String getType() {
        return type;
    }

    /**
     * Set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get lookup key
     * @return 
     */
    public String getLookupKey() {
        return lookupKey;
    }

    /**
     * Set lookup key
     * @param lookupKey 
     */
    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    /**
     * Get id
     * @return 
     */
    public Integer getId() {
        return Id;
    }

    /**
     * Set Id
     * @param Id 
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }
}
