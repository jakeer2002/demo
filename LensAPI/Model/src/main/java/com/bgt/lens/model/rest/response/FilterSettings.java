// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Filter settings
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class FilterSettings {
    /**
     * List of Resume Filters
     */
    @XmlElement(name = "CustomFilters", required = false)
    protected CustomFilters customFilters;
    
    
    /**
     * List of posting filters
     */
    @XmlElement(name = "FacetFilters", required = false)
    protected FacetFilters facetFilters;

    /**
     * Get custom filters
     * @return 
     */
    public CustomFilters getCustomFilters() {
        return customFilters;
    }

    /**
     * Set custom filters
     * @param customFilters 
     */
    public void setCustomFilters(CustomFilters customFilters) {
        this.customFilters = customFilters;
    }

    /**
     * Get facet filters
     * @return 
     */
    public FacetFilters getFacetFilters() {
        return facetFilters;
    }

    /**
     * Set facet filters
     * @param facetFilters 
     */
    public void setFacetFilters(FacetFilters facetFilters) {
        this.facetFilters = facetFilters;
    }

}
