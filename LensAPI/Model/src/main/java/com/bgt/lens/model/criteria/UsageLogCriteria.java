// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import java.util.Date;

/**
 *
 * Usage log criteria parameters to get data from database
 */
public class UsageLogCriteria {

    /**
     * Usage logs from date
     */
    private Date from;

    /**
     * Usage log to date
     */
    private Date to;

    /**
     * Client Id
     */
    private Integer clientId;
    
    /**
     * Consumer key
     */
    private String consumerKey;

    /**
     * API Id
     */
    private Integer apiId;
    
    /**
     * Api
     */
    private String api;

    /**
     * HTTP method
     */
    private String method;

    /**
     * Resource name
     */
    private String resource;

    /**
     * Instance Type
     */
    private String instanceType;
    
    /**
     * Locale
     */
    private String locale;
    
    /**
     * Parse Variant
     */
    private String parseVariant;

    /**
     * Any error in processing the request
     */
    private Boolean raisedError;

    /**
     * Default result set limit
     */
    private int defaultResultLimit;

    /**
     * Get from date
     *
     * @return
     */
    public Date getFrom() {
        return from == null ? null : new Date(from.getTime());
    }

    /**
     * Set From date
     *
     * @param From
     */
    public void setFrom(Date From) {
        this.from = new Date(From.getTime());
    }

    /**
     * Get to date
     *
     * @return
     */
    public Date getTo() {
        return to == null ? null : new Date(to.getTime());
    }

    /**
     * Set to date
     *
     * @param To
     */
    public void setTo(Date To) {
        this.to = new Date(To.getTime());
    }

    /**
     * Get client Id
     *
     * @return
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client Id
     *
     * @param ClientId
     */
    public void setClientId(Integer ClientId) {
        this.clientId = ClientId;
    }
    
    /**
     * Get consumer key
     * @return 
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get API Id
     *
     * @return
     */
    public Integer getApiId() {
        return apiId;
    }

    /**
     * Set API Id
     *
     * @param ApiId
     */
    public void setApiId(Integer ApiId) {
        this.apiId = ApiId;
    }

    /**
     * Get Api
     * @return 
     */
    public String getApi() {
        return api;
    }

    /**
     * Set Api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * Get HTTP method
     *
     * @return
     */
    public String getMethod() {
        return method;
    }

    /**
     * Set HTTP method
     *
     * @param Method
     */
    public void setMethod(String Method) {
        this.method = Method;
    }

    /**
     * Get resource
     *
     * @return
     */
    public String getResource() {
        return resource;
    }

    /**
     * Set resource name
     *
     * @param Resource
     */
    public void setResource(String Resource) {
        this.resource = Resource;
    }
    
    /**
     * Get instance type
     * @return 
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get locale
     * @return 
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set locale
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get parse variant
     * @return 
     */
    public String getParseVariant() {
        return parseVariant;
    }

    /**
     * Set parse variant
     * @param parseVariant
     */
    public void setParseVariant(String parseVariant) {
        this.parseVariant = parseVariant;
    }

    /**
     * Get error status
     *
     * @return
     */
    public Boolean isRaisedError() {
        return raisedError;
    }

    /**
     * Set error status
     *
     * @param raisedError
     */
    public void setRaisedError(Boolean raisedError) {
        this.raisedError = raisedError;
    }

    /**
     * Get default result limit
     *
     * @return
     */
    public int getDefaultResultLimit() {
        return defaultResultLimit;
    }

    /**
     * Set default result limit
     *
     * @param defaultResultLimit
     */
    public void setDefaultResultLimit(int defaultResultLimit) {
        this.defaultResultLimit = defaultResultLimit;
    }
}
