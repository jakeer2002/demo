// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Education history
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class EducationHistory {
    /**
     * Degree
     */
    @XmlElement(name = "Degree", required = false)
    protected String degree;
    
    /**
     * Major
     */
    @XmlElement(name = "Major", required = false)
    protected String major;
    
    /**
     * End date
     */
    @XmlElement(name = "End", required = false)
    protected String end;

    /**
     * Get degree
     * @return 
     */
    public String getDegree() {
        return degree;
    }

    /**
     * Set degree
     * @param degree 
     */
    public void setDegree(String degree) {
        this.degree = degree;
    }

    /**
     * Get major
     * @return 
     */
    public String getMajor() {
        return major;
    }

    /**
     * Set major
     * @param major 
     */
    public void setMajor(String major) {
        this.major = major;
    }

    /**
     * Get end
     * @return 
     */
    public String getEnd() {
        return end;
    }

    /**
     * Set end
     * @param end 
     */
    public void setEnd(String end) {
        this.end = end;
    }
}
