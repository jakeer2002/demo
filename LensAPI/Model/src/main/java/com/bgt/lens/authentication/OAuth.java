// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.authentication;

import com.bgt.lens.helpers.Helper;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

/**
 *
 * OAuth
 */
public class OAuth {

    private static final Logger LOGGER = LogManager.getLogger(OAuth.class);

    /**
     * OAuth version
     */
    protected final String OAuthVersion = "1.0";

    // no need if we dont use get query parameters
    /**
     * OAuth parameter prefix
     */
    protected final String OAuthParameterPrefix = "oauth_";

    // vairable names in Pascal case looks suitable here
    /**
     * OAuth consumer key
     */
    public final String OAuthConsumerKeyKey = "oauth_consumer_key";

    /**
     * OAuth callback key
     */
    public final String OAuthCallbackKey = "oauth_callback";

    /**
     * OAuth version key
     */
    public final String OAuthVersionKey = "oauth_version";

    /**
     * OAuth Signature Method Key
     */
    public final String OAuthSignatureMethodKey = "oauth_signature_method";

    /**
     * OAuth Signature Key
     */
    public final String OAuthSignatureKey = "oauth_signature";

    /**
     * OAuth Timestamp Key
     */
    public final String OAuthTimestampKey = "oauth_timestamp";

    /**
     * OAuth Nonce Key
     */
    public final String OAuthNonceKey = "oauth_nonce";

    /**
     * OAuth Token Key
     */
    public final String OAuthTokenKey = "oauth_token";

    /**
     * OAuth Token Secret Key
     */
    public final String OAuthTokenSecretKey = "oauth_token_secret";

    /**
     * HMACSHA1 Signature Type
     */
    protected final String HMACSHA1SignatureType = "HMAC-SHA1";

    /**
     * Plain Text Signature Type
     */
    protected final String PlainTextSignatureType = "PLAINTEXT";

    /**
     * RSASHA1 Signature Type
     */
    protected final String RSASHA1SignatureType = "RSA-SHA1";

    /**
     * HMAC_SHA1
     */
    private final String HMAC_SHA1 = "HmacSHA1";

    /**
     * Encoding
     */
    private final String ENC = "UTF-8";

    /**
     * base64
     */
    private final Base64 base64 = new Base64();

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Signature generation methods
     */
    public enum SignatureMethods {

        /**
         *
         * Unknown signature method
         */
        UNKNOWN,
        /**
         *
         * Plain text
         */
        PLAINTEXT,
        /**
         *
         * HMAC-SHA1
         */
        HMACSHA1,
        /**
         *
         * RSA-SHA1
         */
        RSASHA1
    }

    /**
     * Query parameter processor
     */
    protected class QueryParameter {

        /**
         *
         * Set query parameter
         *
         * @param name
         * @param value
         */
        public QueryParameter(String name, String value) {
            Name = name;
            Value = value;
        }

        /**
         *
         * Name
         */
        public String Name;

        /**
         *
         * Value
         */
        public String Value;

        /**
         *
         * Get query parameter name
         *
         * @return
         */
        public String getName() {
            return this.Name;
        }

        /**
         *
         * Set query parameter name
         *
         * @param name
         */
        public void setName(String name) {
            this.Name = name;
        }

        /**
         *
         * Get query parameter value
         *
         * @return
         */
        public String getValue() {
            return this.Value;
        }

        /**
         *
         * Set query parameter value
         *
         * @param value
         */
        public void setValue(String value) {
            this.Value = value;
        }

    }

    // Get query parameters
    /**
     * Get query parameters
     *
     * @param paramters
     * @return
     */
    private List<QueryParameter> getQueryParameters(String paramters) {

        List<QueryParameter> result = new ArrayList<>();
        if (_helper.isNotNull(paramters) && paramters.startsWith("?")) {
            paramters = paramters.substring(1);
        }

        if (!_helper.isNullOrEmpty(paramters)) {
            String[] p = paramters.split("&");
            for (String s : p) {
                if (!_helper.isNullOrEmpty(s) && !s.startsWith(OAuthParameterPrefix)) {
                    if (s.contains("=")) {
                        String[] temp = s.split("=");
                        result.add(new QueryParameter(temp[0], temp[1]));
                    } else {
                        result.add(new QueryParameter(s, ""));
                    }
                }
            }
        }
        return result;
    }

    // Normalize request parameters
    /**
     * Normalize request parameters
     *
     * @param parameters
     * @return
     */
    protected String normalizeRequestParameters(List<QueryParameter> parameters) {

        StringBuilder sb = new StringBuilder();
        QueryParameter p = null;

        for (int i = 0; i < parameters.size(); i++) {
            p = parameters.get(i);
            sb.append(String.format("%s=%s", p.Name, p.Value));
            if (i < (parameters.size() - 1)) {
                sb.append("&");
            }
        }

        return sb.toString();
    }

    // generate HMACSHA1 signature base
    /**
     * Generate signature base
     *
     * @param request
     * @param consumerKey
     * @param token
     * @param tokenSecret
     * @param httpMethod
     * @param timeStamp
     * @param nonce
     * @param signatureType
     * @return
     */
    public String generateSignatureBase(HttpServletRequest request, String consumerKey, String token, String tokenSecret, String httpMethod, Long timeStamp, String nonce, String signatureType) throws UnsupportedEncodingException {
        if (token == null) {
            token = "";
        }
        if (tokenSecret == null) {
            tokenSecret = "";
        }

        // check other credentials
        if (_helper.isNullOrEmpty(consumerKey)) {
            throw new IllegalArgumentException("consumerKey");
        }
        if (_helper.isNullOrEmpty(httpMethod)) {
            throw new IllegalArgumentException("httpMethod");
        }
        if (_helper.isNullOrEmpty(signatureType)) {
            throw new IllegalArgumentException("signatureType");
        }

        // send request parameters
         
        java.util.List<QueryParameter> parameters = getQueryParameters(request.getQueryString());
        parameters.add(new QueryParameter(OAuthVersionKey, OAuthVersion));
        parameters.add(new QueryParameter(OAuthNonceKey, nonce));
        parameters.add(new QueryParameter(OAuthTimestampKey, timeStamp.toString()));
        parameters.add(new QueryParameter(OAuthSignatureMethodKey, signatureType));
        parameters.add(new QueryParameter(OAuthConsumerKeyKey, consumerKey));

        // add token
        if (!_helper.isNullOrEmpty(token)) {
            parameters.add(new QueryParameter(OAuthTokenKey, token));
        }

        Collections.sort(parameters, new QueryParameterComparer());

        String normalizedUrl = String.format("%s://%s", request.getScheme(), request.getServerName());

        if (!(("http".equals(request.getScheme()) && request.getServerPort() == 80) || ("https".equals(request.getScheme()) && request.getServerPort() == 443))) {
            normalizedUrl += ":" + request.getServerPort();
        }

        normalizedUrl += request.getRequestURI();

        String normalizedRequestParameters = normalizeRequestParameters(parameters);

        StringBuilder signatureBase = new StringBuilder();
        signatureBase.append(String.format("%s&", httpMethod.toUpperCase(Locale.ENGLISH)));
        signatureBase.append(String.format("%s&", URLEncoder.encode(normalizedUrl,java.nio.charset.StandardCharsets.UTF_8.toString())));
        signatureBase.append(String.format("%s", URLEncoder.encode(normalizedRequestParameters,java.nio.charset.StandardCharsets.UTF_8.toString())));
        return signatureBase.toString();
    }

    // generate signature using hashmap
    /**
     * Generate OAuth signature hash
     *
     * @param signatureBase
     * @param hashKey
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public String generateSignatureUsingHash(String signatureBase, byte[] hashKey) throws UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeyException {
        SecretKey key = new SecretKeySpec(hashKey, HMAC_SHA1);

        Mac mac = Mac.getInstance(HMAC_SHA1);
        mac.init(key);

        // encode it, base64 it and return.
        return new String(base64.encode(mac.doFinal(signatureBase.getBytes(
                "UTF-8"))), "UTF-8").trim();
    }

    //generate Signature
    /**
     * Generate OAuth signature
     *
     * @param request
     * @param consumerKey
     * @param consumerSecret
     * @param token
     * @param tokenSecret
     * @param httpMethod
     * @param timeStamp
     * @param nonce
     * @param signatureType
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public String generateSignature(HttpServletRequest request, String consumerKey, String consumerSecret, String token, String tokenSecret, String httpMethod, Long timeStamp, String nonce, SignatureMethods signatureType) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {

        switch (signatureType) {
            case PLAINTEXT:
                return String.format("%s&%s", consumerSecret, tokenSecret);

            case HMACSHA1:
                String signatureBase = generateSignatureBase(request, consumerKey, token, tokenSecret, httpMethod, timeStamp, nonce, HMACSHA1SignatureType);
                LOGGER.info("SignatureBase :" + signatureBase);
                byte[] hashKey = (String.format("%s&%s", URLEncoder.encode(consumerSecret,java.nio.charset.StandardCharsets.UTF_8.toString()), _helper.isNullOrEmpty(tokenSecret) ? "" : URLEncoder.encode(tokenSecret,java.nio.charset.StandardCharsets.UTF_8.toString()))).getBytes("ASCII");

                String signature = generateSignatureUsingHash(signatureBase, hashKey);

                StringBuilder auth = new StringBuilder();
                auth.append(String.format("%s=\"%s\", ", OAuthConsumerKeyKey, URLEncoder.encode(consumerKey,java.nio.charset.StandardCharsets.UTF_8.toString())));
                auth.append(String.format("%s=\"%s\", ", OAuthConsumerKeyKey, URLEncoder.encode(consumerKey,java.nio.charset.StandardCharsets.UTF_8.toString())));
                auth.append(String.format("%s=\"%s\", ", OAuthNonceKey, URLEncoder.encode(nonce,java.nio.charset.StandardCharsets.UTF_8.toString())));
                auth.append(String.format("%s=\"%s\", ", OAuthSignatureKey, URLEncoder.encode(signature,java.nio.charset.StandardCharsets.UTF_8.toString())));
                auth.append(String.format("%s=\"%s\", ", OAuthSignatureMethodKey, "HMAC-SHA1"));
                auth.append(String.format("%s=\"%s\", ", OAuthTokenKey, URLEncoder.encode(token,java.nio.charset.StandardCharsets.UTF_8.toString())));

                // authHeader will be used further
                //String authHeader = auth.toString();
                return signature;

            case RSASHA1:
                throw new UnsupportedOperationException("Not implemented");
            default:
                throw new IllegalArgumentException("Unknown Signature type");

        }

    }

    // generate timeStamp
    /**
     * Generate timestamp
     *
     * @param dateTime
     * @return
     */
    public Long generateTimeStamp(DateTime dateTime) {
        DateTime modifiedUtcTime = dateTime;
        DateTime unixTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        return (modifiedUtcTime.getMillis() - unixTime.getMillis()) / 1000L;
    }

    //Signature methods
    /**
     * Parse signature method from request header
     *
     * @param signatureMethod
     * @return
     */
    public SignatureMethods parseSignatureMethod(String signatureMethod) {
        switch (signatureMethod) {
            case PlainTextSignatureType:
                return SignatureMethods.PLAINTEXT;
            case HMACSHA1SignatureType:
                return SignatureMethods.HMACSHA1;
            case RSASHA1SignatureType:
                return SignatureMethods.RSASHA1;
            default:
                return SignatureMethods.UNKNOWN;
        }
    }

    /**
     * Query parameter comparator
     */
    
    public class QueryParameterComparer implements java.util.Comparator <QueryParameter> {

        /**
         *
         * Compare query parameter
         *
         * @param ob1
         * @param ob2
         * @return
         */
        @Override
        public int compare(QueryParameter ob1, QueryParameter ob2) {
            
            if (ob1 instanceof QueryParameter && ob2 instanceof QueryParameter) {
                QueryParameter x = (QueryParameter) ob1;
                QueryParameter y = (QueryParameter) ob2;
                if (x.Value == null ? y.Value == null : x.Value.equals(y.Value)) {
                    if (y.Value != null) {
                        return x.Value.compareTo(y.Value);
                    }
                }
                return x.Name.compareTo(y.Name);
            } else {
                throw new ClassCastException("QueryParameterComparer: Illegal arguments!");
            }
        }
    }
}
