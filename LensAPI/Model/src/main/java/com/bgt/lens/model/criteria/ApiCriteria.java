// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

/**
 *
 * API criteria parameters to get details from database
 */
public class ApiCriteria {

    /**
     * API Key
     */
    private String apiKey;

    /**
     * API name
     */
    private String name;

    /**
     * Get API Key
     *
     * @return
     */
    public String getApiKey() {
        return this.apiKey;
    }

    /**
     * Set API Key
     *
     * @param apiKey
     */
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * Get API name
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set API name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

}
