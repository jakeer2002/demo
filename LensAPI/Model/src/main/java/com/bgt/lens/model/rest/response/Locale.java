/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bgt.lens.model.rest.response;

import javax.xml.bind.annotation.XmlElement;

public class Locale {
   //  @XmlElement(name = "Language", required = true)
    protected String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
   // @XmlElement(name = "Country", required = true)
    protected String country;
    
}
