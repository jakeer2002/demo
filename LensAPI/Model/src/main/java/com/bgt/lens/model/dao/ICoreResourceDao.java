// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.ResourceCriteria;
import com.bgt.lens.model.entity.CoreResources;
import java.util.List;

/**
 *
 * Resource dao Interface
 */
public interface ICoreResourceDao {

    /**
     * Create resource
     *
     * @param coreResources
     */
    public void save(CoreResources coreResources);

    /**
     * Update resource
     *
     * @param coreResources
     */
    public void update(CoreResources coreResources);

    /**
     * Delete resource
     *
     * @param coreResources
     */
    public void delete(CoreResources coreResources);

    /**
     * Get resource by Id
     *
     * @param coreResourceId
     * @return
     */
    public CoreResources getResourcesById(int coreResourceId);

    /**
     * Get resource criteria
     *
     * @param resourceCriteria
     * @return
     */
    public List<CoreResources> getResourcesByCriteria(ResourceCriteria resourceCriteria);
    
    /**
     * Get resource criteria
     *
     * @param resourceCriteria
     * @return
     */
    public List<CoreResources> getResourcesWithoutApiByCriteria(ResourceCriteria resourceCriteria);
}
