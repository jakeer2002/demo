// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Search result
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class SearchResult {
    /**
     * Vendor name
     */
    @XmlElement(name = "VendorName", required = false)
    protected String vendorName;
    
    /**
     * Locale
     */
    @XmlElement(name = "Locale", required = false)
    protected String locale;
    
    /**
     * Instance type
     */
    @XmlElement(name = "InstanceType", required = false)
    protected String instanceType;
    
    /**
     * Results count
     */
    @XmlElement(name = "ResultsCount", required = false)
    protected Integer resultsCount;
    
    /**
     * Offset
     */
    @XmlElement(name = "Offset", required = false)
    protected Integer offset;
    
    /**
     * Maximum document count
     */
    @XmlElement(name = "MaxDocumentCount", required = false)
    protected Integer maxDocumentCount;
    
    /**
     * Minimum LENS/Keyword score
     */
    @XmlElement(name = "MinimumScore", required = false)
    protected Integer minimumScore;

    /**
     * Sort
     */
    @XmlElement(name = "Sort", required = false)
    protected String sort;
    
    /**
     * Order by
     */
    @XmlElement(name = "OrderBy", required = false)
    protected String orderBy;
    
    /**
     * Sort direction
     */
    @XmlElement(name = "SortDirection", required = false)
    protected String sortDirection;
    
    /**
     * Lastdocid
     */
    @XmlElement(name = "Lastdocid", required = false)
    protected String lastdocid;
    
    /**
     * List of candidates
     */
    @XmlElement(name = "Candidates", required = false)
    protected List<Candidate> candidates;
    
    /**
     * Getdocids
     */
    @XmlElement(name = "Getdocids", required = false)
    protected Getdocids getdocids;    
    
    /**
     * List of jobs
     */
    @XmlElement(name = "Jobs", required = false)
    protected List<Job> jobs;
    
    /**
     * List of facet distribution
     */
    @XmlElement(name = "FacetDistribution", required = false)
    protected List<FacetDistribution> facetDistribution;
    
    /**
     * List of facet distribution
     */
    @XmlElement(name = "ScoringMode", required = false)
    protected String scoringMode;
    
    /**
     * Show records start index
     */
    @XmlElement(name = "StartIndex", required = false)
    protected Integer startIndex;
    
    /**
     * Show records end index
     */
    @XmlElement(name = "EndIndex", required = false)
    protected Integer endIndex;
    
    /**
     * Es Total found
     */
    @XmlElement(name = "ESTotalFound", required = false)
    protected Integer esTotalFound;
    
    /**
     * Total found
     */
    @XmlElement(name = "TotalFound", required = false)
    protected Integer totalFound;
    
    /**
     * LENS version
     */
    @XmlElement(name = "LensVersion", required = false)
    protected String lensVersion;

    /**
     * Get getdocids
     * @return 
     */
    public Getdocids getGetdocids() {
        return getdocids;
    }

    /**
     * Set getdocids
     * @param getdocids 
     */
    public void setGetdocids(Getdocids getdocids) {
        this.getdocids = getdocids;
    }
    
    
    /**
     * Get vendor name
     * @return 
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * Set vendor name
     * @param vendorName 
     */
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    /**
     * Get locale
     * @return 
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set locale
     * @param locale 
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get instance type
     * @return 
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     * @param instanceType 
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get results count
     * @return 
     */
    public Integer getResultsCount() {
        return resultsCount;
    }

    /**
     * Set results count
     * @param resultsCount 
     */
    public void setResultsCount(Integer resultsCount) {
        this.resultsCount = resultsCount;
    }

    /**
     * Get offset
     * @return 
     */
    public Integer getOffset() {
        return offset;
    }

    /**
     * Set offset
     * @param offset 
     */
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    /**
     * Get maximum document count
     * @return 
     */
    public Integer getMaxDocumentCount() {
        return maxDocumentCount;
    }

    /**
     * Set maximum document count
     * @param maxDocumentCount 
     */
    public void setMaxDocumentCount(Integer maxDocumentCount) {
        this.maxDocumentCount = maxDocumentCount;
    }

    /**
     * Get minimum score
     * @return 
     */
    public Integer getMinimumScore() {
        return minimumScore;
    }

    /**
     * Set minimum score
     * @param minimumScore 
     */
    public void setMinimumScore(Integer minimumScore) {
        this.minimumScore = minimumScore;
    }

    /**
     * Get list of candidates
     * @return 
     */
    public List<Candidate> getCandidates() {
        return candidates;
    }

    /**
     * Set list of candidates
     * @param candidate 
     */
    public void setCandidates(List<Candidate> candidate) {
        this.candidates = candidate;
    }
    
    /**
     * Get facet distribution list
     * @return 
     */
    public List<FacetDistribution> getFacetDistribution() {
        return facetDistribution;
    }

    /**
     * Set facet distribution list
     * @param facetDistributionList 
     */
    public void setFacetDistribution(List<FacetDistribution> facetDistributionList) {
        this.facetDistribution = facetDistributionList;
    }
    
    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
    /**
     * Get order by
     * @return 
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Set order by
     * @param orderBy 
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * Get sort direction
     * @return 
     */
    public String getSortDirection() {
        return sortDirection;
    }

    /**
     * Set sort direction
     * @param sortDirection 
     */
    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }
    
    /**
     * Get scoring mode
     * @return 
     */
    public String getScoringMode() {
        return scoringMode;
    }

    /**
     * Set scoring mode
     * @param scoringMode 
     */
    public void setScoringMode(String scoringMode) {
        this.scoringMode = scoringMode;
    }
    
    /**
     * Start index
     * @return 
     */
    public Integer getStartIndex() {
        return startIndex;
    }

    /**
     * Start index
     * @param startIndex 
     */
    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * End index
     * @return 
     */
    public Integer getEndIndex() {
        return endIndex;
    }

    /**
     * End index
     * @param endIndex 
     */
    public void setEndIndex(Integer endIndex) {
        this.endIndex = endIndex;
    }

    /**
     * Get Jobs
     *
     * @return
     */
    public List<Job> getJobs() {
        return jobs;
    }

    /**
     * Set Jobs
     *
     * @param jobs
     */
    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }
    
    /**
     * Get total docs found in ES
     * @return 
     */
    public Integer getEsTotalFound() {
        return esTotalFound;
    }

    /**
     * Set total docs found in ES
     * @param esTotalFound 
     */
    public void setEsTotalFound(Integer esTotalFound) {
        this.esTotalFound = esTotalFound;
    }
    
    /**
     * Get total docs found 
     * @return 
     */
    public Integer getTotalFound() {
        return totalFound;
    }

    /**
     * Set total docs found
     * @param totalFound 
     */
    public void setTotalFound(Integer totalFound) {
        this.totalFound = totalFound;
    }

    /**
     * Get LENS version
     * @return 
     */
    public String getLensVersion() {
        return lensVersion;
    }

    /**
     * Set LENS version
     * @param lensVersion 
     */
    public void setLensVersion(String lensVersion) {
        this.lensVersion = lensVersion;
    }

    /**
     * Get Lastdocid
     * @return 
     */
    public String getLastdocid() {
        return lastdocid;
    }

    /**
     * Set lastdocid
     * @param lastdocid 
     */
    public void setLastdocid(String lastdocid) {
        this.lastdocid = lastdocid;
    }
    
}
