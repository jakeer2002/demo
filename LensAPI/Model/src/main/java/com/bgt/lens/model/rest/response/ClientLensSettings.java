// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD) 
public class ClientLensSettings {
    
    @XmlElement(name = "InstanceId", required = false)
    protected Integer instanceId;
        /**
     * RateLimitEnabled
     */
    @XmlElement(name = "RateLimitEnabled", required = false)
    protected boolean rateLimitEnabled;

    /**
     * RateLimitTimePeriod
     */
    @XmlElement(name = "RateLimitTimePeriod", required = false)
    protected Integer rateLimitTimePeriod;

    /**
     * RateLimitCount
     *
     */
    @XmlElement(name = "RateLimitCount", required = false)
    protected Integer rateLimitCount;
    
    /**
     * Custom Skill Id
     *
     */
    @XmlElement(name = "CustomSkillsId", required = false)
    protected Integer customSkillsId;

    public Integer getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Integer instanceId) {
        this.instanceId = instanceId;
    }

    public boolean isRateLimitEnabled() {
        return rateLimitEnabled;
    }

    public void setRateLimitEnabled(boolean rateLimitEnabled) {
        this.rateLimitEnabled = rateLimitEnabled;
    }

    public Integer getRateLimitTimePeriod() {
        return rateLimitTimePeriod;
    }

    public void setRateLimitTimePeriod(Integer rateLimitTimePeriod) {
        this.rateLimitTimePeriod = rateLimitTimePeriod;
    }

    public Integer getRateLimitCount() {
        return rateLimitCount;
    }

    public void setRateLimitCount(Integer rateLimitCount) {
        this.rateLimitCount = rateLimitCount;
    }

    public Integer getCustomSkillsId() {
        return customSkillsId;
    }

    public void setCustomSkillsId(Integer customSkillsId) {
        this.customSkillsId = customSkillsId;
    }   
    
}
