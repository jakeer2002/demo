// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.authentication;


import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpStatus;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Helper;

/**
 *
 * Authorization header parser
 */
public class AuthorizationHeaderParser {
	
	private final Helper _helper = new Helper();

    /**
     * Parse http header
     *
     * @param input
     * @return
     */
    public String parse(String input) {
    	
    	Object client_id;
    	String token=input.split(" ")[1];
    	byte[] en_payload;
		try {
			en_payload = token.split("\\.")[1].getBytes("UTF-8");
			String de_payload= new String(Base64.decodeBase64(en_payload),"UTF-8");
	        JSONObject json_payload = (JSONObject) new JSONParser().parse(de_payload.trim());
	        
	        client_id=json_payload.get("client_id");
	        return client_id.toString();
		} catch (Exception ex ) {
			throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_AUTHENTICATION_HEADER), HttpStatus.BAD_REQUEST);
		}
		
    }
}
