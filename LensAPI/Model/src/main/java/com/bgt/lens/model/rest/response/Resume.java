/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author psagadevan
 */
/**
 *
 * @author psagadevan
 */
@XmlRootElement(name = "Resume")
public class Resume {
    
    @XmlElement(name = "Resume")
    @JsonProperty("Resume")
    public Object resume;
    
}
