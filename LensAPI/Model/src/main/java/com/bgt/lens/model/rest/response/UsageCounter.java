// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * Usage counter
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class UsageCounter {

    /**
     * Usage counter Id
     */
    @XmlElement(name = "Id", required = false)
    protected Integer id;

    /**
     * Client Id
     */
    @XmlElement(name = "ClientId", required = false)
    protected Integer clientId;

    /**
     * API Id
     */
    @XmlElement(name = "ApiId", required = false)
    protected Integer apiId;

    /**
     * HTTP method
     */
    @XmlElement(name = "Method", required = false)
    protected String method;

    /**
     * Resource name
     */
    @XmlElement(name = "Resource", required = false)
    protected String resource;

    /**
     * Usage count
     */
    @XmlElement(name = "UsageCount", required = false)
    protected Integer usageCount;

    /**
     * Created date
     */
    @XmlElement(name = "CreatedOn", required = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    protected Date createdOn;

    /**
     * Updated date
     */
    @XmlElement(name = "UpdatedOn", required = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    protected Date updatedOn;

    /**
     * Get usage counter Id
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set usage counter Id
     *
     * @param Id
     */
    public void setId(Integer Id) {
        this.id = Id;
    }

    /**
     * Get client Id
     *
     * @return
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client Id
     *
     * @param ClientId
     */
    public void setClientId(Integer ClientId) {
        this.clientId = ClientId;
    }

    /**
     * Get API Id
     *
     * @return
     */
    public Integer getApiId() {
        return apiId;
    }

    /**
     * Set API Id
     *
     * @param ApiId
     */
    public void setApiId(Integer ApiId) {
        this.apiId = ApiId;
    }

    /**
     * Get HTTP method
     *
     * @return
     */
    public String getMethod() {
        return method;
    }

    /**
     * Set HTTP method
     *
     * @param Method
     */
    public void setMethod(String Method) {
        this.method = Method;
    }

    /**
     * Get resource
     *
     * @return
     */
    public String getResource() {
        return resource;
    }

    /**
     * Set resource
     *
     * @param Resource
     */
    public void setResource(String Resource) {
        this.resource = Resource;
    }

    /**
     * Get usage count
     *
     * @return
     */
    public Integer getUsageCount() {
        return usageCount;
    }

    /**
     * Set usage count
     *
     * @param UsageCount
     */
    public void setUsageCount(Integer UsageCount) {
        this.usageCount = UsageCount;
    }

    /**
     * Get created date
     *
     * @return
     */
    public Date getCreatedOn() {
        return createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Set created date
     *
     * @param CreatedOn
     */
    public void setCreatedOn(Date CreatedOn) {
        this.createdOn = CreatedOn == null ? null : new Date(CreatedOn.getTime());
    }

    /**
     * Get updated date
     *
     * @return
     */
    public Date getUpdatedOn() {
        return updatedOn == null ? null : new Date(updatedOn.getTime());
    }

    /**
     * Set updated date
     *
     * @param UpdatedOn
     */
    public void setUpdatedOn(Date UpdatedOn) {
        this.updatedOn = UpdatedOn == null ? null : new Date(UpdatedOn.getTime());
    }

}
