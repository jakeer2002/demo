// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.bgt.lens.model.parserservice.request.InfoResponse;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;

/**
 *
 * API response
 */
@XmlRootElement(name = "APIResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Client.class, InfoResponse.class,Api.class, LensSettings.class, License.class, 
    LicenseList.class, ClientList.class, LensSettingsList.class, ApiList.class, UsageCounter.class, 
    UsageCounterList.class, UsageLog.class, UsageLogList.class, ErrorLog.class, ErrorLogList.class, 
    Instance.class, InstanceList.class,com.bgt.lens.model.rest.response.Resume.class, Resources.class, 
    ResourceList.class, JSONObject.class, RegisterDocument.class, Vendor.class, VendorList.class, 
    RegisterLog.class, RegisterLogList.class, SearchCommandDump.class, SearchCommandDumpList.class, 
    SearchResult.class, FetchResume.class, FetchJob.class, FacetFiltersList.class})
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class ApiResponse {

    /**
     * Initialize the new instance of API Response Message
     */
    public ApiResponse() {
        status = false;
        statusCode = HttpStatus.OK;
        responseData = "";
    }

    /**
     * API response status.True for success and False for Failure
     */
    @XmlElement(name = "Status")
    public boolean status;

    /**
     * API response status code.True for success and False for Failure
     */
    @XmlElement(name = "StatusCode")
    public HttpStatus statusCode;

    /**
     * Unique id for a transaction
     */
    @XmlElement(name = "RequestId")
    public String requestId;

    /**
     * Unique id for a transaction
     */
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @XmlElement(name = "TimeStamp")
    public Date timeStamp;

    /**
     * Info command response message
     */
    @XmlElement(name = "ResponseData")
    public Object responseData;

    /**
     * Identified Locale
     */
    @XmlElement(name = "IdentifiedLocale")
    public String identifiedLocale;

    /**
     * ProcessedWithLocale
     */
    @XmlElement(name = "ProcessedWithLocale")
    public String processedWithLocale;

    /**
     * *
     * Get status
     *
     * @return
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * *
     * Set status
     *
     * @param Status
     */
    public void setStatus(boolean Status) {
        this.status = Status;
    }

    /**
     * *
     * Get status code
     *
     * @return
     */
    public HttpStatus getStatusCode() {
        return statusCode;
    }

    /**
     * *
     * Set status code
     *
     * @param StatusCode
     */
    public void setStatusCode(HttpStatus StatusCode) {
        this.statusCode = StatusCode;
    }

    /**
     * *
     * Get request Id
     *
     * @return
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * *
     * Set Request Id
     *
     * @param RequestId
     */
    public void setRequestId(String RequestId) {
        this.requestId = RequestId;
    }

    /**
     * *
     * Get timestamp
     *
     * @return
     */
    public Date getTimeStamp() {
        return timeStamp == null ? null : new Date(timeStamp.getTime());
    }

    /**
     * *
     * Set timestamp
     *
     * @param TimeStamp
     */
    public void setTimeStamp(Date TimeStamp) {
        this.timeStamp = TimeStamp == null ? null : new Date(TimeStamp.getTime());
    }

    /**
     * *
     * Get response data
     *
     * @return
     */
    public Object getResponseData() {
        return responseData;
    }

    /**
     * *
     * Set response data
     *
     * @param ResponseData
     */
    public void setResponseData(Object ResponseData) {
        this.responseData = ResponseData;
    }

    /**
     * Get Identified locale
     *
     * @return
     */
    public String getIdentifiedLocale() {
        return identifiedLocale;
    }

    /**
     * Set Locale
     *
     * @param identifiedLocale
     */
    public void setIdentifiedLocale(String identifiedLocale) {
        this.identifiedLocale = identifiedLocale;
    }

    /**
     * Get Processed with locale
     *
     * @return
     */
    public String getProcessedWithLocale() {
        return processedWithLocale;
    }

    /**
     * Set Processed with locale
     *
     * @param processedWithLocale
     */
    public void setProcessedWithLocale(String processedWithLocale) {
        this.processedWithLocale = processedWithLocale;
    }
}
