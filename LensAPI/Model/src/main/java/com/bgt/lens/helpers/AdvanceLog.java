// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.helpers;

public class AdvanceLog {

    /**
     * Authentication Validation Seconds
     */
    protected double authenticationValidation;

    /**
     * Authentication Log Seconds
     */
    protected double authenticationLog;

    /**
     * Utility Data Processing Seconds
     */
    protected double utilityDataProcessing;

    /**
     * Lens Communication Seconds
     */
    protected double lensCommunication;

    /**
     * Usage Counter Update Seconds
     */
    protected double usageCounterUpdate;

    /**
     * Usage Log Seconds
     */
    protected double usageLog;

    /**
     * errorLog Seconds
     */
    protected double errorLog;

    /**
     * Update Transaction Count Seconds
     */
    protected double UpdateTransactionCount;

    /**
     * Lens Document Dump Seconds
     */
    protected double LensDocumentDump;
    
    /**
     * Locale detection dump seconds 
     */
    protected double localeDetectionDump;
    
    /**
     * HRXML stylesheet update 
     */
    protected double hrxmlStyleSheetUpdate;  
    
    /**
     * Parsing dump
     */
    protected double parsingDump;
    
    /**
     * Search controller unit
     */
    protected double searchControllerUnit;
    
    /**
     * Search service unit
     */
    protected double searchServiceUnit;
    
    /**
     * Search result process unit
     */
    protected double searchResultProcessUnit;
    
    /**
     * Search command dump unit
     */
    protected double searchCommandDumpUnit;
    
    /**
     * Search command build unit
     */
    protected double searchCommandbuildUnit;
    
    /**
     * Search command process unit
     */
    protected double searchCommandProcessUnit;
    
    /**
     * Resume search - parse resume
     */
    protected double searchParseDocument;
    
    /**
     * Facet dist command build unit
     */
    protected double facetDistributionCommandBuildUnit;
    
    /**
     * Facet dist command process unit
     */
    protected double facetDistributionCommandProcessUnit;
    
    /**
     * Register resume unit
     */
    protected double registerDocumentWithoutCustomFilterUnit;
    
    /**
     * Tag binary data unit
     */
    protected double registerParseDocument;
    
    /**
     * Register document with custom filter unit
     */
    protected double registerDocumentWithCustomFilterUnit;
    
    /**
     * Unregister document unit
     */
    protected double unregisterDocumentUnit;
    
    /**
     * Over write register document unit
     */
    protected double overwriteRegisterDocumentUnit;
    
    /**
     * Update registered document count
     */
    protected double updateRegisterDocumentCount;
    
    /**
     * Fetch resume process
     */
    protected double fetchDocumentProcessUnit;
    
        /**
     * Update registered document count
     */
    protected double apiRateLimit;
    
    /**
     * Fetch resume process
     */
    protected double productRateLimit;
    
    /**
     * Get authentication validation
     * @return 
     */
    public double getAuthenticationValidation() {
        return authenticationValidation;
    }

    /**
     * Set authentication validation
     * @param authenticationValidation 
     */
    public void setAuthenticationValidation(double authenticationValidation) {
        this.authenticationValidation = authenticationValidation;
    }

    /**
     * Get authentication log
     * @return 
     */
    public double getAuthenticationLog() {
        return authenticationLog;
    }

    /**
     * Set authentication log
     * @param authenticationLog 
     */
    public void setAuthenticationLog(double authenticationLog) {
        this.authenticationLog = authenticationLog;
    }

    /**
     * Get utility data processing
     * @return 
     */
    public double getUtilityDataProcessing() {
        return utilityDataProcessing;
    }

    /**
     * Set utility data processing
     * @param utilityDataProcessing 
     */
    public void setUtilityDataProcessing(double utilityDataProcessing) {
        this.utilityDataProcessing = utilityDataProcessing;
    }

    /**
     * Get LENS communication
     * @return 
     */
    public double getLensCommunication() {
        return lensCommunication;
    }

    /**
     * Set LENS communication
     * @param lensCommunication 
     */
    public void setLensCommunication(double lensCommunication) {
        this.lensCommunication = lensCommunication;
    }

    /**
     * Get usage counter update
     * @return 
     */
    public double getUsageCounterUpdate() {
        return usageCounterUpdate;
    }

    /**
     * Set usage counter updated
     * @param usageCounterUpdate 
     */
    public void setUsageCounterUpdate(double usageCounterUpdate) {
        this.usageCounterUpdate = usageCounterUpdate;
    }

    /**
     * Get usage log
     * @return 
     */
    public double getUsageLog() {
        return usageLog;
    }

    /**
     * Set usage log
     * @param usageLog 
     */
    public void setUsageLog(double usageLog) {
        this.usageLog = usageLog;
    }

    /**
     * Get error log
     * @return 
     */
    public double getErrorLog() {
        return errorLog;
    }

    /**
     * Set error log
     * @param errorLog 
     */
    public void setErrorLog(double errorLog) {
        this.errorLog = errorLog;
    }

    /**
     * Get update transaction count
     * @return 
     */
    public double getUpdateTransactionCount() {
        return UpdateTransactionCount;
    }

    /**
     * Set update transaction count
     * @param UpdateTransactionCount 
     */
    public void setUpdateTransactionCount(double UpdateTransactionCount) {
        this.UpdateTransactionCount = UpdateTransactionCount;
    }

    /**
     * Get LENS document dump
     * @return 
     */
    public double getLensDocumentDump() {
        return LensDocumentDump;
    }

    /**
     * Set LENS document dump
     * @param LensDocumentDump 
     */
    public void setLensDocumentDump(double LensDocumentDump) {
        this.LensDocumentDump = LensDocumentDump;
    }
    
    /**
     * Get locale detect dump
     * @return 
     */
    public double getLocaleDetectionDump() {
        return localeDetectionDump;
    }

    /**
     * Set locale detect dump
     * @param localeDetectionDump 
     */
    public void setLocaleDetectionDump(double localeDetectionDump) {
        this.localeDetectionDump = localeDetectionDump;
    }
    
    /**
     * Get HRXML stylesheet update
     * @return 
     */
    public double getHrxmlStyleSheetUpdate() {
        return hrxmlStyleSheetUpdate;
    }

    /**
     * Set HRXML stylesheet update
     * @param HRXMLStyleSheetUpdate 
     */
    public void setHrxmlStyleSheetUpdate(double HRXMLStyleSheetUpdate) {
        this.hrxmlStyleSheetUpdate = HRXMLStyleSheetUpdate;
    }
    
    /**
     * Get parsing dump
     * @return 
     */
    public double getParsingDump() {
        return parsingDump;
    }

    /**
     * Set parsing dump
     * @param parsingDump 
     */
    public void setParsingDump(double parsingDump) {
        this.parsingDump = parsingDump;
    }
    
    /**
     * Get search controller unit
     * @return 
     */
    public double getSearchControllerUnit() {
        return searchControllerUnit;
    }

    /**
     * Set search controller unit
     * @param searchControllerUnit 
     */
    public void setSearchControllerUnit(double searchControllerUnit) {
        this.searchControllerUnit = searchControllerUnit;
    }
    
    /**
     * Get search service unit
     * @return 
     */
    public double getSearchServiceUnit() {
        return searchServiceUnit;
    }

    /**
     * Set search service unit
     * @param searchServiceUnit 
     */
    public void setSearchServiceUnit(double searchServiceUnit) {
        this.searchServiceUnit = searchServiceUnit;
    }
    
    /**
     * Get search result process unit
     * @return 
     */
    public double getSearchResultProcessUnit() {
        return searchResultProcessUnit;
    }

    /**
     * Set search result process unit
     * @param searchResultProcessUnit 
     */
    public void setSearchResultProcessUnit(double searchResultProcessUnit) {
        this.searchResultProcessUnit = searchResultProcessUnit;
    }
    
    /**
     * Get search command dump unit
     * @return 
     */
    public double getSearchCommandDumpUnit() {
        return searchCommandDumpUnit;
    }

    /**
     * Set search command dump unit
     * @param searchCommandDumpUnit 
     */
    public void setSearchCommandDumpUnit(double searchCommandDumpUnit) {
        this.searchCommandDumpUnit = searchCommandDumpUnit;
    }
    
    /**
     * Get Search command build unit
     * @return 
     */
    public double getSearchCommandbuildUnit() {
        return searchCommandbuildUnit;
    }

    /**
     * Set search command build unit
     * @param searchCommandbuildUnit 
     */
    public void setSearchCommandbuildUnit(double searchCommandbuildUnit) {
        this.searchCommandbuildUnit = searchCommandbuildUnit;
    }
    
    /**
     * Get search command process unit
     * @return 
     */
    public double getSearchCommandProcessUnit() {
        return searchCommandProcessUnit;
    }

    /**
     * Set search command process unit
     * @param searchCommandProcessUnit 
     */
    public void setSearchCommandProcessUnit(double searchCommandProcessUnit) {
        this.searchCommandProcessUnit = searchCommandProcessUnit;
    }
    
    /**
     * Get resume search parse resume unit
     * @return 
     */
    public double getSearchParseDocument() {
        return searchParseDocument;
    }

    /**
     * Set resume search parse resume unit
     * @param searchResumeParseCV 
     */
    public void setSearchParseDocument(double searchParseDocument) {
        this.searchParseDocument = searchParseDocument;
    }
    
    /**
     * Get facet dist command build unit
     * @return 
     */
    public double getFacetDistributionCommandBuildUnit() {
        return facetDistributionCommandBuildUnit;
    }

    /**
     * Set facet dist command build unit
     * @param facetdistributionCommandBuildUnit 
     */
    public void setFacetDistributionCommandBuildUnit(double facetdistributionCommandBuildUnit) {
        this.facetDistributionCommandBuildUnit = facetdistributionCommandBuildUnit;
    }
    
    /**
     * Get facet dist command process unit
     * @return 
     */
    public double getFacetDistributionCommandProcessUnit() {
        return facetDistributionCommandProcessUnit;
    }

    /**
     * Set facet command process unit
     * @param facetdistributionCommandProcessUnit 
     */
    public void setFacetDistributionCommandProcessUnit(double facetdistributionCommandProcessUnit) {
        this.facetDistributionCommandProcessUnit = facetdistributionCommandProcessUnit;
    }
    
    /**
     * Get register resume unit
     * @return 
     */
    public double getRegisterDocumentWithoutCustomFilterUnit() {
        return registerDocumentWithoutCustomFilterUnit;
    }

    /**
     * Set register resume unit
     * @param registerResumeUnit 
     */
    public void setRegisterDocumentWithoutCustomFilterUnit(double registerDocumentWithoutCustomFilterUnit) {
        this.registerDocumentWithoutCustomFilterUnit = registerDocumentWithoutCustomFilterUnit;
    }

    /**
     * Get tag binary data unit
     * @return 
     */
    public double getRegisterParseDocument() {
        return registerParseDocument;
    }

    /**
     * Set tag binary data unit
     * @param tagbinaryDataUnit 
     */
    public void setRegisterParseDocument(double registerParseDocument) {
        this.registerParseDocument = registerParseDocument;
    }

    /**
     * Get register document with custom filter unit
     * @return 
     */
    public double getRegisterDocumentWithCustomFilterUnit() {
        return registerDocumentWithCustomFilterUnit;
    }

    /**
     * Set register document with custom filter unit
     * @param registerDocumentWithCustimFilterUnit 
     */
    public void setRegisterDocumentWithCustomFilterUnit(double registerDocumentWithCustomFilterUnit) {
        this.registerDocumentWithCustomFilterUnit = registerDocumentWithCustomFilterUnit;
    }

    /**
     * Get unregister document unit
     * @return 
     */
    public double getUnregisterDocumentUnit() {
        return unregisterDocumentUnit;
    }

    /**
     * Set unregister document unit
     * @param unregisterDocumentUnit 
     */
    public void setUnregisterDocumentUnit(double unregisterDocumentUnit) {
        this.unregisterDocumentUnit = unregisterDocumentUnit;
    }

    /**
     * Get overwrite register document unit
     * @return 
     */
    public double getOverwriteRegisterDocumentUnit() {
        return overwriteRegisterDocumentUnit;
    }

    /**
     * set overwrite register document unit
     * @param overwriteRegisterDocumentUnit 
     */
    public void setOverwriteRegisterDocumentUnit(double overwriteRegisterDocumentUnit) {
        this.overwriteRegisterDocumentUnit = overwriteRegisterDocumentUnit;
    }
    
    /**
     * Get update registered document count
     * @return 
     */
    public double getUpdateRegisterDocumentCount() {
        return updateRegisterDocumentCount;
    }

    /**
     * Set update registered document count
     * @param updateRegisterDocumentCount 
     */
    public void setUpdateRegisterDocumentCount(double updateRegisterDocumentCount) {
        this.updateRegisterDocumentCount = updateRegisterDocumentCount;
    }

    /**
     * Get fetch resume process unit
     * @return 
     */
    public double getFetchDocumentProcessUnit() {
        return fetchDocumentProcessUnit;
    }

    /**
     * Set fetch resume process unit
     * @param fetchResumeProcessUnit 
     */
    public void setFetchDocumentProcessUnit(double fetchDocumentProcessUnit) {
        this.fetchDocumentProcessUnit = fetchDocumentProcessUnit;
    }
    
    public double getApiRateLimit() {
        return apiRateLimit;
    }

    public void setApiRateLimit(double apiRateLimit) {
        this.apiRateLimit = apiRateLimit;
    }

    public double getProductRateLimit() {
        return productRateLimit;
    }

    public void setProductRateLimit(double productRateLimit) {
        this.productRateLimit = productRateLimit;
    }
}
