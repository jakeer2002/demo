// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.api;

import com.bgt.lens.authentication.OAuthHeader;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.SearchRegisterlog;
import com.bgt.lens.model.rest.response.ApiResponse;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 *
 * API context
 */
public class ApiContext {

    /**
     * Request Id
     */
    protected UUID requestId;

    /**
     * Request URI
     */
    protected String requestUri;

    /**
     * HTTP method
     */
    protected String method;

    /**
     * Resource name
     */
    protected String resource;

    /**
     * Request in time
     */
    protected Date inTime;

    /**
     * Response out time
     */
    protected Date outTime;

    /**
     * Elapsed seconds
     */
    protected String seconds;

    /**
     * Request headers
     */
    protected String requestHeader;

    /**
     * Exception, if any issues
     */
    protected Exception exception;

    /**
     * Request data
     */
    protected Object requestContent;

    /**
     * OAuth header
     */
    protected OAuthHeader oAuthHeader;

    /**
     * Client details
     */
    protected CoreClient client;

    /**
     * Client API details
     */
    protected CoreClientapi clientApi;

    /**
     * API response
     */
    protected ApiResponse apiResponse;
    /**
     * API authenticationType
     */
    protected String authenticationType;

    /**
     * Response type
     */
    protected String accept;

    /**
     * Request type
     */
    protected String contentType;

    /**
     * Request type (REST or SOAP)
     */
    protected String requestType;

    /**
     * HTTP status code
     */
    protected int statusCode;

    /**
     * Trace log, if exception
     */
    protected String traceLog;

    /**
     * Advance Log - Profile methods execution time
     */
    protected AdvanceLog advanceLog;

    /**
     * Advance Log - Profile methods execution time
     */
    protected boolean isAdvanceLog;

    /**
     * Instance type
     */
    protected String instanceType;

    /**
     * Locale
     */
    protected String locale;

    /**
     * Parse variant
     */
    protected String parseVariant;

    /**
     * Return type for SOAP
     */
    protected String returnType;

    /**
     * Search command
     */
    protected String searchCommand;

    /**
     * Search type
     */
    protected List<String> filterType;

    /**
     * LENS settings Id
     */
    protected Integer lensSettingsId;

    /**
     * Vendor Id
     */
    protected Integer vendorId;

    /**
     * Search register log
     */
    protected SearchRegisterlog searchRegisterLog;

    /**
     * api
     */
    protected String api;

    /**
     * Document Type
     */
    protected String docType;

    /**
     * Server IP address
     */
    protected String serverAddress;

    /**
     * Client IP address
     */
    protected String clientAddress;

    protected String requestCount;

    protected String remainingRequestCount;
    
    protected long retryTimeStamp;


    /**
     * Get request Id
     *
     * @return
     */
    public UUID getRequestId() {
        return requestId;
    }

    /**
     * Set request Id
     *
     * @param requestId
     */
    public void setRequestId(UUID requestId) {
        this.requestId = requestId;
    }

    /**
     * Get request URI
     *
     * @return
     */
    public String getRequestUri() {
        return requestUri;
    }

    /**
     * Set request URI
     *
     * @param requestUri
     */
    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    /**
     * Get HTTP method
     *
     * @return
     */
    public String getMethod() {
        return method;
    }

    /**
     * Set HTTP method
     *
     * @param method
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Get resource
     *
     * @return
     */
    public String getResource() {
        return resource;
    }

    /**
     * Set resource
     *
     * @param resource
     */
    public void setResource(String resource) {
        this.resource = resource;
    }

    /**
     * Get request in time
     *
     * @return
     */
    public Date getInTime() {
        return inTime == null ? null : new Date(inTime.getTime());
    }

    /**
     * Set request in time
     *
     * @param inTime
     */
    public void setInTime(Date inTime) {
        this.inTime = inTime == null ? null : new Date(inTime.getTime());
    }

    /**
     * Get response out time
     *
     * @return
     */
    public Date getOutTime() {
        return outTime == null ? null : new Date(outTime.getTime());
    }

    /**
     * Set response out time
     *
     * @param outTime
     */
    public void setOutTime(Date outTime) {
        this.outTime = outTime == null ? null : new Date(outTime.getTime());
    }

    /**
     * Get elapsed time
     *
     * @return
     */
    public String getSeconds() {
        return seconds;
    }

    /**
     * Set elapsed time
     *
     * @param seconds
     */
    public void setSeconds(String seconds) {
        this.seconds = seconds;
    }

    /**
     * Get request header
     *
     * @return
     */
    public String getRequestHeader() {
        return requestHeader;
    }

    /**
     * Set request header
     *
     * @param requestHeader
     */
    public void setRequestHeader(String requestHeader) {
        this.requestHeader = requestHeader;
    }

    /**
     * Get exception
     *
     * @return
     */
    public Exception getException() {
        return exception;
    }

    /**
     * Set exception
     *
     * @param exception
     */
    public void setException(Exception exception) {
        this.exception = exception;
    }

    /**
     * Get request content
     *
     * @return
     */
    public Object getRequestContent() {
        return requestContent;
    }

    /**
     * Set request content
     *
     * @param requestContent
     */
    public void setRequestContent(Object requestContent) {
        this.requestContent = requestContent;
    }

    /**
     * Get OAuth header
     *
     * @return
     */
    public OAuthHeader getoAuthHeader() {
        return oAuthHeader;
    }

    /**
     * Set OAuth header
     *
     * @param oAuthHeader
     */
    public void setoAuthHeader(OAuthHeader oAuthHeader) {
        this.oAuthHeader = oAuthHeader;
    }

    /**
     * Get client details
     *
     * @return
     */
    public CoreClient getClient() {
        return client;
    }

    /**
     * Set client details
     *
     * @param client
     */
    public void setClient(CoreClient client) {
        this.client = client;
    }

    /**
     * Get client API details
     *
     * @return
     */
    public CoreClientapi getClientApi() {
        return clientApi;
    }

    /**
     * Set client API details
     *
     * @param clientApi
     */
    public void setClientApi(CoreClientapi clientApi) {
        this.clientApi = clientApi;
    }

    /**
     * Get ApiResponse
     *
     * @return
     */
    public ApiResponse getApiResponse() {
        return apiResponse;
    }

    /**
     * Set ApiResponse
     *
     * @param apiResponse
     */
    public void setApiResponse(ApiResponse apiResponse) {
        this.apiResponse = apiResponse;
    }

    /**
     * Get ApiResponse
     *
     * @return
     */
    public String getAuthenticationType() {
        return authenticationType;
    }

    /**
     * Set ApiResponse
     *
     * @param authenticationType
     */
    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    /**
     * Get Accept header
     *
     * @return
     */
    public String getAccept() {
        return accept;
    }

    /**
     * Set Accept header
     *
     * @param accept
     */
    public void setAccept(String accept) {
        this.accept = accept;
    }

    /**
     * Get Content Type header
     *
     * @return
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Set Content_Type header
     *
     * @param contentType
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * Get request type
     *
     * @return
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * Set request type
     *
     * @param requestType
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     * Get status code
     *
     * @return
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Set status code
     *
     * @param statusCode
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Get trace log
     *
     * @return
     */
    public String getTraceLog() {
        return traceLog;
    }

    /**
     * Set trace log
     *
     * @param traceLog
     */
    public void setTraceLog(String traceLog) {
        this.traceLog = traceLog;
    }

    /**
     * Get Advance Log
     *
     * @return
     */
    public AdvanceLog getAdvanceLog() {
        return advanceLog;
    }

    /**
     * Set Advance Log
     *
     * @param advanceLog
     */
    public void setAdvanceLog(AdvanceLog advanceLog) {
        this.advanceLog = advanceLog;
    }

    /**
     * Get Is Advance Log
     *
     * @return
     */
    public Boolean isAdvanceLog() {
        return isAdvanceLog;
    }

    /**
     * Set Is Advance Log
     *
     * @param isAdvanceLog
     */
    public void setIsAdvanceLog(Boolean isAdvanceLog) {
        this.isAdvanceLog = isAdvanceLog;
    }

    /**
     * Advanced log flag
     *
     * @return
     */
    public boolean isIsAdvanceLog() {
        return isAdvanceLog;
    }

    /**
     * Advanced log flag
     *
     * @param isAdvanceLog
     */
    public void setIsAdvanceLog(boolean isAdvanceLog) {
        this.isAdvanceLog = isAdvanceLog;
    }

    /**
     * Get instance Type
     *
     * @return
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     *
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get locale
     *
     * @return
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set locale
     *
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get parse variant
     *
     * @return
     */
    public String getParseVariant() {
        return parseVariant;
    }

    /**
     * Set parse variant
     *
     * @param parseVariant
     */
    public void setParseVariant(String parseVariant) {
        this.parseVariant = parseVariant;
    }

    /**
     * Get return type for SOAP
     *
     * @return
     */
    public String getReturnType() {
        return returnType;
    }

    /**
     * Set return type for SOAP
     *
     * @param returnType
     */
    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    /**
     * Get search command
     *
     * @return
     */
    public String getSearchCommand() {
        return searchCommand;
    }

    /**
     * Set search command
     *
     * @param searchCommand
     */
    public void setSearchCommand(String searchCommand) {
        this.searchCommand = searchCommand;
    }

    /**
     * Get LENS settings id
     *
     * @return
     */
    public Integer getLensSettingsId() {
        return lensSettingsId;
    }

    /**
     * Set LENS settings Id
     *
     * @param lensSettingsId
     */
    public void setLensSettingsId(Integer lensSettingsId) {
        this.lensSettingsId = lensSettingsId;
    }

    /**
     * Get Vendor Id
     *
     * @return
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * Set vendor Id
     *
     * @param vendorId
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * Get search register log
     *
     * @return
     */
    public SearchRegisterlog getSearchRegisterLog() {
        return searchRegisterLog;
    }

    /**
     * Set search register log
     *
     * @param searchRegisterLog
     */
    public void setSearchRegisterLog(SearchRegisterlog searchRegisterLog) {
        this.searchRegisterLog = searchRegisterLog;
    }

    /**
     * Get application API
     *
     * @return
     */
    public String getApi() {
        return api;
    }

    /**
     * Set Application API
     *
     * @param api
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * Get Filter Type
     *
     * @return
     */
    public List<String> getFilterType() {
        return filterType;
    }

    /**
     * Set Filter Type
     *
     * @param filterType
     */
    public void setFilterType(List<String> filterType) {
        this.filterType = filterType;
    }

    /**
     * Get Document Type
     *
     * @return
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Set Document Type
     *
     * @param docType
     */
    public void setDocType(String docType) {
        this.docType = docType;
    }

    /**
     * Get server address
     *
     * @return
     */
    public String getServerAddress() {
        return serverAddress;
    }

    /**
     * Set server address
     *
     * @param serverAddress
     */
    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    /**
     * Get client address
     *
     * @return
     */
    public String getClientAddress() {
        return clientAddress;
    }

    /**
     * Set client address
     *
     * @param clientAddress
     */
    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    /**
     * Get client requestCount
     *
     * @return
     */
    public String getRequestCount() {
        return requestCount;
    }

    /**
     * Set client requestCount
     *
     * @param requestCount
     */
    public void setRequestCount(String requestCount) {
        this.requestCount = requestCount;
    }

    /**
     * Get client remainingRequestCount
     *
     * @return
     */
    public String getRemainingRequestCount() {
        return remainingRequestCount;
    }

    /**
     * Set client remainingRequestCount
     *
     * @param remainingRequestCount
     */
    public void setRemainingRequestCount(String remainingRequestCount) {
        this.remainingRequestCount = remainingRequestCount;
    }

    /**
     * Get client retryTimeStamp
     *
     * @return
     */

    public long getRetryTimeStamp() {
        return retryTimeStamp;
    }

    /**
     * Set client retryTimeStamp
     *
     * @param retryTimeStamp
     */

    public void setRetryTimeStamp(long retryTimeStamp) {
        this.retryTimeStamp = retryTimeStamp;
    }
}
