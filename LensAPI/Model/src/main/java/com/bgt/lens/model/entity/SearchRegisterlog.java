// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * SearchRegisterlog
 */
@Entity
@Table(name = "search_registerlog")
public class SearchRegisterlog implements java.io.Serializable {

    /**
     * Unique registration Id
     */
    private Integer id;

    /**
     * Request Id
     */
    private String requestId;

    /**
     * Client Id
     */
    private int clientId;

    /**
     * Vendor Id
     */
    private int vendorId;

    /**
      * LENS settings Id
      */
     private int lensSettingsId;
     
    /**
     * Locale
     */
    private String locale;

    /**
     * Document Type
     */
    private String documentType;
        /**
     * Document Type
     */
    private boolean isParsedBinaryData;

    /**
     * Instance Type
     */
    private String instanceType;

    /**
     * Document Id
     */
    private String docId;

    /**
     * Register or unregister
     */
    private boolean registerOrUnregister;

    /**
     * Failover update status
     */
    private boolean failoverUpdateStatus;

    /**
     * OverWrite
     */
    private boolean overWrite;

    /**
     * Register binary data
     */
    private String binaryData;
    
    /**
     * Register document with custom filters
     */
    private boolean registerWithCustomFilters;

    /**
     * Created On
     */
    private Date createdOn;

    /**
     * Updated On
     */
    private Date updatedOn;

    public SearchRegisterlog() {
    }

    public SearchRegisterlog(String requestId, int clientId, int vendorId,int lensSettingsId, String docId, String locale, String documentType, boolean isParsedBinaryData, String instanceType, boolean registerOrUnregister, boolean failoverUpdateStatus, boolean overWrite, Date createdOn) {
        this.requestId = requestId;
        this.clientId = clientId;
        this.vendorId = vendorId;
        this.lensSettingsId = lensSettingsId;
        this.docId = docId;
        this.locale = locale;
        this.documentType = documentType;
        this.isParsedBinaryData = isParsedBinaryData;
        this.instanceType = instanceType;
        this.registerOrUnregister = registerOrUnregister;
        this.failoverUpdateStatus = failoverUpdateStatus;
        this.overWrite = overWrite;
        this.createdOn = createdOn;
    }

    public SearchRegisterlog(Integer id, String requestId, int clientId, int lensSettingsId, String locale, String documentType, String instanceType, int vendorId, String docId, boolean registerOrUnregister, boolean failoverUpdateStatus, boolean overWrite, Date createdOn, Date updatedOn) {
    	this.id = id;
    	this.requestId = requestId;
        this.clientId = clientId;
        this.vendorId = vendorId;
        this.lensSettingsId = lensSettingsId;        
        this.docId = docId;
        this.locale = locale;
        this.documentType = documentType;
        this.isParsedBinaryData = isParsedBinaryData;
        this.instanceType = instanceType;
        this.registerOrUnregister = registerOrUnregister;
        this.failoverUpdateStatus = failoverUpdateStatus;
        this.overWrite = overWrite;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
    }

    /**
     * Get Id
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    /**
     * Set Id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get request Id
     *
     * @return
     */
    @Column(name = "RequestId", nullable = false)
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set request Id
     *
     * @param requestId
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Get client Id
     *
     * @return
     */
    @Column(name = "ClientId", nullable = false)
    public int getClientId() {
        return this.clientId;
    }

    /**
     * Set client Id
     *
     * @param clientId
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * Get LENS settings Id
     *
     * @return
     */
    @Column(name = "LensSettingsId", nullable = false)
    public int getLensSettingsId() {
        return this.lensSettingsId;
    }

    /**
     * Set LENS settings Id
     *
     * @param lensSettingsId
     */
    public void setLensSettingsId(int lensSettingsId) {
        this.lensSettingsId = lensSettingsId;
    }

    /**
     * Get vendor Id
     *
     * @return
     */
    @Column(name = "VendorId", nullable = false)
    public int getVendorId() {
        return this.vendorId;
    }

    /**
     * Set vendor Id
     *
     * @param vendorId
     */
    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * Get doc Id
     *
     * @return
     */
    @Column(name = "DocId", nullable = false)
    public String getDocId() {
        return this.docId;
    }

    /**
     * Set document Id
     *
     * @param docId
     */
    public void setDocId(String docId) {
        this.docId = docId;
    }
    
      /**
     * Get parsed binary data status
     *
     * @return
     */
    @Column(name = "IsParsedBinaryData", nullable = false)
    public boolean getIsParsedBinaryData() {
        return this.isParsedBinaryData;
    }

    /**
     * Set parsed binary data status
     *
     * @param isParsedBinaryData
     */
    public void setIsParsedBinaryData(boolean isParsedBinaryData) {
        this.isParsedBinaryData = isParsedBinaryData;
    }

    /**
     * get register or unregister status
     *
     * @return
     */
    @Column(name = "RegisterOrUnregister", nullable = false)
    public boolean getRegisterOrUnregister() {
        return this.registerOrUnregister;
    }

    /**
     * Set register or unregister flag
     *
     * @param registerOrUnregister
     */
    public void setRegisterOrUnregister(boolean registerOrUnregister) {
        this.registerOrUnregister = registerOrUnregister;
    }

    /**
     * Get failover update status
     *
     * @return
     */
    @Column(name = "FailoverUpdateStatus", nullable = false)
    public boolean getFailoverUpdateStatus() {
        return this.failoverUpdateStatus;
    }

    /**
     * Set failover update status
     *
     * @param failoverUpdateStatus
     */
    public void setFailoverUpdateStatus(boolean failoverUpdateStatus) {
        this.failoverUpdateStatus = failoverUpdateStatus;
    }

    /**
     * Get over write status
     *
     * @return
     */
    @Column(name = "OverWrite", nullable = false)
    public boolean getOverWrite() {
        return this.overWrite;
    }

    /**
     * Set over write status
     *
     * @param overWrite
     */
    public void setOverWrite(boolean overWrite) {
        this.overWrite = overWrite;
    }
    
    /**
     * Get over write status
     *
     * @return
     */
    @Column(name = "BinaryData", nullable = false)
    public String getBinaryData() {
        return this.binaryData;
    }

    /**
     * Set over write status
     *
     * @param binaryData
     */
    public void setBinaryData(String binaryData) {
        this.binaryData = binaryData;
    }
    
    /**
     * Get over write status
     *
     * @return
     */
    @Column(name = "RegisterWithCustomFilters", nullable = false)
    public boolean getRegisterWithCustomFilters() {
        return this.registerWithCustomFilters;
    }

    /**
     * Set over write status
     *
     * @param registerWithCustomFilters
     */
    public void setRegisterWithCustomFilters(boolean registerWithCustomFilters) {
        this.registerWithCustomFilters = registerWithCustomFilters;
    }

    /**
     * Get created on
     *
     * @return
     */
    @Column(name = "CreatedOn", nullable = false)
    public Date getCreatedOn() {
        return this.createdOn;
    }

    /**
     * Set created on
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Get updated on
     *
     * @return
     */
    @Column(name = "UpdatedOn", nullable = true)
    public Date getUpdatedOn() {
        return this.updatedOn;
    }

    /**
     * Set updated on
     *
     * @param updatedOn
     */
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * Get Locale
     * @return
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set Locale
     * 
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get Document Type
     *
     * @return
     */
    public String getDocumentType() {
        return documentType;
    }

    /**
     * Set Document Type
     *
     * @param documentType
     */
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    /**
     * Get Instance Type
     *
     * @return
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set Instance Type
     *
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }
    
    
}
