// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.helpers;

import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Enum.InstanceType;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.ClientApi;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.InstanceList;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.adminservice.request.UpdateApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateResourceRequest;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.CanonRequest;
import com.bgt.lens.model.parserservice.request.ConvertRequest;
import com.bgt.lens.model.parserservice.request.ParseJobRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.parserservice.request.ProcessLocaleRequest;
import com.bgt.lens.model.rest.response.FilterSettings;
import com.bgt.lens.model.rest.response.Instance;
import java.io.StringReader;
import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.joda.time.LocalDate;
import org.springframework.http.HttpStatus;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 *
 * Input request validator 12 Jan 2016 - PIG : Added NA in Validate LENS
 * settings
 */
public class Validator {

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Core mapper object
     */
    private final CoreMappers _coreMapper = new CoreMappers();

    private final int internal = 1;
    private final int external = 2;
    
    private boolean enableKeywordSearchValidation;

    public Validator() {
    }
    
    public Validator(boolean enableKeywordSearchValidationValue) {
        enableKeywordSearchValidation = enableKeywordSearchValidationValue;
    }
    
    
    /**
     * Validate Create API details
     *
     * @param api
     * @return
     * @throws Exception
     */
    public boolean validateApi(CreateApiRequest api) throws Exception {
        if (_helper.isNotNull(api)) {

            // Validate API Key
            if (_helper.isNotNullOrEmpty(api.getKey())) {
                if (!_helper.isAlphaNumeric(api.getKey())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY), HttpStatus.BAD_REQUEST);
            }

            // Validate API Name
            if (_helper.isNotNullOrEmpty(api.getName())) {
                if (!_helper.isAlphaNumericWithSomeSpecialCharacter(api.getName())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_NAME), HttpStatus.BAD_REQUEST);
                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_NAME), HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY_NAME), HttpStatus.BAD_REQUEST);
        }
        return true;
    }

    /**
     * Validate Update API request
     *
     * @param api
     * @return
     * @throws Exception
     */
    public boolean validateApi(UpdateApiRequest api) throws Exception {
        if (_helper.isNotNull(api)) {
            if (_helper.isNotNullOrEmpty(api.getKey()) || _helper.isNotNullOrEmpty(api.getName())) {
                if (_helper.isNotNullOrEmpty(api.getKey())) {
                    if (!_helper.isAlphaNumeric(api.getKey())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                    }
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY), HttpStatus.BAD_REQUEST);
                }

                if (_helper.isNotNullOrEmpty(api.getName())) {
                    if (!_helper.isAlphaNumericWithSomeSpecialCharacter(api.getName())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_NAME), HttpStatus.BAD_REQUEST);
                    }
                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY_NAME), HttpStatus.BAD_REQUEST);
            }

        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY_NAME), HttpStatus.BAD_REQUEST);
        }
        return true;
    }

    /**
     * Validate Add resource request
     *
     * @param resource
     * @return
     * @throws Exception
     */
    public boolean validateResource(AddResourceRequest resource) throws Exception {
        if (_helper.isNotNull(resource)) {
            if (_helper.isNullOrEmpty(resource.getResourceName())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_RESOURCE_NAME), HttpStatus.BAD_REQUEST);
            } else if (!_helper.isAlphaNumeric(resource.getResourceName())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_NAME), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNullOrEmpty(resource.getApiKey())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY), HttpStatus.BAD_REQUEST);
            }

        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_RESOURCE), HttpStatus.BAD_REQUEST);
        }
        return true;
    }

    /**
     * Validate update resource request
     *
     * @param resource
     * @return
     * @throws Exception
     */
    public boolean validateResource(UpdateResourceRequest resource) throws Exception {
        if (_helper.isNotNull(resource)) {
            if (_helper.isNotNullOrEmpty(resource.getResourceName()) || _helper.isNotNullOrEmpty(resource.getApiKey())) {
                if (_helper.isNotNullOrEmpty(resource.getResourceName())) {
                    if (!_helper.isAlphaNumeric(resource.getResourceName())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_NAME), HttpStatus.BAD_REQUEST);
                    }
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_RESOURCE_NAME), HttpStatus.BAD_REQUEST);
                }

            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_RESOURCE_NAME), HttpStatus.BAD_REQUEST);
            }

        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_RESOURCE_NAME), HttpStatus.BAD_REQUEST);
        }
        return true;
    }

    /**
     * Validate Add Lens settings request
     *
     * @param settings
     * @return
     * @throws Exception
     */
    public boolean validateLensSettings(AddLensSettingsRequest settings) throws Exception {
        if (_helper.isNotNull(settings)) {
            if (_helper.isNullOrEmpty(settings.getHost())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_HOST), HttpStatus.BAD_REQUEST);
            }

            if (!_helper.isValidHostName(settings.getHost())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_LENS_HOST), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNull(settings.getPort()) || settings.getPort().intValue() <= 0) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_LENS_PORT), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNotNullOrEmpty(settings.getInstanceType()) && _helper.isValidParsingInstanceType(settings.getInstanceType())) {
                if (!_helper.isNullOrEmpty(settings.getLocale())) {
                    if (!_helper.isValidLocale(settings.getLocale())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_LENS_LOCALE), HttpStatus.BAD_REQUEST);
                    }

                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_LOCALE), HttpStatus.BAD_REQUEST);
                }
            }

            if (!_helper.isNullOrEmpty(settings.getLocale())) {
                if (!_helper.isValidLocale(settings.getLocale())) {
                    if (!"NA".equalsIgnoreCase(settings.getLocale())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_LENS_LOCALE), HttpStatus.BAD_REQUEST);
                    }
                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_LOCALE), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNullOrEmpty(settings.getVersion())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_VERSION), HttpStatus.BAD_REQUEST);
            }

            // Verfied lens settings format - PIG
            if (!_helper.isValidLensVersion(settings.getVersion())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_LENS_VERSION), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNotNull(settings.getFailoverLensSettings()) && _helper.isNotNull(settings.getFailoverLensSettings().getFailoverLensSettingsIdList()) && settings.getFailoverLensSettings().getFailoverLensSettingsIdList().size() > 0) {
                Set<BigInteger> failoverInstanceSet = new HashSet<>();
                failoverInstanceSet.addAll(settings.getFailoverLensSettings().getFailoverLensSettingsIdList());

                if (failoverInstanceSet.size() != settings.getFailoverLensSettings().getFailoverLensSettingsIdList().size()) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_FAILOVER_ID_LIST), HttpStatus.BAD_REQUEST);
                }
                //TODO: Check the failover id is valid - PIG
            }            

        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_SETTINGS), HttpStatus.BAD_REQUEST);
        }
        return true;
    }

    /**
     * Validate create consumer request
     *
     * @param consumer
     * @param apiConfig
     * @return
     * @throws Exception
     */
    public boolean validateConsumer(CreateConsumerRequest consumer, ApiConfiguration apiConfig) throws Exception {
        if (_helper.isNotNull(consumer)) {
            if (_helper.isNullOrEmpty(consumer.getKey())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
            }
            if (_helper.isNullOrEmpty(consumer.getSecret())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_SECRET), HttpStatus.BAD_REQUEST);
            }
            // Added TIER Validation - PIG
            if (_helper.isNotNull(consumer.getTier()) && consumer.getTier().intValue() != internal && consumer.getTier().intValue() != external) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TIER), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNotNullOrEmpty(consumer.getKey()) && _helper.isNotNullOrEmpty(consumer.getSecret())) {

                if (!(_helper.isAlphaNumeric(consumer.getKey()))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
                }

                if (!(_helper.isValidSecret(consumer.getSecret()))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CONSUMER_SECRET), HttpStatus.BAD_REQUEST);
                }

                if (!_helper.isAlphaNumericWithSomeSpecialCharacter(consumer.getName())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CONSUMER_NAME), HttpStatus.BAD_REQUEST);
                }
                if (!"BASIC".equalsIgnoreCase(consumer.getAuthenticationType()) && !"OAUTH".equalsIgnoreCase(consumer.getAuthenticationType())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_AUTHENTICATION_TYPE), HttpStatus.BAD_REQUEST);
                }

                if (_helper.isNotNull(consumer.getClientApiList()) && consumer.getClientApiList().getClientApi().size() > 0) {

                    List<ClientApi> apiList = consumer.getClientApiList().getClientApi();

                    consumer.getClientApiList().getClientApi().forEach(api -> {

                        if (_helper.isNull(api.getApiId())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_ID), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNullOrEmpty(api.getApiKey())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY), HttpStatus.BAD_REQUEST);
                        }

                        if (_helper.isNotNull(api.getApiId()) && _helper.isNotNullOrEmpty(api.getApiKey())) {

                            if (!_helper.isAlphaNumeric(api.getApiKey())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                            }

                            if (_helper.isNotNullOrEmpty(api.getSecret())) {
                                if (!(_helper.isAlphaNumeric(api.getApiKey()))) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                                }
                                if (!(_helper.isValidSecret(api.getSecret()))) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_SECRET), HttpStatus.BAD_REQUEST);
                                }
                            }

                            if (_helper.isNullOrEmpty(api.getApiKey())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY), HttpStatus.BAD_REQUEST);
                            }
                            if (_helper.isNullOrEmpty(api.getSecret())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_SECRET), HttpStatus.BAD_REQUEST);
                            }

                            if (!(_helper.isNotNull(api.getLicensedResources()) && _helper.isNotNull(api.getLicensedResources().getResourceList()) && api.getLicensedResources().getResourceList().size() > 0)) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_LIST), HttpStatus.BAD_REQUEST);
                            }

                            if (_helper.isNotNullOrEmpty(api.getActivationDate())) {
                                if (_helper.isNotNullOrEmpty(api.getExpiryDate())) {
                                    Date today = new LocalDate().toDateTimeAtStartOfDay().toDate();
                                    Date activationDate = null;
                                    Date expiryDate = null;

                                    try {
                                        activationDate = LocalDate.parse(api.getActivationDate()).toDateTimeAtStartOfDay().toDate();
                                    } catch (Exception ex) {
                                        if (ex.getClass().equals(IllegalArgumentException.class)) {
                                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
                                        } else {
                                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                                        }
                                    }

                                    try {
                                        expiryDate = LocalDate.parse(api.getExpiryDate()).toDateTimeAtStartOfDay().toDate();
                                    } catch (Exception ex) {
                                        if (ex.getClass().equals(IllegalArgumentException.class)) {
                                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
                                        } else {
                                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                                        }
                                    }

                                    if (!activationDate.before(today)) {
                                        if (expiryDate.before(activationDate)) {
                                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                                        } else if (_helper.isNotNull(api.getAllowedTransactions())) {
                                            if (api.getAllowedTransactions().intValue() < 0) {
                                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                                            }
                                        }
                                    } else {
                                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                                    }

                                } else {
                                    Date today = new LocalDate().toDateTimeAtStartOfDay().toDate();
                                    Date activationDate = null;
                                    try {
                                        activationDate = LocalDate.parse(api.getActivationDate()).toDateTimeAtStartOfDay().toDate();
                                    } catch (Exception ex) {
                                        if (ex.getClass().equals(IllegalArgumentException.class)) {
                                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
                                        } else {
                                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                                        }
                                    }
                                    if (activationDate.before(today)) {
                                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                                    }
                                }
                                if (_helper.isNotNull(api.getAllowedTransactions())) {
                                    if (api.getAllowedTransactions().intValue() < 0) {
                                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                                    }
                                }
                            } else if (_helper.isNotNullOrEmpty(api.getExpiryDate())) {
                                Date today = new LocalDate().toDateTimeAtStartOfDay().toDate();

                                Date activationDate = today;
                                Date expiryDate = Date.from(Instant.now());
                                try {
                                    expiryDate = LocalDate.parse(api.getExpiryDate()).toDateTimeAtStartOfDay().toDate();
                                } catch (Exception ex) {
                                    if (ex.getClass().equals(IllegalArgumentException.class)) {
                                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
                                    } else {
                                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                                    }
                                }

                                if (!activationDate.before(today)) {
                                    if (expiryDate.before(activationDate)) {
                                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                                    } else if (_helper.isNotNull(api.getAllowedTransactions())) {
                                        if (api.getAllowedTransactions().intValue() < 0) {
                                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                                        }
                                    }
                                } else {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                                }
                            } else if (_helper.isNotNull(api.getAllowedTransactions())) {
                                if (api.getAllowedTransactions().intValue() < 0) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                                }
                            }
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY_SECRET), HttpStatus.BAD_REQUEST);
                        }
                    });

                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CLIENT_API), HttpStatus.BAD_REQUEST);
                }

                if (!(_helper.isNotNull(consumer.getLensSettings()) && _helper.isNotNull(consumer.getLensSettings().getInstanceList()) && consumer.getLensSettings().getInstanceList().size() > 0)) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_SETTINGS), HttpStatus.BAD_REQUEST);
                }

                if ((_helper.isNotNull(consumer.getLensSettings()) && _helper.isNotNull(consumer.getLensSettings().getInstanceList()) && consumer.getLensSettings().getInstanceList().size() > 0)) {

                    consumer.getLensSettings().getInstanceList().forEach(instance -> {

                        if (!_helper.isNotNull(instance.getInstanceId())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_SETTINGS), HttpStatus.BAD_REQUEST);
                        }

                        if (consumer.isRateLimitEnabled()) {
                            if (!_helper.isNotNull(consumer.getRateLimitTimePeriod())) {
                                // Throw error
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_USER_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                            } else if (!_helper.isValidInteger(consumer.getRateLimitTimePeriod().longValue())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_USER_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                            }

                            if (!_helper.isNotNull(consumer.getRateLimitCount())) {
                                // Throw error
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_USER_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);
                            } else if (!_helper.isValidInteger(consumer.getRateLimitCount().longValue())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_USER_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);

                            }
                        }

                        if (instance.isRateLimitEnabled()) {
                            if (!_helper.isNotNull(instance.getRateLimitTimePeriod())) {
                                // Throw error
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                            } else if (!_helper.isValidInteger(instance.getRateLimitTimePeriod().longValue())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                            }
                            if (!_helper.isNotNull(instance.getRateLimitCount())) {
                                // Throw error
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);
                            } else if (!_helper.isValidInteger(instance.getRateLimitCount().longValue())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);

                            }
                        }             
                    });
                }
                // Search settings validation
                // Added validation for max search count - PIG
                if (_helper.isNotNull(consumer.getVendorSettings()) && _helper.isNotNull(consumer.getVendorSettings().getVendorIdList()) && consumer.getVendorSettings().getVendorIdList().size() > 0) {
                    if (_helper.isNotNull(consumer.getSearchSettings())) {

                        if (_helper.isNotNull(consumer.getSearchSettings().getMaxSearchCount()) && (!_helper.isValidInteger(consumer.getSearchSettings().getMaxSearchCount()))) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_MAX_SEARCHCOUNT), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNotNull(consumer.getSearchSettings().getMaxSearchCount()) && Integer.valueOf(consumer.getSearchSettings().getMaxSearchCount().toString()) <= 0) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_MAX_SEARCHCOUNT), HttpStatus.BAD_REQUEST);
                        }
                        String resumeVendorId = "";
                        String postingVendorId = "";
                        if (_helper.isNotNull(consumer.getSearchSettings().getResume()) && consumer.getSearchSettings().getResume().size() > 0) {

                            for (SearchSettings.Resume resume : consumer.getSearchSettings().getResume()) {
                                if (_helper.isNull(resume.getInstanceId())) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                                }
                                resumeVendorId = resume.getVendorId().toString();
                                if (_helper.isNull(resume.getVendorId())) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_OR_INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                                }

//                                if (!consumer.getLensSettings().getInstanceList().contains(resume.getInstanceId())) {
//                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
//                                }
                                boolean isAvailable = false;

                                isAvailable = consumer.getLensSettings().getInstanceList().stream().anyMatch(ils -> ils.getInstanceId().equals(resume.getInstanceId()));

                                if (!isAvailable) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                                }

                                if (!consumer.getVendorSettings().getVendorIdList().contains(resume.getVendorId())) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                                }
                            }
                        }
                        if (_helper.isNotNull(consumer.getSearchSettings().getPosting()) && consumer.getSearchSettings().getPosting().size() > 0) {
                            for (SearchSettings.Posting posting : consumer.getSearchSettings().getPosting()) {
                                if (_helper.isNull(posting.getInstanceId())) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                                }
                                postingVendorId = posting.getVendorId().toString();
                                if (_helper.isNull(posting.getVendorId())) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_OR_INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                                }
//                                if (!consumer.getLensSettings().getInstanceList().contains(posting.getInstanceId())) {
//                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
//                                }

                                boolean isAvailable = false;

                                isAvailable = consumer.getLensSettings().getInstanceList().stream().anyMatch(ils -> ils.getInstanceId().equals(posting.getInstanceId()));

                                if (!isAvailable) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                                }

                                if (!consumer.getVendorSettings().getVendorIdList().contains(posting.getVendorId())) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                                }
                            }
                        }
                        if (!resumeVendorId.equals("") && !postingVendorId.equals("")) {
                            if (resumeVendorId.equals(postingVendorId)) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.SAME_VENDOR_ID_ERROR), HttpStatus.BAD_REQUEST);

                            }
                        }
                        // Added validation for search settings
                        if (_helper.isNotNull(consumer.getSearchSettings())) {
                            if (consumer.getSearchSettings().getResume().isEmpty() && consumer.getSearchSettings().getPosting().isEmpty()) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_SEARCH_SETTINGS), HttpStatus.BAD_REQUEST);
                            }
                        }
                    }
                }

                // Check API RateLimit Details
                if (consumer.isRateLimitEnabled()) {
                    if (_helper.isNotNull(consumer.getRateLimitTimePeriod())) {
                        if (!_helper.isNotNull(consumer.getRateLimitCount())) {
                            // throw error
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);
                        }else if (!_helper.isValidInteger(consumer.getRateLimitCount().longValue())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_USER_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);

                            }
                    } else {
                        // throw error
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                    }
                }

            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
        }

        return true;
    }

    /**
     * Validate client API
     *
     * @param apiList
     * @param apiConfig
     * @return
     * @throws Exception
     */
    /**
     * Validate client API
     *
     * @param apiList
     * @param apiConfig
     * @param oldApiDetails
     * @return
     * @throws Exception
     */
    public boolean validateUpdateClientApi(List<ClientApi> apiList, ApiConfiguration apiConfig, List<CoreClientapi> oldApiDetails) throws Exception {

        apiList.forEach(api -> {

            if (_helper.isNotNull(api.getApiId()) && _helper.isNotNullOrEmpty(api.getApiKey())) {

                if (!_helper.isAlphaNumeric(api.getApiKey())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }

                if (_helper.isNotNullOrEmpty(api.getSecret())) {
                    if (!(_helper.isAlphaNumeric(api.getApiKey()) && _helper.isValidSecret(api.getSecret()))) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY_SECRET), HttpStatus.BAD_REQUEST);
                    }
                }

                if (_helper.isNull(api.getApiKey()) && _helper.isEmpty(api.getSecret())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY_SECRET), HttpStatus.BAD_REQUEST);
                }

                if (!(_helper.isNotNull(api.getLicensedResources()) && _helper.isNotNull(api.getLicensedResources().getResourceList()) && api.getLicensedResources().getResourceList().size() > 0)) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_LIST), HttpStatus.BAD_REQUEST);
                }

                if (_helper.isNotNullOrEmpty(api.getActivationDate())) {
                    if (_helper.isNotNullOrEmpty(api.getExpiryDate())) {
                        Date today = new LocalDate().toDateTimeAtStartOfDay().toDate();
                        Date activationDate = null;
                        Date expiryDate = null;

                        try {
                            activationDate = LocalDate.parse(api.getActivationDate()).toDateTimeAtStartOfDay().toDate();
                        } catch (Exception ex) {
                            if (ex.getClass().equals(IllegalArgumentException.class)) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
                            } else {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                            }
                        }

                        try {
                            expiryDate = LocalDate.parse(api.getExpiryDate()).toDateTimeAtStartOfDay().toDate();
                        } catch (Exception ex) {
                            if (ex.getClass().equals(IllegalArgumentException.class)) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
                            } else {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                            }
                        }

                        if (!activationDate.before(today)) {
                            if (expiryDate.before(activationDate)) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                            } else if (_helper.isNotNull(api.getAllowedTransactions())) {
//                                if (api.getAllowedTransactions().intValue() == 0) {
//                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
//                                }
                                
                                oldApiDetails.forEach(oldApi->{
                                    if (api.getApiKey().equals(oldApi.getCoreApi().getApiKey())) {
                                        if ((oldApi.getAllowedTransactions() + api.getAllowedTransactions().intValue() < oldApi.getRemainingTransactions() + api.getAllowedTransactions().intValue()) || (oldApi.getAllowedTransactions() + api.getAllowedTransactions().intValue() < 0) || api.getAllowedTransactions().intValue() == 0 || (oldApi.getRemainingTransactions() + api.getAllowedTransactions().intValue() < 0)) {
                                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                                        }
                                    }
                                });
                            }
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                        }

                    } else {
                        Date today = new LocalDate().toDateTimeAtStartOfDay().toDate();
                        Date activationDate = null;
                        try {
                            activationDate = LocalDate.parse(api.getActivationDate()).toDateTimeAtStartOfDay().toDate();
                        } catch (Exception ex) {
                            if (ex.getClass().equals(IllegalArgumentException.class)) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
                            } else {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                            }
                        }
                        if (activationDate.before(today)) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                        }
                    }

                    if (_helper.isNotNull(api.getAllowedTransactions())) {
                        
                        oldApiDetails.forEach(oldApi->{                       
                            if (api.getApiKey().equals(oldApi.getCoreApi().getApiKey())) {
                                if ((oldApi.getAllowedTransactions() + api.getAllowedTransactions().intValue() < oldApi.getRemainingTransactions() + api.getAllowedTransactions().intValue()) || (oldApi.getAllowedTransactions() + api.getAllowedTransactions().intValue() < 0) || api.getAllowedTransactions().intValue() == 0 || (oldApi.getRemainingTransactions() + api.getAllowedTransactions().intValue() < 0)) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                                }
                            }
                        });
                    }
                } else if (_helper.isNotNullOrEmpty(api.getExpiryDate())) {
                    Date today = new LocalDate().toDateTimeAtStartOfDay().toDate();

                    Date activationDate = today;
                    Date expiryDate = Date.from(Instant.now());
                    try {
                        expiryDate = LocalDate.parse(api.getExpiryDate()).toDateTimeAtStartOfDay().toDate();
                    } catch (Exception ex) {
                        if (ex.getClass().equals(IllegalArgumentException.class)) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
                        } else {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                        }
                    }

                    if (!activationDate.before(today)) {
                        if (expiryDate.before(activationDate)) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                        } else if (_helper.isNotNull(api.getAllowedTransactions())) {

                            oldApiDetails.forEach(oldApi->{
                                if (api.getApiKey().equals(oldApi.getCoreApi().getApiKey())) {
                                    if ((oldApi.getAllowedTransactions() + api.getAllowedTransactions().intValue() < oldApi.getRemainingTransactions() + api.getAllowedTransactions().intValue()) || (oldApi.getAllowedTransactions() + api.getAllowedTransactions().intValue() < 0) || (oldApi.getRemainingTransactions() + api.getAllowedTransactions().intValue() <= 0)) {
                                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                                    }
                                }

                            });
                        }
                    } else {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ACTIVATION_DATE), HttpStatus.BAD_REQUEST);
                    }
                } else if (_helper.isNotNull(api.getAllowedTransactions())) {

                    oldApiDetails.forEach(oldApi->{
                        if (api.getApiKey().equals(oldApi.getCoreApi().getApiKey())) {
                            if ((oldApi.getAllowedTransactions() + api.getAllowedTransactions().intValue() < oldApi.getRemainingTransactions() + api.getAllowedTransactions().intValue()) || (oldApi.getAllowedTransactions() + api.getAllowedTransactions().intValue() < 0) || api.getAllowedTransactions().intValue() == 0 || (oldApi.getRemainingTransactions() + api.getAllowedTransactions().intValue() < 0)) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                            }
                        }
                    });

                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY_SECRET), HttpStatus.BAD_REQUEST);
            }
        });

        return true;
    }

    /**
     * Validate convert request
     *
     * @param convertRequest
     * @return
     * @throws Exception
     */
    public boolean validateConvertRequest(ConvertRequest convertRequest) throws Exception {
        if (_helper.isNotNull(convertRequest)) {
            if (_helper.isNullOrEmpty(convertRequest.getBinaryData())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA), HttpStatus.BAD_REQUEST);
            }
            if (_helper.isNullOrEmpty(convertRequest.getInstanceType())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_REQUEST_DATA), HttpStatus.BAD_REQUEST);
        }
        return true;
    }

    /**
     * Validate canon request
     *
     * @param canonRequest
     * @return
     * @throws Exception
     */
    public boolean validateCanonRequest(CanonRequest canonRequest) throws Exception {
        if (_helper.isNotNull(canonRequest)) {
            if (_helper.isNullOrEmpty(canonRequest.getBinaryData())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA), HttpStatus.BAD_REQUEST);
            }
            if (_helper.isNullOrEmpty(canonRequest.getInstanceType())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_REQUEST_DATA), HttpStatus.BAD_REQUEST);
        }
        return true;
    }

    /**
     * Validate canon request
     *
     * @param processLocaleRequest
     * @return
     * @throws Exception
     */
    public boolean validateProcessLocaleRequest(ProcessLocaleRequest processLocaleRequest) throws Exception {
        if (_helper.isNotNull(processLocaleRequest)) {
            if (_helper.isNullOrEmpty(processLocaleRequest.getBinaryData())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA), HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_REQUEST_DATA), HttpStatus.BAD_REQUEST);
        }
        return true;
    }

    /**
     * Validate parse resume request
     *
     * @param parseResumeRequest
     * @return
     * @throws Exception
     */
    public boolean validateParseResumeRequest(ParseResumeRequest parseResumeRequest) throws Exception {
        if (_helper.isNotNull(parseResumeRequest)) {
            if (_helper.isNullOrEmpty(parseResumeRequest.getBinaryData())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA), HttpStatus.BAD_REQUEST);
            }
            if (_helper.isNullOrEmpty(parseResumeRequest.getInstanceType())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
            if (!_helper.isNull(parseResumeRequest.getFieldList())) {
                if (!_helper.isValidResumeFieldList(parseResumeRequest.getFieldList().getField())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESUME_FIELDLIST), HttpStatus.BAD_REQUEST);
                }
            }
        } else {
            throw new Exception("Convert Request details are empty");
        }
        return true;
    }

    /**
     * Validate parse job request
     *
     * @param parseJobRequest
     * @return
     * @throws Exception
     */
    public boolean validateParseJobRequest(ParseJobRequest parseJobRequest) throws Exception {
        if (_helper.isNotNull(parseJobRequest)) {
            if (_helper.isNullOrEmpty(parseJobRequest.getBinaryData())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA), HttpStatus.BAD_REQUEST);
            }
            if (_helper.isNullOrEmpty(parseJobRequest.getInstanceType())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
            if (!_helper.isNull(parseJobRequest.getFieldList())) {
                if (!_helper.isValidResumeFieldList(parseJobRequest.getFieldList().getField())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_JOB_FIELDLIST), HttpStatus.BAD_REQUEST);
                }
            }
        } else {
            throw new Exception("Convert Request details are empty");
        }
        return true;
    }

    /**
     * Validate XML command
     *
     * @param xml
     * @return
     * @throws Exception
     */
    public String validatedCommand(String xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        Document doc = builder.parse(is);
        String command = _coreMapper.getOuterXml(doc.getChildNodes());
        return command;
    }
}
