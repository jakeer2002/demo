// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Register Log
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class RegisterLog {
    /**
     * Id
     */
    @XmlElement(name = "Id", required = false)
    protected Integer id;
    
    /**
     * Request Id
     */
    @XmlElement(name = "RequestId", required = false)
    protected String requestId;
    
    /**
     * Client Id
     */
    @XmlElement(name = "ClientId", required = false)
    protected Integer clientId;
     
    /**
     * LENS settings Id
     */
    @XmlElement(name = "LensSettingsId", required = false)
    protected Integer lensSettingsId;
    
    /**
     * Vendor Id
     */
    @XmlElement(name = "VendorId", required = false)
    protected Integer vendorId;
    
    /**
     * Locale
     */
    @XmlElement(name = "Locale", required = false)
    protected String locale;
    
     /**
     * Document Type
     */
    @XmlElement(name = "DocumentType", required = false)
    protected String documentType;
    
        
     /**
     * Instance Type
     */
    @XmlElement(name = "InstanceType", required = false)
    protected String instanceType;   
    
    
    /**
     * Doc Id
     */
    @XmlElement(name = "DocId", required = false)
    protected String docId;
    
    /**
     * Register or Unregister
     */
    @XmlElement(name = "RegisterOrUnregister", required = false)
    protected String registerOrUnregister;
    
    /**
     * Failover update status
     */
    @XmlElement(name = "FailoverUpdateStatus", required = false)
    protected Boolean failoverUpdateStatus;
    
     /**
     * Over write status
     */
    @XmlElement(name = "OverWrite", required = false)
    protected Boolean overWrite;
    
    /**
     * Created On
     */
    @XmlElement(name = "CreatedOn", required = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    protected Date createdOn;
    
    /**
     * Updated On
     */
    @XmlElement(name = "UpdatedOn", required = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    protected Date updatedOn;
       

    /**
     * Get Id
     * @return 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
     /**
     * Get Request Id
     * @return 
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set Request Id
     * @param requestId 
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Get client Id
     * @return 
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client Id
     * @param clientId 
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get LENS settings Id
     * @return 
     */
    public Integer getLensSettingsId() {
        return lensSettingsId;
    }

    /**
     * Set LENS settings Id
     * @param lensSettingsId 
     */
    public void setLensSettingsId(Integer lensSettingsId) {
        this.lensSettingsId = lensSettingsId;
    }

    /**
     * Get vendor Id
     * @return 
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * Set Vendor Id
     * @param vendorId 
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * Get doc Id
     * @return 
     */
    public String getDocId() {
        return docId;
    }

    /**
     * Set Doc Id
     * @param docId 
     */
    public void setDocId(String docId) {
        this.docId = docId;
    }

    /**
     * Get register or unregister
     * @return 
     */
    public String getRegisterOrUnregister() {
        return registerOrUnregister;
    }

    /**
     * Set register or unregister
     * @param registerOrUnregister 
     */
    public void setRegisterOrUnregister(String registerOrUnregister) {
        this.registerOrUnregister = registerOrUnregister;
    }

    /**
     * Get failover update status
     * @return 
     */
    public Boolean isFailoverUpdateStatus() {
        return failoverUpdateStatus;
    }

    /**
     * Set failover update status
     * @param failoverUpdateStatus 
     */
    public void setFailoverUpdateStatus(Boolean failoverUpdateStatus) {
        this.failoverUpdateStatus = failoverUpdateStatus;
    }
    
    /**
     * Get over write status
     * @return 
     */
    public Boolean isOverWrite() {
        return overWrite;
    }

    /**
     * Set over write status
     * @param overWrite 
     */
    public void setOverWrite(Boolean overWrite) {
        this.overWrite = overWrite;
    }

    /**
     * Get created on
     * @return 
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * Set created On
     * @param createdOn 
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Get updated On
     * @return 
     */
    public Date getUpdatedOn() {
        return updatedOn;
    }

    /**
     * Set updated on
     * @param updatedOn 
     */
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * Get Locale
     *
     * @return
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set Locale
     *
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get Document Type
     *
     * @return
     */
    public String getDocumentType() {
        return documentType;
    }

    /**
     * set Document Type
     *
     * @param documentType
     */
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    /**
     * Get Instance Type
     *
     * @return
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set Instance Type
     *
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }
    
    
}
