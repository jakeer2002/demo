// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Value;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.dao.ICoreClientapiDao;
import com.bgt.lens.model.entity.CoreClientapi;

/**
 *
 * Client API table data management
 */
public class CoreClientapiDao implements ICoreClientapiDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(CoreClientapiDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;
    
    /**
     * Retry transaction count update count
     */
    @Value("${RetryUpdateTransactionCount}")
    private Integer retryUpdateTransactionCount;

    /**
     * Initialize Client API Dao
     */
    public CoreClientapiDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create client API
     *
     * @param coreClientApi
     */
    @Override
    public void save(CoreClientapi coreClientApi) {
        helperDao.save(sessionFactory, coreClientApi);
    }

    /**
     * Update client API
     *
     * @param coreClientApi
     */
    @Override
    public void update(CoreClientapi coreClientApi) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
           tx = session.beginTransaction();
//            CoreClientapi clientApi = (CoreClientapi) session.get(CoreClientapi.class, coreClientApi.getId(), LockMode.OPTIMISTIC);
//            clientApi.setRemainingTransactions(clientApi.getRemainingTransactions() - 1);
//            session.update(clientApi);
//            session.load(CoreClientapi.class, coreClientApi.getId(), LockOptions.UPGRADE);
            LOGGER.info("Updating transaction count - " + coreClientApi.getRemainingTransactions());
            String hqlUpdate = "update CoreClientapi c set c.remainingTransactions = c.remainingTransactions -1 where c.id_1 = :clientapiid";
            int status = session.createQuery(hqlUpdate).setParameter("clientapiid", coreClientApi.getId_1()).executeUpdate();
//            int status = query.executeUpdate();
//            session.load(CoreClientapi.class, coreClientApi.getId(), LockMode.PESSIMISTIC_WRITE);
//            String hqlUpdate = "update CoreClientapi c set c.remainingTransactions = c.remainingTransactions -1 where c.id_1 = " + coreClientApi.getId_1();
//            final Query query = session.createQuery(hqlUpdate);
//            int status = query.executeUpdate();
//            boolean updateStatus = false;
//            int retryCount = 0;
//            while((!updateStatus) && retryCount < retryUpdateTransactionCount){
//                try{
//                    if(retryCount > 0){
//                        LOGGER.info("Error while updating transaction count. Retrying the update.");
//                    }
//                    session = this.sessionFactory.openSession();
//                    tx = session.beginTransaction();
//                    CoreClientapi clientApi = (CoreClientapi)session.load(CoreClientapi.class, coreClientApi.getId(), LockMode.OPTIMISTIC);
//                    clientApi.setRemainingTransactions(clientApi.getRemainingTransactions() - 1);
//                    tx.commit();
//                    updateStatus = true;
//                    LOGGER.info("Updated transaction count. ClientApiId - " +  coreClientApi.getId_1());
//                }
//                catch(Exception ex){
//                    LOGGER.error("Error while updating transaction count. Retry count - " + retryCount);
//                    retryCount++;
//                    if(retryCount >= retryUpdateTransactionCount){
//                        throw ex;
//                    }
//                }
//            }
            
//            session.load(CoreClientapi.class, coreClientApi.getId(), LockMode.OPTIMISTIC);
//            CoreClientapi clientApi = (CoreClientapi) session.get(CoreClientapi.class, coreClientApi.getId(), LockMode.OPTIMISTIC_FORCE_INCREMENT);
//            session.refresh(clientApi);
//            clientApi.setRemainingTransactions(clientApi.getRemainingTransactions() - 1);
//            LOGGER.info(clientApi.getRemainingTransactions());
//            session.update(clientApi);
//            session.flush();
//            session.refresh(clientApi);
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getClass());
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                tx.rollback();
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
//        helperDao.update(sessionFactory, coreClientApi);
    }

    /**
     * Delete client API
     *
     * @param coreClientApi
     */
    @Override
    public void delete(CoreClientapi coreClientApi) {
        helperDao.delete(sessionFactory, coreClientApi);
    }

    /**
     * Get client API by Id
     *
     * @param clientApiId
     * @return
     */
    @Override
    public CoreClientapi getClientApiById(int clientApiId) {
        return (CoreClientapi) helperDao.getById(sessionFactory, CoreClientapi.class, clientApiId);
    }

    /**
     * Get client API by criteria
     *
     * @param clientapiCriteria
     * @return
     */
    @Override
    public List<CoreClientapi> getClientApiByCriteria(ClientapiCriteria clientapiCriteria) {
        List<CoreClientapi> coreClientapiList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

         //   Criteria cr = session.createCriteria(CoreClientapi.class);
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreClientapi> criteriaQuery = cb.createQuery(CoreClientapi.class);
            Root<CoreClientapi> coreClientapiRoot = criteriaQuery.from(CoreClientapi.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(clientapiCriteria.getActivationDate())) {
            	conditions.add(cb.greaterThanOrEqualTo(coreClientapiRoot.<Date>get("activationDate"), clientapiCriteria.getActivationDate()));
            }
            if (_helper.isNotNull(clientapiCriteria.getExpiryDate())) {
            	conditions.add(cb.lessThanOrEqualTo(coreClientapiRoot.<Date>get("expiryDate"), clientapiCriteria.getExpiryDate()));
            }

            if (_helper.isNotNull(clientapiCriteria.getCoreClient())) {
            	conditions.add(cb.equal(coreClientapiRoot.get("coreClient"), clientapiCriteria.getCoreClient()));
            }

            if (_helper.isNotNull(clientapiCriteria.getCoreApi())) {
            	conditions.add(cb.equal(coreClientapiRoot.get("coreApi"), clientapiCriteria.getCoreApi()));
            }

            if (_helper.isNotNull(clientapiCriteria.isEnabled())) {
            	conditions.add(cb.equal(coreClientapiRoot.get("enabled"), clientapiCriteria.isEnabled()));
            }

            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreClientapi> query = session.createQuery(criteriaQuery);
            List<CoreClientapi> results = query.getResultList();
            if (results != null && results.size() > 0) {
                coreClientapiList = results;

                Set<CoreClientapi> coreUsageLogSet = new HashSet<>();
                coreUsageLogSet.addAll(coreClientapiList);

                coreClientapiList.clear();
                coreClientapiList.addAll(coreUsageLogSet);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                tx.rollback();
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreClientapiList;
    }

}
