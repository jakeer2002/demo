// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.dao.ICoreApiDao;
import com.bgt.lens.model.entity.CoreApi;

/**
 *
 * API table data management
 */
public class CoreApiDao implements ICoreApiDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Database session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize API Dao
     */
    public CoreApiDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set Session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create API
     *
     * @param coreApi
     */
    @Override
    public void save(CoreApi coreApi) {
        helperDao.save(sessionFactory, coreApi);
    }

    /**
     * Update API
     *
     * @param coreApi
     */
    @Override
    public void update(CoreApi coreApi) {
        helperDao.update(sessionFactory, coreApi);
    }

    /**
     * Delete API
     *
     * @param coreApi
     */
    @Override
    public void delete(CoreApi coreApi) {
        helperDao.delete(sessionFactory, coreApi);
    }

    /**
     * Get API by Id
     *
     * @param apiId
     * @return
     */
    @Override
    public CoreApi getApiById(int apiId) {
        return (CoreApi) helperDao.getById(sessionFactory, CoreApi.class, apiId);
    }

    /**
     * Get API by criteria
     *
     * @param apiCriteria
     * @return
     */
    @Override
    public List<CoreApi> getApiByCriteria(ApiCriteria apiCriteria) {
        List<CoreApi> coreApiList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
          //  Criteria cr = session.createCriteria(CoreApi.class);
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreApi> criteriaQuery = cb.createQuery(CoreApi.class);
            Root<CoreApi> coreApiRoot = criteriaQuery.from(CoreApi.class);
            List<Predicate> conditionsList = new ArrayList<Predicate>();
            if (!_helper.isNullOrEmpty(apiCriteria.getApiKey())) {
            	conditionsList.add(cb.equal(coreApiRoot.get("apiKey"), apiCriteria.getApiKey()));
            }

            if (!_helper.isNullOrEmpty(apiCriteria.getName())) {
            	conditionsList.add(cb.equal(coreApiRoot.get("name"), apiCriteria.getName()));
            }

            // CacheMode.IGNORE 
            //cr.setCacheMode(CacheMode.IGNORE);
            criteriaQuery.where(conditionsList.toArray(new Predicate[] {}));
            TypedQuery<CoreApi> query = session.createQuery(criteriaQuery);
            List<CoreApi> results = query.getResultList();
          //  List results = cr.list();

            if (results != null && results.size() > 0) {
                coreApiList = results;

                Set<CoreApi> coreApiSet = new HashSet<>();
                coreApiSet.addAll(coreApiList);

                coreApiList.clear();
                coreApiList.addAll(coreApiSet);
            }
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
            }
            LOGGER.info("Transaction Rollback");
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreApiList;
    }
    
    /**
     * Get API by criteria
     *
     * @param apiCriteria
     * @return
     */
    @Override
    public List<CoreApi> findApiDuplicateByCriteria(ApiCriteria apiCriteria) {
        List<CoreApi> coreApiList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
          //  Criteria cr = session.createCriteria(CoreApi.class);
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreApi> criteriaQuery = cb.createQuery(CoreApi.class);
            Root<CoreApi> coreApiRoot = criteriaQuery.from(CoreApi.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNullOrEmpty(apiCriteria.getApiKey()) && _helper.isNotNullOrEmpty(apiCriteria.getName())) {
            	conditions.add(cb.or(cb.equal(coreApiRoot.get("apiKey"), apiCriteria.getApiKey()), cb.equal(coreApiRoot.get("name"), apiCriteria.getName())));
            }
            else if(_helper.isNotNullOrEmpty(apiCriteria.getApiKey())){
            	conditions.add(cb.equal(coreApiRoot.get("apiKey"), apiCriteria.getApiKey()));
            }
            else if (!_helper.isNullOrEmpty(apiCriteria.getName())) {
            	conditions.add(cb.equal(coreApiRoot.get("name"), apiCriteria.getName()));
            }

            // CacheMode.IGNORE 
           // cr.setCacheMode(CacheMode.IGNORE);

          //  List results = cr.list();
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreApi> query = session.createQuery(criteriaQuery);
            List<CoreApi> results = query.getResultList();
            if (results != null && results.size() > 0) {
                coreApiList = results;

                Set<CoreApi> coreApiSet = new HashSet<>();
                coreApiSet.addAll(coreApiList);

                coreApiList.clear();
                coreApiList.addAll(coreApiSet);
            }
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
            }
            LOGGER.info("Transaction Rollback");
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreApiList;
    }

}
