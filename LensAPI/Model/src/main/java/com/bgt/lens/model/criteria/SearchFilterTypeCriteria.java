// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import java.util.ArrayList;
import java.util.List;


/**
 * Search Filter Type Criteria
 *
 */
public class SearchFilterTypeCriteria {
     /**
     * Id
     */
    protected Integer id;
        
    /**
     * Filter Type List
     */
    protected List<String> filterType;
    
     /**
     * Get Id
     * @return 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * Get Filter Type
     * @return
     */
    public List<String> getFilterType(){
        return filterType;
    }
    
    /**
     * Set Filter Type
     * @param filterType
     */
    public void setFilterType(List<String> filterType){
        this.filterType = filterType;
    }
}
