// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * CoreUsagelog
 */
@Entity
@Table(name = "core_usagelog")
public class CoreUsagelog implements java.io.Serializable {

    /**
     * Usage log Id
     */
    private Integer id;

    /**
     * Request Id
     */
    private String requestId;

    /**
     * Client Id
     */
    private Integer clientId;
    
    /**
     * Consumer Key
     */
    private String consumerKey;
    
    /**
     * Api
     */
    private String api;

    /**
     * API Id
     */
    private Integer apiId;

    /**
     * HTTP URI
     */
    private String uri;

    /**
     * HTTP method
     */
    private String method;

    /**
     * Resource name
     */
    private String resource;

    /**
     * Product name
     */
    private String instanceType;
    
    /**
     * Locale
     */
    private String locale;
    
    /**
     * Parse variant
     */
    private String parseVariant;

    /**
     * Request success or failure status
     */
    private boolean raisedError;

    /**
     * Request in time
     */
    private Date inTime;

    /**
     * Response sent time
     */
    private Date outTime;

    /**
     * Elapsed time
     */
    private Long milliseconds;
    
    /**
     * IP Address
     */
    private String serverAddress;
    
    /**
     * Client IP Address
     */
    private String clientAddress;

    /**
     * Request content
     */
    private String requestContent;

    /**
     * Response content
     */
    private String responseContent;

    /**
     * HTTP headers
     */
    private String headers;
    /**
     * Core usage log data
     */
    private CoreUsagelogdata coreUsagelogdata;

    /**
     * Initialize usage log
     */
    public CoreUsagelog() {
    }

    /**
     * Initialize usage log
     *
     * @param requestId
     * @param uri
     * @param method
     * @param resource
     * @param raisedError
     */
    public CoreUsagelog(String requestId, String uri, String method, String resource, boolean raisedError) {
        this.requestId = requestId;
        this.uri = uri;
        this.method = method;
        this.resource = resource;
        this.raisedError = raisedError;
    }

    /**
     * Initialize usage log
     *@param id
     * @param requestId
     * @param clientId
     * @param apiId
     * @param uri
     * @param method
     * @param resource
     * @param instanceType
     * @param locale
     * @param parseVariant
     * @param raisedError
     * @param inTime
     * @param outTime
     * @param milliseconds
     */
    public CoreUsagelog(Integer id,String requestId, Integer clientId, Integer apiId, String uri, String method, String resource,String instanceType, String local, String parseVariant, boolean raisedError, Date inTime, Date outTime, Long milliseconds) {
        this.id = id;
    	this.requestId = requestId;
        this.clientId = clientId;
        this.apiId = apiId;
        this.uri = uri;
        this.method = method;
        this.resource = resource;
        this.instanceType = instanceType;
        this.locale = local;
        this.parseVariant = parseVariant;
        this.raisedError = raisedError;
        this.inTime = inTime == null ? null : new Date(inTime.getTime());
        this.outTime = outTime == null ? null : new Date(outTime.getTime());
        this.milliseconds = milliseconds;

    }
    
    /**
     * Initialize usage log
     *
     * @param requestId
     * @param clientId
     * @param apiId
     * @param uri
     * @param method
     * @param resource
     * @param raisedError
     * @param inTime
     * @param outTime
     * @param milliseconds
     */
    public CoreUsagelog(String requestId, Integer clientId, Integer apiId, String uri, String method, String resource, boolean raisedError, Date inTime, Date outTime, Long milliseconds) {
        this.requestId = requestId;
        this.clientId = clientId;
        this.apiId = apiId;
        this.uri = uri;
        this.method = method;
        this.resource = resource;
        this.raisedError = raisedError;
        this.inTime = inTime == null ? null : new Date(inTime.getTime());
        this.outTime = outTime == null ? null : new Date(outTime.getTime());
        this.milliseconds = milliseconds;
//        this.requestContent = requestContent;
//        this.responseContent = responseContent;
//        this.headers = headers;
    }

    /**
     * Get usage log Id
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    /**
     * Set usage log Id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get request Id
     *
     * @return
     */
    @Column(name = "RequestId", nullable = true, length = 50)
    public String getRequestId() {
        return this.requestId;
    }

    /**
     * Set request Id
     *
     * @param requestId
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Get client Id
     *
     * @return
     */
    @Column(name = "ClientId")
    public Integer getClientId() {
        return this.clientId;
    }

    /**
     * Set client Id
     *
     * @param clientId
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }
    
    /**
     * Get consumer key
     * @return 
     */
    @Transient
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }
    
    /**
     * Get Api
     * @return 
     */
    @Transient
    public String getApi() {
        return api;
    }

    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * Get API Id
     *
     * @return
     */
    @Column(name = "ApiId")
    public Integer getApiId() {
        return this.apiId;
    }

    /**
     * Set API Id
     *
     * @param apiId
     */
    public void setApiId(Integer apiId) {
        this.apiId = apiId;
    }

    /**
     * Get URI
     *
     * @return
     */
    @Column(name = "Uri", nullable = true, length = 4000)
    public String getUri() {
        return this.uri;
    }

    /**
     * Set URI
     *
     * @param uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * Get HTTP method
     *
     * @return
     */
    @Column(name = "Method", nullable = true, length = 10)
    public String getMethod() {
        return this.method;
    }

    /**
     * Set HTTP method
     *
     * @param method
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Get resource name
     *
     * @return
     */
    @Column(name = "Resource", nullable = true, length = 200)
    public String getResource() {
        return this.resource;
    }

    /**
     * Set resource name
     *
     * @param resource
     */
    public void setResource(String resource) {
        this.resource = resource;
    }
    
    /**
     * Get instance type
     *
     * @return
     */
    @Column(name = "InstanceType", nullable = true, length = 10)
    public String getInstanceType() {
        return this.instanceType;
    }

    /**
     * Set instance type
     *
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }
    
    /**
     * Get locale
     *
     * @return
     */
    @Column(name = "Locale", nullable = true, length = 20)
    public String getLocale() {
        return this.locale;
    }

    /**
     * Set locale
     *
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }
    
    /**
     * Get parse variant
     *
     * @return
     */
    @Column(name = "ParseVariant", nullable = true, length = 20)
    public String getParseVariant() {
        return this.parseVariant;
    }

    /**
     * Set parse variant
     *
     * @param parseVariant
     */
    public void setParseVariant(String parseVariant) {
        this.parseVariant = parseVariant;
    }

    /**
     * Get request success or failure status
     *
     * @return
     */
    @Column(name = "RaisedError", nullable = false)
    public boolean isRaisedError() {
        return this.raisedError;
    }

    /**
     * Set request success or failure status
     *
     * @param raisedError
     */
    public void setRaisedError(boolean raisedError) {
        this.raisedError = raisedError;
    }

    /**
     * Get request in time
     *
     * @return
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "InTime", length = 19)
    public Date getInTime() {
        return inTime == null ? null : new Date(inTime.getTime());
    }

    /**
     * Set request in time
     *
     * @param inTime
     */
    public void setInTime(Date inTime) {
        this.inTime = inTime == null ? null : new Date(inTime.getTime());
    }

    /**
     * Get response sent time
     *
     * @return
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "OutTime", length = 19)
    public Date getOutTime() {
        return outTime == null ? null : new Date(outTime.getTime());
    }

    /**
     * Set response sent time
     *
     * @param outTime
     */
    public void setOutTime(Date outTime) {
        this.outTime = outTime == null ? null : new Date(outTime.getTime());
    }

    /**
     * Get elapsed time
     *
     * @return
     */
    @Column(name = "Milliseconds")
    public Long getMilliseconds() {
        return this.milliseconds;
    }

    /**
     * Set elapsed time
     *
     * @param milliseconds
     */
    public void setMilliseconds(Long milliseconds) {
        this.milliseconds = milliseconds;
    }
    
    /**
     * Get IP Address
     *
     * @return
     */
    @Column(name = "ServerAddress")
    public String getServerAddress() {
        return this.serverAddress;
    }

    /**
     * Set IP Address
     *
     * @param serverAddress
     */
    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }
    
    /**
     * Get client address
     * @return 
     */
    @Column(name = "ClientAddress")
    public String getClientAddress() {
        return clientAddress;
    }

    /**
     * Set client address
     * @param clientAddress 
     */
    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    /**
     * Get request content
     *
     * @return
     */
    @Transient
//    @Column(name = "RequestContent")
    public String getRequestContent() {
        return this.requestContent;
    }

    /**
     * Set request content
     *
     * @param requestContent
     */
    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    /**
     * Get response content
     *
     * @return
     */
    @Transient
//    @Column(name = "ResponseContent")
    public String getResponseContent() {
        return this.responseContent;
    }

    /**
     * Set response content
     *
     * @param responseContent
     */
    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

    /**
     * Get HTTP headers
     *
     * @return
     */
    @Transient
//    @Column(name = "Headers")
    public String getHeaders() {
        return this.headers;
    }

    /**
     * Set HTTP headers
     *
     * @param headers
     */
    public void setHeaders(String headers) {
        this.headers = headers;
    }
    
    /**
     * Get core usage log data
     * @return 
     */
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "coreUsagelog")
    @Cascade(CascadeType.REMOVE)
    @BatchSize(size = 20)
    public CoreUsagelogdata getCoreUsagelogdata() {
        return coreUsagelogdata;
    }

    /**
     * Set core usage log data
     * @param coreUsagelogdata 
     */
    public void setCoreUsagelogdata(CoreUsagelogdata coreUsagelogdata) {
        this.coreUsagelogdata = coreUsagelogdata;
    }

}
