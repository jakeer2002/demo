// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.SearchVendorSettingsCriteria;
import com.bgt.lens.model.entity.SearchVendorsettings;
import java.util.List;

/**
 *
 * Search Vendor settings Dao interface
 */
public interface ISearchVendorSettingsDao {
    /**
     * Create vendor
     *
     * @param searchVendorSettings
     */
    public void save(SearchVendorsettings searchVendorSettings);

    /**
     * Update vendor
     *
     * @param searchVendorSettings
     */
    public void update(SearchVendorsettings searchVendorSettings);

    /**
     * Delete vendor
     *
     * @param searchVendorSettings
     */
    public void delete(SearchVendorsettings searchVendorSettings);
    
    /**
     * Get vendor by Id
     * @param vendorId
     * @return 
     */
    public SearchVendorsettings getVendorById(int vendorId);
    
    /**
     * Get vendor settings by criteria
     *
     * @param searchVendorSettingsCriteria
     * @return
     */
    public List<SearchVendorsettings> getVendorSettingsByCriteria(SearchVendorSettingsCriteria searchVendorSettingsCriteria);
}
