// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import java.util.Date;

/**
 *
 * Client API criteria to get details from database
 */
public class ClientapiCriteria {

    /**
     * Client data
     */
    private CoreClient coreClient;

    /**
     * API data
     */
    private CoreApi coreApi;

    /**
     * License Expiry date
     */
    private Date expiryDate;

    /**
     * License activation date
     */
    private Date activationDate;

    /**
     * API status
     */
    private Boolean enabled;

    /**
     * Get client details
     *
     * @return
     */
    public CoreClient getCoreClient() {
        return coreClient;
    }

    /**
     * Set client details
     *
     * @param coreClient
     */
    public void setCoreClient(CoreClient coreClient) {
        this.coreClient = coreClient;
    }

    /**
     * Get API details
     *
     * @return
     */
    public CoreApi getCoreApi() {
        return coreApi;
    }

    /**
     * Set API details
     *
     * @param coreApi
     */
    public void setCoreApi(CoreApi coreApi) {
        this.coreApi = coreApi;
    }

    /**
     * Get License expiry date
     *
     * @return
     */
    public Date getExpiryDate() {
        return expiryDate == null ? null : new Date(expiryDate.getTime());
    }

    /**
     * Set License expiry date
     *
     * @param expiryDate
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate == null ? null : new Date(expiryDate.getTime());
    }

    /**
     * Get License activation date
     *
     * @return
     */
    public Date getActivationDate() {
        return activationDate == null ? null : new Date(activationDate.getTime());
    }

    /**
     * Set License activation date
     *
     * @param activationDate
     */
    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate == null ? null : new Date(activationDate.getTime());
    }

    /**
     * Get API status
     *
     * @return
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Set API status
     *
     * @param enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
