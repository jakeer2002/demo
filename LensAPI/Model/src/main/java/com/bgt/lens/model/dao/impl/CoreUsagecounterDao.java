// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.dao.ICoreUsagecounterDao;
import com.bgt.lens.model.entity.CoreUsagecounter;

/**
 *
 * Usage counter table data management
 */
public class CoreUsagecounterDao implements ICoreUsagecounterDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(CoreUsagecounterDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize usage counter Dao
     */
    public CoreUsagecounterDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create usage counter
     *
     * @param coreUsageCounter
     */
    @Override
    public void save(CoreUsagecounter coreUsageCounter) {
        helperDao.save(sessionFactory, coreUsageCounter);
    }

    /**
     * Update usage counter
     *
     * @param coreUsageCounter
     */
    @Override
    public void update(CoreUsagecounter coreUsageCounter) {
        helperDao.update(sessionFactory, coreUsageCounter);
    }

    /**
     * Delete usage counter
     *
     * @param coreUsageCounter
     */
    @Override
    public void delete(CoreUsagecounter coreUsageCounter) {
        helperDao.delete(sessionFactory, coreUsageCounter);
    }

    /**
     * Get usage counter by Id
     *
     * @param usageCounterId
     * @return
     */
    @Override
    public CoreUsagecounter getUsageCounterById(Integer usageCounterId) {
        return (CoreUsagecounter) helperDao.getById(sessionFactory, CoreUsagecounter.class, usageCounterId);
    }

    /**
     * Get usage counter by Id and client
     *
     * @param usageCounterId
     * @param clientId
     * @return
     */
    @Override
    public CoreUsagecounter getUsageCounterById(Integer usageCounterId, Integer clientId) {
        CoreUsagecounter coreUsagecounter = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreUsagecounter> criteriaQuery = cb.createQuery(CoreUsagecounter.class);
            Root<CoreUsagecounter> coreUsagecounterRoot = criteriaQuery.from(CoreUsagecounter.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(clientId)) {
            	conditions.add(cb.equal(coreUsagecounterRoot.get("clientId"), clientId));
            }

            if (_helper.isNotNull(usageCounterId)) {
            	conditions.add(cb.equal(coreUsagecounterRoot.get("id"), usageCounterId));
            }

            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreUsagecounter> query = session.createQuery(criteriaQuery);
            query.setMaxResults(1);
            List<CoreUsagecounter> results=query.getResultList();
            if (results != null && results.size() > 0) {
            	coreUsagecounter = results.get(0);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
            }
            LOGGER.info("Transaction Rollback");
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreUsagecounter;
    }

    /**
     * Get usage counter by criteria
     *
     * @param usageCounterCriteria
     * @return
     */
    @Override
    public List<CoreUsagecounter> getUsageCounterByCriteria(UsagecounterCriteria usageCounterCriteria) {
        List<CoreUsagecounter> coreUsagecounterList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<CoreUsagecounter> criteria = builder.createQuery(CoreUsagecounter.class);
            Root<CoreUsagecounter> coreUsagecounterRoot = criteria.from(CoreUsagecounter.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(usageCounterCriteria.getClientId())) {
            	conditions.add(builder.equal(coreUsagecounterRoot.get("clientId"), usageCounterCriteria.getClientId()));
            }

            if (_helper.isNotNull(usageCounterCriteria.getApiId())) {
            	conditions.add(builder.equal(coreUsagecounterRoot.get("apiId"), usageCounterCriteria.getApiId()));
            }

            if (!_helper.isNullOrEmpty(usageCounterCriteria.getMethod())) {
            	conditions.add(builder.equal(coreUsagecounterRoot.get("method"), usageCounterCriteria.getMethod()));
            }

            if (!_helper.isNullOrEmpty(usageCounterCriteria.getResource())) {
            	conditions.add(builder.equal(coreUsagecounterRoot.get("resource"), usageCounterCriteria.getResource()));
            }

            if (!_helper.isNull(usageCounterCriteria.getFromDate())) {
            	conditions.add(builder.greaterThanOrEqualTo(coreUsagecounterRoot.<Date>get("createdOn"), usageCounterCriteria.getFromDate()));
            }

            if (!_helper.isNull(usageCounterCriteria.getToDate())) {
            	conditions.add(builder.lessThanOrEqualTo(coreUsagecounterRoot.<Date>get("createdOn"), usageCounterCriteria.getToDate()));
            }

            criteria.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreUsagecounter> query = session.createQuery(criteria);
            List<CoreUsagecounter> results = query.getResultList();

            if (results != null && results.size() > 0) {
                coreUsagecounterList = results;

                Set<CoreUsagecounter> coreUsageCounterSet = new HashSet<>();
                coreUsageCounterSet.addAll(coreUsagecounterList);

                coreUsagecounterList.clear();
                coreUsagecounterList.addAll(coreUsageCounterSet);
            }
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
            }
            LOGGER.info("Transaction Rollback");
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreUsagecounterList;
    }

}
