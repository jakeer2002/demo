// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.SearchRegisterLogCriteria;
import com.bgt.lens.model.entity.SearchRegisterlog;
import java.util.List;

/**
 *
 * Search Lookup Dao interface
 */
public interface ISearchRegisterLogDao {
    /**
     * Create registration log
     *
     * @param searchRegisterLog
     */
    public void save(SearchRegisterlog searchRegisterLog);

    /**
     * Update registration log
     *
     * @param searchRegisterLog
     */
    public void update(SearchRegisterlog searchRegisterLog);

    /**
     * Delete registration log
     *
     * @param searchRegisterLog
     */
    public void delete(SearchRegisterlog searchRegisterLog);
    
    /**
     * Get registration by Id
     * @param registrationId
     * @return 
     */
    public SearchRegisterlog getRegisterLogById(int registrationId, int clientId);
    
    /**
     * Get registration log count
     * @return 
     */
    public int getRegisterLogCount();
    /**
     * Get register log by criteria
     *
     * @param searchRegisterLogCriteria
     * @return
     */
    public List<SearchRegisterlog> getRegisterLogByCriteria(SearchRegisterLogCriteria searchRegisterLogCriteria);
}
