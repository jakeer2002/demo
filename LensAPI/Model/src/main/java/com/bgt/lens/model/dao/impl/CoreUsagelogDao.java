// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.UsageLogCriteria;
import com.bgt.lens.model.dao.ICoreUsagelogDao;
import com.bgt.lens.model.entity.CoreUsagelog;

/**
 *
 * Usage log table data management
 */
public class CoreUsagelogDao implements ICoreUsagelogDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(CoreUsagelogDao.class);
    /**
     * Helper object
     */
    private final Helper _helper = new Helper();
    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize usage log Dao
     */
    public CoreUsagelogDao() {
        helperDao = new HelperDao();
    }

    /**
     * Set session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create usage log
     *
     * @param coreUsageLog
     */
    @Override
    public void save(CoreUsagelog coreUsageLog) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            LOGGER.info("Store usage log start time - " + Date.from(Instant.now()));
            tx = session.beginTransaction();
            session.save(coreUsageLog);
//            coreUsageLog.getCoreUsagelogdata().setCoreUsagelog(coreUsageLog);
            if (coreUsageLog.getCoreUsagelogdata() != null) {
                coreUsageLog.getCoreUsagelogdata().setUsageLogId(coreUsageLog.getId());
                session.save(coreUsageLog.getCoreUsagelogdata());
            }
            LOGGER.info("Store usage log end time - " + Date.from(Instant.now()));
//            helperDao.save(sessionFactory, coreUsageLog);
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                tx.rollback();
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Update usage log
     *
     * @param coreUsageLog
     */
    @Override
    public void update(CoreUsagelog coreUsageLog) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            session.update(coreUsageLog);
            if (coreUsageLog.getCoreUsagelogdata() != null) {
                coreUsageLog.getCoreUsagelogdata().setCoreUsagelog(coreUsageLog);
                session.update(coreUsageLog.getCoreUsagelogdata());
            }
//            helperDao.update(sessionFactory, coreUsageLog);
            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                tx.rollback();
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Delete usage log
     *
     * @param coreUsageLog
     */
    @Override
    public void delete(CoreUsagelog coreUsageLog) {
        helperDao.delete(sessionFactory, coreUsageLog);
    }

    /**
     * Get usage log by Id
     *
     * @param usageLogId
     * @return
     */
    @Override
    public CoreUsagelog getUsageLogById(String usageLogId) {
        CoreUsagelog coreUsagelog = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreUsagelog> criteriaQuery = cb.createQuery(CoreUsagelog.class);
            Root<CoreUsagelog> coreUsagelogRoot = criteriaQuery.from(CoreUsagelog.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(usageLogId)) {
            	conditions.add(cb.equal(coreUsagelogRoot.get("requestId"), usageLogId));
            }

            criteriaQuery.multiselect(coreUsagelogRoot.get("id"),
            		coreUsagelogRoot.get("requestId"),
            		coreUsagelogRoot.get("clientId"),
            		coreUsagelogRoot.get("apiId"),
            		coreUsagelogRoot.get("uri"),
            		coreUsagelogRoot.get("method"),
            		coreUsagelogRoot.get("resource"),
            		coreUsagelogRoot.get("instanceType"),
            		coreUsagelogRoot.get("locale"),
            		coreUsagelogRoot.get("parseVariant"),
            		coreUsagelogRoot.get("raisedError"),
            		coreUsagelogRoot.get("inTime"),
            		coreUsagelogRoot.get("outTime"),
            		coreUsagelogRoot.get("milliseconds")
            		).distinct(true);

            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreUsagelog> query = session.createQuery(criteriaQuery);
            query.setMaxResults(1);
            List<CoreUsagelog> results=query.getResultList();
            if (results != null && results.size() > 0) {
            	coreUsagelog = results.get(0);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreUsagelog;
    }

    /**
     * Get usage log by id and client
     *
     * @param usageLogId
     * @param clientId
     * @return
     */
    @Override
    public CoreUsagelog getUsageLogById(String usageLogId, int clientId) {
        CoreUsagelog coreUsagelog = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreUsagelog> criteriaQuery = cb.createQuery(CoreUsagelog.class);
            Root<CoreUsagelog> coreUsagelogRoot = criteriaQuery.from(CoreUsagelog.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(usageLogId)) {
            	conditions.add(cb.equal(coreUsagelogRoot.get("requestId"), usageLogId));
            }

            if (_helper.isNotNull(clientId)) {
            	conditions.add(cb.equal(coreUsagelogRoot.get("clientId"), clientId));
            }

            criteriaQuery.multiselect(coreUsagelogRoot.get("id"),
            		coreUsagelogRoot.get("requestId"),
            		coreUsagelogRoot.get("clientId"),
            		coreUsagelogRoot.get("apiId"),
            		coreUsagelogRoot.get("uri"),
            		coreUsagelogRoot.get("method"),
            		coreUsagelogRoot.get("resource"),
            		coreUsagelogRoot.get("instanceType"),
            		coreUsagelogRoot.get("locale"),
            		coreUsagelogRoot.get("parseVariant"),
            		coreUsagelogRoot.get("raisedError"),
            		coreUsagelogRoot.get("inTime"),
            		coreUsagelogRoot.get("outTime"),
            		coreUsagelogRoot.get("milliseconds")
            		).distinct(true);

            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreUsagelog> query = session.createQuery(criteriaQuery);
            query.setMaxResults(1);
            List<CoreUsagelog> results=query.getResultList();
            if (results != null && results.size() > 0) {
            	coreUsagelog = results.get(0);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreUsagelog;
    }

    /**
     * Get usage log by criteria
     *
     * @param usageLogCriteria
     * @return
     */
    @Override
    public List<CoreUsagelog> getUsageLogByCriteria(UsageLogCriteria usageLogCriteria) {
        List<CoreUsagelog> coreUsagelogList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreUsagelog> criteriaQuery = cb.createQuery(CoreUsagelog.class);
            Root<CoreUsagelog> coreUsagelogRoot = criteriaQuery.from(CoreUsagelog.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(usageLogCriteria.getFrom())) {
            	conditions.add(cb.greaterThanOrEqualTo(coreUsagelogRoot.get("inTime"), usageLogCriteria.getFrom()));
            }
            if (_helper.isNotNull(usageLogCriteria.getTo())) {
            	conditions.add(cb.lessThanOrEqualTo(coreUsagelogRoot.get("outTime"), usageLogCriteria.getTo()));
            }

            if (_helper.isNotNull(usageLogCriteria.getClientId()) && usageLogCriteria.getClientId() > 0) {
            	conditions.add(cb.equal(coreUsagelogRoot.get("clientId"), usageLogCriteria.getClientId()));
            }

            if (_helper.isNotNull(usageLogCriteria.getApiId()) && usageLogCriteria.getApiId() > 0) {
            	conditions.add(cb.equal(coreUsagelogRoot.get("apiId"), usageLogCriteria.getApiId()));
            }

            if (!_helper.isNullOrEmpty(usageLogCriteria.getMethod())) {
            	conditions.add(cb.equal(coreUsagelogRoot.get("method"), usageLogCriteria.getMethod()));
            }

            if (!_helper.isNullOrEmpty(usageLogCriteria.getResource())) {
            	conditions.add(cb.equal(coreUsagelogRoot.get("resource"), usageLogCriteria.getResource()));
            }

            if (_helper.isNotNull(usageLogCriteria.isRaisedError())) {
            	conditions.add(cb.equal(coreUsagelogRoot.get("raisedError"), usageLogCriteria.isRaisedError()));
            }

            criteriaQuery.multiselect(coreUsagelogRoot.get("id"),
            		coreUsagelogRoot.get("requestId"),
            		coreUsagelogRoot.get("clientId"),
            		coreUsagelogRoot.get("apiId"),
            		coreUsagelogRoot.get("uri"),
            		coreUsagelogRoot.get("method"),
            		coreUsagelogRoot.get("resource"),
            		coreUsagelogRoot.get("instanceType"),
            		coreUsagelogRoot.get("locale"),
            		coreUsagelogRoot.get("parseVariant"),
            		coreUsagelogRoot.get("raisedError"),
            		coreUsagelogRoot.get("inTime"),
            		coreUsagelogRoot.get("outTime"),
            		coreUsagelogRoot.get("milliseconds")
            		).distinct(true);
            
            
            criteriaQuery.orderBy(cb.desc(coreUsagelogRoot.get("id")));
            
            
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreUsagelog> query = session.createQuery(criteriaQuery);
            query.setMaxResults(usageLogCriteria.getDefaultResultLimit());
            List<CoreUsagelog> results = query.getResultList();

            // ToDo : Need to check duplicate in list
            if (results != null && results.size() > 0) {
                coreUsagelogList = results;

                Set<CoreUsagelog> coreUsageLogSet = new HashSet<>();
                coreUsageLogSet.addAll(coreUsagelogList);

                coreUsagelogList.clear();
                coreUsagelogList.addAll(coreUsageLogSet);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreUsagelogList;
    }

}
