// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.helpers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * Enum list
 */
public class Enum {

    /**
     * HTTP headers
     */
    public enum Headers {

        /**
         *
         * JSON
         */
        ApplicationJSON {
            @Override
            public String toString() {
                return "application/json";
            }
        },
        /**
         *
         * XML
         */
        ApplicationXML {
            @Override
            public String toString() {
                return "application/xml";
            }
        },
    }

    /**
     * Resume parsing variants
     */
    public enum VariantType {

        /**
         *
         * BGT XML
         */
        bgtxml,
        /**
         *
         * Structured BGT XML
         */
        structuredbgtxml,
        /**
         *
         * HR XML
         */
        hrxml,
        /**
         *
         * HTML
         */
        htm,
        /**
         *
         * RTF
         */
        rtf,
        /**
         * JSON
         */
        json,
        /**
         * Fetch
         */
        fetch,
        /**
         * Register
         */
        register,
        /**
         * Unregister
         */
        unregister,
        /**
         * Search
         */
        Search,
        /**
         * Send Xml command
         */
        Clarify
    }

    /**
     * Resume parsing fields
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    public enum Fields {

        /**
         * All resume fields
         */
        All,
        /**
         * Contact
         */
        Contact,
        /**
         * Statements
         */
        Statements,
        /**
         * Skills
         */
        Skills,
        /**
         * Professional
         */
        Professional,
        /**
         * Education
         */
        Education,
        /**
         * Summary
         */
        Summary,
        /**
         * Experience
         */
        Experience,
        /**
         * References
         */
        References,
        /**
         */
        SkillRollUp,
        /**
         * Duties
         */
        Duties,
        /**
         * BackGround
         */
        BackGround,
        /**
         * DataElementsRollup Section
         */
        DataElementsRollup

    }

    /**
     * Instance type
     */
    public enum InstanceType {

        /**
         *
         * XRay
         */
        /**
         *
         * XRay
         */
        XRay,
        /**
         * XRay Jobs
         */
        XRayJOBS,
        /**
         *
         * XRAYEDU
         */
        XRAYEDU,
        /**
         *
         * Lens
         */
        LensSearch,
        /**
         *
         * Talent/Mine
         */
        TM,
        /**
         *
         * Job/Mine
         */
        JM,
        /**
         *
         * Locale Detector
         */
        LD,
        /**
         * SPECTRUM
         */
        SPECTRUM
    }

    /**
     * Encoding type
     */
    public enum Encoding {

        /**
         *
         * UTF-8
         */
        UTF {
            @Override
            public String toString() {
                return "utf-8";
            }
        },
        /**
         *
         * ISO-8859-1
         */
        ISO8859 {
            @Override
            public String toString() {
                return "iso-8859-1";
            }
        }
    }

    public enum ApplicationAPI {

        /**
         *
         * Admin Service API
         */
        AdminService {
            @Override
            public String toString() {
                return "Core";
            }
        },
        /**
         *
         * Parser Service API
         */
        ParserService {
            @Override
            public String toString() {
                return "Utility";
            }
        },
        /**
         *
         * Search Service API
         */
        SearchService {
            @Override
            public String toString() {
                return "Search";
            }
        }
    }

    /**
     * HTTP request method type
     */
    public enum HttpMethod {

        /**
         *
         * HTTP GET
         */
        GET,
        /**
         *
         * HTTP POST
         */
        POST,
        /**
         *
         * HTTP PUT
         */
        PUT,
        /**
         *
         * HTTP DELETE
         */
        DELETE,
        /**
         *
         * HTTP OPTIONS
         */
        OPTIONS
    }

    public enum ParsingType {

        RESUME {
            @Override
            public String toString() {
                return "resume";
            }
        },
        JOB {
            @Override
            public String toString() {
                return "job";
            }
        }
    }

    /**
     * Instance Type
     */
    public enum ParsingInstanceType {

        /**
         * LENS
         */
        /**
         * LENS
         */
        LENSSEARCH {
            @Override
            public String toString() {
                return "lenssearch";
            }
        },
        /**
         * XRAY
         */
        XRAY {
            @Override
            public String toString() {
                return "xray";
            }
        },
        /**
         * XRAY Jobs
         */
        XRAYJOBS {
            @Override
            public String toString() {
                return "xrayjobs";
            }
        },
        /**
         * XRAYEDU
         */
        XRAYEDU {
            @Override
            public String toString() {
                return "xrayedu";
            }
        },
        /**
         * LENS optic(Jobs)
         */
        JM {
            @Override
            public String toString() {
                return "jm";
            }
        },
        /**
         * LENS optic(Resumes)
         */
        TM {
            @Override
            public String toString() {
                return "tm";
            }
        },
        /**
         * Locale Detector
         */
        LD {
            @Override
            public String toString() {
                return "ld";
            }
        },
        /**
         * LENS Spectrum
         */
        SPECTRUM {
            @Override
            public String toString() {
                return "spectrum";
            }
        }
    }

    /**
     * Vendor status
     */
    public enum vendorStatus {

        /**
         * Open
         */
        Open,
        /**
         * Close
         */
        Close
    }

    /**
     * Document type
     */
    public enum docType {

        /**
         * Resume
         */
        resume,
        /**
         * Posting
         */
        posting
    }

    /**
     * Register duplicate document
     */
    public enum registerDuplicate {

        /**
         * Error
         */
        error,
        /**
         * Overwrite
         */
        overwrite
    }

    /**
     * Resume search types
     */
    public enum resumeSearchTypes {

        /**
         * Search for candidates by job description.
         */
        ByJobDescription,
        /**
         * Search for candidates by resume.
         */
        ByResume,
        /**
         * Search for resumes like this.
         */
        ResumesLikeThis,
        /**
         * Search by Geographical data
         */
        GeographicalSearch,
        /**
         * Search by custom filter
         */
        CustomFilterSearch,
        /**
         * Search by years of experience
         */
        YearsOfExperienceBasedSearch,
        /**
         * Search by keywords
         */
        ByKeywords,
        /**
         * Faceted Search
         */
        FacetedSearch,
        /**
         * Search by additional filters only
         */
        OpenSearch

    }

    /**
     * Keyword search types
     */
    public enum keywordSearchTypes {

        /**
         * Generic KeyWord Search.
         */
        Generic,
        /**
         * CKS Search.
         */
        CKS,
        /**
         * Custom Boolean Search.
         */
        CustomBooleanSearch
    }

    /**
     * Keyword search types
     */
    public enum scoringMode {

        /**
         * Search for candidates by job description.
         */
        hardfiltersonly {
            @Override
            public String toString() {
                return "hard-filters-only";
            }
        },
        /**
         * Search for candidates by resume.
         */
        skiplensscoring {
            @Override
            public String toString() {
                return "skip-lens-scoring";
            }
        },
        /**
         * Search for resumes like this.
         */
        svdfiltersonly {
            @Override
            public String toString() {
                return "svd-filters-only";
            }
        }
    }

    /**
     * Distance Units
     */
    public enum distanceUnits {

        /**
         * Miles
         */
        Miles {
            @Override
            public String toString() {
                return "miles";
            }
        },
        /**
         * Kilometers
         */
        Kilometers {
            @Override
            public String toString() {
                return "km";
            }
        }
    }

    /**
     * Keyword operators
     */
    public enum keywordOperator {

        /**
         * And
         */
        And {
            @Override
            public String toString() {
                return "and";
            }
        },
        /**
         * Or
         */
        Or {
            @Override
            public String toString() {
                return "or";
            }
        },
        /**
         * None
         */
        None
    }

    /**
     * Lookup Type
     */
    public enum lookupType {

        /**
         * ResumeKeywordFilters
         */
        ResumeKeywordFilters,
        /**
         * PostingKeywordFilters
         */
        PostingKeywordFilters
        
    }

    /**
     * Keyword context
     */
    public enum keywordContext {

        /**
         * KeywordFilters
         */
        FullResume,
        /**
         * Experience section
         */
        ExperienceSection,
        /**
         * Last Job
         */
        LastJob,
        /**
         * Last two jobs
         */
        LastTwoJobs,
        /**
         * Last job title
         */
        LastJobTitle,
        /**
         * Last two job title
         */
        LastTwoJobTitles,
        /**
         * Jobtitle
         */
        JobTitle,
        /**
         * Employer
         */
        Employer,
        /**
         * Most recent employer
         */
        MostRecentEmployer,
        /**
         * Education
         */
        Education,
        /**
         * Degree
         */
        Degree,
        /**
         * Major
         */
        Major,
        /**
         * SIC
         */
        SIC {
            @Override
            public String toString() {
                return "2DigitSIC";
            }
        },
        /**
         * Contact
         */
        ContactSection
    }

    /**
     * Search results order by
     */
    public enum orderResultsBy {

        /**
         * Document Id
         */
        id,
        /**
         * LENS / Keyword score
         */
        score,
        /**
         * Experience
         */
        experience,
        /**
         * Name
         */
        name
    }

    /**
     * Sort Direction
     */
    public enum sortDirection {

        /**
         * Ascending order
         */
        ASC,
        /**
         * Descending order
         */
        DESC
    }

    /**
     * Register Log Type
     */
    public enum registerLogType {

        /**
         * Ascending order
         */
        Register,
        /**
         * Descending order
         */
        Unregister
    }

    /**
     *
     */
    public enum searchFilterType {

        /**
         * Job Vs Resume Search
         */
        JobVsResumeSearch,
        /**
         * Resume Vs Job Search
         */
        ResumeVsJobSearch,
        /**
         * Job Vs Job Search
         */
        JobVsJobSearch,
        /**
         * Resumes Like This Search
         */
        JobsLikeThisSearch,
        /**
         * Resumes Id Vs Job Search
         */
        ResumeIdVsJobSearch,
        /**
         * Job Id Vs Resume Search
         */
        JobIdVsResumeSearch,
        /**
         * Resume Vs Resume Search
         */
        ResumeVsResumeSearch,
        /**
         * Resumes Like This Search
         */
        ResumesLikeThisSearch,
        /**
         * Generic Search
         */
        GenericSearch,
        /**
         * CKS
         */
        CKS,
        /**
         * Custom Boolean Search
         */
        CustomBooleanSearch,
        /**
         * Geographical Search
         */
        GeographicalSearch,
        /**
         * Custom Filter Search
         */
        CustomFilterSearch,
        /**
         * Years Of Experience Based Search
         */
        YearsOfExperienceBasedSearch,
        /**
         * Faceted Search
         */
        FacetedSearch
    }

    public enum RateLimitTimePeriod {
        hourly {
            @Override
            public String toString() {
                return "hourly";
            }
        },
        daily {
            @Override
            public String toString() {
                return "daily";
            }
        },
        monthly {
            @Override
            public String toString() {
                return "monthly";
            }
        }
    }

}
