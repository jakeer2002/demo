// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.config;

/**
 * LENS server services config
 * @author pvarudaraj
 */
public class LensServiceConfig {
    /** 
     * Service label
     */
    protected String serviceLabel;
    
    /**
     * Service type
     */
    protected String type;
    
    /**
     * Service name
     */
    protected String serviceName;

    /**
     * Get service label
     * @return 
     */
    public String getServiceLabel() {
        return serviceLabel;
    }

    /**
     * Set service label
     * @param serviceLabel 
     */
    public void setServiceLabel(String serviceLabel) {
        this.serviceLabel = serviceLabel;
    }

    /**
     * Get service type
     * @return 
     */
    public String getType() {
        return type;
    }

    /**
     * Set service type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get service name
     * @return 
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Set service name
     * @param serviceName 
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
