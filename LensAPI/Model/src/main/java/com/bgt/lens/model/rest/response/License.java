// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * License
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class License {

    /**
     * Client Id
     */
    @XmlElement(name = "ClientId", required = false)
    protected Integer clientId;

    /**
     * API List
     */
    @XmlElement(name = "ClientApi", required = false)
    protected List<ClientApi> clientApi;

    /**
     * Get client Id
     *
     * @return
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client Id
     *
     * @param clientId
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get client API list
     *
     * @return
     */
    public List<ClientApi> getClientApi() {
        return clientApi;
    }

    /**
     * Set client API list
     *
     * @param clientApi
     */
    public void setClientApi(List<ClientApi> clientApi) {
        this.clientApi = clientApi;
    }

}
