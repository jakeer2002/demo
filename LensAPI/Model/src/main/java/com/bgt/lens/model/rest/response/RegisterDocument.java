// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Resume repository
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class RegisterDocument {
    
    /**
     * Document type
     */
    @XmlElement(name = "Type", required = false)
    protected String type;
    
    /**
     * Document Id
     */
    @XmlElement(name = "Id", required = false)
    protected String id;
    
    /**
     * Vendor name
     */
    @XmlElement(name = "Vendor", required = false)
    protected String vendor;
    
    /**
     * Locale
     */
    @XmlElement(name = "Locale", required = false)
    protected String locale;
    
    /**
     * Instance Type
     */
    @XmlElement(name = "InstanceType", required = false)
    protected String instanceType;
    
    /**
     * Maximum number of document can be registered
     */
    @XmlElement(name = "MaxDocumentCount", required = false)
    protected Integer maxDocumentCount;
    
    /**
     * Number of documents registered so far
     */
    @XmlElement(name = "RegisteredDocumentCount", required = false)
    protected Integer registeredDocumentCount;
    
    /**
     * Flag to indicate whether the document has been overwritten
     */
    @XmlElement(name = "IsOverWritten", required = false)
    protected Boolean isOverWritten;
    
    /**
     * Get type
     * @return 
     */
    public String getType() {
        return type;
    }

    /**
     * Set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get Id
     * @return 
     */
    public String getId() {
        return id;
    }

    /**
     * Set Id
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get vendor
     * @return 
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * Set vendor
     * @param vendor 
     */
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * Get locale
     * @return 
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set locale
     * @param locale 
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get instance type
     * @return 
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     * @param instanceType 
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }
    
    /**
     * Get maximum document count
     * @return 
     */
    public Integer getMaxDocumentCount() {
        return maxDocumentCount;
    }

    /**
     * Set maximum document count
     * @param maxDocumentCount 
     */
    public void setMaxDocumentCount(Integer maxDocumentCount) {
        this.maxDocumentCount = maxDocumentCount;
    }

    /**
     * Get registered document count
     * @return 
     */
    public Integer getRegisteredDocumentCount() {
        return registeredDocumentCount;
    }

    /**
     * Set registered document count
     * @param registeredDocumentCount 
     */
    public void setRegisteredDocumentCount(Integer registeredDocumentCount) {
        this.registeredDocumentCount = registeredDocumentCount;
    }
    
    /**
     * Get isOverwritten flag
     * @return 
     */
    public Boolean isIsOverWritten() {
        return isOverWritten;
    }

    /**
     * Set isOverWritten flag
     * @param isOverWritten 
     */
    public void setIsOverWritten(Boolean isOverWritten) {
        this.isOverWritten = isOverWritten;
    }
}
