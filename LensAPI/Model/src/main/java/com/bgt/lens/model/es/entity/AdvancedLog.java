// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.es.entity;

import java.util.Date;

/**
 * Advanced Log
 */
public class AdvancedLog {
    /**
     * Request Id
     */
    public String requestId;
    
    /**
     * Consumer key
     */
    public String consumerKey;
    
    /**
     * Api name
     */
    public String api;
    
    /**
     * Api Rate Limit
     */
    public Double apiRateLimit;
    
    /**
     * Product Rate Limit
     */
    public Double productRateLimit;
    
    /**
     * Resource name
     */
    public String resourceName;
    
    /**
     * URL
     */
    public String url;
    
    /**
     * HTTP method
     */
    public String method;
    
    /**
     * Auth validation
     */
    public Double authValidation;
    
    /**
     * Auth log
     */
    public Double authLog;
    
    /**
     * Tag document
     */
    public Double tagDocument;
    
    /**
     * HRXML update
     */
    public Double hrxmlUpdate;
    
    /**
     * Locale detection
     */
    public Double localeDetection;
    
    /**
     * Overall LENS communication
     */
    public Double overallLensCommunication;
    
    /**
     * Update transaction count
     */
    public Double updateTransactionCount;
    
    /**
     * Document dump
     */
    public Double documentDump;
    
    /**
     * Register tag document
     */
    public Double registerTagDocument;
    
    /**
     * Register with custom filters
     */
    public Double registerWithCustomFilters;
    
    /**
     * Register without custom filters
     */
    public Double registerWithoutCustomFilters;
    
    /**
     * Unregister
     */
    public Double unregister;
    
    /**
     * Overwrite register document
     */
    public Double overWriteRegisterDocument;
    
    /**
     * Update register document count
     */
    public Double updateRegisterDocumentCount;
    
    /**
     * Facet dist command build
     */
    public Double facetDistcommandBuild;
    
    /**
     * Facet dist command process
     */
    public Double facetDistCommandProcess;
    
    /**
     * Search tag document
     */
    public Double searchTagDocument;
    
    /**
     * Search command build
     */
    public Double searchCommandBuild;
    
    /**
     * Search command process
     */
    public Double searchCommandProcess;
    
    /**
     * Search result process
     */
    public Double searchResultProcess;
    
    /**
     * Search command dump
     */
    public Double searchCommandDump;
    
    /**
     * Fetch command process
     */
    public Double fetchCommandProcess;
    
    /**
     * Usage counter update
     */
    public Double usageCounterUpdate;
    
    /**
     * Usage log update
     */
    public Double usageLogUpdate;
    
    /**
     * Error log update
     */
    public Double errorLogUpdate;
    
    /**
     * Status
     */
    public Boolean status;
    
    /**
     * Start time
     */
    public Date startTime;
    
    /**
     * End time
     */
    public Date endTime;
    
    /**
     * Elapsed time
     */
    public Double elapsedTime;
    
    /**
     * Version
     */
    public String version;

    /**
     * Get request id
     * @return 
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set request id
     * @param requestId 
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Get consumer key
     * @return 
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get api
     * @return 
     */
    public String getApi() {
        return api;
    }

    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * Get api rate limit
     * @return 
     */
    public Double getApiRateLimit() {
        return apiRateLimit;
    }

    /**
     * Set api rate limit
     * @param apiRateLimit 
     */
    public void setApiRateLimit(Double apiRateLimit) {
        this.apiRateLimit = apiRateLimit;
    }

    /**
     * Get product rate limit
     * @return 
     */
    public Double getProductRateLimit() {
        return productRateLimit;
    }

    /**
     * Set product rate limit
     * @param productRateLimit 
     */
    public void setProductRateLimit(Double productRateLimit) {
        this.productRateLimit = productRateLimit;
    }

    /**
     * Get resource name
     * @return 
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     * Set resource name
     * @param resourceName 
     */
    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    /**
     * Get URL
     * @return 
     */
    public String getUrl() {
        return url;
    }

    /**
     * Set URL
     * @param url 
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
    /**
     * Get methid
     * @return 
     */
    public String getMethod() {
        return method;
    }

    /**
     * Set method
     * @param method 
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Get auth validation
     * @return 
     */
    public Double getAuthValidation() {
        return authValidation;
    }

    /**
     * Set auth validation
     * @param authValidation 
     */
    public void setAuthValidation(Double authValidation) {
        this.authValidation = authValidation;
    }

    /**
     * Get auth log
     * @return 
     */
    public Double getAuthLog() {
        return authLog;
    }

    /**
     * Set auth log
     * @param authLog 
     */
    public void setAuthLog(Double authLog) {
        this.authLog = authLog;
    }

    /**
     * Get tag document
     * @return 
     */
    public Double getTagDocument() {
        return tagDocument;
    }

    /**
     * Set tag document
     * @param tagDocument 
     */
    public void setTagDocument(Double tagDocument) {
        this.tagDocument = tagDocument;
    }

    /**
     * Get HRXML update
     * @return 
     */
    public Double getHrxmlUpdate() {
        return hrxmlUpdate;
    }

    /**
     * Set HRXML update
     * @param hrxmlUpdate 
     */
    public void setHrxmlUpdate(Double hrxmlUpdate) {
        this.hrxmlUpdate = hrxmlUpdate;
    }

    /**
     * Get locale detection
     * @return 
     */
    public Double getLocaleDetection() {
        return localeDetection;
    }

    /**
     * Set locale detection
     * @param localeDetection 
     */
    public void setLocaleDetection(Double localeDetection) {
        this.localeDetection = localeDetection;
    }

    /**
     * Get overall LENS communication
     * @return 
     */
    public Double getOverallLensCommunication() {
        return overallLensCommunication;
    }

    /**
     * Set overall LENS communication
     * @param overallLensCommunication 
     */
    public void setOverallLensCommunication(Double overallLensCommunication) {
        this.overallLensCommunication = overallLensCommunication;
    }

    /**
     * Get update transaction count
     * @return 
     */
    public Double getUpdateTransactionCount() {
        return updateTransactionCount;
    }

    /**
     * Set update transaction count
     * @param updateTransactionCount 
     */
    public void setUpdateTransactionCount(Double updateTransactionCount) {
        this.updateTransactionCount = updateTransactionCount;
    }

    /**
     * Get document dump
     * @return 
     */
    public Double getDocumentDump() {
        return documentDump;
    }

    /**
     * Set document dump
     * @param documentDump 
     */
    public void setDocumentDump(Double documentDump) {
        this.documentDump = documentDump;
    }

    /**
     * Get register tag document
     * @return 
     */
    public Double getRegisterTagDocument() {
        return registerTagDocument;
    }

    /**
     * Set register tag document
     * @param registerTagDocument 
     */
    public void setRegisterTagDocument(Double registerTagDocument) {
        this.registerTagDocument = registerTagDocument;
    }

    /**
     * Get register with custom filters
     * @return 
     */
    public Double getRegisterWithCustomFilters() {
        return registerWithCustomFilters;
    }

    /**
     * Set register with custom filters
     * @param registerWithCustomFilters 
     */
    public void setRegisterWithCustomFilters(Double registerWithCustomFilters) {
        this.registerWithCustomFilters = registerWithCustomFilters;
    }

    /**
     * Get register without custom filters
     * @return 
     */
    public Double getRegisterWithoutCustomFilters() {
        return registerWithoutCustomFilters;
    }

    /**
     * Set register with custom filters
     * @param registerWithoutCustomFilters 
     */
    public void setRegisterWithoutCustomFilters(Double registerWithoutCustomFilters) {
        this.registerWithoutCustomFilters = registerWithoutCustomFilters;
    }

    /**
     * Get unregister
     * @return 
     */
    public Double getUnregister() {
        return unregister;
    }

    /**
     * Set unregister
     * @param unregister 
     */
    public void setUnregister(Double unregister) {
        this.unregister = unregister;
    }

    /**
     * Get overwrite register document
     * @return 
     */
    public Double getOverWriteRegisterDocument() {
        return overWriteRegisterDocument;
    }

    /**
     * Set overwrite register document
     * @param overWriteRegisterDocument 
     */
    public void setOverWriteRegisterDocument(Double overWriteRegisterDocument) {
        this.overWriteRegisterDocument = overWriteRegisterDocument;
    }

    /**
     * Get update register document count
     * @return 
     */
    public Double getUpdateRegisterDocumentCount() {
        return updateRegisterDocumentCount;
    }

    /**
     * Set update register document count
     * @param updateRegisterDocumentCount 
     */
    public void setUpdateRegisterDocumentCount(Double updateRegisterDocumentCount) {
        this.updateRegisterDocumentCount = updateRegisterDocumentCount;
    }

    /**
     * Get facet dist command build
     * @return 
     */
    public Double getFacetDistcommandBuild() {
        return facetDistcommandBuild;
    }

    /**
     * Set facet dist command build
     * @param facetDistcommandBuild 
     */
    public void setFacetDistcommandBuild(Double facetDistcommandBuild) {
        this.facetDistcommandBuild = facetDistcommandBuild;
    }

    /**
     * Get facet dist command process
     * @return 
     */
    public Double getFacetDistCommandProcess() {
        return facetDistCommandProcess;
    }

    /**
     * Set facet dist command process
     * @param facetDistCommandProcess 
     */
    public void setFacetDistCommandProcess(Double facetDistCommandProcess) {
        this.facetDistCommandProcess = facetDistCommandProcess;
    }

    /**
     * Get search tag document
     * @return 
     */
    public Double getSearchTagDocument() {
        return searchTagDocument;
    }

    /**
     * Set search tag document
     * @param searchTagDocument 
     */
    public void setSearchTagDocument(Double searchTagDocument) {
        this.searchTagDocument = searchTagDocument;
    }

    /**
     * Get search command build
     * @return 
     */
    public Double getSearchCommandBuild() {
        return searchCommandBuild;
    }

    /**
     * Set search command build
     * @param searchCommandBuild 
     */
    public void setSearchCommandBuild(Double searchCommandBuild) {
        this.searchCommandBuild = searchCommandBuild;
    }

    /**
     * Get search command process
     * @return 
     */
    public Double getSearchCommandProcess() {
        return searchCommandProcess;
    }

    /**
     * Set search command process
     * @param searchCommandProcess 
     */
    public void setSearchCommandProcess(Double searchCommandProcess) {
        this.searchCommandProcess = searchCommandProcess;
    }

    /**
     * Get search result process
     * @return 
     */
    public Double getSearchResultProcess() {
        return searchResultProcess;
    }

    /**
     * Set search result process
     * @param searchResultProcess 
     */
    public void setSearchResultProcess(Double searchResultProcess) {
        this.searchResultProcess = searchResultProcess;
    }

    /**
     * Get search command dump
     * @return 
     */
    public Double getSearchCommandDump() {
        return searchCommandDump;
    }

    /**
     * Set search command dump
     * @param searchCommandDump 
     */
    public void setSearchCommandDump(Double searchCommandDump) {
        this.searchCommandDump = searchCommandDump;
    }

    /**
     * Get fetch command process
     * @return 
     */
    public Double getFetchCommandProcess() {
        return fetchCommandProcess;
    }

    /**
     * Set fetch command process
     * @param fetchCommandProcess 
     */
    public void setFetchCommandProcess(Double fetchCommandProcess) {
        this.fetchCommandProcess = fetchCommandProcess;
    }

    /**
     * Get usage counter update
     * @return 
     */
    public Double getUsageCounterUpdate() {
        return usageCounterUpdate;
    }

    /**
     * Set usage counter update
     * @param usageCounterUpdate 
     */
    public void setUsageCounterUpdate(Double usageCounterUpdate) {
        this.usageCounterUpdate = usageCounterUpdate;
    }

    /**
     * Get usage log data
     * @return 
     */
    public Double getUsageLogUpdate() {
        return usageLogUpdate;
    }

    /**
     * Set usage log data
     * @param usageLogUpdate 
     */
    public void setUsageLogUpdate(Double usageLogUpdate) {
        this.usageLogUpdate = usageLogUpdate;
    }

    /**
     * Get error log update
     * @return 
     */
    public Double getErrorLogUpdate() {
        return errorLogUpdate;
    }

    /**
     * Set error log update
     * @param errorLogUpdate 
     */
    public void setErrorLogUpdate(Double errorLogUpdate) {
        this.errorLogUpdate = errorLogUpdate;
    }

    /**
     * Get status
     * @return 
     */
    public Boolean isStatus() {
        return status;
    }

    /**
     * Set status
     * @param status 
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Get start time
     * @return 
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Set start time
     * @param startTime 
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Get end time
     * @return 
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Set end time
     * @param endTime 
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * Get elapsed time
     * @return 
     */
    public Double getElapsedTime() {
        return elapsedTime;
    }

    /**
     * Set elapsed time
     * @param elapsedTime 
     */
    public void setElapsedTime(Double elapsedTime) {
        this.elapsedTime = elapsedTime;
    }
    
    /**
     * Get version
     * @return 
     */
    public String getVersion() {
        return version;
    }

    /**
     * Set version
     * @param version 
     */
    public void setVersion(String version) {
        this.version = version;
    }
}
