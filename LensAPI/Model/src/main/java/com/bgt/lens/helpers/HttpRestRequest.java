// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.helpers;

import java.net.URL;

/**
 *
 * @author kjanakiraman
 */
public class HttpRestRequest {

    public URL webServiceURL;
    public String requestBody;
    public Enum.HttpMethod httpMethod;
    public Enum.Headers contentType;
    public Enum.Headers acceptType;

    public URL getWebServiceURL() {
        return webServiceURL;
    }

    public void setWebServiceURL(URL webServiceURL) {
        this.webServiceURL = webServiceURL;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public Enum.HttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(Enum.HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public Enum.Headers getContentType() {
        return contentType;
    }

    public void setContentType(Enum.Headers contentType) {
        this.contentType = contentType;
    }

    public Enum.Headers getAcceptType() {
        return acceptType;
    }

    public void setAcceptType(Enum.Headers acceptType) {
        this.acceptType = acceptType;
    }
    
    
}