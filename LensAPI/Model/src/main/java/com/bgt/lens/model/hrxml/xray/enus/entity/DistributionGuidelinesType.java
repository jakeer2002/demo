//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.09.01 at 04:44:03 PM IST 
//


package com.bgt.lens.model.hrxml.xray.enus.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DistributionGuidelinesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DistributionGuidelinesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DistributeTo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://ns.hr-xml.org/2007-04-15}DistributionType">
 *                 &lt;sequence>
 *                   &lt;element name="ContactMethod" type="{http://ns.hr-xml.org/2007-04-15}ContactMethodType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DoNotDistributeTo" type="{http://ns.hr-xml.org/2007-04-15}DistributionType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DistributionGuidelinesType", propOrder = {
    "distributeTo",
    "doNotDistributeTo"
})
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class DistributionGuidelinesType {

    @XmlElement(name = "DistributeTo")
    protected List<DistributionGuidelinesType.DistributeTo> distributeTo;
    @XmlElement(name = "DoNotDistributeTo")
    protected List<DistributionType> doNotDistributeTo;

    /**
     * Gets the value of the distributeTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the distributeTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDistributeTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DistributionGuidelinesType.DistributeTo }
     * 
     * 
     */
    public List<DistributionGuidelinesType.DistributeTo> getDistributeTo() {
        if (distributeTo == null) {
            distributeTo = new ArrayList<DistributionGuidelinesType.DistributeTo>();
        }
        return this.distributeTo;
    }

    /**
     * Gets the value of the doNotDistributeTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the doNotDistributeTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDoNotDistributeTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DistributionType }
     * 
     * 
     */
    public List<DistributionType> getDoNotDistributeTo() {
        if (doNotDistributeTo == null) {
            doNotDistributeTo = new ArrayList<DistributionType>();
        }
        return this.doNotDistributeTo;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://ns.hr-xml.org/2007-04-15}DistributionType">
     *       &lt;sequence>
     *         &lt;element name="ContactMethod" type="{http://ns.hr-xml.org/2007-04-15}ContactMethodType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contactMethod"
    })
    @JsonInclude(value=JsonInclude.Include.NON_EMPTY)
    public static class DistributeTo
        extends DistributionType
    {

        @XmlElement(name = "ContactMethod")
        protected List<ContactMethodType> contactMethod;

        /**
         * Gets the value of the contactMethod property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the contactMethod property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContactMethod().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ContactMethodType }
         * 
         * 
         */
        public List<ContactMethodType> getContactMethod() {
            if (contactMethod == null) {
                contactMethod = new ArrayList<ContactMethodType>();
            }
            return this.contactMethod;
        }

    }

}
