// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * FetchResume
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class FetchResume {
    
    /**
     * ResumeId
     */
    @XmlElement(name = "ResumeId")
    protected String resumeId;
    
    /**
     * Resume
     */
    @XmlElement(name = "Resume")
    protected Object resume;
    
    /**
     * Vendor
     */
    @XmlElement(name = "Vendor")
    protected String vendor;
    
    /**
     * InstanceType
     */
    @XmlElement(name = "InstanceType")
    protected String instanceType;
    
    /**
     * Locale
     */
    @XmlElement(name = "Locale")
    protected String locale;

    /**
     * Get resume id
     * @return 
     */
    public String getResumeId() {
        return resumeId;
    }

    /**
     * Set resume id
     * @param resumeId 
     */
    public void setResumeId(String resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * Get resume
     * @return 
     */
    public Object getResume() {
        return resume;
    }

    /**
     * Set resume
     * @param resume 
     */
    public void setResume(Object resume) {
        this.resume = resume;
    }

    /**
     * Get vendor
     * @return 
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * Set vendor
     * @param vendor 
     */
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * Get instance type
     * @return 
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     * @param instanceType 
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get locale
     * @return 
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set locale
     * @param locale 
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }
    
    
}
