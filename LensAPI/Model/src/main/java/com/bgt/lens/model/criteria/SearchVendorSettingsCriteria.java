// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import java.util.List;

/**
 * Search vendor settings criteria
 */
public class SearchVendorSettingsCriteria {
    
    /**
     * Vendor Id
     */
    protected Integer vendorId;
    
    /**
     * Vendor name
     */
    protected String vendorName;
    
    /**
     * Instance Id
     */
    protected Integer instanceId;
    
    /**
     * Document Type
     */
    protected String type;
    
//    /**
//     * Doc server service label
//     */
//    protected String docServer;
//    
//    /**
//     * Hypercube service label
//     */
//    protected String hypercubeServer;
    
    /**
     * Status of the vendor
     */
    protected String status;
    
    /**
     * Client Id
     */
    protected Integer clientId;
    
    /**
     * LENS settings id
     */
    protected List<Integer> lensSettingsId;
    
    /**
     * Get vendor id
     * @return 
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * Set vendor Id
     * @param vendorId 
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * Get vendor name
     * @return 
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * Set vendor name
     * @param vendorName 
     */
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    /**
     * Get instance Id
     * @return 
     */
    public Integer getInstanceId() {
        return instanceId;
    }

    /**
     * Set instance Id
     * @param instanceId 
     */
    public void setInstanceId(Integer instanceId) {
        this.instanceId = instanceId;
    }

    /**
     * Get type
     * @return 
     */
    public String getType() {
        return type;
    }

    /**
     * Set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }

//    /**
//     * Get doc server
//     * @return 
//     */
//    public String getDocServer() {
//        return docServer;
//    }
//
//    /**
//     * Set doc server
//     * @param docServer 
//     */
//    public void setDocServer(String docServer) {
//        this.docServer = docServer;
//    }
//
//    /**
//     * Get hypercube server
//     * @return 
//     */
//    public String getHypercubeServer() {
//        return hypercubeServer;
//    }
//
//    /**
//     * Set hypercube server
//     * @param hypercubeServer 
//     */
//    public void setHypercubeServer(String hypercubeServer) {
//        this.hypercubeServer = hypercubeServer;
//    }

    /**
     * Get status
     * @return 
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set status
     * @param status 
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    /**
     * Get client id
     * @return 
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client id
     * @param clientId 
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get lens settings id list
     * @return 
     */
    public List<Integer> getLensSettingsId() {
        return lensSettingsId;
    }

    /**
     * Set lens settings id list
     * @param lensSettingsId 
     */
    public void setLensSettingsId(List<Integer> lensSettingsId) {
        this.lensSettingsId = lensSettingsId;
    }
}
