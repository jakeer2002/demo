// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

/**
 * User Criteria to get data from database
 */
public class UserCriteria {

    /**
     * User ID
     */
    public Integer userId;

    /**
     * Email
     */
    public String email;

    /**
     * API Access
     */
    public Boolean apiaccess;

    /**
     * Enabled
     */
    public Boolean enabled;

    /**
     * Consumer Key
     */
    public String consumerKey;

    /**
     * Get User ID
     *
     * @return
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * Set User ID
     *
     * @param userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * Get Email
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set Email
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Is API access
     *
     * @return
     */
    public Boolean isApiaccess() {
        return apiaccess;
    }

    /**
     * Set API access
     *
     * @param apiaccess
     */
    public void setApiaccess(Boolean apiaccess) {
        this.apiaccess = apiaccess;
    }

    /**
     * Is Enabled
     *
     * @return
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Set Enabled
     *
     * @param enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Get Consumer Key
     *
     * @return
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set Consumer Key
     *
     * @param consumerKey
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }
}
