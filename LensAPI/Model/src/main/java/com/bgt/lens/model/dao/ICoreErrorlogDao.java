// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.ErrorLogCriteria;
import com.bgt.lens.model.entity.CoreErrorlog;
import java.util.List;

/**
 *
 * Error log Dao interface
 */
public interface ICoreErrorlogDao {

    /**
     * Create Error log
     *
     * @param coreErrorlog
     */
    public void save(CoreErrorlog coreErrorlog);

    /**
     * Update Error log
     *
     * @param coreErrorlog
     */
    public void update(CoreErrorlog coreErrorlog);

    /**
     * Delete Error log
     *
     * @param coreErrorlog
     */
    public void delete(CoreErrorlog coreErrorlog);

    /**
     * Get Error log by id
     *
     * @param errorLogId
     * @return
     */
    public CoreErrorlog getErrorLogById(String errorLogId);

    /**
     * Get Error log by Id and client
     *
     * @param errorLogId
     * @param clientId
     * @return
     */
    public CoreErrorlog getErrorLogById(String errorLogId, int clientId);

    /**
     * Get Error log by criteria
     *
     * @param errorLogCriteria
     * @return
     */
    public List<CoreErrorlog> getErrorLogByCriteria(ErrorLogCriteria errorLogCriteria);

}
