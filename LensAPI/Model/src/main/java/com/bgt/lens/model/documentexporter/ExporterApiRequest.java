// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.documentexporter;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@XmlRootElement(name = "request")
@XmlType(propOrder = {"client", "runTime", "locale", "acquiredDate", "fileExtension", "resource", "instanceType", "payLoad", "requestId", "isParsed"})
@JsonPropertyOrder({"client", "runTime", "locale", "acquiredDate", "fileExtension", "resource", "instanceType", "payLoad", "requestId", "isParsed"})
public class ExporterApiRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    private String client;
    private String runTime;
    private String locale;
    private String payLoad;
    private String fileExtension;
    private String instanceType;
    private String resource;
    private String requestId;
    private String acquiredDate;
    private Boolean isParsed;

    public ExporterApiRequest() {

    }

    public ExporterApiRequest(String client, String runTime, String locale, String payLoad, String fileExtension,
            String instanceType, String resource, String requestId, String acquiredDate, Boolean isParsed) {
        this.client = client;
        this.runTime = runTime;
        this.locale = locale;
        this.payLoad = payLoad;
        this.fileExtension = fileExtension;
        this.instanceType = instanceType;
        this.resource = resource;
        this.requestId = requestId;
        this.acquiredDate = acquiredDate;
        this.isParsed = isParsed;

    }

    public boolean getIsParsed() {
        return isParsed;
    }

    public void setIsParsed(boolean isParsed) {
        this.isParsed = isParsed;

    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getPayLoad() {
        return payLoad;
    }

    public void setPayLoad(String payLoad) {
        this.payLoad = payLoad;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getInstanceType() {
        return instanceType;
    }

    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getAcquiredDate() {
        return acquiredDate;
    }

    public void setAcquiredDate(String acquiredDate) {
        this.acquiredDate = acquiredDate;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;

    }

}
