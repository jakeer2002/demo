// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.SearchLookupCriteria;
import com.bgt.lens.model.entity.SearchLookup;
import java.util.List;

/**
 *
 * Search Lookup Dao interface
 */
public interface ISearchLookupDao {
    /**
     * Create lookup
     *
     * @param searchLookup
     */
    public void save(SearchLookup searchLookup);

    /**
     * Update lookup
     *
     * @param searchLookup
     */
    public void update(SearchLookup searchLookup);

    /**
     * Delete lookup
     *
     * @param searchLookup
     */
    public void delete(SearchLookup searchLookup);
    
    /**
     * Get lookup by Id
     * @param lookupId
     * @return 
     */
    public SearchLookup getLookupById(int lookupId);
    
    /**
     * Get lookup by criteria
     *
     * @param searchLookupCriteria
     * @return
     */
    public List<SearchLookup> getLookupsByCriteria(SearchLookupCriteria searchLookupCriteria);
}
