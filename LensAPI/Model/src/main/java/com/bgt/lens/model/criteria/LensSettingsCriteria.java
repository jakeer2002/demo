// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import com.bgt.lens.model.entity.CoreClient;

/**
 *
 * Lens settings criteria parameters to get data from database
 */
public class LensSettingsCriteria {

    private Integer id;

    /**
     * Lens host
     */
    private String host;

    /**
     * Lens port
     */
    private Integer port;

    /**
     * Encoding
     */
    private String characterSet;

    /**
     * Timeout
     */
    private Integer timeout;

    /**
     * Locale of the instance
     */
    private String locale;

    /**
     * Locale of the language
     */
    private String language;

    /**
     * Locale of the country
     */
    private String country;

    /**
     * HRXML version
     */
    private String hrxmlVersion;

    /**
     * HRXML version
     */
    private String hrxmlFileName;

    /**
     * Lens version
     */
    private String lensVersion;

    /**
     * Instance type (Lens/XRay/JM/TM)
     */
    private String instanceType;
    private String docServerResume;
    private String docServerPostings;
    private String hypercubeServerResume;

    public String getDocServerResume() {
        return docServerResume;
    }

    public void setDocServerResume(String docServerResume) {
        this.docServerResume = docServerResume;
    }

    public String getDocServerPostings() {
        return docServerPostings;
    }

    public void setDocServerPostings(String docServerPostings) {
        this.docServerPostings = docServerPostings;
    }

    public String getHypercubeServerResume() {
        return hypercubeServerResume;
    }

    public void setHypercubeServerResume(String hypercubeServerResume) {
        this.hypercubeServerResume = hypercubeServerResume;
    }

    public String getHypercubeServerPostings() {
        return hypercubeServerPostings;
    }

    public void setHypercubeServerPostings(String hypercubeServerPostings) {
        this.hypercubeServerPostings = hypercubeServerPostings;
    }
    private String hypercubeServerPostings;

    /**
     * Instance status active or not
     */
    private Boolean status;

    /**
     * whether projection hibernate include HRXML content
     */
    private Boolean isHRXML;

    /**
     * Client data
     */
    private Boolean isFailover;

    public Boolean isFailover() {
        return isFailover;
    }

    /**
     * Set Lens status
     *
     * @param status
     */
    public void setIsFailover(Boolean isFailover) {
        this.isFailover = isFailover;
    }

    private CoreClient coreClient;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get Is Lens active
     *
     * @return
     */
    public Boolean isStatus() {
        return status;
    }

    /**
     * Set Lens status
     *
     * @param status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Get Lens version
     *
     * @return
     */
    public String getLensVersion() {
        return this.lensVersion;
    }

    /**
     * Set Lens version
     *
     * @param lensVersion
     */
    public void setLensVersion(String lensVersion) {
        this.lensVersion = lensVersion;
    }

    /**
     * Get Instance type
     *
     * @return
     */
    public String getInstanceType() {
        return this.instanceType;
    }

    /**
     * Set instance type
     *
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get locale
     *
     * @return
     */
    public String getLocale() {
        return this.locale;
    }

    /**
     * Set locale
     *
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get Language
     *
     * @return
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Set Language
     *
     * @param language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Get Country
     *
     * @return
     */
    public String getCountry() {
        return country;
    }

    /**
     * Set Country
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Get hrxmlVersion
     *
     */
    public String getHrxmlVersion() {
        return hrxmlVersion;
    }

    /**
     * Set hrxmlVersion
     *
     * @param hrxmlVersion
     */
    public void setHrxmlVersion(String hrxmlVersion) {
        this.hrxmlVersion = hrxmlVersion;
    }

    public String getHrxmlFileName() {
        return hrxmlFileName;
    }

    public void setHrxmlFileName(String hrxmlFileName) {
        this.hrxmlFileName = hrxmlFileName;
    }

    /**
     * Get encoding
     *
     * @return
     */
    public String getCharacterSet() {
        return this.characterSet;
    }

    /**
     * Set encoding
     *
     * @param characterSet
     */
    public void setCharacterSet(String characterSet) {
        this.characterSet = characterSet;
    }

    /**
     * Get Lens host
     *
     * @return
     */
    public String getHost() {
        return this.host;
    }

    /**
     * Set Lens host
     *
     * @param host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Get Lens port
     *
     * @return
     */
    public Integer getPort() {
        return this.port;
    }

    /**
     * Set Lens port
     *
     * @param port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Get timeout
     *
     * @return
     */
    public Integer getTimeout() {
        return this.timeout;
    }

    /**
     * Set timeout
     *
     * @param timeout
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * Get HRXML contract or not
     *
     * @return
     */
    public Boolean isIsHRXML() {
        return isHRXML;
    }

    /**
     * Set HRXML contract - True or False
     *
     * @param isHRXML
     */
    public void setIsHRXML(Boolean isHRXML) {
        this.isHRXML = isHRXML;
    }

    /**
     * Get CoreClient of LenSettings
     *
     * @return
     */
    public CoreClient getCoreClient() {
        return coreClient;
    }

    /**
     * Set CoreClient
     *
     * @param coreClient
     */
    public void setCoreClient(CoreClient coreClient) {
        this.coreClient = coreClient;
    }

}
