// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Facet Distribution
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class FacetDistribution {
    /**
     * Dimension name
     */
    @XmlElement(name = "Name", required = false)
    protected String name;
    
    /**
     * Distribution list
     */
    @XmlElement(name = "Filter", required = false)
    protected List<Distribution> filter;
    
    /**
     * Facet URI applied
     */
    @XmlElement(name = "Uri", required = false)
    protected String uri;
    
    /**
     * Filter list count
     */
    @XmlElement(name = "FilterCount", required = false)
    protected Integer filterCount;
    
    /**
     * Affected documents count
     */
    @XmlElement(name = "DocumentCount", required = false)
    protected Integer documentCount;

    /**
     * Get dimension name
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * Set dimension name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get facet distribution
     * @return 
     */
    public List<Distribution> getFilter() {
        return filter;
    }

    /**
     * Set facet distribution
     * @param filter 
     */
    public void setFilter(List<Distribution> filter) {
        this.filter = filter;
    }

    /**
     * get facet uri
     * @return 
     */
    public String getUri() {
        return uri;
    }

    /**
     * Set facet uri
     * @param uri 
     */
    public void setUri(String uri) {
        this.uri = uri;
    }
    
    /**
     * Get facet filter count
     * @return 
     */
    public Integer getFilterCount() {
        return filterCount;
    }

    /**
     * Get filter count
     * @param filterCount 
     */
    public void setFilterCount(Integer filterCount) {
        this.filterCount = filterCount;
    }

    /**
     * Get document count
     * @return 
     */
    public Integer getDocumentCount() {
        return documentCount;
    }

    /**
     * Set document count
     * @param documentCount 
     */
    public void setDocumentCount(Integer documentCount) {
        this.documentCount = documentCount;
    }
}
