// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Search command dump
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class SearchCommandDump {
    /**
     * Id
     */
    @XmlElement(name = "Id", required = false)
    protected Integer id;
    
    /**
     * Request Id
     */
    @XmlElement(name = "RequestId", required = false)
    protected String requestId;
    
    /**
     * Client Id
     */
    @XmlElement(name = "ClientId", required = false)
    protected Integer clientId;
    
    /**
     * LENS settings Id
     */
    @XmlElement(name = "LensSettingsId", required = false)
    protected Integer lensSettingsId;
    
    /**
     * Vendor Id
     */
    @XmlElement(name = "VendorId", required = false)
    protected Integer vendorId;
    
    /**
     * Search command 
     */
    @XmlElement(name = "SearchCommand", required = false)
    protected String searchCommand;
    
    /**
     * Created on
     */
    @XmlElement(name = "CreatedOn", required = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    protected Date createdOn;
    
    @XmlElement(name = "DocType", required = false)
    protected String docType;

    /**
     * Get Id
     * @return 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get Request Id
     * @return 
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set Request Id
     * @param requestId 
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
    /**
     * Get client Id
     * @return 
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client Id
     * @param clientId 
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get LENS settings Id
     * @return 
     */
    public Integer getLensSettingsId() {
        return lensSettingsId;
    }

    /**
     * Set LENS settings Id
     * @param lensSettingsId 
     */
    public void setLensSettingsId(Integer lensSettingsId) {
        this.lensSettingsId = lensSettingsId;
    }

    /**
     * Get vendor Id
     * @return 
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * Set vendor Id
     * @param vendorId 
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * Get search command
     * @return 
     */
    public String getSearchCommand() {
        return searchCommand;
    }

    /**
     * Set search command
     * @param searchCommand 
     */
    public void setSearchCommand(String searchCommand) {
        this.searchCommand = searchCommand;
    }

    /**
     * Get created on
     * @return 
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * Set created on
     * @param createdOn 
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    /**
     * Get docType
     * @return 
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Set docType
     * @param docType 
     */
    public void setDocType(String docType) {
        this.docType = docType;
    }
}
