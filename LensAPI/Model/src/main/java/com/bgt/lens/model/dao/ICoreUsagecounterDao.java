// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.entity.CoreUsagecounter;
import java.util.List;

/**
 *
 * Usage counter Dao interface
 */
public interface ICoreUsagecounterDao {

    /**
     * Create usage counter
     *
     * @param coreUsageCounter
     */
    public void save(CoreUsagecounter coreUsageCounter);

    /**
     * Update usage counter
     *
     * @param coreUsageCounter
     */
    public void update(CoreUsagecounter coreUsageCounter);

    /**
     * Delete usage counter
     *
     * @param coreUsageCounter
     */
    public void delete(CoreUsagecounter coreUsageCounter);

    /**
     * Get usage counter by Id
     *
     * @param usageCounterId
     * @return
     */
    public CoreUsagecounter getUsageCounterById(Integer usageCounterId);

    /**
     * Get usage counter by id and client
     *
     * @param usageCounterId
     * @param clientId
     * @return
     */
    public CoreUsagecounter getUsageCounterById(Integer usageCounterId, Integer clientId);

    /**
     * Get usage counter by criteria
     *
     * @param usageCounterCriteria
     * @return
     */
    public List<CoreUsagecounter> getUsageCounterByCriteria(UsagecounterCriteria usageCounterCriteria);

}
