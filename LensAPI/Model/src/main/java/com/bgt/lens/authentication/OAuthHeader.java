// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.authentication;

/**
 *
 * OAuth header
 */
public class OAuthHeader {

    /**
     * Consumer key
     */
    protected String consumerKey;

    /**
     * Call back method
     */
    protected String callBack;

    /**
     * OAuth version
     */
    protected String version;

    /**
     * OAuth signature method
     */
    protected OAuth.SignatureMethods signatureMethod;

    /**
     * OAuth signature
     */
    protected String signature;

    /**
     * Timestamp
     */
    protected Long timeStamp;

    /**
     * Nonce
     */
    protected String nonce;

    /**
     * OAuth access token
     */
    protected String token;

    /**
     * Get consumer key
     *
     * @return
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     *
     * @param consumerKey
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get call back
     *
     * @return
     */
    public String getCallBack() {
        return callBack;
    }

    /**
     * Set call back
     *
     * @param callBack
     */
    public void setCallBack(String callBack) {
        this.callBack = callBack;
    }

    /**
     * Get OAuth version
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     * Set OAuth version
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Get OAuth signature method
     *
     * @return
     */
    public OAuth.SignatureMethods getSignatureMethod() {
        return signatureMethod;
    }

    /**
     * Set signature method
     *
     * @param signatureMethod
     */
    public void setSignatureMethod(OAuth.SignatureMethods signatureMethod) {
        this.signatureMethod = signatureMethod;
    }

    /**
     * Get OAuth signature
     *
     * @return
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Set signature
     *
     * @param signature
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    /**
     * Get OAuth timestamp
     *
     * @return
     */
    public Long getTimeStamp() {
        return timeStamp;
    }

    /**
     * Set OAuth timestamp
     *
     * @param timeStamp
     */
    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * Get Nonce
     *
     * @return
     */
    public String getNonce() {
        return nonce;
    }

    /**
     * Set nonce
     *
     * @param nonce
     */
    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    /**
     * Get access token
     *
     * @return
     */
    public String getToken() {
        return token;
    }

    /**
     * Set access token
     *
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

}
