// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.entity.SearchFilterLog;


public interface ISearchFilterLogDao {
    
    /**
     * Create Search Filter Log 
     *
     * @param searchFilterLog
     */
    public void save(SearchFilterLog searchFilterLog);

    /**
     * Update Search Filter Log 
     *
     * @param searchFilterLog
     */
    public void update(SearchFilterLog searchFilterLog);

    /**
     * Delete Search Filter Log
     *
     * @param searchFilterLog
     */
    public void delete(SearchFilterLog searchFilterLog);
    
}
