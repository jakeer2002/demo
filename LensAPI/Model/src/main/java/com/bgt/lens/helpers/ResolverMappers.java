// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.helpers;

import com.bgt.lens.model.parserservice.request.ParseResumeBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeHRXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeStructuredBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeWithHTMRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeWithRTFRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.adminservice.AddLensSettingsResponse;
import com.bgt.lens.model.rest.response.adminservice.AddResourceResponse;
import com.bgt.lens.model.rest.response.adminservice.CreateApiResponse;
import com.bgt.lens.model.rest.response.adminservice.CreateConsumerResponse;
import com.bgt.lens.model.rest.response.adminservice.DeleteApiResponse;
import com.bgt.lens.model.rest.response.adminservice.DeleteConsumerResponse;
import com.bgt.lens.model.rest.response.adminservice.DeleteLensSettingsResponse;
import com.bgt.lens.model.rest.response.adminservice.DeleteResourceResponse;
import com.bgt.lens.model.rest.response.adminservice.GetAllLicenseDetailsResponse;
import com.bgt.lens.model.rest.response.adminservice.GetApiByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetApiResponse;
import com.bgt.lens.model.rest.response.adminservice.GetClientByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetClientsResponse;
import com.bgt.lens.model.rest.response.adminservice.GetErrorLogByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetErrorLogsResponse;
import com.bgt.lens.model.rest.response.adminservice.GetLensSettingsByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetLensSettingsResponse;
import com.bgt.lens.model.rest.response.adminservice.GetLicenseResponse;
import com.bgt.lens.model.rest.response.adminservice.GetResourceByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetResourceResponse;
import com.bgt.lens.model.rest.response.adminservice.GetUsageCounterByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetUsageCounterResponse;
import com.bgt.lens.model.rest.response.adminservice.GetUsageLogByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetUsageLogsResponse;
import com.bgt.lens.model.rest.response.adminservice.UpdateApiResponse;
import com.bgt.lens.model.rest.response.adminservice.UpdateConsumerResponse;
import com.bgt.lens.model.rest.response.adminservice.UpdateLensSettingsResponse;
import com.bgt.lens.model.rest.response.adminservice.UpdateResourceResponse;
import com.bgt.lens.model.rest.response.parserservice.CanonResponse;
import com.bgt.lens.model.rest.response.parserservice.ConvertResponse;
import com.bgt.lens.model.rest.response.parserservice.InfoResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobBGXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobStructuredBGXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobWithHTMResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobWithRTFResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeBGXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeHRXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeStructuredBGXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeWithHTMResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeWithRTFResponse;
import com.bgt.lens.model.rest.response.parserservice.PingResponse;
import com.bgt.lens.model.rest.response.parserservice.ProcessLocaleResponse;

public class ResolverMappers {

    public InfoResponse getinfo(ApiResponse apiResponse) {
        InfoResponse infoResponse = new InfoResponse();
        infoResponse.setRequestId(apiResponse.requestId);
        infoResponse.setResponseData(apiResponse.responseData);
        infoResponse.setStatus(apiResponse.status);
        infoResponse.setStatusCode(apiResponse.statusCode);
        infoResponse.setTimeStamp(apiResponse.timeStamp);
        return infoResponse;
    }

    public PingResponse getPing(ApiResponse apiResponse) {
        PingResponse pingResponse = new PingResponse();
        pingResponse.setRequestId(apiResponse.requestId);
        pingResponse.setResponseData(apiResponse.responseData);
        pingResponse.setStatus(apiResponse.status);
        pingResponse.setStatusCode(apiResponse.statusCode);
        pingResponse.setTimeStamp(apiResponse.timeStamp);
        return pingResponse;
    }

    public ConvertResponse convertBinaryData(ApiResponse apiResponse) {
        ConvertResponse convertResponse = new ConvertResponse();
        convertResponse.setRequestId(apiResponse.requestId);
        convertResponse.setResponseData(apiResponse.responseData);
        convertResponse.setStatus(apiResponse.status);
        convertResponse.setStatusCode(apiResponse.statusCode);
        convertResponse.setTimeStamp(apiResponse.timeStamp);
        return convertResponse;
    }

    public CanonResponse canonBinaryData(ApiResponse apiResponse) {
        CanonResponse canonResponse = new CanonResponse();
        canonResponse.setRequestId(apiResponse.requestId);
        canonResponse.setResponseData(apiResponse.responseData);
        canonResponse.setStatus(apiResponse.status);
        canonResponse.setStatusCode(apiResponse.statusCode);
        canonResponse.setTimeStamp(apiResponse.timeStamp);
        return canonResponse;
    }
//SIC

    public ParseResumeWithHTMRequest parseResumeWithHTMRequest(ParseResumeRequest parseResumeRequest) {
        ParseResumeWithHTMRequest canonResponse = new ParseResumeWithHTMRequest();
        canonResponse.setBinaryData(parseResumeRequest.getBinaryData());
        canonResponse.setExtension(parseResumeRequest.getExtension());
        canonResponse.setInstanceType(parseResumeRequest.getInstanceType());
        canonResponse.setLocale(parseResumeRequest.getLocale());

        return canonResponse;
    }
    
    public ParseResumeWithRTFRequest parseResumeWithRTFRequest(ParseResumeRequest parseResumeRequest) {
        ParseResumeWithRTFRequest canonResponse = new ParseResumeWithRTFRequest();
        canonResponse.setBinaryData(parseResumeRequest.getBinaryData());
        canonResponse.setExtension(parseResumeRequest.getExtension());
        canonResponse.setInstanceType(parseResumeRequest.getInstanceType());
        canonResponse.setLocale(parseResumeRequest.getLocale());

        return canonResponse;
    }
    
     public ParseResumeBGXMLRequest parseResumeBGXMLRequest(ParseResumeRequest parseResumeRequest) {
        ParseResumeBGXMLRequest canonResponse = new ParseResumeBGXMLRequest();
        canonResponse.setBinaryData(parseResumeRequest.getBinaryData());
        canonResponse.setExtension(parseResumeRequest.getExtension());
        canonResponse.setInstanceType(parseResumeRequest.getInstanceType());
        canonResponse.setLocale(parseResumeRequest.getLocale());

        return canonResponse;
    }
     public ParseResumeHRXMLRequest parseResumeHRXMLRequest(ParseResumeRequest parseResumeRequest) {
        ParseResumeHRXMLRequest canonResponse = new ParseResumeHRXMLRequest();
        canonResponse.setBinaryData(parseResumeRequest.getBinaryData());
        canonResponse.setExtension(parseResumeRequest.getExtension());
        canonResponse.setInstanceType(parseResumeRequest.getInstanceType());
        canonResponse.setLocale(parseResumeRequest.getLocale());

        return canonResponse;
    }
      public ParseResumeStructuredBGXMLRequest parseResumeStructuredBGXMLRequest(ParseResumeRequest parseResumeRequest) {
        ParseResumeStructuredBGXMLRequest canonResponse = new ParseResumeStructuredBGXMLRequest();
        canonResponse.setBinaryData(parseResumeRequest.getBinaryData());
        canonResponse.setExtension(parseResumeRequest.getExtension());
        canonResponse.setInstanceType(parseResumeRequest.getInstanceType());
        canonResponse.setLocale(parseResumeRequest.getLocale());

        return canonResponse;
    }

    public ParseResumeResponse parseResume(ApiResponse apiResponse) {
        ParseResumeResponse parseResumeResponse = new ParseResumeResponse();
        parseResumeResponse.setRequestId(apiResponse.requestId);
        parseResumeResponse.setResponseData(apiResponse.responseData);
        parseResumeResponse.setStatus(apiResponse.status);
        parseResumeResponse.setStatusCode(apiResponse.statusCode);
        parseResumeResponse.setTimeStamp(apiResponse.timeStamp);
        return parseResumeResponse;
    }

    public ParseResumeBGXMLResponse parseResumeBGXML(ApiResponse apiResponse) {
        ParseResumeBGXMLResponse parseResumeBGXMLResponse = new ParseResumeBGXMLResponse();
        parseResumeBGXMLResponse.setRequestId(apiResponse.requestId);
        parseResumeBGXMLResponse.setResponseData(apiResponse.responseData);
        parseResumeBGXMLResponse.setStatus(apiResponse.status);
        parseResumeBGXMLResponse.setStatusCode(apiResponse.statusCode);
        parseResumeBGXMLResponse.setTimeStamp(apiResponse.timeStamp);
        return parseResumeBGXMLResponse;
    }

    public ParseResumeStructuredBGXMLResponse parseResumeStructuredBGXML(ApiResponse apiResponse) {
        ParseResumeStructuredBGXMLResponse parseResumeStructuredBGXMLResponse = new ParseResumeStructuredBGXMLResponse();
        parseResumeStructuredBGXMLResponse.setRequestId(apiResponse.requestId);
        parseResumeStructuredBGXMLResponse.setResponseData(apiResponse.responseData);
        parseResumeStructuredBGXMLResponse.setStatus(apiResponse.status);
        parseResumeStructuredBGXMLResponse.setStatusCode(apiResponse.statusCode);
        parseResumeStructuredBGXMLResponse.setTimeStamp(apiResponse.timeStamp);
        return parseResumeStructuredBGXMLResponse;
    }

    public ParseResumeHRXMLResponse parseResumeHRXML(ApiResponse apiResponse) {
        ParseResumeHRXMLResponse parseResumeHRXMLResponse = new ParseResumeHRXMLResponse();
        parseResumeHRXMLResponse.setRequestId(apiResponse.requestId);
        parseResumeHRXMLResponse.setResponseData(apiResponse.responseData);
        parseResumeHRXMLResponse.setStatus(apiResponse.status);
        parseResumeHRXMLResponse.setStatusCode(apiResponse.statusCode);
        parseResumeHRXMLResponse.setTimeStamp(apiResponse.timeStamp);
        return parseResumeHRXMLResponse;
    }

    public ParseResumeWithRTFResponse parseResumeWithRTF(ApiResponse apiResponse) {
        ParseResumeWithRTFResponse parseResumeWithRTFResponse = new ParseResumeWithRTFResponse();
        parseResumeWithRTFResponse.setRequestId(apiResponse.requestId);
        parseResumeWithRTFResponse.setResponseData(apiResponse.responseData);
        parseResumeWithRTFResponse.setStatus(apiResponse.status);
        parseResumeWithRTFResponse.setStatusCode(apiResponse.statusCode);
        parseResumeWithRTFResponse.setTimeStamp(apiResponse.timeStamp);
        return parseResumeWithRTFResponse;
    }

    public ParseResumeWithHTMResponse parseResumeWithHTM(ApiResponse apiResponse) {
        ParseResumeWithHTMResponse parseResumeWithHTMResponse = new ParseResumeWithHTMResponse();
        parseResumeWithHTMResponse.setRequestId(apiResponse.requestId);
        parseResumeWithHTMResponse.setResponseData(apiResponse.responseData);
        parseResumeWithHTMResponse.setStatus(apiResponse.status);
        parseResumeWithHTMResponse.setStatusCode(apiResponse.statusCode);
        parseResumeWithHTMResponse.setTimeStamp(apiResponse.timeStamp);
        return parseResumeWithHTMResponse;
    }

    public ParseJobResponse parseJob(ApiResponse apiResponse) {
        ParseJobResponse parseJobResponse = new ParseJobResponse();
        parseJobResponse.setRequestId(apiResponse.requestId);
        parseJobResponse.setResponseData(apiResponse.responseData);
        parseJobResponse.setStatus(apiResponse.status);
        parseJobResponse.setStatusCode(apiResponse.statusCode);
        parseJobResponse.setTimeStamp(apiResponse.timeStamp);
        return parseJobResponse;
    }

    public ParseJobBGXMLResponse parseJobBGXML(ApiResponse apiResponse) {
        ParseJobBGXMLResponse parseJobBGXMLResponse = new ParseJobBGXMLResponse();
        parseJobBGXMLResponse.setRequestId(apiResponse.requestId);
        parseJobBGXMLResponse.setResponseData(apiResponse.responseData);
        parseJobBGXMLResponse.setStatus(apiResponse.status);
        parseJobBGXMLResponse.setStatusCode(apiResponse.statusCode);
        parseJobBGXMLResponse.setTimeStamp(apiResponse.timeStamp);
        return parseJobBGXMLResponse;
    }

    public ParseJobStructuredBGXMLResponse parseJobStructuredBGXML(ApiResponse apiResponse) {
        ParseJobStructuredBGXMLResponse parseJobStructuredBGXMLResponse = new ParseJobStructuredBGXMLResponse();
        parseJobStructuredBGXMLResponse.setRequestId(apiResponse.requestId);
        parseJobStructuredBGXMLResponse.setResponseData(apiResponse.responseData);
        parseJobStructuredBGXMLResponse.setStatus(apiResponse.status);
        parseJobStructuredBGXMLResponse.setStatusCode(apiResponse.statusCode);
        parseJobStructuredBGXMLResponse.setTimeStamp(apiResponse.timeStamp);
        return parseJobStructuredBGXMLResponse;
    }

    public ParseJobWithRTFResponse parseJobWithRFT(ApiResponse apiResponse) {
        ParseJobWithRTFResponse parseJobWithRTFResponse = new ParseJobWithRTFResponse();
        parseJobWithRTFResponse.setRequestId(apiResponse.requestId);
        parseJobWithRTFResponse.setResponseData(apiResponse.responseData);
        parseJobWithRTFResponse.setStatus(apiResponse.status);
        parseJobWithRTFResponse.setStatusCode(apiResponse.statusCode);
        parseJobWithRTFResponse.setTimeStamp(apiResponse.timeStamp);
        return parseJobWithRTFResponse;
    }

    public ParseJobWithHTMResponse parseJobWithHTM(ApiResponse apiResponse) {
        ParseJobWithHTMResponse parseJobWithHTMResponse = new ParseJobWithHTMResponse();
        parseJobWithHTMResponse.setRequestId(apiResponse.requestId);
        parseJobWithHTMResponse.setResponseData(apiResponse.responseData);
        parseJobWithHTMResponse.setStatus(apiResponse.status);
        parseJobWithHTMResponse.setStatusCode(apiResponse.statusCode);
        parseJobWithHTMResponse.setTimeStamp(apiResponse.timeStamp);
        return parseJobWithHTMResponse;
    }

    public ProcessLocaleResponse findLocale(ApiResponse apiResponse) {
        ProcessLocaleResponse processLocaleResponse = new ProcessLocaleResponse();
        processLocaleResponse.setRequestId(apiResponse.requestId);
        processLocaleResponse.setResponseData(apiResponse.responseData);
        processLocaleResponse.setStatus(apiResponse.status);
        processLocaleResponse.setStatusCode(apiResponse.statusCode);
        processLocaleResponse.setTimeStamp(apiResponse.timeStamp);
        return processLocaleResponse;
    }

    public CreateApiResponse createApi(ApiResponse apiResponse) {
        CreateApiResponse createApiResponse = new CreateApiResponse();
        createApiResponse.setRequestId(apiResponse.requestId);
        createApiResponse.setResponseData(apiResponse.responseData);
        createApiResponse.setStatus(apiResponse.status);
        createApiResponse.setStatusCode(apiResponse.statusCode);
        createApiResponse.setTimeStamp(apiResponse.timeStamp);
        return createApiResponse;
    }

    public UpdateApiResponse updateApi(ApiResponse apiResponse) {
        UpdateApiResponse updateApiResponse = new UpdateApiResponse();
        updateApiResponse.setRequestId(apiResponse.requestId);
        updateApiResponse.setResponseData(apiResponse.responseData);
        updateApiResponse.setStatus(apiResponse.status);
        updateApiResponse.setStatusCode(apiResponse.statusCode);
        updateApiResponse.setTimeStamp(apiResponse.timeStamp);
        return updateApiResponse;
    }

    public DeleteApiResponse deleteApi(ApiResponse apiResponse) {
        DeleteApiResponse deleteApiResponse = new DeleteApiResponse();
        deleteApiResponse.setRequestId(apiResponse.requestId);
        deleteApiResponse.setResponseData(apiResponse.responseData);
        deleteApiResponse.setStatus(apiResponse.status);
        deleteApiResponse.setStatusCode(apiResponse.statusCode);
        deleteApiResponse.setTimeStamp(apiResponse.timeStamp);
        return deleteApiResponse;
    }

    public GetApiResponse getApi(ApiResponse apiResponse) {
        GetApiResponse getApiResponse = new GetApiResponse();
        getApiResponse.setRequestId(apiResponse.requestId);
        getApiResponse.setResponseData(apiResponse.responseData);
        getApiResponse.setStatus(apiResponse.status);
        getApiResponse.setStatusCode(apiResponse.statusCode);
        getApiResponse.setTimeStamp(apiResponse.timeStamp);
        return getApiResponse;
    }

    public GetApiByIdResponse getApiById(ApiResponse apiResponse) {
        GetApiByIdResponse getApiByIdResponse = new GetApiByIdResponse();
        getApiByIdResponse.setRequestId(apiResponse.requestId);
        getApiByIdResponse.setResponseData(apiResponse.responseData);
        getApiByIdResponse.setStatus(apiResponse.status);
        getApiByIdResponse.setStatusCode(apiResponse.statusCode);
        getApiByIdResponse.setTimeStamp(apiResponse.timeStamp);
        return getApiByIdResponse;
    }

    public AddResourceResponse addResource(ApiResponse apiResponse) {
        AddResourceResponse addResourceResponse = new AddResourceResponse();
        addResourceResponse.setRequestId(apiResponse.requestId);
        addResourceResponse.setResponseData(apiResponse.responseData);
        addResourceResponse.setStatus(apiResponse.status);
        addResourceResponse.setStatusCode(apiResponse.statusCode);
        addResourceResponse.setTimeStamp(apiResponse.timeStamp);
        return addResourceResponse;
    }

    public UpdateResourceResponse updateResource(ApiResponse apiResponse) {
        UpdateResourceResponse updateResourceResponse = new UpdateResourceResponse();
        updateResourceResponse.setRequestId(apiResponse.requestId);
        updateResourceResponse.setResponseData(apiResponse.responseData);
        updateResourceResponse.setStatus(apiResponse.status);
        updateResourceResponse.setStatusCode(apiResponse.statusCode);
        updateResourceResponse.setTimeStamp(apiResponse.timeStamp);
        return updateResourceResponse;
    }

    public DeleteResourceResponse deleteResource(ApiResponse apiResponse) {
        DeleteResourceResponse deleteResourceResponse = new DeleteResourceResponse();
        deleteResourceResponse.setRequestId(apiResponse.requestId);
        deleteResourceResponse.setResponseData(apiResponse.responseData);
        deleteResourceResponse.setStatus(apiResponse.status);
        deleteResourceResponse.setStatusCode(apiResponse.statusCode);
        deleteResourceResponse.setTimeStamp(apiResponse.timeStamp);
        return deleteResourceResponse;
    }

    public GetResourceResponse getResource(ApiResponse apiResponse) {
        GetResourceResponse getResourceResponse = new GetResourceResponse();
        getResourceResponse.setRequestId(apiResponse.requestId);
        getResourceResponse.setResponseData(apiResponse.responseData);
        getResourceResponse.setStatus(apiResponse.status);
        getResourceResponse.setStatusCode(apiResponse.statusCode);
        getResourceResponse.setTimeStamp(apiResponse.timeStamp);
        return getResourceResponse;
    }

    public GetResourceByIdResponse getResourcebyId(ApiResponse apiResponse) {
        GetResourceByIdResponse getResourceByIdResponse = new GetResourceByIdResponse();
        getResourceByIdResponse.setRequestId(apiResponse.requestId);
        getResourceByIdResponse.setResponseData(apiResponse.responseData);
        getResourceByIdResponse.setStatus(apiResponse.status);
        getResourceByIdResponse.setStatusCode(apiResponse.statusCode);
        getResourceByIdResponse.setTimeStamp(apiResponse.timeStamp);
        return getResourceByIdResponse;
    }

    public AddLensSettingsResponse addLensSettings(ApiResponse apiResponse) {
        AddLensSettingsResponse addLensSettingsResponse = new AddLensSettingsResponse();
        addLensSettingsResponse.setRequestId(apiResponse.requestId);
        addLensSettingsResponse.setResponseData(apiResponse.responseData);
        addLensSettingsResponse.setStatus(apiResponse.status);
        addLensSettingsResponse.setStatusCode(apiResponse.statusCode);
        addLensSettingsResponse.setTimeStamp(apiResponse.timeStamp);
        return addLensSettingsResponse;
    }

    public UpdateLensSettingsResponse updateLensSettings(ApiResponse apiResponse) {
        UpdateLensSettingsResponse UpdateLensSettingsResponse = new UpdateLensSettingsResponse();
        UpdateLensSettingsResponse.setRequestId(apiResponse.requestId);
        UpdateLensSettingsResponse.setResponseData(apiResponse.responseData);
        UpdateLensSettingsResponse.setStatus(apiResponse.status);
        UpdateLensSettingsResponse.setStatusCode(apiResponse.statusCode);
        UpdateLensSettingsResponse.setTimeStamp(apiResponse.timeStamp);
        return UpdateLensSettingsResponse;
    }

    public DeleteLensSettingsResponse deleteLensSettings(ApiResponse apiResponse) {
        DeleteLensSettingsResponse deleteLensSettingsResponse = new DeleteLensSettingsResponse();
        deleteLensSettingsResponse.setRequestId(apiResponse.requestId);
        deleteLensSettingsResponse.setResponseData(apiResponse.responseData);
        deleteLensSettingsResponse.setStatus(apiResponse.status);
        deleteLensSettingsResponse.setStatusCode(apiResponse.statusCode);
        deleteLensSettingsResponse.setTimeStamp(apiResponse.timeStamp);
        return deleteLensSettingsResponse;
    }

    public GetLensSettingsResponse getLensSettings(ApiResponse apiResponse) {
        GetLensSettingsResponse getLensSettingsResponse = new GetLensSettingsResponse();
        getLensSettingsResponse.setRequestId(apiResponse.requestId);
        getLensSettingsResponse.setResponseData(apiResponse.responseData);
        getLensSettingsResponse.setStatus(apiResponse.status);
        getLensSettingsResponse.setStatusCode(apiResponse.statusCode);
        getLensSettingsResponse.setTimeStamp(apiResponse.timeStamp);
        return getLensSettingsResponse;
    }

    public GetLensSettingsByIdResponse getLensSettingsById(ApiResponse apiResponse) {
        GetLensSettingsByIdResponse getLensSettingsByIdResponse = new GetLensSettingsByIdResponse();
        getLensSettingsByIdResponse.setRequestId(apiResponse.requestId);
        getLensSettingsByIdResponse.setResponseData(apiResponse.responseData);
        getLensSettingsByIdResponse.setStatus(apiResponse.status);
        getLensSettingsByIdResponse.setStatusCode(apiResponse.statusCode);
        getLensSettingsByIdResponse.setTimeStamp(apiResponse.timeStamp);
        return getLensSettingsByIdResponse;

    }

    public CreateConsumerResponse createConsumer(ApiResponse apiResponse) {
        CreateConsumerResponse createConsumerResponse = new CreateConsumerResponse();
        createConsumerResponse.setRequestId(apiResponse.requestId);
        createConsumerResponse.setResponseData(apiResponse.responseData);
        createConsumerResponse.setStatus(apiResponse.status);
        createConsumerResponse.setStatusCode(apiResponse.statusCode);
        createConsumerResponse.setTimeStamp(apiResponse.timeStamp);
        return createConsumerResponse;
    }

    public UpdateConsumerResponse updateConsumer(ApiResponse apiResponse) {
        UpdateConsumerResponse updateConsumerResponse = new UpdateConsumerResponse();
        updateConsumerResponse.setRequestId(apiResponse.requestId);
        updateConsumerResponse.setResponseData(apiResponse.responseData);
        updateConsumerResponse.setStatus(apiResponse.status);
        updateConsumerResponse.setStatusCode(apiResponse.statusCode);
        updateConsumerResponse.setTimeStamp(apiResponse.timeStamp);
        return updateConsumerResponse;
    }

    public DeleteConsumerResponse deleteConsumer(ApiResponse apiResponse) {
        DeleteConsumerResponse deleteConsumerResponse = new DeleteConsumerResponse();
        deleteConsumerResponse.setRequestId(apiResponse.requestId);
        deleteConsumerResponse.setResponseData(apiResponse.responseData);
        deleteConsumerResponse.setStatus(apiResponse.status);
        deleteConsumerResponse.setStatusCode(apiResponse.statusCode);
        deleteConsumerResponse.setTimeStamp(apiResponse.timeStamp);
        return deleteConsumerResponse;
    }

    public GetClientsResponse getClients(ApiResponse apiResponse) {
        GetClientsResponse getClientsResponse = new GetClientsResponse();
        getClientsResponse.setRequestId(apiResponse.requestId);
        getClientsResponse.setResponseData(apiResponse.responseData);
        getClientsResponse.setStatus(apiResponse.status);
        getClientsResponse.setStatusCode(apiResponse.statusCode);
        getClientsResponse.setTimeStamp(apiResponse.timeStamp);
        return getClientsResponse;
    }

    public GetClientByIdResponse getClientById(ApiResponse apiResponse) {
        GetClientByIdResponse getClientByIdResponse = new GetClientByIdResponse();
        getClientByIdResponse.setRequestId(apiResponse.requestId);
        getClientByIdResponse.setResponseData(apiResponse.responseData);
        getClientByIdResponse.setStatus(apiResponse.status);
        getClientByIdResponse.setStatusCode(apiResponse.statusCode);
        getClientByIdResponse.setTimeStamp(apiResponse.timeStamp);
        return getClientByIdResponse;
    }

    public GetAllLicenseDetailsResponse getLicenseDetails(ApiResponse apiResponse) {
        GetAllLicenseDetailsResponse getAllLicenseDetailsResponse = new GetAllLicenseDetailsResponse();
        getAllLicenseDetailsResponse.setRequestId(apiResponse.requestId);
        getAllLicenseDetailsResponse.setResponseData(apiResponse.responseData);
        getAllLicenseDetailsResponse.setStatus(apiResponse.status);
        getAllLicenseDetailsResponse.setStatusCode(apiResponse.statusCode);
        getAllLicenseDetailsResponse.setTimeStamp(apiResponse.timeStamp);
        return getAllLicenseDetailsResponse;
    }

    public GetLicenseResponse getLicenseDetailsById(ApiResponse apiResponse) {
        GetLicenseResponse getLicenseResponse = new GetLicenseResponse();
        getLicenseResponse.setRequestId(apiResponse.requestId);
        getLicenseResponse.setResponseData(apiResponse.responseData);
        getLicenseResponse.setStatus(apiResponse.status);
        getLicenseResponse.setStatusCode(apiResponse.statusCode);
        getLicenseResponse.setTimeStamp(apiResponse.timeStamp);
        return getLicenseResponse;
    }

    public GetUsageLogsResponse getUsageLogs(ApiResponse apiResponse) {
        GetUsageLogsResponse getUsageLogsResponse = new GetUsageLogsResponse();
        getUsageLogsResponse.setRequestId(apiResponse.requestId);
        getUsageLogsResponse.setResponseData(apiResponse.responseData);
        getUsageLogsResponse.setStatus(apiResponse.status);
        getUsageLogsResponse.setStatusCode(apiResponse.statusCode);
        getUsageLogsResponse.setTimeStamp(apiResponse.timeStamp);
        return getUsageLogsResponse;
    }

    public GetUsageLogByIdResponse getUsageLogsById(ApiResponse apiResponse) {
        GetUsageLogByIdResponse getUsageLogByIdResponse = new GetUsageLogByIdResponse();
        getUsageLogByIdResponse.setRequestId(apiResponse.requestId);
        getUsageLogByIdResponse.setResponseData(apiResponse.responseData);
        getUsageLogByIdResponse.setStatus(apiResponse.status);
        getUsageLogByIdResponse.setStatusCode(apiResponse.statusCode);
        getUsageLogByIdResponse.setTimeStamp(apiResponse.timeStamp);
        return getUsageLogByIdResponse;
    }

    public GetErrorLogsResponse getErrorLogs(ApiResponse apiResponse) {
        GetErrorLogsResponse getErrorLogsResponse = new GetErrorLogsResponse();
        getErrorLogsResponse.setRequestId(apiResponse.requestId);
        getErrorLogsResponse.setResponseData(apiResponse.responseData);
        getErrorLogsResponse.setStatus(apiResponse.status);
        getErrorLogsResponse.setStatusCode(apiResponse.statusCode);
        getErrorLogsResponse.setTimeStamp(apiResponse.timeStamp);
        return getErrorLogsResponse;
    }

    public GetErrorLogByIdResponse getErrorLogsById(ApiResponse apiResponse) {
        GetErrorLogByIdResponse getErrorLogByIdResponse = new GetErrorLogByIdResponse();
        getErrorLogByIdResponse.setRequestId(apiResponse.requestId);
        getErrorLogByIdResponse.setResponseData(apiResponse.responseData);
        getErrorLogByIdResponse.setStatus(apiResponse.status);
        getErrorLogByIdResponse.setStatusCode(apiResponse.statusCode);
        getErrorLogByIdResponse.setTimeStamp(apiResponse.timeStamp);
        return getErrorLogByIdResponse;
    }

    public GetUsageCounterResponse getUsageCounter(ApiResponse apiResponse) {
        GetUsageCounterResponse getUsageCounterResponse = new GetUsageCounterResponse();
        getUsageCounterResponse.setRequestId(apiResponse.requestId);
        getUsageCounterResponse.setResponseData(apiResponse.responseData);
        getUsageCounterResponse.setStatus(apiResponse.status);
        getUsageCounterResponse.setStatusCode(apiResponse.statusCode);
        getUsageCounterResponse.setTimeStamp(apiResponse.timeStamp);
        return getUsageCounterResponse;
    }

    public GetUsageCounterByIdResponse getUsageCounterById(ApiResponse apiResponse) {
        GetUsageCounterByIdResponse getUsageCounterByIdResponse = new GetUsageCounterByIdResponse();
        getUsageCounterByIdResponse.setRequestId(apiResponse.requestId);
        getUsageCounterByIdResponse.setResponseData(apiResponse.responseData);
        getUsageCounterByIdResponse.setStatus(apiResponse.status);
        getUsageCounterByIdResponse.setStatusCode(apiResponse.statusCode);
        getUsageCounterByIdResponse.setTimeStamp(apiResponse.timeStamp);
        return getUsageCounterByIdResponse;
    }
}
