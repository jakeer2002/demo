// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * SearchLookup
 */
@Entity
@Table(name = "search_lookup")
public class SearchLookup  implements java.io.Serializable {
    
    /**
     * Id
     */
     private Integer id;
     
     /**
      * Type
      */
     private String type;
     
     /**
      * Lookup Key
      */
     private String lookupKey;
     
     /**
      * Lookup value
      */
     private String lookupValue;

    public SearchLookup() {
    }

    public SearchLookup(String type, String lookupKey, String lookupValue) {
       this.type = type;
       this.lookupKey = lookupKey;
       this.lookupValue = lookupValue;
    }
   
    /**
     * Get Id
     * @return 
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    /**
     * Set Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * Get type
     * @return 
     */
    @Column(name = "Type", nullable = false)
    public String getType() {
        return this.type;
    }
    
    /**
     * Set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }
    
    /**
     * Get lookup key
     * @return 
     */
    @Column(name = "LookupKey", nullable = false)
    public String getLookupKey() {
        return this.lookupKey;
    }
    
    /**
     * Set lookup key
     * @param lookupKey 
     */
    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }
    
    /**
     * Get lookup value
     * @return 
     */
    @Column(name = "LookupValue", nullable = false)
    public String getLookupValue() {
        return this.lookupValue;
    }
    
    /**
     * Set lookup value
     * @param lookupValue 
     */
    public void setLookupValue(String lookupValue) {
        this.lookupValue = lookupValue;
    }
}


