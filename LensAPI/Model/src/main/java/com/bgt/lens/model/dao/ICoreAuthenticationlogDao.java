// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.entity.CoreAuthenticationlog;

/**
 *
 * Authentication log interface Dao
 */
public interface ICoreAuthenticationlogDao {

    /**
     * Create Authentication log
     *
     * @param coreAuthenticationlog
     */
    public void save(CoreAuthenticationlog coreAuthenticationlog);

    /**
     * Update authentication log
     *
     * @param coreAuthenticationlog
     */
    public void update(CoreAuthenticationlog coreAuthenticationlog);

    /**
     * Delete authentication log
     *
     * @param coreAuthenticationlog
     */
    public void delete(CoreAuthenticationlog coreAuthenticationlog);

    /**
     * Get authentication log by Id
     *
     * @param authenticationLogId
     * @return
     */
    public CoreAuthenticationlog getAuthenticationLogById(int authenticationLogId);
    
    /**
     * Get authentication log by request Id
     *
     * @param requestId
     * @return
     */
    public CoreAuthenticationlog getAuthenticationLogByRequestId(String requestId);
}
