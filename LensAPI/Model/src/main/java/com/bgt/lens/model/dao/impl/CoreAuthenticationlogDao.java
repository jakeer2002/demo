// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.dao.ICoreAuthenticationlogDao;
import com.bgt.lens.model.entity.CoreAuthenticationlog;

/**
 *
 * Authentication log table data management
 */
public class CoreAuthenticationlogDao implements ICoreAuthenticationlogDao {
    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(CoreAuthenticationlogDao.class);
    
    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Database Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize authentication log Dao
     */
    public CoreAuthenticationlogDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create authentication log
     *
     * @param coreAuthenticationlog
     */
    @Override
    public void save(CoreAuthenticationlog coreAuthenticationlog) {
        helperDao.save(sessionFactory, coreAuthenticationlog);
    }

    /**
     * Update authentication log
     *
     * @param coreAuthenticationlog
     */
    @Override
    public void update(CoreAuthenticationlog coreAuthenticationlog) {
        helperDao.update(sessionFactory, coreAuthenticationlog);
    }

    /**
     * Delete authentication log
     *
     * @param coreAuthenticationlog
     */
    @Override
    public void delete(CoreAuthenticationlog coreAuthenticationlog) {
        helperDao.delete(sessionFactory, coreAuthenticationlog);
    }

    /**
     * Get authentication log by Id
     *
     * @param authenticationLogId
     * @return
     */
    @Override
    public CoreAuthenticationlog getAuthenticationLogById(int authenticationLogId) {
        return (CoreAuthenticationlog) helperDao.getById(sessionFactory, CoreAuthenticationlog.class, authenticationLogId);
    }
    
    /**
     * Get authentication log by Id
     *
     * @param requestId
     * @return
     */
    @Override
    public CoreAuthenticationlog getAuthenticationLogByRequestId(String requestId) {
        CoreAuthenticationlog coreAuthenticationLog = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

           // Criteria cr = session.createCriteria(CoreAuthenticationlog.class);
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CoreAuthenticationlog> criteriaQuery = cb.createQuery(CoreAuthenticationlog.class);
            Root<CoreAuthenticationlog> coreAuthenticationlogRoot = criteriaQuery.from(CoreAuthenticationlog.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(requestId)) {
            	conditions.add(cb.equal(coreAuthenticationlogRoot.get("requestId"), requestId));
            }

            // CacheMode.IGNORE 
           // cr.setCacheMode(CacheMode.IGNORE);
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<CoreAuthenticationlog> query = session.createQuery(criteriaQuery);
            query.setMaxResults(1);
            List<CoreAuthenticationlog> results=query.getResultList();
            if (results != null && results.size() > 0) {
            	coreAuthenticationLog = results.get(0);
            }
         //   coreAuthenticationLog = (CoreAuthenticationlog) query.getSingleResult();

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return coreAuthenticationLog;
    }
}
