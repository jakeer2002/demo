// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * API
 */

@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class Api {

    /**
     * API Id
     */
    @XmlElement(name = "Id", required =false)
    protected Integer id;

    /**
     * API key
     */
    @XmlElement(name = "Key", required = false)
    protected String key;

    /**
     * API name
     */
    @XmlElement(name = "Name", required = false)
    protected String name;

    /**
     * Created on date
     */
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @XmlElement(name = "CreatedOn", required = false)
    protected Date createdOn;

    /**
     * Updated on date
     */
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @XmlElement(name = "UpdatedOn", required = false)
    protected Date updatedOn;

    /**
     * Get API Id
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set API Id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get API Key
     *
     * @return
     */
    public String getKey() {
        return key;
    }

    /**
     * Set API Key
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Get Name
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Set API name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get API created date
     *
     * @return
     */
    public Date getCreatedOn() {
        return createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Set API Created date
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Get API updated date
     *
     * @return
     */
    public Date getUpdatedOn() {
        return updatedOn == null ? null : new Date(updatedOn.getTime());
    }

    /**
     * Set API updated date
     *
     * @param updatedOn
     */
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn == null ? null : new Date(updatedOn.getTime());
    }

}
