// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.es.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;

/**
 * CoreUsagelog
 */
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoreErrorlog implements java.io.Serializable {

    /**
     * Request Id
     */
    @JsonProperty("requestId")
    private String requestId;

    /**
     * Client Id
     */
    @JsonProperty("clientId")
    private Integer clientId;

    /**
     * API Id
     */
    @JsonProperty("apiId")
    private Integer apiId;
    
    /**
     * Consumer key
     */
    @JsonProperty("consumerKey")
    private String consumerKey;

    /**
     * API
     */
    @JsonProperty("api")
    private String api;

    /**
     * Status code
     */
    @JsonProperty("statusCode")
    private Integer statusCode;

    /**
     * User message
     */
    @JsonProperty("userMessage")
    private String userMessage;

    /**
     * Exception
     */
    @JsonProperty("exception")
    private String exception;

    /**
     * Trace log
     */
    @JsonProperty("traceLog")
    private String traceLog;
    
    /**
     * Time on which the log created
     */
    @JsonProperty("createdOn")
    private Date createdOn;

    /**
     * Get request id
     * @return 
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set request id
     * @param requestId 
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Get client id
     * @return 
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client id
     * @param clientId 
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get api id
     * @return 
     */
    public Integer getApiId() {
        return apiId;
    }

    /**
     * Set api id
     * @param apiId 
     */
    public void setApiId(Integer apiId) {
        this.apiId = apiId;
    }
    
    /**
     * Get consumer key
     * @return 
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get api
     * @return 
     */
    public String getApi() {
        return api;
    }

    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * Get status code
     * @return 
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * Set status code
     * @param statusCode 
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Get user message
     * @return 
     */
    public String getUserMessage() {
        return userMessage;
    }

    /**
     * Set user message
     * @param userMessage 
     */
    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    /**
     * Get exception
     * @return 
     */
    public String getException() {
        return exception;
    }

    /**
     * Set exception
     * @param exception 
     */
    public void setException(String exception) {
        this.exception = exception;
    }

    /**
     * Get trace log
     * @return 
     */
    public String getTraceLog() {
        return traceLog;
    }

    /**
     * Set trace log
     * @param traceLog 
     */
    public void setTraceLog(String traceLog) {
        this.traceLog = traceLog;
    }

    /**
     * Get created on
     * @return 
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * Set created on
     * @param createdOn 
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

}
