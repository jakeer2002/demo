// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * AggregatedCoreLensDocument
 */
@Entity
@Table(name = "aggregated_core_lensdocument")
public class AggregatedCoreLensDocument  implements java.io.Serializable {

     private int id;
     private int clientId;
     private int apiId;
     private String method;
     private String resource;
     private String instanceType;
     private String locale;
     private long minFileSize;
     private long maxFileSize;
     private long avgFileSize;
     private String date;
     private int hour;
     private long documentCount;

    public AggregatedCoreLensDocument() {
    }

	
    public AggregatedCoreLensDocument(int id, int clientId, int apiId, String method, String resource, String instanceType, long minFileSize, long maxFileSize, long avgFileSize, String date, int hour, long documentCount) {
        this.id = id;
        this.clientId = clientId;
        this.apiId = apiId;
        this.method = method;
        this.resource = resource;
        this.instanceType = instanceType;
        this.minFileSize = minFileSize;
        this.maxFileSize = maxFileSize;
        this.avgFileSize = avgFileSize;
        this.date = date;
        this.hour = hour;
        this.documentCount = documentCount;
    }
    public AggregatedCoreLensDocument(int id, int clientId, int apiId, String method, String resource, String instanceType, String locale, long minFileSize, long maxFileSize, long avgFileSize, String date, int hour, long documentCount) {
       this.id = id;
       this.clientId = clientId;
       this.apiId = apiId;
       this.method = method;
       this.resource = resource;
       this.instanceType = instanceType;
       this.locale = locale;
       this.minFileSize = minFileSize;
       this.maxFileSize = maxFileSize;
       this.avgFileSize = avgFileSize;
       this.date = date;
       this.hour = hour;
       this.documentCount = documentCount;
    }
   
    /**
     * Get Id
     * @return 
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }
    
    /**
     * Set Id
     * @param id 
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * Get Client Id
     * @return 
     */
    @Column(name = "ClientId", nullable = false)
    public int getClientId() {
        return this.clientId;
    }
    
    /**
     * Set Client Id
     * @param clientId 
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
    
    /**
     * Get Api Id
     * @return 
     */
    @Column(name = "ApiId", nullable = false)
    public int getApiId() {
        return this.apiId;
    }
    
    /**
     * Set Api Id
     * @param apiId 
     */
    public void setApiId(int apiId) {
        this.apiId = apiId;
    }
    
    /**
     * Get Method
     * @return 
     */
    @Column(name = "Method", nullable = false, length = 10)
    public String getMethod() {
        return this.method;
    }
    
    /**
     * Set Method
     * @param method 
     */
    public void setMethod(String method) {
        this.method = method;
    }
    
    /**
     * Get Resource
     * @return 
     */
    @Column(name = "Resource", nullable = false, length = 20)
    public String getResource() {
        return this.resource;
    }
    
    /**
     * Set Resource
     * @param resource 
     */
    public void setResource(String resource) {
        this.resource = resource;
    }
    
    /**
     * Get Instance Type
     * @return 
     */
    @Column(name = "InstanceType", nullable = false, length = 10)
    public String getInstanceType() {
        return this.instanceType;
    }
    
    /**
     * Set Instance Type
     * @param instanceType 
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }
    
    /**
     * Get Locale
     * @return 
     */
    @Column(name = "Locale", length = 10)
    public String getLocale() {
        return this.locale;
    }
    
    /**
     * Set locale
     * @param locale 
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }
    
    /**
     * Get Min File Size
     * @return 
     */
    @Column(name = "MinFileSize", nullable = false)
    public long getMinFileSize() {
        return this.minFileSize;
    }
    
    /**
     * Set Min File Size
     * @param minFileSize 
     */
    public void setMinFileSize(long minFileSize) {
        this.minFileSize = minFileSize;
    }
    
    /**
     * Get Max File Size
     * @return 
     */
    @Column(name = "MaxFileSize", nullable = false)
    public long getMaxFileSize() {
        return this.maxFileSize;
    }
    
    /**
     * Set Max File Size
     * @param maxFileSize 
     */
    public void setMaxFileSize(long maxFileSize) {
        this.maxFileSize = maxFileSize;
    }
    
    /**
     * Get Avg File Size
     * @return 
     */
    @Column(name = "AvgFileSize", nullable = false)
    public long getAvgFileSize() {
        return this.avgFileSize;
    }
    
    /**
     * Set Avg File Size
     * @param avgFileSize 
     */
    public void setAvgFileSize(long avgFileSize) {
        this.avgFileSize = avgFileSize;
    }
    
    /**
     * Get Date
     * @return 
     */
    @Column(name = "Date", nullable = false)
    public String getDate() {
        return this.date;
    }
    
    /**
     * Set Date
     * @param date 
     */
    public void setDate(String date) {
        this.date = date;
    }
    
    /**
     * Get Hour
     * @return 
     */
    @Column(name = "Hour", nullable = false)
    public int getHour() {
        return this.hour;
    }
    
    /**
     * Set Hour
     * @param hour 
     */
    public void setHour(int hour) {
        this.hour = hour;
    }
    
    /**
     * Get Document Count
     * @return 
     */
    @Column(name = "DocumentCount", nullable = false)
    public long getDocumentCount() {
        return this.documentCount;
    }
    
    /**
     * Set Document Count
     * @param documentCount 
     */
    public void setDocumentCount(long documentCount) {
        this.documentCount = documentCount;
    }
}


