// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import java.util.List;

/**
 *
 * API dao interface
 */
public interface ICoreApiDao {

    /**
     * Create API
     *
     * @param coreApi
     */
    public void save(CoreApi coreApi);

    /**
     * Update API
     *
     * @param coreApi
     */
    public void update(CoreApi coreApi);

    /**
     * Delete API
     *
     * @param coreApi
     */
    public void delete(CoreApi coreApi);

    /**
     * Get API by Id
     *
     * @param apiId
     * @return
     */
    public CoreApi getApiById(int apiId);

    /**
     * Get API by criteria
     *
     * @param apiCriteria
     * @return
     */
    public List<CoreApi> getApiByCriteria(ApiCriteria apiCriteria);
    
    /**
     * Get api by criteria
     * @param apiCriteria
     * @return 
     */
    public List<CoreApi> findApiDuplicateByCriteria(ApiCriteria apiCriteria);
}
