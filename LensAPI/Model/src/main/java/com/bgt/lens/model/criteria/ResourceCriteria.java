// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;

/**
 *
 * Resource criteria parameters to get data from database
 */
public class ResourceCriteria {

    /**
     * Resource name
     */
    private String resourceName;

    /**
     * API data
     */
    private CoreApi api;

    /**
     * Client data
     */
    private CoreClient coreClient;

    /**
     * Get resource name
     *
     * @return
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     * Set resource name
     *
     * @param resourceName
     */
    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    /**
     * Get API data
     *
     * @return
     */
    public CoreApi getApi() {
        return api;
    }

    /**
     * Set API details
     *
     * @param api
     */
    public void setApi(CoreApi api) {
        this.api = api;
    }
    
    /**
     * Get Core Client
     * @return 
     */
    public CoreClient getCoreClient() {
        return coreClient;
    }
    
    /**
     * Set Core Client
     * @param coreClient 
     */
    public void setCoreClient(CoreClient coreClient) {
        this.coreClient = coreClient;
    }
}
