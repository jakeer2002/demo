// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Match Explanation
 */
@XmlRootElement(name = "matchexplanation")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class MatchExplanation {
    
    /**
     * Matched
     */
    @XmlAttribute(name="matched")
    public String matched;
    
    /**
     * Total
     */
    @XmlAttribute(name="total")
    public String total;
    
    /**
     * Text content
     */
    @XmlElement(name="Models")
    public Object models;

    /**
     * Get matched
     * @return 
     */
    public String getMatched() {
        return matched;
    }

    /**
     * Set matched
     * @param matched 
     */
    public void setMatched(String matched) {
        this.matched = matched;
    }
    
    /**
     * Get total
     * @return 
     */
    public String getTotal() {
        return total;
    }

    /**
     * Set total
     * @param total 
     */
    public void setTotal(String total) {
        this.total = total;
    }

    /**
     * Get object
     * @return 
     */
    public Object getModels() {
        return models;
    }

    /**
     * Set object
     * @param object 
     */
    public void setModels(Object models) {
        this.models = models;
    }
}
