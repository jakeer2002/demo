// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

/**
 *
 * Error log criteria to get the data from the database
 */
public class ErrorLogCriteria {

    /**
     * Request Id
     */
    private String requestId;

    /**
     * Client Id
     */
    private Integer clientId;

    /**
     * API Id
     */
    private Integer apiId;

    /**
     * Consumer key
     */
    private String consumerKey;
    
    /**
     * Api
     */
    private String api;

    /**
     * HTTP status code
     */
    private Integer statusCode;

    /**
     * Default result set limit
     */
    private int defaultResultLimit;

    /**
     * Get request id
     *
     * @return
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set request id
     *
     * @param RequestId
     */
    public void setRequestId(String RequestId) {
        this.requestId = RequestId;
    }

    /**
     * Get client Id
     *
     * @return
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client id
     *
     * @param ClientId
     */
    public void setClientId(Integer ClientId) {
        this.clientId = ClientId;
    }

    /**
     * Get API id
     *
     * @return
     */
    public Integer getApiId() {
        return apiId;
    }

    /**
     * Set API Id
     *
     * @param ApiId
     */
    public void setApiId(Integer ApiId) {
        this.apiId = ApiId;
    }

    /**
     * Get consumer key
     * @return 
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get api
     * @return 
     */
    public String getApi() {
        return api;
    }

    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * Get status code
     *
     * @return
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * Set status code
     *
     * @param StatusCode
     */
    public void setStatusCode(Integer StatusCode) {
        this.statusCode = StatusCode;
    }

    /**
     * Get default result limit
     *
     * @return
     */
    public int getDefaultResultLimit() {
        return defaultResultLimit;
    }

    /**
     * Set default result limit
     *
     * @param defaultResultLimit
     */
    public void setDefaultResultLimit(int defaultResultLimit) {
        this.defaultResultLimit = defaultResultLimit;
    }

}
