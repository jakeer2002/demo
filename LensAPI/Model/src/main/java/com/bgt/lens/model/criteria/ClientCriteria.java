// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

/**
 *
 * Client criteria parameters to get details from database
 */
public class ClientCriteria {

    /**
     * Client Key
     */
    private String clientKey;

    /**
     * Client Name
     */
    private String name;

    /**
     * Document dump status
     */
    private Boolean dumpStatus;

    /**
     * Get Client Key
     *
     * @return
     */
    public String getClientKey() {
        return this.clientKey;
    }

    /**
     * Set Client Key
     *
     * @param apiKey
     */
    public void setClientKey(String apiKey) {
        this.clientKey = apiKey;
    }

    /**
     * Get Client Name
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set Client Name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get Document dump status
     *
     * @return
     */
    public Boolean isDumpStatus() {
        return dumpStatus;
    }

    /**
     * Set Document dump status
     *
     * @param dumpStatus
     */
    public void setDumpStatus(Boolean dumpStatus) {
        this.dumpStatus = dumpStatus;
    }

}
