// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Custom Filter
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class Filter {
    /**
     * Key
     */
    @XmlElement(name = "Key", required = false)
    protected String key;
    
    @XmlElement(name = "Name", required = false)
    protected String name;
    
    @XmlElement(name = "Value", required = false)
    protected Integer value;
    
    @XmlElement(name = "Min", required = false)
    protected Integer min;
    
    @XmlElement(name = "Max", required = false)
    protected Integer max;
    
    @XmlElement(name = "Values", required = false)
    protected List<Integer> values; 

    /**
     * Get Key
     * @return 
     */
    public String getKey() {
        return key;
    }

    /**
     * Set Key
     * @param key 
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Get Value
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * Set Value
     * @param value 
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Get Min
     * @return 
     */
    public Integer getMin() {
        return min;
    }

    /**
     * Set Min
     * @param min 
     */
    public void setMin(Integer min) {
        this.min = min;
    }

    /**
     * Get Max
     * @return 
     */
    public Integer getMax() {
        return max;
    }

    /**
     * Set Max
     * @param max 
     */
    public void setMax(Integer max) {
        this.max = max;
    }

    /**
     * Get list of values
     * @return 
     */
    public List<Integer> getValues() {
        return values;
    }

    /**
     * Set list of values
     * @param values 
     */
    public void setValues(List<Integer> values) {
        this.values = values;
    }
    
    /**
     * Get value
     * @return 
     */
    public Integer getValue() {
        return value;
    }

    /**
     * Set value
     * @param value 
     */
    public void setValue(Integer value) {
        this.value = value;
    }
}
