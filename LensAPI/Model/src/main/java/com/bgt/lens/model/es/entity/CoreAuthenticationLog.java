// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.es.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;

/**
 * CoreUsagelog
 */
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoreAuthenticationLog implements java.io.Serializable {

    /**
     * Request Id
     */
    @JsonProperty("requestId")
    private String requestId;

    /**
     * Consumer key
     */
    @JsonProperty("consumerKey")
    private String consumerKey;

    /**
     * API
     */
    @JsonProperty("api")
    private String api;

    /**
     * Status code
     */
    @JsonProperty("nonce")
    private String nonce;
    
    /**
     * Status code
     */
    @JsonProperty("signature")
    private String signature;

    /**
     * User message
     */
    @JsonProperty("timestamp")
    private Long timestamp;
    
    /**
     * Time on which the log created
     */
    @JsonProperty("createdOn")
    private Date createdOn;

    /**
     * Get request id
     * @return 
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set request id
     * @param requestId 
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Get consumer key
     * @return 
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get api
     * @return 
     */
    public String getApi() {
        return api;
    }

    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * Get nonce
     * @return 
     */
    public String getNonce() {
        return nonce;
    }

    /**
     * Set nonce
     * @param nonce 
     */
    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    /**
     * Get signature
     * @return 
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Set signature
     * @param signature 
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    /**
     * Get timestamp
     * @return 
     */
    public Long getTimestamp() {
        return timestamp;
    }

    /**
     * Set timestamp
     * @param timestamp 
     */
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Get created on
     * @return 
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * Set created on
     * @param createdOn 
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
}
