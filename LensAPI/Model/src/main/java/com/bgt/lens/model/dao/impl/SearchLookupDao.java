// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.SearchLookupCriteria;
import com.bgt.lens.model.dao.ISearchLookupDao;
import com.bgt.lens.model.entity.SearchFilterType;
import com.bgt.lens.model.entity.SearchLookup;
import com.bgt.lens.model.entity.SearchVendorsettings;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Search lookup dao implementation
 * @author pvarudaraj
 */
public class SearchLookupDao implements ISearchLookupDao{
    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize Lens settings Dao
     */
    public SearchLookupDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set Session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create lookup
     *
     * @param searchLookup
     */
    @Override
    public void save(SearchLookup searchLookup ) {
        helperDao.save(sessionFactory, searchLookup);
    }

    /**
     * Update lookup
     *
     * @param searchLookup
     */
    @Override
    public void update(SearchLookup searchLookup ) {
        helperDao.update(sessionFactory, searchLookup);
    }

    /**
     * Delete lookup
     *
     * @param searchLookup
     */
    @Override
    public void delete(SearchLookup searchLookup ) {
        helperDao.delete(sessionFactory, searchLookup);
    }

    /**
     * Get lookups by criteria
     * @param searchLookupCriteria
     * @return 
     */
    @Override
    public List<SearchLookup> getLookupsByCriteria(SearchLookupCriteria searchLookupCriteria) {
        List<SearchLookup> lookupList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<SearchLookup> criteriaQuery = cb.createQuery(SearchLookup.class);
            Root<SearchLookup> searchLookupRoot = criteriaQuery.from(SearchLookup.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(searchLookupCriteria.getId())) {
            	conditions.add(cb.equal(searchLookupRoot.get("id"), searchLookupCriteria.getId()));
            }

            if (_helper.isNotNullOrEmpty(searchLookupCriteria.getType())) {
            	conditions.add(cb.equal(searchLookupRoot.get("type"), searchLookupCriteria.getType()));
            }
            
            if (_helper.isNotNullOrEmpty(searchLookupCriteria.getLookupKey())) {
            	conditions.add(cb.equal(searchLookupRoot.get("lookupKey"), searchLookupCriteria.getLookupKey()));
            }

            criteriaQuery.orderBy(cb.asc(searchLookupRoot.get("id")));
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<SearchLookup> query = session.createQuery(criteriaQuery);
            List<SearchLookup> results = query.getResultList();
            // ToDo : Need to check duplicate in list
            if (results != null && results.size() > 0) {
                lookupList = results;

                Set<SearchLookup> lookupSet = new HashSet<>();
                lookupSet.addAll(lookupList);

                lookupList.clear();
                lookupList.addAll(lookupSet);
                
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } 
        catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return lookupList;
    }

    /**
     * Get lookup by Id
     * @param lookupId
     * @return 
     */
    @Override
    public SearchLookup getLookupById(int lookupId) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            SearchLookup lookup = (SearchLookup) session.get(SearchLookup.class, lookupId);

            tx.commit();
            LOGGER.info("Transaction Commit");
            return lookup;
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
