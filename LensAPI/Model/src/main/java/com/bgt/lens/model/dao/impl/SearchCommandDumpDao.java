// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.dao.ISearchCommandDumpDao;
import com.bgt.lens.model.entity.SearchCommanddump;

/**
 * Search command dump dao implementation
 *
 * @author pvarudaraj
 */
public class SearchCommandDumpDao implements ISearchCommandDumpDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize Lens settings Dao
     */
    public SearchCommandDumpDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set Session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create dump
     *
     * @param searchCommandDump
     */
    @Override
    public void save(SearchCommanddump searchCommandDump) {
        helperDao.save(sessionFactory, searchCommandDump);
    }

    /**
     * Update searchCommandDump
     *
     * @param searchCommandDump
     */
    @Override
    public void update(SearchCommanddump searchCommandDump) {
        helperDao.update(sessionFactory, searchCommandDump);
    }

    /**
     * Delete searchCommandDump
     *
     * @param searchCommandDump
     */
    @Override
    public void delete(SearchCommanddump searchCommandDump) {
        helperDao.delete(sessionFactory, searchCommandDump);
    }

    /**
     * Get search command dump by criteria
     *
     * @param searchDumpCriteria
     * @return
     */
    @Override
    public List<SearchCommanddump> getDumpByCriteria(SearchCommandDumpCriteria searchDumpCriteria) {
        List<SearchCommanddump> dumpList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<SearchCommanddump> criteriaQuery = cb.createQuery(SearchCommanddump.class);
            Root<SearchCommanddump> searchCommanddumpRoot = criteriaQuery.from(SearchCommanddump.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(searchDumpCriteria.getId())) {
            	conditions.add(cb.equal(searchCommanddumpRoot.get("id"), searchDumpCriteria.getId()));
            }

            if (_helper.isNotNull(searchDumpCriteria.getRequestId())) {
            	conditions.add(cb.equal(searchCommanddumpRoot.get("requestId"), searchDumpCriteria.getRequestId()));
            }

            if (_helper.isNotNull(searchDumpCriteria.getClientId())) {
            	conditions.add(cb.equal(searchCommanddumpRoot.get("clientId"), searchDumpCriteria.getClientId()));
            }

            if (_helper.isNotNull(searchDumpCriteria.getLensSettingsId())) {
            	conditions.add(cb.equal(searchCommanddumpRoot.get("lensSettingsId"), searchDumpCriteria.getLensSettingsId()));
            }

            if (_helper.isNotNull(searchDumpCriteria.getVendorId())) {
            	conditions.add(cb.equal(searchCommanddumpRoot.get("vendorId"), searchDumpCriteria.getVendorId()));
            }

            if (_helper.isNotNull(searchDumpCriteria.getFrom())) {
            	conditions.add(cb.greaterThanOrEqualTo(searchCommanddumpRoot.<Date>get("createdOn"), searchDumpCriteria.getFrom()));
            }

            if (_helper.isNotNull(searchDumpCriteria.getTo())) {
            	conditions.add(cb.lessThanOrEqualTo(searchCommanddumpRoot.<Date>get("createdOn"), searchDumpCriteria.getTo()));
            }

            if (_helper.isNotNullOrEmpty(searchDumpCriteria.getType())) {
            	conditions.add(cb.equal(searchCommanddumpRoot.get("documentType"), searchDumpCriteria.getType()));
            }

            criteriaQuery.orderBy(cb.asc(searchCommanddumpRoot.get("id")));
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<SearchCommanddump> query = session.createQuery(criteriaQuery);
            List<SearchCommanddump> results = query.getResultList();
            // ToDo : Need to check duplicate in list
            if (results != null && results.size() > 0) {
                dumpList = results;

                Set<SearchCommanddump> dumpSet = new HashSet<>();
                dumpSet.addAll(dumpList);

                dumpList.clear();
                dumpList.addAll(dumpSet);

            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return dumpList;
    }

    /**
     * Get dump by Id
     *
     * @param dumpId
     * @return
     */
    @Override
    public SearchCommanddump getDumpById(int dumpId, int clientId) {
        SearchCommanddump searchCommandDump = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<SearchCommanddump> criteriaQuery = cb.createQuery(SearchCommanddump.class);
            Root<SearchCommanddump> searchCommanddumpRoot = criteriaQuery.from(SearchCommanddump.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(dumpId)) {
            	conditions.add(cb.equal(searchCommanddumpRoot.get("id"), dumpId));
            }

            if (_helper.isNotNull(clientId)) {
            	conditions.add(cb.equal(searchCommanddumpRoot.get("clientId"), clientId));
            }

            criteriaQuery.multiselect(searchCommanddumpRoot.get("id"),
            		searchCommanddumpRoot.get("requestId"),
            		searchCommanddumpRoot.get("clientId"),
            		searchCommanddumpRoot.get("lensSettingsId"),
            		searchCommanddumpRoot.get("locale"),
            		searchCommanddumpRoot.get("documentType"),
            		searchCommanddumpRoot.get("instanceType"),
            		searchCommanddumpRoot.get("vendorId"),
            		searchCommanddumpRoot.get("searchCommand"),
            		searchCommanddumpRoot.get("createdOn")
            		).distinct(true);
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<SearchCommanddump> query = session.createQuery(criteriaQuery);
            query.setMaxResults(1);
            List<SearchCommanddump> results=query.getResultList();
            if (results != null && results.size() > 0) {
            	searchCommandDump = results.get(0);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return searchCommandDump;
    }
}
