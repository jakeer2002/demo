// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Location history
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class LocationHistory {
    /**
     * City
     */
    @XmlElement(name = "City", required = false)
    protected String city;
    
    /**
     * State
     */
    @XmlElement(name = "State", required = false)
    protected String state;

    /**
     * Get city
     * @return 
     */
    public String getCity() {
        return city;
    }

    /**
     * Set city
     * @param city 
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Get state
     * @return 
     */
    public String getState() {
        return state;
    }
    
    /**
     * Set state
     * @param state 
     */
    public void setState(String state) {
        this.state = state;
    }
}
