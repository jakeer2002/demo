// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Job
 */
@XmlRootElement(name = "Job")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class Job {
    
    /**
     * Document Id
     */
    @XmlElement(name = "JobId", required = false)
    protected String jobId;
    
     /**
     * Score
     */
    @XmlElement(name = "Score", required = false)
    protected Integer score;
    
     /**
     * Job Title
     */
    @XmlElement(name = "JobTitle", required = false)
    protected String jobTitle;
    
     /**
     * Employer
     */
    @XmlElement(name = "Employer", required = false)
    protected String employer;
    
     /**
     * Skills
     */
    @XmlElement(name = "Skills", required = false)
    protected Skill skills;
    
     /**
     * List of custom filters
     */
    @XmlElement(name = "CustomFilters", required = false)
    protected List<Filter> customFilters;
    
     /**
     * List of custom xpath data
     */
    @XmlElement(name = "CustomXpathData", required = false)
    protected List<CustomXpathData> customXpathData;    
    
    /**
     * Match explanation
     */
    @XmlElement(name = "MatchExplanation", required = false)
    protected MatchExplanation matchExplanation;

    /**
     * Get Document Id
     *
     * @return
     */
    public String getJobId() {
        return jobId;
    }

    /**
     * Set Document Id
     *
     * @param jobId
     */
    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    /**
     * Get Source
     *
     * @return
     */
    public Integer getScore() {
        return score;
    }

    /**
     * Set Source
     *
     * @param score
     */
    public void setScore(Integer score) {
        this.score = score;
    }

    /**
     * Get Job Title
     *
     * @return
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Set Job Title
     *
     * @param jobTitle
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * Get Employer
     *
     * @return
     */
    public String getEmployer() {
        return employer;
    }

    /**
     * Set Employer
     *
     * @param employer
     */
    public void setEmployer(String employer) {
        this.employer = employer;
    }

    /**
     * Get Skills
     *
     * @return
     */
    public Skill getSkills() {
        return skills;
    }

    /**
     * Set Skill
     *
     * @param skills
     */
    public void setSkills(Skill skills) {
        this.skills = skills;
    }

    /**
     * Get Custom Filter
     *
     * @return
     */
    public List<Filter> getCustomFilters() {
        return customFilters;
    }

    /**
     * Set Custom Filters
     *
     * @param customFilters
     */
    public void setCustomFilters(List<Filter> customFilters) {
        this.customFilters = customFilters;
    }

    /**
     * Get Custom Xpath Data
     *
     * @return
     */
    public List<CustomXpathData> getCustomXpathData() {
        return customXpathData;
    }

    /**
     * Set Custom Xpath Data
     *
     * @param customXpathData
     */
    public void setCustomXpathData(List<CustomXpathData> customXpathData) {
        this.customXpathData = customXpathData;
    }

    /**
     * Get Match Explanation
     *
     * @return
     */
    public MatchExplanation getMatchExplanation() {
        return matchExplanation;
    }

    /**
     * Set Match Explanation
     *
     * @param matchExplanation
     */
    public void setMatchExplanation(MatchExplanation matchExplanation) {
        this.matchExplanation = matchExplanation;
    }         
}
