// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import java.util.Date;

/**
 *
 * Usage counter criteria parameters to get data from database
 */
public class UsagecounterCriteria {

    /**
     * Usage counter from date
     */
    private Date from;

    /**
     * Usage counter
     */
    private Date to;

    /**
     * Client Id
     */
    private Integer clientId;

    /**
     * API Id
     */
    private Integer apiId;

    /**
     * HTTP method
     */
    private String method;

    /**
     * Resource name
     */
    private String resource;

    /**
     * Get From date
     *
     * @return
     */
    public Date getFromDate() {
        return from == null ? null : new Date(from.getTime());
    }

    /**
     * Set from date
     *
     * @param from
     */
    public void setFromDate(Date from) {
        this.from = from == null ? null : new Date(from.getTime());
    }

    /**
     * Get to date
     *
     * @return
     */
    public Date getToDate() {
        return to == null ? null : new Date(to.getTime());
    }

    /**
     * Set to date
     *
     * @param to
     */
    public void setToDate(Date to) {
        this.to = to == null ? null : new Date(to.getTime());
    }

    /**
     * Get client Id
     *
     * @return
     */
    public Integer getClientId() {
        return this.clientId;
    }

    /**
     * Set client Id
     *
     * @param clientId
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get API Id
     *
     * @return
     */
    public Integer getApiId() {
        return this.apiId;
    }

    /**
     * Set API Id
     *
     * @param apiId
     */
    public void setApiId(Integer apiId) {
        this.apiId = apiId;
    }

    /**
     * Get HTTP method
     *
     * @return
     */
    public String getMethod() {
        return this.method;
    }

    /**
     * Set HTTP method
     *
     * @param method
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Get resource
     *
     * @return
     */
    public String getResource() {
        return this.resource;
    }

    /**
     * Set resource
     *
     * @param resource
     */
    public void setResource(String resource) {
        this.resource = resource;
    }

}
