// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Search Filter Type 
 */
@Entity
@Table(name = "search_filtertype")
public class SearchFilterType implements java.io.Serializable{
     
    /**
     * Id
     */
     private Integer id;
     
     /**
     * Filter type     
     */
     private String filterType;
     
     /**
     * Search Filter Logs
     *
     */     
     private Set<SearchFilterLog> searchFilterLogs = new HashSet<SearchFilterLog>(0);
     
     public SearchFilterType() {
    }

    public SearchFilterType(Integer id,String filterType) {
       this.id = id;
       this.filterType = filterType;       
    }
    
    public SearchFilterType(Integer id, String filterType, Set<SearchFilterLog> searchFilterLogs) {
       this.id = id;
       this.filterType = filterType;
       this.searchFilterLogs = searchFilterLogs;
    }
    
    /**
     * Get id
     * @return 
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    /**
     * Set Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * Get Filter Type
     * @return 
     */
    @Column(name = "FilterType", nullable = false)
    public String getFilterType() {
        return this.filterType;
    }
    
    /**
     * Set Filter Type
     * @param filterType 
     */
    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }
    
    /**
     * Get Search Filter Logs
     *
     * @return
     */
    @OneToMany(fetch=FetchType.LAZY, mappedBy="searchFilterType")
    public Set<SearchFilterLog> getSearchFilterLogs() {
        return this.searchFilterLogs;
    }
    
    /**
     * Set Search Filter Logs
     *
     * @param searchFilterLogs
     */
    public void setSearchFilterLogs(Set<SearchFilterLog> searchFilterLogs) {
        this.searchFilterLogs = searchFilterLogs;
    }
    
}
