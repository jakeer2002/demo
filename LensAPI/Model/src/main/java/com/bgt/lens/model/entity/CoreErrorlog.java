// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * CoreErrorlog generated by hbm2java
 */
@Entity
@Table(name = "core_errorlog")
public class CoreErrorlog implements java.io.Serializable {

    /**
     * Error log Id
     */
    private Integer id;

    /**
     * Request Id
     */
    private String requestId;

    /**
     * Client Id
     */
    private Integer clientId;

    /**
     * API id
     */
    private Integer apiId;
    
    /**
     * Consumer Key
     */
    private String consumerKey;
    
    /**
     * Api
     */
    private String api;

    /**
     * Status Code
     */
    private int statusCode;

    /**
     * End user message
     */
    private String userMessage;

    /**
     * Exception message
     */
    private String exception;

    /**
     * Exception trace log
     */
    private String traceLog;

    /**
     * Error log created date
     */
    private Date createdOn;

    /**
     * Initialize error log
     */
    public CoreErrorlog() {
    }

    /**
     * Initialize error log
     *
     * @param requestId
     * @param statusCode
     * @param createdOn
     */
    public CoreErrorlog(String requestId, int statusCode, Date createdOn) {
        this.requestId = requestId;
        this.statusCode = statusCode;
        this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Initialize error log
     *
     * @param requestId
     * @param clientId
     * @param apiId
     * @param statusCode
     * @param userMessage
     * @param exception
     * @param traceLog
     * @param createdOn
     */
    public CoreErrorlog(String requestId, Integer clientId, Integer apiId, int statusCode, String userMessage, String exception, String traceLog, Date createdOn) {
        this.requestId = requestId;
        this.clientId = clientId;
        this.apiId = apiId;
        this.statusCode = statusCode;
        this.userMessage = userMessage;
        this.exception = exception;
        this.traceLog = traceLog;
        this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Get error log Id
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    /**
     * Set error log Id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get request Id
     *
     * @return
     */
    @Column(name = "RequestId", nullable = false, length = 50)
    public String getRequestId() {
        return this.requestId;
    }

    /**
     * Set request Id
     *
     * @param requestId
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Get client Id
     *
     * @return
     */
    @Column(name = "ClientId")
    public Integer getClientId() {
        return this.clientId;
    }

    /**
     * Set client Id
     *
     * @param clientId
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get API Id
     *
     * @return
     */
    @Column(name = "ApiId")
    public Integer getApiId() {
        return this.apiId;
    }

    /**
     * Set API Id
     *
     * @param apiId
     */
    public void setApiId(Integer apiId) {
        this.apiId = apiId;
    }

    /**
     * Get consumer key
     * @return 
     */
    @Transient
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }
    
    /**
     * Get Api
     * @return 
     */
    @Transient
    public String getApi() {
        return api;
    }

    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }
    
    /**
     * Get status code
     *
     * @return
     */
    @Column(name = "StatusCode", nullable = false)
    public int getStatusCode() {
        return this.statusCode;
    }

    /**
     * Set status code
     *
     * @param statusCode
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Get user message
     *
     * @return
     */
    @Column(name = "UserMessage")
    public String getUserMessage() {
        return this.userMessage;
    }

    /**
     * Set user message
     *
     * @param userMessage
     */
    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    /**
     * Get exception message
     *
     * @return
     */
    @Column(name = "Exception")
    public String getException() {
        return this.exception;
    }

    /**
     * Set exception message
     *
     * @param exception
     */
    public void setException(String exception) {
        this.exception = exception;
    }

    /**
     * Get trace log
     *
     * @return
     */
    @Column(name = "TraceLog")
    public String getTraceLog() {
        return this.traceLog;
    }

    /**
     * Set trace log
     *
     * @param traceLog
     */
    public void setTraceLog(String traceLog) {
        this.traceLog = traceLog;
    }

    /**
     * Get error log created date
     *
     * @return
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedOn", nullable = false, length = 19)
    public Date getCreatedOn() {
        return createdOn == null ? null : new Date(createdOn.getTime());
    }

    /**
     * Set error log creation date
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn == null ? null : new Date(createdOn.getTime());
    }

}
