// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import java.util.Date;

/**
 *
 * Aggregated Lens document criteria parameters to get data from database
 */
public class AggregatedLensDocumentCriteria {

    /**
     * Date
     */
    private Date date;

    /**
     * Client Id
     */
    private Integer clientId;

    /**
     * API Id
     */
    private Integer apiId;

    /**
     * HTTP method
     */
    private String method;

    /**
     * Resource name
     */
    private String resource;

    /**
     * Instance Type
     */
    private String instanceType;
    
    /**
     * Locale
     */
    private String locale;
    
    /**
     * Locale
     */
    private Integer hour;

    /**
     * Get client Id
     *
     * @return
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client Id
     *
     * @param ClientId
     */
    public void setClientId(Integer ClientId) {
        this.clientId = ClientId;
    }

    /**
     * Get API Id
     *
     * @return
     */
    public Integer getApiId() {
        return apiId;
    }

    /**
     * Set API Id
     *
     * @param ApiId
     */
    public void setApiId(Integer ApiId) {
        this.apiId = ApiId;
    }

    /**
     * Get HTTP method
     *
     * @return
     */
    public String getMethod() {
        return method;
    }

    /**
     * Set HTTP method
     *
     * @param Method
     */
    public void setMethod(String Method) {
        this.method = Method;
    }

    /**
     * Get resource
     *
     * @return
     */
    public String getResource() {
        return resource;
    }

    /**
     * Set resource name
     *
     * @param Resource
     */
    public void setResource(String Resource) {
        this.resource = Resource;
    }
    
    /**
     * Get instance type
     * @return 
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get locale
     * @return 
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set locale
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get date
     * @return 
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set date
     * @param date 
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get hour
     * @return 
     */
    public Integer getHour() {
        return hour;
    }

    /**
     * Set hour
     * @param hour 
     */
    public void setHour(Integer hour) {
        this.hour = hour;
    }
}
