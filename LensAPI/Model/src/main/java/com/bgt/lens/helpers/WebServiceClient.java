// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.helpers;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;

/**
 * 
 * @author kjanakiraman
 *
 */
public class WebServiceClient {

	private static final Logger logger = LogManager.getLogger(WebServiceClient.class);

	public String sendHTTPRequest(URL webServiceURL, String httpMethod, String contentType,String acceptType, String requestBody, Integer talentPipelineTimeout)
			throws Exception {
		HttpURLConnection request = null;
		BufferedReader in = null;
		StringBuilder response = null;
		try {
			// Setup the Request
			request = (HttpURLConnection) webServiceURL.openConnection();
			request.setRequestMethod(httpMethod);
			request.setRequestProperty("Content-Type", contentType);
			request.setRequestProperty("Accept", acceptType);
			request.setDoOutput(true);
                        request.setReadTimeout(talentPipelineTimeout);
			// Set the request body if making a POST request
			if (HttpMethod.POST.toString().equalsIgnoreCase(httpMethod)
					|| HttpMethod.PUT.toString().equalsIgnoreCase(httpMethod)) {
				byte[] byteArray = requestBody.getBytes("UTF-8");

				request.setRequestProperty("Content-Length", "" + Integer.toString(byteArray.length));
				request.setUseCaches(false);
				request.setDoInput(true);

				try (DataOutputStream wr = new DataOutputStream(request.getOutputStream())) {
					wr.writeBytes(requestBody);
					wr.flush();
				}
			}
			// Get Response
			int responseCode = request.getResponseCode();
			response = new StringBuilder();
			String inputLine;
			if (responseCode >= 400) {
				in = new BufferedReader(new InputStreamReader(request.getErrorStream(), "UTF-8"));
				response = new StringBuilder();
				while ((inputLine = in.readLine()) != null) {
					// response.append(inputLine);
					response.append(inputLine + System.getProperty("line.separator"));
				}
				in.close();
			} else if (responseCode == 200) {
				logger.debug("|Published to API.");
				in = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
				response = new StringBuilder();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine + System.getProperty("line.separator"));
				}
				in.close();
			} else {
				in = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
				response = new StringBuilder();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine + System.getProperty("line.separator"));
				}
				in.close();
			}
			request.disconnect();

		} catch (Exception ex) {
			logger.error("|Exception occurred in Talent Pipeline. Exception - " + ex.getMessage());
			return ex.getMessage();
		} finally {
			if (in != null) {
				in.close();
			}
			if (request != null) {
				request.disconnect();
			}
		}
		return response.toString();
	}
}
