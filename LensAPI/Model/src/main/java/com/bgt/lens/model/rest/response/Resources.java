// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * Resources
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class Resources {

    /**
     * Resource Id
     */
    @XmlElement(name = "ResourceId", required = false)
    protected Integer resourceId;

    /**
     * Resource name
     */
    @XmlElement(name = "ResourceName", required = false)
    protected String resourceName;

    /**
     * API key
     */
    @XmlElement(name = "ApiKey", required = false)
    protected String apiKey;

    /**
     * Get resource Id
     *
     * @return
     */
    public Integer getResourceId() {
        return resourceId;
    }

    /**
     * Set resource Id
     *
     * @param ResourceId
     */
    public void setResourceId(Integer ResourceId) {
        this.resourceId = ResourceId;
    }

    /**
     * Get resource name
     *
     * @return
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     * Set resource name
     *
     * @param ResourceName
     */
    public void setResourceName(String ResourceName) {
        this.resourceName = ResourceName;
    }

    /**
     * Get API key
     *
     * @return
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * Set API key
     *
     * @param ApiKey
     */
    public void setApiKey(String ApiKey) {
        this.apiKey = ApiKey;
    }

}
