// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.dao.ICoreLensdocumentDao;
import com.bgt.lens.model.entity.AggregatedCoreLensDocument;
import com.bgt.lens.model.entity.CoreLensdocument;

/**
 *
 * Lens document table data management
 */
public class CoreLensdocumentDao implements ICoreLensdocumentDao {
    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(CoreLensdocumentDao.class);

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;
    
    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize Lens document Dao
     */
    public CoreLensdocumentDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create Lens document
     *
     * @param coreLensDocument
     */
    @Override
    public void save(CoreLensdocument coreLensDocument) {
        helperDao.save(sessionFactory, coreLensDocument);
    }

    /**
     * Update Lens document
     *
     * @param coreLensDocument
     */
    @Override
    public void update(CoreLensdocument coreLensDocument) {
        helperDao.update(sessionFactory, coreLensDocument);
    }

    /**
     * Delete Lens document
     *
     * @param coreLensDocument
     */
    @Override
    public void delete(CoreLensdocument coreLensDocument) {
        helperDao.delete(sessionFactory, coreLensDocument);
    }

    /**
     * Get Lens document by Id
     *
     * @param lensDocumentId
     * @return
     */
    @Override
    public CoreLensdocument getLensDocumentById(int lensDocumentId) {
        return (CoreLensdocument) helperDao.getById(sessionFactory, CoreLensdocument.class, lensDocumentId);
    }
    
    /**
     * Update document dump count
     *
     * @param clientapiCriteria
     * @return
     */
    @Override
    public void updateDocumentDumpCount(AggregatedCoreLensDocument aggLensDocument) {
        List<AggregatedCoreLensDocument> aggregatedDocumentList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<AggregatedCoreLensDocument> criteriaQuery = cb.createQuery(AggregatedCoreLensDocument.class);
            Root<AggregatedCoreLensDocument> aggregatedCoreLensDocumentRoot = criteriaQuery.from(AggregatedCoreLensDocument.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(aggLensDocument.getClientId())) {
            	conditions.add(cb.ge(aggregatedCoreLensDocumentRoot.get("clientId"), aggLensDocument.getClientId()));
            }
            if (_helper.isNotNull(aggLensDocument.getApiId())) {
            	conditions.add(cb.le(aggregatedCoreLensDocumentRoot.get("apiId"), aggLensDocument.getApiId()));
            }

            if (_helper.isNotNull(aggLensDocument.getInstanceType())) {
            	conditions.add(cb.equal(aggregatedCoreLensDocumentRoot.get("instanceType"), aggLensDocument.getInstanceType()));
            }

            if (_helper.isNotNull(aggLensDocument.getLocale())) {
            	conditions.add(cb.equal((aggregatedCoreLensDocumentRoot.get("locale")), aggLensDocument.getLocale()));
            }

            if (_helper.isNotNull(aggLensDocument.getResource())) {
            	conditions.add(cb.equal(aggregatedCoreLensDocumentRoot.get("resource"), aggLensDocument.getResource()));
            }
            
            if (_helper.isNotNull(aggLensDocument.getDate())) {
            	conditions.add(cb.equal(aggregatedCoreLensDocumentRoot.get("date"), aggLensDocument.getDate()));
            }
            
            if (_helper.isNotNull(aggLensDocument.getHour())) {
            	conditions.add(cb.equal(aggregatedCoreLensDocumentRoot.get("hour"), aggLensDocument.getHour()));
            }

            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<AggregatedCoreLensDocument> query = session.createQuery(criteriaQuery);
            List<AggregatedCoreLensDocument> results = query.getResultList();
            if (results != null && results.size() > 0) {
                aggregatedDocumentList =  results;
                aggregatedDocumentList.get(0).setDocumentCount(
                    aggregatedDocumentList.get(0).getDocumentCount() + 1);
                
                aggregatedDocumentList.get(0).setAvgFileSize((aggregatedDocumentList.get(0).getAvgFileSize() 
                    + aggLensDocument.getMinFileSize())/2);
                
                if(aggregatedDocumentList.get(0).getMaxFileSize() < aggLensDocument.getMinFileSize()){
                    aggregatedDocumentList.get(0).setMaxFileSize(aggLensDocument.getMinFileSize());
                }
                
                if(aggregatedDocumentList.get(0).getMinFileSize() > aggLensDocument.getMinFileSize()){
                    aggregatedDocumentList.get(0).setMinFileSize(aggLensDocument.getMinFileSize());
                }
                
                helperDao.update(sessionFactory, aggregatedDocumentList.get(0));
                
//                aggregatedDocumentList = (List<AggregatedCoreLensDocument>) (List<?>) results;
//
//                Set<AggregatedCoreLensDocument> coreUsageLogSet = new HashSet<>();
//                coreUsageLogSet.addAll(aggregatedDocumentList);
//
//                aggregatedDocumentList.clear();
//                aggregatedDocumentList.addAll(coreUsageLogSet);
            }
            else{
                aggLensDocument.setDocumentCount(1);
                helperDao.save(sessionFactory, aggLensDocument);
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                tx.rollback();
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

}
