// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import java.util.Date;

/**
 * Search register log criteria
 */
public class SearchRegisterLogCriteria {
    
    /**
     * Unique Id
     */
    protected Integer Id;
    
    /**
     * Request Id
     */
    protected String requestId;
    
    /**
     * Client Id
     */
    protected Integer clientId;
      
    /**
     * LENS settings Id
     */
    protected Integer lensSettingsId;
    
    /**
     * Vendor Id
     */
    protected Integer vendorId;
    
    /**
      * Document type. Resume or posting
      */
    protected String type;
    
    /**
     * Register or Unregister status
     */
    protected Boolean registerOrUnregister;
    
    /**
     * Failover update status
     */
    protected Boolean failoverUpdateStatus;
    
    /**
     * Over write status
     */
    protected Boolean overWrite;
    
    
    /**
     * Doc Id
     */
    protected String docId;
    
    /**
     * From date
     */
    protected Date from;
    
    /**
     * To date
     */
    protected Date to;
    
    /**
     * Get unique Id
     * @return 
     */
    public Integer getId() {
        return Id;
    }

    /**
     * Set unique Id
     * @param Id 
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }
    
    /**
     * Get request Id
     * @return 
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set request Id
     * @param requestId 
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }   
    
    /**
     * Get client Id
     * @return 
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client Id
     * @param clientId 
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get LENS settings Id
     * @return 
     */
    public Integer getLensSettingsId() {
        return lensSettingsId;
    }

    /**
     * Set LENS settings Id
     * @param lensSettingsId 
     */
    public void setLensSettingsId(Integer lensSettingsId) {
        this.lensSettingsId = lensSettingsId;
    }

    /**
     * Get vendor Id
     * @return 
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * Set vendor Id
     * @param vendorId 
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }
    
    /**
     * Get type
     * @return 
     */
    public String getType() {
        return type;
    }

    /**
     * Set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Register or unregister status
     * @return 
     */
    public Boolean isRegisterOrUnregister() {
        return registerOrUnregister;
    }

    /**
     * Set register or unregister
     * @param registerOrUnregister 
     */
    public void setRegisterOrUnregister(Boolean registerOrUnregister) {
        this.registerOrUnregister = registerOrUnregister;
    }

    /**
     * Failover update status
     * @return 
     */
    public Boolean isFailoverUpdateStatus() {
        return failoverUpdateStatus;
    }

    /**
     * Set failover update status
     * @param failoverUpdateStatus 
     */
    public void setFailoverUpdateStatus(Boolean failoverUpdateStatus) {
        this.failoverUpdateStatus = failoverUpdateStatus;
    }

     /**
     * Over Write
     * @return 
     */
    public Boolean isOverWrite() {
        return overWrite;
    }

    /**
     * Set Over Write
     * @param overWrite 
     */
    public void setOverWrite(Boolean overWrite) {
        this.overWrite = overWrite;
    }
    /**
     * Get document Id
     * @return 
     */
    public String getDocId() {
        return docId;
    }

    /**
     * Set document Id
     * @param docId 
     */
    public void setDocId(String docId) {
        this.docId = docId;
    }

    /**
     * Get from date
     * @return 
     */
    public Date getFrom() {
        return from;
    }

    /**
     * Set from date
     * @param from 
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * Get to date
     * @return 
     */
    public Date getTo() {
        return to;
    }

    /**
     * Set to date
     * @param to 
     */
    public void setTo(Date to) {
        this.to = to;
    }

    
}
