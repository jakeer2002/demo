// <editor-fold defaultstate="collapsed" desc="Copyright � 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

//import com.bgt.lens.model.adminservice.request.Locale;
import java.util.List;


public class DefaultLocaleList {
      public DefaultLocaleList() {
        }
 //  @XmlElement(name = "Locale")
        protected List<Locale> locale;

    public List<Locale> getLocale() {
        return locale;
    }

    public void setLocale(List<Locale> locale) {
        this.locale = locale;
    }

      
}

