/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bgt.lens.model.rest.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author sjagannathan
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class CoreCustomskillsettings {

    @XmlElement(name = "CustomSkillsList", required = false)
    protected List<CustomSkillsList> customSkillsList;

    public List<CustomSkillsList> getCustomSkillsList() {
        return customSkillsList;
    }

    public void setCustomSkillsList(List<CustomSkillsList> customSkillsList) {
        this.customSkillsList = customSkillsList;
    }
}
