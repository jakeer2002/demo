// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * Usage log
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class UsageLog {

    /**
     * Usage log id
     */
    @XmlElement(name = "Id", required = false)
    protected Integer id;

    /**
     * Request Id
     */
    @XmlElement(name = "RequestId", required = false)
    protected String requestId;

    /**
     * Client Id
     */
    @XmlElement(name = "ClientId", required = false)
    protected Integer clientId;

    /**
     * API Id
     */
    @XmlElement(name = "ApiId", required = false)
    protected Integer apiId;

    /**
     * Consumer key
     */
    @XmlElement(name = "ConsumerKey", required = false)
    protected String consumerKey;

    /**
     * API
     */
    @XmlElement(name = "Api", required = false)
    protected String api;
    
    /**
     * URI
     */
    @XmlElement(name = "Uri", required = false)
    protected String uri;

    /**
     * HTTP method
     */
    @XmlElement(name = "Method", required = false)
    protected String method;

    /**
     * Resource name
     */
    @XmlElement(name = "Resource", required = false)
    protected String resource;

    /**
     * Request success or failure status
     */
    @XmlElement(name = "RaisedError", required = false)
    protected Boolean raisedError;

    /**
     * Request in time
     */
    @XmlElement(name = "InTime", required = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    protected Date inTime;

    /**
     * Response sent time
     */
    @XmlElement(name = "OutTime", required = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    protected Date outTime;

    /**
     * Elapsed time
     */
    @XmlElement(name = "TimeElapsed", required = false)
    protected long timeElapsed;

    /**
     * Request content
     */
    @XmlElement(name = "RequestContent", required = false)
    protected String requestContent;

    /**
     * Response content
     */
    @XmlElement(name = "ResponseContent", required = false)
    protected String responseContent;

    /**
     * Request headers
     */
    @XmlElement(name = "Headers", required = false)
    protected String headers;

    /**
     * Get usage log Id
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set usage log Id
     *
     * @param Id
     */
    public void setId(Integer Id) {
        this.id = Id;
    }

    /**
     * Get request Id
     *
     * @return
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set request Id
     *
     * @param RequestId
     */
    public void setRequestId(String RequestId) {
        this.requestId = RequestId;
    }

    /**
     * Get client id
     *
     * @return
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client Id
     *
     * @param ClientId
     */
    public void setClientId(Integer ClientId) {
        this.clientId = ClientId;
    }

    /**
     * Get API Id
     *
     * @return
     */
    public Integer getApiId() {
        return apiId;
    }

    /**
     * Set API Id
     *
     * @param ApiId
     */
    public void setApiId(Integer ApiId) {
        this.apiId = ApiId;
    }

    /**
     * Get consumer key
     * @return 
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    /**
     * Get api
     * @return 
     */
    public String getApi() {
        return api;
    }

    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }
    
    /**
     * Get URI
     *
     * @return
     */
    public String getUri() {
        return uri;
    }

    /**
     * Set URI
     *
     * @param Uri
     */
    public void setUri(String Uri) {
        this.uri = Uri;
    }

    /**
     * Get HTTP method
     *
     * @return
     */
    public String getMethod() {
        return method;
    }

    /**
     * Set HTTP method
     *
     * @param Method
     */
    public void setMethod(String Method) {
        this.method = Method;
    }

    /**
     * Get resource
     *
     * @return
     */
    public String getResource() {
        return resource;
    }

    /**
     * Set resource
     *
     * @param Resource
     */
    public void setResource(String Resource) {
        this.resource = Resource;
    }

    /**
     * Request status
     *
     * @return
     */
    public Boolean isRaisedError() {
        return raisedError;
    }

    /**
     * Set request status
     *
     * @param RaisedError
     */
    public void setRaisedError(Boolean RaisedError) {
        this.raisedError = RaisedError;
    }

    /**
     * Get Request in time
     *
     * @return
     */
    public Date getInTime() {
        return inTime == null ? null : new Date(inTime.getTime());
    }

    /**
     * Set request in time
     *
     * @param InTime
     */
    public void setInTime(Date InTime) {
        this.inTime = InTime == null ? null : new Date(InTime.getTime());
    }

    /**
     * Get response out time
     *
     * @return
     */
    public Date getOutTime() {
        return outTime == null ? null : new Date(outTime.getTime());
    }

    /**
     * Set response out time
     *
     * @param OutTime
     */
    public void setOutTime(Date OutTime) {
        this.outTime = OutTime == null ? null : new Date(OutTime.getTime());
    }

    /**
     * Get elapsed time
     *
     * @return
     */
    public long getTimeElapsed() {
        return timeElapsed;
    }

    /**
     * Set elapsed time
     *
     * @param timeElapsed
     */
    public void setTimeElapsed(long timeElapsed) {
        this.timeElapsed = timeElapsed;
    }

    /**
     * Get request content
     *
     * @return
     */
    public String getRequestContent() {
        return requestContent;
    }

    /**
     * Set request content
     *
     * @param RequestContent
     */
    public void setRequestContent(String RequestContent) {
        this.requestContent = RequestContent;
    }

    /**
     * Get response content
     *
     * @return
     */
    public String getResponseContent() {
        return responseContent;
    }

    /**
     * Set response content
     *
     * @param ResponseContent
     */
    public void setResponseContent(String ResponseContent) {
        this.responseContent = ResponseContent;
    }

    /**
     * Get request headers
     *
     * @return
     */
    public String getHeaders() {
        return headers;
    }

    /**
     * Set request headers
     *
     * @param Headers
     */
    public void setHeaders(String Headers) {
        this.headers = Headers;
    }

}
