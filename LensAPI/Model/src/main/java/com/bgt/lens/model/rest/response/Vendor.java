// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Vendor response
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class Vendor {
    /**
     * Vendor Id
     */
    @XmlElement(name = "VendorId", required = false)
    protected Integer vendorId;
    
    /**
     * Instance Id
     */
    @XmlElement(name = "InstanceId", required = false)
    protected Integer instanceId;
    
    /**
     * Vendor name
     */
    @XmlElement(name = "Vendor", required = false)
    protected String vendor;
    
    /**
     * Locale
     */
    @XmlElement(name = "Locale", required = false)
    protected String locale;
    
    /**
     * Instance Type
     */
    @XmlElement(name = "InstanceType", required = false)
    protected String instanceType;
    
    /**
     * Status
     */
    @XmlElement(name = "Status", required = false)
    protected String status;
    
    /**
     * Type
     */
    @XmlElement(name = "Type", required = false)
    protected String type;
    
      /**
     * ISBGVendor
     */
    @XmlElement(name = "IsBGVendor", required = false)
    protected boolean isBGVendor;
    
    /**
     * Maximum document count
     */
    @XmlElement(name = "MaxDocumentCount", required = false)
    protected Integer maxDocumentCount;
    
    /**
     * Registered document count
     */
    @XmlElement(name = "RegisteredDocumentCount", required = false)
    protected Integer registeredDocumentCount;
    
    /**
     * CreatedOn
     */
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @XmlElement(name = "CreatedOn")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    protected Date createdOn;
    
    /**
     * UpdatedOn
     */
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @XmlElement(name = "UpdatedOn")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    protected Date updatedOn;
    
    /**
     * Get vendor Id
     * @return 
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * Set vendor Id
     * @param vendorId 
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * Get instance Id
     * @return 
     */
    public Integer getInstanceId() {
        return instanceId;
    }

    /**
     * Set instance Id
     * @param instanceId 
     */
    public void setInstanceId(Integer instanceId) {
        this.instanceId = instanceId;
    }

    /**
     * Get vendor name
     * @return 
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * Set vendor name
     * @param vendor 
     */
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * Get locale
     * @return 
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set locale
     * @param locale 
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get instance type
     * @return 
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     * @param instanceType 
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get status
     * @return 
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set status
     * @param status 
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Get type
     * @return 
     */
    public String getType() {
        return type;
    }

    /**
     * Set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }
    
    /**
     * Get maximum document count
     * @return 
     */
    public Integer getMaxDocumentCount() {
        return maxDocumentCount;
    }

    /**
     * Set maximum document count
     * @param maxDocumentCount 
     */
    public void setMaxDocumentCount(Integer maxDocumentCount) {
        this.maxDocumentCount = maxDocumentCount;
    }

    /**
     * Get registered document count
     * @return 
     */
    public Integer getRegisteredDocumentCount() {
        return registeredDocumentCount;
    }

    /**
     * Set registered document count
     * @param registeredDocumentCount 
     */
    public void setRegisteredDocumentCount(Integer registeredDocumentCount) {
        this.registeredDocumentCount = registeredDocumentCount;
    }
    
    /**
     * Get created on
     * @return 
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * Set created on
     * @param createdOn 
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Get updated on
     * @return 
     */
    public Date getUpdatedOn() {
        return updatedOn;
    }

    /**
     * Set updated on
     * @param updatedOn 
     */
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * IsBGVendor
     *
     * @return
     */
    public boolean isIsBGVendor() {
        return isBGVendor;
    }

    /**
     * Set IsBGVendor
     *
     * @param isBGVendor
     */
    public void setIsBGVendor(boolean isBGVendor) {
        this.isBGVendor = isBGVendor;
    }

}
