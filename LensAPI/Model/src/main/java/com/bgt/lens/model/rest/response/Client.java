// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.config.SearchSettings;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * Client
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class Client {

    /**
     * Initializes new instance of User
     */
    public Client() {

    }

    /**
     * Client Id
     */
    @XmlElement(name = "Id", required = false)
    protected int id;

    /**
     * Client key
     */
    @XmlElement(name = "Key", required = false)
    protected String key;

    /**
     * client name
     */
    @XmlElement(name = "Name", required = false)
    protected String name;

    /**
     * Client secret
     */
    @XmlElement(name = "Secret", required = false)
    protected String secret;

    /**
     * Licensed resources
     */
    @XmlElement(name = "LicensedResources", required = false)
    protected List<Integer> licensedResources;

    /**
     * Dump status
     */
    @XmlElement(name = "DumpStatus", required = false)
    protected Boolean dumpStatus;

    /**
     * Locale detection flag
     */
    @XmlElement(name = "EnableLocaleDetection", required = false)
    protected Boolean enableLocaleDetection;
    
    /**
     * Locale detection flag
     */
    @XmlElement(name = "AuthenticationType", required = false)
    protected String authenticationType;

    /**
     * Instance list
     *
     */
    @XmlElement(name = "ClientLensSettings", required = false)
    protected List<ClientLensSettings> clientLensSettings;

    /**
     * List of accessible API
     */
    @XmlElement(name = "ClientApi", required = false)
    protected List<ClientApi> clientApi;

    /**
     * List of default locales
     */
    @XmlElement(name = "DefaultLocales", required = false)
    protected DefaultLocaleList defaultLocales;

    /**
     * List of default locales
     */
    @XmlElement(name = "SearchSettings", required = false)
    protected SearchSettings searchSettings;
    
    /**
     * Vendor list
     *
     */
    @XmlElement(name = "Vendors", required = false)
    protected List<Integer> vendors;
    
    /**
     * RateLimitEnabled
     */
    @XmlElement(name = "RateLimitEnabled", required = false)
    protected boolean rateLimitEnabled;

    /**
     * RateLimitTimePeriod
     */
    @XmlElement(name = "RateLimitTimePeriod", required = false)
    protected Integer rateLimitTimePeriod;

    /**
     * RateLimitCount
     *
     */
    @XmlElement(name = "RateLimitCount", required = false)
    protected Integer rateLimitCount;
    
    
    /**
     * Custom skill Id
     *
     */
    @XmlElement(name = "CustomSkillId", required = false)
    protected Integer customSkillId;

    /**
     * Get client Id
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Set client Id
     *
     * @param Id
     */
    public void setId(int Id) {
        this.id = Id;
    }

    /**
     * Get client key
     *
     * @return
     */
    public String getKey() {
        return key;
    }

    /**
     * Set client key
     *
     * @param Key
     */
    public void setKey(String Key) {
        this.key = Key;
    }

    /**
     * Get client name
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Set client name
     *
     * @param Name
     */
    public void setName(String Name) {
        this.name = Name;
    }

    /**
     * Get client secret
     *
     * @return
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Set client secret
     *
     * @param Secret
     */
    public void setSecret(String Secret) {
        this.secret = Secret;
    }

    /**
     * Get licensed resource list
     *
     * @return
     */
    public List<Integer> getLicensedResources() {
        return licensedResources;
    }

    /**
     * Set licensed resource list
     *
     * @param LicensedResources
     */
    public void setLicensedResources(List<Integer> LicensedResources) {
        this.licensedResources = LicensedResources;
    }

    /**
     * Get document dump status
     *
     * @return
     */
    public Boolean isDumpStatus() {
        return dumpStatus;
    }

    /**
     * Set document dump status
     *
     * @param DumpStatus
     */
    public void setDumpStatus(Boolean DumpStatus) {
        this.dumpStatus = DumpStatus;
    }

    /**
     * Get Lens instances
     *
     * @return
     */
    public List<ClientLensSettings> getClientLensSettings() {
        return clientLensSettings;
    }

    /**
     * Set ClientLensSettings
     *
     * @param clientLensSettings
     */
    public void setClientLensSettings(List<ClientLensSettings> clientLensSettings) {
        this.clientLensSettings = clientLensSettings;
    }

    /**
     * Get client API List
     *
     * @return
     */
    public List<ClientApi> getClientApi() {
        return clientApi;
    }

    /**
     * Set client API List
     *
     * @param ClientApi
     */
    public void setClientApi(List<ClientApi> ClientApi) {
        this.clientApi = ClientApi;
    }

    /**
     * Get locale detection flag
     *
     * @return
     */
    public Boolean isEnableLocaleDetection() {
        return enableLocaleDetection;
    }

    /**
     * Set locale detection flag
     *
     * @param enableLocaleDetection
     */
    public void setEnableLocaleDetection(Boolean enableLocaleDetection) {
        this.enableLocaleDetection = enableLocaleDetection;
    }

    /**
     * Get default locale list
     *
     * @return
     */
    public DefaultLocaleList getDefaultLocales() {
        return defaultLocales;
    }

    /**
     * Set default locale list
     *
     * @param defaultLocales
     */
    public void setDefaultLocales(DefaultLocaleList defaultLocales) {
        this.defaultLocales = defaultLocales;
    }
    
    /**
     * Get authentication type
     *
     * @return
     */
    public String getAuthenticationType() {
        return authenticationType;
    }

    /**
     * Set authentication type
     *
     * @param authenticationType
     */
    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }
    
    /**
     * Get search settings
     * @return 
     */
    public SearchSettings getSearchSettings() {
        return searchSettings;
    }

    /**
     * Set search settings
     * @param searchSettings 
     */
    public void setSearchSettings(SearchSettings searchSettings) {
        this.searchSettings = searchSettings;
    }
    
    /**
     * Get vendors
     * @return 
     */
    public List<Integer> getVendors() {
        return vendors;
    }

    /**
     * Set vendors
     * @param vendors 
     */
    public void setVendors(List<Integer> vendors) {
        this.vendors = vendors;
    }
    
    public boolean isRateLimitEnabled() {
        return rateLimitEnabled;
    }

    public void setRateLimitEnabled(boolean rateLimitEnabled) {
        this.rateLimitEnabled = rateLimitEnabled;
    }

    public Integer getRateLimitTimePeriod() {
        return rateLimitTimePeriod;
    }

    public void setRateLimitTimePeriod(Integer rateLimitTimePeriod) {
        this.rateLimitTimePeriod = rateLimitTimePeriod;
    }

    public Integer getRateLimitCount() {
        return rateLimitCount;
    }

    public void setRateLimitCount(Integer rateLimitCount) {
        this.rateLimitCount = rateLimitCount;
    }

    public Integer getCustomSkillId() {
        return customSkillId;
    }

    public void setCustomSkillId(Integer customSkillId) {
        this.customSkillId = customSkillId;
    }   

}
