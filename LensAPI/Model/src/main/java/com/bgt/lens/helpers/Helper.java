// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.helpers;

import com.bgt.lens.LensException;
import com.bgt.lens.api.ApiContext;
import com.bgt.lens.authentication.AuthorizationHeaderParser;
import com.bgt.lens.config.LensServiceConfig;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Enum.ApplicationAPI;
import static com.bgt.lens.helpers.Enum.ApplicationAPI.AdminService;
import static com.bgt.lens.helpers.Enum.ApplicationAPI.ParserService;
import static com.bgt.lens.helpers.Enum.ApplicationAPI.SearchService;
import com.bgt.lens.helpers.Enum.Encoding;
import com.bgt.lens.helpers.Enum.Fields;
import com.bgt.lens.helpers.Enum.Headers;
import com.bgt.lens.helpers.Enum.HttpMethod;
import com.bgt.lens.helpers.Enum.InstanceType;
import com.bgt.lens.helpers.Enum.ParsingInstanceType;
import com.bgt.lens.helpers.Enum.ParsingType;
import com.bgt.lens.helpers.Enum.VariantType;
import com.bgt.lens.helpers.Enum.resumeSearchTypes;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ClientApi;
import com.bgt.lens.model.rest.response.DefaultLocaleList;
import com.bgt.lens.model.rest.response.License;
import com.bgt.lens.model.rest.response.Locale;
// import com.bgt.lens.model.resume.tm.en_us.Customrollup;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.StreamFilter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingRandomAccessFileAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * API Helper
 */
public class Helper implements ServletContextAware {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(Helper.class);
    private static final Logger ADLOGGER = LogManager.getLogger("AdvanceLogger");
    private static final Logger ADSEARCHLOGGER = LogManager.getLogger("AdvancedSearchLogger");
    private final String resdocType = "ResDoc";
    private final String jobdocType = "JobDoc";
    private final String basePackageName = "com.bgt.lens.model";
    private final String resumeResourceName = "resume";
    private final String jobResourceName = "job";
    private static ServletContext servletContext;
    private JAXBContext hrxmlContext = null;
    
    /**
     *
     */
    public Helper(){
    }
    
    /**
     *
     * @param jaxbContext
     */
    public Helper(JAXBContext jaxbContext){
        hrxmlContext = jaxbContext;
    }

    /**
     *
     * @param servletContext
     */
    @Override
    public void setServletContext(ServletContext servletContext) {
        Helper.servletContext = servletContext;
    }

    /**
     * Validate integer
     *
     * @param value
     * @return
     */
    public boolean isValidInteger(Long value) {
        if (value != null) {
            if (value < Integer.MIN_VALUE || value > Integer.MAX_VALUE) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validate integer
     *
     * @param value
     * @return
     */
    public boolean isValidMaxDocumentCount(Long value) {
        if (value != null) {
            if (value < 10 || value > 2000001) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validate encoding type
     *
     * @param encoding
     * @return
     */
    public boolean isValidEncoding(String encoding) {

        for (Encoding encodingName : Encoding.values()) {
            if (!encodingName.toString().equalsIgnoreCase(encoding)) {
            } else {
                return true;
            }
        }

        return false;
    }

    /**
     * Validate resume field
     *
     * @param fieldList
     * @return
     */
    public boolean isValidResumeFieldList(List<String> fieldList) {
        boolean contains = false;
        
        for (String field : fieldList) {            
            for (Fields f : Fields.values()) {
                if (f.name().equalsIgnoreCase(field)) {
                    return true;
                }
            }            
        }
        return contains;
    }

    /**
     * Parse date
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public Date parseDate(String date) throws ParseException {
        try {
            if (isNullOrEmpty(date)) {
                throw new ParseException("Invalid Date. Date is empty or null", 0);
            }            
            LocalDate dt = getDateFormat().parseLocalDate(date);
            return dt.toDateTimeAtStartOfDay().toDate();
        } catch (Exception ex) {
            throw new ParseException(ex.getLocalizedMessage(), 0);
        }

    }

    /**
     * Get date format
     *
     * @return
     */
    public DateTimeFormatter getDateFormat() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        return formatter;
    }

    /**
     * Get resource name from URL
     *
     * @param urlContext
     * @param apiContext
     * @return
     */
    public String getResourceName(String urlContext, ApiContext apiContext) {
        String action = null;

        urlContext = urlContext.startsWith("/") ? urlContext.substring(1) : urlContext;
        if (isNotNullOrEmpty(urlContext)) {
            String routeData[] = urlContext.split("/");
            if (routeData.length >= 2) {
                 if (AdminService.name().equalsIgnoreCase(routeData[0])) {
                    if (isNotNull(apiContext)) {
                        apiContext.setApi(AdminService.toString());
                    }
                    action = routeData[1];
                } else if (ParserService.name().equalsIgnoreCase(routeData[0])) {
                    if (isNotNull(apiContext)) {
                        apiContext.setApi(ParserService.toString());
                    }
                    action = routeData[1];
                } else if (SearchService.name().equalsIgnoreCase(routeData[0])) {
                    if (isNotNull(apiContext)) {
                        apiContext.setApi(SearchService.toString());
                    }
                    action = routeData[1];
                }
            }

        }
        return action;
    }

    /**
     * Get SOAP operation name
     *
     * @param soapOperationName
     * @return
     */
    public String getSoapResourceName(String soapOperationName) {
        HashMap<String, String> resourceMap = getListOfSoapResources();
        return resourceMap.get(soapOperationName);
    }

    /**
     * Get SOAP API name
     *
     * @param soapOperationName
     * @return
     */
    public String getSoapApiName(String soapOperationName) {
        HashMap<String, String> resourceMap = getListOfSoapApis();
        return resourceMap.get(soapOperationName);
    }

    /**
     * Check the string is empty
     *
     * @param input
     * @return
     */
    public boolean isEmpty(String input) {
        if (input == null) {
            return false;
        }
        return input.trim().equals("");
    }

    /**
     * Check string is not empty
     *
     * @param input
     * @return
     */
    public boolean isNotEmpty(String input) {
        if (input == null) {
            return false;
        }
        return !input.trim().equals("");
    }

    /**
     * Check object null
     *
     * @param input
     * @return
     */
    public boolean isNull(Object input) {
        return input == null;
    }

    /**
     * Check object not null
     *
     * @param input
     * @return
     */
    public boolean isNotNull(Object input) {
        return input != null;
    }

    /**
     * Check string is null or empty
     *
     * @param s
     * @return
     */
    public boolean isNullOrEmpty(String s) {
        return s == null || s.trim().length() == 0;
    }

    /**
     * Check canon document error
     *
     * @param lensResult
     * @return
     */
    public boolean isCanonInvalidDocument(String lensResult) {
        boolean isError;
        isError = (lensResult.contains("<error>no doc found</error>"));
        if (isError) {
            LOGGER.error("Parsing error: " + lensResult);
        }
        return isError;
    }

    /**
     * Check string is not null or not empty
     *
     * @param s
     * @return
     */
    public boolean isNotNullOrEmpty(String s) {
        return s != null && s.trim().length() != 0;
    }

    /**
     * Check string is not null or whitespace
     *
     * @param s
     * @return
     */
    public boolean isNullOrWhitespace(String s) {
        return s == null || isWhitespace(s);

    }

    /**
     * Check string is whitespace
     *
     * @param s
     * @return
     */
    private boolean isWhitespace(String s) {
        int length = s.length();
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                if (!Character.isWhitespace(s.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Decode base64 string
     *
     * @param base64EncodeString
     * @return
     * @throws UnsupportedEncodingException
     */
    public byte[] decodeBase64String(String base64EncodeString) throws UnsupportedEncodingException {
        return Base64.decodeBase64(base64EncodeString.getBytes("UTF-8"));
    }

    /**
     * Base64 encode string
     *
     * @param binaryData
     * @return
     * @throws UnsupportedEncodingException
     */
    public String encodeBase64String(byte[] binaryData) throws UnsupportedEncodingException {
        byte[] encodedBytes = Base64.encodeBase64(binaryData);
        return new String(encodedBytes, "UTF-8");
    }

    /**
     * Long to Integer
     *
     * @param l
     * @return
     */
    public int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            throw new IllegalArgumentException(l + " cannot be cast to int without changing its value.");
        }
        return (int) l;
    }

    /**
     * String to Boolean
     *
     * @param string
     * @return
     */
    public boolean stringToBool(String string) {
        if (string.equalsIgnoreCase("true")) {
            return true;
        }
        if (string.equalsIgnoreCase("false")) {
            return false;
        }
        throw new IllegalArgumentException(string + " is not a bool. Only true and false are.");
    }

    /**
     * Gregorian calendar to normal date
     *
     * @param date
     * @return
     */
    public XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {

        }
        return xmlCalendar;
    }

    /**
     * Check alphanumeric
     *
     * @param s
     * @return
     */
    public boolean isAlphaNumeric(String s) {
        String pattern = "^[a-zA-Z0-9]*$";
        return s.matches(pattern);
    }

    /**
     * Check alphanumeric
     *
     * @param s
     * @return
     */
    public boolean isValidDocumentId(String s) {
        String pattern = "^[a-zA-Z0-9-_]*$";
        return s.matches(pattern);
    }

    /**
     * Check alphanumeric
     *
     * @param s
     * @return
     */
    public boolean isValidLensVersion(String s) {
        String pattern = "^[a-zA-Z0-9].*$";
        return s.matches(pattern);
    }

    /**
     * Check secret strength
     *
     * @param s
     * @return
     */
    public boolean isValidSecret(String s) {
        // String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^+=])(?=\\S+$).{5,}$";
        String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{32,}$";
        return s.matches(pattern);
    }

    /**
     *
     * @param locale
     * @return
     */
    public boolean isValidLocale(String locale) {
        String pattern = "(?:.*[a-z])(?:_.*[a-z])";
        return locale.matches(pattern);
    }

    /**
     * Check alphanumeric with special characters
     *
     * @param s
     * @return
     */
    public boolean isAlphaNumericWithSomeSpecialCharacter(String s) {
        String pattern = "^[a-zA-Z_0-9\\s-]*$";
        return s.matches(pattern);
    }

    /**
     * Validate host name
     *
     * @param s
     * @return
     */
    public boolean isValidHostName(String s) {
        // http://jsfiddle.net/DanielD/8S4nq/
        String pattern = "((^\\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\\s*$)|(^\\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:)))(%.+)?\\s*$))|(^\\s*((?=.{1,255}$)(?=.*[A-Za-z].*)[0-9A-Za-z](?:(?:[0-9A-Za-z]|\\b-){0,61}[0-9A-Za-z])?(?:\\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|\\b-){0,61}[0-9A-Za-z])?)*)\\s*$)";
        return s.matches(pattern);
    }

    /**
     * Gregorian date to normal date
     *
     * @param calendar
     * @return
     */
    public Date xmlGregorianCalendarToDate(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }

    /**
     * Check list size
     *
     * @param object
     * @return
     */
    public boolean sizeGreaterThanZero(List<Object> object) {
        return object.size() > 0;
    }

    /**
     * Get SOAP resource list
     *
     * @return
     */
    public HashMap<String, String> getListOfSoapResources() {
        HashMap<String, String> resourceMap = new HashMap<>();
        resourceMap.put("CreateConsumerRequest", "clients");
        resourceMap.put("UpdateConsumerRequest", "clients");
        resourceMap.put("DeleteConsumerRequest", "clients");
        resourceMap.put("GetClientsRequest", "clients");
        resourceMap.put("GetClientByIdRequest", "clients");

        resourceMap.put("CreateApiRequest", "apis");
        resourceMap.put("UpdateApiRequest", "apis");
        resourceMap.put("DeleteApiRequest", "apis");
        resourceMap.put("GetApiRequest", "apis");
        resourceMap.put("GetApiByIdRequest", "apis");

        resourceMap.put("AddLensSettingsRequest", "lens");
        resourceMap.put("UpdateLensSettingsRequest", "lens");
        resourceMap.put("DeleteLensSettingsRequest", "lens");
        resourceMap.put("GetLensSettingsRequest", "lens");
        resourceMap.put("GetLensSettingsByIdRequest", "lens");

        resourceMap.put("GetAllLicenseDetailsRequest", "licenseall");
        resourceMap.put("GetLicenseRequest", "license");

        resourceMap.put("AddResourceRequest", "resources");
        resourceMap.put("UpdateResourceRequest", "resources");
        resourceMap.put("DeleteResourceRequest", "resources");
        resourceMap.put("GetResourceRequest", "resources");
        resourceMap.put("GetResourceByIdRequest", "resources");

        resourceMap.put("GetUsageLogsRequest", "UsageLogs");
        resourceMap.put("GetUsageLogByIdRequest", "UsageLogs");

        resourceMap.put("GetUsageCounterRequest", "UsageCounters");
        resourceMap.put("GetUsageCounterByIdRequest", "UsageCounters");

        resourceMap.put("GetErrorLogsRequest", "ErrorLogs");
        resourceMap.put("GetErrorLogByIdRequest", "ErrorLogs");

        resourceMap.put("InfoRequest", "status");
        resourceMap.put("PingRequest", "status");
        resourceMap.put("ConvertRequest", "converter");
        resourceMap.put("CanonRequest", "canonicalizer");
        resourceMap.put("ParseResumeRequest", "resume");
        resourceMap.put("ParseResumeBGXMLRequest", "resume");
        resourceMap.put("ParseResumeStructuredBGXMLRequest", "resume");
        resourceMap.put("ParseResumeHRXMLRequest", "resume");
        resourceMap.put("ParseResumeWithRTFRequest", "resume");
        resourceMap.put("ParseResumeWithHTMRequest", "resume");
        resourceMap.put("ParseResumeWithXMLRequest", "resume");

        resourceMap.put("ParseJobRequest", "job");
        resourceMap.put("ParseJobBGXMLRequest", "job");
        resourceMap.put("ParseJobStructuredBGXMLRequest", "job");
        resourceMap.put("ParseJobHRXMLRequest", "job");
        resourceMap.put("ParseJobWithRTFRequest", "job");
        resourceMap.put("ParseJobWithHTMRequest", "job");
        resourceMap.put("ParseJobWithXMLRequest", "job");

        resourceMap.put("ProcessLocaleRequest", "processlocale");

        resourceMap.put("CreateVendorRequest", "vendor");
        resourceMap.put("OpenVendorRequest", "vendor");
        resourceMap.put("CloseVendorRequest", "vendor");
        resourceMap.put("DeleteVendorRequest", "vendor");
        resourceMap.put("UpdateVendorRequest", "vendor");
        resourceMap.put("GetVendorByIdRequest", "vendor");
        resourceMap.put("GetVendorByCriteriaRequest", "vendorlist");
        resourceMap.put("RegisterResumeRequest", "resume");
        resourceMap.put("UnregisterResumeRequest", "resume");
        resourceMap.put("ResumeSearchRequest", "resumesearch");
        resourceMap.put("FetchResumeRequest", "resume");
        resourceMap.put("GetRegisterLogByIdRequest", "registerLog");
        resourceMap.put("GetRegisterLogByCriteriaRequest", "registerLog");
        resourceMap.put("GetSearchCommandDumpByIdRequest", "commandDump");
        resourceMap.put("GetSearchCommandDumpByCriteriaRequest", "commandDump");
        resourceMap.put("GetFacetFilterListRequest", "facets");
        resourceMap.put("GetFacetDistributionRequest", "facets");
        resourceMap.put("RegisterJobRequest", "job");
        resourceMap.put("UnregisterJobRequest", "job");
        resourceMap.put("JobSearchRequest", "jobsearch");
        resourceMap.put("FetchJobRequest", "job");
        resourceMap.put("GetDocIdsJobRequest", "job");
        resourceMap.put("GetDocIdsResumeRequest", "resume");

        return resourceMap;
    }

    /**
     * Get SOAP API list
     *
     * @return
     */
    public HashMap<String, String> getListOfSoapApis() {
        HashMap<String, String> resourceMap = new HashMap<>();
        resourceMap.put("CreateConsumerRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("UpdateConsumerRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("DeleteConsumerRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetClientsRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetClientByIdRequest", ApplicationAPI.AdminService.toString());

        resourceMap.put("CreateApiRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("UpdateApiRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("DeleteApiRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetApiRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetApiByIdRequest", ApplicationAPI.AdminService.toString());

        resourceMap.put("AddLensSettingsRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("UpdateLensSettingsRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("DeleteLensSettingsRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetLensSettingsRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetLensSettingsByIdRequest", ApplicationAPI.AdminService.toString());

        resourceMap.put("GetAllLicenseDetailsRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetLicenseRequest", ApplicationAPI.AdminService.toString());

        resourceMap.put("AddResourceRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("UpdateResourceRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("DeleteResourceRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetResourceRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetResourceByIdRequest", ApplicationAPI.AdminService.toString());

        resourceMap.put("GetUsageLogsRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetUsageLogByIdRequest", ApplicationAPI.AdminService.toString());

        resourceMap.put("GetUsageCounterRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetUsageCounterByIdRequest", ApplicationAPI.AdminService.toString());

        resourceMap.put("GetErrorLogsRequest", ApplicationAPI.AdminService.toString());
        resourceMap.put("GetErrorLogByIdRequest", ApplicationAPI.AdminService.toString());

        resourceMap.put("InfoRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("PingRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ConvertRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("CanonRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseResumeRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseResumeBGXMLRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseResumeStructuredBGXMLRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseResumeHRXMLRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseResumeWithRTFRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseResumeWithHTMRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseResumeWithXMLRequest", ApplicationAPI.ParserService.toString());

        resourceMap.put("ParseJobRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseJobBGXMLRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseJobStructuredBGXMLRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseJobHRXMLRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseJobWithRTFRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseJobWithHTMRequest", ApplicationAPI.ParserService.toString());
        resourceMap.put("ParseJobWithXMLRequest", ApplicationAPI.ParserService.toString());

        resourceMap.put("ProcessLocaleRequest", ApplicationAPI.ParserService.toString());

        resourceMap.put("CreateVendorRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("OpenVendorRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("CloseVendorRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("DeleteVendorRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("UpdateVendorRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetVendorByIdRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetVendorByCriteriaRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("RegisterResumeRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("UnregisterResumeRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("ResumeSearchRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("FetchResumeRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetRegisterLogByIdRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetRegisterLogByCriteriaRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetSearchCommandDumpByIdRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetSearchCommandDumpByCriteriaRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetFacetFilterListRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetFacetDistributionRequest", ApplicationAPI.SearchService.toString());

        resourceMap.put("RegisterJobRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("UnregisterJobRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("FetchJobRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("JobSearchRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetDocIdsResumeRequest", ApplicationAPI.SearchService.toString());
        resourceMap.put("GetDocIdsJobRequest", ApplicationAPI.SearchService.toString());
        return resourceMap;
    }

    /**
     * Validate max transaction count
     *
     * @param count
     * @return
     */
    public boolean validateTransactionCount(Integer count) {

        return count <= 999999999;
    }

    /**
     * Get Object to Json
     *
     * @param obj
     * @return
     * @throws JsonProcessingException
     */
    public String getJsonStringFromObject(Object obj) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(obj);
    }

    /**
     * Get Object to Json
     *
     * @param obj
     * @return
     * @throws JsonProcessingException
     */
    public String getJsonStringFromObjectWithoutNull(Object obj) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_EMPTY).writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(obj);
    }

    /**
     * Get Object to Json
     *
     * @param obj
     * @return
     * @throws JsonProcessingException
     */
    public String getCamelCaseJsonStringFromObject(Object obj) throws JsonProcessingException {
        // ObjectWriter ow = new ObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.PASCAL_CASE_TO_CAMEL_CASE).writer().withDefaultPrettyPrinter();
//        ObjectWriter ow = new ObjectMapper().enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY).writer();
        ObjectWriter ow = new ObjectMapper().writer();
        return ow.writeValueAsString(obj);
    }

    /**
     * Get Object to Json
     *
     * @param obj
     * @return
     * @throws JsonProcessingException
     */
    public String getCamelCaseJsonStringFromObjectMinfied(Object obj) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer();
//         ObjectWriter ow = new ObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.PASCAL_CASE_TO_CAMEL_CASE).writer();
       //  ow = new ObjectMapper().enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY).writer();
         //ow = new ObjectMapper().writer();
        return ow.writeValueAsString(obj);
    }

    /**
     * Get Object to XML
     *
     * @param obj
     * @return
     * @throws JAXBException
     */
    public String getXMLStringFromObject(Object obj) throws JAXBException {
        LOGGER.info("Usage log - XMLstring from object - Start - " + Date.from(Instant.now()));
        StringWriter stringWriter = null, writer = null;
        if (obj instanceof String) {
            return (String) obj;
        }
        try {
            JAXBContext jc = JAXBContext.newInstance(obj.getClass(), JSONObject.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            stringWriter = new StringWriter();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.marshal(obj, stringWriter);

            // remove empty tags
//            final InputSource src = new InputSource(new StringReader(stringWriter.toString().replaceAll("<(\\w+)>\\s*</\\1>|<\\w+/>", "")));
//            final Element document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
//            final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
//            final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
//            final LSSerializer lsSerializer = impl.createLSSerializer();
//            writer = new StringWriter();
//            lsSerializer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
//            lsSerializer.getDomConfig().setParameter("xml-declaration", true);
//            LSOutput lsOutput = impl.createLSOutput();
//            lsOutput.setEncoding("UTF-8");
//            lsOutput.setCharacterStream(writer);
//            lsSerializer.write(document, lsOutput);
            return stringWriter.toString().replaceAll("<(\\w+)>\\s*</\\1>|<\\w+/>", "");
        } catch (Exception ex) {
            return stringWriter.toString().replaceAll("<(\\w+)>\\s*</\\1>|<\\w+/>", "");
        }
    }

    /**
     * Get Object to XML For SOAP Response
     *
     * @param qname
     * @param obj
     * @return
     * @throws JAXBException
     */
    public String getXMLStringFromObjectForSOAPResponse(String qname, Object obj) throws JAXBException {

        // JAXBElement<ApiResponse> jaxbElement = new JAXBElement(new QName(qname), ApiResponse.class, obj);
        JAXBElement jaxbElement = new JAXBElement (new QName(qname), obj.getClass(), obj);
        JAXBContext jc = JAXBContext.newInstance(obj.getClass());
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(jaxbElement, stringWriter);

        return stringWriter.toString();
    }

    /**
     * Get ApiContext
     *
     * @return
     */
    public ApiContext getApiContext() {
        ApiContext apiContext = new ApiContext();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if (request.getAttribute("ApiContext") != null) {
            apiContext = (ApiContext) request.getAttribute("ApiContext");
        }
        return apiContext;
    }

    /**
     * Set ApiContext
     *
     * @param apiContext
     */
    public void setApiContext(ApiContext apiContext) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        request.setAttribute("ApiContext", apiContext);
    }

    /**
     * Validate instance type
     *
     * @param type
     * @return
     */
    public boolean isValidInstanceType(String type) {

        for (InstanceType iType : InstanceType.values()) {
            if (iType.toString().equalsIgnoreCase(type)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Validate Resume Search type
     *
     * @param resumeSearchType
     * @return
     */
    public boolean isValidResumeSearchType(String resumeSearchType) {

        for (resumeSearchTypes rSearchTypes : resumeSearchTypes.values()) {
            if (rSearchTypes.toString().equalsIgnoreCase(resumeSearchType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Validate scoring mode
     *
     * @param scoringModeFilter
     * @return
     */
    public boolean isValidScoringMode(String scoringModeFilter) {

        for (Enum.scoringMode scoring : Enum.scoringMode.values()) {
            if (scoring.name().equalsIgnoreCase(scoringModeFilter)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Validate instance type
     *
     * @param type
     * @return
     */
    public boolean isValidParsingInstanceType(String type) {

        for (InstanceType iType : InstanceType.values()) {
            if (iType.toString().equalsIgnoreCase(type) && (!type.equalsIgnoreCase(InstanceType.LD.toString()))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Validate instance type
     *
     * @param type
     * @return
     */
    public boolean isValidLocaleDetectorInstanceType(String type) {

        for (InstanceType iType : InstanceType.values()) {
            if ((type.equalsIgnoreCase(InstanceType.LD.toString()))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Validate resume parsing variant type
     *
     * @param type
     * @return
     */
    public boolean isValidVariantType(String type) {

        for (VariantType variantType : VariantType.values()) {
            if (variantType.name().equalsIgnoreCase(type)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if object property defined
     *
     * @param obj
     * @param fieldName
     * @return
     */
    public boolean isPropertyDefined(Object obj, String fieldName) {
        for (Field f : obj.getClass().getFields()) {
            if (f.getName().equalsIgnoreCase(fieldName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check Len processing failure
     *
     * @param lensResult
     * @return
     */
    public boolean checkLensResultForFailure(String lensResult) {
        boolean isError;
        isError = !(StringUtils.containsIgnoreCase(lensResult, "<error>")
                || StringUtils.containsIgnoreCase(lensResult, "error tagging")
                || StringUtils.containsIgnoreCase(lensResult, "error converting")
                || StringUtils.containsIgnoreCase(lensResult, "error tagging:")
                || StringUtils.containsIgnoreCase(lensResult, "error converting:")
                || StringUtils.containsIgnoreCase(lensResult, "<fail")
                || StringUtils.containsIgnoreCase(lensResult, "failure>")
                || StringUtils.containsIgnoreCase(lensResult, "Empty document text")
                || StringUtils.containsIgnoreCase(lensResult, "(tag:"));
        if (!isError) {
            LOGGER.error("Parsing error: " + lensResult);
        }
        return isError;
    }

    /**
     * BGTXML to HRXML conversion
     *
     * @param bgtXML
     * @param hrXML
     * @return
     * @throws Exception
     */
    @PerformanceLogging
    public String applyStyleSheet(String bgtXML, String hrXML) throws Exception {
        try {
            LOGGER.info("Started to transform BGTXML to HRXML");
            StreamSource bgtXMLSource = new StreamSource(new StringReader(bgtXML));
            //StreamSource xslStyleSheet = new StreamSource(LensRepository.class.getResourceAsStream(hrxmlFilePath));
            StreamSource xslStyleSheet = new StreamSource(new StringReader(hrXML));

            TransformerFactory factory = new org.apache.xalan.processor.TransformerFactoryImpl();
            Transformer transformer = factory.newTransformer(xslStyleSheet);
            StringWriter hrxmlWriter = new StringWriter();
            StreamResult result = new StreamResult(hrxmlWriter);
            transformer.transform(bgtXMLSource, result);
            LOGGER.info("Transformed BGTXML to HRXML");
            return hrxmlWriter.toString();
        } catch (Exception | TransformerFactoryConfigurationError ex) {
            LOGGER.error("Transforming HRXML failed. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), getErrorMessageWithURL(ApiErrors.STYLESHEET_TRANSFORMATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Validate httpMethod
     *
     * @param httpMethod
     * @return
     */
    public boolean isValidHttpMethod(String httpMethod) {
        // TODO : want to optimize : instead of looping
        for (HttpMethod httpMethodEnum : HttpMethod.values()) {
            if (httpMethodEnum.name().equalsIgnoreCase(httpMethod)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param resource
     * @return
     */
    public boolean isValidAPIResource(String resource) {
        if (getListOfSoapResources().containsValue(resource)) {
            return true;
        }
        return false;
    }

    /**
     * Get Time difference in seconds
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public String getTimeDifference(Date startTime, Date endTime) {
        double diffenceFloat = (double)(endTime.getTime() - startTime.getTime()) / 1000;
        DecimalFormat df = new DecimalFormat("#.###");
        
//        String diffenceSeconds = String.valueOf((diffenceFloat / 1000 % 60));
        return String.valueOf(df.format(diffenceFloat));
    }

    /**
     * Update performance log
     * @param apiContext
     */
    public void updatePerformanceLog(ApiContext apiContext) {
        try {
            // Get log file from the advanced logger file appender
            LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
            Configuration config = ctx.getConfiguration();
//            ApiContext apiContext = getApiContext();
            RollingRandomAccessFileAppender fileAppender = null;
            apiContext.getAdvanceLog().setUtilityDataProcessing(0);
            if (ParserService.toString().equalsIgnoreCase(apiContext.getApi())) {            
                fileAppender = (RollingRandomAccessFileAppender) config.getAppender("AdvanceLogger");
                if (fileAppender != null) {

                    // Check file access permission
                    File file = new File(fileAppender.getFileName());
                    if (!file.canWrite()) {
                        LOGGER.error("Unable to create/update advanced log");
                    } else {
                        // Generate log data and update log file
                        DateFormat advformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

                        boolean raisedError = true;
                        if (apiContext.getApiResponse() != null) {
                            raisedError = !apiContext.getApiResponse().status;
                        }
                        AdvanceLog advanceLog = apiContext.getAdvanceLog();
                        String fileContent = apiContext.getRequestId().toString() + ","
                                + apiContext.getResource() + ","
                                + apiContext.getRequestUri() + ","
                                + apiContext.getMethod() + ","
                                + advanceLog.getApiRateLimit()+ ","
                                + advanceLog.getProductRateLimit()+ ","
                                + advanceLog.getAuthenticationValidation() + ","
                                + advanceLog.getAuthenticationLog() + ","
                                + advanceLog.getParsingDump() + ","
                                + advanceLog.getHrxmlStyleSheetUpdate() + ","
                                + advanceLog.getLocaleDetectionDump() + ","
                                + advanceLog.getLensCommunication() + ","
                                + advanceLog.getUpdateTransactionCount() + ","
                                + advanceLog.getLensDocumentDump() + ","
                                + advanceLog.getSearchControllerUnit() + ","
                                + advanceLog.getUsageCounterUpdate() + ","
                                + advanceLog.getUsageLog() + ","
                                + advanceLog.getErrorLog() + ","
                                + raisedError + ","
                                + advformatter.format(apiContext.getInTime()) + ","
                                + advformatter.format(apiContext.getOutTime()) + ","
                                + apiContext.getSeconds();

                        ADLOGGER.info(fileContent);
                    }
                } else {
                    LOGGER.error("Unable to intialize advance logger. Please check the configuration.");
                }
            } else if (SearchService.toString().equalsIgnoreCase(apiContext.getApi())) {                
                fileAppender = (RollingRandomAccessFileAppender) config.getAppender("AdvancedSearchLogger");
                if (fileAppender != null) {

                    // Check file access permission
                    File file = new File(fileAppender.getFileName());
                    if (!file.canWrite()) {
                        LOGGER.error("Unable to create/update advanced log");
                    } else {
                        // Generate log data and update log file
                        DateFormat advformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

                        boolean raisedError = true;
                        if (apiContext.getApiResponse() != null) {
                            raisedError = !apiContext.getApiResponse().status;
                        }
                        AdvanceLog advanceLog = apiContext.getAdvanceLog();
                        String fileContent = apiContext.getRequestId().toString() + ","
                                + apiContext.getResource() + ","
                                + apiContext.getRequestUri() + ","
                                + apiContext.getMethod() + ","
                                + advanceLog.getApiRateLimit()+ ","
                                + advanceLog.getProductRateLimit()+ ","
                                + advanceLog.getAuthenticationValidation() + ","
                                + advanceLog.getAuthenticationLog() + ","
                                + advanceLog.getRegisterParseDocument() + ","
                                + advanceLog.getRegisterDocumentWithCustomFilterUnit() + ","
                                + advanceLog.getRegisterDocumentWithoutCustomFilterUnit() + ","
                                + advanceLog.getUnregisterDocumentUnit() + ","
                                + advanceLog.getOverwriteRegisterDocumentUnit() + ","
                                + advanceLog.getUpdateRegisterDocumentCount() + ","
                                + advanceLog.getFacetDistributionCommandBuildUnit() + ","
                                + advanceLog.getFacetDistributionCommandProcessUnit() + ","
                                + advanceLog.getSearchParseDocument() + ","
                                + advanceLog.getSearchCommandbuildUnit() + ","
                                + advanceLog.getSearchCommandProcessUnit() + ","
                                + advanceLog.getSearchResultProcessUnit() + ","
                                + advanceLog.getUpdateTransactionCount() + ","
                                + advanceLog.getFetchDocumentProcessUnit() + ","
                                + advanceLog.getSearchServiceUnit() + ","
                                + advanceLog.getSearchCommandDumpUnit() + ","
                                + advanceLog.getSearchControllerUnit() + ","
                                + advanceLog.getUsageCounterUpdate() + ","
                                + advanceLog.getUsageLog() + ","
                                + advanceLog.getErrorLog() + ","
                                + raisedError + ","
                                + advformatter.format(apiContext.getInTime()) + ","
                                + advformatter.format(apiContext.getOutTime()) + ","
                                + apiContext.getSeconds();

                        ADSEARCHLOGGER.info(fileContent);
                    }
                } else {
                    LOGGER.error("Unable to intialize advance logger. Please check the configuration.");
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }

    }

    /**
     *
     * @param result
     * @return
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public String getErrorResponseFromLensResult(String result) throws SAXException, ParserConfigurationException, IOException {
        try {
            if (isNullOrEmpty(result)) {
                return result;
            }

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource inStream = new InputSource();
            inStream.setCharacterStream(new StringReader(result));
            Document doc = dBuilder.parse(inStream);
            NodeList nodeList = doc.getElementsByTagName("error");

            if (nodeList != null && nodeList.getLength() > 0) {
                return nodeList.item(0).getTextContent();
            } else {
                return result;
            }
        } catch (ParserConfigurationException | SAXException | IOException | DOMException ex) {
            return result;
        }
    }

    /**
     *
     * @param resume
     * @return
     * @throws JAXBException
     */
    public Object changeResponseDataHRXMLObject(Object resume) throws JAXBException {

        String result = "";
        ApiContext apiContext = getApiContext();
        if (isNotNullOrEmpty(apiContext.getAccept()) && isNotNull(resume)) {
             if (isNotNull(apiContext.getAccept()) && Enum.Headers.ApplicationJSON.toString().equalsIgnoreCase(apiContext.getAccept())) {
                try {
                    com.bgt.lens.model.rest.response.Resume resumeTag = new com.bgt.lens.model.rest.response.Resume();
                    resumeTag.resume = resume;

                    String update = getCamelCaseJsonStringFromObject(resumeTag);
                    JSONObject JSO = JSONObject.fromObject(update);
                    return JSO;
                } catch (Exception ex) {
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), getErrorMessageWithURL(ApiErrors.JSON_TRANSFORM_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                result = getXMLStringFromObject(resume);
                return result;
            }
        }
        if (isNullOrEmpty(result)) {
            result = getXMLStringFromObject(resume);
            return result;
        }
        return result;
    }

    // TODO : added for JSON parse

    /**
     *
     * @param object
     * @return
     * @throws JAXBException
     */
        public Object changeResponseDataObjectForStructuredJSON(Object object) throws JAXBException {
        String result = "";
        String camelCaseResponse = "";
        ApiContext apiContext = getApiContext();
        if (isNotNullOrEmpty(apiContext.getAccept()) && isNotNull(object)) {
              if (isNotNull(apiContext.getAccept()) && Enum.Headers.ApplicationJSON.toString().equalsIgnoreCase(apiContext.getAccept())) {
                try {
                    if (ParsingType.RESUME.toString().equals(apiContext.getResource())) {
                        com.bgt.lens.model.rest.response.ResDoc resumeTag = new com.bgt.lens.model.rest.response.ResDoc();
                        resumeTag.ResDoc = object;
                        // No special chars in JSON field names(keys)
                        camelCaseResponse = getCamelCaseJsonStringFromObjectMinfied(resumeTag).replaceAll("[\"][a-zA-Z0-9_]*[\"]:null[ ]*[,]?", "");

                    } else {
                        com.bgt.lens.model.rest.response.JobDoc jobTag = new com.bgt.lens.model.rest.response.JobDoc();
                        jobTag.JobDoc = object;
                        // No special chars in JSON field names(keys)
                        camelCaseResponse = getCamelCaseJsonStringFromObjectMinfied(jobTag).replaceAll("[\"][a-zA-Z0-9_]*[\"]:null[ ]*[,]?", "");
                    }
                    JSONObject JSO = JSONObject.fromObject(camelCaseResponse);
                    return JSO;
                } catch (Exception ex) {
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), getErrorMessageWithURL(ApiErrors.JSON_TRANSFORM_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                result = getXMLStringFromObject(object);
                return result;
            }
        }
        if (isNullOrEmpty(result)) {
            result = getXMLStringFromObject(object);
            return result;
        }
        return result;
    }

    /**
     *
     * @param result
     * @return
     */
    public Object changeResponseDataObject(String result) {
        ApiContext apiContext = getApiContext();
        if (isNotNullOrEmpty(apiContext.getAccept()) && isNotNullOrEmpty(result)) {
             if (isNotNull(apiContext.getAccept()) && Enum.Headers.ApplicationJSON.toString().equalsIgnoreCase(apiContext.getAccept())) {            
                try {
                    XMLSerializer xmlSerializer = new XMLSerializer();
                    xmlSerializer.setForceTopLevelObject(true);
                    xmlSerializer.setTypeHintsEnabled(false);
                    // Remove empty tags
                    JSON out = xmlSerializer.read(result.replaceAll("<(\\w+)></\\1>|<\\w+/>", ""));

                    String update = out.toString().replaceAll("\":null", "\":{}");
                    JSONObject JSO = JSONObject.fromObject(update);
                    return JSO;
                } catch (Exception ex) {
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), getErrorMessageWithURL(ApiErrors.JSON_TRANSFORM_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                return result;
            }
        }
        return result;
    }

    /**
     *
     * @param document
     * @return
     * @throws Exception
     */
    public String printXmlDocument(Document document) throws Exception {
        DOMSource domSource = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(domSource, result);
        String response = writer.toString();
        return response;
    }

    /**
     * Method to get the locale from the bgtres result from locale instance and
     * return the coreLenssettings.
     *
     * @param locMessage
     * @return locale <code>String</code>
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     * @throws com.bgt.lens.LensException
     */
    public String getLocale(String locMessage) throws ParserConfigurationException, SAXException, IOException, LensException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = factory.newDocumentBuilder();
            InputSource inStream = new InputSource();
            inStream.setCharacterStream(new java.io.StringReader(locMessage));
            org.w3c.dom.Document doc = db.parse(inStream);
            NodeList nodeList = doc.getElementsByTagName("locale");

            for (int index = 0; index < nodeList.getLength(); index++) {
                org.w3c.dom.Node node = nodeList.item(index);
                if (node.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    org.w3c.dom.Element element = (org.w3c.dom.Element) node;
                    //Node locNode = element.getAttributeNode("code");
                    return element.getTextContent();
                }
            }
            throw new ApiException("", "Unable to get the locale information from the parsed xml", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (SAXException | ParserConfigurationException sxex) {
            throw new ApiException("", "Unable to get the locale information from the parsed xml", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @param lenssettingses
     * @param lensMessage
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws LensException
     */
    public CoreLenssettings getLensSettingsByLocale(List<CoreLenssettings> lenssettingses, String lensMessage) throws ParserConfigurationException, SAXException, IOException, LensException {
        try {
            String locale = getLocale(lensMessage);
            CoreLenssettings coreLenssettings = null;
            if (lenssettingses != null && lenssettingses.size() > 0) {
                for (CoreLenssettings coreLenssetting : lenssettingses) {
                    if (coreLenssetting.getLocale().equalsIgnoreCase(locale)) {
                        coreLenssettings = coreLenssetting;
                        break;
                    }
                }
            }
            if (coreLenssettings != null) {
                return coreLenssettings;
            } else {
                throw new ApiException("", "Instance not found", HttpStatus.METHOD_NOT_ALLOWED);
            }

        } catch (ParserConfigurationException | SAXException | IOException | LensException ex) {
            throw ex;
        }
    }

    /**
     * Get Object to Json
     *
     * @param json
     * @param obj
     * @return
     * @throws JsonProcessingException
     */
    public Object getObjectFromJson(String json, Object obj) throws JsonProcessingException, IOException {
        if (!isNullOrEmpty(json)) {
            Object response = new ObjectMapper().readValue(json, obj.getClass());
            return response;
        }
        return null;
    }

    /**
     *
     * @param DefaultLocale
     * @param lenssettingses
     * @param instanceType
     * @return
     * @throws IOException
     */
    public List<CoreLenssettings> updateDefaultLocaleList(String DefaultLocale, List<CoreLenssettings> lenssettingses, String instanceType) throws IOException {
        List<CoreLenssettings> updatedLenssettingses = new ArrayList<>();
        List<CoreLenssettings> defaultLocaleLensSettings = new ArrayList<>();

        if (lenssettingses != null && lenssettingses.size() > 0) {
            updatedLenssettingses = lenssettingses;
            if (isNotNull(DefaultLocale)) {
                DefaultLocaleList defaultLocaleList = (DefaultLocaleList) getObjectFromJson(DefaultLocale, new DefaultLocaleList());

                List<Locale> localeList = defaultLocaleList.getLocale();

                if (localeList != null && localeList.size() > 0) {
                    for (Locale locale : localeList) {
                        if (locale.getLanguage().equals("*")) {
                            final String defaultLocale = locale.getCountry();
                            Predicate condition = new Predicate() {
                                @Override
                                public boolean evaluate(Object settings) {
                                    return (((CoreLenssettings) settings).getLocale().equals(defaultLocale));
                                }
                            };
                            List<CoreLenssettings> result = (List<CoreLenssettings>) CollectionUtils.select(updatedLenssettingses, condition);
                            if (result != null && result.size() > 0) {
                                CoreLenssettings lenssettings = result.get(0);
                                CoreLenssettings lensSettingsxx = new CoreLenssettings();
                                lensSettingsxx.setCharacterSet(lenssettings.getCharacterSet());
                                lensSettingsxx.setHost(lenssettings.getHost());
                                lensSettingsxx.setPort(lenssettings.getPort());
                                lensSettingsxx.setCountry(lenssettings.getCountry());
                                lensSettingsxx.setLanguage(lenssettings.getLanguage());
                                lensSettingsxx.setTimeout(lenssettings.getTimeout());
                                lensSettingsxx.setLocale("xl_xx"+","+lenssettings.getLocale());
                                lensSettingsxx.setLensVersion(lenssettings.getLensVersion());
                                lensSettingsxx.setInstanceType(lenssettings.getInstanceType());
                                lensSettingsxx.setStatus(lenssettings.isStatus());
                                lensSettingsxx.setHrxmlcontent(lenssettings.getHrxmlcontent());
                                lensSettingsxx.setCoreClientlenssettingses(lenssettings.getCoreClientlenssettingses());

                                defaultLocaleLensSettings.add(lensSettingsxx);

                                CoreLenssettings lensSettingsStar = new CoreLenssettings();
                                lensSettingsStar.setCharacterSet(lenssettings.getCharacterSet());
                                lensSettingsStar.setHost(lenssettings.getHost());
                                lensSettingsStar.setPort(lenssettings.getPort());
                                lensSettingsStar.setCountry(lenssettings.getCountry());
                                lensSettingsStar.setLanguage(lenssettings.getLanguage());
                                lensSettingsStar.setTimeout(lenssettings.getTimeout());
                                lensSettingsStar.setLocale("*_*"+","+lenssettings.getLocale());
                                lensSettingsStar.setLensVersion(lenssettings.getLensVersion());
                                lensSettingsStar.setStatus(lenssettings.isStatus());
                                lensSettingsStar.setInstanceType(lenssettings.getInstanceType());
                                lensSettingsStar.setHrxmlcontent(lenssettings.getHrxmlcontent());
                                lensSettingsStar.setCoreClientlenssettingses(lenssettings.getCoreClientlenssettingses());

                                defaultLocaleLensSettings.add(lensSettingsStar);
                            }
                        } else {
                            final String defaultLocale = locale.getLanguage() + "_" + locale.getCountry();
                            Predicate condition = new Predicate() {
                                @Override
                                public boolean evaluate(Object settings) {
                                    return (((CoreLenssettings) settings).getLocale().equals(defaultLocale));
                                }
                            };
                            List<CoreLenssettings> result = (List<CoreLenssettings>) CollectionUtils.select(updatedLenssettingses, condition);
                            if (result != null && result.size() > 0) {
                                CoreLenssettings lenssettings = result.get(0);
                                CoreLenssettings lensSettingsxx = new CoreLenssettings();
                                lensSettingsxx.setCharacterSet(lenssettings.getCharacterSet());
                                lensSettingsxx.setHost(lenssettings.getHost());
                                lensSettingsxx.setPort(lenssettings.getPort());
                                lensSettingsxx.setCountry(lenssettings.getCountry());
                                lensSettingsxx.setLanguage(lenssettings.getLanguage());
                                lensSettingsxx.setTimeout(lenssettings.getTimeout());
                                lensSettingsxx.setLocale(locale.getLanguage() + "_xx"+","+lenssettings.getLocale());
                                lensSettingsxx.setLensVersion(lenssettings.getLensVersion());
                                lensSettingsxx.setInstanceType(lenssettings.getInstanceType());
                                lensSettingsxx.setStatus(lenssettings.isStatus());
                                lensSettingsxx.setHrxmlcontent(lenssettings.getHrxmlcontent());
                                lensSettingsxx.setCoreClientlenssettingses(lenssettings.getCoreClientlenssettingses());

                                defaultLocaleLensSettings.add(lensSettingsxx);

                                CoreLenssettings lensSettingsStar = new CoreLenssettings();
                                lensSettingsStar.setCharacterSet(lenssettings.getCharacterSet());
                                lensSettingsStar.setHost(lenssettings.getHost());
                                lensSettingsStar.setPort(lenssettings.getPort());
                                lensSettingsStar.setCountry(lenssettings.getCountry());
                                lensSettingsStar.setLanguage(lenssettings.getLanguage());
                                lensSettingsStar.setTimeout(lenssettings.getTimeout());
                                lensSettingsStar.setLocale(locale.getLanguage() + "_*"+","+lenssettings.getLocale());
                                lensSettingsStar.setLensVersion(lenssettings.getLensVersion());
                                lensSettingsStar.setStatus(lenssettings.isStatus());
                                lensSettingsStar.setInstanceType(lenssettings.getInstanceType());
                                lensSettingsStar.setCoreClientlenssettingses(lenssettings.getCoreClientlenssettingses());
                                
                                defaultLocaleLensSettings.add(lensSettingsStar);

                                lensSettingsStar.setHrxmlcontent(lenssettings.getHrxmlcontent());
                            }
                        }
                    }
                }
            }
        }

        updatedLenssettingses.addAll(defaultLocaleLensSettings);
        return updatedLenssettingses;
    }

    /**
     *
     * @param strResult
     * @param defaultLocale
     * @param identifiedLocale
     * @param coreLensSettingsList
     * @return
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws LensException
     * @throws SAXException
     */
    public String updateLocale(String strResult, String defaultLocale, String identifiedLocale, List<CoreLenssettings> coreLensSettingsList) throws ParserConfigurationException, IOException, LensException, SAXException {
        DefaultLocaleList defaultLocaleList = (DefaultLocaleList) getObjectFromJson(defaultLocale, new DefaultLocaleList());
        //String[] localeObj = identifiedLocale.split("_");
        final String locale = identifiedLocale;

        Predicate condition = new Predicate() {
            @Override
            public boolean evaluate(Object settings) {
                return (((CoreLenssettings) settings).getLocale().equalsIgnoreCase(locale));
            }
        };
        List<CoreLenssettings> result = (List<CoreLenssettings>) CollectionUtils.select(coreLensSettingsList, condition);
        if (isNotNull(result) && result.size() > 0) {
            CoreLenssettings lensSettings = result.get(0);
            if (isNotNullOrEmpty(lensSettings.getLocale()) && (lensSettings.getLocale().contains("xx") || lensSettings.getLocale().contains("*"))) {
                String processedLocale = identifiedLocale;
                List<Locale> localeList = defaultLocaleList.getLocale();

                for (Locale loc : localeList) {
                    if (locale.startsWith(loc.getLanguage())) {
                        processedLocale = loc.getLanguage() + "_" + loc.getCountry();
                    }
                }
                if (processedLocale.equals(identifiedLocale)) {
                    for (Locale loc : localeList) {
                        if (loc.getLanguage().equals("*")) {
                            processedLocale = loc.getCountry();
                        }
                    }
                }

                strResult = strResult.replace("<special><locale>" + identifiedLocale + "</locale></special>", "<special><locale>" + processedLocale + "</locale></special>");
            }
        } else {
            String processedLocale = identifiedLocale;
            List<Locale> localeList = defaultLocaleList.getLocale();

            for (Locale loc : localeList) {
                if (locale.startsWith(loc.getLanguage())) {
                    processedLocale = loc.getLanguage() + "_" + loc.getCountry();
                }
            }
            final String processedLocaleInfo = processedLocale;
            Predicate conditionLocale = new Predicate() {
                @Override
                public boolean evaluate(Object settings) {
                    return (((CoreLenssettings) settings).getLocale().equalsIgnoreCase(processedLocaleInfo));
                }
            };
            List<CoreLenssettings> results = (List<CoreLenssettings>) CollectionUtils.select(coreLensSettingsList, conditionLocale);
            if (isNotNull(results) && results.size() > 0) {
            } else {
                for (Locale loc : localeList) {
                    if (loc.getLanguage().equals("*")) {
                        processedLocale = loc.getCountry();
                    }
                }
            }

            if (processedLocale.equals(identifiedLocale)) {
                for (Locale loc : localeList) {
                    if (loc.getLanguage().equals("*")) {
                        processedLocale = loc.getCountry();
                    }
                }
            }
            strResult = strResult.replace("<special><locale>" + identifiedLocale + "</locale></special>", "<special><locale>" + processedLocale + "</locale></special>");
        }
        return strResult;
    }

    /**
     *
     * @param lensResult
     * @param locale
     * @return
     * @throws JAXBException
     */
    public Object getLENSResponseObject(String lensResult, String locale) throws JAXBException {
        ApiContext apiContext = getApiContext();
        Object object = null;
        try {
            String instanceType = getInstanceTypeFromRequest(apiContext.getInstanceType());
            String resource = apiContext.getResource();

            if (apiContext.getAccept() == null || apiContext.getAccept().equalsIgnoreCase(Headers.ApplicationXML.toString())) {
                object = lensResult;
            } else {
                JAXBContext jaxbContext = null;

                // If Instance type is XRAY
                if ((ParsingInstanceType.XRAY.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))
                        || (ParsingInstanceType.XRAYJOBS.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))
                        || (ParsingInstanceType.LENSSEARCH.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))) {
                    switch (locale) {
                        case "cn_cn":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.cn_cn.ResDoc.class);
                            break;
                        case "de_at":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.de_at.ResDoc.class);
                            break;
                        case "de_ch":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.de_ch.ResDoc.class);
                            break;
                        case "de_de":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.de_de.ResDoc.class);
                            break;
                        case "du_be":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.du_be.ResDoc.class);
                            break;
                        case "du_nl":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.du_nl.ResDoc.class);
                            break;
                        case "en_ae":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_ae.ResDoc.class);
                            break;
                        case "en_ar":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_ar.ResDoc.class);
                            break;
                        case "en_at":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_at.ResDoc.class);
                            break;
                        case "en_au":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_au.ResDoc.class);
                            break;
                        case "en_be":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_be.ResDoc.class);
                            break;
                        case "en_br":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_br.ResDoc.class);
                            break;
                        case "en_ca":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_ca.ResDoc.class);
                            break;
                        case "en_ch":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_ch.ResDoc.class);
                            break;
                        case "en_cn":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_cn.ResDoc.class);
                            break;
                        case "en_de":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_de.ResDoc.class);
                            break;
                        case "en_es":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_es.ResDoc.class);
                            break;
                        case "en_fr":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_fr.ResDoc.class);
                            break;
                        case "en_gb":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_gb.ResDoc.class);
                            break;
                        case "en_hk":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_hk.ResDoc.class);
                            break;
                        case "en_ie":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_ie.ResDoc.class);
                            break;
                        case "en_in":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_in.ResDoc.class);
                            break;
                        case "en_it":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_it.ResDoc.class);
                            break;
                        case "en_jp":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_jp.ResDoc.class);
                            break;
                        case "en_mx":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_mx.ResDoc.class);
                            break;
                        case "en_nl":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_nl.ResDoc.class);
                            break;
                        case "en_nz":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_nz.ResDoc.class);
                            break;
                        case "en_pl":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_pl.ResDoc.class);
                            break;
                        case "en_pt":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_pt.ResDoc.class);
                            break;
                        case "en_se":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_se.ResDoc.class);
                            break;
                        case "en_sg":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_sg.ResDoc.class);
                            break;
                        case "en_tr":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_tr.ResDoc.class);
                            break;
                        case "en_tw":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_tw.ResDoc.class);
                            break;
                        case "en_us":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_us.ResDoc.class);
                            break;
                        case "en_za":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_za.ResDoc.class);
                            break;
                        case "es_ar":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.es_ar.ResDoc.class);
                            break;
                        case "es_cl":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.es_cl.ResDoc.class);
                            break;
                        case "es_es":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.es_es.ResDoc.class);
                            break;
                        case "es_mx":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.es_mx.ResDoc.class);
                            break;
                        case "es_us":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.es_us.ResDoc.class);
                            break;
                        case "fr_be":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.fr_be.ResDoc.class);
                            break;
                        case "fr_ca":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.fr_ca.ResDoc.class);
                            break;
                        case "fr_ch":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.fr_ch.ResDoc.class);
                            break;
                        case "fr_dz":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.fr_dz.ResDoc.class);
                            break;
                        case "fr_fr":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.fr_fr.ResDoc.class);
                            break;
                        case "fr_ma":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.fr_ma.ResDoc.class);
                            break;
                        case "fr_tn":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.fr_tn.ResDoc.class);
                            break;
                        case "it_it":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.it_it.ResDoc.class);
                            break;
                        case "iw_il":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.iw_il.ResDoc.class);
                            break;
                        case "ja_jp":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.ja_jp.ResDoc.class);
                            break;
                        case "pl_pl":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.pl_pl.ResDoc.class);
                            break;
                        case "pt_br":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.pt_br.ResDoc.class);
                            break;
                        case "pt_pt":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.pt_pt.ResDoc.class);
                            break;
                        case "ru_ru":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.ru_ru.ResDoc.class);
                            break;
                        case "sv_se":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.sv_se.ResDoc.class);
                            break;
                        case "tr_tr":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.tr_tr.ResDoc.class);
                            break;
                        case "zh_tw":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.zh_tw.ResDoc.class);
                            break;

                        default:
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.en_us.ResDoc.class);
                            break;
                    }
                } else if (ParsingInstanceType.XRAYEDU.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName)) {

                    jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.xray.edu.en_us.ResDoc.class);

                } else if (ParsingInstanceType.XRAY.toString().equals(instanceType.toLowerCase()) || ParsingInstanceType.XRAYJOBS.toString().equals(instanceType.toLowerCase()) || ParsingInstanceType.LENSSEARCH.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(jobResourceName)) {
                    switch (locale) {
                        case "cn_cn":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.cn_cn.JobDoc.class);
                            break;
                        case "de_de":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.de_de.JobDoc.class);
                            break;
                        case "du_be":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.du_be.JobDoc.class);
                            break;
                        case "du_nl":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.du_nl.JobDoc.class);
                            break;
                        case "en_au":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.en_au.JobDoc.class);
                            break;
                        case "en_ca":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.en_ca.JobDoc.class);
                            break;
                        case "en_gb":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.en_gb.JobDoc.class);
                            break;
                        case "en_in":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.en_in.JobDoc.class);
                            break;
                        case "en_nl":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.en_nl.JobDoc.class);
                            break;
                        case "en_us":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.en_us.JobDoc.class);
                            break;
                        case "en_za":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.en_za.JobDoc.class);
                            break;
                        case "es_ar":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.es_ar.JobDoc.class);
                            break;
                        case "es_cl":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.es_cl.JobDoc.class);
                            break;
                        case "es_es":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.es_es.JobDoc.class);
                            break;
                        case "es_mx":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.es_mx.JobDoc.class);
                            break;
                        case "es_us":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.es_us.JobDoc.class);
                            break;
                        case "fr_be":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.fr_be.JobDoc.class);
                            break;
                        case "fr_ca":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.fr_ca.JobDoc.class);
                            break;
                        case "fr_fr":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.fr_fr.JobDoc.class);
                            break;
                        case "it_it":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.it_it.JobDoc.class);
                            break;
                        case "ja_jp":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.ja_jp.JobDoc.class);
                            break;
                        case "zh_tw":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.zh_tw.JobDoc.class);
                            break;

                        default:
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.lens.en_us.JobDoc.class);
                            break;

                    }
                } else if ((ParsingInstanceType.TM.toString().equals(instanceType.toLowerCase()) || ParsingInstanceType.SPECTRUM.toString().equals(instanceType.toLowerCase()))
                        && resource.equalsIgnoreCase(resumeResourceName)) {
                    switch (locale) {
                        case "en_au":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.tm.en_au.ResDoc.class);
                            break;
                        case "en_ca":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.tm.en_ca.ResDoc.class);
                            break;
                        case "en_gb":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.tm.en_gb.ResDoc.class);
                            break;
                        case "en_nz":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.tm.en_nz.ResDoc.class);
                            break;
                        case "en_us":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.tm.en_us.ResDoc.class);
                            break;

                        default:
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.tm.en_us.ResDoc.class);
                            break;
                    }
                } else if ((ParsingInstanceType.JM.toString().equals(instanceType.toLowerCase()))
                        && resource.equalsIgnoreCase(jobResourceName)) {
                    switch (locale) {
                        case "en_au":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.jm.en_au.JobDoc.class);
                            break;
                        case "en_gb":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.jm.en_gb.JobDoc.class);
                            break;
                        case "en_nz":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.jm.en_nz.JobDoc.class);
                            break;
                        case "en_sg":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.jm.en_sg.JobDoc.class);
                            break;
                        case "en_us":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.jm.en_us.JobDoc.class);
                            break;

                        default:
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.jm.en_us.JobDoc.class);
                            break;
                    }
                } else if ((ParsingInstanceType.SPECTRUM.toString().equals(instanceType.toLowerCase()) || (ParsingInstanceType.TM.toString().equals(instanceType.toLowerCase())))
                        && resource.equalsIgnoreCase(jobResourceName)) {
                    switch (locale) {
                        case "en_au":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.spectrum.en_au.JobDoc.class);
                            break;
                        case "en_gb":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.spectrum.en_gb.JobDoc.class);
                            break;
                        case "en_nz":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.spectrum.en_nz.JobDoc.class);
                            break;
                        case "en_us":
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.spectrum.en_us.JobDoc.class);
                            break;

                        default:
                            jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.job.spectrum.en_us.JobDoc.class);
                            break;
                    }
                }

                // Remove empty nodes
                DocumentBuilder dBuilder = null;
                Document doc = null;
                dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(lensResult.getBytes("UTF-8")));
                doc = dBuilder.parse(new ByteArrayInputStream(lensResult.getBytes(xmlStreamReader.getCharacterEncodingScheme())));

                String header = "";
                if (lensResult.contains("<?xml version='1.0' encoding='iso-8859-1'?>")) {
                    header = "<?xml version='1.0' encoding='iso-8859-1'?>";
                } else if (lensResult.contains("<?xml version='1.0' encoding='utf-8'?>")) {
                    header = "<?xml version='1.0' encoding='utf-8'?>";
                } else {
                    header = "<?xml version='1.0' encoding='utf-8'?>";
                }

                removeEmptyNodes(doc);
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer trans = tf.newTransformer();
                trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                StreamResult result = new StreamResult(new OutputStreamWriter(bos, xmlStreamReader.getCharacterEncodingScheme()));
                trans.transform(new DOMSource(doc), result);
                lensResult = header + bos.toString("UTF-8");

//                XMLInputFactory xif = XMLInputFactory.newFactory();
//                XMLStreamReader xsr = xif.createXMLStreamReader(new StringReader(lensResult));
//                xsr = xif.createFilteredReader(xsr, new StreamFilter() {
//
//                    @Override
//                    public boolean accept(XMLStreamReader reader) {
//                        if(reader.getEventType() == XMLStreamReader.CHARACTERS) {
//                            return reader.getText().trim().length() > 0;
//                        } 
//                        return true;
//                    }
//
//                });
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                object = jaxbUnmarshaller.unmarshal(new StringReader(lensResult));
                
                XPathFactory factory = XPathFactory.newInstance();
                XPath xPath = factory.newXPath();
                InputSource is = new InputSource(new StringReader(lensResult));
                
                if ((ParsingInstanceType.XRAY.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))
                        || (ParsingInstanceType.XRAYJOBS.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))
                        || (ParsingInstanceType.LENSSEARCH.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))) {
                    //check if custom rollup section exists
                    // Custom rollup section
                    NodeList customRollupSection = null;
                    if(resource.equalsIgnoreCase(resumeResourceName)){
                        customRollupSection  = (NodeList) xPath.evaluate("/ResDoc/customrollup", is, XPathConstants.NODESET);
                    }
                    else if(resource.equalsIgnoreCase(jobResourceName)){
                        customRollupSection  = (NodeList) xPath.evaluate("/JobDoc/customrollup", is, XPathConstants.NODESET);
                    }
                    if(customRollupSection != null && customRollupSection.getLength() > 0){
                        for (int i = 0; i < customRollupSection.getLength(); ++i) {
                            Node nNode = customRollupSection.item(i);
                            if(nNode.getAttributes() != null && nNode.getAttributes().getLength() > 0){
                                Map attrMap = new HashMap<>();
                                for(int j = 0; j < nNode.getAttributes().getLength(); ++j){
                                    attrMap.put("@" + nNode.getAttributes().item(j).getLocalName(),
                                        nNode.getAttributes().item(j).getNodeValue());
                                }
                                NodeList nNodeList = nNode.getChildNodes();
                                String customRollup = new CoreMappers().getOuterXml(nNodeList);
                                if (isNotNullOrEmpty(customRollup)) {
                                    Object customRollupObj = changeResponseDataObject(customRollup);
                                    if(customRollupObj instanceof JSONObject){
                                        JSONObject jsonCustomRollObj = new JSONObject();
                                        jsonCustomRollObj.putAll(attrMap);
                                        jsonCustomRollObj.putAll(((JSONObject)customRollupObj));
                                        object = updateCustomRollupLENSObject(object, jsonCustomRollObj, instanceType, locale, resource);
    //                                    ((JSONObject)customRollupObj).putAll(attrMap);
    //                                    object = ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).customrollup.add(jsonCustomRollObj);
                                    }
                                }
                                else{
                                    JSONObject jsonCustomRollObj = new JSONObject();
                                    jsonCustomRollObj.putAll(attrMap);
                                    object = updateCustomRollupLENSObject(object, jsonCustomRollObj, instanceType, locale, resource);
                                }
                            }
                            else{
                                NodeList nNodeList = nNode.getChildNodes();
                                String customRollup = new CoreMappers().getOuterXml(nNodeList);
                                if (isNotNullOrEmpty(customRollup)) {
                                    Object customRollupObj = changeResponseDataObject(customRollup);
                                    if(customRollupObj instanceof JSONObject){
                                        object = updateCustomRollupLENSObject(object, customRollupObj, instanceType, locale, resource);
    //                                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).customrollup.add(customRollupObj);
                                    }
                                }
                            }
                        }
                    }
                }
                
                // Special section
                NodeList specialSection = null;
                is = new InputSource(new StringReader(lensResult));
                if(resource.equalsIgnoreCase(resumeResourceName)){
                    specialSection  = (NodeList) xPath.evaluate("/ResDoc/special", is, XPathConstants.NODESET);
                }
                else if(resource.equalsIgnoreCase(jobResourceName)){
                    specialSection  = (NodeList) xPath.evaluate("/JobDoc/special", is, XPathConstants.NODESET);
                }
                if(specialSection != null && specialSection.getLength() > 0){
                    for (int i = 0; i < specialSection.getLength(); ++i) {
                        Node nNode = specialSection.item(i);
                        if(nNode.getAttributes() != null && nNode.getAttributes().getLength() > 0){
                            Map attrMap = new HashMap<>();
                            for(int j = 0; j < nNode.getAttributes().getLength(); ++j){
                                attrMap.put("@" + nNode.getAttributes().item(j).getLocalName(),
                                    nNode.getAttributes().item(j).getNodeValue());
                            }
                            NodeList nNodeList = nNode.getChildNodes();
                            String special = new CoreMappers().getOuterXml(nNodeList);
                            if (isNotNullOrEmpty(special)) {
                                Object specialObj = changeResponseDataObject(special);
                                if(specialObj instanceof JSONObject){
                                    JSONObject jsonSpecialObj = new JSONObject();
                                    jsonSpecialObj.putAll(attrMap);
                                    jsonSpecialObj.putAll(((JSONObject)specialObj));
                                    object = updateSpecialLENSObject(object, jsonSpecialObj, instanceType, locale, resource);
//                                    ((JSONObject)customRollupObj).putAll(attrMap);
//                                    object = ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).customrollup.add(jsonCustomRollObj);
                                }
                            }
                            else{
                                JSONObject jsonSpecialObj = new JSONObject();
                                jsonSpecialObj.putAll(attrMap);
                                object = updateSpecialLENSObject(object, jsonSpecialObj, instanceType, locale, resource);
                            }
                        }
                        else{
                            NodeList nNodeList = nNode.getChildNodes();
                            String special = new CoreMappers().getOuterXml(nNodeList);
                            if (isNotNullOrEmpty(special)) {
                                Object specialObj = changeResponseDataObject(special);
                                if(specialObj instanceof JSONObject){
                                    object = updateSpecialLENSObject(object, specialObj, instanceType, locale, resource);
//                                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).customrollup.add(customRollupObj);
                                }
                            }
                        }
                    }
                }
                
//                StringWriter sw = new StringWriter();
//                jaxbContext = JAXBContext.newInstance(com.bgt.lens.model.resume.tm.en_us.ResDoc.class);
//                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//                jaxbMarshaller.marshal(object, sw);
//                String xmlString = sw.toString();
//                return xmlString;
//                
//                JSON objJson = new XMLSerializer().read(lensResult);
//                object = objJson;
            }
        } catch (Exception ex) {
            object = lensResult;
            LOGGER.error("Unable to get LENS response object for the locale and instance. Please check the configuration.");
        }
        return object;

    }
    
    /**
     * Update LENS object
     * @param object
     * @param customRollupObj
     * @param instanceType
     * @param locale
     * @param resource
     * @return 
     */
    public Object updateCustomRollupLENSObject(Object object, Object customRollupObj, String instanceType, String locale, String resource){
        // If Instance type is XRAY
        if ((ParsingInstanceType.XRAY.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))
                || (ParsingInstanceType.XRAYJOBS.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))
                || (ParsingInstanceType.LENSSEARCH.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))) {
            switch (locale) {
                case "cn_cn":
                    ((com.bgt.lens.model.resume.xray.cn_cn.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.cn_cn.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "de_at":
                    ((com.bgt.lens.model.resume.xray.de_at.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.de_at.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "de_ch":
                    ((com.bgt.lens.model.resume.xray.de_ch.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.de_ch.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "de_de":
                    ((com.bgt.lens.model.resume.xray.de_de.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.de_de.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "du_be":
                    ((com.bgt.lens.model.resume.xray.du_be.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.du_be.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "du_nl":
                    ((com.bgt.lens.model.resume.xray.du_nl.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.du_nl.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_ae":
                    ((com.bgt.lens.model.resume.xray.en_ae.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ae.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_ar":
                    ((com.bgt.lens.model.resume.xray.en_ar.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ar.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_at":
                    ((com.bgt.lens.model.resume.xray.en_at.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_at.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_au":
                    ((com.bgt.lens.model.resume.xray.en_au.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_au.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_be":
                    ((com.bgt.lens.model.resume.xray.en_be.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_be.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_br":
                    ((com.bgt.lens.model.resume.xray.en_br.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_br.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_ca":
                    ((com.bgt.lens.model.resume.xray.en_ca.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ca.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_ch":
                    ((com.bgt.lens.model.resume.xray.en_ch.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ch.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_cn":
                    ((com.bgt.lens.model.resume.xray.en_cn.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_cn.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_de":
                    ((com.bgt.lens.model.resume.xray.en_de.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_de.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_es":
                    ((com.bgt.lens.model.resume.xray.en_es.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_es.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_fr":
                    ((com.bgt.lens.model.resume.xray.en_fr.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_fr.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.resume.xray.en_gb.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_gb.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_hk":
                    ((com.bgt.lens.model.resume.xray.en_hk.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_hk.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_ie":
                    ((com.bgt.lens.model.resume.xray.en_ie.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ie.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_in":
                    ((com.bgt.lens.model.resume.xray.en_in.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_in.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_it":
                    ((com.bgt.lens.model.resume.xray.en_it.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_it.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_jp":
                    ((com.bgt.lens.model.resume.xray.en_jp.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_jp.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_mx":
                    ((com.bgt.lens.model.resume.xray.en_mx.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_mx.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_nl":
                    ((com.bgt.lens.model.resume.xray.en_nl.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_nl.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_nz":
                    ((com.bgt.lens.model.resume.xray.en_nz.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_nz.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_pl":
                    ((com.bgt.lens.model.resume.xray.en_pl.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_pl.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_pt":
                    ((com.bgt.lens.model.resume.xray.en_pt.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_pt.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_se":
                    ((com.bgt.lens.model.resume.xray.en_se.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_se.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_sg":
                    ((com.bgt.lens.model.resume.xray.en_sg.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_sg.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_tr":
                    ((com.bgt.lens.model.resume.xray.en_tr.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_tr.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_tw":
                    ((com.bgt.lens.model.resume.xray.en_tw.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_tw.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.resume.xray.en_us.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_us.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_za":
                    ((com.bgt.lens.model.resume.xray.en_za.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_za.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "es_ar":
                    ((com.bgt.lens.model.resume.xray.es_ar.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_ar.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "es_cl":
                    ((com.bgt.lens.model.resume.xray.es_cl.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_cl.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "es_es":
                    ((com.bgt.lens.model.resume.xray.es_es.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_es.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "es_mx":
                    ((com.bgt.lens.model.resume.xray.es_mx.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_mx.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "es_us":
                    ((com.bgt.lens.model.resume.xray.es_us.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_us.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_be":
                    ((com.bgt.lens.model.resume.xray.fr_be.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_be.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_ca":
                    ((com.bgt.lens.model.resume.xray.fr_ca.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_ca.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_ch":
                    ((com.bgt.lens.model.resume.xray.fr_ch.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_ch.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_dz":
                    ((com.bgt.lens.model.resume.xray.fr_dz.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_dz.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_fr":
                    ((com.bgt.lens.model.resume.xray.fr_fr.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_fr.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_ma":
                    ((com.bgt.lens.model.resume.xray.fr_ma.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_ma.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_tn":
                    ((com.bgt.lens.model.resume.xray.fr_tn.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_tn.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "it_it":
                    ((com.bgt.lens.model.resume.xray.it_it.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.it_it.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "iw_il":
                    ((com.bgt.lens.model.resume.xray.iw_il.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.iw_il.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "ja_jp":
                    ((com.bgt.lens.model.resume.xray.ja_jp.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.ja_jp.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "pl_pl":
                    ((com.bgt.lens.model.resume.xray.pl_pl.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.pl_pl.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "pt_br":
                    ((com.bgt.lens.model.resume.xray.pt_br.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.pt_br.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "pt_pt":
                    ((com.bgt.lens.model.resume.xray.pt_pt.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.pt_pt.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "ru_ru":
                    ((com.bgt.lens.model.resume.xray.ru_ru.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.ru_ru.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "sv_se":
                    ((com.bgt.lens.model.resume.xray.sv_se.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.sv_se.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "tr_tr":
                    ((com.bgt.lens.model.resume.xray.tr_tr.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.tr_tr.ResDoc)object).customrollup.add(customRollupObj);
                    break;
                case "zh_tw":
                    ((com.bgt.lens.model.resume.xray.zh_tw.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.zh_tw.ResDoc)object).customrollup.add(customRollupObj);
                    break;

                default:
                    ((com.bgt.lens.model.resume.xray.en_us.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_us.ResDoc)object).customrollup.add(customRollupObj);
                    break;
            }
        } else if (ParsingInstanceType.XRAYEDU.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName)) {
            ((com.bgt.lens.model.resume.xray.edu.en_us.ResDoc)object).customrollup = new ArrayList<>();
            ((com.bgt.lens.model.resume.xray.edu.en_us.ResDoc)object).customrollup.add(customRollupObj);

        } else if (ParsingInstanceType.XRAY.toString().equals(instanceType.toLowerCase()) || ParsingInstanceType.XRAYJOBS.toString().equals(instanceType.toLowerCase()) || ParsingInstanceType.LENSSEARCH.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(jobResourceName)) {
            switch (locale) {
                case "cn_cn":
                  ((com.bgt.lens.model.job.lens.cn_cn.JobDoc)object).customrollup = new ArrayList<>();
                   ((com.bgt.lens.model.job.lens.cn_cn.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "de_de":
                    ((com.bgt.lens.model.job.lens.de_de.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.de_de.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "du_be":
                    ((com.bgt.lens.model.job.lens.du_be.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.du_be.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "du_nl":
                    ((com.bgt.lens.model.job.lens.du_nl.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.du_nl.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_au":
                    ((com.bgt.lens.model.job.lens.en_au.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_au.JobDoc)object).customrollup.add(customRollupObj); 
                    break;
                case "en_ca":
                    ((com.bgt.lens.model.job.lens.en_ca.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_ca.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.job.lens.en_gb.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_gb.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_in":
                    ((com.bgt.lens.model.job.lens.en_in.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_in.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_nl":
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).customrollup.add((com.bgt.lens.model.job.spectrum.en_us.Customrollup)customRollupObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.job.lens.en_us.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_us.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "en_za":
                    ((com.bgt.lens.model.job.lens.en_za.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_za.JobDoc)object).customrollup.add(customRollupObj); 
                    break;
                case "es_ar":
                    ((com.bgt.lens.model.job.lens.es_ar.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_ar.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "es_cl":
                    ((com.bgt.lens.model.job.lens.es_cl.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_cl.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "es_es":
                    ((com.bgt.lens.model.job.lens.es_es.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_es.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "es_mx":
                    ((com.bgt.lens.model.job.lens.es_mx.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_mx.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "es_us":
                    ((com.bgt.lens.model.job.lens.es_us.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_us.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_be":
                    ((com.bgt.lens.model.job.lens.fr_be.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.fr_be.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_ca":
                    ((com.bgt.lens.model.job.lens.fr_ca.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.fr_ca.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "fr_fr":
                    ((com.bgt.lens.model.job.lens.fr_fr.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.fr_fr.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "it_it":
                    ((com.bgt.lens.model.job.lens.it_it.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.it_it.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "ja_jp":
                    ((com.bgt.lens.model.job.lens.ja_jp.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.ja_jp.JobDoc)object).customrollup.add(customRollupObj);
                    break;
                case "zh_tw":
                    ((com.bgt.lens.model.job.lens.zh_tw.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.zh_tw.JobDoc)object).customrollup.add(customRollupObj);
                    break;

                default:
                   ((com.bgt.lens.model.job.lens.en_us.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_us.JobDoc)object).customrollup.add(customRollupObj);
                    break;
            }
        } else if ((ParsingInstanceType.TM.toString().equals(instanceType.toLowerCase()) || ParsingInstanceType.SPECTRUM.toString().equals(instanceType.toLowerCase()))
                && resource.equalsIgnoreCase(resumeResourceName)) {
            switch (locale) {
                case "en_au":
                    ((com.bgt.lens.model.resume.tm.en_au.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_au.ResDoc)object).customrollup.add((com.bgt.lens.model.resume.tm.en_au.Customrollup)customRollupObj);
                    break;
                case "en_ca":
                    ((com.bgt.lens.model.resume.tm.en_ca.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_ca.ResDoc)object).customrollup.add((com.bgt.lens.model.resume.tm.en_ca.Customrollup)customRollupObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.resume.tm.en_gb.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_gb.ResDoc)object).customrollup.add((com.bgt.lens.model.resume.tm.en_gb.Customrollup)customRollupObj);
                    break;
                case "en_nz":
                    ((com.bgt.lens.model.resume.tm.en_nz.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_nz.ResDoc)object).customrollup.add((com.bgt.lens.model.resume.tm.en_nz.Customrollup)customRollupObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).customrollup.add((com.bgt.lens.model.resume.tm.en_us.Customrollup) customRollupObj);
                    break;

                default:
                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).customrollup.add((com.bgt.lens.model.resume.tm.en_us.Customrollup) customRollupObj);
                    break;
            }
        } 
        else if ((ParsingInstanceType.JM.toString().equals(instanceType.toLowerCase()))
            && resource.equalsIgnoreCase(jobResourceName)) {
            switch (locale) {
                case "en_au":
                    ((com.bgt.lens.model.job.jm.en_au.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_au.JobDoc)object).customrollup.add((com.bgt.lens.model.job.jm.en_au.Customrollup)customRollupObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.job.jm.en_gb.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_gb.JobDoc)object).customrollup.add((com.bgt.lens.model.job.jm.en_gb.Customrollup)customRollupObj);
                    break;
                case "en_nz":
                    ((com.bgt.lens.model.job.jm.en_nz.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_nz.JobDoc)object).customrollup.add((com.bgt.lens.model.job.jm.en_nz.Customrollup)customRollupObj);
                    break;
                case "en_sg":
                    ((com.bgt.lens.model.job.jm.en_sg.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_sg.JobDoc)object).customrollup.add((com.bgt.lens.model.job.jm.en_sg.Customrollup)customRollupObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.job.jm.en_us.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_us.JobDoc)object).customrollup.add((com.bgt.lens.model.job.jm.en_us.Customrollup)customRollupObj);
                    break;

                default:
                    ((com.bgt.lens.model.job.jm.en_us.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_us.JobDoc)object).customrollup.add((com.bgt.lens.model.job.jm.en_us.Customrollup)customRollupObj);
                    break;
            }
        } else if ((ParsingInstanceType.SPECTRUM.toString().equals(instanceType.toLowerCase()) || (ParsingInstanceType.TM.toString().equals(instanceType.toLowerCase())))
                && resource.equalsIgnoreCase(jobResourceName)) {
            switch (locale) {
                case "en_au":
                    ((com.bgt.lens.model.job.spectrum.en_au.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_au.JobDoc)object).customrollup.add((com.bgt.lens.model.job.spectrum.en_au.Customrollup)customRollupObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.job.spectrum.en_gb.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_gb.JobDoc)object).customrollup.add((com.bgt.lens.model.job.spectrum.en_gb.Customrollup)customRollupObj);
                    break;
                case "en_nz":
                    ((com.bgt.lens.model.job.spectrum.en_nz.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_nz.JobDoc)object).customrollup.add((com.bgt.lens.model.job.spectrum.en_nz.Customrollup)customRollupObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).customrollup.add((com.bgt.lens.model.job.spectrum.en_us.Customrollup)customRollupObj);
                    break;

                default:
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).customrollup = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).customrollup.add((com.bgt.lens.model.job.spectrum.en_us.Customrollup)customRollupObj);
                    break;
            }
        }
        return object;
    }
    
    /**
     * Update LENS object
     * @param object
     * @param specialObj
     * @param instanceType
     * @param locale
     * @param resource
     * @return 
     */
    public Object updateSpecialLENSObject(Object object, Object specialObj, String instanceType, String locale, String resource){
        // If Instance type is XRAY
        if ((ParsingInstanceType.XRAY.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))
                || (ParsingInstanceType.XRAYJOBS.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))
                || (ParsingInstanceType.LENSSEARCH.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName))) {
            switch (locale) {
                case "cn_cn":
                    ((com.bgt.lens.model.resume.xray.cn_cn.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.cn_cn.ResDoc)object).special.add(specialObj);
                    break;
                case "de_at":
                    ((com.bgt.lens.model.resume.xray.de_at.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.de_at.ResDoc)object).special.add(specialObj);
                    break;
                case "de_ch":
                    ((com.bgt.lens.model.resume.xray.de_ch.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.de_ch.ResDoc)object).special.add(specialObj);
                    break;
                case "de_de":
                    ((com.bgt.lens.model.resume.xray.de_de.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.de_de.ResDoc)object).special.add(specialObj);
                    break;
                case "du_be":
                    ((com.bgt.lens.model.resume.xray.du_be.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.du_be.ResDoc)object).special.add(specialObj);
                    break;
                case "du_nl":
                    ((com.bgt.lens.model.resume.xray.du_nl.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.du_nl.ResDoc)object).special.add(specialObj);
                    break;
                case "en_ae":
                    ((com.bgt.lens.model.resume.xray.en_ae.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ae.ResDoc)object).special.add(specialObj);
                    break;
                case "en_ar":
                    ((com.bgt.lens.model.resume.xray.en_ar.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ar.ResDoc)object).special.add(specialObj);
                    break;
                case "en_at":
                    ((com.bgt.lens.model.resume.xray.en_at.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_at.ResDoc)object).special.add(specialObj);
                    break;
                case "en_au":
                    ((com.bgt.lens.model.resume.xray.en_au.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_au.ResDoc)object).special.add(specialObj);
                    break;
                case "en_be":
                    ((com.bgt.lens.model.resume.xray.en_be.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_be.ResDoc)object).special.add(specialObj);
                    break;
                case "en_br":
                    ((com.bgt.lens.model.resume.xray.en_br.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_br.ResDoc)object).special.add(specialObj);
                    break;
                case "en_ca":
                    ((com.bgt.lens.model.resume.xray.en_ca.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ca.ResDoc)object).special.add(specialObj);
                    break;
                case "en_ch":
                    ((com.bgt.lens.model.resume.xray.en_ch.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ch.ResDoc)object).special.add(specialObj);
                    break;
                case "en_cn":
                    ((com.bgt.lens.model.resume.xray.en_cn.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_cn.ResDoc)object).special.add(specialObj);
                    break;
                case "en_de":
                    ((com.bgt.lens.model.resume.xray.en_de.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_de.ResDoc)object).special.add(specialObj);
                    break;
                case "en_es":
                    ((com.bgt.lens.model.resume.xray.en_es.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_es.ResDoc)object).special.add(specialObj);
                    break;
                case "en_fr":
                    ((com.bgt.lens.model.resume.xray.en_fr.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_fr.ResDoc)object).special.add(specialObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.resume.xray.en_gb.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_gb.ResDoc)object).special.add(specialObj);
                    break;
                case "en_hk":
                    ((com.bgt.lens.model.resume.xray.en_hk.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_hk.ResDoc)object).special.add(specialObj);
                    break;
                case "en_ie":
                    ((com.bgt.lens.model.resume.xray.en_ie.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_ie.ResDoc)object).special.add(specialObj);
                    break;
                case "en_in":
                    ((com.bgt.lens.model.resume.xray.en_in.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_in.ResDoc)object).special.add(specialObj);
                    break;
                case "en_it":
                    ((com.bgt.lens.model.resume.xray.en_it.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_it.ResDoc)object).special.add(specialObj);
                    break;
                case "en_jp":
                    ((com.bgt.lens.model.resume.xray.en_jp.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_jp.ResDoc)object).special.add(specialObj);
                    break;
                case "en_mx":
                    ((com.bgt.lens.model.resume.xray.en_mx.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_mx.ResDoc)object).special.add(specialObj);
                    break;
                case "en_nl":
                    ((com.bgt.lens.model.resume.xray.en_nl.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_nl.ResDoc)object).special.add(specialObj);
                    break;
                case "en_nz":
                    ((com.bgt.lens.model.resume.xray.en_nz.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_nz.ResDoc)object).special.add(specialObj);
                    break;
                case "en_pl":
                    ((com.bgt.lens.model.resume.xray.en_pl.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_pl.ResDoc)object).special.add(specialObj);
                    break;
                case "en_pt":
                    ((com.bgt.lens.model.resume.xray.en_pt.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_pt.ResDoc)object).special.add(specialObj);
                    break;
                case "en_se":
                    ((com.bgt.lens.model.resume.xray.en_se.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_se.ResDoc)object).special.add(specialObj);
                    break;
                case "en_sg":
                    ((com.bgt.lens.model.resume.xray.en_sg.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_sg.ResDoc)object).special.add(specialObj);
                    break;
                case "en_tr":
                    ((com.bgt.lens.model.resume.xray.en_tr.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_tr.ResDoc)object).special.add(specialObj);
                    break;
                case "en_tw":
                    ((com.bgt.lens.model.resume.xray.en_tw.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_tw.ResDoc)object).special.add(specialObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.resume.xray.en_us.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_us.ResDoc)object).special.add(specialObj);
                    break;
                case "en_za":
                    ((com.bgt.lens.model.resume.xray.en_za.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_za.ResDoc)object).special.add(specialObj);
                    break;
                case "es_ar":
                    ((com.bgt.lens.model.resume.xray.es_ar.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_ar.ResDoc)object).special.add(specialObj);
                    break;
                case "es_cl":
                    ((com.bgt.lens.model.resume.xray.es_cl.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_cl.ResDoc)object).special.add(specialObj);
                    break;
                case "es_es":
                    ((com.bgt.lens.model.resume.xray.es_es.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_es.ResDoc)object).special.add(specialObj);
                    break;
                case "es_mx":
                    ((com.bgt.lens.model.resume.xray.es_mx.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_mx.ResDoc)object).special.add(specialObj);
                    break;
                case "es_us":
                    ((com.bgt.lens.model.resume.xray.es_us.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.es_us.ResDoc)object).special.add(specialObj);
                    break;
                case "fr_be":
                    ((com.bgt.lens.model.resume.xray.fr_be.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_be.ResDoc)object).special.add(specialObj);
                    break;
                case "fr_ca":
                    ((com.bgt.lens.model.resume.xray.fr_ca.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_ca.ResDoc)object).special.add(specialObj);
                    break;
                case "fr_ch":
                    ((com.bgt.lens.model.resume.xray.fr_ch.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_ch.ResDoc)object).special.add(specialObj);
                    break;
                case "fr_dz":
                    ((com.bgt.lens.model.resume.xray.fr_dz.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_dz.ResDoc)object).special.add(specialObj);
                    break;
                case "fr_fr":
                    ((com.bgt.lens.model.resume.xray.fr_fr.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_fr.ResDoc)object).special.add(specialObj);
                    break;
                case "fr_ma":
                    ((com.bgt.lens.model.resume.xray.fr_ma.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_ma.ResDoc)object).special.add(specialObj);
                    break;
                case "fr_tn":
                    ((com.bgt.lens.model.resume.xray.fr_tn.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.fr_tn.ResDoc)object).special.add(specialObj);
                    break;
                case "it_it":
                    ((com.bgt.lens.model.resume.xray.it_it.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.it_it.ResDoc)object).special.add(specialObj);
                    break;
                case "iw_il":
                    ((com.bgt.lens.model.resume.xray.iw_il.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.iw_il.ResDoc)object).special.add(specialObj);
                    break;
                case "ja_jp":
                    ((com.bgt.lens.model.resume.xray.ja_jp.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.ja_jp.ResDoc)object).special.add(specialObj);
                    break;
                case "pl_pl":
                    ((com.bgt.lens.model.resume.xray.pl_pl.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.pl_pl.ResDoc)object).special.add(specialObj);
                    break;
                case "pt_br":
                    ((com.bgt.lens.model.resume.xray.pt_br.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.pt_br.ResDoc)object).special.add(specialObj);
                    break;
                case "pt_pt":
                    ((com.bgt.lens.model.resume.xray.pt_pt.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.pt_pt.ResDoc)object).special.add(specialObj);
                    break;
                case "ru_ru":
                    ((com.bgt.lens.model.resume.xray.ru_ru.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.ru_ru.ResDoc)object).special.add(specialObj);
                    break;
                case "sv_se":
                    ((com.bgt.lens.model.resume.xray.sv_se.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.sv_se.ResDoc)object).special.add(specialObj);
                    break;
                case "tr_tr":
                    ((com.bgt.lens.model.resume.xray.tr_tr.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.tr_tr.ResDoc)object).special.add(specialObj);
                    break;
                case "zh_tw":
                    ((com.bgt.lens.model.resume.xray.zh_tw.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.zh_tw.ResDoc)object).special.add(specialObj);
                    break;

                default:
                    ((com.bgt.lens.model.resume.xray.en_us.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.xray.en_us.ResDoc)object).special.add(specialObj);
                    break;
            }
        } else if (ParsingInstanceType.XRAYEDU.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(resumeResourceName)) {
            ((com.bgt.lens.model.resume.xray.edu.en_us.ResDoc)object).special = new ArrayList<>();
            ((com.bgt.lens.model.resume.xray.edu.en_us.ResDoc)object).special.add(specialObj);

        } else if (ParsingInstanceType.XRAY.toString().equals(instanceType.toLowerCase()) || ParsingInstanceType.XRAYJOBS.toString().equals(instanceType.toLowerCase()) || ParsingInstanceType.LENSSEARCH.toString().equals(instanceType.toLowerCase()) && resource.equalsIgnoreCase(jobResourceName)) {
            switch (locale) {
                case "cn_cn":
                    ((com.bgt.lens.model.job.lens.cn_cn.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.cn_cn.JobDoc)object).special.add(specialObj);
                    break;
                case "de_de":
                    ((com.bgt.lens.model.job.lens.de_de.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.de_de.JobDoc)object).special.add(specialObj);
                    break;
                case "du_be":
                    ((com.bgt.lens.model.job.lens.du_be.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.du_be.JobDoc)object).special.add(specialObj);
                    break;
                case "du_nl":
                    ((com.bgt.lens.model.job.lens.du_nl.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.du_nl.JobDoc)object).special.add(specialObj);
                    break;
                case "en_au":
                    ((com.bgt.lens.model.job.lens.en_au.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_au.JobDoc)object).special.add(specialObj);
                    break;
                case "en_ca":
                    ((com.bgt.lens.model.job.lens.en_ca.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_ca.JobDoc)object).special.add(specialObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.job.lens.en_gb.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_gb.JobDoc)object).special.add(specialObj);
                    break;
                case "en_in":
                    ((com.bgt.lens.model.job.lens.en_in.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_in.JobDoc)object).special.add(specialObj);
                    break;
                case "en_nl":
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).special.add(specialObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.job.lens.en_us.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_us.JobDoc)object).special.add(specialObj);
                    break;
                case "en_za":
                    ((com.bgt.lens.model.job.lens.en_za.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_za.JobDoc)object).special.add(specialObj);
                    break;
                case "es_ar":
                    ((com.bgt.lens.model.job.lens.es_ar.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_ar.JobDoc)object).special.add(specialObj);
                    break;
                case "es_cl":
                    ((com.bgt.lens.model.job.lens.es_cl.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_cl.JobDoc)object).special.add(specialObj);
                    break;
                case "es_es":
                    ((com.bgt.lens.model.job.lens.es_es.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_es.JobDoc)object).special.add(specialObj);
                    break;
                case "es_mx":
                    ((com.bgt.lens.model.job.lens.es_mx.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_mx.JobDoc)object).special.add(specialObj);
                    break;
                case "es_us":
                    ((com.bgt.lens.model.job.lens.es_us.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.es_us.JobDoc)object).special.add(specialObj);
                    break;
                case "fr_be":
                    ((com.bgt.lens.model.job.lens.fr_be.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.fr_be.JobDoc)object).special.add(specialObj);
                    break;
                case "fr_ca":
                    ((com.bgt.lens.model.job.lens.fr_ca.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.fr_ca.JobDoc)object).special.add(specialObj);
                    break;
                case "fr_fr":
                    ((com.bgt.lens.model.job.lens.fr_fr.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.fr_fr.JobDoc)object).special.add(specialObj);
                    break;
                case "it_it":
                    ((com.bgt.lens.model.job.lens.it_it.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.it_it.JobDoc)object).special.add(specialObj);
                    break;
                case "ja_jp":
                    ((com.bgt.lens.model.job.lens.ja_jp.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.ja_jp.JobDoc)object).special.add(specialObj);
                    break;
                case "zh_tw":
                    ((com.bgt.lens.model.job.lens.zh_tw.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.zh_tw.JobDoc)object).special.add(specialObj);
                    break;

                default:
                    ((com.bgt.lens.model.job.lens.en_us.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.lens.en_us.JobDoc)object).special.add(specialObj);
                    break;

            }
        } else if ((ParsingInstanceType.TM.toString().equals(instanceType.toLowerCase()) || ParsingInstanceType.SPECTRUM.toString().equals(instanceType.toLowerCase()))
                && resource.equalsIgnoreCase(resumeResourceName)) {
            switch (locale) {
                case "en_au":
                    ((com.bgt.lens.model.resume.tm.en_au.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_au.ResDoc)object).special.add(specialObj);
                    break;
                case "en_ca":
                    ((com.bgt.lens.model.resume.tm.en_ca.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_ca.ResDoc)object).special.add(specialObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.resume.tm.en_gb.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_gb.ResDoc)object).special.add(specialObj);
                    break;
                case "en_nz":
                    ((com.bgt.lens.model.resume.tm.en_nz.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_nz.ResDoc)object).special.add(specialObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).special.add(specialObj);
                    break;

                default:
                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.resume.tm.en_us.ResDoc)object).special.add(specialObj);
                    break;
            }
        } 
        else if ((ParsingInstanceType.JM.toString().equals(instanceType.toLowerCase()))
            && resource.equalsIgnoreCase(jobResourceName)) {
            switch (locale) {
                case "en_au":
                    ((com.bgt.lens.model.job.jm.en_au.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_au.JobDoc)object).special.add(specialObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.job.jm.en_gb.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_gb.JobDoc)object).special.add(specialObj);
                    break;
                case "en_nz":
                    ((com.bgt.lens.model.job.jm.en_nz.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_nz.JobDoc)object).special.add(specialObj);
                    break;
                case "en_sg":
                    ((com.bgt.lens.model.job.jm.en_sg.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_sg.JobDoc)object).special.add(specialObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.job.jm.en_us.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_us.JobDoc)object).special.add(specialObj);
                    break;

                default:
                    ((com.bgt.lens.model.job.jm.en_us.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.jm.en_us.JobDoc)object).special.add(specialObj);
                    break;
            }
        } else if ((ParsingInstanceType.SPECTRUM.toString().equals(instanceType.toLowerCase()) || (ParsingInstanceType.TM.toString().equals(instanceType.toLowerCase())))
                && resource.equalsIgnoreCase(jobResourceName)) {
            switch (locale) {
                case "en_au":
                    ((com.bgt.lens.model.job.spectrum.en_au.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_au.JobDoc)object).special.add(specialObj);
                    break;
                case "en_gb":
                    ((com.bgt.lens.model.job.spectrum.en_gb.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_gb.JobDoc)object).special.add(specialObj);
                    break;
                case "en_nz":
                    ((com.bgt.lens.model.job.spectrum.en_nz.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_nz.JobDoc)object).special.add(specialObj);
                    break;
                case "en_us":
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).special.add(specialObj);
                    break;

                default:
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).special = new ArrayList<>();
                    ((com.bgt.lens.model.job.spectrum.en_us.JobDoc)object).special.add(specialObj);
                    break;
            }
        }
        return object;
    }

    /**
     * Remove empty nodes with newline
     * @param node 
     */
    public void removeEmptyNodesHRXML(Node node) {
        NodeList list = node.getChildNodes();
        List<Node> nodesToRecursivelyCall = new LinkedList();

        for (int i = 0; i < list.getLength(); i++) {
            nodesToRecursivelyCall.add(list.item(i));
        }

        for (Node tempNode : nodesToRecursivelyCall) {
            removeEmptyNodesHRXML(tempNode);
        }

        boolean emptyElement = node.getNodeType() == Node.ELEMENT_NODE
                && node.getChildNodes().getLength() == 0;
        
        if(emptyElement){
            Node nextNode = node.getNextSibling();
            if(nextNode != null && nextNode.getNodeValue() != null && nextNode.getNodeValue().trim().isEmpty() 
                && nextNode.getNodeValue().matches("[\\n\\r]+")){
                nextNode.getParentNode().removeChild(nextNode);
            }
        }
        
        boolean emptyText = node.getNodeType() == Node.TEXT_NODE
                && node.getNodeValue().trim().isEmpty();
        
        if(node.getNodeValue() != null && node.getNodeValue().trim().isEmpty() 
            && node.getNodeValue().matches("[\\n\\r]+")){
            emptyText = false;
        }

//        if (emptyElement || emptyText) {
        if (emptyElement) {
            if (!node.hasAttributes()) {
                node.getParentNode().removeChild(node);
            }
        }
    }
    
    /**
     * Remove empty nodes without newline
     * @param node 
     */
    public void removeEmptyNodes(Node node) {
        NodeList list = node.getChildNodes();
        List<Node> nodesToRecursivelyCall = new LinkedList();

        for (int i = 0; i < list.getLength(); i++) {
            nodesToRecursivelyCall.add(list.item(i));
        }

        for (Node tempNode : nodesToRecursivelyCall) {
            removeEmptyNodes(tempNode);
        }

        boolean emptyElement = node.getNodeType() == Node.ELEMENT_NODE
                && node.getChildNodes().getLength() == 0;
        
        boolean emptyText = node.getNodeType() == Node.TEXT_NODE
                && node.getNodeValue().trim().isEmpty();

        if (emptyElement || emptyText) {
            if (!node.hasAttributes()) {
                node.getParentNode().removeChild(node);
            }
        }
    }

// TODO : get LENS Response Object

    /**
     *
     * @param hrxmlVersion
     * @param transformedHRXML
     * @return
     * @throws JAXBException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws XMLStreamException
     * @throws XPathExpressionException
     */
        public Object
            getHRXMLResponseObject(String hrxmlVersion, String transformedHRXML) throws JAXBException, ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException, XMLStreamException, XPathExpressionException {

        Object object = null;

        try {
            
            // Remove empty nodes
            DocumentBuilder dBuilder = null;
            Document doc = null;
            dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
           
            doc = dBuilder.parse(new ByteArrayInputStream(transformedHRXML.getBytes("UTF-8")));

            removeEmptyNodesHRXML(doc);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(new OutputStreamWriter(bos, "UTF-8"));
            trans.transform(new DOMSource(doc), result);
//            lensResult = header + new String(bos.toByteArray());
//            StreamResult result = new StreamResult(new StringWriter());
//            trans.transform(new DOMSource(doc), result);
            transformedHRXML = bos.toString("UTF-8");
            
            // TODO : add 3.0+ support
            String version = hrxmlVersion.replace(".", "");
            JAXBContext jaxbContext = null;

            if (!version.startsWith("3")) {
                jaxbContext = hrxmlContext;
            } else {
                throw new ClassNotFoundException();
            }

            XMLInputFactory xif = XMLInputFactory.newFactory();
            XMLStreamReader xsr = xif.createXMLStreamReader(new StringReader(transformedHRXML));
            xsr = xif.createFilteredReader(xsr, new StreamFilter() {

                @Override
                public boolean accept(XMLStreamReader reader) {
                    if (reader.getEventType() == XMLStreamReader.CHARACTERS) {
                        return reader.getText().trim().length() > 0;
                    }
                    return true;
                }

            });

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            // remove empty nodes
            object = jaxbUnmarshaller.unmarshal(new StringReader(transformedHRXML));
        } catch (ClassNotFoundException ex) {
            object = transformedHRXML;
            LOGGER.error("Unable to get HRXML response object for the locale and instance. Please check the configuration.");
            // If the class is not found then return the String response
        }
        return object;
    }

    /**
     *
     * @param instanceType
     * @return
     */
    public String getInstanceTypeFromRequest(String instanceType) {
        if (isInstanceAvailable(instanceType)) {
            return instanceType.toLowerCase();
        } else {
            return null;
        }
    }

    /**
     *
     * @param instanceType
     * @return
     */
    public boolean isInstanceAvailable(String instanceType) {
        boolean status = false;
        for (ParsingInstanceType t : ParsingInstanceType.values()) {
            if (t.toString().equalsIgnoreCase(instanceType)) {
                return status = true;
            }
        }
        return status;
    }

    /**
     * Sort Client API
     *
     * @param coreClientapiList
     * @return
     */
    public List<ClientApi> sortClientAPI(List<ClientApi> coreClientapiList) {
        if (coreClientapiList != null && coreClientapiList.size() > 0) {            
            coreClientapiList.sort(Comparator.comparing(ClientApi::getApiId, Comparator.naturalOrder()));                        
        }

        return coreClientapiList;
    }

    /**
     * Sort license details by client id
     *
     * @param licenseList
     * @return
     */
    public List<License> sortLicenseByClientId(List<License> licenseList) {
        if (licenseList != null && licenseList.size() > 0) {
            licenseList.sort(Comparator.comparing(License::getClientId, Comparator.naturalOrder()));
        }
        return licenseList;
    }

    /**
     * Get the active resume and posting doc server ans hypercube services label
     *
     * @param infoResponse
     * @param serviceType
     * @return
     */
    public List<LensServiceConfig> getLensServiceConfigFromInfo(String infoResponse, String serviceType) {

        List<LensServiceConfig> serviceConfigList = new ArrayList<>();

        try {
            ByteArrayInputStream input = new ByteArrayInputStream(
                    infoResponse.getBytes("UTF-8"));

            DocumentBuilderFactory dbFactory
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(input);

            NodeList nList = doc.getElementsByTagName("service");

            for (int i = 0; i < nList.getLength(); i++) {
                LensServiceConfig config = new LensServiceConfig();
                Node node = nList.item(i);
                String label = node.getAttributes().getNamedItem("label").getNodeValue();
                String name = node.getAttributes().getNamedItem("name").getNodeValue();
                String status = node.getAttributes().getNamedItem("type").getNodeValue();
                String type = node.getAttributes().getNamedItem("type").getNodeValue();

                if (isNotNullOrEmpty(serviceType) && status.equalsIgnoreCase(status)) {
                    if (serviceType.equalsIgnoreCase("R")) {
                        if (type.equalsIgnoreCase("resume")) {
                            config.setServiceLabel(label);
                            config.setServiceName(name);
                            config.setType(type);
                            serviceConfigList.add(config);
                        }
                    } else if (serviceType.equalsIgnoreCase("P")) {
                        if (type.equalsIgnoreCase("posting")) {
                            config.setServiceLabel(label);
                            config.setServiceName(name);
                            config.setType(type);
                            serviceConfigList.add(config);
                        }
                    }
                } else {
                    config.setServiceLabel(label);
                    config.setServiceName(name);
                    config.setType(type);
                    serviceConfigList.add(config);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Parse info error. Get service list failed.");
        }
        return serviceConfigList;
    }

    /**
     * Remove special characters
     *
     * @param str
     * @return
     */
    public String removeSpecialCharacters(String str) {
        if (str != null) {
            StringBuilder sb = new StringBuilder();

            char[] charList = str.toCharArray();

            for (char c : charList) {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '+' || c == '*' || c == '#' || c == '.' || c == '-' || c == '\'' || c == '/' || c == '\\' || c == '!') {
                    sb.append(c);
                } else {
                    sb.append(' ');
                }
            }
            return sb.toString().trim();
        } else {
            return "";
        }
    }

    /**
     *
     * @param parsedResumeBinaryData
     * @return
     */
    public boolean isValidParsedResumeBinary(String parsedResumeBinaryData) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
//            Document document = builder.parse(new BOMInputStream(new ByteArrayInputStream(parsedResumeBinaryData.getBytes("UTF-8")), true, 
//                ByteOrderMark.UTF_8, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_16LE, ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE));
            Document document = builder.parse(new InputSource(new StringReader(parsedResumeBinaryData)));
            Node rootNode = document.getDocumentElement();
            if (rootNode.getNodeName().equalsIgnoreCase("ResDoc")) {
                return true;
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param parsedJobBinaryData
     * @return
     */
    public boolean isValidParsedJobBinary(String parsedJobBinaryData) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
//            Document document = builder.parse(new BOMInputStream(new ByteArrayInputStream(parsedJobBinaryData.getBytes("UTF-8")), true, 
//                ByteOrderMark.UTF_8, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_16LE, ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE));
            Document document = builder.parse(new InputSource(new StringReader(parsedJobBinaryData)));
            Node rootNode = document.getDocumentElement();
            if (rootNode.getNodeName().equalsIgnoreCase("JobDoc")) {
                return true;
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param errorCode
     * @return
     */
    public String getErrorMessageWithURL(String errorCode) {
        return errorCode.replace("#PORTAL_URL#", Helper.servletContext.getAttribute("portalErrorURL").toString());
    }

    /**
     *
     * @param httpServletRequest
     * @return
     */
    public String getUserNameFromRequest(HttpServletRequest httpServletRequest) {
        // <editor-fold defaultstate="collapsed" desc="Get consumer name from request">
        String authHeaderMap;
        if (isNotNull(httpServletRequest)) {
            String authorizationHeader = httpServletRequest.getHeader("authorization");
            if (isNullOrEmpty(authorizationHeader)) {
                throw new ApiException("", getErrorMessageWithURL(ApiErrors.AUTHENTICATION_HEADER_NOT_FOUND), HttpStatus.BAD_REQUEST);
            }
            AuthorizationHeaderParser authorizationHeaderParser = new AuthorizationHeaderParser();
            authHeaderMap = authorizationHeaderParser.parse(authorizationHeader);
        } else {
            throw new ApiException("", getErrorMessageWithURL(ApiErrors.INVALID_HTTP_REQUEST), HttpStatus.BAD_REQUEST);
        }

        if (isNullOrEmpty(authHeaderMap)) {
            throw new ApiException("", getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
        } else {
            return authHeaderMap;
        }
        // </editor-fold>
    }
    
    public Document convertStringToXMLDocument(String xmlString) throws Exception {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
            //Create DocumentBuilder with default configuration
        builder = factory.newDocumentBuilder();

        //Parse the content to Document object
        Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
        return doc;
    }

}
