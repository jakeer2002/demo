// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

import java.util.Date;

/**
 * Search dump criteria
 */
public class SearchCommandDumpCriteria {
    
    /**
     * Id
     */
    protected Integer id;
    
    
    /**
     * Request Id
     */
    protected String requestId;
        
    /**
     * Client Id
     */
    protected Integer clientId;
    
    /**
     * LENS settings Id
     */
    protected Integer lensSettingsId;
    
    /**
     * Vendor Id
     */
    protected Integer vendorId;
        
    /**
     * From date
     */
    protected Date from;
    
    /**
     * To date
     */
    protected Date to;
    
    /**
     * Document type
     */
    protected String type;
    
    /**
     * Get Id
     * @return 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
     /**
     * Get Request Id
     * @return 
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set Request Id
     * @param requestId 
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Get client Id
     * @return 
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Set client Id
     * @param clientId 
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get LENS settings Id
     * @return 
     */
    public Integer getLensSettingsId() {
        return lensSettingsId;
    }

    /**
     * Set LENS settings Id
     * @param lensSettingsId 
     */
    public void setLensSettingsId(Integer lensSettingsId) {
        this.lensSettingsId = lensSettingsId;
    }

    /**
     * Get vendor Id
     * @return 
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * Set vendor Id
     * @param vendorId 
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * Get from date
     * @return 
     */
    public Date getFrom() {
        return from;
    }

    /**
     * Set from date
     * @param from 
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * Get to date
     * @return 
     */
    public Date getTo() {
        return to;
    }

    /**
     * Set to date
     * @param to 
     */
    public void setTo(Date to) {
        this.to = to;
    }
    
    /**
     * Get type
     * @return 
     */
    public String getType() {
        return type;
    }

    /**
     * Set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }
}
