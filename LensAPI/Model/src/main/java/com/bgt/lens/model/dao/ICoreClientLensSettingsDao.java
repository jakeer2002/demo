// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.entity.CoreApi;

/**
 *
 * CoreClientLensSettings dao interface
 */
public interface ICoreClientLensSettingsDao {
    /**
     * Get API by criteria
     *
     * @param apiCriteria
     * @return
     */
    public CoreApi getCoreClientLensSettingsByCriteria(ApiCriteria apiCriteria);

}
