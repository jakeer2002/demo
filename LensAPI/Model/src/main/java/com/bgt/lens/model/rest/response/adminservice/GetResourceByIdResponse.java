package com.bgt.lens.model.rest.response.adminservice;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.bgt.lens.model.rest.response.ResourceList;
import com.bgt.lens.model.rest.response.Resources;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.springframework.http.HttpStatus;

@XmlAccessorType(value = XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"status", "statusCode", "requestId", "responseData", "timeStamp"})
@XmlRootElement(name = "GetResourceByIdResponse")
@XmlSeeAlso({ResourceList.class, Resources.class})
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)

public class GetResourceByIdResponse {

    public GetResourceByIdResponse() {
        status = false;
        statusCode = HttpStatus.OK;
        responseData = "";
    }
    /**
     * API response status.True for success and False for Failure
     */
    @XmlElement(name = "Status")
    public boolean status;

    /**
     * API response status code.True for success and False for Failure
     */
    @XmlElement(name = "StatusCode")
    public HttpStatus statusCode;

    /**
     * Unique id for a transaction
     */
    @XmlElement(name = "RequestId")
    public String requestId;

    /**
     * Unique id for a transaction
     */
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @XmlElement(name = "TimeStamp")
    public Date timeStamp;

    /**
     * Info command response message
     */
    @XmlElements({
        @XmlElement(name = "ResponseData", type = String.class),
        @XmlElement(name = "ResponseData", type = ResourceList.class),
        @XmlElement(name = "ResponseData", type = Resources.class)
    })
    public Object responseData;

    /**
     * *
     * Get status
     *
     * @return
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * *
     * Set status
     *
     * @param Status
     */
    public void setStatus(boolean Status) {
        this.status = Status;
    }

    /**
     * *
     * Get status code
     *
     * @return
     */
    public HttpStatus getStatusCode() {
        return statusCode;
    }

    /**
     * *
     * Set status code
     *
     * @param StatusCode
     */
    public void setStatusCode(HttpStatus StatusCode) {
        this.statusCode = StatusCode;
    }

    /**
     * *
     * Get request Id
     *
     * @return
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * *
     * Set Request Id
     *
     * @param RequestId
     */
    public void setRequestId(String RequestId) {
        this.requestId = RequestId;
    }

    /**
     * *
     * Get timestamp
     *
     * @return
     */
    public Date getTimeStamp() {
        return timeStamp == null ? null : new Date(timeStamp.getTime());
    }

    /**
     * *
     * Set timestamp
     *
     * @param TimeStamp
     */
    public void setTimeStamp(Date TimeStamp) {
        this.timeStamp = TimeStamp == null ? null : new Date(TimeStamp.getTime());
    }

    /**
     * *
     * Get response data
     *
     * @return
     */
    public Object getResponseData() {
        return responseData;
    }

    /**
     * *
     * Set response data
     *
     * @param ResponseData
     */
    public void setResponseData(Object ResponseData) {
        this.responseData = ResponseData;
    }
}
