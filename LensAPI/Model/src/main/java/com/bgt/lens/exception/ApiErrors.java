// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.exception;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import org.springframework.web.context.ServletContextAware;

/**
 *
 * API Error messages
 */
public class ApiErrors implements ServletContextAware {

    // Follow the standard in Error vairable
    // Error variable should contain atleast one of the words
    // ERROR, EXCEPTION, INVALID, NOT, EMPTY, DUPLICATE, NULL
    // TODO : change into https://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx
    public static final String UNKNOWN_EXCEPTION = "(#GEN001)Unknown Api Error. Refer #PORTAL_URL#/errors/GEN001 for more details.";

    public static final String INVALID_SOAP_ACTION = "(#GEN002)Invalid SOAP action. Refer #PORTAL_URL#/errors/GEN002 for more details.";

    public static final String INVALID_SOAP_REQUEST = "(#GEN003)Invalid SOAP request. Refer #PORTAL_URL#/errors/GEN003 for more details.";

    public static final String INVALID_HTTP_METHOD = "(#GEN004)Invalid HTTP method. Refer #PORTAL_URL#/errors/GEN004 for more details.";

    public static final String CONSUMER_CREATION_ERROR = "(#GEN005)Unable to create consumer. Refer #PORTAL_URL#/errors/GEN005 for more details.";

    public static final String CONSUMER_UPDATE_ERROR = "(#GEN006)Unable to update consumer. Refer #PORTAL_URL#/errors/GEN006 for more details.";

    public static final String CONSUMER_DELETE_ERROR = "(#GEN007)Unable to delete consumer. Refer #PORTAL_URL#/errors/GEN007 for more details.";

    public static final String API_CREATION_ERROR = "(#GEN008)Unable to create Api. Refer #PORTAL_URL#/errors/GEN008 for more details.";

    public static final String API_UPDATE_ERROR = "(#GEN009)Unable to update Api. Refer #PORTAL_URL#/errors/GEN009 for more details.";

    public static final String API_DELETE_ERROR = "(#GEN010)Unable to delete Api. Refer #PORTAL_URL#/errors/GEN010 for more details.";

    public static final String LENS_CREATION_ERROR = "(#GEN011)Unable to add Lens settings. Refer #PORTAL_URL#/errors/GEN011 for more details.";

    public static final String LENS_UPDATE_ERROR = "(#GEN012)Unable to update Lens settings. Refer #PORTAL_URL#/errors/GEN012 for more details.";

    public static final String LENS_DELETE_ERROR = "(#GEN013)Unable to delete Lens settings. Refer #PORTAL_URL#/errors/GEN013 for more details.";

    public static final String RESOURCE_CREATION_ERROR = "(#GEN014)Unable to add resource. Refer #PORTAL_URL#/errors/GEN014 for more details.";

    public static final String RESOURCE_UPDATE_ERROR = "(#GEN015)Unable to update resource. Refer #PORTAL_URL#/errors/GEN015 for more details.";

    public static final String RESOURCE_DELETE_ERROR = "(#GEN016)Unable to delete resource. Refer #PORTAL_URL#/errors/GEN016 for more details.";

    public static final String GET_INFO_ERROR = "(#GEN017)Unable to get info. Refer #PORTAL_URL#/errors/GEN017 for more details.";

    public static final String GET_PING_ERROR = "(#GEN018)Unable to get ping response. Refer #PORTAL_URL#/errors/GEN018 for more details.";

    public static final String CONVERT_ERROR = "(#GEN019)Unable to convert document. Refer #PORTAL_URL#/errors/GEN019 for more details.";

    public static final String CANON_ERROR = "(#GEN020)Unable to canon document. Refer #PORTAL_URL#/errors/GEN020 for more details.";

    public static final String PARSE_ERROR = "(#GEN021)Unable to parse document. Refer #PORTAL_URL#/errors/GEN021 for more details.";

    public static final String EMPTY_PARSED_DATA = "(#GEN022)Parsed data is empty. Refer #PORTAL_URL#/errors/GEN022 for more details.";

    public static final String EMPTY_HRXML_DATA = "(#GEN023)Resume Parser (HRXML) feature is not available. Refer #PORTAL_URL#/errors/GEN023 for more details.";

    public static final String STYLESHEET_TRANSFORMATION_ERROR = "(#GEN024)Error in stylesheet transformation. Refer #PORTAL_URL#/errors/GEN024 for more details.";

    public static final String RESUME_EXTRACT_ERROR = "(#GEN025)Error in extracting resume data. Refer #PORTAL_URL#/errors/GEN025 for more details.";

    public static final String INVALID_RESUME_FIELDLIST = "(#GEN026)Invalid resume field list. Refer #PORTAL_URL#/errors/GEN026 for more details.";

    public static final String INVALID_JOB_FIELDLIST = "(#GEN027)Invalid job field list. Refer #PORTAL_URL#/errors/GEN027 for more details.";

    public static final String LENS_ERROR = "(#GEN028)Error while processing data in Lens. Please check the input data. Refer #PORTAL_URL#/errors/GEN028 for more details.";

    public static final String EMPTY_BINARY_DATA = "(#GEN029)Binary data is empty. Refer #PORTAL_URL#/errors/GEN029 for more details.";

    public static final String EMPTY_INSTANCE_TYPE = "(#GEN030)Instance type is empty. Refer #PORTAL_URL#/errors/GEN030 for more details.";

    public static final String EMPTY_RESUME_SEARCH_TYPE = "(#GEN031)Resume search type is empty. Refer #PORTAL_URL#/errors/GEN031 for more details.";

    public static final String INVALID_ALLOWED_TRANSACTION_COUNT = "(#GEN032)Trying to reduce allowed transaction count to a value less than zero. Please check and update the input allowed transaction count.. Refer #PORTAL_URL#/errors/GEN032 for more details.";

    public static final String INVALID_REMAINING_TRANSACTION_COUNT = "(#GEN033)Trying to reduce remaining transaction count to a value less than zero. Please check and update the input remaining transaction count.. Refer #PORTAL_URL#/errors/GEN033 for more details.";

    public static final String INVALID_API_ID_NUMBER = "(#GEN034)API id should not be less than zero. Refer #PORTAL_URL#/errors/GEN034 for more details.";

    public static final String INVALID_CLIENT_ID_NUMBER = "(#GEN035)Client id should not be less than zero. Refer #PORTAL_URL#/errors/GEN035 for more details.";

    public static final String PARSER_ERROR_SEARCH_JOBDESCRIPTION_BINARYDATA = "(#GEN036)Unable to parse job binary data.. Refer #PORTAL_URL#/errors/GEN036 for more details.";

    public static final String PARSER_ERROR_SEARCH_RESUMEDOCUMENT_BINARYDATA = "(#GEN037)Unable to parse resume binary data.. Refer #PORTAL_URL#/errors/GEN037 for more details.";

    public static final String EMPTY_RESUME_SEARCH_JOBDESCRIPTION_CRITERIA = "(#GEN038)Resume search - Job description criteria required.. Refer #PORTAL_URL#/errors/GEN038 for more details.";

    public static final String EMPTY_RESUME_SEARCH_JOBPLAINTEXT = "(#GEN039)Resume search - Job posting plaintext required.. Refer #PORTAL_URL#/errors/GEN039 for more details.";

    public static final String EMPTY_RESUME_SEARCH_RESUMEDOCUMENT_CRITERIA = "(#GEN040)Resume search - Resume document criteria required.. Refer #PORTAL_URL#/errors/GEN040 for more details.";

    public static final String EMPTY_RESUME_SEARCH_RESUMEDOCUMENT_BINARYDATA_AND_RESUMEID = "(#GEN041)Resume search - Resume document either binary data or resume id is required.. Refer #PORTAL_URL#/errors/GEN041 for more details.";

    public static final String EMPTY_RESUME_SEARCH_JOBDOCUMENT_BINARYDATA_AND_RESUMEID = "(#GEN042)Resume search - Job document either binary data or resume id is required. Refer #PORTAL_URL#/errors/GEN042 for more details.";

    public static final String EMPTY_JOB_SEARCH_RESUMEDOCUMENT_BINARYDATA_AND_RESUMEID = "(#GEN043)Job search - Resume document either binary data or resume id is required. Refer #PORTAL_URL#/errors/GEN043 for more details.";

    public static final String EMPTY_JOB_SEARCH_JOBDOCUMENT_BINARYDATA_AND_RESUMEID = "(#GEN044)Job search - Job document either binary data or resume id is required.. Refer #PORTAL_URL#/errors/GEN044 for more details.";

    public static final String EMPTY_RESUME_SEARCH_RESUMEDOCUMENT_BINARYDATA = "(#GEN045)Resume search - Resume document binaryData required.. Refer #PORTAL_URL#/errors/GEN045 for more details.";

    public static final String EMPTY_LOCALE = "(#GEN046)Locale is empty. Refer #PORTAL_URL#/errors/GEN046 for more details.";

    public static final String EMPTY_FACET_CRITERIA = "(#GEN047)Facet criteria is empty. Refer #PORTAL_URL#/errors/GEN047 for more details.";

    public static final String EMPTY_FACET_DIMENSION_NAME = "(#GEN048)Facet dimension name is empty. Refer #PORTAL_URL#/errors/GEN048 for more details.";

    public static final String EMPTY_SEARCH_CRITERIA = "(#GEN049)Search criteria is empty. Refer #PORTAL_URL#/errors/GEN049 for more details.";

    public static final String EMPTY_REQUEST_DATA = "(#GEN050)Request content is empty. Refer #PORTAL_URL#/errors/GEN050 for more details.";

    public static final String UPDATE_TRANSACTION_COUNT_ERROR = "(#GEN051)Unable to update transaction count. Refer #PORTAL_URL#/errors/GEN051 for more details.";

    public static final String ADD_CLIENTAPI_ERROR = "(#GEN052)Unable to add client Api details. Refer #PORTAL_URL#/errors/GEN052 for more details.";

    public static final String UPDATE_CLIENTAPI_ERROR = "(#GEN053)Unable to update client Api details. Refer #PORTAL_URL#/errors/GEN053 for more details.";

    public static final String DELETE_CLIENTAPI_ERROR = "(#GEN054)Unable to delete client Api details. Refer #PORTAL_URL#/errors/GEN054 for more details.";

    public static final String EMPTY_POST_BODY = "(#GEN055)Unable to process input request content. Refer #PORTAL_URL#/errors/GEN055 for more details.";

    public static final String INVALID_MEDIA_TYPE = "(#GEN056)Invalid content type or accept header. Refer #PORTAL_URL#/errors/GEN056 for more details.";

    public static final String INVALID_HTTP_REQUEST = "(#GEN057)Invalid HTTP request. Refer #PORTAL_URL#/errors/GEN057 for more details.";

    public static final String INVALID_NUMBER = "(#GEN058)The number exceeds the limit. Please provide a number within the integer limit. Refer #PORTAL_URL#/errors/GEN058 for more details.";

    public static final String MAX_DOC_COUNT_ERROR = "(#GEN059)Maximum document count should not be less than registered document count. Refer #PORTAL_URL#/errors/GEN059 for more details.";

    public static final String INVALID_MAXDOCUMENT_COUNT = "(#GEN060)Invalid Maximum Document Count. Please provide a number between 10 to 2000000. Refer #PORTAL_URL#/errors/GEN060 for more details.";

    public static final String INVALID_DATA_TYPE = "(#GEN061)Please check the input specified. An error occurred while processing the input due to invalid data type. Refer #PORTAL_URL#/errors/GEN061 for more details.";

    public static final String INVALID_DOC_TYPE = "(#GEN062)Invalid type criteria specified. Please specify either resume or posting. Refer #PORTAL_URL#/errors/GEN062 for more details.";

    public static final String INVALID_AUTHENTICATION_TIMESTAMP = "(#GEN063)Unauthorized. Invalid TimeStamp. Refer #PORTAL_URL#/errors/GEN063 for more details.";

    public static final String UNAUTHORIZED_RESOURCE_ACCESS = "(#GEN064)Not authorized to use API. Refer #PORTAL_URL#/errors/GEN064 for more details.";

    public static final String INACTIVE_CLIENT = "(#GEN065)Client account has been disabled. Refer #PORTAL_URL#/errors/GEN065 for more details.";

    public static final String INACTIVE_LENS = "(#GEN066)LENS or xray services are not available. Refer #PORTAL_URL#/errors/GEN066 for more details.";

    public static final String LICENSE_EXPIRED = "(#GEN067)License expired. Refer #PORTAL_URL#/errors/GEN067 for more details.";

    public static final String INVALID_SIGNATURE_METHOD = "(#GEN068)Unknown signature method. Refer #PORTAL_URL#/errors/GEN068 for more details.";

    public static final String INVALID_SIGNATURE = "(#GEN069)Invalid signature. Refer #PORTAL_URL#/errors/GEN069 for more details.";

    public static final String AUTHENTICATION_HEADER_NOT_FOUND = "(#GEN070)Authentication header not found. Refer #PORTAL_URL#/errors/GEN070 for more details.";

    public static final String AUTHENTICATION_SCHEME_NOT_FOUND = "(#GEN071)Authentication scheme not found. Refer #PORTAL_URL#/errors/GEN071 for more details.";

    public static final String INVALID_AUTHENTICATION_SCHEME = "(#GEN072)Invalid authentication scheme. Refer #PORTAL_URL#/errors/GEN072 for more details.";

    public static final String EMPTY_SIGNATURE = "(#GEN073)OAuth signature is empty. Refer #PORTAL_URL#/errors/GEN073 for more details.";

    public static final String EMPTY_CONSUMER_KEY_SECRET = "(#GEN074)Empty consumer key or secret. Refer #PORTAL_URL#/errors/GEN074 for more details.";

    public static final String EMPTY_CONSUMER_KEY = "(#GEN075)Empty consumer key. Refer #PORTAL_URL#/errors/GEN075 for more details.";

    public static final String EMPTY_CONSUMER_SECRET = "(#GEN076)Empty Consumer secret. Refer #PORTAL_URL#/errors/GEN076 for more details.";

    public static final String EMPTY_ACCESS_TOKEN_SECRET = "(#GEN077)Empty access token secret. Refer #PORTAL_URL#/errors/GEN077 for more details.";

    public static final String EMPTY_AUTHENTICATION_TYPE = "(#GEN078)Authentication type is empty. Refer #PORTAL_URL#/errors/GEN078 for more details.";

    public static final String INVALID_CONSUMER_KEY = "(#GEN079)Invalid consumer key. Refer #PORTAL_URL#/errors/GEN079 for more details.";

    public static final String INVALID_CONSUMER_NAME = "(#GEN080)Invalid consumer name. Refer #PORTAL_URL#/errors/GEN080 for more details.";

    public static final String INVALID_CONSUMER_SECRET = "(#GEN081)Invalid consumer secret. Refer #PORTAL_URL#/errors/GEN081 for more details.";

    public static final String EMPTY_CONSUMER_NAME = "(#GEN082)Empty consumer name. Refer #PORTAL_URL#/errors/GEN082 for more details.";

    public static final String INVALID_ACCESS_TOKEN_SECRET = "(#GEN083)Invalid access token secret. Refer #PORTAL_URL#/errors/GEN083 for more details.";

    public static final String INVALID_AUTHENTICATION_TYPE = "(#GEN084)Invalid authentication type. Refer #PORTAL_URL#/errors/GEN084 for more details.";

    public static final String INVALID_CONNECTION_TYPE = "(#GEN085)Connection is not secure. Try over HTTPS. Refer #PORTAL_URL#/errors/GEN085 for more details.";

    public static final String EMPTY_ACCESS_TOKEN = "(#GEN086)OAuth access token is empty. Refer #PORTAL_URL#/errors/GEN086 for more details.";

    public static final String INVALID_AUTHENTICATION_HEADER = "(#GEN087)Invalid authentication header. Refer #PORTAL_URL#/errors/GEN087 for more details.";

    public static final String API_SECRET_EMPTY = "(#GEN088)Api secret not found or empty. Refer #PORTAL_URL#/errors/GEN088 for more details.";

    public static final String EMPTY_RESOURCE_LIST = "(#GEN089)Empty accesible resource list. Refer #PORTAL_URL#/errors/GEN089 for more details.";

    public static final String INVALID_RESOURCE_LIST = "(#GEN090)Invalid resource list. Refer #PORTAL_URL#/errors/GEN090 for more details.";

    public static final String EMPTY_CLIENT_API = "(#GEN091)Empty client Api. Refer #PORTAL_URL#/errors/GEN091 for more details.";

    public static final String EMPTY_LENS_SETTINGS = "(#GEN092)Empty Lens settings. Refer #PORTAL_URL#/errors/GEN092 for more details.";

    public static final String EMPTY_API_KEY_SECRET = "(#GEN093)Empty Api key or secret. Refer #PORTAL_URL#/errors/GEN093 for more details.";

    public static final String EMPTY_API_SECRET = "(#GEN094)Empty Api secret. Refer #PORTAL_URL#/errors/GEN094 for more details.";

    public static final String EMPTY_API_KEY = "(#GEN095)Empty Api key. Refer #PORTAL_URL#/errors/GEN095 for more details.";

    public static final String EMPTY_API_NAME = "(#GEN096)Empty Api name. Refer #PORTAL_URL#/errors/GEN096 for more details.";

    public static final String EMPTY_RESOURCE_NAME = "(#GEN097)Empty resource name. Refer #PORTAL_URL#/errors/GEN097 for more details.";

    public static final String EMPTY_RESOURCE = "(#GEN098)Empty resource. Refer #PORTAL_URL#/errors/GEN098 for more details.";

    public static final String EMPTY_API_KEY_NAME = "(#GEN099)Empty Api key or name. Refer #PORTAL_URL#/errors/GEN099 for more details.";

    public static final String INVALID_ACTIVATION_DATE = "(#GEN100)Invalid activation date. Refer #PORTAL_URL#/errors/GEN100 for more details.";

    public static final String INVALID_DATE = "(#GEN101)Invalid date. Refer #PORTAL_URL#/errors/GEN101 for more details.";

    public static final String INVALID_FROM_TO_DATE = "(#GEN102)Invalid from or to date. Refer #PORTAL_URL#/errors/GEN102 for more details.";

    public static final String INVALID_EXPIRY_DATE = "(#GEN103)Invalid expiry date. Refer #PORTAL_URL#/errors/GEN103 for more details.";

    public static final String INVALID_VENDOR_SETTINGS_ID_LIST = "(#GEN104)Vendor settings id list is empty or invalid. Refer #PORTAL_URL#/errors/GEN104 for more details.";

    public static final String INVALID_TRANSACTION_COUNT = "(#GEN105)Invalid transaction count. Refer #PORTAL_URL#/errors/GEN105 for more details.";

    public static final String INVALID_DATA_LENGTH = "(#GEN106)Violation on length of any of the specified input data. Please update the request and try again. Refer #PORTAL_URL#/errors/GEN106 for more details.";

    public static final String INVALID_API_OR_LENS_OR_RESOURCE = "(#GEN107)Invalid Api or Lens settings or resource Id. Refer #PORTAL_URL#/errors/GEN107 for more details.";

    public static final String DUPLICATE_CONSUMER_KEY = "(#GEN108)Duplicate consumer key. Refer #PORTAL_URL#/errors/GEN108 for more details.";

    public static final String DUPLICATE_API_KEY_IN_CLIENT_API = "(#GEN109)Duplicate Api key provided in client api list. Refer #PORTAL_URL#/errors/GEN109 for more details.";   
    
    public static final String CONSUMER_KEY_ALREADY_EXISTS = "(#GEN110)Consumer with provided key already exists. Please check and provide new consumer key.. Refer #PORTAL_URL#/errors/GEN110 for more details.";

    public static final String API_KEY_ALREADY_EXISTS = "(#GEN111)API key already exists. Please check and provide new API key.. Refer #PORTAL_URL#/errors/GEN111 for more details.";

    public static final String API_NAME_ALREADY_EXISTS = "(#GEN112)API name already exists. Please check and provide new API name.. Refer #PORTAL_URL#/errors/GEN112 for more details.";

    public static final String CONSUMER_NAME_ALREADY_EXISTS = "(#GEN113)Consumer with provided name already exists. Please check and provide new consumer name.. Refer #PORTAL_URL#/errors/GEN113 for more details.";

    public static final String LENSSETTINGS_ID_NOT_EXIST = "(#GEN114)LENS settings Id specified does not exists. Refer #PORTAL_URL#/errors/GEN114 for more details.";

    public static final String VENDORSETTINGS_ID_NOT_EXIST = "(#GEN115)Vendor settings Id specified does not exists. Refer #PORTAL_URL#/errors/GEN115 for more details.";

    public static final String RESOURCE_ID_NOT_EXIST = "(#GEN116)Resource Id specified does not exist. Please check and update the resource id list.. Refer #PORTAL_URL#/errors/GEN116 for more details.";

    public static final String DUPLICATE_API_KEY = "(#GEN117)Duplicate Api key. Refer #PORTAL_URL#/errors/GEN117 for more details.";

    public static final String DUPLICATE_API_KEY_OR_NAME = "(#GEN118)Duplicate Api key or name. Refer #PORTAL_URL#/errors/GEN118 for more details.";

    public static final String DUPLICATE_API_KEY_NAME = "(#GEN119)Duplicate Api key or name. Refer #PORTAL_URL#/errors/GEN119 for more details.";

    public static final String INVALID_API_KEY = "(#GEN120)Invalid Api key. Refer #PORTAL_URL#/errors/GEN120 for more details.";

    public static final String INVALID_API_KEY_SECRET = "(#GEN121)Invalid Api key or secret. Refer #PORTAL_URL#/errors/GEN121 for more details.";

    public static final String INVALID_API_NAME = "(#GEN122)Invalid Api name. Refer #PORTAL_URL#/errors/GEN122 for more details.";

    public static final String INVALID_API_KEY_NAME = "(#GEN123)Invalid Api key or name. Refer #PORTAL_URL#/errors/GEN123 for more details.";

    public static final String INVALID_API_ID = "(#GEN124)Invalid Api Id. Refer #PORTAL_URL#/errors/GEN124 for more details.";

    public static final String EMPTY_API_ID = "(#GEN125)Empty Api Id. Refer #PORTAL_URL#/errors/GEN125 for more details.";

    public static final String INVALID_CLIENT_ID = "(#GEN126)Invalid Client Id. Refer #PORTAL_URL#/errors/GEN126 for more details.";

    public static final String EMPTY_CLIENT_ID = "(#GEN127)Empty Client Id. Refer #PORTAL_URL#/errors/GEN127 for more details.";

    public static final String INVALID_LENSSETTINGS_ID = "(#GEN128)Invalid Lens settings Id. Refer #PORTAL_URL#/errors/GEN128 for more details.";

    public static final String EMPTY_LENSSETTINGS_ID = "(#GEN129)Empty Lens settings Id. Refer #PORTAL_URL#/errors/GEN129 for more details.";

    public static final String INVALID_REGISTERLOG_ID = "(#GEN130)Invalid Register Log Id. Refer #PORTAL_URL#/errors/GEN130 for more details.";

    public static final String EMPTY_REGISTERLOG_ID = "(#GEN131)Empty Register Log Id. Refer #PORTAL_URL#/errors/GEN131 for more details.";

    public static final String INVALID_USAGELOG_ID = "(#GEN132)Invalid usage log Id. Refer #PORTAL_URL#/errors/GEN132 for more details.";

    public static final String INVALID_SEARCHCOMMANDDUMP_ID = "(#GEN133)Invalid search command dump Id. Refer #PORTAL_URL#/errors/GEN133 for more details.";

    public static final String INVALID_REGISTER_UNREGISTER_TYPE = "(#GEN134)Invalid key specified for RegisterOrUnregister field. Refer #PORTAL_URL#/errors/GEN134 for more details.";

    public static final String INVALID_ERRORLOG_ID = "(#GEN135)Invalid error log Id. Refer #PORTAL_URL#/errors/GEN135 for more details.";

    public static final String INVALID_USAGECOUNTER_ID = "(#GEN136)Invalid usage counter Id. Refer #PORTAL_URL#/errors/GEN136 for more details.";

    public static final String INVALID_RESOURCE_ID = "(#GEN137)Invalid resource Id. Refer #PORTAL_URL#/errors/GEN137 for more details.";

    public static final String EMPTY_RESOURCE_ID = "(#GEN138)Empty resource Id. Refer #PORTAL_URL#/errors/GEN138 for more details.";

    public static final String INVALID_RESOURCE_NAME = "(#GEN139)Invalid resource name. Refer #PORTAL_URL#/errors/GEN139 for more details.";

    public static final String EMPTY_LENS_HOST = "(#GEN140)Empty Lens host. Refer #PORTAL_URL#/errors/GEN140 for more details.";

    public static final String EMPTY_FACET_FILTER_CONFIG = "(#GEN141)Facet filter configuration of specific LENS is empty. Refer #PORTAL_URL#/errors/GEN141 for more details.";

    public static final String INVALID_LENS_HOST = "(#GEN142)Invalid Lens host. Refer #PORTAL_URL#/errors/GEN142 for more details.";

    public static final String EMPTY_LENS_LOCALE = "(#GEN143)Empty Lens locale. Refer #PORTAL_URL#/errors/GEN143 for more details.";

    public static final String INVALID_LENS_PORT = "(#GEN144)Invalid Lens port. Refer #PORTAL_URL#/errors/GEN144 for more details.";

    public static final String INVALID_LENS_LOCALE = "(#GEN145)Invalid Lens locale. Refer #PORTAL_URL#/errors/GEN145 for more details.";

    public static final String INVALID_LENS_VERSION = "(#GEN146)Invalid Lens version. Refer #PORTAL_URL#/errors/GEN146 for more details.";

    public static final String EMPTY_LENS_VERSION = "(#GEN147)Empty Lens version. Refer #PORTAL_URL#/errors/GEN147 for more details.";

    public static final String DUPLICATE_FAILOVER_ID_LIST = "(#GEN148)Duplicate failover instance id list specified. Refer #PORTAL_URL#/errors/GEN148 for more details.";

    public static final String FAILOVER_TO_SAME_INSTANCE = "(#GEN149)An instance should not be failover to itself. Remove the current updating instance from the failover settings list and try again. Refer #PORTAL_URL#/errors/GEN149 for more details.";

    public static final String INVALID_FAILOVER_ID_LIST = "(#GEN150)Invalid failover instance id list specified. Refer #PORTAL_URL#/errors/GEN150 for more details.";

    public static final String UNIQUE_LENS_SETTINGS_VIOLATION_EXCEPTION = "(#GEN151)LENS settings with specified configuration already exists. Please update the details and try again.. Refer #PORTAL_URL#/errors/GEN151 for more details.";

    public static final String GET_CLIENTS_ERROR = "(#GEN152)Unable to get clients matching given criteria. Refer #PORTAL_URL#/errors/GEN152 for more details.";

    public static final String GET_API_ERROR = "(#GEN153)Unable to get Api matching given criteria. Refer #PORTAL_URL#/errors/GEN153 for more details.";

    public static final String GET_LENS_ERROR = "(#GEN154)Unable to get Lens settings matching given criteria. Refer #PORTAL_URL#/errors/GEN154 for more details.";

    public static final String GET_LICENSE_ERROR = "(#GEN155)Unable to get license details matching given criteria. Refer #PORTAL_URL#/errors/GEN155 for more details.";

    public static final String GET_USAGELOG_ERROR = "(#GEN156)Unable to get usage logs. Refer #PORTAL_URL#/errors/GEN156 for more details.";

    public static final String GET_AUTHENTICATIONLOG_ERROR = "(#GEN157)Unable to get authentication logs. Refer #PORTAL_URL#/errors/GEN157 for more details.";

    public static final String GET_ERRORLOG_ERROR = "(#GEN158)Unable to get error logs. Refer #PORTAL_URL#/errors/GEN158 for more details.";

    public static final String ADD_AUTHENTICATIONLOG_ERROR = "(#GEN159)Unable to add authentication log. Refer #PORTAL_URL#/errors/GEN159 for more details.";

    public static final String UPDATE_AUTHENTICATIONLOG_ERROR = "(#GEN160)Unable to update authentication log. Refer #PORTAL_URL#/errors/GEN160 for more details.";

    public static final String DELETE_AUTHENTICATIONLOG_ERROR = "(#GEN161)Unable to delete authentication log. Refer #PORTAL_URL#/errors/GEN161 for more details.";

    public static final String ADD_LENSDOCUMENTDUMP_ERROR = "(#GEN162)Unable to dump Lens document. Refer #PORTAL_URL#/errors/GEN162 for more details.";

    public static final String UPDATE_LENSDOCUMENTDUMP_ERROR = "(#GEN163)Unable to update dump Lens document. Refer #PORTAL_URL#/errors/GEN163 for more details.";

    public static final String DELETE_LENSDOCUMENTDUMP_ERROR = "(#GEN164)Unable to delete dump Lens document. Refer #PORTAL_URL#/errors/GEN164 for more details.";

    public static final String ADD_USAGELOG_ERROR = "(#GEN165)Unable to add usage log. Refer #PORTAL_URL#/errors/GEN165 for more details.";

    public static final String UPDATE_USAGELOG_ERROR = "(#GEN166)Unable to update usage log. Refer #PORTAL_URL#/errors/GEN166 for more details.";

    public static final String DELETE_USAGELOG_ERROR = "(#GEN167)Unable to delete usage log. Refer #PORTAL_URL#/errors/GEN167 for more details.";

    public static final String UPDATE_USAGECOUNTER_ERROR = "(#GEN168)Unable to update usage counter. Refer #PORTAL_URL#/errors/GEN168 for more details.";

    public static final String DELETE_USAGECOUNTER_ERROR = "(#GEN169)Unable to delete usage counter. Refer #PORTAL_URL#/errors/GEN169 for more details.";

    public static final String ADD_ERRORLOG_ERROR = "(#GEN170)Unable to add error log. Refer #PORTAL_URL#/errors/GEN170 for more details.";

    public static final String UPDATE_ERRORLOG_ERROR = "(#GEN171)Unable to update error log. Refer #PORTAL_URL#/errors/GEN171 for more details.";

    public static final String DELETE_ERRORLOG_ERROR = "(#GEN172)Unable to delete error log. Refer #PORTAL_URL#/errors/GEN172 for more details.";

    public static final String GET_USAGECOUNTER_ERROR = "(#GEN173)Unable to get usage counter. Refer #PORTAL_URL#/errors/GEN173 for more details.";

    public static final String GET_RESOURCE_ERROR = "(#GEN174)Unable to get resource. Refer #PORTAL_URL#/errors/GEN174 for more details.";

    public static final String GET_CLIENTAPI_ERROR = "(#GEN175)Unable to get client Api. Refer #PORTAL_URL#/errors/GEN175 for more details.";

    public static final String INVALID_DATE_FORMAT = "(#GEN176)Invalid date format. Date format should be yyyy-MM-dd. Refer #PORTAL_URL#/errors/GEN176 for more details.";

    // This will be removed. 
    // Not an error 
    public static final String LICENSE_ACTIVATION_DATE = "(#GEN177)License will be activated on. Refer #PORTAL_URL#/errors/GEN177 for more details.";
    //

    public static final String INVALID_ENCODING_FORMAT = "(#GEN178)Invalid encoding format specified. Refer #PORTAL_URL#/errors/GEN178 for more details.";

    public static final String INVALID_INSTANCE_TYPE = "(#GEN179)Invalid instance type specified. Refer #PORTAL_URL#/errors/GEN179 for more details.";

    public static final String INVALID_RESUME_SEARCH_TYPE = "(#GEN180)Invalid resume search type specified. Refer #PORTAL_URL#/errors/GEN180 for more details.";

    public static final String INVALID_SOCRING_MODE = "(#GEN181)Invalid scoring mode. Refer #PORTAL_URL#/errors/GEN181 for more details.";

    public static final String SHOW_RECORDS_MIN_INDEX_REQUIRED = "(#GEN182)Min record index value is required. Refer #PORTAL_URL#/errors/GEN182 for more details.";

    public static final String SHOW_RECORDS_INVALID_MIN_MAX_INDEX = "(#GEN183)Invalid min or max index value specified. Refer #PORTAL_URL#/errors/GEN183 for more details.";

    public static final String SHOW_RECORDS_INVALID_MIN_INDEX = "(#GEN184)Invalid min index value specified. Refer #PORTAL_URL#/errors/GEN184 for more details.";

    public static final String SHOW_RECORDS_INVALID_MAX_INDEX = "(#GEN185)Invalid max index value specified. Refer #PORTAL_URL#/errors/GEN185 for more details.";

    public static final String SHOW_RECORDS_MAX_INDEX_REQUIRED = "(#GEN186)Max index record value is required. Refer #PORTAL_URL#/errors/GEN186 for more details.";

    public static final String SHOW_RECORDS_MULTI_VALUE = "(#GEN187)Multiple values specified for show records filter. Either showRecords or Min, max value should be specified and not both. Refer #PORTAL_URL#/errors/GEN187 for more details.";

    public static final String INVALID_SHOW_RECORDS_DATA = "(#GEN188)Invalid show records filter value. Please specify either true or false. Refer #PORTAL_URL#/errors/GEN188 for more details.";

    public static final String REQUIRED_RESUME_SEARCH_TYPE = "(#GEN189)Resume search type are mandatory. Refer #PORTAL_URL#/errors/GEN189 for more details.";

    public static final String INVALID_INSTANCE_TYPE_CUSTOM_FILTER_CONFIG = "(#GEN190)Invalid instance type specified for custom filter configuration. Specify either lens or spectrum as instance type for custom filter configuration. Refer #PORTAL_URL#/errors/GEN190 for more details.";

    public static final String JSON_TRANSFORM_ERROR = "(#GEN191)Unable to convert parsed resume to JSON format.. Refer #PORTAL_URL#/errors/GEN191 for more details.";

    public static final String XML_TRANSFORM_ERROR = "(#GEN192)Unable to convert parsed resume to XML format.. Refer #PORTAL_URL#/errors/GEN192 for more details.";

    public static final String INVALID_VARIANT_TYPE = "(#GEN193)Invalid variant type. Refer #PORTAL_URL#/errors/GEN193 for more details.";

    public static final String EMPTY_LANGUAGE = "(#GEN194)Default locale - Language is empty. Refer #PORTAL_URL#/errors/GEN194 for more details.";

    public static final String EMPTY_COUNTRY = "(#GEN195)Default locale - Country is empty. Refer #PORTAL_URL#/errors/GEN195 for more details.";

    public static final String INVALID_CONSUMER_KEY_SECRET = "(#GEN196)Invalid consumer key or secret. Refer #PORTAL_URL#/errors/GEN196 for more details.";

    public static final String EMPTY_API_KEY_ID = "(#GEN197)Empty Api Id or key. Refer #PORTAL_URL#/errors/GEN197 for more details.";

    public static final String INVALID_API_SECRET = "(#GEN198)Invalid Api secret. Refer #PORTAL_URL#/errors/GEN198 for more details.";

    public static final String EMPTY_VENDOR_NAME = "(#GEN199)Vendor name is empty. Refer #PORTAL_URL#/errors/GEN199 for more details.";

    public static final String EMPTY_RESUME_ID = "(#GEN200)Resume id is empty. Refer #PORTAL_URL#/errors/GEN200 for more details.";

    public static final String RESUME_ID_INVALID_LENGTH = "(#GEN201)Resume id length should not be greater than 100 and valid characters are A-Z a-z 0-9 - _. Refer #PORTAL_URL#/errors/GEN201 for more details.";

    public static final String EMPTY_JOB_ID = "(#GEN202)Job id is empty. Refer #PORTAL_URL#/errors/GEN202 for more details.";

    public static final String JOB_ID_INVALID_LENGTH = "(#GEN203)Job id length should not be greater than 100 and valid characters are A-Z a-z 0-9 - _. Refer #PORTAL_URL#/errors/GEN203 for more details.";

    public static final String VENDOR_NAME_INVALID_LENGTH = "(#GEN204)Vendor name length should not be greater than 50. Refer #PORTAL_URL#/errors/GEN204 for more details.";

    public static final String VENDOR_NAME_INVALID_CHAR = "(#GEN205)Vendor name should contain either alaphabets or numbers and not other special characters. Refer #PORTAL_URL#/errors/GEN205 for more details.";

    public static final String RESUME_ID_INVALID_CHAR = "(#GEN206)Resume id length should not be greater than 100 and valid characters are A-Z a-z 0-9 - _. Refer #PORTAL_URL#/errors/GEN206 for more details.";

    public static final String POSTAL_CODE_INVALID_CHAR = "(#GEN207)Postal code should contain either alaphabets or numbers and not other special characters. Refer #PORTAL_URL#/errors/GEN207 for more details.";

    public static final String EMPTY_INSTANCE_ID = "(#GEN208)Instance Id not specified. Refer #PORTAL_URL#/errors/GEN208 for more details.";

    public static final String EMPTY_DOCSERVER_LABEL = "(#GEN209)Doc server name is empty. Refer #PORTAL_URL#/errors/GEN209 for more details.";

    public static final String VENDOR_CREATION_ERROR = "(#GEN210)Unable to create vendor settings. Refer #PORTAL_URL#/errors/GEN210 for more details.";

    public static final String FETCH_RESUME_ERROR = "(#GEN211)Unable to fetch resume. Refer #PORTAL_URL#/errors/GEN211 for more details.";

    public static final String FETCH_JOB_ERROR = "(#GEN212)Unable to fetch job. Refer #PORTAL_URL#/errors/GEN212 for more details.";

    public static final String SEARCH_RESUME_ERROR = "(#GEN213)Unable to search resume. Refer #PORTAL_URL#/errors/GEN213 for more details.";

    public static final String SEARCH_JOB_ERROR = "(#GEN214)Unable to search job. Refer #PORTAL_URL#/errors/GEN214 for more details.";

    public static final String GET_REGISTERLOG_ERROR = "(#GEN215)Unable to fetch registration log. Refer #PORTAL_URL#/errors/GEN215 for more details.";

    public static final String GET_SEARCHCOMMANDDUMP_ERROR = "(#GEN216)Unable to fetch search command dump. Refer #PORTAL_URL#/errors/GEN216 for more details.";

    public static final String GET_SEARCHFILTERTYPE_ERROR = "(#GEN217)Unable to fetch search filter type. Refer #PORTAL_URL#/errors/GEN217 for more details.";

    public static final String GET_FACETFILTERLIST_ERROR = "(#GEN218)Unable to get facet filter list. Refer #PORTAL_URL#/errors/GEN218 for more details.";

    public static final String GET_FACET_DISTRIBUTION_ERROR = "(#GEN219)Unable to get facet distribution. Refer #PORTAL_URL#/errors/GEN219 for more details.";

    public static final String REGISTER_DOCUMENT_ERROR = "(#GEN220)Unable to register document. Refer #PORTAL_URL#/errors/GEN220 for more details.";

    public static final String UNREGISTER_DOCUMENT_ERROR = "(#GEN221)Unable to unregister document. Refer #PORTAL_URL#/errors/GEN221 for more details.";

    public static final String GET_VENDOR_ERROR = "(#GEN222)Unable to get vendor. Refer #PORTAL_URL#/errors/GEN222 for more details.";

    public static final String GET_LOOKUP_ERROR = "(#GEN223)Unable to get lookups. Refer #PORTAL_URL#/errors/GEN223 for more details.";

    public static final String VENDOR_UPDATE_ERROR = "(#GEN224)Unable to update vendor details. Refer #PORTAL_URL#/errors/GEN224 for more details.";

    public static final String VENDOR_CLOSE_ERROR = "(#GEN225)Unable to close vendor. Refer #PORTAL_URL#/errors/GEN225 for more details.";

    public static final String VENDOR_DELETE_ERROR = "(#GEN226)Unable to delete vendor details. Refer #PORTAL_URL#/errors/GEN226 for more details.";

    public static final String INVALID_SEARCH_INSTANCETYPE = "(#GEN227)Search instance type should be either Spectrum or Lens. Refer #PORTAL_URL#/errors/GEN227 for more details.";

    public static final String DUPLICATE_SEARCH_INSTANCETYPE = "(#GEN228)Duplicate search instance type. Refer #PORTAL_URL#/errors/GEN228 for more details.";

    public static final String EMPTY_VENDOR_ID = "(#GEN229)Vendor id is empty. Refer #PORTAL_URL#/errors/GEN229 for more details.";

    public static final String EMPTY_MAX_COUNT = "(#GEN230)Maximum document count is empty or invalid. Refer #PORTAL_URL#/errors/GEN230 for more details.";

    public static final String INVALID_INSTANCE_ID = "(#GEN231)Invalid Instance Id. Refer #PORTAL_URL#/errors/GEN231 for more details.";

    public static final String INVALID_VENDOR_ID = "(#GEN232)Invalid Vendor Id. Refer #PORTAL_URL#/errors/GEN232 for more details.";

    public static final String INVALID_SEARCH_SETTINGS = "(#GEN233)Invalid Search settings. Check resume or posting vendor details. Refer #PORTAL_URL#/errors/GEN233 for more details.";

    public static final String EMPTY_OR_INVALID_VENDOR_ID = "(#GEN234)Vendor id is empty or invalid. Refer #PORTAL_URL#/errors/GEN234 for more details.";

    public static final String INVALID_TIER = "(#GEN235)Invalid Tier Id. Specify 1 for Internal and 2 for External Users. Refer #PORTAL_URL#/errors/GEN235 for more details.";

    public static final String INVALID_OPERATOR_NAME = "(#GEN236)Invalid operator name specified. Please specify either AND or OR. Refer #PORTAL_URL#/errors/GEN236 for more details.";

    public static final String INVALID_MAX_SEARCHCOUNT = "(#GEN237)Invalid maximum allowed search count. Refer #PORTAL_URL#/errors/GEN237 for more details.";

    public static final String INVALID_MINIMUM_YRSEXP = "(#GEN238)Invalid minimum years experience count. Refer #PORTAL_URL#/errors/GEN238 for more details.";

    public static final String INVALID_MAXIMUM_YRSEXP = "(#GEN239)Invalid maximum years experience count. Refer #PORTAL_URL#/errors/GEN239 for more details.";

    public static final String INVALID_OFFSET_COUNT = "(#GEN240)Invalid offset count. Refer #PORTAL_URL#/errors/GEN240 for more details.";

    public static final String INVALID_MIN_SEARCHSCORE = "(#GEN241)Invalid minimum LENS score. Refer #PORTAL_URL#/errors/GEN241 for more details.";

    public static final String RESUME_SEARCH_COMMAND_BUILD_ERROR = "(#GEN242)Unable to create search command. Refer #PORTAL_URL#/errors/GEN242 for more details.";

    public static final String JOB_SEARCH_COMMAND_BUILD_ERROR = "(#GEN243)Unable to create job search command. Refer #PORTAL_URL#/errors/GEN243 for more details.";

    public static final String INVALID_FACET_CRITERIA = "(#GEN244)Invalid facet criteria specified. Refer #PORTAL_URL#/errors/GEN244 for more details.";

    public static final String INVALID_KEYWORD_CRITERIA = "(#GEN245)Invalid keyword search type specified. Please specify either Generic,  CKS or CustomBooleanSearch. Refer #PORTAL_URL#/errors/GEN245 for more details.";

    public static final String INVALID_SEARCHCRITERIA = "(#GEN246)No search criteria found. Refer #PORTAL_URL#/errors/GEN246 for more details.";

    public static final String ADD_SEARCHCOMMANDDUMP_ERROR = "(#GEN247)Unable to dump search command. Refer #PORTAL_URL#/errors/GEN247 for more details.";

    public static final String ADD_REGISTERLOG_ERROR = "(#GEN248)Unable to add register log to database. Refer #PORTAL_URL#/errors/GEN248 for more details.";

    public static final String ADD_SEARCHLOOKUP_ERROR = "(#GEN249)Unable to add search lookup to database. Refer #PORTAL_URL#/errors/GEN249 for more details.";

    public static final String INVALID_INSTANCE_TYPE_FACET_FILTER_CONFIG = "(#GEN250)Invalid instance type specified for facet filter configuration. Specify either lenssearch or spectrum as instance type for custom filter configuration. Refer #PORTAL_URL#/errors/GEN250 for more details.";

    public static final String EMPTY_FACET_FILTER_KEY = "(#GEN251)Facet filter key is empty. Refer #PORTAL_URL#/errors/GEN251 for more details.";

    public static final String EMPTY_FACET_FILTER_VALUE = "(#GEN252)Facet filter value is empty. Refer #PORTAL_URL#/errors/GEN252 for more details.";

    public static final String EMPTY_FACET_FILTER_NAME = "(#GEN253)Facet filter name is empty. Refer #PORTAL_URL#/errors/GEN253 for more details.";

    public static final String DUPLICATE_FACET_FILTER_KEY = "(#GEN254)Duplicate facet filter key specified. Refer #PORTAL_URL#/errors/GEN254 for more details.";

    public static final String DUPLICATE_FACET_FILTER_VALUE = "(#GEN255)Duplicate facet filter value specified. Refer #PORTAL_URL#/errors/GEN255 for more details.";

    public static final String DUPLICATE_FACET_FILTER_NAME = "(#GEN256)Duplicate facet filter name specified. Refer #PORTAL_URL#/errors/GEN256 for more details.";

    public static final String INVALID_FACET_FILTER_CONFIG = "(#GEN257)Invalid facet filter configuration specified. Refer #PORTAL_URL#/errors/GEN257 for more details.";

    public static final String INVALID_FACET_FILTER_NAME = "(#GEN258)Invalid facet filter name specified. Refer #PORTAL_URL#/errors/GEN258 for more details.";

    public static final String EMPTY_CUSTOM_FILTER_CONFIG = "(#GEN259)Custom filter configuration is empty. Refer #PORTAL_URL#/errors/GEN259 for more details.";

    public static final String EMPTY_CUSTOM_FILTER_KEY = "(#GEN260)Custom filter key is empty. Refer #PORTAL_URL#/errors/GEN260 for more details.";

    public static final String EMPTY_CUSTOM_FILTER_VALUE = "(#GEN261)Custom filter value is empty. Refer #PORTAL_URL#/errors/GEN261 for more details.";

    public static final String EMPTY_CUSTOM_FILTER_NAME = "(#GEN262)Custom filter name is empty. Refer #PORTAL_URL#/errors/GEN262 for more details.";

    public static final String DUPLICATE_CUSTOM_FILTER_KEY = "(#GEN263)Duplicate custom filter key specified. Refer #PORTAL_URL#/errors/GEN263 for more details.";

    public static final String DUPLICATE_CUSTOM_FILTER_VALUE = "(#GEN264)Duplicate custom filter value specified. Refer #PORTAL_URL#/errors/GEN264 for more details.";

    public static final String DUPLICATE_CUSTOM_FILTER_NAME = "(#GEN265)Duplicate custom filter name specified. Refer #PORTAL_URL#/errors/GEN265 for more details.";

    public static final String INVALID_CUSTOM_FILTER_CONFIG = "(#GEN266)Invalid custom filter configuration specified. Refer #PORTAL_URL#/errors/GEN266 for more details.";

    public static final String INVALID_CUSTOM_FILTER_NAME = "(#GEN267)Invalid custom filter name specified. Refer #PORTAL_URL#/errors/GEN267 for more details.";

    public static final String CREATE_REGISTER_CUSTOM_FILTER_ERROR = "(#GEN268)Unable to create register command with custom filter. Refer #PORTAL_URL#/errors/GEN268 for more details.";

    public static final String CREATE_REGISTER_PARSED_RESUME_ERROR = "(#GEN269)Unable to create register command with parsed data. Refer #PORTAL_URL#/errors/GEN269 for more details.";

    public static final String INVALID_PARSED_RESUME = "(#GEN270)Invalid parsed resume data. Refer #PORTAL_URL#/errors/GEN270 for more details.";

    public static final String INVALID_PARSED_JOB = "(#GEN271)Invalid parsed job data. Refer #PORTAL_URL#/errors/GEN271 for more details.";

    public static final String ADD_FILTERlOG_ERROR = "(#GEN272)Unable to add filter log details. Refer #PORTAL_URL#/errors/GEN272 for more details.";

    public static final String GET_COMMANDDUMPID_ERROR = "(#GEN273)Unable to get command dump id. Refer #PORTAL_URL#/errors/GEN273 for more details.";

    public static final String EMPTY_JOB_SEARCH_RESUMEDESCRIPTION_CRITERIA = "(#GEN274)Job search - Resume description criteria required.. Refer #PORTAL_URL#/errors/GEN274 for more details.";

    public static final String EMPTY_JOB_SEARCH_RESUMEPLAINTEXT = "(#GEN275)Job search - Resume plaintext required.. Refer #PORTAL_URL#/errors/GEN275 for more details.";

    public static final String EMPTY_JOB_SEARCH_JOBDOCUMENT_CRITERIA = "(#GEN276)Job search - Job document criteria required.. Refer #PORTAL_URL#/errors/GEN276 for more details.";

    public static final String EMPTY_JOB_SEARCH_JOBDOCUMENT_BINARYDATA = "(#GEN277)Job search - Job document binaryData required.. Refer #PORTAL_URL#/errors/GEN277 for more details.";

    public static final String EMPTY_JOB_SEARCH_RESUMEINCORPUS_CRITERIA = "(#GEN278)Job search - Resume In Corpus criteria required.. Refer #PORTAL_URL#/errors/GEN278 for more details.";

    public static final String EMPTY_JOB_SEARCH_RESUMEINCORPUS_RESUMEID = "(#GEN279)Job search - Resume In Corpus resume id required.. Refer #PORTAL_URL#/errors/GEN279 for more details.";

    public static final String EMPTY_RESUME_SEARCH_JOBINCORPUS_JOBID = "(#GEN280)Resume search - Job In Corpus job id required.. Refer #PORTAL_URL#/errors/GEN280 for more details.";

    public static final String EMPTY_JOB_SEARCH_JOBSLIKETHIS_CRITERIA = "(#GEN281)Job search - JobsLikeThis criteria required.. Refer #PORTAL_URL#/errors/GEN281 for more details.";

    public static final String EMPTY_JOB_SEARCH_JOBSLIKETHIS_RESUMEID = "(#GEN282)Job search - JobsLikeThis job id required.. Refer #PORTAL_URL#/errors/GEN282 for more details.";

    public static final String PARSER_ERROR_JOB_SEARCH_RESUMEDESCRIPTION_RESUMEPLAINTEXT = "(#GEN283)Job search - unable to parse resume description.. Refer #PORTAL_URL#/errors/GEN283 for more details.";

    public static final String PARSER_ERROR_JOB_SEARCH_JOBDOCUMENT_BINARYDATA = "(#GEN284)Job search - unable to parse job description binary data.. Refer #PORTAL_URL#/errors/GEN284 for more details.";

    public static final String INVALID_DISTANCE = "(#GEN285)Invalid distance specified. Refer #PORTAL_URL#/errors/GEN285 for more details.";

    public static final String EMPTY_DISTANCE = "(#GEN286)Distance is empty. Refer #PORTAL_URL#/errors/GEN286 for more details.";

    public static final String LENS_SERVICE_UNAVAILABLE = "(#GEN287)The service is temporarily unavailable, Please contact your Burning Glass representative. Refer #PORTAL_URL#/errors/GEN287 for more details.";

    public static final String LENS_CONVERSION_ERROR = "(#GEN288)Error converting: text not generated. Refer #PORTAL_URL#/errors/GEN288 for more details.";

    public static final String INVALID_CANON_DATA = "(#GEN289)Invalid canon binary data. Refer #PORTAL_URL#/errors/GEN289 for more details.";

    public static final String UNMARSHALL_ERROR = "(#GEN290)Could not unmarshall xml data. Refer #PORTAL_URL#/errors/GEN290 for more details.";

    public static final String HRXML_VERSION_ERROR = "(#GEN291)Invalid HRXML version for the assigned HRXML stylesheet. Refer #PORTAL_URL#/errors/GEN291 for more details.";

    public static final String INVALID_INSTANCE_TYPE_LOCALE = "(#GEN292)Invalid instance type or locale specified. Refer #PORTAL_URL#/errors/GEN292 for more details.";

    public static final String LD_NOT_FOUND = "(#GEN293)Locale Detector not found. Refer #PORTAL_URL#/errors/GEN293 for more details.";

    public static final String INSTANCE_NOT_FOUND = "(#GEN294)LENS instance not found. Refer #PORTAL_URL#/errors/GEN294 for more details.";

    // SEARCH ERRORS - API SPECIFIC
    public static final String INVALID_MAX_DOC_COUNT = "(GEN295)Invalid maximum document count. Refer #PORTAL_URL#/errors/GEN295 for more details.";

    public static final String INVALID_REGISTERED_DOC_COUNT = "(GEN296)Invalid registered document count. Refer #PORTAL_URL#/errors/GEN296 for more details.";

    public static final String VENDOR_ALREADY_EXISTS_ERROR = "(GEN297)Vendor already exists. Refer #PORTAL_URL#/errors/GEN297 for more details.";

    public static final String CREATE_VENDOR_ERROR = "(GEN298)Unable to create vendor. Refer #PORTAL_URL#/errors/GEN298 for more details.";

    public static final String CREATE_VENDOR_ERROR_COUNT_PASSED = "(GEN299)Unable to create vendor. The maximum allowed vendor configuration per doc server reached. Refer #PORTAL_URL#/errors/GEN299 for more details.";

    public static final String OPEN_VENDOR_ERROR = "(GEN300)Unable to open vendor. Refer #PORTAL_URL#/errors/GEN300 for more details.";

    public static final String VENDOR_NOT_FOUND = "(GEN301)Vendor not found. Refer #PORTAL_URL#/errors/GEN301 for more details.";

    public static final String DELETE_VENDOR_ERROR = "(GEN302)Unable to delete vendor. Refer #PORTAL_URL#/errors/GEN302 for more details.";

    public static final String INVALID_VENOR_STATUS = "(GEN303)Invalid vendor status. Refer #PORTAL_URL#/errors/GEN303 for more details.";

    public static final String MAX_DOC_COUNT_LESSTHAN_REG_DOC_COUNT_ERROR = "(GEN304)Maximum document count should not be less than the number of registered document count. Refer #PORTAL_URL#/errors/GEN304 for more details.";

    public static final String REGISTERATION_LIMIT_REACHED_ERROR = "(GEN305)Maximum allowed registration limit reached. Please contact your Burning Glass support to register more documents. Refer #PORTAL_URL#/errors/GEN305 for more details.";

    public static final String VENDOR_CLOSED_ERROR = "(GEN306)The vendor is in closed status. Please check with your Burning Glass support to reopen the vendor. Refer #PORTAL_URL#/errors/GEN306 for more details.";

    public static final String CUSTOM_FILTER_NOT_UPDATED = "(GEN307)Custom filter details are not updated for the specified LENS configuration. Refer #PORTAL_URL#/errors/GEN307 for more details.";

    public static final String UNREGISTER_ERROR_DUPLICATE_ID_ERROR = "(GEN308)Duplicate id. Unable to unregister document. Refer #PORTAL_URL#/errors/GEN308 for more details.";

    public static final String UPDATE_UNREGISTER_DOCUMENT_ERROR = "(GEN309)Apologies. An error occurred while updating the registered document count. Unregistered the document. Please try again later.. Refer #PORTAL_URL#/errors/GEN309 for more details.";

    public static final String UNREGISTER_DOCUMENT_COUNT_ERROR = "(GEN310)Apologies. An error occurred while updating the registered document count. Unable to unregister the document.. Refer #PORTAL_URL#/errors/GEN310 for more details.";

    public static final String REGISTER_RESUME_ERROR = "(GEN311)Unable to register resume. Refer #PORTAL_URL#/errors/GEN311 for more details.";

    public static final String DEFAULT_VENDOR_NOT_FOUND_FOR_GIVEN_INSTANCE = "(GEN312)No default vendor found for the specified instance type and locale. Please specify vendor name. Refer #PORTAL_URL#/errors/GEN313 for more details.";

    public static final String UPDATE_REGISTERED_DOC_COUNT_ERROR = "(GEN313)Unable to update registered document count. Refer #PORTAL_URL#/errors/GEN314 for more details.";

    public static final String RESET_REGISTERED_DOC_COUNT_ERROR = "(GEN314)Unable to reset the registered document count. Refer #PORTAL_URL#/errors/GEN315 for more details.";

    public static final String NOT_ALLOWED_BGCORPUS = "(GEN315)User not allowed to store/remove documents to BGT demo corpus. Please contact burning glass support for more details.. Refer #PORTAL_URL#/errors/GEN316 for more details.";

    public static final String TRANSACTION_COUNT_LIMIT_REACHED = "(GEN316)The transaction count reached the limit. Please contact your Burning Glass representative. Refer #PORTAL_URL#/errors/GEN317 for more details.";

    public static final String FETCH_DOCUMENT_ERROR = "(GEN317)Unable to fetch document. Refer #PORTAL_URL#/errors/GEN318 for more details.";

    public static final String RESUME_NOT_FOUND = "(GEN318)No matching resume found. Refer #PORTAL_URL#/errors/GEN319 for more details.";

    public static final String INVALID_RESULT_FACET_DISTRIBUTION_COUNT = "(GEN319)Invalid result facet distribution count specified. Refer #PORTAL_URL#/errors/GEN321 for more details.";

    public static final String SEARCH_ERROR = "(GEN320)Unable to search, due to internal server error. Refer #PORTAL_URL#/errors/GEN322 for more details.";

    public static final String OFFSET_EXCEED = "(GEN321)Offset exceeds the search result count.. Refer #PORTAL_URL#/errors/GEN323 for more details.";

    public static final String REGISTERATION_LOG_NOT_FOUND = "(GEN322)Registration log does not exist. Refer #PORTAL_URL#/errors/GEN324 for more details.";

    public static final String SEARCH_COMMAND_NOT_FOUND = "(GEN323)Search command dump does not exist. Refer #PORTAL_URL#/errors/GEN325 for more details.";

    public static final String EMPTY_FACET_FILTERLIST = "(GEN324)Facet filter list is empty. Check the client and LENS settings details specified. Refer #PORTAL_URL#/errors/GEN326 for more details.";

    public static final String VENDOR_NOT_CONFIGURED_HYPERCUBE = "(GEN325)The vendor is not configured within hypercube server. Refer #PORTAL_URL#/errors/GEN327 for more details.";

    public static final String INVALID_VENDOR_TYPE = "(GEN326)Invalid vendor type specified. Refer #PORTAL_URL#/errors/GEN328 for more details.";

    public static final String DUPLICATE_DOCUMENT_ID_UNREGISTER_ERROR = "(GEN327)Duplicate id. Unable to unregister document. Refer #PORTAL_URL#/errors/GEN329 for more details.";

    public static final String REGISTER_JOB_ERROR = "(GEN328)Unable to register job. Refer #PORTAL_URL#/errors/GEN330 for more details.";

    public static final String UNREGISTER_JOB_ERROR = "(GEN329)Unable to unregister job. Refer #PORTAL_URL#/errors/GEN331 for more details.";

    public static final String JOB_NOT_FOUND = "(GEN330)No matching job found. Refer #PORTAL_URL#/errors/GEN332 for more details.";

    public static final String INVALID_LENS_SETTINGS_FOR_SEARCH = "(GEN331)Invalid LENS settings id specified for search. Please specify the id of LENS or spectrum instance. Refer #PORTAL_URL#/errors/GEN331 for more details.";

    public static final String USAGE_LOG_NOT_EXIST = "(GEN332)Usage log does not exist. Refer #PORTAL_URL#/errors/GEN332 for more details.";

    public static final String ERROR_LOG_NOT_EXIST = "(GEN333)Error log does not exist. Refer #PORTAL_URL#/errors/GEN333 for more details.";

    public static final String USAGE_COUNTER_NOT_EXIST = "(GEN334)Usage counter does not exist. Refer #PORTAL_URL#/errors/GEN334 for more details.";

    public static final String CLIENTAPI_NOT_EXIST = "(GEN335)Client Api does not exist. Refer #PORTAL_URL#/errors/GEN335 for more details.";

    public static final String CLIENT_NOT_EXIST = "(GEN336)Client does not exist. Refer #PORTAL_URL#/errors/GEN336 for more details.";

    public static final String EMPTY_CLIENT_LIST = "(GEN337)Client list is empty. Refer #PORTAL_URL#/errors/GEN337 for more details.";

    public static final String API_NOT_EXIST = "(GEN338)Api does not exist. Refer #PORTAL_URL#/errors/GEN338 for more details.";

    public static final String RESOURCE_NOT_EXIST = "(GEN339)Resource does not exist. Refer #PORTAL_URL#/errors/GEN339 for more details.";

    public static final String SETTINGS_ALREADY_EXISTS_ERROR = "(GEN340)Settings already exists. Try with different data. Refer #PORTAL_URL#/errors/GEN340 for more details.";

    public static final String INSTANCE_TYPE_MISMATCH_ERROR = "(GEN341)Type of failover instance mismatch with the actual instance to be updated. Refer #PORTAL_URL#/errors/GEN341 for more details.";

    public static final String LENS_SETTINGS_NOT_EXIST = "(GEN342)Lens settings does not exist. Refer #PORTAL_URL#/errors/GEN342 for more details.";

    public static final String MAXIMUM_YRSEXP_LESS_THAN_MINYRSEXP = "(#GEN343)Maximum years of experience should not be less than minimum years of experience. Refer #PORTAL_URL#/errors/GEN343 for more details.";

    public static final String EMPTY_CLARIFY_TEXT = "(#GEN344)Clarify command text is empty. Refer #PORTAL_URL#/errors/GEN344 for more details.";

    public static final String CLARIFY_COMMAND_ERROR = "(GEN345)Unable to clarify text. Refer #PORTAL_URL#/errors/GEN345 for more details.";

    public static final String EMPTY_CLARIFY_RESPONSE = "(GEN346)Clarify response is empty. Refer #PORTAL_URL#/errors/GEN346 for more details.";

    public static final String UNAUTHORIZED_CLARIFY_ACCESS = "(#GEN347)User not authorized to clarify text. Refer #PORTAL_URL#/errors/GEN347 for more details.";

    public static final String FAILOVER_INSTANCE_NOT_FOUND = "(#GEN348)Failover LENS instance not found. Refer #PORTAL_URL#/errors/GEN348 for more details.";

    public static final String RATELIMIT_ERROR = "(#GEN349)Request Limit count has been exceeded please try after sometime. Refer #PORTAL_URL#/errors/GEN349 for more details.";

    public static final String PRODUCT_LOCALE_RATELIMIT_ERROR = "(#GEN350)Product & Locale request Limit count has been exceeded please try after sometime. Refer #PORTAL_URL#/errors/GEN350 for more details.";

    public static final String SAME_VENDOR_ID_ERROR = "(#GEN351)Same vendor id specified in both resume and posting search settings. Refer #PORTAL_URL#/errors/GEN351 for more details.";

    public static final String TOO_MANY_REQUESTS_ERROR = "(#GEN352)The total number of concurrent requests has exceeded the limit allowed by the system. Please resubmit your request with valid number. Refer #PORTAL_URL#/errors/GEN352 for more details.";

    public static final String EMPTY_RATE_LIMIT_PERIOD = "(#GEN353)Empty or invalid Rate Limit time period provided. Please resubmit your request with valid number. Refer #PORTAL_URL#/errors/GEN353 for more details.";

    public static final String EMPTY_RATE_LIMIT_COUNT = "(#GEN354)Empty or Invalid Rate Limit count provided. Refer #PORTAL_URL#/errors/GEN354 for more details.";

    public static final String EMPTY_LENS_RATE_LIMIT_PERIOD = "(#GEN355)Empty or invalid LENS Rate Limit time period provided. Please resubmit your request with valid number. Refer #PORTAL_URL#/errors/GEN355 for more details.";

    public static final String EMPTY_LENS_RATE_LIMIT_COUNT = "(#GEN356)Empty or Invalid LENS Rate Limit count provided. Refer #PORTAL_URL#/errors/GEN356 for more details.";
    public static final String EMPTY_USER_RATE_LIMIT_PERIOD = "(#GEN358)Empty or invalid Rate Limit time period provided. Please resubmit your request with valid number. Refer #PORTAL_URL#/errors/GEN358 for more details.";

    public static final String EMPTY_USER_RATE_LIMIT_COUNT = "(#GEN359)Empty or Invalid Rate Limit count provided. Refer #PORTAL_URL#/errors/GEN359 for more details.";

    public static final String DUPLICATE_RESOURCE = "(#GEN357)Resource name already exists. Refer #PORTAL_URL#/errors/GEN357 for more details.";

    public static final String ADD_ADVANCEDLOG_ERROR = "(#GEN360)Unable to add advanced log. Refer #PORTAL_URL#/errors/GEN360 for more details.";
    
    public static final String LENS_SETTINGS_USED_BY_CLIENT = "(#GEN361)Lens setting is using by clients and cannot be deleted. Refer #PORTAL_URL#/errors/GEN361 for more details.";
    
    public static final String VENDOR_SETTINGS_USED_BY_CLIENT = "(#GEN362)Vendor setting is using by clients and cannot be deleted. Refer #PORTAL_URL#/errors/GEN362 for more details.";

    public static final String INVALID_FACET_DISTRIBUTION_COUNT = "(#GEN363)Invalid facet distribution count. Refer #PORTAL_URL#/errors/GEN363 for more details.";
    
    public static final String EMPTY_CUSTOM_SKILL_KEY = "(#GEN364)Custom skill key is empty. Refer #PORTAL_URL#/errors/GEN364 for more details.";    

    public static final String EMPTY_CUSTOM_SKILL_NAME = "(#GEN365)Custom skill name is empty. Refer #PORTAL_URL#/errors/GEN365 for more details.";
    
    public static final String DUPLICATE_CUSTOM_SKILL_KEY = "(#GEN366)Duplicate custom skill key specified. Refer #PORTAL_URL#/errors/GEN366 for more details.";    

    public static final String DUPLICATE_CUSTOM_SKILL_NAME = "(#GEN367)Duplicate custom skill name specified. Refer #PORTAL_URL#/errors/GEN367 for more details.";
    
    public static final String INVALID_INSTANCE_TYPE_CUSTOM_SKILL_CONFIG = "(#GEN368)Invalid instance type specified for custom skill configuration. Refer #PORTAL_URL#/errors/GEN368 for more details.";
    
    public static final String CUSTOMSKILL_CODE_USED_BY_CLIENT = "(#GEN369)Custom skill code is using by clients and cannot be deleted. Refer #PORTAL_URL#/errors/GEN369 for more details.";
    
    public static final String CUSTOMSKILL_ID_NOT_EXIST = "(#GEN370)Custom skill Id specified does not exist. Please check and update the custom skill id.. Refer #PORTAL_URL#/errors/GEN370 for more details.";
    
    public static final String DUPLICATE_LENSSETTINGS = "(#GEN371)Duplicate Lens settings provided in client lens settings list. Refer #PORTAL_URL#/errors/GEN371 for more details.";
    
    public static final String INVALID_CUSTOMSKILL_KEY = "(#GEN372)Invalid Custom Skill key. Refer #PORTAL_URL#/errors/GEN372 for more details.";
    
    public static final String INVALID_CUSTOMSKILL_NAME = "(#GEN373)Invalid Custom Skill name. Refer #PORTAL_URL#/errors/GEN373 for more details.";
    
    public static final String INVALID_SEARCH_SORT_REQUEST = "(#GEN374)Invalid Search Request - Request should not contain both Sort and SortBy or SortDirection in additional filters. Refer #PORTAL_URL#/errors/GEN374 for more details.";
    
    public static final String INVALID_SEARCH_SORT_CUSTOMFILTER_REQUEST = "(#GEN375)Invalid Search Request - Sort in the additional filters contain one or more invalid custom filters. Refer #PORTAL_URL#/errors/GEN375 for more details.";
    
    public static final String INVALID_SEARCH_KEYWORD_COMBINATION = "(#GEN376)Invalid Search Request - Request should not contain both Keywords and CustomBooleanSearchKeyword in Keyword Search Criteria. Refer #PORTAL_URL#/errors/GEN376 for more details.";
            
    private static ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        ApiErrors.servletContext = servletContext;
    }

    // LENS ERROR MESSAGES - PARSE, SEARCH
    // Since the error codes and error messages wont change often, we are keeping it in a class.

    public String getLensErrorMessage(String error) {
        Map<String, String> lensErrrosMap = new HashMap<>();
        String errormessage = "Cannot Execute";
        // SEARCH ERRORS
        lensErrrosMap.put("SEA001", "Problem closing vendor");
        lensErrrosMap.put("SEA002", "Problem creating vendor");
        lensErrrosMap.put("SEA003", "Problem opening vendor");
        lensErrrosMap.put("SEA004", "Attribute type not found in search query");
        lensErrrosMap.put("SEA005", "Invalid max value found in yrsexp clause");
        lensErrrosMap.put("SEA006", "Invalid min value found in yrsexp clause");
        lensErrrosMap.put("SEA007", "Invalid max value found in distance clause");
        lensErrrosMap.put("SEA008", "Invalid count value found in search");
        lensErrrosMap.put("SEA009", "Invalid count-only value found in search");
        lensErrrosMap.put("SEA010", "Invalid output-grouping value found in search");
        lensErrrosMap.put("SEA011", "Invalid max value found in custom filters clause");
        lensErrrosMap.put("SEA012", "Invalid min value found in custom filters clause");
        lensErrrosMap.put("SEA013", "Invalid min value found in search");
        lensErrrosMap.put("SEA014", "Invalid max value found in search");
        lensErrrosMap.put("SEA015", "Invalid max value found in ayrsexp clause");
        lensErrrosMap.put("SEA016", "Invalid min value found in ayrsexp clause");
        lensErrrosMap.put("SEA017", "Invalid element found in search");
        lensErrrosMap.put("SEA018", "Invalid filter-count value found in search clause");
        lensErrrosMap.put("SEA019", "Invalid filter-min value found in search clause");
        lensErrrosMap.put("SEA020", "Supported value range for attribute min is between 0 to 1000");
        lensErrrosMap.put("SEA021", "Supported value range for attribute max is between 0 to 1000");
        lensErrrosMap.put("SEA022", "Supported value range for attribute filter-min is 1 to 1000");
        lensErrrosMap.put("SEA023", "Invalid values for attribute array in custom array filters clause");
        lensErrrosMap.put("SEA024", "Invalid unknown value found in distance clause");
        lensErrrosMap.put("SEA025", "Invalid unknown value found in yrsexp clause");
        lensErrrosMap.put("SEA026", "Invalid unknown value found in ayrsexp clause");
        lensErrrosMap.put("SEA027", "Invalid unknown value found in custom/custom array filters clause");
        lensErrrosMap.put("SEA028", "Invalid value found for units in distance clause");
        lensErrrosMap.put("SEA029", "Invalid scoring-mode specified in search clause");
        lensErrrosMap.put("SEA030", "Minimum value of filter-count is 1 in search clause");
        lensErrrosMap.put("SEA031", "Element include would only support attribute var");
        lensErrrosMap.put("SEA032", "Element filter would only support attribute var");
        lensErrrosMap.put("SEA033", "Unable to create vendor");
        lensErrrosMap.put("SEA034", "Unable to open vendor");
        lensErrrosMap.put("SEA035", "Couldn't create document: duplicate key");
        lensErrrosMap.put("SEA036", "Valid DocServer is not found for this Listener");
        lensErrrosMap.put("SEA037", "Document not found");
        lensErrrosMap.put("SEA038", "Couldn't contact HyperCube valid HyperCube not found");
        lensErrrosMap.put("SEA039", "Couldn't create document: duplicate id");
        lensErrrosMap.put("SEA040", "No mapping found");
        lensErrrosMap.put("SEA041", "Cannot use text field in sort");
        lensErrrosMap.put("SEA042", "Unknown field");
        lensErrrosMap.put("SEA043", "Unknown sort order");
        lensErrrosMap.put("SEA044", "Unknown sort mode");
        
        // PARSE ERRORS

        lensErrrosMap.put("PAR001", "Empty document text");
        lensErrrosMap.put("PAR002", "Failed to convert document");
        lensErrrosMap.put("PAR003", "Processing Failed: Empty document text");
        lensErrrosMap.put("PAR004", "Processing Failed: Tagging failed: Text too large");
        lensErrrosMap.put("PAR005", "Processing Failed: Standard Exception");
        lensErrrosMap.put("PAR006", "Tagging failed: Unknown error");
        lensErrrosMap.put("PAR007", "Tagging failed: Model not loaded");
        lensErrrosMap.put("PAR008", "Processing Failed: Empty input text");
        lensErrrosMap.put("PAR009", "Preprocessing failed: Data allocation failed");
        lensErrrosMap.put("PAR010", "Preprocessing failed: Unknown error");
        lensErrrosMap.put("PAR011", "Error In Creating Process");
        lensErrrosMap.put("PAR012", "Time Out Interval Elapsed");
        lensErrrosMap.put("PAR013", "Document Conversion Error");
        lensErrrosMap.put("PAR014", "Post processing failed: any bgt error message");
        lensErrrosMap.put("PAR015", "Post processing failed: any standard exception");
        lensErrrosMap.put("PAR016", "Post processing failed: Unknown error");
        lensErrrosMap.put("PAR017", "Script failed: any bgt error message");
        lensErrrosMap.put("PAR018", "Script failed: any standard exception");
        lensErrrosMap.put("PAR019", "Script failed: Unknown error");
        lensErrrosMap.put("PAR020", "Internal conversion error");
        lensErrrosMap.put("PAR021", "rtf output: Document type non-compatible with the converter");
        lensErrrosMap.put("PAR022", "htm output: Document type non-compatible with the converter");
        lensErrrosMap.put("PAR023", "Converters returned empty output");
        lensErrrosMap.put("PAR024", "Known Format: Unable to parse");
        lensErrrosMap.put("PAR025", "Cannot open file for Write");
        lensErrrosMap.put("PAR026", "Cannot open file for Read");
        lensErrrosMap.put("PAR027", "File read error");
        lensErrrosMap.put("PAR028", "File write error");
        lensErrrosMap.put("PAR029", "Unknown Document Format");
        lensErrrosMap.put("PAR030", "Tagging failed: Unable to parse");
        lensErrrosMap.put("PAR031", "XML parse error"); // error message removed from string at end
        lensErrrosMap.put("PAR032", "BGTAG model settings missing from config file");
        lensErrrosMap.put("PAR033", "Error converting: tagger not found");
        lensErrrosMap.put("PAR034", "Error converting: text not generated");
        lensErrrosMap.put("PAR035", "Lookup for BGTAG Model is not loaded properly");
        lensErrrosMap.put("PAR036", "Error in loading BGTAG lookup");
        lensErrrosMap.put("PAR037", "Error loading lookup trie file");
        lensErrrosMap.put("PAR038", "Error tagging with htm: tagger not found");
        lensErrrosMap.put("PAR039", "Error tagging with htm: text not generated");
        lensErrrosMap.put("PAR040", "Error tagging with rtf: tagger not found");
        lensErrrosMap.put("PAR041", "Error tagging with rtf: text not generated");
        lensErrrosMap.put("PAR042", "Error tagging: posting tagger not found");
        lensErrrosMap.put("PAR043", "Error tagging: resume tagger not found");
        lensErrrosMap.put("PAR044", "Error tagging: tag text not generated");
        lensErrrosMap.put("PAR045", "Unknown error");
        lensErrrosMap.put("PAR046", "Invalid command");// <TAGNAME> not supported removed from string at end
        lensErrrosMap.put("PAR047", "Invalid log level"); // LOGLEVEL removed from string at end
        lensErrrosMap.put("PAR048", "A required privilege is not held by the server");
        lensErrrosMap.put("PAR049", "Resume tagger not found");
        lensErrrosMap.put("PAR050", "Unable to log audit record: none");
        lensErrrosMap.put("PAR051", "Unable to send audit record: transient");
        lensErrrosMap.put("PAR052", "Unable to send transactions: transient");
        lensErrrosMap.put("PAR053", "Unexpected element"); // <element_name> removed from string at end
        lensErrrosMap.put("PAR054", "Unknown service");
        lensErrrosMap.put("PAR055", "Posting tagger not found");

        for (Map.Entry<String, String> entry : lensErrrosMap.entrySet()) {
            if (error.toLowerCase().contains(entry.getValue().toLowerCase())) {
                return "(" + entry.getKey() + ")" + entry.getValue() + ". Refer " + ApiErrors.servletContext.getAttribute("portalErrorURL").toString() + "/errors/" + entry.getKey() + " for more details.";
            }else if(error.toLowerCase().contains(errormessage.toLowerCase())) {
            	return "(" + "PAR013" + ")" + "Document Conversion Error" + ". Refer " + ApiErrors.servletContext.getAttribute("portalErrorURL").toString() + "/errors/" + "PAR013" + " for more details.";
            }
        }

        String filteredError = error.replaceFirst("\\(?.*?\\:?\\d+?\\)?(\\s)*\\s", "");
        filteredError = filteredError.replaceFirst("(BGTEC\\d+:)\\s", "");

        return "(LENSGEN001)" + filteredError + ". Refer " + ApiErrors.servletContext.getAttribute("portalErrorURL").toString() + "/errors/LENSGEN001 for more details.";

//        return null;
    }
}
