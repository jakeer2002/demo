/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bgt.lens.model.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author sjagannathan
 */
@Entity
@Table(name = "Core_CustomSkillSettings")
public class CoreCustomskillsettings implements java.io.Serializable {

    private Integer id;
    private CoreLenssettings coreLenssettings;
    private String customSkillCode;
    private String customSkillName;    
    private Set<CoreClientlenssettings> coreClientlenssettings = new HashSet<>(0);

    /**
     *
     */
    public CoreCustomskillsettings() {
    }

    /**
     *
     * @param id
     * @param coreLenssettings
     * @param customSkillCode
     * @param customSkillName
     */
    public CoreCustomskillsettings(Integer id, CoreLenssettings coreLenssettings, String customSkillCode, String customSkillName) {
        this.id = id;
        this.coreLenssettings = coreLenssettings;
        this.customSkillCode = customSkillCode;
        this.customSkillName = customSkillName;
    }
    
    
    /**
    * Get Vendor Id
    * @return 
    */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    /**
     * Set vendor Id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * Get LENS settings Id
     * @return 
     * 
     * 
     */    
    @ManyToOne(fetch = FetchType.LAZY,  cascade = javax.persistence.CascadeType.ALL)    
    @JoinColumn(name = "LensSettingsId", nullable = false)
    public CoreLenssettings getCoreLenssettings() {
        return this.coreLenssettings;
    }
    
    /**
     * Set LENS settings
     * @param coreLenssettings 
     */
    public void setCoreLenssettings(CoreLenssettings coreLenssettings) {
        this.coreLenssettings = coreLenssettings;
    }

    /**
     * Get Custom Skill Key
     * @return 
     */
     /**
     * Get vendor name
     * @return 
     */
    @Column(name = "CustomSkillCode", nullable = false, length = 50)
    public String getCustomSkillCode() {
        return customSkillCode;
    }

    /**
     * Set Custom Skill Key
     * @param customSkillCode
     * @return 
     */
    public void setCustomSkillCode(String customSkillCode) {
        this.customSkillCode = customSkillCode;
    }

    /**
     * Get Custom Skill Name
     * @return 
     */
     /**
     * Get vendor name
     * @return 
     */
    @Column(name = "CustomSkillName", nullable = false, length = 50)
    public String getCustomSkillName() {
        return customSkillName;
    }

    /**
     * Set Custom Skill Name
     * @param customSkillName
     * @return 
     */
    public void setCustomSkillName(String customSkillName) {
        this.customSkillName = customSkillName;
    }

    /**
     * Get Vendor details
     *
     * @return
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "coreCustomskillsettings")             
    @BatchSize(size = 20)            
    public Set<CoreClientlenssettings> getCoreClientlenssettings() {
        return coreClientlenssettings;
    }

    public void setCoreClientlenssettings(Set<CoreClientlenssettings> coreClientlenssettings) {
        this.coreClientlenssettings = coreClientlenssettings;
    }
}
