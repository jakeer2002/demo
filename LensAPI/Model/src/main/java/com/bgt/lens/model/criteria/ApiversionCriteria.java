// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.criteria;

/**
 * api version criteria parameters to get data from database
 */
public class ApiversionCriteria {

    /**
     * ID
     */
    public Integer id;

    /**
     * api Version
     */
    public String apiVersion;

    /**
     * Get id
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get api version
     *
     * @return
     */
    public String getApiVersion() {
        return apiVersion;
    }

    /**
     * Set api version
     *
     * @param apiVersion
     */
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
}
