// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.config;

import java.util.List;

public class SearchSettings {

    protected Boolean enableClarify;
    protected Integer maxSearchCount;
    protected List<Resume> resume;
    protected List<Posting> posting;
    
    public Boolean isEnableClarify() {
        return enableClarify;
    }

    public void setEnableClarify(Boolean enableClarify) {
        this.enableClarify = enableClarify;
    }

    public Integer getMaxSearchCount() {
        return maxSearchCount;
    }

    public void setMaxSearchCount(Integer maxSearchCount) {
        this.maxSearchCount = maxSearchCount;
    }

    public List<Resume> getResume() {
        return resume;
    }

    public void setResume(List<Resume> resume) {
        this.resume = resume;
    }

    public List<Posting> getPosting() {
        return posting;
    }

    public void setPosting(List<Posting> posting) {
        this.posting = posting;
    }
}
