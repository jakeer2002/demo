// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.SearchVendorSettingsCriteria;
import com.bgt.lens.model.dao.ISearchVendorSettingsDao;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.SearchVendorsettings;

/**
 * Search vendor settings dao implementation
 * @author pvarudaraj
 */
public class SearchVendorSettingsDao implements ISearchVendorSettingsDao{
    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize Lens settings Dao
     */
    public SearchVendorSettingsDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set Session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create Len settings
     *
     * @param searchVendorSettings
     */
    @Override
    public void save(SearchVendorsettings searchVendorSettings ) {
        helperDao.save(sessionFactory, searchVendorSettings);
    }

    /**
     * Update Lens settings
     *
     * @param searchVendorSettings
     */
    @Override
    public void update(SearchVendorsettings searchVendorSettings) {
        helperDao.update(sessionFactory, searchVendorSettings);
    }

    /**
     * Delete Lens settings
     *
     * @param searchVendorSettings
     */
    @Override
    public void delete(SearchVendorsettings searchVendorSettings) {
        helperDao.delete(sessionFactory, searchVendorSettings);
    }

    /**
     * Get vendor settings by criteria
     * @param searchVendorSettingsCriteria
     * @return 
     */
    @Override
    public List<SearchVendorsettings> getVendorSettingsByCriteria(SearchVendorSettingsCriteria searchVendorSettingsCriteria) {
        List<SearchVendorsettings> vendorList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<SearchVendorsettings> criteriaQuery = cb.createQuery(SearchVendorsettings.class);
            Root<SearchVendorsettings> searchVendorsettingsRoot = criteriaQuery.from(SearchVendorsettings.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(searchVendorSettingsCriteria.getVendorId())) {
            	conditions.add(cb.equal(searchVendorsettingsRoot.get("id"), searchVendorSettingsCriteria.getVendorId()));
            }

            if (_helper.isNotNullOrEmpty(searchVendorSettingsCriteria.getVendorName())) {
            	conditions.add(cb.equal(searchVendorsettingsRoot.get("vendorName"), searchVendorSettingsCriteria.getVendorName()));
            }
            
            if (_helper.isNotNullOrEmpty(searchVendorSettingsCriteria.getType())) {
            	conditions.add(cb.equal(searchVendorsettingsRoot.get("type"), searchVendorSettingsCriteria.getType()));
            }

            if (_helper.isNotNull(searchVendorSettingsCriteria.getInstanceId())) {
            	conditions.add(cb.equal(searchVendorsettingsRoot.get("coreLenssettings").get("id"), searchVendorSettingsCriteria.getInstanceId()));
            }
            
            if (_helper.isNotNull(searchVendorSettingsCriteria.getClientId())) {
            	Join<SearchVendorsettings, CoreClient> childJoin = searchVendorsettingsRoot.join( "coreClients" );
                conditions.add(cb.equal(childJoin.get("id"), searchVendorSettingsCriteria.getClientId()));
            }
            
            if (_helper.isNotNull(searchVendorSettingsCriteria.getLensSettingsId()) && searchVendorSettingsCriteria.getLensSettingsId().size() > 0){
                Predicate or = cb.disjunction();
                for(Integer settingsId : searchVendorSettingsCriteria.getLensSettingsId()){
                    or.getExpressions().add((cb.equal(searchVendorsettingsRoot.get("coreLenssettings").get("id"),settingsId)));                   
                }
                conditions.add(or);
            }

//            if (_helper.isNotNullOrEmpty(searchVendorSettingsCriteria.getDocServer())) {
//                cr.add(Restrictions.eq("docServer", searchVendorSettingsCriteria.getDocServer()));
//            }
//            
//            if (_helper.isNotNullOrEmpty(searchVendorSettingsCriteria.getHypercubeServer())) {
//                cr.add(Restrictions.eq("hyperCubeServer", searchVendorSettingsCriteria.getHypercubeServer()));
//            }
//            
            if (_helper.isNotNullOrEmpty(searchVendorSettingsCriteria.getStatus())) {
            	conditions.add(cb.equal(searchVendorsettingsRoot.get("status"), searchVendorSettingsCriteria.getStatus()));
            }
            

            // DefaultResultLimit
            criteriaQuery.orderBy(cb.asc(searchVendorsettingsRoot.get("id")));
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<SearchVendorsettings> query = session.createQuery(criteriaQuery);
            List<SearchVendorsettings> results = query.getResultList();
            // ToDo : Need to check duplicate in list
            if (results != null && results.size() > 0) {
                vendorList =  results;

                Set<SearchVendorsettings> vendorSet = new HashSet<>();
                vendorSet.addAll(vendorList);

                vendorList.clear();
                vendorList.addAll(vendorSet);
                
                if(vendorList.size() > 0){
                    for(SearchVendorsettings vendorSettings : vendorList){
                        vendorSettings.getCoreLenssettings().getInstanceType();
                        vendorSettings.getCoreLenssettings().getLocale();
                    }
                }
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } 
        catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return vendorList;
    }

    /**
     * Get vendor by Id
     * @param vendorId
     * @return 
     */
    @Override
    public SearchVendorsettings getVendorById(int vendorId) {
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();
            SearchVendorsettings vendorSettings = (SearchVendorsettings) session.get(SearchVendorsettings.class, vendorId);

            if (vendorSettings != null) {
                Hibernate.initialize(vendorSettings.getCoreLenssettings());
                Hibernate.initialize(vendorSettings.getCoreClients());
            }
            tx.commit();
            LOGGER.info("Transaction Commit");
            return vendorSettings;
        } catch (Exception ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
