// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.exception;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.rest.response.ApiResponse;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;

/**
 *
 * API Exception handler
 */
public class ExceptionHandler {
    
    private static final Helper _helper = new Helper();

    /**
     * Handle Exception
     *
     * @param ex
     * @param response
     * @return
     */
    public ApiResponse handleException(Exception ex, ApiResponse response) {
        if (ex instanceof HttpMessageNotReadableException) {
            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.EMPTY_POST_BODY);
            response.statusCode = HttpStatus.BAD_REQUEST;
        }

        if (ex instanceof HttpRequestMethodNotSupportedException) {
            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_HTTP_METHOD);
            response.statusCode = HttpStatus.METHOD_NOT_ALLOWED;
        }

        if (ex instanceof HttpMediaTypeNotSupportedException) {
            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_MEDIA_TYPE);
            response.statusCode = HttpStatus.BAD_REQUEST;
        }

        if (ex instanceof ApiException) {
            ApiException exception = (ApiException) ex;
            response.responseData = _helper.getErrorMessageWithURL(exception.getMessage());
            response.statusCode = exception.getStatusCode();
        }

        if (ex instanceof TypeMismatchException) {
            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATA_TYPE);
            response.statusCode = HttpStatus.BAD_REQUEST;
        }
        
        if (ex instanceof NumberFormatException) {
            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATA_TYPE);
            response.statusCode = HttpStatus.BAD_REQUEST;
        }
        
        if(ex instanceof MissingServletRequestParameterException){
            response.responseData = _helper.getErrorMessageWithURL(ex.getMessage());
            response.statusCode = HttpStatus.BAD_REQUEST;
        }

        return response;
    }

}
