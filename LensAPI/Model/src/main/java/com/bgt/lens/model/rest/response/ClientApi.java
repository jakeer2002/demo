// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.bgt.lens.helpers.JaxbDateSerializer;
import com.bgt.lens.helpers.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * List of client API
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class ClientApi {

    /**
     * Client API Id
     */
    @XmlElement(name = "Id", required = false)
    protected Integer id;

    /**
     * API Id
     */
    @XmlElement(name = "ApiId", required = false)
    protected Integer apiId;

    /**
     * API Id
     */
    @XmlElement(name = "ApiKey", required = false)
    protected String apiKey;

    /**
     * Enabled
     */
    @XmlElement(name = "Enabled", required = false)
    protected boolean enabled;

    /**
     * API secret
     */
    @XmlElement(name = "Secret", required = false)
    protected String secret;

    /**
     * Activation date
     */
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @XmlElement(name = "ActivationDate", required = false)
    protected Date activationDate;

    /**
     * Expiry date
     */
    @JsonSerialize(using = JsonDateSerializer.class)
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    @XmlElement(name = "ExpiryDate", required = false)
    protected Date expiryDate;

    /**
     * Number of allowed transactions
     */
    @XmlElement(name = "AllowedTransactions", required = false)
    protected Integer allowedTransactions;

    /**
     * Number of remaining transactions
     */
    @XmlElement(name = "RemainingTransactions", required = false)
    protected Integer remainingTransactions;

    /**
     * Get client API Id
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set client API Id
     *
     * @param Id
     */
    public void setId(Integer Id) {
        this.id = Id;
    }

    /**
     * Get API Id
     *
     * @return
     */
    public Integer getApiId() {
        return apiId;
    }

    /**
     * Set API Id
     *
     * @param ApiId
     */
    public void setApiId(Integer ApiId) {
        this.apiId = ApiId;
    }

    /**
     * Get API key
     *
     * @return
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * Set API key
     *
     * @param ApiKey
     */
    public void setApiKey(String ApiKey) {
        this.apiKey = ApiKey;
    }

    /**
     * Get API enabled status
     *
     * @return
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Set API enabled status
     *
     * @param Enabled
     */
    public void setEnabled(boolean Enabled) {
        this.enabled = Enabled;
    }

    /**
     * Get access token secret
     *
     * @return
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Set access token secret
     *
     * @param Secret
     */
    public void setSecret(String Secret) {
        this.secret = Secret;
    }

    /**
     * Get activation date
     *
     * @return
     */
    public Date getActivationDate() {
        return activationDate == null ? null : new Date(activationDate.getTime());
    }

    /**
     * Set activation date
     *
     * @param ActivationDate
     */
    public void setActivationDate(Date ActivationDate) {
        this.activationDate = ActivationDate == null ? null : new Date(ActivationDate.getTime());
    }

    /**
     * Get expiry date
     *
     * @return
     */
    public Date getExpiryDate() {
        return expiryDate == null ? null : new Date(expiryDate.getTime());
    }

    /**
     * Set expiry date
     *
     * @param ExpiryDate
     */
    public void setExpiryDate(Date ExpiryDate) {
        this.expiryDate = ExpiryDate == null ? null : new Date(ExpiryDate.getTime());
    }

    /**
     * Get allowed transaction count
     *
     * @return
     */
    public Integer getAllowedTransactions() {
        return allowedTransactions;
    }

    /**
     * Set allowed transaction count
     *
     * @param AllowedTransactions
     */
    public void setAllowedTransactions(Integer AllowedTransactions) {
        this.allowedTransactions = AllowedTransactions;
    }

    /**
     * Get remaining transaction count
     *
     * @return
     */
    public Integer getRemainingTransactions() {
        return remainingTransactions;
    }

    /**
     * Set remaining transaction count
     *
     * @param RemainingTransactions
     */
    public void setRemainingTransactions(Integer RemainingTransactions) {
        this.remainingTransactions = RemainingTransactions;
    }

}
