// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.es.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;

/**
 * CoreUsagelog
 */
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoreUsagelog implements java.io.Serializable {

    /**
     * Request Id
     */
    @JsonProperty("_id")
    private String requestId;
    
    /**
     * Consumer key
     */
    @JsonProperty("consumerKey")
    private String consumerKey;
    
    /**
     * API
     */
    @JsonProperty("api")
    private String api;

    /**
     * HTTP URI
     */
    @JsonProperty("uri")
    private String uri;

    /**
     * HTTP method
     */
    @JsonProperty("method")
    private String method;

    /**
     * Resource name
     */
    @JsonProperty("resource")
    private String resource;

    /**
     * Product name
     */
    @JsonProperty("instanceType")
    private String instanceType;
    
    /**
     * Locale
     */
    @JsonProperty("locale")
    private String locale;
    
    /**
     * Parse variant
     */
    @JsonProperty("parseVariant")
    private String parseVariant;

    /**
     * Request success or failure status
     */
    @JsonProperty("raisedError")
    private boolean raisedError;

    /**
     * Request in time
     */
    @JsonProperty("inTime")
    private Date inTime;

    /**
     * Response sent time
     */
    @JsonProperty("outTime")
    private Date outTime;

    /**
     * Elapsed time
     */
    @JsonProperty("elapsedTime")
    private Long elapsedTime;
    
    /**
     * IP Address
     */
    @JsonProperty("serverAddress")
    private String serverAddress;
    
    /**
     * Client IP Address
     */
    @JsonProperty("clientAddress")
    private String clientAddress;

    /**
     * Get request Id
     * @return 
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Set request id
     * @param requestId 
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
    
    /**
     * Get consumer key
     * @return 
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Set consumer key
     * @param consumerKey 
     */
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }
    
    /**
     * Get api
     * @return 
     */
    public String getApi() {
        return api;
    }
    
    /**
     * Set api
     * @param api 
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * Get URI
     * @return 
     */
    public String getUri() {
        return uri;
    }

    /**
     * Set uri
     * @param uri 
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * Get method
     * @return 
     */
    public String getMethod() {
        return method;
    }

    /**
     * Set method
     * @param method 
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Get resource
     * @return 
     */
    public String getResource() {
        return resource;
    }

    /**
     * Set resource
     * @param resource 
     */
    public void setResource(String resource) {
        this.resource = resource;
    }

    /**
     * Get instance type
     * @return 
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     * @param instanceType 
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get locale
     * @return 
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set locale
     * @param locale 
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get parse variant
     * @return 
     */
    public String getParseVariant() {
        return parseVariant;
    }

    /**
     * Set parse variant
     * @param parseVariant 
     */
    public void setParseVariant(String parseVariant) {
        this.parseVariant = parseVariant;
    }

    /**
     * Get raised error
     * @return 
     */
    public boolean isRaisedError() {
        return raisedError;
    }

    /**
     * Set raised error
     * @param raisedError 
     */
    public void setRaisedError(boolean raisedError) {
        this.raisedError = raisedError;
    }

    /**
     * Get in time
     * @return 
     */
    public Date getInTime() {
        return inTime;
    }

    /**
     * Set in time
     * @param inTime 
     */
    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }

    /**
     * Get out time
     * @return 
     */
    public Date getOutTime() {
        return outTime;
    }

    /**
     * Set out time
     * @param outTime 
     */
    public void setOutTime(Date outTime) {
        this.outTime = outTime;
    }

    /**
     * Get miliseconds
     * @return 
     */
    public Long getElapsedTime() {
        return elapsedTime;
    }

    /**
     * Set milliseconds
     * @param elapsedTime 
     */
    public void setElapsedTime(Long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    /**
     * Get server address
     * @return 
     */
    public String getServerAddress() {
        return serverAddress;
    }

    /**
     * Set server address
     * @param serverAddress 
     */
    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    /**
     * Get client address
     * @return 
     */
    public String getClientAddress() {
        return clientAddress;
    }

    /**
     * Set client address
     * @param clientAddress 
     */
    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }
}
