// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HelperDao;
import com.bgt.lens.model.criteria.SearchRegisterLogCriteria;
import com.bgt.lens.model.dao.ISearchRegisterLogDao;
import com.bgt.lens.model.entity.SearchRegisterlog;

/**
 * Search register log dao implementation
 *
 * @author pvarudaraj
 */
public class SearchRegisterLogDao implements ISearchRegisterLogDao {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(HelperDao.class);

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Session factory
     */
    private SessionFactory sessionFactory;

    /**
     * Helper Dao object
     */
    public HelperDao helperDao;

    /**
     * Initialize Lens settings Dao
     */
    public SearchRegisterLogDao() {
        helperDao = new HelperDao();
        sessionFactory = null;
    }

    /**
     * Set Session factory
     *
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create register log
     *
     * @param searchRegisterLog
     */
    @Override
    public void save(SearchRegisterlog searchRegisterLog) {
        helperDao.save(sessionFactory, searchRegisterLog);
    }

    /**
     * Update register log
     *
     * @param searchRegisterLog
     */
    @Override
    public void update(SearchRegisterlog searchRegisterLog) {
        helperDao.update(sessionFactory, searchRegisterLog);
    }

    /**
     * Delete searchRegisterLog
     *
     * @param searchRegisterLog
     */
    @Override
    public void delete(SearchRegisterlog searchRegisterLog) {
        helperDao.delete(sessionFactory, searchRegisterLog);
    }

    /**
     * Get registration by criteria
     *
     * @param searchRegisterLogCriteria
     * @return
     */
    @Override
    public List<SearchRegisterlog> getRegisterLogByCriteria(SearchRegisterLogCriteria searchRegisterLogCriteria) {
        List<SearchRegisterlog> registerLogList = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

           // Criteria cr = session.createCriteria(SearchRegisterlog.class);
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<SearchRegisterlog> criteriaQuery = cb.createQuery(SearchRegisterlog.class);
            Root<SearchRegisterlog> searchRegisterlogRoot = criteriaQuery.from(SearchRegisterlog.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(searchRegisterLogCriteria.getId())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("id"), searchRegisterLogCriteria.getId()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.getRequestId())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("requestId"), searchRegisterLogCriteria.getRequestId()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.getClientId())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("clientId"), searchRegisterLogCriteria.getClientId()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.isRegisterOrUnregister())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("registerOrUnregister"), searchRegisterLogCriteria.isRegisterOrUnregister()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.isFailoverUpdateStatus())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("failoverUpdateStatus"), searchRegisterLogCriteria.isFailoverUpdateStatus()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.isOverWrite())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("overWrite"), searchRegisterLogCriteria.isOverWrite()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.getLensSettingsId())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("lensSettingsId"), searchRegisterLogCriteria.getLensSettingsId()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.getVendorId())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("vendorId"), searchRegisterLogCriteria.getVendorId()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.getDocId())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("docId"), searchRegisterLogCriteria.getDocId()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.getFrom())) {
            	conditions.add(cb.greaterThanOrEqualTo(searchRegisterlogRoot.<Date>get("createdOn"), searchRegisterLogCriteria.getFrom()));
            }

            if (_helper.isNotNull(searchRegisterLogCriteria.getTo())) {
            	conditions.add(cb.lessThanOrEqualTo(searchRegisterlogRoot.<Date>get("createdOn"), searchRegisterLogCriteria.getTo()));
            }

            if (_helper.isNotNullOrEmpty(searchRegisterLogCriteria.getType())) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("documentType"), searchRegisterLogCriteria.getType()));
            }

            // CacheMode.IGNORE 
            //  The session will never interact with the cache, except to invalidate cache items when updates occur
            // http://learningviacode.blogspot.in/2013/08/cachemodes-in-hibernate.html
           // cr.setCacheMode(CacheMode.IGNORE);

            // DefaultResultLimit
     //       cr.addOrder(Order.asc("id"));
            criteriaQuery.orderBy(cb.asc(searchRegisterlogRoot.get("id")));
            if(conditions.size()>0) {
            	   criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            }
            TypedQuery<SearchRegisterlog> query = session.createQuery(criteriaQuery);
            List<SearchRegisterlog> results = query.getResultList();
           // List results = cr.list();
            // ToDo : Need to check duplicate in list
            if (results != null && results.size() > 0) {
                registerLogList = results;

                Set<SearchRegisterlog> registerLogSet = new HashSet<>();
                registerLogSet.addAll(registerLogList);

                registerLogList.clear();
                registerLogList.addAll(registerLogSet);

            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return registerLogList;
    }

    /**
     * Get Register log by Id
     *
     * @param registerId
     * @param clientId
     * @return
     */
    @Override
    public SearchRegisterlog getRegisterLogById(int registerId, int clientId) {
        SearchRegisterlog searchRegisterLog = null;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

           // Criteria cr = session.createCriteria(SearchRegisterlog.class);
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<SearchRegisterlog> criteriaQuery = cb.createQuery(SearchRegisterlog.class);
            Root<SearchRegisterlog> searchRegisterlogRoot = criteriaQuery.from(SearchRegisterlog.class);
            List<Predicate> conditions = new ArrayList<Predicate>();
            if (_helper.isNotNull(registerId)) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("id"), registerId));
            }

            if (_helper.isNotNull(clientId)) {
            	conditions.add(cb.equal(searchRegisterlogRoot.get("clientId"), clientId));
            }

            // ProjectionList
//            ProjectionList projList = Projections.projectionList();
//            projList.add(Projections.property("id").as("id"));
//            projList.add(Projections.property("requestId").as("requestId"));
//            projList.add(Projections.property("clientId").as("clientId"));
//            projList.add(Projections.property("lensSettingsId").as("lensSettingsId"));
//            projList.add(Projections.property("locale").as("locale"));
//            projList.add(Projections.property("documentType").as("documentType"));
//            projList.add(Projections.property("instanceType").as("instanceType"));
//            projList.add(Projections.property("vendorId").as("vendorId"));
//            projList.add(Projections.property("docId").as("docId"));
//            projList.add(Projections.property("registerOrUnregister").as("registerOrUnregister"));
//            projList.add(Projections.property("failoverUpdateStatus").as("failoverUpdateStatus"));
//            projList.add(Projections.property("overWrite").as("overWrite"));
//            projList.add(Projections.property("createdOn").as("createdOn"));
//            projList.add(Projections.property("updatedOn").as("updatedOn"));
            criteriaQuery.multiselect(searchRegisterlogRoot.get("id"),
            		searchRegisterlogRoot.get("requestId"),
            		searchRegisterlogRoot.get("clientId"),
            		searchRegisterlogRoot.get("lensSettingsId"),
            		searchRegisterlogRoot.get("locale"),
            		searchRegisterlogRoot.get("documentType"),
            		searchRegisterlogRoot.get("instanceType"),
            		searchRegisterlogRoot.get("vendorId"),
            		searchRegisterlogRoot.get("docId"),
            		searchRegisterlogRoot.get("registerOrUnregister"),
            		searchRegisterlogRoot.get("failoverUpdateStatus"),
            		searchRegisterlogRoot.get("overWrite"),
            		searchRegisterlogRoot.get("createdOn"),
            		searchRegisterlogRoot.get("updatedOn")
            		).distinct(true);
            // CacheMode.IGNORE 
          //  cr.setCacheMode(CacheMode.IGNORE);

         //   cr.setProjection(Projections.distinct(projList));
           // cr.setResultTransformer(new AliasToBeanResultTransformer(SearchRegisterlog.class));
            criteriaQuery.where(conditions.toArray(new Predicate[] {}));
            TypedQuery<SearchRegisterlog> query = session.createQuery(criteriaQuery);
            query.setMaxResults(1);
            List<SearchRegisterlog> results=query.getResultList();
            if (results != null && results.size() > 0) {
            	searchRegisterLog = results.get(0);
            }
          //  searchRegisterLog = (SearchRegisterlog) query.getSingleResult();

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return searchRegisterLog;
    }

	@Override
	public int getRegisterLogCount() {
		int count = 0;
        LOGGER.info("Get Current Session from SessionFactory");
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        try {
            LOGGER.info("Transaction Begin");
            tx = session.beginTransaction();

            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Object> criteriaQuery = cb.createQuery(Object.class);
            Root<SearchRegisterlog> searchRegisterlogRoot = criteriaQuery.from(SearchRegisterlog.class);

            criteriaQuery.select(cb.countDistinct(searchRegisterlogRoot.get("id")));
            TypedQuery<Object> query = session.createQuery(criteriaQuery);
            query.setMaxResults(1);
            List<Object> results=query.getResultList();
            if (results != null && results.size() > 0) {
            	count = Integer.valueOf(results.get(0).toString());
            }

            tx.commit();
            LOGGER.info("Transaction Commit");
        } catch (HibernateException ex) {
            LOGGER.error("Hibernate Exception : " + ex.getMessage());
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    LOGGER.error("Couldn't roll back transaction", rbe);
                }
                LOGGER.info("Transaction Rollback");
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return count;
	}
}
