// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * SearchCommanddump
 */
@Entity
@Table(name = "search_commanddump")
public class SearchCommanddump implements java.io.Serializable {

    /**
     * Id
     */
    private Integer id;

    /**
     * Request Id
     */
    private String requestId;

    /**
     * Client Id
     */
    private Integer clientId;

    /**
     * LensSettings Id
     */
    private Integer lensSettingsId;

    /**
     * Vendor Id
     */
    private Integer vendorId;

    /**
     * Locale
     */
    private String locale;

    /**
     * Document Type
     */
    private String documentType;

    /**
     * Instance Type
     */
    private String instanceType;

    /**
     * Search Command
     */
    private String searchCommand;

    /**
     * Created On
     */
    private Date createdOn;

    private Set<SearchFilterLog> searchFilterLogs = new HashSet<SearchFilterLog>(0);

    public SearchCommanddump() {
    }

    public SearchCommanddump(Integer clientId, String requestId, Integer lensSettingsId, Integer vendorId, String locale, String documentType, String instanceType, String searchCommand, Date createdOn, Set<SearchFilterLog> searchFilterLogs) {
        this.clientId = clientId;
        this.requestId = requestId;
        this.lensSettingsId = lensSettingsId;
        this.vendorId = vendorId;
        this.locale = locale;
        this.documentType = documentType;
        this.instanceType = instanceType;
        this.searchCommand = searchCommand;
        this.createdOn = createdOn;
        this.searchFilterLogs = searchFilterLogs;
    }

    public SearchCommanddump(Integer id, String requestId,Integer clientId, Integer lensSettingsId, String locale, String documentType, String instanceType, Integer vendorId, String searchCommand, Date createdOn) {
        this.id = id;
        this.requestId = requestId;
        this.clientId = clientId;
        this.lensSettingsId = lensSettingsId;
        this.locale = locale;
        this.documentType = documentType;
        this.instanceType = instanceType;
        this.vendorId = vendorId;
        this.searchCommand = searchCommand;
        this.createdOn = createdOn;
    }
    /**
     * Get id
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    /**
     * Set Id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get request Id
     *
     * @return
     */
    @Column(name = "RequestId", nullable = false)
    public String getRequestId() {
        return this.requestId;
    }

    /**
     * Set request Id
     *
     * @param requestId
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Get client Id
     *
     * @return
     */
    @Column(name = "ClientId", nullable = false)
    public Integer getClientId() {
        return this.clientId;
    }

    /**
     * Set client Id
     *
     * @param clientId
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * Get lens settings id
     *
     * @return
     */
    @Column(name = "LensSettingsId", nullable = false)
    public Integer getLensSettingsId() {
        return this.lensSettingsId;
    }

    /**
     * Set lens settings id
     *
     * @param lensSettingsId
     */
    public void setLensSettingsId(Integer lensSettingsId) {
        this.lensSettingsId = lensSettingsId;
    }

    /**
     * Get vendor id
     *
     * @return
     */
    @Column(name = "VendorId", nullable = false)
    public Integer getVendorId() {
        return this.vendorId;
    }

    /**
     * Set vendor id
     *
     * @param vendorId
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * Get search command
     *
     * @return
     */
    @Column(name = "SearchCommand", nullable = false)
    public String getSearchCommand() {
        return this.searchCommand;
    }

    /**
     * Set search command
     *
     * @param searchCommand
     */
    public void setSearchCommand(String searchCommand) {
        this.searchCommand = searchCommand;
    }

    /**
     * Get created on
     *
     * @return
     */
    @Column(name = "CreatedOn", nullable = false)
    public Date getCreatedOn() {
        return this.createdOn;
    }

    /**
     * Set created on
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Get Locale
     *
     * @return
     */
    @Column(name = "Locale")
    public String getLocale() {
        return locale;
    }

    /**
     * Set Locale
     *
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get Document Type
     *
     * @return
     */
    @Column(name = "DocumentType")
    public String getDocumentType() {
        return documentType;
    }

    /**
     * Set Document Type
     *
     * @param documentType
     */
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    /**
     * Get Instance Type
     *
     * @return
     */
    @Column(name = "InstanceType")
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set Instance Type
     *
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "searchCommanddump")
    public Set<SearchFilterLog> getSearchFilterLogs() {
        return this.searchFilterLogs;
    }

    public void setSearchFilterLogs(Set<SearchFilterLog> searchFilterLogs) {
        this.searchFilterLogs = searchFilterLogs;
    }

}
