// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.dao;

import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.criteria.SearchLookupCriteria;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchLookup;
import java.util.List;

/**
 *
 * Search Lookup Dao interface
 */
public interface ISearchCommandDumpDao {
    /**
     * Create search command dump
     *
     * @param searchCommandDump
     */
    public void save(SearchCommanddump searchCommandDump);

    /**
     * Update search command dump
     *
     * @param searchCommandDump
     */
    public void update(SearchCommanddump searchCommandDump);

    /**
     * Delete search command dump
     *
     * @param searchCommandDump
     */
    public void delete(SearchCommanddump searchCommandDump);
    
    /**
     * Get dump by Id
     * @param dumpId
     * @param clientId
     * @return 
     */
    public SearchCommanddump getDumpById(int dumpId, int clientId);
    
    /**
     * Get search command by criteria
     *
     * @param searchDumpCriteria
     * @return
     */
    public List<SearchCommanddump> getDumpByCriteria(SearchCommandDumpCriteria searchDumpCriteria);  
}
