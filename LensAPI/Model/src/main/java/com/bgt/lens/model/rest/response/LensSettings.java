// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * Lens settings
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class LensSettings {

    /**
     * Lens settings Id
     */
    @XmlElement(name = "Id", required = false)
    protected Integer id;

    /**
     * Lens host name
     */
    @XmlElement(name = "Host", required = false)
    protected String host;

    /**
     * Lens port
     */
    @XmlElement(name = "Port", required = false)
    protected Integer port;

    /**
     * Encoding type
     */
    @XmlElement(name = "Charset", required = false)
    protected String charset;

    /**
     * Lens timeout
     */
    @XmlElement(name = "Timeout", required = false)
    protected Integer timeout;

    /**
     * Instance locale
     */
    @XmlElement(name = "Locale", required = false)
    protected String locale;

    /**
     * Instance Language
     */
    @XmlElement(name = "Language", required = false)
    protected String language;

    /**
     * Instance Country
     */
    @XmlElement(name = "Country", required = false)
    protected String country;

    /**
     * Instance HRXMLContent
     */
    @XmlElement(name = "HRXMLContent", required = false)
    protected String hrxmlContent;

    /**
     * Instance HRXMLContent
     */
    @XmlElement(name = "HRXMLVersion", required = false)
    protected String hrxmlVersion;
    
    /**
     * Instance HRXMLContent
     */
    @XmlElement(name = "HRXMLFileName", required = false)
    protected String hrxmlFileName;

    /**
     * Instance Type
     */
    @XmlElement(name = "InstanceType", required = false)
    protected String instanceType;
    
    /**
     * Facet filter settings
     */
    @XmlElement(name = "FilterSettings", required = false)
    protected FilterSettings filterSettings;

    /**
     * Version
     */
    @XmlElement(name = "Version", required = false)
    protected String version;

    @XmlElement(name = "DocServerResume", required = false)
    protected String docServerResume;
    @XmlElement(name = "DocServerPostings", required = false)
    protected String docServerPostings;
    @XmlElement(name = "HypercubeServerResume", required = false)
    protected String hypercubeServerResume;
    @XmlElement(name = "HypercubeServerPostings", required = false)
    protected String hypercubeServerPostings;

     /**
     * Custom Skills Settings
     */
    @XmlElement(name = "CustomSkillsSettings", required = false)
    protected CoreCustomskillsettings customSkillsSettings;    
   
    /**
     * Instance status
     */
    @XmlElement(name = "Status", required = false)
    protected Boolean status;

     @XmlElement(name = "Failoverlenssettings", required = false)
     protected List<Integer> failoverLensSettings;

    public List<Integer> getFailoverLensSettings() {
        return failoverLensSettings;
    }

    public void setFailoverLensSettings(List<Integer> failoverLensSettings) {
        this.failoverLensSettings = failoverLensSettings;
    }
       
    /**
     * get Lens settings Id
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set Lens settings Id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get lens host
     *
     * @return
     */
    public String getHost() {
        return host;
    }

    /**
     * Set Lens host
     *
     * @param host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Get Lens port
     *
     * @return
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Set Lens port
     *
     * @param port
     */
    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * Get Lens encoding
     *
     * @return
     */
    public String getCharset() {
        return charset;
    }

    /**
     * Set Lens encoding
     *
     * @param charset
     */
    public void setCharset(String charset) {
        this.charset = charset;
    }

    /**
     * Get connection timeout
     *
     * @return
     */
    public Integer getTimeout() {
        return timeout;
    }

    /**
     * Set connection timeout
     *
     * @param timeout
     */
    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    /**
     * Get Lens locale
     *
     * @return
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set Lens locale
     *
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get Language of the instance
     *
     * @return
     */
    public String getLanguage() {
        return this.language;
    }

    /**
     * Set Language of the instance
     *
     * @param language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Get Country of the instance
     *
     * @return
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * Set Country of the instance
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    

    public String getHrxmlFileName() {
        return hrxmlFileName;
    }

    public void setHrxmlFileName(String hrxmlFileName) {
        this.hrxmlFileName = hrxmlFileName;
    }

    /**
     * Get instance type
     *
     * @return
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     *
     * @param instanceType
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get HRXML content
     * @return 
     */
    public String getHrxmlContent() {
        return hrxmlContent;
    }

    /**
     * Set HRXML content
     * @param hrxmlContent 
     */
    public void setHrxmlContent(String hrxmlContent) {
        this.hrxmlContent = hrxmlContent;
    }

    /**
     * Get HRXML version
     * @return 
     */
    public String getHrxmlVersion() {
        return hrxmlVersion;
    }

    /**
     * Set HRXML version
     * @param hrxmlVersion 
     */
    public void setHrxmlVersion(String hrxmlVersion) {
        this.hrxmlVersion = hrxmlVersion;
    }

    /**
     * Get filter settings
     * @return 
     */
    public FilterSettings getFilterSettings() {
        return filterSettings;
    }

    /**
     * Set filter settings
     * @param filterSettings 
     */
    public void setFilterSettings(FilterSettings filterSettings) {
        this.filterSettings = filterSettings;
    }

    /**
     * Get Lens version
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     * *
     * Set Lens version
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }
    
     public String getDocServerResume() {
        return docServerResume;
    }

    public void setDocServerResume(String docServerResume) {
        this.docServerResume = docServerResume;
    }

    public String getDocServerPostings() {
        return docServerPostings;
    }

    public void setDocServerPostings(String docServerPostings) {
        this.docServerPostings = docServerPostings;
    }

    public String getHypercubeServerResume() {
        return hypercubeServerResume;
    }

    public void setHypercubeServerResume(String hypercubeServerResume) {
        this.hypercubeServerResume = hypercubeServerResume;
    }

    public String getHypercubeServerPostings() {
        return hypercubeServerPostings;
    }

    public void setHypercubeServerPostings(String hypercubeServerPostings) {
        this.hypercubeServerPostings = hypercubeServerPostings;
    }

    /**
     * Get instance status
     *
     * @return
     */
    public Boolean isStatus() {
        return status;
    }

    /**
     * Set instance status
     *
     * @param status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    public CoreCustomskillsettings getCustomSkillsSettings() {
        return customSkillsSettings;
    }

    public void setCustomSkillsSettings(CoreCustomskillsettings customSkillsSettings) {
        this.customSkillsSettings = customSkillsSettings;
    }   
    
}
