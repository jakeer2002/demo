// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>

package com.bgt.lens.model.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Facet Distribution List
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=JsonInclude.Include.NON_EMPTY)
public class FacetDistributionList {
    /**
     * Document Type. Resume or Posting
     */
    @XmlElement(name = "Type", required = false)
    protected String type;
    
    /**
     * Locale
     */
    @XmlElement(name = "Locale", required = false)
    protected String locale;
    
    /**
     * Vendor
     */
    @XmlElement(name = "Vendor", required = false)
    protected String vendor;
    
    /**
     * Instance type
     */
    @XmlElement(name = "InstanceType", required = false)
    protected String instanceType;
    
    /**
     * List of facet distribution
     */
    @XmlElement(name = "FacetDistribution", required = false)
    protected List<FacetDistribution> facetDistribution;
    
    /**
     * Get type
     * @return 
     */
    public String getType() {
        return type;
    }

    /**
     * Set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get locale
     * @return 
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Set locale
     * @param locale 
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get vendor
     * @return 
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * Set vendor
     * @param vendor 
     */
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * Get instance type
     * @return 
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Set instance type
     * @param instanceType 
     */
    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    /**
     * Get facet distribution list
     * @return 
     */
    public List<FacetDistribution> getFacetDistribution() {
        return facetDistribution;
    }

    /**
     * Set facet distribution list
     * @param facetDistributionList 
     */
    public void setFacetDistribution(List<FacetDistribution> facetDistributionList) {
        this.facetDistribution = facetDistributionList;
    }
}
