// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.model.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * CoreResources generated by hbm2java
 */
@Entity
@Table(name = "core_resources", uniqueConstraints = {
    @UniqueConstraint(columnNames = "Resource"),
    @UniqueConstraint(columnNames = "ApiId")}
)
public class CoreResources implements java.io.Serializable {

    /**
     * Resource Id
     */
    private Integer id;

    /**
     * API details
     */
    private CoreApi coreApi;

    /**
     * Resource name
     */
    private String resource;

    /**
     * Client details
     */
    private Set<CoreClient> coreClients = new HashSet<>(0);

    /**
     * Initialize resources
     */
    public CoreResources() {
    }

    /**
     * Initialize resources
     *
     * @param coreApi
     * @param resource
     */
    public CoreResources(CoreApi coreApi, String resource) {
        this.coreApi = coreApi;
        this.resource = resource;
    }

    /**
     * Initialize resources
     *
     * @param id
     * @param resource
     */
    public CoreResources(Integer id, String resource) {
        this.id = id;
        this.resource = resource;
    }
    
    /**
     * Initialize resources
     *
     * @param coreApi
     * @param resource
     * @param coreClients
     */
    public CoreResources(CoreApi coreApi, String resource, Set<CoreClient> coreClients) {
        this.coreApi = coreApi;
        this.resource = resource;
        this.coreClients = coreClients;
    }

    /**
     * Get resource Id
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    /**
     * Set resource Id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get API
     *
     * @return
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ApiId", nullable = false)
    public CoreApi getCoreApi() {
        return this.coreApi;
    }

    /**
     * Set API
     *
     * @param coreApi
     */
    public void setCoreApi(CoreApi coreApi) {
        this.coreApi = coreApi;
    }

    /**
     * Get resource
     *
     * @return
     */
    @Column(name = "Resource", unique = true, nullable = false, length = 50)
    public String getResource() {
        return this.resource;
    }

    /**
     * Set resource
     *
     * @param resource
     */
    public void setResource(String resource) {
        this.resource = resource;
    }

    /**
     * Get clients
     *
     * @return
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "core_clientresources", joinColumns = {
        @JoinColumn(name = "ResourceId", nullable = false, updatable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "ClientId", nullable = false, updatable = false)})
    public Set<CoreClient> getCoreClients() {
        return this.coreClients;
    }

    /**
     * Set clients
     *
     * @param coreClients
     */
    public void setCoreClients(Set<CoreClient> coreClients) {
        this.coreClients = coreClients;
    }

}
