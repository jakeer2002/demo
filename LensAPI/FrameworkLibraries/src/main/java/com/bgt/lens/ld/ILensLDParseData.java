// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.ld;

import com.bgt.lens.instancemanager.impl.LensInstanceManager;
import com.bgt.lens.LensException;
import com.bgt.lens.LensMessage;
import com.bgt.lens.ServerBusyException;
import com.bgt.lens.helpers.Enum.VariantType;
import java.util.concurrent.ConcurrentHashMap;

public interface ILensLDParseData {

    LensMessage processLocale(byte[] data, String docType, char type, LensInstanceManager lensInstanceManager, ConcurrentHashMap lensDetails) throws LensException, ServerBusyException;

    String tagDocument(byte[] data, String docType, char type, String locale, VariantType variantType, LensInstanceManager lensInstanceManager, ConcurrentHashMap lensDetails) throws LensException, ServerBusyException;

    void ldParsingCheckRateLimit(Integer clientId, String host, Integer port,String locale, String instanceType);
}
