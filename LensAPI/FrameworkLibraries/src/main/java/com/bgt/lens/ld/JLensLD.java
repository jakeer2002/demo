// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.ld;

import com.bgt.lens.instancemanager.impl.LensInstanceManager;
import com.bgt.lens.*;
import static com.bgt.lens.LensSession.RESUME_TYPE;
import com.bgt.lens.helpers.Enum.VariantType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.CharacterData;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * JLensLD is a component that allows a Java application to talk to Lens server
 * with locale based routing capability.
 */
public class JLensLD extends JLens {

    @Autowired
    LensInstanceManager lensInstanceManager;

    private static final Logger LOGGER = LogManager.getLogger(LensInstanceManager.class);
//    private static List<CoreLenssettings> lensSettingsList = null;
//    private final LensInstanceManager lensInstanceManager = new LensInstanceManager();
    private final Helper _helper = new Helper();
    private final char resumeType = 'R';
    private final char postingType = 'P';

    @Autowired
    ILensLDParseData lensLDParseData;

    public JLensLD() {
        super("localhost", 0, EncodingConstants.en_UTF_8);
    }

    public ILensLDParseData getLensLDParseData() {
        return lensLDParseData;
    }

    public void setLensLDParseData(ILensLDParseData lensLDParseData) {
        this.lensLDParseData = lensLDParseData;
    }

    /**
     * Method use to create <code>LensSession</code>, this method will use the
     * global
     *
     * @param lenssettingses
     * @param jLensLD
     * @return LensSession
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     */
    public synchronized LensSession createSession(List<CoreLenssettings> lenssettingses, JLensLD jLensLD) throws LensException, ServerBusyException {
//        lensSettingsList = new ArrayList<>();
//        lensSettingsList = lenssettingses;
        return jLensLD;
    }

    /**
     * Load the lens settings to memory given by the client.
     *
     * @throws LensException if any XML parser error occurs
     * @see LensException
     */
    @Override
    public void open() throws LensException {
//        try {
//            lensInstanceManager.loadConfigDetails(lensSettingsList);
//        } catch (LensException lex) {
//            LOGGER.error("Exception : ", lex);
//            throw lex;
//        }
    }

    /**
     * Method to convert binary data into ASCII text.
     *
     * @param data <code>byte[]</code>
     * @param docType <code>String</code> type of the document to be converted,
     * ex .doc, .pdf
     * @return <code>LensMessage</code> result
     * @throws LensException if any communication error, timeout occurs
     * @throws com.bgt.lens.ServerBusyException
     * @see LensMessage
     * @see LensException
     */
    @Override
    public LensMessage convertBinaryData(byte[] data, String docType) throws LensException, ServerBusyException {
//        LensMessage convMessage = null;
//        try {
//            convMessage = convertToUnicode(data, docType);
//        } catch (LensException | ServerBusyException lex) {
//            throw lex;
//        } catch (Exception ex) {
//            LOGGER.error("Exception : ", ex);
//            throw new LensException(ErrorIndex.INTERNAL_ERR);
//        }
//        return convMessage;
        return null;
    }

    public LensMessage convertBinaryData(byte[] data, String docType, List<CoreLenssettings> lensSettings) throws LensException, ServerBusyException {
        LensMessage convMessage = null;
        try {
            convMessage = convertToUnicode(data, docType, lensSettings);
        } catch (LensException | ServerBusyException lex) {
            throw lex;
        } catch (Exception ex) {
            LOGGER.error("Exception : ", ex);
            throw new LensException(ErrorIndex.INTERNAL_ERR);
        }
        return convMessage;
    }

    /**
     * Method to get the Locale from the document and Tag the document with
     * respective Lens Instance
     *
     * @param data <code>byte[]</code> data to be tagged
     * @param docType <code>String</code> Document type (DOC|PDF|RTF|TXT)
     * @param type <code>char</code> R or P
     * @return outMessage <code>LensMessage</code>
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     */
    @Override
    public LensMessage tagBinaryData(byte[] data, String docType, char type) throws LensException, ServerBusyException {
        return null;
    }

    public LensMessage tagBinaryData(byte[] data, String docType, char type, List<CoreLenssettings> lensSettings) throws LensException, ServerBusyException {
        LensMessage localeMessage = null;
        LensMessage outMessage = null;
        LensSession lensSession = null;

        try {
            String lensResult = null;
            ConcurrentHashMap lensDetails = generateLensSettingsHashMap(lensSettings);
            localeMessage = lensLDParseData.processLocale(data, docType, type, lensInstanceManager, lensDetails);
            lensResult = localeMessage.getMessageData();
            // Added to check whether the output of Locale is error. If error, then return it as error.
            if (!_helper.checkLensResultForFailure(lensResult)) {
                return localeMessage;
            }
            String locale = lensInstanceManager.getLocale(lensResult);
            LOGGER.info("Locale Detection completed. Identified locale " + locale);
            String messageData = lensLDParseData.tagDocument(data, docType, type, locale, VariantType.bgtxml, lensInstanceManager, lensDetails);
            if (type == resumeType) {
                messageData = replaceLast(messageData, "</ResDoc>", "<special><locale>" + locale + "</locale></special></ResDoc>");
            } else {
                messageData = replaceLast(messageData, "</JobDoc>", "<special><locale>" + locale + "</locale></special></JobDoc>");
            }
            outMessage = LensMessage.create(messageData, LensMessage.XML_TYPE);
            lensInstanceManager.closeSession(lensSession);
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            lensInstanceManager.closeSession(lensSession);
            throw lex;

        } catch (IOException | ParserConfigurationException | SAXException ex) {
            LOGGER.error("Lens Exception : " + ex);
            lensInstanceManager.closeSession(lensSession);
            throw new LensException(ErrorIndex.INTERNAL_ERR);

        }
        return outMessage;
    }

    /**
     * Method to get the Locale from the document and Tag the document with
     * respective Lens Instance
     *
     * @param data <code>byte[]</code> data to be tagged
     * @param docType <code>String</code> Document type (DOC|PDF|RTF|TXT)
     * @param type <code>char</code> R or P
     * @param locale <code>locale</code> Locale information
     * @return outMessage <code>LensMessage</code>
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     */
    @Override
    public LensMessage tagBinaryData(byte[] data, String docType, char type, String locale) throws LensException, ServerBusyException {
//        LensMessage outMessage = null;
//        LensSession lensSession = null;
//        try {
//            lensSession = lensInstanceManager.getLensInstance(locale);
//            outMessage = lensSession.tagBinaryData(data, docType, type);
//            String messageData = outMessage.getMessageData();
//            if (type == resumeType) {
//                messageData = messageData.replaceFirst("</ResDoc>", "<special><locale>" + locale + "</locale></special></ResDoc>");
//            } else {
//                messageData = messageData.replaceFirst("</JobDoc>", "<special><locale>" + locale + "</locale></special></JobDoc>");
//            }
//            outMessage = LensMessage.create(messageData, LensMessage.XML_TYPE);
//            lensInstanceManager.closeSession(lensSession);
//        } catch (LensException | ServerBusyException lex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + lex);
//            throw lex;
//        } catch (UnsupportedEncodingException ex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + ex);
//            throw new LensException(ErrorIndex.INTERNAL_ERR);
//
//        }
//        return outMessage;
        return null;
    }

    /**
     * Method to get the Locale from the document and TagWithRTF the document
     * with respective Lens Instance
     *
     * @param data <code>byte[]</code> data to be tagged
     * @param docType <code>String</code> Document type (DOC|PDF|RTF|TXT)
     * @param type <code>char</code> R or P
     * @return outMessage <code>LensMessage</code>
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     */
    @Override
    public LensMessage tagBinaryDataWithRTF(byte[] data, String docType, char type) throws LensException, ServerBusyException {
//        LensMessage localeMessage = null;
//        LensMessage outMessage = null;
//        LensSession lensSession = null;
//        try {
//            String lensResult = null;
//            localeMessage = lensLDParseData.processLocale(data, docType, type, lensInstanceManager);
//            lensResult = localeMessage.getMessageData();
//            // Added to check whether the output of Locale is error. If error, then return it as error.
//            if (!_helper.checkLensResultForFailure(lensResult)) {
//                return localeMessage;
//            }
//            String locale = lensInstanceManager.getLocale(localeMessage.getMessageData());
//            String messageData = lensLDParseData.tagDocument(data, docType, type, locale, VariantType.rtf, lensInstanceManager);
//            if (type == resumeType) {
//                messageData = messageData.replaceFirst("</ResDoc>", "<special><locale>" + locale + "</locale></special></ResDoc>");
//            } else {
//                messageData = messageData.replaceFirst("</JobDoc>", "<special><locale>" + locale + "</locale></special></JobDoc>");
//            }
//            outMessage = LensMessage.create(messageData, LensMessage.XML_TYPE);
//            lensInstanceManager.closeSession(lensSession);
//        } catch (LensException | ServerBusyException lex) {
//            LOGGER.error("Lens Exception : " + lex);
//            lensInstanceManager.closeSession(lensSession);
//            throw lex;
//
//        } catch (IOException | ParserConfigurationException | SAXException ex) {
//            LOGGER.error("Lens Exception : " + ex);
//            lensInstanceManager.closeSession(lensSession);
//            throw new LensException(ErrorIndex.INTERNAL_ERR);
//
//        }
//        return outMessage;
        return null;
    }

    public LensMessage tagBinaryDataWithRTF(byte[] data, String docType, char type, List<CoreLenssettings> lensSettings) throws LensException, ServerBusyException {
        LensMessage localeMessage = null;
        LensMessage outMessage = null;
        LensSession lensSession = null;

        try {
            String lensResult = null;
            ConcurrentHashMap lensDetails = generateLensSettingsHashMap(lensSettings);
            localeMessage = lensLDParseData.processLocale(data, docType, type, lensInstanceManager, lensDetails);
            lensResult = localeMessage.getMessageData();
            // Added to check whether the output of Locale is error. If error, then return it as error.
            if (!_helper.checkLensResultForFailure(lensResult)) {
                return localeMessage;
            }
            String locale = lensInstanceManager.getLocale(localeMessage.getMessageData());
            String messageData = lensLDParseData.tagDocument(data, docType, type, locale, VariantType.rtf, lensInstanceManager, lensDetails);
            if (type == resumeType) {
                messageData = replaceLast(messageData, "</ResDoc>", "<special><locale>" + locale + "</locale></special></ResDoc>");
            } else {
                messageData = replaceLast(messageData, "</JobDoc>", "<special><locale>" + locale + "</locale></special></JobDoc>");
            }
            outMessage = LensMessage.create(messageData, LensMessage.XML_TYPE);
            lensInstanceManager.closeSession(lensSession);
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            lensInstanceManager.closeSession(lensSession);
            throw lex;

        } catch (IOException | ParserConfigurationException | SAXException ex) {
            LOGGER.error("Lens Exception : " + ex);
            lensInstanceManager.closeSession(lensSession);
            throw new LensException(ErrorIndex.INTERNAL_ERR);

        }
        return outMessage;
    }

    /**
     * Method to get the Locale from the document and TagWithHTM the document
     * with respective Lens Instance
     *
     * @param data <code>byte[]</code> data to be tagged
     * @param docType <code>String</code> Document type (DOC|PDF|RTF|TXT)
     * @param type <code>char</code> R or P
     * @return outMessage <code>LensMessage</code>
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     */
    @Override
    public LensMessage tagBinaryDataWithHTM(byte[] data, String docType, char type) throws LensException, ServerBusyException {
//        LensMessage localeMessage = null;
//        LensMessage outMessage = null;
//        LensSession lensSession = null;
//        try {
//            String lensResult = null;
//            localeMessage = lensLDParseData.processLocale(data, docType, type, lensInstanceManager);
//            lensResult = localeMessage.getMessageData();
//            // Added to check whether the output of Locale is error. If error, then return it as error.
//            if (!_helper.checkLensResultForFailure(lensResult)) {
//                return localeMessage;
//            }
//            String locale = lensInstanceManager.getLocale(localeMessage.getMessageData());
//            String messageData = lensLDParseData.tagDocument(data, docType, type, locale, VariantType.htm, lensInstanceManager);
//            if (type == resumeType) {
//                messageData = messageData.replaceFirst("</ResDoc>", "<special><locale>" + locale + "</locale></special></ResDoc>");
//            } else {
//                messageData = messageData.replaceFirst("</JobDoc>", "<special><locale>" + locale + "</locale></special></JobDoc>");
//            }
//            outMessage = LensMessage.create(messageData, LensMessage.XML_TYPE);
//            lensInstanceManager.closeSession(lensSession);
//        } catch (LensException | ServerBusyException lex) {
//            LOGGER.error("Lens Exception : " + lex);
//            lensInstanceManager.closeSession(lensSession);
//            throw lex;
//
//        } catch (IOException | ParserConfigurationException | SAXException ex) {
//            LOGGER.error("Lens Exception : " + ex);
//            lensInstanceManager.closeSession(lensSession);
//            throw new LensException(ErrorIndex.INTERNAL_ERR);
//
//        }
//        return outMessage;
        return null;
    }

    public LensMessage tagBinaryDataWithHTM(byte[] data, String docType, char type, List<CoreLenssettings> lensSettings) throws LensException, ServerBusyException {
        LensMessage localeMessage = null;
        LensMessage outMessage = null;
        LensSession lensSession = null;

        try {
            String lensResult = null;
            ConcurrentHashMap lensDetails = generateLensSettingsHashMap(lensSettings);
            localeMessage = lensLDParseData.processLocale(data, docType, type, lensInstanceManager, lensDetails);
            lensResult = localeMessage.getMessageData();
            // Added to check whether the output of Locale is error. If error, then return it as error.
            if (!_helper.checkLensResultForFailure(lensResult)) {
                return localeMessage;
            }
            String locale = lensInstanceManager.getLocale(localeMessage.getMessageData());
            String messageData = lensLDParseData.tagDocument(data, docType, type, locale, VariantType.htm, lensInstanceManager, lensDetails);
            if (type == resumeType) {
                messageData = replaceLast(messageData, "</ResDoc>", "<special><locale>" + locale + "</locale></special></ResDoc>");
            } else {
                messageData = replaceLast(messageData, "</JobDoc>", "<special><locale>" + locale + "</locale></special></JobDoc>");
            }
            outMessage = LensMessage.create(messageData, LensMessage.XML_TYPE);
            lensInstanceManager.closeSession(lensSession);
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            lensInstanceManager.closeSession(lensSession);
            throw lex;

        } catch (IOException | ParserConfigurationException | SAXException ex) {
            LOGGER.error("Lens Exception : " + ex);
            lensInstanceManager.closeSession(lensSession);
            throw new LensException(ErrorIndex.INTERNAL_ERR);

        }
        return outMessage;
    }

    /**
     * Method to tag a plain text file into BGT XML.
     *
     * @param file <code>File</code> a valid O/S file object.
     * @param type <code>char</code> either resume or posting
     * @return <code>LensMessage</code> result
     * @throws LensException if any communication error, timeout occurs
     * @throws com.bgt.lens.ServerBusyException
     * @see LensMessage
     * @see LensException
     * @see LensSession#RESUME_TYPE
     * @see LensSession#POSTING_TYPE
     */
    @Override
    public LensMessage tagFile(File file, char type) throws LensException, ServerBusyException {
        try {

            if (!file.exists()) {
                throw new LensException(ErrorIndex.FILE_NOT_EXISTS);
            }
            String docType = "";
            int extIndex = file.getName().lastIndexOf(".");
            if (extIndex != -1) {
                docType = file.getName().substring(extIndex, file.getName().length());
            }

            byte[] data = readFile(file);

            return tagBinaryData(data, docType, type);
        } catch (ServerBusyException | LensException sbx) {
            LOGGER.error("Lens Exception : " + sbx);
            throw sbx;
        } catch (IOException iex) {
            LOGGER.error("Lens Exception : " + iex);
            throw new LensException(ErrorIndex.IO_READ_ERROR, iex);
        }
    }

    /**
     * Method to tag a plain text file into BGT XML.
     *
     * @param file <code>File</code> a valid O/S file object.
     * @param type <code>char</code> either resume or posting
     * @param locale <code>String</code> locale information
     * @return <code>LensMessage</code> result
     * @throws LensException if any communication error, timeout occurs
     * @throws com.bgt.lens.ServerBusyException
     * @see LensMessage
     * @see LensException
     * @see LensSession#RESUME_TYPE
     * @see LensSession#POSTING_TYPE
     */
    @Override
    public LensMessage tagFile(File file, char type, String locale) throws LensException, ServerBusyException {
        try {

            if (!file.exists()) {
                throw new LensException(ErrorIndex.FILE_NOT_EXISTS);
            }
            String docType = "";
            int extIndex = file.getName().lastIndexOf(".");
            if (extIndex != -1) {
                docType = file.getName().substring(extIndex, file.getName().length());
            }

            byte[] data = readFile(file);

            return tagBinaryData(data, docType, type, locale);
        } catch (ServerBusyException | LensException sbx) {
            LOGGER.error("Lens Exception : " + sbx);
            throw sbx;
        } catch (IOException iex) {
            LOGGER.error("Lens Exception : " + iex);
            throw new LensException(ErrorIndex.IO_READ_ERROR, iex);
        }
    }

    /**
     * Method to send the BGT XML to Lens server
     *
     * @param message <code>LensMessage</code>
     * @param timeout <code>long</code>
     * @return <code>LensMessage</code>
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     */
    @Override
    public LensMessage sendMessage(LensMessage message, long timeout) throws LensException, ServerBusyException {
//        LensMessage outMessage = null;
//        LensSession lensSession = null;
//        try {
//            // To support Locale command.
//            if (message.getType() == LensMessage.LOCALE_TYPE) {
//                lensSession = lensInstanceManager.getLocaleInstance();
//                outMessage = lensSession.sendMessage(message, timeout);
//                lensInstanceManager.closeSession(lensSession);
//            } else {
//                DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
//                dbfac.setCoalescing(true);
//                DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
//                Document doc = docBuilder.parse(new InputSource(new StringReader(message.getMessageData())));
//                NodeList nodeList = doc.getElementsByTagName("bgtcmd");
//                for (int s = 0; s < nodeList.getLength(); s++) {
//                    String value = "";
//                    Node locNode = nodeList.item(s);
//
//                    if (locNode.getNodeType() == Node.ELEMENT_NODE) {
//                        Element locElmnt = (Element) locNode;
//
//                        NodeList hostElmntLst = locElmnt.getElementsByTagName("tag");
//                        NodeList hostElmntLst1 = locElmnt.getElementsByTagName("canon");
//                        NodeList hostElmntLst2 = locElmnt.getElementsByTagName("info");
//                        NodeList hostElmntLst3 = locElmnt.getElementsByTagName("locale");
//
//                        if (hostElmntLst.getLength() > 0) {
//                            Element hostElmnt = (Element) hostElmntLst.item(0);
//
//                            Node child = hostElmnt.getFirstChild();
//                            if (child instanceof CharacterData) {
//
//                                CharacterData cd = (CharacterData) child;
//                                value = cd.getData();
//                            }
//                            return tagBinaryData(value.getBytes("utf-8"), "txt", RESUME_TYPE);
//                        } else if (hostElmntLst1.getLength() > 0) {
//                            // Modified to get canon element from canon command.
//                            // hostElmntLst1 = locElmnt.getElementsByTagName("locale");
//                            hostElmntLst1 = locElmnt.getElementsByTagName("canon");
//                            Element hostElmnt = (Element) hostElmntLst1.item(0);
//                            NodeList host = hostElmnt.getChildNodes();
//                            value = (((Node) host.item(0)).getNodeValue());
//                            if (value == null) {
//                                throw new LensException(ErrorIndex.XML_COMMAND_MISSING_LOCALE);
//                            }
//                            // Modified to create session with default LD instance.
//                            //LensSession lensSession = LensInstanceManager.getLensInstance(value);
//                            lensSession = lensInstanceManager.getLocaleInstance();
//
//                            outMessage = lensSession.sendMessage(message, timeout);
//
//                            lensInstanceManager.closeSession(lensSession);
//
//                        } else if (hostElmntLst2.getLength() > 0) {
//                            return getInfo();
//                            // To support Locale command.
//                        } else if (hostElmntLst3.getLength() > 0) {
//                            hostElmntLst3 = locElmnt.getElementsByTagName("locale");
//                            Element hostElmnt = (Element) hostElmntLst3.item(0);
//                            NodeList host = hostElmnt.getChildNodes();
//                            value = (((Node) host.item(0)).getNodeValue());
//                            if (value == null) {
//                                throw new LensException(ErrorIndex.XML_COMMAND_MISSING_LOCALE);
//                            }
//                            lensSession = lensInstanceManager.getLocaleInstance();
//
//                            outMessage = lensSession.sendMessage(message, timeout);
//
//                            lensInstanceManager.closeSession(lensSession);
//                        } else {
//                            throw new LensException(ErrorIndex.COMMAND_NOT_SUPPORTED);
//                        }
//                    }
//
//                }
//            }
//        } catch (SAXException | ParserConfigurationException sxex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + sxex);
//            throw new LensException(ErrorIndex.XML_PARSER_ERROR, sxex);
//        } catch (IOException ex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + ex);
//            throw new LensException(ErrorIndex.IO_READ_ERROR, ex);
//        } catch (LensException lex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + lex);
//            throw lex;
//        } catch (ServerBusyException | DOMException ex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + ex);
//            throw new LensException(ErrorIndex.INTERNAL_ERR, ex);
//        }
//        return outMessage;
        return null;
    }

    public LensMessage sendMessage(LensMessage message, long timeout, List<CoreLenssettings> lensSettings) throws LensException, ServerBusyException {
        LensMessage outMessage = null;
        LensSession lensSession = null;

        ConcurrentHashMap lensDetails = generateLensSettingsHashMap(lensSettings);
        try {
            // To support Locale command.
            if (message.getType() == LensMessage.LOCALE_TYPE) {
                lensSession = lensInstanceManager.getLocaleInstance(lensDetails);
                outMessage = lensSession.sendMessage(message, timeout);
                lensInstanceManager.closeSession(lensSession);
            } else {
                DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
                dbfac.setCoalescing(true);
                DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
                Document doc = docBuilder.parse(new InputSource(new StringReader(message.getMessageData())));
                NodeList nodeList = doc.getElementsByTagName("bgtcmd");
                for (int s = 0; s < nodeList.getLength(); s++) {
                    String value = "";
                    Node locNode = nodeList.item(s);

                    if (locNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element locElmnt = (Element) locNode;

                        NodeList hostElmntLst = locElmnt.getElementsByTagName("tag");
                        NodeList hostElmntLst1 = locElmnt.getElementsByTagName("canon");
                        NodeList hostElmntLst2 = locElmnt.getElementsByTagName("info");
                        NodeList hostElmntLst3 = locElmnt.getElementsByTagName("locale");

                        if (hostElmntLst.getLength() > 0) {
                            Element hostElmnt = (Element) hostElmntLst.item(0);

                            Node child = hostElmnt.getFirstChild();
                            if (child instanceof CharacterData) {

                                CharacterData cd = (CharacterData) child;
                                value = cd.getData();
                            }
                            return tagBinaryData(value.getBytes("utf-8"), "txt", RESUME_TYPE);
                        } else if (hostElmntLst1.getLength() > 0) {
                            // Modified to get canon element from canon command.
                            // hostElmntLst1 = locElmnt.getElementsByTagName("locale");
                            hostElmntLst1 = locElmnt.getElementsByTagName("canon");
                            Element hostElmnt = (Element) hostElmntLst1.item(0);
                            NodeList host = hostElmnt.getChildNodes();
                            value = (((Node) host.item(0)).getNodeValue());
                            if (value == null) {
                                throw new LensException(ErrorIndex.XML_COMMAND_MISSING_LOCALE);
                            }
                            // Modified to create session with default LD instance.
                            //LensSession lensSession = LensInstanceManager.getLensInstance(value);
                            lensSession = lensInstanceManager.getLocaleInstance(lensDetails);

                            outMessage = lensSession.sendMessage(message, timeout);

                            lensInstanceManager.closeSession(lensSession);

                        } else if (hostElmntLst2.getLength() > 0) {
                            return getInfo();
                            // To support Locale command.
                        } else if (hostElmntLst3.getLength() > 0) {
                            hostElmntLst3 = locElmnt.getElementsByTagName("locale");
                            Element hostElmnt = (Element) hostElmntLst3.item(0);
                            NodeList host = hostElmnt.getChildNodes();
                            value = (((Node) host.item(0)).getNodeValue());
                            if (value == null) {
                                throw new LensException(ErrorIndex.XML_COMMAND_MISSING_LOCALE);
                            }
                            lensSession = lensInstanceManager.getLocaleInstance(lensDetails);

                            outMessage = lensSession.sendMessage(message, timeout);

                            lensInstanceManager.closeSession(lensSession);
                        } else {
                            throw new LensException(ErrorIndex.COMMAND_NOT_SUPPORTED);
                        }
                    }

                }
            }
        } catch (SAXException | ParserConfigurationException sxex) {
            lensInstanceManager.closeSession(lensSession);
            LOGGER.error("Lens Exception : " + sxex);
            throw new LensException(ErrorIndex.XML_PARSER_ERROR, sxex);
        } catch (IOException ex) {
            lensInstanceManager.closeSession(lensSession);
            LOGGER.error("Lens Exception : " + ex);
            throw new LensException(ErrorIndex.IO_READ_ERROR, ex);
        } catch (LensException lex) {
            lensInstanceManager.closeSession(lensSession);
            LOGGER.error("Lens Exception : " + lex);
            throw lex;
        } catch (ServerBusyException | DOMException ex) {
            lensInstanceManager.closeSession(lensSession);
            LOGGER.error("Lens Exception : " + ex);
            throw new LensException(ErrorIndex.INTERNAL_ERR, ex);
        }
        return outMessage;
    }

    /**
     * Method to send the BGT XML to Lens server
     *
     * @param message <code>LensMessage</code>
     * @param timeout <code>long</code>
     * @param locale <code>locale</code> Locale information
     * @return <code>LensMessage</code>
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     */
    @Override
    public LensMessage sendMessage(LensMessage message, long timeout, String locale) throws LensException, ServerBusyException {
//        LensMessage outMessage = null;
//        LensSession lensSession = null;
//        try {
//
//            DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
//            dbfac.setCoalescing(true);
//            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
//            Document doc = docBuilder.parse(new InputSource(new StringReader(message.getMessageData())));
//            NodeList nodeList = doc.getElementsByTagName("bgtcmd");
//            for (int s = 0; s < nodeList.getLength(); s++) {
//                String value = "";
//                Node locNode = nodeList.item(s);
//
//                if (locNode.getNodeType() == Node.ELEMENT_NODE) {
//                    Element locElmnt = (Element) locNode;
//
//                    NodeList hostElmntLst = locElmnt.getElementsByTagName("tag");
//                    NodeList hostElmntLst1 = locElmnt.getElementsByTagName("canon");
//                    NodeList hostElmntLst2 = locElmnt.getElementsByTagName("info");
//                    NodeList hostElmntLst3 = locElmnt.getElementsByTagName("locale");
//
//                    if (hostElmntLst.getLength() > 0) {
//                        Element hostElmnt = (Element) hostElmntLst.item(0);
//
//                        Node child = hostElmnt.getFirstChild();
//                        if (child instanceof CharacterData) {
//
//                            CharacterData cd = (CharacterData) child;
//                            value = cd.getData();
//                        }
//                        return tagBinaryData(value.getBytes("utf-8"), "txt", RESUME_TYPE, locale);
//                    } else if (hostElmntLst1.getLength() > 0) {
//                        hostElmntLst1 = locElmnt.getElementsByTagName("canon");
//                        Element hostElmnt = (Element) hostElmntLst1.item(0);
//                        NodeList host = hostElmnt.getChildNodes();
//                        value = (((Node) host.item(0)).getNodeValue());
//                        if (value == null) {
//                            throw new LensException(ErrorIndex.XML_COMMAND_MISSING_LOCALE);
//                        }
//                        lensSession = lensInstanceManager.getLensInstance(locale);
//
//                        outMessage = lensSession.sendMessage(message, timeout);
//
//                        lensInstanceManager.closeSession(lensSession);
//
//                    } else if (hostElmntLst2.getLength() > 0) {
//                        return getInfo();
//                    } else if (hostElmntLst3.getLength() > 0) {
//                        hostElmntLst3 = locElmnt.getElementsByTagName("locale");
//                        Element hostElmnt = (Element) hostElmntLst3.item(0);
//                        NodeList host = hostElmnt.getChildNodes();
//                        value = (((Node) host.item(0)).getNodeValue());
//                        if (value == null) {
//                            throw new LensException(ErrorIndex.XML_COMMAND_MISSING_LOCALE);
//                        }
//                        lensSession = lensInstanceManager.getLocaleInstance();
//
//                        outMessage = lensSession.sendMessage(message, timeout);
//
//                        lensInstanceManager.closeSession(lensSession);
//                    } else {
//                        throw new LensException(ErrorIndex.COMMAND_NOT_SUPPORTED);
//                    }
//                }
//
//            }
//
//        } catch (SAXException | ParserConfigurationException sxex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + sxex);
//            throw new LensException(ErrorIndex.XML_PARSER_ERROR, sxex);
//        } catch (IOException ex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + ex);
//            throw new LensException(ErrorIndex.IO_READ_ERROR, ex);
//        } catch (LensException lex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + lex);
//            throw lex;
//        } catch (ServerBusyException | DOMException ex) {
//            lensInstanceManager.closeSession(lensSession);
//            LOGGER.error("Lens Exception : " + ex);
//            throw new LensException(ErrorIndex.INTERNAL_ERR, ex);
//        }
//        return outMessage;
        return null;
    }

    public LensMessage sendMessage(LensMessage message, long timeout, String locale, List<CoreLenssettings> lensSettings) throws LensException, ServerBusyException {
        LensMessage outMessage = null;
        LensSession lensSession = null;

        ConcurrentHashMap lensDetails = generateLensSettingsHashMap(lensSettings);
        try {

            DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
            dbfac.setCoalescing(true);
            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
            Document doc = docBuilder.parse(new InputSource(new StringReader(message.getMessageData())));
            NodeList nodeList = doc.getElementsByTagName("bgtcmd");
            for (int s = 0; s < nodeList.getLength(); s++) {
                String value = "";
                Node locNode = nodeList.item(s);

                if (locNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element locElmnt = (Element) locNode;

                    NodeList hostElmntLst = locElmnt.getElementsByTagName("tag");
                    NodeList hostElmntLst1 = locElmnt.getElementsByTagName("canon");
                    NodeList hostElmntLst2 = locElmnt.getElementsByTagName("info");
                    NodeList hostElmntLst3 = locElmnt.getElementsByTagName("locale");

                    if (hostElmntLst.getLength() > 0) {
                        Element hostElmnt = (Element) hostElmntLst.item(0);

                        Node child = hostElmnt.getFirstChild();
                        if (child instanceof CharacterData) {

                            CharacterData cd = (CharacterData) child;
                            value = cd.getData();
                        }
                        return tagBinaryData(value.getBytes("utf-8"), "txt", RESUME_TYPE, locale);
                    } else if (hostElmntLst1.getLength() > 0) {
                        hostElmntLst1 = locElmnt.getElementsByTagName("canon");
                        Element hostElmnt = (Element) hostElmntLst1.item(0);
                        NodeList host = hostElmnt.getChildNodes();
                        value = (((Node) host.item(0)).getNodeValue());
                        if (value == null) {
                            throw new LensException(ErrorIndex.XML_COMMAND_MISSING_LOCALE);
                        }
                        lensSession = lensInstanceManager.getLensInstance(locale, lensDetails);

                        outMessage = lensSession.sendMessage(message, timeout);

                        lensInstanceManager.closeSession(lensSession);

                    } else if (hostElmntLst2.getLength() > 0) {
                        return getInfo();
                    } else if (hostElmntLst3.getLength() > 0) {
                        hostElmntLst3 = locElmnt.getElementsByTagName("locale");
                        Element hostElmnt = (Element) hostElmntLst3.item(0);
                        NodeList host = hostElmnt.getChildNodes();
                        value = (((Node) host.item(0)).getNodeValue());
                        if (value == null) {
                            throw new LensException(ErrorIndex.XML_COMMAND_MISSING_LOCALE);
                        }
                        lensSession = lensInstanceManager.getLocaleInstance(lensDetails);

                        outMessage = lensSession.sendMessage(message, timeout);

                        lensInstanceManager.closeSession(lensSession);
                    } else {
                        throw new LensException(ErrorIndex.COMMAND_NOT_SUPPORTED);
                    }
                }

            }

        } catch (SAXException | ParserConfigurationException sxex) {
            lensInstanceManager.closeSession(lensSession);
            LOGGER.error("Lens Exception : " + sxex);
            throw new LensException(ErrorIndex.XML_PARSER_ERROR, sxex);
        } catch (IOException ex) {
            lensInstanceManager.closeSession(lensSession);
            LOGGER.error("Lens Exception : " + ex);
            throw new LensException(ErrorIndex.IO_READ_ERROR, ex);
        } catch (LensException lex) {
            lensInstanceManager.closeSession(lensSession);
            LOGGER.error("Lens Exception : " + lex);
            throw lex;
        } catch (ServerBusyException | DOMException ex) {
            lensInstanceManager.closeSession(lensSession);
            LOGGER.error("Lens Exception : " + ex);
            throw new LensException(ErrorIndex.INTERNAL_ERR, ex);
        }
        return outMessage;
    }

    /**
     * Method to get info of all the Lens services running.
     *
     * @return <code>LensMessage</code>
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     */
    @Override
    public LensMessage getInfo() throws LensException, ServerBusyException {
//        String info = null;
//        try {
//            info = lensInstanceManager.getLensInstanceInfo();
//        } catch (LensException | ServerBusyException lex) {
//            LOGGER.error("Lens Exception : " + lex);
//            throw lex;
//        } catch (Exception ex) {
//            LOGGER.error("Lens Exception : " + ex);
//            throw new LensException(ErrorIndex.INTERNAL_ERR);
//        }
//        LensMessage tempMsg = null;
//        try {
//
//            tempMsg = LensMessage.create(info, LensMessage.XML_TYPE);
//        } catch (UnsupportedEncodingException ex) {
//            LOGGER.error("Lens Exception : " + ex);
//        }
//        return tempMsg;
        return null;

    }

    public LensMessage getInfo(List<CoreLenssettings> lensSettings) throws LensException, ServerBusyException {
        String info = null;

        try {
            info = lensInstanceManager.getLensInstanceInfo(lensSettings);
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            throw lex;
        } catch (Exception ex) {
            LOGGER.error("Lens Exception : " + ex);
            throw new LensException(ErrorIndex.INTERNAL_ERR);
        }
        LensMessage tempMsg = null;
        try {

            tempMsg = LensMessage.create(info, LensMessage.XML_TYPE);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.error("Lens Exception : " + ex);
        }
        return tempMsg;

    }

    /**
     * Method to close the LensSession
     *
     * @throws com.bgt.lens.LensException
     */
    @Override
    public void close() throws LensException {
        // ToDo: need to check this
    }

    /**
     * Method to read the entire content of a file into a byte array
     *
     * @param file <code>File</code> a valid O/S file object
     * @return <code>byte[]</code> content of the file
     * @throws IOException
     */
    private byte[] readFile(File file) throws IOException {
        FileInputStream fin = new FileInputStream(file);
        byte[] data = new byte[fin.available()];
        fin.read(data);
        return data;
    }

    /**
     * Method to send the document to convert into Unicode
     *
     * @param data <code>byte[]</code> Binary document to convert
     * @param docType <code>String</code> Document type (txt, doc, pdf)
     * @return convMessage <code>LensMessage</code> converted Message
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     * @throws java.lang.Exception
     */
    private LensMessage convertToUnicode(byte[] data, String docType, List<CoreLenssettings> lensSettings) throws LensException, ServerBusyException, Exception {
        LensMessage convMessage = null;
        LensSession unicodeSession = null;

        try {
            unicodeSession = lensInstanceManager.getConvertInstance(lensSettings);
            convMessage = unicodeSession.convertBinaryData(data, docType);
            lensInstanceManager.closeSession(unicodeSession);
        } catch (LensException | ServerBusyException lex) {
            lensInstanceManager.closeSession(unicodeSession);
            LOGGER.error("Lens Exception : " + lex);
            throw lex;

        }
        return convMessage;
    }

    /**
     * Method used to replace invalid XML Characters to valid XML Characters
     *
     * @param convMessageText <code>String</code> converted message with invalid
     * XML character
     * @return convMessageText <code>String</code> converted message with
     * replaced valid XMl character
     */
    private String refineXML(String convMessageText) {
        convMessageText = convMessageText.replaceAll("&", "&amp;");
        convMessageText = convMessageText.replaceAll("<", "&lt;");
        convMessageText = convMessageText.replaceAll(">", "&gt;");
        return convMessageText;
    }

    /**
     * This method ensures that the output String has only valid XML unicode
     * characters as specified by the XML 1.0 standard. For reference, please
     * see <a href="http://www.w3.org/TR/2000/REC-xml-20001006#NT-Char">the
     * standard</a>. This method will return an empty String if the input is
     * null or empty.
     *
     * @param in
     * @return The in String, stripped of invalid characters.
     */
    public String stripInValidXMLCharacters(String in) {
        StringBuilder out = new StringBuilder(); // Used to hold the output.

        int codePoint; // Used to reference the current character.
        // String ss = "\ud801\udc00"; // This is actualy one unicode character,
        // represented by two code units!!!.

        // System.out.println(ss.codePointCount(0, ss.length()));// See: 1
        int i = 0;
        while (i < in.length()) {
            codePoint = in.codePointAt(i); // This is the unicode code of the character
            //System.out.println("(" + i + "," + codePoint + ")");

            if ((codePoint == 0x9)
                    || // Consider testing larger ranges first to // improve speed.
                    (codePoint == 0xA)
                    || (codePoint == 0xD)
                    || ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
                    || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF))) {
                out.append(Character.toChars(codePoint));
            }
            i += Character.charCount(codePoint); // Increment with the number of
            // code units(java chars)
            // needed to represent a
            // Unicode char.

        }
        return out.toString();
    }

    /**
     * Method to generate LensSetting HashMap
     *
     * @param lenssettingses
     * @return
     */
    public ConcurrentHashMap generateLensSettingsHashMap(List<CoreLenssettings> lenssettingses) {
        ConcurrentHashMap lensDetails = new ConcurrentHashMap();
        try {
            String hashKey = null;
            if (lenssettingses != null && lenssettingses.size() > 0) {
                String hashValue = null;
                for (CoreLenssettings coreLenssettings : lenssettingses) {
                    // HaskKey as Locale
                    // HashValue as hostname,port,encoding,timout,locale,instanceType
                    if (coreLenssettings.getInstanceType().equalsIgnoreCase(com.bgt.lens.helpers.Enum.InstanceType.LD.name())) {
                        hashKey = com.bgt.lens.helpers.Enum.InstanceType.LD.name();
                        hashValue = (coreLenssettings.getHost()) + ","
                                + (coreLenssettings.getPort()) + ","
                                + (coreLenssettings.getCharacterSet()) + ","
                                + (coreLenssettings.getTimeout()) + ","
                                + com.bgt.lens.helpers.Enum.InstanceType.LD.name() + ","
                                + (coreLenssettings.getInstanceType());
                    } else if (!coreLenssettings.getLocale().contains(",")) {

                        String customSkillCode = null;
                        List<CoreClientlenssettings> coreClientlenssettingses = new ArrayList<>(coreLenssettings.getCoreClientlenssettingses());
                        if (coreClientlenssettingses.size() > 0) {
                            CoreCustomskillsettings coreCustomskillsettings = coreClientlenssettingses.get(0).getCoreCustomskillsettings();
                            if (_helper.isNotNull(coreCustomskillsettings)) {
                                customSkillCode = coreCustomskillsettings.getCustomSkillCode();
                            }
                        }

                        hashKey = coreLenssettings.getLocale();
                        hashValue = (coreLenssettings.getHost()) + ","
                                + (coreLenssettings.getPort()) + ","
                                + (coreLenssettings.getCharacterSet()) + ","
                                + (coreLenssettings.getTimeout()) + ","
                                + (coreLenssettings.getLocale()) + ","
                                + (coreLenssettings.getInstanceType()) + ","
                                + customSkillCode;
                    } else {
                        String customSkillCode = null;
                        List<CoreClientlenssettings> coreClientlenssettingses = new ArrayList<>(coreLenssettings.getCoreClientlenssettingses());
                        if (coreClientlenssettingses.size() > 0) {
                            CoreCustomskillsettings coreCustomskillsettings = coreClientlenssettingses.get(0).getCoreCustomskillsettings();
                            if (_helper.isNotNull(coreCustomskillsettings)) {
                                customSkillCode = coreCustomskillsettings.getCustomSkillCode();
                            }
                        }
                        hashKey = coreLenssettings.getLocale().split(",")[0];
                        hashValue = (coreLenssettings.getHost()) + ","
                                + (coreLenssettings.getPort()) + ","
                                + (coreLenssettings.getCharacterSet()) + ","
                                + (coreLenssettings.getTimeout()) + ","
                                + (coreLenssettings.getLocale().split(",")[1]) + ","
                                + (coreLenssettings.getInstanceType()) + ","
                                + customSkillCode;
                    }

                    lensDetails.put(hashKey, hashValue);

                }
                return lensDetails;
            }
            return null;
        } catch (Exception ex) {
            LOGGER.error("An exception occured while converting lens settings to hashmap : ", ex);
            return null;
        }
    }

    public String replaceLast(String text, String regex, String replacement) {
        return text.replaceFirst("(?s)" + regex + "(?!.*?" + regex + ")", replacement);
    }
}
