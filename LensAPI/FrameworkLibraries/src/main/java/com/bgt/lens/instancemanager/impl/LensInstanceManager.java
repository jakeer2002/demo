// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.instancemanager.impl;

import com.bgt.lens.*;
import com.bgt.lens.api.ApiContext;
import com.bgt.lens.helpers.Enum.InstanceType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.instancemanager.ILensInstanceManager;
import com.bgt.lens.ld.DBLensSettingsManager;
import com.bgt.lens.ld.ILensLDParseData;
import com.bgt.lens.model.entity.CoreLenssettings;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * LensInstanceManager is a class which maintains all the LensSession creation
 * of the instance present.
 */
public class LensInstanceManager implements ILensInstanceManager {

    @Autowired
    ILensLDParseData lensLDParseData;

    private static final Logger LOGGER = LogManager.getLogger(LensInstanceManager.class);

    private static final Helper _helper = new Helper();

    /**
     * Method to load LensConfig details
     *
     * @param lenssettingses
     * @return <code>boolean</code>
     * @throws com.bgt.lens.LensException
     */
    @Override
    public boolean loadConfigDetails(List<CoreLenssettings> lenssettingses) throws LensException {
        return true;
    }

    /**
     * Method to get the LensSession for converting document.
     *
     * @param lenssettingses
     * @return <code>LensSession</code> LensSession
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     * @see LensException
     * @see ServerBusyException
     */
    @Override
    public LensSession getConvertInstance(List<CoreLenssettings> lenssettingses) throws LensException, ServerBusyException {
        try {
            LensSession convertSession = null;
            ConcurrentHashMap lensDetails = new DBLensSettingsManager().generateLensSettingsHashMap(lenssettingses);
            if (lensDetails != null) {
                if (lensDetails.isEmpty()) {
                    throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
                }
            } else {
                throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
            }
            // To use LocaleDetector instance for converting. 
            if (!lensDetails.containsKey(InstanceType.LD.name())) {
                throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
            }
            String lensInstanceValue = (String) lensDetails.get(InstanceType.LD.name());
            String splitValue[] = splitValue(lensInstanceValue);
            String inHost = splitValue[0];
            int inPort = Integer.parseInt(splitValue[1]);
            String inEncoding = splitValue[2];
            int timeOut = Integer.parseInt(splitValue[3]);

            // <editor-fold desc="LENS Rate Limit">
            // Do LENS Rate Limiting
            // </editor-fold>
            convertSession = JLens.createSession(inHost, inPort, inEncoding);
            convertSession.open();
            convertSession.setEnableTransactionTimeout(true);
            convertSession.setTransactionTimeout((long) 1000 * (long) timeOut);
            return convertSession;
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            throw lex;
        } catch (NumberFormatException ex) {
            LOGGER.error("Lens Exception : " + ex);
            throw new LensException(ErrorIndex.INTERNAL_ERR);
        }

    }

    /**
     * Method to get the LensSession for LocaleDetector instance to find locale
     * for the document.
     *
     * @param lensDetails
     * @return <code>LensSession</code> locale LensSession
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     * @see LensException
     * @see ServerBusyException
     */
    @Override
    public LensSession getLocaleInstance(ConcurrentHashMap lensDetails) throws LensException, ServerBusyException {
        try {
            LensSession localeSession = null;
//            ConcurrentHashMap lensDetails = new DBLensSettingsManager().generateLensSettingsHashMap(lenssettingses);
            if (lensDetails != null) {
                if (lensDetails.isEmpty()) {
                    throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
                }
            } else {
                throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
            }
            // To use LocaleDetector instance for converting. 
            if (!lensDetails.containsKey(InstanceType.LD.name())) {
                throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
            }
            String lensInstanceValue = (String) lensDetails.get(InstanceType.LD.name());
            String splitValue[] = splitValue(lensInstanceValue);
            String inHost = splitValue[0];
            int inPort = Integer.parseInt(splitValue[1]);
            String inEncoding = splitValue[2];
            int timeOut = Integer.parseInt(splitValue[3]);
            LOGGER.info("LD Host:" + inHost + "  Port: " + inPort + " CharacterSet: " + inEncoding + " TimeOut: " + timeOut);

            // <editor-fold desc="LENS Rate Limit">
            String localeName = splitValue[4];
            String instanceType = splitValue[5];
            // LENS Rate Limiting
            ApiContext apiContext = _helper.getApiContext();
            if (_helper.isNotNull(apiContext.getClient())) {
                Integer clientId = apiContext.getClient().getId();
                lensLDParseData.ldParsingCheckRateLimit(clientId, inHost, inPort, localeName, instanceType);
            }
            // </editor-fold>

            localeSession = JLens.createSession(inHost, inPort, inEncoding);
            localeSession.open();
            localeSession.setEnableTransactionTimeout(true);
            localeSession.setTransactionTimeout((long) 1000 * (long) timeOut);
            return localeSession;
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            throw lex;  
        } catch (NumberFormatException ex) {
            LOGGER.error("Lens Exception : " + ex);
            throw new LensException(ErrorIndex.INTERNAL_ERR);
        }
    }

    @Override
    public String getDefaultLocaleByLocale(String locale, List<CoreLenssettings> lenssettingses) throws LensException {
        String[] localeObj = locale.split("_");
        String languageInstance = localeObj[0] + "_*";
        String defaultLocale = "*_*";
        String updatedLocale = locale;

        ConcurrentHashMap lensDetails = new DBLensSettingsManager().generateLensSettingsHashMap(lenssettingses);
        if (lensDetails != null) {
            if (lensDetails.isEmpty()) {
                throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
            }
        } else {
            throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
        }
        // To use LocaleDetector instance for converting. 
        if (!lensDetails.containsKey(InstanceType.LD.name())) {
            throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
        }

        // ToDo : Need to Added support default instance *_*
        //String defaultInstance = "*_*";
        String lensInstanceValue = null;
        if (lensDetails.containsKey(locale)) {
            updatedLocale = locale;
        } else if (lensDetails.containsKey(languageInstance)) {
            updatedLocale = languageInstance;
        } else if (lensDetails.containsKey(defaultLocale)) {
            updatedLocale = defaultLocale;
        }
        return updatedLocale;
    }

    /**
     * Method to get the LensSession for Lens/XRay instance to find tag the
     * document.
     *
     * @param locale <code>String</code> locale code
     * @param lensDetails
     * @return <code>LensSession</code> locale LensSession
     * @throws com.bgt.lens.LensException
     * @throws com.bgt.lens.ServerBusyException
     * @see LensException
     * @see ServerBusyException
     */
    @Override
    public LensSession getLensInstance(String locale, ConcurrentHashMap lensDetails) throws LensException, ServerBusyException {
        try {
            LensSession lensSession = null;
            String[] localeObj = locale.split("_");
            String languageInstance = localeObj[0] + "_*";
            String defaultLocale = "*_*";

            if (lensDetails != null) {
                if (lensDetails.isEmpty()) {
                    throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
                }
            } else {
                throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
            }

            // ToDo : Need to Add support for default instance *_*
            //String defaultInstance = "*_*";
            String lensInstanceValue = null;
            if (lensDetails.containsKey(locale)) {
                lensInstanceValue = (String) lensDetails.get(locale);
            } else if (lensDetails.containsKey(languageInstance)) {
                lensInstanceValue = (String) lensDetails.get(languageInstance);
            } else if (lensDetails.containsKey(defaultLocale)) {
                lensInstanceValue = (String) lensDetails.get(defaultLocale);
            } else {
                throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING, locale + " & " + languageInstance + " & " + defaultLocale);
            }

            String splitValue[] = splitValue(lensInstanceValue);
            String inHost = splitValue[0];
            int inPort = Integer.parseInt(splitValue[1]);
            String inEncoding = splitValue[2];
            int timeOut = Integer.parseInt(splitValue[3]);

            // LOG instance details
            LOGGER.info("Host:" + inHost + "  Port: " + inPort + " CharacterSet:" + inEncoding + " TimeOut" + timeOut);

            // <editor-fold desc="LENS Rate Limit">
            String localeName = splitValue[4];
            String instanceType = splitValue[5];
            // LENS Rate Limiting
            ApiContext apiContext = _helper.getApiContext();
            if (_helper.isNotNull(apiContext.getClient())) {
                Integer clientId = apiContext.getClient().getId();
                lensLDParseData.ldParsingCheckRateLimit(clientId, inHost, inPort, localeName, instanceType);
            }
            // </editor-fold>

            lensSession = JLens.createSession(inHost, inPort, inEncoding);
            lensSession.open();
            lensSession.setEnableTransactionTimeout(true);
            lensSession.setTransactionTimeout((long) 1000 * (long) timeOut);
            return lensSession;
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            throw lex;
        } catch (NumberFormatException ex) {
            LOGGER.error("Lens Exception : " + ex);
            throw new LensException(ErrorIndex.INTERNAL_ERR);
        }
    }

    /**
     * Method to get info about the services of all instance.
     *
     * @param lenssettingses
     * @return infoResult <code>String</code>
     * @throws java.lang.Exception
     * @see Exception
     */
    @Override
    public String getLensInstanceInfo(List<CoreLenssettings> lenssettingses) throws Exception {
        try {

            DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            // create the root element and add it to the document
            Element root = doc.createElement("lensinstance");
            doc.appendChild(root);

            ConcurrentHashMap lensDetails = new DBLensSettingsManager().generateLensSettingsHashMap(lenssettingses);
            if (lensDetails != null) {
                if (lensDetails.isEmpty()) {
                    throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
                }
            } else {
                throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
            }

            for (Enumeration e = lensDetails.keys(); e.hasMoreElements();) {
                LensSession session = null;
                String currentKey = e.nextElement().toString();
                String lensInstanceValue = (String) lensDetails.get(currentKey);
                String splitValue[] = splitValue(lensInstanceValue);
                String inHost = splitValue[0];
                int inPort = Integer.parseInt(splitValue[1]);
                String inEncoding = splitValue[2];
                int timeOut = Integer.parseInt(splitValue[3]);
                Element child = doc.createElement("instance");
                child.setAttribute("host", inHost);
                child.setAttribute("port", String.valueOf(inPort));
                child.setAttribute("lensEncoding", inEncoding);
                root.appendChild(child);
                try {
                    session = JLens.createSession(inHost, inPort, inEncoding);
                    session.open();
                    session.setEnableTransactionTimeout(true);
                    session.setTransactionTimeout((long) 1000 * (long) timeOut);
                    Text text = doc.createTextNode(removeXMLHeader(session.getInfo().getMessageData()));
                    child.appendChild(text);
                    session.close();
                } catch (LensException | ServerBusyException lex) {
                    Text text = doc.createTextNode(lex.getMessage());
                    child.appendChild(text);
                    if (session != null && session.isOpen()) {
                        session.close();
                    }
                } finally {
                    if (session != null && session.isOpen()) {
                        try {
                            session.close();
                        } catch (LensException lex) {
                            LOGGER.error("Session Close Lens Exception : " + lex);
                        }
                    }
                }
            }
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");

            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            trans.transform(source, result);
            String infoResult = sw.toString();
            infoResult = infoResult.replaceAll("&lt;", "<");
            infoResult = infoResult.replaceAll("&gt;", ">");
            return infoResult;
        } catch (LensException | IllegalArgumentException | ParserConfigurationException | TransformerException | DOMException ex) {
            throw new LensException(ErrorIndex.INTERNAL_ERR);
        }
    }

    /**
     * Remove the XML Header in bgt xml
     *
     * @param XMLData
     * @return
     */
    @Override
    public String removeXMLHeader(String XMLData) {
        if (XMLData.startsWith("<?xml version='1.0' encoding='utf-8'?>")) {
            StringBuilder s = new StringBuilder(XMLData);
            StringBuilder AfterRemoval = s.delete(20, 36);
            XMLData = AfterRemoval.toString();
        } else if (XMLData.startsWith("<?xml version='1.0' encoding='iso-8859-1'?>")) {
            StringBuilder s = new StringBuilder(XMLData);
            StringBuilder AfterRemoval = s.delete(20, 41);
            XMLData = AfterRemoval.toString();
        }
        return XMLData;
    }

    /**
     * Method to close the Lens session using the locale
     *
     * @param session <code>LensSession</code>
     */
    @Override
    public void closeSession(LensSession session) {
        if (session != null && session.isOpen()) {
            try {
                session.close();
            } catch (LensException lex) {
                //throw lex;
            }
        }
    }

    /**
     * Method to split the string
     *
     * @param value <code>String</code>
     * @return splitValue <code>String[]</code>
     */
    @Override
    public String[] splitValue(String value) {
        String splitValue[] = value.split(",");
        return splitValue;
    }

    /**
     * Method to get the locale from the bgtres result from locale instance.
     *
     * @param locMessage
     * @return locale <code>String</code>
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     * @throws com.bgt.lens.LensException
     */
    @Override
    public String getLocale(String locMessage) throws ParserConfigurationException, SAXException, IOException, LensException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = factory.newDocumentBuilder();
            InputSource inStream = new InputSource();
            inStream.setCharacterStream(new java.io.StringReader(locMessage));
            org.w3c.dom.Document doc = db.parse(inStream);
            NodeList nodeList = doc.getElementsByTagName("locale");

            for (int index = 0; index < nodeList.getLength(); index++) {
                org.w3c.dom.Node node = nodeList.item(index);
                if (node.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    org.w3c.dom.Element element = (org.w3c.dom.Element) node;
                    Node locNode = element.getAttributeNode("code");
                    return locNode.getNodeValue();
                }
            }
            throw new LensException(ErrorIndex.CODE_ATTRIB_MISSING, locMessage);
        } catch (SAXException | ParserConfigurationException sxex) {
            throw new LensException(ErrorIndex.XML_PARSER_ERROR, sxex);
        }
    }

}
