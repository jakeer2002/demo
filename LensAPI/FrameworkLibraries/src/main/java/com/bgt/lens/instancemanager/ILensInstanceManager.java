// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.instancemanager;

import com.bgt.lens.LensException;
import com.bgt.lens.LensSession;
import com.bgt.lens.ServerBusyException;
import com.bgt.lens.model.entity.CoreLenssettings;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * Lens repository interface
 */
public interface ILensInstanceManager {
    
    boolean loadConfigDetails(List<CoreLenssettings> lenssettingses) throws LensException;

    LensSession getConvertInstance(List<CoreLenssettings> lenssettingses) throws LensException, ServerBusyException;

    LensSession getLocaleInstance(ConcurrentHashMap lensDetails) throws LensException, ServerBusyException;
    
    String getDefaultLocaleByLocale(String locale, List<CoreLenssettings> lenssettingses) throws LensException;
    
    LensSession getLensInstance(String locale, ConcurrentHashMap lensDetails) throws LensException, ServerBusyException;
    
    String getLensInstanceInfo(List<CoreLenssettings> lenssettingses) throws Exception;

    String removeXMLHeader(String XMLData);

    void closeSession(LensSession session);

    String[] splitValue(String value);
    
    String getLocale(String locMessage) throws ParserConfigurationException, SAXException, IOException, LensException;
    
}
