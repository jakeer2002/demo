// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.ld;

import com.bgt.lens.*;
import com.bgt.lens.helpers.Enum.InstanceType;
import com.bgt.lens.model.entity.CoreLenssettings;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * DBLensSettingsManager is a class which maintains all configuration file
 * details.
 */
public class DBLensSettingsManager {

    private static final Logger LOGGER = LogManager.getLogger(DBLensSettingsManager.class);

    private final ConcurrentHashMap LensInstanceDetails = new ConcurrentHashMap();

    /**
     * Method to get the load the Lens details into the HashMap
     *
     * @param lenssettingses
     * @return LensInstanceDetails <code>Hashtable</code>
     * @throws com.bgt.lens.LensException
     * @see LensException
     */
    public ConcurrentHashMap getConfigDetailsAsHashMap(List<CoreLenssettings> lenssettingses) throws LensException {
        if (generateLensSettingHashMap(lenssettingses)) {
            if (!LensInstanceDetails.isEmpty()) {
                return LensInstanceDetails;
            } else {
                throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
            }
        } else {
            throw new LensException(ErrorIndex.INSTANCE_DETAILS_MISSING);
        }

    }

    /**
     * Method to add the value to HashMap
     *
     * @param key
     * @param value
     */
    private void addValueToHash(String key, String value) {
        LensInstanceDetails.put(key, value);
    }

    /**
     * Method to generate LensSetting HashMap
     */
    private boolean generateLensSettingHashMap(List<CoreLenssettings> lenssettingses) {
        try {
            String hashKey = null;
            if (lenssettingses != null && lenssettingses.size() > 0) {
                String hashValue = null;
                for (CoreLenssettings coreLenssettings : lenssettingses) {
                    // HaskKey as Locale
                    // HashValue as hostname,port,encoding,timout,locale,instanceType
                    if (coreLenssettings.getInstanceType().equalsIgnoreCase(InstanceType.LD.name())) {
                        hashKey = InstanceType.LD.name();
                        hashValue = (coreLenssettings.getHost()) + ","
                                + (coreLenssettings.getPort()) + ","
                                + (coreLenssettings.getCharacterSet()) + ","
                                + (coreLenssettings.getTimeout()) + ","
                                + InstanceType.LD.name() + ","
                                + (coreLenssettings.getInstanceType());
                    } else {
                        hashKey = coreLenssettings.getLocale();
                        hashValue = (coreLenssettings.getHost()) + ","
                                + (coreLenssettings.getPort()) + ","
                                + (coreLenssettings.getCharacterSet()) + ","
                                + (coreLenssettings.getTimeout()) + ","
                                + (coreLenssettings.getLocale()) + ","
                                + (coreLenssettings.getInstanceType());
                    }

                    addValueToHash(hashKey, hashValue);

                }
                return true;
            }
            return false;
        } catch (Exception ex) {
            LOGGER.error("An exception occured while converting lens settings to hashmap : ", ex);
            return false;
        }
    }
    
    /**
     * Method to generate LensSetting HashMap
     * @param lenssettingses
     * @return 
     */
    public ConcurrentHashMap generateLensSettingsHashMap(List<CoreLenssettings> lenssettingses) {
        ConcurrentHashMap lensDetails = new ConcurrentHashMap();
        try {
            String hashKey = null;
            if (lenssettingses != null && lenssettingses.size() > 0) {
                String hashValue = null;
                for (CoreLenssettings coreLenssettings : lenssettingses) {
                    // HaskKey as Locale
                    // HashValue as hostname,port,encoding,timout,locale,instanceType
                    if (coreLenssettings.getInstanceType().equalsIgnoreCase(com.bgt.lens.helpers.Enum.InstanceType.LD.name())) {
                        hashKey = com.bgt.lens.helpers.Enum.InstanceType.LD.name();
                        hashValue = (coreLenssettings.getHost()) + ","
                                + (coreLenssettings.getPort()) + ","
                                + (coreLenssettings.getCharacterSet()) + ","
                                + (coreLenssettings.getTimeout()) + ","
                                + com.bgt.lens.helpers.Enum.InstanceType.LD.name() + ","
                                + (coreLenssettings.getInstanceType());
                    } else {
                        hashKey = coreLenssettings.getLocale();
                        hashValue = (coreLenssettings.getHost()) + ","
                                + (coreLenssettings.getPort()) + ","
                                + (coreLenssettings.getCharacterSet()) + ","
                                + (coreLenssettings.getTimeout()) + ","
                                + (coreLenssettings.getLocale()) + ","
                                + (coreLenssettings.getInstanceType());
                    }

                    lensDetails.put(hashKey, hashValue);

                }
                return lensDetails;
            }
            return null;
        } catch (Exception ex) {
            LOGGER.error("An exception occured while converting lens settings to hashmap : ", ex);
            return null;
        }
    }

}
