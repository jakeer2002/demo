// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.ld.impl;

import com.bgt.lens.instancemanager.impl.LensInstanceManager;
import com.bgt.lens.LensException;
import com.bgt.lens.LensMessage;
import com.bgt.lens.LensSession;
import com.bgt.lens.ServerBusyException;
import com.bgt.lens.helpers.Enum.VariantType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.PerformanceLogging;
import com.bgt.lens.ld.ILensLDParseData;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LensLDParseDataImpl implements ILensLDParseData {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(LensLDParseDataImpl.class);

    private static final Helper _helper = new Helper();

    /**
     * *
     * Process Locale
     *
     * @param data
     * @param docType
     * @param type
     * @param lensInstanceManager
     * @param lensDetails
     * @return
     * @throws LensException
     * @throws ServerBusyException
     */
    @PerformanceLogging
    @Override
    public LensMessage processLocale(byte[] data, String docType, char type, LensInstanceManager lensInstanceManager, ConcurrentHashMap lensDetails) throws LensException, ServerBusyException {
        LensMessage localeMessage = null;
        LensSession lensSession = null;
        try {
            lensSession = lensInstanceManager.getLocaleInstance(lensDetails);
            localeMessage = lensSession.getLocale(data, docType, type);
            return localeMessage;
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            lensInstanceManager.closeSession(lensSession);
            throw lex;
        } finally {
            lensInstanceManager.closeSession(lensSession);
        }
    }

    /**
     * Tag Binary Data
     *
     * @param data
     * @param docType
     * @param type
     * @param locale
     * @param variantType
     * @param lensInstanceManager
     * @param lensDetails
     * @return
     * @throws LensException
     * @throws ServerBusyException
     */
    @PerformanceLogging
    @Override
    public String tagDocument(byte[] data, String docType, char type, String locale, VariantType variantType, LensInstanceManager lensInstanceManager, ConcurrentHashMap lensDetails) throws LensException, ServerBusyException {
        LensMessage outMessage = null;
        LensSession lensSession = null;
        String messageData = null;
        try {
            lensSession = lensInstanceManager.getLensInstance(locale, lensDetails);
            //locale = lensInstanceManager.getDefaultLocaleByLocale(locale);
            
            String[] localeObj = locale.split("_");
            String languageInstance = localeObj[0] + "_*";
            String defaultLocale = "*_*";

            String lensInstanceValue = null;
            if (lensDetails.containsKey(locale)) {
                lensInstanceValue = (String) lensDetails.get(locale);
            } else if (lensDetails.containsKey(languageInstance)) {
                lensInstanceValue = (String) lensDetails.get(languageInstance);
            } else if (lensDetails.containsKey(defaultLocale)) {
                lensInstanceValue = (String) lensDetails.get(defaultLocale);
            }
            
            String customSkillCode = lensInstanceValue.split(",")[6];

            //Assigning the temporary value to provide the temporary fix for custom skill code
            char[] skillCode = null;

            if (variantType.equals(com.bgt.lens.helpers.Enum.VariantType.bgtxml)) {
                if (!"null".equals(customSkillCode)) {
                    skillCode = customSkillCode.trim().toCharArray();
                    outMessage = lensSession.tagBinaryData(data, docType, type, skillCode);
                } else {
                    outMessage = lensSession.tagBinaryData(data, docType, type);
                }
            } else if (variantType.equals(com.bgt.lens.helpers.Enum.VariantType.rtf)) {
                if (!"null".equals(customSkillCode)) {
                    skillCode = customSkillCode.trim().toCharArray();
                    outMessage = lensSession.tagBinaryDataWithRTF(data, docType, type, skillCode);
                } else {
                    outMessage = lensSession.tagBinaryDataWithRTF(data, docType, type);
                }
            } else if (variantType.equals(com.bgt.lens.helpers.Enum.VariantType.htm)) {
                if (!"null".equals(customSkillCode)) {
                    skillCode = customSkillCode.trim().toCharArray();
                    outMessage = lensSession.tagBinaryDataWithHTM(data, docType, type, skillCode);
                } else {
                    outMessage = lensSession.tagBinaryDataWithHTM(data, docType, type);
                }
            }

            if (outMessage != null) {
                messageData = outMessage.getMessageData();
            }
            return messageData;
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            lensInstanceManager.closeSession(lensSession);
            throw lex;
        } catch (Exception ex) {
            LOGGER.error("Lens Exception : " + ex);
            lensInstanceManager.closeSession(lensSession);
            throw ex;
        } finally {
            lensInstanceManager.closeSession(lensSession);
        }
    }

    @Override
    public void ldParsingCheckRateLimit(Integer clientId, String host, Integer port, String locale, String instanceType) {
    }
}
