// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.interceptor;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.aspect.MessageContextHolder;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.services.logger.ILoggerService;
import com.bgt.lens.services.logger.impl.AsyncLogStorageService;
import com.bgt.lens.services.logger.impl.ElasticSearchTransportClient;
import java.net.InetAddress;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * *
 * SOAP message Interceptor
 */
public class MessageContextHolderInterceptor implements EndpointInterceptor {

    private static final Logger LOGGER = LogManager.getLogger(MessageContextHolderInterceptor.class);
    private final Helper _helper = new Helper();

    @Autowired(required = false)
    private ConcurrentRequestLimiter concurrentRequestLimiter;

    private SOAPBody soapBody;
    private String soapOperationName;
    
    @Value("${ElasticSearchAdvancedLogIndex}")
    private String elasticSearchAdvancedLogIndex;
    
    @Value("${ElasticSearchAdvancedLogType}")
    private String elasticSearchAdvancedLogType;
    
    @Value("${AddAdvancedLogToELK}")
    private boolean addAdvancedLogToELK;
    
//    @Autowired
//    private ILoggerService loggerService;
	
    @Autowired
    ElasticSearchTransportClient esTransportClient;
    
//    @Autowired
//    ILoggerService loggerService;

    /**
     * *
     * SOAP message handler
     *
     * @param messageContext
     * @param endpoint
     * @return
     * @throws Exception
     */
    @Override
    public boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        long startTime = System.currentTimeMillis();
        Date startDateTime = Date.from(Instant.now());

        // Get user name from request
        // if concurrentRequestLimiter isn't null, then concurrent request limiting is enabled
        if (_helper.isNotNull(concurrentRequestLimiter)) {
            String userName = _helper.getUserNameFromRequest(request);
            concurrentRequestLimiter.acquire(userName);
        }

        LOGGER.info("Request URL::" + request.getRequestURL().toString() + ":: Start Time=" + System.currentTimeMillis());
        request.setAttribute("startTime", startTime);

        MessageContextHolder.setMessageContext(messageContext);

        messageContext = MessageContextHolder.getMessageContext();
        SaajSoapMessage saajSoapMessage = (SaajSoapMessage) messageContext.getRequest();
        SOAPMessage soapMessage = saajSoapMessage.getSaajMessage();

        try {
            soapBody = soapMessage.getSOAPBody();
            if (soapBody.getFirstChild().getLocalName() != null) {
                soapOperationName = soapBody.getFirstChild().getLocalName();
            } else {
                soapOperationName = soapBody.getChildNodes().item(1).getLocalName();
            }
            InetAddress ip = InetAddress.getLocalHost();
            ApiContext apiContext = new ApiContext();

            //Get server and client address
            apiContext.setServerAddress(ip.getHostName() + "/" + ip.getHostAddress());

            String clientAddress = request.getHeader("x-clientip");
            if (_helper.isNotNullOrEmpty(clientAddress)) {
                apiContext.setClientAddress(clientAddress);
            } else {
                apiContext.setClientAddress(request.getRemoteAddr());
            }

            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setInTime(startDateTime);

            apiContext.setRequestUri(request.getRequestURL().toString());
            apiContext.setMethod(request.getMethod());
            apiContext.setAdvanceLog(new AdvanceLog());
            apiContext.setResource(_helper.getSoapResourceName(soapOperationName));
            apiContext.setApi(_helper.getSoapApiName(soapOperationName));

            if (soapOperationName.equalsIgnoreCase("InfoRequest")) {
                apiContext.setReturnType("InfoResponse");
            } else if (soapOperationName.equalsIgnoreCase("PingRequest")) {
                apiContext.setReturnType("PingResponse");
            } else if (soapOperationName.equalsIgnoreCase("ConvertRequest")) {
                apiContext.setReturnType("ConvertResponse");
            } else if (soapOperationName.equalsIgnoreCase("CanonRequest")) {
                apiContext.setReturnType("CanonResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseResumeRequest")) {
                apiContext.setReturnType("ParseResumeResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseResumeBGXMLRequest")) {
                apiContext.setReturnType("ParseResumeBGXMLResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseResumeStructuredBGXMLRequest")) {
                apiContext.setReturnType("ParseResumeStructuredBGXMLResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseResumeHRXMLRequest")) {
                apiContext.setReturnType("ParseResumeHRXMLResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseResumeWithRTFRequest")) {
                apiContext.setReturnType("ParseResumeWithRTFResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseResumeWithHTMRequest")) {
                apiContext.setReturnType("ParseResumeWithHTMResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseJobRequest")) {
                apiContext.setReturnType("ParseJobResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseJobBGXMLRequest")) {
                apiContext.setReturnType("ParseJobBGXMLResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseJobStructuredBGXMLRequest")) {
                apiContext.setReturnType("ParseJobStructuredBGXMLResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseJobWithHTMRequest")) {
                apiContext.setReturnType("ParseJobWithHTMResponse");
            } else if (soapOperationName.equalsIgnoreCase("ParseJobWithRTFRequest")) {
                apiContext.setReturnType("ParseJobWithRTFResponse");
            } else if (soapOperationName.equalsIgnoreCase("ProcessLocaleRequest")) {
                apiContext.setReturnType("ProcessLocaleResponse");
            } else if (soapOperationName.equalsIgnoreCase("CreateApiRequest")) {
                apiContext.setReturnType("CreateApiResponse");
            } else if (soapOperationName.contains("DeleteApiRequest")) {
                apiContext.setReturnType("DeleteApiResponse");
            } else if (soapOperationName.contains("UpdateApiRequest")) {
                apiContext.setReturnType("UpdateApiResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetApiRequest")) {
                apiContext.setReturnType("GetApiResponse");
            } else if (soapOperationName.contains("GetApiByIdRequest")) {
                apiContext.setReturnType("GetApiByIdResponse");
            } else if (soapOperationName.equalsIgnoreCase("AddResourceRequest")) {
                apiContext.setReturnType("AddResourceResponse");
            } else if (soapOperationName.contains("DeleteResourceRequest")) {
                apiContext.setReturnType("DeleteResourceResponse");
            } else if (soapOperationName.contains("UpdateResourceRequest")) {
                apiContext.setReturnType("UpdateResourceResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetResourceRequest")) {
                apiContext.setReturnType("GetResourceResponse");
            } else if (soapOperationName.contains("GetResourceByIdRequest")) {
                apiContext.setReturnType("GetResourceByIdResponse");
            } else if (soapOperationName.equalsIgnoreCase("AddLensSettingsRequest")) {
                apiContext.setReturnType("AddLensSettingsResponse");
            } else if (soapOperationName.contains("UpdateLensSettingsRequest")) {
                apiContext.setReturnType("UpdateLensSettingsResponse");
            } else if (soapOperationName.contains("DeleteLensSettingsRequest")) {
                apiContext.setReturnType("DeleteLensSettingsResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetLensSettingsRequest")) {
                apiContext.setReturnType("GetLensSettingsResponse");
            } else if (soapOperationName.contains("GetLensSettingsByIdRequest")) {
                apiContext.setReturnType("GetLensSettingsByIdResponse");
            } else if (soapOperationName.equalsIgnoreCase("CreateConsumerRequest")) {
                apiContext.setReturnType("CreateConsumerResponse");
            } else if (soapOperationName.contains("UpdateConsumerRequest")) {
                apiContext.setReturnType("UpdateConsumerResponse");
            } else if (soapOperationName.contains("DeleteConsumerRequest")) {
                apiContext.setReturnType("DeleteConsumerResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetClientsRequest")) {
                apiContext.setReturnType("GetClientsResponse");
            } else if (soapOperationName.contains("GetClientByIdRequest")) {
                apiContext.setReturnType("GetClientByIdResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetAllLicenseDetailsRequest")) {
                apiContext.setReturnType("GetAllLicenseDetailsResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetLicenseRequest")) {
                apiContext.setReturnType("GetLicenseResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetUsageLogsRequest")) {
                apiContext.setReturnType("GetUsageLogsResponse");
            } else if (soapOperationName.contains("GetUsageLogByIdRequest")) {
                apiContext.setReturnType("GetUsageLogByIdResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetErrorLogsRequest")) {
                apiContext.setReturnType("GetErrorLogsResponse");
            } else if (soapOperationName.contains("GetErrorLogByIdRequest")) {
                apiContext.setReturnType("GetErrorLogByIdResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetUsageCounterRequest")) {
                apiContext.setReturnType("GetUsageCounterResponse");
            } else if (soapOperationName.contains("GetUsageCounterByIdRequest")) {
                apiContext.setReturnType("GetUsageCounterByIdResponse");
            } else if (soapOperationName.equalsIgnoreCase("CreateVendorRequest")) {
                apiContext.setReturnType("CreateVendorResponse");
            } else if (soapOperationName.contains("OpenVendorRequest")) {
                apiContext.setReturnType("OpenVendorResponse");
            } else if (soapOperationName.contains("CloseVendorRequest")) {
                apiContext.setReturnType("CloseVendorResponse");
            } else if (soapOperationName.contains("DeleteVendorRequest")) {
                apiContext.setReturnType("DeleteVendorResponse");
            } else if (soapOperationName.contains("GetVendorByIdRequest")) {
                apiContext.setReturnType("GetVendorByIdResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetVendorByCriteriaRequest")) {
                apiContext.setReturnType("GetVendorByCriteriaResponse");
            } else if (soapOperationName.contains("UpdateVendorRequest")) {
                apiContext.setReturnType("UpdateVendorResponse");
            } else if (soapOperationName.equalsIgnoreCase("RegisterResumeRequest")) {
                apiContext.setReturnType("RegisterResumeResponse");
            } else if (soapOperationName.contains("UnregisterResumeRequest")) {
                apiContext.setReturnType("UnregisterResumeResponse");
            } else if (soapOperationName.equalsIgnoreCase("ResumeSearchRequest")) {
                apiContext.setReturnType("ResumeSearchResponse");
            } else if (soapOperationName.contains("FetchResumeRequest")) {
                apiContext.setReturnType("FetchResumeResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetRegisterLogByCriteriaRequest")) {
                apiContext.setReturnType("GetRegisterLogByCriteriaResponse");
            } else if (soapOperationName.contains("GetRegisterLogByIdRequest")) {
                apiContext.setReturnType("GetRegisterLogByIdResponse");
            } else if (soapOperationName.equalsIgnoreCase("GetSearchCommandDumpByCriteriaRequest")) {
                apiContext.setReturnType("GetSearchCommandDumpByCriteriaResponse");
            } else if (soapOperationName.contains("GetSearchCommandDumpByIdRequest")) {
                apiContext.setReturnType("GetSearchCommandDumpByIdResponse");
            } else if (soapOperationName.contains("GetFacetFilterListRequest")) {
                apiContext.setReturnType("GetFacetFilterListResponse");
            } else if (soapOperationName.contains("GetFacetDistributionRequest")) {
                apiContext.setReturnType("GetFacetDistributionResponse");
            } else if (soapOperationName.contains("RegisterJobRequest")) {
                apiContext.setReturnType("RegisterJobResponse");
            } else if (soapOperationName.contains("UnregisterJobRequest")) {
                apiContext.setReturnType("UnregisterJobResponse");
            } else if (soapOperationName.contains("FetchJobRequest")) {
                apiContext.setReturnType("FetchJobResponse");
            } else if (soapOperationName.contains("JobSearchRequest")) {
                apiContext.setReturnType("JobSearchResponse");
            }

            request.setAttribute("ApiContext", apiContext);
            return true;
        } catch (SOAPException ex) {
            LOGGER.error("Invalid SOAP action." + " - " + ex);
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_SOAP_ACTION), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * *
     * SOAP response message handler
     *
     * @param messageContext
     * @param endpoint
     * @return
     * @throws Exception
     */
    @Override
    public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
        MessageContextHolder.removeMessageContext();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        if (_helper.isNotNull(concurrentRequestLimiter)) {
            String userName = _helper.getUserNameFromRequest(request);
            concurrentRequestLimiter.release(userName);
        }

        return true;
    }

    /**
     * *
     * SOAP error handler
     *
     * @param messageContext
     * @param endpoint
     * @return
     * @throws Exception
     */
    @Override
    public boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
        MessageContextHolder.removeMessageContext();
//        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
//        if (_helper.isNotNull(concurrentRequestLimiter)) {
//            String userName = _helper.getUserNameFromRequest(request);
//            concurrentRequestLimiter.release(userName);
//        }

        return true;
    }

    /**
     * *
     * SOAP request process completion
     *
     * @param messageContext
     * @param endpoint
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) throws Exception {
        Date endDateTime = Date.from(Instant.now());
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setOutTime(endDateTime);
        apiContext.setSeconds(_helper.getTimeDifference(apiContext.getInTime(), apiContext.getOutTime()));

           if (apiContext.isAdvanceLog()) {
            _helper.updatePerformanceLog(apiContext);
            LOGGER.info("Add advanced log to elastic search instance");
            AsyncLogStorageService asyncLogService = new AsyncLogStorageService(esTransportClient);
            asyncLogService.elasticSearchAdvancedLogIndex = elasticSearchAdvancedLogIndex;
            asyncLogService.elasticSearchAdvancedLogType = elasticSearchAdvancedLogType;
            
            if(addAdvancedLogToELK){
                asyncLogService.addAdvancedLogToAMQPAsync(apiContext);
            }
//            loggerService.addAdvancedLogWithoutAsync(apiContext);
        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        HttpServletResponse httpServletResponse = RequestAndResponseContextHolder.response();
        httpServletResponse.setHeader("X-Rate-Limit-Limit", apiContext.getRequestCount());
        httpServletResponse.setHeader("X-Rate-Limit-Remaining", apiContext.getRemainingRequestCount());
        httpServletResponse.setHeader("X-Rate-Limit-Reset", String.valueOf(apiContext.getRetryTimeStamp()));
        
        long startTime = (Long) request.getAttribute("startTime");
        LOGGER.info("Request URL::" + request.getRequestURL().toString()
                + ":: End Time=" + System.currentTimeMillis());
        LOGGER.info("Request URL::" + request.getRequestURL().toString()
                + ":: Time Taken in Milliseconds =" + (System.currentTimeMillis() - startTime) + ":: Time Taken in Seconds =" + ((System.currentTimeMillis() - startTime) / 1000.0));
    }
}
