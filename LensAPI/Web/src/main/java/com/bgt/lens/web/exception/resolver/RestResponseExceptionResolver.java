// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.exception.resolver;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ExceptionHandler;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.ResolverMappers;
import com.bgt.lens.model.rest.response.ApiResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

// Global exception handler for REST requests
/**
 * @see <a
 * href="http://www.jayway.com/2012/09/23/improve-your-spring-rest-api-part-ii/">Used
 * source</a>
 */
@Component
public class RestResponseExceptionResolver extends AbstractHandlerExceptionResolver {

    /**
     * *
     * get Precedence
     *
     * @return
     */
    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    @Autowired
    private RequestMappingHandlerAdapter adapter;

    private List<HttpMessageConverter<?>> messageConverters;

    private static final Logger LOGGER = LogManager.getLogger(RestResponseExceptionResolver.class);

    private static final Helper _helper = new Helper();
    private final ResolverMappers _resolverMappers = new ResolverMappers();

    // Format response that should be sent to the customer if an unexpected error occurred
    /**
     * *
     * Handle Exception
     *
     * @param request
     * @param response
     * @param aHandler
     * @param ex
     * @return
     */
    @Override
    public ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response,
            Object aHandler, Exception ex) {

        LOGGER.error("Exception :" + ex);

        ApiResponse apiResponse = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        if (_helper.isNotNull(apiContext) && _helper.isNotNull(apiContext.getRequestId())) {
            apiResponse.requestId = apiContext.getRequestId().toString();
        } else {
            apiResponse.requestId = UUID.randomUUID().toString();
        }

        if (_helper.isNotNullOrEmpty(response.getHeader("X-Rate-Limit-Limit"))) {
            response.setHeader("X-Rate-Limit-Limit", apiContext.getRequestCount());
            response.setHeader("X-Rate-Limit-Remaining", apiContext.getRemainingRequestCount());
            response.setHeader("X-Rate-Limit-Reset", String.valueOf(apiContext.getRetryTimeStamp()));
        }

        apiResponse.responseData = _helper.getErrorMessageWithURL(ApiErrors.UNKNOWN_EXCEPTION);
        apiResponse.timeStamp = Date.from(Instant.now());
        apiResponse.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
        apiResponse.setStatus(false);

        apiResponse = new ExceptionHandler().handleException(ex, apiResponse);
        Object obj = null;
        if (apiContext.getReturnType() == null) {
            obj = apiResponse;
        } else {
            switch (apiContext.getReturnType()) {
                case "InfoResponse":
                    obj = _resolverMappers.getinfo(apiResponse);
                    break;
                case "PingResponse":
                    obj = _resolverMappers.getPing(apiResponse);
                    break;
                case "ConvertResponse":
                    obj = _resolverMappers.convertBinaryData(apiResponse);
                    break;
                case "CanonResponse":
                    obj = _resolverMappers.canonBinaryData(apiResponse);
                    break;
                case "ParseResumeResponse":
                    obj = _resolverMappers.parseResume(apiResponse);
                    break;
                case "ParseResumeBGXMLResponse":
                    obj = _resolverMappers.parseResumeBGXML(apiResponse);
                    break;
                case "ParseResumeStructuredBGXMLResponse":
                    obj = _resolverMappers.parseResumeStructuredBGXML(apiResponse);
                    break;
                case "ParseResumeHRXMLResponse":
                    obj = _resolverMappers.parseResumeHRXML(apiResponse);
                    break;
                case "ParseResumeWithRTFResponse":
                    obj = _resolverMappers.parseResumeWithRTF(apiResponse);
                    break;
                case "ParseResumeWithHTMResponse":
                    obj = _resolverMappers.parseResumeWithHTM(apiResponse);
                    break;
                case "ParseJobResponse":
                    obj = _resolverMappers.parseJob(apiResponse);
                    break;
                case "ParseJobBGXMLResponse":
                    obj = _resolverMappers.parseJobBGXML(apiResponse);
                    break;
                case "ParseJobStructuredBGXMLResponse":
                    obj = _resolverMappers.parseJobStructuredBGXML(apiResponse);
                    break;
                case "ParseJobWithRTFResponse":
                    obj = _resolverMappers.parseJobWithRFT(apiResponse);
                    break;
                case "ParseJobWithHTMResponse":
                    obj = _resolverMappers.parseJobWithHTM(apiResponse);
                    break;
                case "ProcessLocaleResponse":
                    obj = _resolverMappers.findLocale(apiResponse);
                    break;
                case "CreateApiResponse":
                    obj = _resolverMappers.createApi(apiResponse);
                    break;
                case "UpdateApiResponse":
                    obj = _resolverMappers.updateApi(apiResponse);
                    break;
                case "DeleteApiResponse":
                    obj = _resolverMappers.deleteApi(apiResponse);
                    break;
                case "GetApiResponse":
                    obj = _resolverMappers.getApi(apiResponse);
                    break;
                case "GetApiByIdResponse":
                    obj = _resolverMappers.getApiById(apiResponse);
                    break;
                case "AddResourceResponse":
                    obj = _resolverMappers.addResource(apiResponse);
                    break;
                case "UpdateResourceResponse":
                    obj = _resolverMappers.updateResource(apiResponse);
                    break;
                case "DeleteResourceResponse":
                    obj = _resolverMappers.deleteResource(apiResponse);
                    break;
                case "GetResourceResponse":
                    obj = _resolverMappers.getResource(apiResponse);
                    break;
                case "GetResourceByIdResponse":
                    obj = _resolverMappers.getResourcebyId(apiResponse);
                    break;
                case "AddLensSettingsResponse":
                    obj = _resolverMappers.addLensSettings(apiResponse);
                    break;
                case "UpdateLensSettingsResponse":
                    obj = _resolverMappers.updateLensSettings(apiResponse);
                    break;
                case "DeleteLensSettingsResponse":
                    obj = _resolverMappers.deleteLensSettings(apiResponse);
                    break;
                case "GetLensSettingsResponse":
                    obj = _resolverMappers.getLensSettings(apiResponse);
                    break;
                case "GetLensSettingsByIdResponse":
                    obj = _resolverMappers.getLensSettingsById(apiResponse);
                    break;
                case "CreateConsumerResponse":
                    obj = _resolverMappers.createConsumer(apiResponse);
                    break;
                case "UpdateConsumerResponse":
                    obj = _resolverMappers.updateConsumer(apiResponse);
                    break;
                case "DeleteConsumerResponse":
                    obj = _resolverMappers.deleteConsumer(apiResponse);
                    break;
                case "GetClientsResponse":
                    obj = _resolverMappers.getClients(apiResponse);
                    break;
                case "GetClientByIdResponse":
                    obj = _resolverMappers.getClientById(apiResponse);
                    break;
                case "GetAllLicenseDetailsResponse":
                    obj = _resolverMappers.getLicenseDetails(apiResponse);
                    break;
                case "GetLicenseResponse":
                    obj = _resolverMappers.getLicenseDetailsById(apiResponse);
                    break;
                case "GetUsageLogsResponse":
                    obj = _resolverMappers.getUsageLogs(apiResponse);
                    break;
                case "GetUsageLogByIdResponse":
                    obj = _resolverMappers.getUsageLogsById(apiResponse);
                    break;
                case "GetErrorLogsResponse":
                    obj = _resolverMappers.getErrorLogs(apiResponse);
                    break;
                case "GetErrorLogByIdResponse":
                    obj = _resolverMappers.getErrorLogsById(apiResponse);
                    break;
                case "GetUsageCounterResponse":
                    obj = _resolverMappers.getUsageCounter(apiResponse);
                    break;
                case "GetUsageCounterByIdResponse":
                    obj = _resolverMappers.getUsageCounterById(apiResponse);
                    break;
                }
        }

        try {
            ServletWebRequest webRequest = new ServletWebRequest(request, response);
            response.setStatus(apiResponse.statusCode.value());
            return handleResponseBody(obj, webRequest);
        } catch (IOException | ServletException handlerException) {
            LOGGER.warn("Handling of [" + ex.getClass().getName() + "] resulted in Exception", handlerException);
        }
        apiContext.setApiResponse(apiResponse);
        _helper.setApiContext(apiContext);
        return null;
    }

    @SuppressWarnings("unchecked")
    private ModelAndView handleResponseBody(Object returnValue, ServletWebRequest webRequest)
            throws ServletException, IOException {

        messageConverters = adapter.getMessageConverters();

        HttpInputMessage inputMessage = new ServletServerHttpRequest(webRequest.getRequest());
        List<MediaType> acceptedMediaTypes = inputMessage.getHeaders().getAccept();
        if (acceptedMediaTypes.isEmpty()) {
            acceptedMediaTypes = Collections.singletonList(MediaType.ALL);
        }
        MediaType.sortByQualityValue(acceptedMediaTypes);
        HttpOutputMessage outputMessage = new ServletServerHttpResponse(webRequest.getResponse());
        Class<?> returnValueType = returnValue.getClass();
        if (this.messageConverters != null) {
            for (MediaType acceptedMediaType : acceptedMediaTypes) {
                for (HttpMessageConverter messageConverter : this.messageConverters) {
                    if (messageConverter.canWrite(returnValueType, acceptedMediaType)) {
                        messageConverter.write(returnValue, acceptedMediaType, outputMessage);
                        return new ModelAndView();
                    }
                }
            }
        }

        LOGGER.warn("Could not find HttpMessageConverter that supports return type [" + returnValueType + "] and "
                + acceptedMediaTypes);
        return null;
    }
}
