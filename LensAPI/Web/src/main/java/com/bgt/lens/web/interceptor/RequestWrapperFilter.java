/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bgt.lens.web.interceptor;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * @author pvarudaraj
 */
@Component
public class RequestWrapperFilter extends OncePerRequestFilter {

    // not the most elegant, but one of the spring friends suggested this way.
    private ThreadLocal<HttpServletResponse> responses = new ThreadLocal<>();

    @Override
    public void doFilterInternal(HttpServletRequest request,
            HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
            HttpServletRequest currentRequest = (HttpServletRequest) request;
            MultipleReadHttpRequest wrappedRequest = new MultipleReadHttpRequest(currentRequest);
            RequestAndResponseContextHolder.response(response);
            filterChain.doFilter(wrappedRequest, response);
    }
}
