// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.exception.resolver;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ExceptionHandler;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.web.interceptor.RequestAndResponseContextHolder;
import java.time.Instant;
import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import static org.springframework.context.i18n.LocaleContextHolder.getLocale;
import org.springframework.stereotype.Component;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.AbstractEndpointExceptionResolver;
import org.springframework.ws.soap.SoapBody;
import org.springframework.ws.soap.SoapFault;
import org.springframework.ws.soap.SoapMessage;

// Global exception handler for SOAP requests
/**
 * *
 * Global SOAP Exception resolver
 */
@Component
public class WebServiceResponseExceptionResolver extends AbstractEndpointExceptionResolver {
    
    /**
     * Helper
     */
    private static final Helper _helper = new Helper();

    //private static final Logger LOGGER = Logger.getLogger(WebServiceResponseExceptionResolver.class);
    /**
     * *
     * SOAP exception handling
     *
     * @param messageContext
     * @param endpoint
     * @param ex
     * @return
     */
    @Override
    protected boolean resolveExceptionInternal(MessageContext messageContext, Object endpoint, Exception ex) {
        SoapMessage response = (SoapMessage) messageContext.getResponse();

        ApiContext apiContext = _helper.getApiContext();
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.responseData = _helper.getErrorMessageWithURL(ApiErrors.UNKNOWN_EXCEPTION);
        apiResponse.timeStamp = Date.from(Instant.now());
        apiResponse.setStatus(false);
        
        HttpServletResponse httpServletResponse = RequestAndResponseContextHolder.response();
        if (_helper.isNotNullOrEmpty(httpServletResponse.getHeader("X-Rate-Limit-Limit"))) {
            httpServletResponse.setHeader("X-Rate-Limit-Limit", apiContext.getRequestCount());
            httpServletResponse.setHeader("X-Rate-Limit-Remaining", apiContext.getRemainingRequestCount());
            httpServletResponse.setHeader("X-Rate-Limit-Reset", String.valueOf(apiContext.getRetryTimeStamp()));
        }

        apiResponse = new ExceptionHandler().handleException(ex, apiResponse);

        String faultString = apiResponse.responseData.toString();
        SoapBody body = response.getSoapBody();
        SoapFault fault = body.addServerOrReceiverFault(faultString, getLocale());
        customizeFault(messageContext, endpoint, ex, fault);
        apiContext.setApiResponse(apiResponse);
        _helper.setApiContext(apiContext);

        return true;
    }

    /**
     * *
     * Customize SOAP fault
     *
     * @param messageContext
     * @param endpoint
     * @param ex
     * @param fault
     */
    protected void customizeFault(MessageContext messageContext, Object endpoint, Exception ex, SoapFault fault) {
    }

}
