// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.rest;

/**
 *
 * API URI's
 */
public class UriConstants {

    /**
     * Clients
     */
    public static final String Clients = "/clients";

    /**
     * Clients
     */
    public static final String ResellerClients = "/clients/reseller";

    /**
     * API's
     */
    public static final String Api = "/apis";

    /**
     * Lens
     */
    public static final String Lens = "/lens";

    /**
     * LicenseAll
     */
    public static final String LicenseAll = "/licenseall";

    /**
     * License
     */
    public static final String License = "/license";

    /**
     * Usage logs
     */
    public static final String UsageLogs = "/usagelogs";

    /**
     * Error logs
     */
    public static final String ErrorLogs = "/errorlogs";

    /**
     * Usage counters
     */
    public static final String UsageCounters = "/usagecounters";

    /**
     * Resources
     */
    public static final String Resources = "/resources";

    /**
     * Info
     */
    public static final String Info = "/status/info";

    /**
     * Ping
     */
    public static final String Ping = "/status/ping";

    /**
     * Converter
     */
    public static final String Converter = "/converter";

    /**
     * Canonicalizer
     */
    public static final String Canonicalizer = "/canonicalizer";

    /**
     * Resume parser
     */
    public static final String ResumeParser = "/resume";
    /**
     * ResumeParserBGXML
     */
    public static final String ResumeParserBGXML = "/resume/bgtxml";
     /**
     * ResumeParserStructuredBGXML
     */
    public static final String ResumeParserStructuredBGXML = "/resume/structuredbgtxml";
    /**
     * ResumeParserHRXML
     */
    public static final String ResumeParserHRXML = "/resume/hrxml";
    /**
     * ResumeParserRTF
     */
    public static final String ResumeParserRTF = "/resume/rtf";
    /**
     * ResumeParserHTM
     */
    public static final String ResumeParserHTM = "/resume/htm";
    /**
     * Job parser
     */
    public static final String JobParser = "/job";
    /**
     * JobParserBGXML
     */
    public static final String JobParserBGXML = "/job/bgtxml";
     /**
     * JobParserRTF
     */
    public static final String JobParserRTF = "/job/rtf";
    /**
     * JobParserBGXML
     */
    public static final String JobParserHTM = "/job/htm";
    /**
     * JobParserStructuredBGXML
     */
    public static final String JobParserStructuredBGXML = "/job/structuredbgtxml";
    /**
     * Find locale
     */
    public static final String FindLocale = "/processlocale";

}
