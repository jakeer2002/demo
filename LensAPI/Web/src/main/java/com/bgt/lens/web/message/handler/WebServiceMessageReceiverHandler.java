package com.bgt.lens.web.message.handler;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ws.InvalidXmlException;
import org.springframework.ws.NoEndpointFoundException;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.WebServiceMessageFactory;
import org.springframework.ws.context.DefaultMessageContext;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.support.DefaultStrategiesHelper;
import org.springframework.ws.transport.EndpointAwareWebServiceConnection;
import org.springframework.ws.transport.WebServiceConnection;
import org.springframework.ws.transport.WebServiceMessageReceiver;
import org.springframework.ws.transport.context.DefaultTransportContext;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpServletConnection;
import org.springframework.ws.transport.http.HttpTransportConstants;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.transport.http.WebServiceMessageReceiverHandlerAdapter;
import org.springframework.ws.transport.support.TransportUtils;

/**
 * Adapter to map XML parsing and other low-level errors to SOAP faults instead
 * of server standard error pages. Also, this class will always use return code
 * HTTP_BADREQUEST (status 400) to requests
 *
 * @see <a
 * href="https://jbossadventure.wordpress.com/2013/09/27/spring-ws-and-exception-handling-23-xml-parsing-issues/">See</a>
 * @see <a href="http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383529">
 * SOAP error codes</a>
 */
// todo : TRACE everything
public class WebServiceMessageReceiverHandler
        extends WebServiceMessageReceiverHandlerAdapter
        implements Ordered, ApplicationContextAware {

    private ApplicationContext context;
    
    private final Helper _helper = new Helper();

    /**
     * *
     *
     * @param applicationContext
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        this.context = applicationContext;
    }

    /**
     * *
     * Set properties
     */
    @Override
    public void afterPropertiesSet() {
        DefaultStrategiesHelper defaultStrategiesHelper = new DefaultStrategiesHelper(MessageDispatcherServlet.class);
        WebServiceMessageFactory factory = defaultStrategiesHelper
                .getDefaultStrategy(WebServiceMessageFactory.class, context);
        setMessageFactory(factory);
    }

    /**
     * *
     *
     * @param connection
     * @param receiver
     * @throws Exception
     */
    protected void overriddenHandleConnection(WebServiceConnection connection, WebServiceMessageReceiver receiver)
            throws Exception {

        TransportContext previousTransportContext = TransportContextHolder.getTransportContext();
        TransportContextHolder.setTransportContext(new DefaultTransportContext(connection));

        try {
            WebServiceMessage request = connection.receive(getMessageFactory());
            MessageContext messageContext = new DefaultMessageContext(request, getMessageFactory());
            receiver.receive(messageContext);
            if (messageContext.hasResponse()) {
                connection.send(messageContext.getResponse());
            }
        } catch (NoEndpointFoundException ex) {
            if (connection instanceof EndpointAwareWebServiceConnection) {
                ((EndpointAwareWebServiceConnection) connection).endpointNotFound();
            }
            throw ex;
        } finally {
            TransportUtils.closeConnection(connection);
            TransportContextHolder.setTransportContext(previousTransportContext);
        }
    }

    /**
     * *
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public ModelAndView handle(HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            Object handler) throws Exception {
        if (HttpTransportConstants.METHOD_POST.equals(httpServletRequest.getMethod())) {
            WebServiceConnection connection = new InterceptWebServiceConnection(httpServletRequest, httpServletResponse);
            try {
                handleConnection(connection, (WebServiceMessageReceiver) handler);
            } catch (InvalidXmlException ex) {
                handleInvalidXmlException(httpServletRequest, httpServletResponse, handler, ex);
            } catch (Exception ex) {
                handleGeneralException(httpServletRequest, httpServletResponse, handler, ex);
            }
        } else {
            handleNonPostMethod(httpServletRequest, httpServletResponse, handler);
        }
        return null;
    }

    /**
     * *
     *
     * @param httpServletRequest
     * @param response
     * @param handler
     * @param ex
     * @throws IOException
     */
    private void handleGeneralException(
            HttpServletRequest httpServletRequest,
            HttpServletResponse response, Object handler,
            Exception ex) throws IOException {
        writeUnExpectedErrorResponseWithMessage(response, _helper.getErrorMessageWithURL(ApiErrors.UNKNOWN_EXCEPTION));
    }

    /**
     * By default, sets SC_BAD_REQUEST as response in Spring, so overwritten to
     *
     * @param httpServletRequest
     * @param response
     * @param handler
     * @param ex
     * @throws java.io.IOException
     */
    @Override
    protected void handleInvalidXmlException(
            HttpServletRequest httpServletRequest,
            HttpServletResponse response, Object handler, InvalidXmlException ex)
            throws IOException {
        writeErrorResponseWithMessage(response, _helper.getErrorMessageWithURL(ApiErrors.INVALID_SOAP_REQUEST));
    }

    /**
     * By default, sets SC_METHOD_NOT_ALLOWED as response in Spring, so
     * overwritten to provide SC_BAD_REQUEST and reasonable SOAP fault response.
     *
     * @param httpServletRequest
     * @param response
     * @param handler
     * @throws java.lang.Exception
     */
    @Override
    protected void handleNonPostMethod(HttpServletRequest httpServletRequest,
            HttpServletResponse response,
            Object handler) throws Exception {
        writeErrorResponseWithMessage(response, _helper.getErrorMessageWithURL(ApiErrors.INVALID_HTTP_METHOD));
    }

    /**
     * *
     *
     * @param response
     * @param message
     * @throws IOException
     */
    private void writeErrorResponseWithMessage(HttpServletResponse response, String message)
            throws IOException {
        String errorXml = String.format(
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'>"
                + "<SOAP-ENV:Header/><SOAP-ENV:Body>"
                + "<SOAP-ENV:Fault>"
                + "<faultcode>SOAP-ENV:Client</faultcode>"
                + "<faultstring xml:lang='en'>%s</faultstring>"
                + "</SOAP-ENV:Fault>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>",
                StringEscapeUtils.escapeXml10(message)
        );

        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        response.setContentType("text/xml");
        response.getWriter().write(errorXml);
        response.getWriter().flush();
    }

    /**
     * *
     *
     * @param response
     * @param message
     * @throws IOException
     */
    private void writeUnExpectedErrorResponseWithMessage(HttpServletResponse response, String message)
            throws IOException {
        String errorXml = String.format(
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'>"
                + "<SOAP-ENV:Header/><SOAP-ENV:Body>"
                + "<SOAP-ENV:Fault>"
                + "<faultcode>SOAP-ENV:Client</faultcode>"
                + "<faultstring xml:lang='en'>%s</faultstring>"
                + "</SOAP-ENV:Fault>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>",
                StringEscapeUtils.escapeXml10(message)
        );

        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        response.setContentType("text/xml");
        response.getWriter().write(errorXml);
        response.getWriter().flush();
    }

    /**
     * *
     * get order
     *
     * @return Integer
     */
    @Override
    public int getOrder() {
        return 1;
    }

    /**
     * This class is needed as
     * org.springframework.ws.transport.http.HttpServletConnection will throw an
     * exception if it is used outside Spring framework files. However,
     * extending it and using the same implementation seems to be fine.
     *
     */
    static class InterceptWebServiceConnection extends HttpServletConnection {

        protected InterceptWebServiceConnection(HttpServletRequest httpServletRequest,
                HttpServletResponse httpServletResponse) {
            super(httpServletRequest, httpServletResponse);
        }
    }
}
