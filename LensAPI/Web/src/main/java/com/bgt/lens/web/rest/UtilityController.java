// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.rest;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Enum.VariantType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.parserservice.request.CanonRequest; 
import com.bgt.lens.model.parserservice.request.ConvertRequest;
import com.bgt.lens.model.parserservice.request.InfoRequest;
import com.bgt.lens.model.parserservice.request.ParseJobBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseJobRequest;
import com.bgt.lens.model.parserservice.request.ParseJobStructuredBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseJobWithHTMRequest;
import com.bgt.lens.model.parserservice.request.ParseJobWithRTFRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeHRXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeStructuredBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeWithHTMRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeWithRTFRequest;
import com.bgt.lens.model.parserservice.request.PingRequest;
import com.bgt.lens.model.parserservice.request.ProcessLocaleRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.parserservice.CanonResponse;
import com.bgt.lens.model.rest.response.parserservice.ConvertResponse;
import com.bgt.lens.model.rest.response.parserservice.InfoResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobBGXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobStructuredBGXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobWithHTMResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseJobWithRTFResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeBGXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeHRXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeStructuredBGXMLResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeWithHTMResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeWithRTFResponse;
import com.bgt.lens.model.rest.response.parserservice.PingResponse;
import com.bgt.lens.model.rest.response.parserservice.ProcessLocaleResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.utility.IUtilityService;
import java.io.IOException;
import java.net.InetAddress;
import java.time.Instant;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * Utility controller - Lens/XRay related functionalities
 */
@RestController
public class UtilityController {

    @Autowired
    IUtilityService utilityService;
    private IDataRepositoryService dbService;
    private static final Logger LOGGER = LogManager.getLogger(UtilityController.class);
    private final Helper _helper = new Helper();
    @Value("${EnableDocumentStoreService}")
    private Boolean enableDocumentStoreService;
    @Value("${DSSInstanceType}")
    private String dSSInstanceType;
    @Value("${DSSLocale}")
    private String dSSLocale;
    
    /**
     * Get the status of Lens
     *
     * @param type
     * @param locale
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Info, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<InfoResponse> getInfo(@RequestParam(value = "type", required = false) String type, @RequestParam(value = "locale", required = false) String locale) throws Exception {
        LOGGER.info("Get Info. Type - " + type + " Locale - " + locale);
        ApiContext apiContext = _helper.getApiContext();
        try {
            InfoResponse infoResponse = new InfoResponse();
            ApiResponse response;
            
            // Update requestContent
            InfoRequest infoRequest = new InfoRequest();
            infoRequest.setInstanceType(type);
            infoRequest.setLocale(locale);

            apiContext.setInstanceType(infoRequest.getInstanceType());
            apiContext.setLocale(infoRequest.getLocale());

            apiContext.setRequestContent(infoRequest);
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(type)) {
                LOGGER.error("Invalid instance type - " + type);
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }

            response = utilityService.info(infoRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            infoResponse.setStatus(response.status);
            infoResponse.setStatusCode(response.statusCode);
            infoResponse.setResponseData(response.responseData);
            infoResponse.setRequestId(response.requestId);
            infoResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(infoResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_INFO_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get Lens/XRay status
     *
     * @param type
     * @param locale
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Ping, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<PingResponse> getPing(@RequestParam(value = "type", required = false) String type, @RequestParam(value = "locale", required = false) String locale) throws Exception {
        LOGGER.info("Get Ping. Type - " + type + "Locale - " + locale);
        ApiContext apiContext = _helper.getApiContext();
        try {
            PingResponse pingResponse = new PingResponse();
            ApiResponse response;
            
            // Update requestContent         
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(type);
            pingRequest.setLocale(locale);

            apiContext.setInstanceType(pingRequest.getInstanceType());
            apiContext.setLocale(pingRequest.getLocale());

            apiContext.setRequestContent(pingRequest);
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(type)) {
                LOGGER.error("Invalid instance type - " + type);
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }

            response = utilityService.ping(pingRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            pingResponse.setStatus(response.status);
            pingResponse.setResponseData(response.responseData);
            pingResponse.setRequestId(response.requestId);
            pingResponse.setStatusCode(response.statusCode);
            pingResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(pingResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_PING_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Convert binary data
     *
     * @param convertRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Converter, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ConvertResponse> convertBinaryData(@RequestBody ConvertRequest convertRequest) throws Exception {
        LOGGER.info("Convert binary data. Type - " + convertRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            ConvertResponse convertResponse = new ConvertResponse();
            
            // Update requestContent            
            apiContext.setRequestContent(convertRequest);
            apiContext.setInstanceType(convertRequest.getInstanceType());
            apiContext.setLocale(convertRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(convertRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + convertRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
            response = utilityService.convertBinaryData(convertRequest);

            if (_helper.isNotNullOrEmpty(convertRequest.getLocale())) {
                apiContext.setLocale(convertRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            convertResponse.setStatus(response.status);
            convertResponse.setResponseData(response.responseData);
            convertResponse.setRequestId(response.requestId);
            convertResponse.setStatusCode(response.statusCode);
            convertResponse.setTimeStamp(response.timeStamp);
            convertResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(convertResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONVERT_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Canon binary data
     *
     * @param canonRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Canonicalizer, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<CanonResponse> canonBinaryData(@RequestBody CanonRequest canonRequest) throws Exception {
        LOGGER.info("Canon binary data. Type - " + canonRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            CanonResponse canonResponse = new CanonResponse();
            
            // Update requestContent            
            apiContext.setRequestContent(canonRequest);
            apiContext.setInstanceType(canonRequest.getInstanceType());
            apiContext.setLocale(canonRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(canonRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + canonRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
            
            response = utilityService.canonBinaryData(canonRequest);

            if (_helper.isNotNullOrEmpty(canonRequest.getLocale())) {
                apiContext.setLocale(canonRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            canonResponse.setStatus(response.status);
            canonResponse.setResponseData(response.responseData);
            canonResponse.setRequestId(response.requestId);
            canonResponse.setStatusCode(response.statusCode);
            canonResponse.setTimeStamp(response.timeStamp);
            canonResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(canonResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CANON_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume
     *
     * @param parseResumeRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.ResumeParser, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseResumeResponse> parseResume(@RequestBody ParseResumeRequest parseResumeRequest) throws Exception {
        LOGGER.info("Parse resume request");
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            ParseResumeResponse parseResumeResponse = new ParseResumeResponse();

            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setLocale(parseResumeRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
             //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = apiContext.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,"");
               }catch(Exception e){
                   LOGGER.error("Exception - " + e.getMessage());
                   response = utilityService.parseResume(parseResumeRequest);
               }
            }else{               
                response = utilityService.parseResume(parseResumeRequest);
            }
            }
            else{
            response = utilityService.parseResume(parseResumeRequest);
            }

            if (_helper.isNotNullOrEmpty(parseResumeRequest.getLocale())) {
                apiContext.setLocale(parseResumeRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            //apiContext.setParseVariant(VariantType.json.toString());
            // resume parsing without variant
            apiContext.setParseVariant(null);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseResumeResponse.setStatus(response.status);
            parseResumeResponse.setStatusCode(response.statusCode);

            parseResumeResponse.setResponseData(response.responseData);
            parseResumeResponse.setRequestId(response.requestId);
            parseResumeResponse.setTimeStamp(response.timeStamp);
            parseResumeResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseResumeResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume
     *
     * @param parseResumeBGXMLRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.ResumeParserBGXML, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseResumeBGXMLResponse> parseResumeBGXML(@RequestBody ParseResumeBGXMLRequest parseResumeBGXMLRequest) throws Exception {
        LOGGER.info("Parse resume BGXML. Type - " + parseResumeBGXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseResumeBGXMLResponse parseResumeBGXMLResponse = new ParseResumeBGXMLResponse();
            

            // Update requestContent           
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeBGXMLRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeBGXMLRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeBGXMLRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeBGXMLRequest.getLocale());
            
            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setLocale(parseResumeBGXMLRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeBGXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeBGXMLRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }                      
             //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService ){
                String DSSLocale = apiContext.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.bgtxml.toString());
               }catch(Exception e){
                   LOGGER.error("Exception - " + e.getMessage());
                   response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.bgtxml.toString());
               }
            }else{               
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.bgtxml.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.bgtxml.toString());
            }
            if (_helper.isNotNullOrEmpty(parseResumeBGXMLRequest.getLocale())) {
                apiContext.setLocale(parseResumeBGXMLRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            apiContext.setParseVariant(VariantType.bgtxml.toString());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseResumeBGXMLResponse.setStatus(response.status);
            parseResumeBGXMLResponse.setStatusCode(response.statusCode);

            parseResumeBGXMLResponse.setResponseData(response.responseData);
            parseResumeBGXMLResponse.setRequestId(response.requestId);
            parseResumeBGXMLResponse.setTimeStamp(response.timeStamp);
            parseResumeBGXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeBGXMLResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseResumeBGXMLResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume / structured
     *
     * @param parseResumeStructuredBGXMLRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.ResumeParserStructuredBGXML, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseResumeStructuredBGXMLResponse> parseResumeStructuredBGXML(@RequestBody ParseResumeStructuredBGXMLRequest parseResumeStructuredBGXMLRequest) throws Exception {
        LOGGER.info("Parse resume BGXML. Type - " + parseResumeStructuredBGXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response=null;
            ParseResumeStructuredBGXMLResponse parseResumeStructuredBGXMLResponse = new ParseResumeStructuredBGXMLResponse();
            
            // Update requestContent           
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeStructuredBGXMLRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeStructuredBGXMLRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeStructuredBGXMLRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeStructuredBGXMLRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setLocale(parseResumeRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeStructuredBGXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeStructuredBGXMLRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
             //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = apiContext.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.structuredbgtxml.toString());
               }catch(Exception e){
                   LOGGER.error("Exception - " + e.getMessage());
                   response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.structuredbgtxml.toString());
               }
            }else{
                response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.structuredbgtxml.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.structuredbgtxml.toString());
            }  
            
            if (_helper.isNotNullOrEmpty(parseResumeStructuredBGXMLRequest.getLocale())) {
                apiContext.setLocale(parseResumeStructuredBGXMLRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            apiContext.setParseVariant(VariantType.structuredbgtxml.toString());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseResumeStructuredBGXMLResponse.setStatus(response.status);
            parseResumeStructuredBGXMLResponse.setStatusCode(response.statusCode);

            parseResumeStructuredBGXMLResponse.setResponseData(response.responseData);
            parseResumeStructuredBGXMLResponse.setRequestId(response.requestId);
            parseResumeStructuredBGXMLResponse.setTimeStamp(response.timeStamp);
            parseResumeStructuredBGXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeStructuredBGXMLResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseResumeStructuredBGXMLResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume
     *
     * @param parseResumeHRXMLRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.ResumeParserHRXML, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseResumeHRXMLResponse> parseResumeHRXML(@RequestBody ParseResumeHRXMLRequest parseResumeHRXMLRequest) throws Exception {
        LOGGER.info("Parse resume HRXML. Type - " + parseResumeHRXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseResumeHRXMLResponse parseResumeHRXMLResponse = new ParseResumeHRXMLResponse();
            
            // Update requestContent           
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeHRXMLRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeHRXMLRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeHRXMLRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeHRXMLRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setLocale(parseResumeHRXMLRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeHRXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeHRXMLRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
             //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = apiContext.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.hrxml.toString());
               }catch(Exception e){
                   LOGGER.error("Exception - " + e.getMessage());
                   response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.hrxml.toString());
               }
            }else{
                response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.hrxml.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.hrxml.toString());
            }
            if (_helper.isNotNullOrEmpty(parseResumeHRXMLRequest.getLocale())) {
                apiContext.setLocale(parseResumeHRXMLRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            apiContext.setParseVariant(VariantType.hrxml.toString());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseResumeHRXMLResponse.setStatus(response.status);
            parseResumeHRXMLResponse.setStatusCode(response.statusCode);

            parseResumeHRXMLResponse.setResponseData(response.responseData);
            parseResumeHRXMLResponse.setRequestId(response.requestId);
            parseResumeHRXMLResponse.setTimeStamp(response.timeStamp);
            parseResumeHRXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeHRXMLResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseResumeHRXMLResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume
     *
     * @param parseResumeWithRTFRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.ResumeParserRTF, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseResumeWithRTFResponse> parseResumeRTF(@RequestBody ParseResumeWithRTFRequest parseResumeWithRTFRequest) throws Exception {
        LOGGER.info("Parse resume RTF. Type - " + parseResumeWithRTFRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            ParseResumeWithRTFResponse parseResumeWithRTFResponse = new ParseResumeWithRTFResponse();
            
            // Update requestContent           
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeWithRTFRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeWithRTFRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeWithRTFRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeWithRTFRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setLocale(parseResumeWithRTFRequest.getLocale());
            apiContext.setParseVariant(VariantType.rtf.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeWithRTFRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeWithRTFRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }     
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = apiContext.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.rtf.toString());
               }catch(Exception e){
                    LOGGER.error("Exception - " + e.getMessage());
                    response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.rtf.toString());
               }
            }else{
                response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.rtf.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.rtf.toString());
            }
            if (_helper.isNotNullOrEmpty(parseResumeWithRTFRequest.getLocale())) {
                apiContext.setLocale(parseResumeWithRTFRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseResumeWithRTFResponse.setStatus(response.status);
            parseResumeWithRTFResponse.setStatusCode(response.statusCode);

            parseResumeWithRTFResponse.setResponseData(response.responseData);
            parseResumeWithRTFResponse.setRequestId(response.requestId);
            parseResumeWithRTFResponse.setTimeStamp(response.timeStamp);
            parseResumeWithRTFResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeWithRTFResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseResumeWithRTFResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resumethHTMRequest
     * @param parseResumeWithHTMRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.ResumeParserHTM, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseResumeWithHTMResponse> parseResumeHTM(@RequestBody ParseResumeWithHTMRequest parseResumeWithHTMRequest) throws Exception {
        LOGGER.info("Parse resume HTM. Type - " + parseResumeWithHTMRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseResumeWithHTMResponse parseResumeWithHTMResponse = new ParseResumeWithHTMResponse();
            
            // Update requestContent           
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeWithHTMRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeWithHTMRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeWithHTMRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeWithHTMRequest.getLocale());
            apiContext.setLocale(parseResumeWithHTMRequest.getLocale());
            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.htm.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeWithHTMRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeWithHTMRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
             //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = apiContext.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.htm.toString());
               }catch(Exception e){
                    LOGGER.error("Exception - " + e.getMessage());
                    response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.htm.toString());
               }
            }else{
                response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.htm.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.htm.toString());
            }
            if (_helper.isNotNullOrEmpty(parseResumeWithHTMRequest.getLocale())) {
                apiContext.setLocale(parseResumeWithHTMRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseResumeWithHTMResponse.setStatus(response.status);
            parseResumeWithHTMResponse.setStatusCode(response.statusCode);

            parseResumeWithHTMResponse.setResponseData(response.responseData);
            parseResumeWithHTMResponse.setRequestId(response.requestId);
            parseResumeWithHTMResponse.setTimeStamp(response.timeStamp);
            parseResumeWithHTMResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeWithHTMResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseResumeWithHTMResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse jobs
     *
     * @param parseJobRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.JobParser, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseJobResponse> parseJob(@RequestBody ParseJobRequest parseJobRequest) throws Exception {
        LOGGER.info("Parse job request");
        ApiContext apiContext = _helper.getApiContext();
        try {
            ParseJobResponse parseJobResponse = new ParseJobResponse();
            ApiResponse response;
            // Update requestContent     
            apiContext.setRequestContent(parseJobRequest);
            apiContext.setInstanceType(parseJobRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.json.toString());
            apiContext.setLocale(parseJobRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
            response = utilityService.parseJob(parseJobRequest);

            if (_helper.isNotNullOrEmpty(parseJobRequest.getLocale())) {
                apiContext.setLocale(parseJobRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseJobResponse.setStatus(response.status);
            parseJobResponse.setStatusCode(response.statusCode);
            parseJobResponse.setResponseData(response.responseData);
            parseJobResponse.setRequestId(response.requestId);
            parseJobResponse.setTimeStamp(response.timeStamp);
            parseJobResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseJobResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse jobs
     *
     * @param parseJobBGXMLRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.JobParserBGXML, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseJobBGXMLResponse> parseJobBGXML(@RequestBody ParseJobBGXMLRequest parseJobBGXMLRequest) throws Exception {

        LOGGER.info("Parse job BGXML. Type - " + parseJobBGXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseJobBGXMLResponse parseJobBGXMLResponse = new ParseJobBGXMLResponse();
            
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(parseJobBGXMLRequest.getInstanceType());
            parseJobRequest.setBinaryData(parseJobBGXMLRequest.getBinaryData());
            parseJobRequest.setExtension(parseJobBGXMLRequest.getExtension());
            parseJobRequest.setLocale(parseJobBGXMLRequest.getLocale());

            // Update requestContent            
            apiContext.setRequestContent(parseJobBGXMLRequest);
            apiContext.setInstanceType(parseJobBGXMLRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.bgtxml.toString());
            apiContext.setLocale(parseJobBGXMLRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobBGXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobBGXMLRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }

            response = utilityService.parseJobVariant(parseJobRequest, VariantType.bgtxml.toString());

            if (_helper.isNotNullOrEmpty(parseJobBGXMLRequest.getLocale())) {
                apiContext.setLocale(parseJobBGXMLRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }
            
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseJobBGXMLResponse.setStatus(response.status);
            parseJobBGXMLResponse.setStatusCode(response.statusCode);
            parseJobBGXMLResponse.setResponseData(response.responseData);
            parseJobBGXMLResponse.setRequestId(response.requestId);
            parseJobBGXMLResponse.setTimeStamp(response.timeStamp);
            parseJobBGXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobBGXMLResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseJobBGXMLResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
    
    /**
     * Parse jobs / structured
     *
     * @param parseJobStructuredBGXMLRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.JobParserStructuredBGXML, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseJobStructuredBGXMLResponse> parseJobStructuredBGXML(@RequestBody ParseJobStructuredBGXMLRequest parseJobStructuredBGXMLRequest) throws Exception {

        LOGGER.info("Parse job BGXML. Type - " + parseJobStructuredBGXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseJobStructuredBGXMLResponse parseJobStructuredBGXMLResponse = new ParseJobStructuredBGXMLResponse();
            
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(parseJobStructuredBGXMLRequest.getInstanceType());
            parseJobRequest.setBinaryData(parseJobStructuredBGXMLRequest.getBinaryData());
            parseJobRequest.setExtension(parseJobStructuredBGXMLRequest.getExtension());
            parseJobRequest.setLocale(parseJobStructuredBGXMLRequest.getLocale());

            // Update requestContent            
            apiContext.setRequestContent(parseJobStructuredBGXMLRequest);
            apiContext.setInstanceType(parseJobStructuredBGXMLRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.structuredbgtxml.toString());
            apiContext.setLocale(parseJobStructuredBGXMLRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobStructuredBGXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobStructuredBGXMLRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }

            response = utilityService.parseJobVariant(parseJobRequest, VariantType.structuredbgtxml.toString());

            if (_helper.isNotNullOrEmpty(parseJobStructuredBGXMLRequest.getLocale())) {
                apiContext.setLocale(parseJobStructuredBGXMLRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseJobStructuredBGXMLResponse.setStatus(response.status);
            parseJobStructuredBGXMLResponse.setStatusCode(response.statusCode);
            parseJobStructuredBGXMLResponse.setResponseData(response.responseData);
            parseJobStructuredBGXMLResponse.setRequestId(response.requestId);
            parseJobStructuredBGXMLResponse.setTimeStamp(response.timeStamp);
            parseJobStructuredBGXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobStructuredBGXMLResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseJobStructuredBGXMLResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse jobs
     *
     * @param parseJobWithRTFRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.JobParserRTF, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseJobWithRTFResponse> parseJobRTF(@RequestBody ParseJobWithRTFRequest parseJobWithRTFRequest) throws Exception {

        LOGGER.info("Parse job RTF. Type - " + parseJobWithRTFRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseJobWithRTFResponse parseJobWithRTFResponse = new ParseJobWithRTFResponse();
            
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(parseJobWithRTFRequest.getInstanceType());
            parseJobRequest.setBinaryData(parseJobWithRTFRequest.getBinaryData());
            parseJobRequest.setExtension(parseJobWithRTFRequest.getExtension());
            parseJobRequest.setLocale(parseJobWithRTFRequest.getLocale());

            // Update requestContent            
            apiContext.setRequestContent(parseJobWithRTFRequest);
            apiContext.setInstanceType(parseJobWithRTFRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.rtf.toString());
            apiContext.setLocale(parseJobWithRTFRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobWithRTFRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobWithRTFRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }
            
            response = utilityService.parseJobVariant(parseJobRequest, VariantType.rtf.toString());

            if (_helper.isNotNullOrEmpty(parseJobWithRTFRequest.getLocale())) {
                apiContext.setLocale(parseJobWithRTFRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }
            
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseJobWithRTFResponse.setStatus(response.status);
            parseJobWithRTFResponse.setStatusCode(response.statusCode);
            parseJobWithRTFResponse.setResponseData(response.responseData);
            parseJobWithRTFResponse.setRequestId(response.requestId);
            parseJobWithRTFResponse.setTimeStamp(response.timeStamp);
            parseJobWithRTFResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobWithRTFResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseJobWithRTFResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse jobs
     *
     * @param parseJobWithHTMRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.JobParserHTM, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ParseJobWithHTMResponse> parseJobHTM(@RequestBody ParseJobWithHTMRequest parseJobWithHTMRequest) throws Exception {

        LOGGER.info("Parse job HTM. Type - " + parseJobWithHTMRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseJobWithHTMResponse parseJobWithHTMResponse = new ParseJobWithHTMResponse();
            
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(parseJobWithHTMRequest.getInstanceType());
            parseJobRequest.setBinaryData(parseJobWithHTMRequest.getBinaryData());
            parseJobRequest.setExtension(parseJobWithHTMRequest.getExtension());
            parseJobRequest.setLocale(parseJobWithHTMRequest.getLocale());

            // Update requestContent            
            apiContext.setRequestContent(parseJobWithHTMRequest);
            apiContext.setInstanceType(parseJobWithHTMRequest.getInstanceType());
            apiContext.setLocale(parseJobWithHTMRequest.getLocale());
            apiContext.setParseVariant(VariantType.htm.toString());
            apiContext.setLocale(parseJobWithHTMRequest.getLocale());
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobWithHTMRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobWithHTMRequest.getInstanceType());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
            }

            response = utilityService.parseJobVariant(parseJobRequest, VariantType.htm.toString());

            if (_helper.isNotNullOrEmpty(parseJobWithHTMRequest.getLocale())) {
                apiContext.setLocale(parseJobWithHTMRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }
            
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseJobWithHTMResponse.setStatus(response.status);
            parseJobWithHTMResponse.setStatusCode(response.statusCode);
            parseJobWithHTMResponse.setResponseData(response.responseData);
            parseJobWithHTMResponse.setRequestId(response.requestId);
            parseJobWithHTMResponse.setTimeStamp(response.timeStamp);
            parseJobWithHTMResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobWithHTMResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(parseJobWithHTMResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    @RequestMapping(value = UriConstants.FindLocale, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ProcessLocaleResponse> findLocale(@RequestBody ProcessLocaleRequest processLocaleRequest) throws Exception {
        LOGGER.info("Process Locale");
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            ProcessLocaleResponse processLocaleResponse = new ProcessLocaleResponse();
            apiContext.setRequestContent(processLocaleRequest);
            apiContext.setInstanceType(com.bgt.lens.helpers.Enum.InstanceType.LD.toString());
            
            _helper.setApiContext(apiContext);

            response = utilityService.findLocale(processLocaleRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            processLocaleResponse.setStatus(response.status);
            processLocaleResponse.setStatusCode(response.statusCode);
            processLocaleResponse.setResponseData(response.responseData);
            processLocaleResponse.setRequestId(response.requestId);
            processLocaleResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(processLocaleResponse, HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.PARSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}
