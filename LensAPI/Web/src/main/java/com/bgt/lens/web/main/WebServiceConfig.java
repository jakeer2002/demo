// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.main;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

/**
 * *
 * Web service URL configuration
 */
@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    /**
     * *
     * SOAP message dispatcher servlet initializer
     *
     * @param applicationContext
     * @return
     */
    @Bean
    public ServletRegistrationBean createServletRegistrationBean(ApplicationContext applicationContext) {
        MessageDispatcherServlet soapServlet = new MessageDispatcherServlet();
        soapServlet.setApplicationContext(applicationContext);
        soapServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean <> (soapServlet,"/*");
    }

    /**
     * *
     * REST dispatcher servlet initializer
     *
     * @param applicationContext
     * @return
     */
    @Bean
    public ServletRegistrationBean mcreateServletRegistrationBean(ApplicationContext applicationContext) {
        DispatcherServlet restServlet = new DispatcherServlet();
        restServlet.setApplicationContext(applicationContext);
        String[] url = {"/adminservice/*", "/parserservice/*"};
        return new ServletRegistrationBean <> (restServlet, url);
    }

    /**
     * *
     * Admin Service SOAP wsdl definition
     *
     * @param adminServiceSoapSchema
     * @return
     */
    @Bean(name = "adminservice")
    public DefaultWsdl11Definition defaultWsdl11DefinitionForAdminService(XsdSchema adminServiceSoapSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("AdminServicePort");
        wsdl11Definition.setLocationUri("/adminservice.wsdl");
        wsdl11Definition.setTargetNamespace("http://bgt.com/lens/model/adminservice");
        wsdl11Definition.setSchema(adminServiceSoapSchema);
        return wsdl11Definition;
    }

    /**
     * *
     * Parser Service SOAP wsdl definition
     *
     * @param parserServiceSoapSchema
     * @return
     */
    @Bean(name = "parserservice")
    public DefaultWsdl11Definition defaultWsdl11DefinitionForParserService(XsdSchema parserServiceSoapSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("ParserServicePort");
        wsdl11Definition.setLocationUri("/parserservice.wsdl");
        wsdl11Definition.setTargetNamespace("http://bgt.com/lens/model/parserservice");
        wsdl11Definition.setSchema(parserServiceSoapSchema);
        return wsdl11Definition;
    }

    /**
     * *
     * SOAP XSD schema configuration
     *
     * @return
     */
    @Bean
    public XsdSchema adminServiceSoapSchema() {
        return new SimpleXsdSchema(new ClassPathResource("adminservice.xsd"));
    }

    /**
     * *
     * SOAP XSD schema configuration
     *
     * @return
     */
    @Bean
    public XsdSchema parserServiceSoapSchema() {
        return new SimpleXsdSchema(new ClassPathResource("parserservice.xsd"));
    }

}
