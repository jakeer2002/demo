// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.rest;

import java.util.Calendar;
import javax.servlet.ServletContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * *
 * Index Controller
 */
@RestController
public class IndexController {

    private final Logger logger = LogManager.getLogger(IndexController.class);
    
    @Autowired
    ServletContext servletContext;

    /**
     * *
     * Index
     *
     * @return
     */
    @RequestMapping("/")
    public String index() {
        logger.info("Index page loaded successfully");
        return "<html><body style=\"font-family:Ubuntu, Trebuchet MS, Helvetica, Arial, sans-serif\">LENS api v"+(String) servletContext.getAttribute("applicationVersion")+"<br/><br/>"
                + "Copyright &copy; " + Integer.toString(Calendar.getInstance().get(Calendar.YEAR)) + " Burning Glass International Inc."
                + "<br/><br/>Proprietary and Confidential<br/>"
                + "<br/>All rights are reserved. Reproduction or transmission "
                + "in whole or in part, in<br/> any form or by any means, electronic,"
                + " mechanical or otherwise, is prohibited<br/> without the "
                + "prior written consent of Burning Glass International Inc."
                + "<br/></body></html>";
    }

    @RequestMapping("")
    public String indexPage() {
        logger.info("Index page loaded successfully");
        return "<html><body style=\"font-family:Ubuntu, Trebuchet MS, Helvetica, Arial, sans-serif\">LENS api v"+(String) servletContext.getAttribute("applicationVersion")+"<br/><br/>"
                + "Copyright &copy; " + Integer.toString(Calendar.getInstance().get(Calendar.YEAR)) + " Burning Glass International Inc."
                + "<br/><br/>Proprietary and Confidential<br/>"
                + "<br/>All rights are reserved. Reproduction or transmission "
                + "in whole or in part, in<br/> any form or by any means, electronic,"
                + " mechanical or otherwise, is prohibited<br/> without the "
                + "prior written consent of Burning Glass International Inc."
                + "<br/></body></html>";
    }
}
