// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.interceptor;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.services.logger.ILoggerService;
import com.bgt.lens.web.rest.UriConstants;
import java.net.InetAddress;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * Request processing handler class simplified implementation of
 * pre-only/post-only interceptors.
 */
public class RequestProcessingInterceptor implements HandlerInterceptor {

    private static final Logger LOGGER = LogManager.getLogger(RequestProcessingInterceptor.class);
    
    private final Helper _helper = new Helper();
    
    @Autowired
    private ILoggerService loggerService;
    
    @Value("${AddAdvancedLogToELK}")
    private boolean addAdvancedLogToELK;
    
    @Autowired(required = false)
    private ConcurrentRequestLimiter concurrentRequestLimiter;

    /**
     * handle the request before sent to the controllers
     *
     * @param request
     * @param response
     * @param handler
     * @return boolean
     * @throws java.lang.Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {

        if (_helper.isNotNull(concurrentRequestLimiter)) {
            String userName = _helper.getUserNameFromRequest(request);
            concurrentRequestLimiter.acquire(userName);
        }

        long startTime = System.currentTimeMillis();
        Date startDateTime = Date.from(Instant.now());
        LOGGER.info("Request URL::" + request.getRequestURL().toString() + ":: Start Time=" + System.currentTimeMillis());
        request.setAttribute("startTime", startTime);
        InetAddress ip = InetAddress.getLocalHost();
        ApiContext apiContext = new ApiContext();

        //Get server and client address
        apiContext.setServerAddress(ip.getHostName() + "/" + ip.getHostAddress());

        String clientAddress = request.getHeader("x-clientip");
        if (_helper.isNotNullOrEmpty(clientAddress)) {
            apiContext.setClientAddress(clientAddress);
        } else {
            apiContext.setClientAddress(request.getRemoteAddr());
        }

        apiContext.setRequestId(UUID.randomUUID());
        apiContext.setInTime(startDateTime);
        if (_helper.isNullOrEmpty(request.getQueryString())) {
            apiContext.setRequestUri(request.getRequestURL().toString());
        } else {
            apiContext.setRequestUri(request.getRequestURL().append('?').append(request.getQueryString()).toString());
        }
        apiContext.setMethod(request.getMethod());
        apiContext.setAdvanceLog(new AdvanceLog());
        String accept = request.getHeader("accept");
        String contentType = request.getHeader("content-type");

        if (accept != null) {
            apiContext.setAccept(accept);
        } else {
            apiContext.setAccept("application/xml");
        }

        if (contentType != null) {
            apiContext.setContentType(contentType);
        } else {
            apiContext.setContentType("application/xml");
        }

        // Get urlContext - Action name
        String urlContext = request.getRequestURI().replace(request.getContextPath() + request.getServletPath(), "");

        if (urlContext.equalsIgnoreCase(UriConstants.Info)) {
            apiContext.setReturnType("InfoResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Ping)) {
            apiContext.setReturnType("PingResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Converter)) {
            apiContext.setReturnType("ConvertResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Canonicalizer)) {
            apiContext.setReturnType("CanonResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.ResumeParser)) {
            apiContext.setReturnType("ParseResumeResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.ResumeParserBGXML)) {
            apiContext.setReturnType("ParseResumeBGXMLResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.ResumeParserStructuredBGXML)) {
            apiContext.setReturnType("ParseResumeStructuredBGXMLResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.ResumeParserHRXML)) {
            apiContext.setReturnType("ParseResumeHRXMLResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.ResumeParserRTF)) {
            apiContext.setReturnType("ParseResumeWithRTFResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.ResumeParserHTM)) {
            apiContext.setReturnType("ParseResumeWithHTMResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.JobParser)) {
            apiContext.setReturnType("ParseJobResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.JobParserBGXML)) {
            apiContext.setReturnType("ParseJobBGXMLResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.JobParserStructuredBGXML)) {
            apiContext.setReturnType("ParseJobStructuredBGXMLResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.JobParserHTM)) {
            apiContext.setReturnType("ParseJobWithHTMResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.JobParserRTF)) {
            apiContext.setReturnType("ParseJobWithRTFResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.FindLocale)) {
            apiContext.setReturnType("ProcessLocaleResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Api) && (apiContext.getMethod().equalsIgnoreCase("POST"))) {
            apiContext.setReturnType("CreateApiResponse");
        } else if (urlContext.contains(UriConstants.Api) && (apiContext.getMethod().equalsIgnoreCase("DELETE"))) {
            apiContext.setReturnType("DeleteApiResponse");
        } else if (urlContext.contains(UriConstants.Api) && (apiContext.getMethod().equalsIgnoreCase("PUT"))) {
            apiContext.setReturnType("UpdateApiResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Api) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetApiResponse");
        } else if (urlContext.contains(UriConstants.Api) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetApiByIdResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Resources) && (apiContext.getMethod().equalsIgnoreCase("POST"))) {
            apiContext.setReturnType("AddResourceResponse");
        } else if (urlContext.contains(UriConstants.Resources) && (apiContext.getMethod().equalsIgnoreCase("DELETE"))) {
            apiContext.setReturnType("DeleteResourceResponse");
        } else if (urlContext.contains(UriConstants.Resources) && (apiContext.getMethod().equalsIgnoreCase("PUT"))) {
            apiContext.setReturnType("UpdateResourceResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Resources) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetResourceResponse");
        } else if (urlContext.contains(UriConstants.Resources) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetResourceByIdResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Lens) && (apiContext.getMethod().equalsIgnoreCase("POST"))) {
            apiContext.setReturnType("AddLensSettingsResponse");
        } else if (urlContext.contains(UriConstants.Lens) && (apiContext.getMethod().equalsIgnoreCase("PUT"))) {
            apiContext.setReturnType("UpdateLensSettingsResponse");
        } else if (urlContext.contains(UriConstants.Lens) && (apiContext.getMethod().equalsIgnoreCase("DELETE"))) {
            apiContext.setReturnType("DeleteLensSettingsResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Lens) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetLensSettingsResponse");
        } else if (urlContext.contains(UriConstants.Lens) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetLensSettingsByIdResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Clients) && (apiContext.getMethod().equalsIgnoreCase("POST"))) {
            apiContext.setReturnType("CreateConsumerResponse");
        } else if (urlContext.contains(UriConstants.Clients) && (apiContext.getMethod().equalsIgnoreCase("PUT"))) {
            apiContext.setReturnType("UpdateConsumerResponse");
        } else if (urlContext.contains(UriConstants.Clients) && (apiContext.getMethod().equalsIgnoreCase("DELETE"))) {
            apiContext.setReturnType("DeleteConsumerResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.Clients) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetClientsResponse");
        } else if (urlContext.contains(UriConstants.Clients) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetClientByIdResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.LicenseAll)) {
            apiContext.setReturnType("GetAllLicenseDetailsResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.License)) {
            apiContext.setReturnType("GetLicenseResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.UsageLogs) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetUsageLogsResponse");
        } else if (urlContext.contains(UriConstants.UsageLogs) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetUsageLogByIdResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.ErrorLogs) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetErrorLogsResponse");
        } else if (urlContext.contains(UriConstants.ErrorLogs) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetErrorLogByIdResponse");
        } else if (urlContext.equalsIgnoreCase(UriConstants.UsageCounters) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetUsageCounterResponse");
        } else if (urlContext.contains(UriConstants.UsageCounters) && (apiContext.getMethod().equalsIgnoreCase("GET"))) {
            apiContext.setReturnType("GetUsageCounterByIdResponse");
        } 
        
        if (_helper.isNullOrEmpty(urlContext)) {
            response.sendRedirect(request.getContextPath() + request.getServletPath() + "/");
            return false;
        }

        // Get resource name
        String resourceContext = request.getRequestURI().replace(request.getContextPath(), "");
        apiContext.setResource(_helper.getResourceName(resourceContext, apiContext));
        request.setAttribute("ApiContext", apiContext);
        return true;
    }

    /**
     * handle the request before sent to the controllers
     *
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws java.lang.Exception
     */
    @Override
    public void postHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        if (_helper.isNotNull(concurrentRequestLimiter)) {
            String userName = _helper.getUserNameFromRequest(request);
            concurrentRequestLimiter.release(userName);
        }
        System.out.println("Request URL::" + request.getRequestURL().toString()
                + " Sent to Handler :: Current Time=" + System.currentTimeMillis());
    }

    /**
     * handle the request before sent to the controllers
     *
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws java.lang.Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request,
            HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        Date endDateTime = Date.from(Instant.now());
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setOutTime(endDateTime);
        apiContext.setSeconds(_helper.getTimeDifference(apiContext.getInTime(), apiContext.getOutTime()));

        if (apiContext.isAdvanceLog()) {
            LOGGER.info("Add advanced log to file system");
            _helper.updatePerformanceLog(apiContext);
            LOGGER.info("Add advanced log to elastic search instance");
            
            if(addAdvancedLogToELK){
                loggerService.addAdvancedLog(apiContext);
            }
        }
        
        HttpServletResponse httpServletResponse = RequestAndResponseContextHolder.response();
        httpServletResponse.setHeader("X-Rate-Limit-Limit", apiContext.getRequestCount());
        httpServletResponse.setHeader("X-Rate-Limit-Remaining", apiContext.getRemainingRequestCount());
        httpServletResponse.setHeader("X-Rate-Limit-Reset", String.valueOf(apiContext.getRetryTimeStamp()));
 
        long startTime = (Long) request.getAttribute("startTime");
        LOGGER.info("Request URL::" + request.getRequestURL().toString()
                + ":: End Time=" + System.currentTimeMillis());
        LOGGER.info("Request URL::" + request.getRequestURL().toString()
                + ":: Time Taken in Milliseconds =" + (System.currentTimeMillis() - startTime) + ":: Time Taken in Seconds =" + ((System.currentTimeMillis() - startTime) / 1000.0));

    }

}
