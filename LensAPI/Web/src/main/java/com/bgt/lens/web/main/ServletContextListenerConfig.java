 // <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.main;

import java.io.File;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingRandomAccessFileAppender;
import org.springframework.context.annotation.Configuration;

/**
 * ServletContextListener Class
 */
@Configuration
public class ServletContextListenerConfig {

    private static final Logger LOGGER = LogManager.getLogger(ServletContextListenerConfig.class);
    private static final Logger ADLOGGER = LogManager.getLogger("AdvanceLogger");

    /**
     * Initialized Servlet Context
     */
    @PostConstruct
    public void contextInitialized() {
        LOGGER.info("Context Initialized");
        try{

            LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
            org.apache.logging.log4j.core.config.Configuration config = ctx.getConfiguration();
            RollingRandomAccessFileAppender techical = (RollingRandomAccessFileAppender) config.getAppender("AdvanceLogger");
            File file = new File(techical.getFileName());

            if (!file.canWrite()) {
                LOGGER.error("Unable to create/update advanced log");
            }     
        }
        catch(Exception ex){
            LOGGER.error(ex.getMessage());
        }

    }

    /**
     * Destroy Servlet Context
     */
    @PreDestroy
    public void contextdestroyed() {
        LOGGER.info("Context Destroyed");

        // Now deregister JDBC drivers in this context's ClassLoader:
        // Get the webapp's ClassLoader
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        // Loop through all drivers
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            if (driver.getClass().getClassLoader() == cl) {
                // This driver was registered by the webapp's ClassLoader, so deregister it:
                try {
                    LOGGER.info("Deregistering JDBC driver {}", driver);
                    DriverManager.deregisterDriver(driver);
                } catch (SQLException ex) {
                    LOGGER.error("Error deregistering JDBC driver {}", driver, ex);
                }
            } else {
                // driver was not registered by the webapp's ClassLoader and may be in use elsewhere
                LOGGER.trace("Not deregistering JDBC driver {} as it does not belong to this webapp's ClassLoader", driver);
            }
        }
    }
}
