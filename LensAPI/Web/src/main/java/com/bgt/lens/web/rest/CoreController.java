// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.rest;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.DeleteApiRequest;
import com.bgt.lens.model.adminservice.request.DeleteConsumerRequest;
import com.bgt.lens.model.adminservice.request.DeleteLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.DeleteResourceRequest;
import com.bgt.lens.model.adminservice.request.GetApiByIdRequest;
import com.bgt.lens.model.adminservice.request.GetApiRequest;
import com.bgt.lens.model.adminservice.request.GetClientByIdRequest;
import com.bgt.lens.model.adminservice.request.GetClientsRequest;
import com.bgt.lens.model.adminservice.request.GetErrorLogByIdRequest;
import com.bgt.lens.model.adminservice.request.GetErrorLogsRequest;
import com.bgt.lens.model.adminservice.request.GetLensSettingsByIdRequest;
import com.bgt.lens.model.adminservice.request.GetLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.GetResourceByIdRequest;
import com.bgt.lens.model.adminservice.request.GetResourceRequest;
import com.bgt.lens.model.adminservice.request.GetUsageCounterByIdRequest;
import com.bgt.lens.model.adminservice.request.GetUsageCounterRequest;
import com.bgt.lens.model.adminservice.request.GetUsageLogByIdRequest;
import com.bgt.lens.model.adminservice.request.GetUsageLogsRequest;
import com.bgt.lens.model.adminservice.request.UpdateApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.UpdateResourceRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiList;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.ClientList;
import com.bgt.lens.model.rest.response.ErrorLog;
import com.bgt.lens.model.rest.response.ErrorLogList;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.LensSettingsList;
import com.bgt.lens.model.rest.response.License;
import com.bgt.lens.model.rest.response.LicenseList;
import com.bgt.lens.model.rest.response.ResourceList;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.UsageCounter;
import com.bgt.lens.model.rest.response.UsageCounterList;
import com.bgt.lens.model.rest.response.UsageLog;
import com.bgt.lens.model.rest.response.UsageLogList;
import com.bgt.lens.model.rest.response.adminservice.AddLensSettingsResponse;
import com.bgt.lens.model.rest.response.adminservice.AddResourceResponse;
import com.bgt.lens.model.rest.response.adminservice.CreateApiResponse;
import com.bgt.lens.model.rest.response.adminservice.CreateConsumerResponse;
import com.bgt.lens.model.rest.response.adminservice.DeleteApiResponse;
import com.bgt.lens.model.rest.response.adminservice.DeleteConsumerResponse;
import com.bgt.lens.model.rest.response.adminservice.DeleteLensSettingsResponse;
import com.bgt.lens.model.rest.response.adminservice.DeleteResourceResponse;
import com.bgt.lens.model.rest.response.adminservice.GetAllLicenseDetailsResponse;
import com.bgt.lens.model.rest.response.adminservice.GetApiByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetApiResponse;
import com.bgt.lens.model.rest.response.adminservice.GetClientByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetClientsResponse;
import com.bgt.lens.model.rest.response.adminservice.GetErrorLogByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetErrorLogsResponse;
import com.bgt.lens.model.rest.response.adminservice.GetLensSettingsByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetLensSettingsResponse;
import com.bgt.lens.model.rest.response.adminservice.GetLicenseResponse;
import com.bgt.lens.model.rest.response.adminservice.GetResourceByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetResourceResponse;
import com.bgt.lens.model.rest.response.adminservice.GetUsageCounterByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetUsageCounterResponse;
import com.bgt.lens.model.rest.response.adminservice.GetUsageLogByIdResponse;
import com.bgt.lens.model.rest.response.adminservice.GetUsageLogsResponse;
import com.bgt.lens.model.rest.response.adminservice.UpdateApiResponse;
import com.bgt.lens.model.rest.response.adminservice.UpdateConsumerResponse;
import com.bgt.lens.model.rest.response.adminservice.UpdateLensSettingsResponse;
import com.bgt.lens.model.rest.response.adminservice.UpdateResourceResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.license.ILicenseManagementService;
import com.bgt.lens.services.logger.ILoggerService;
import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * *
 * Core controller - Administration related functionalities
 */
@RestController
public class CoreController {

    @Autowired
    HttpServletRequest request;

    @Autowired
    ICoreService coreService;

    @Autowired
    ILoggerService loggerService;

    @Autowired
    ILicenseManagementService licenseService; 

    private static final Logger LOGGER = LogManager.getLogger(CoreController.class);
    private final Helper _helper = new Helper();

    /**
     * Creates new API
     *
     * @param api
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Api, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<CreateApiResponse> createApi(@RequestBody CreateApiRequest api) throws Exception {
        LOGGER.info("Create Api: ApiKey - " + api.getKey());
        ApiContext apiContext = _helper.getApiContext();
        try {
            CreateApiResponse createApiResponse = new CreateApiResponse();
            apiContext.setRequestContent(api);
            ApiResponse response = coreService.createApi(api);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            createApiResponse.setStatus(response.status);
            createApiResponse.setStatusCode(response.statusCode);
            createApiResponse.setResponseData(response.responseData);
            createApiResponse.setRequestId(response.requestId);
            createApiResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(createApiResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Update API
     *
     * @param apiId
     * @param api
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Api + "/{apiId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<UpdateApiResponse> updateApi(@PathVariable int apiId, @RequestBody UpdateApiRequest api) throws Exception {
        LOGGER.info("Update Api: ApId - " + apiId);
        api.setApiId(new BigInteger(Integer.toString(apiId)));
        ApiContext apiContext = _helper.getApiContext();
        try {
            UpdateApiResponse updateApiResponse = new UpdateApiResponse();
            apiContext.setRequestContent(api);
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.updateApi(apiId, api);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            updateApiResponse.setStatus(response.status);
            updateApiResponse.setStatusCode(response.statusCode);
            updateApiResponse.setResponseData(response.responseData);
            updateApiResponse.setRequestId(response.requestId);
            updateApiResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(updateApiResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get the list of API
     *
     * @param key
     * @param name
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Api, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetApiResponse> getApi(@RequestParam(value = "key", required = false) String key, @RequestParam(value = "name", required = false) String name) throws Exception {
        LOGGER.info("Get Api by Criteria: Key - " + key + ", " + name + ", ");
        ApiContext apiContext = _helper.getApiContext();
        try {
            GetApiResponse getApiResponse = new GetApiResponse();
            GetApiRequest api = new GetApiRequest();
            api.setApiKey(key);
            api.setApiName(name);
            apiContext.setRequestContent(api);
            
            _helper.setApiContext(apiContext);

            ApiResponse response = coreService.getApi(api);

            if (response.status == true) {
                ApiList apiList = new ApiList();
                apiList.setApiList((List<Api>) response.responseData);
                response.responseData = apiList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getApiResponse.setRequestId(response.requestId);
            getApiResponse.setStatus(response.status);
            getApiResponse.setStatusCode(response.statusCode);
            getApiResponse.setTimeStamp(response.timeStamp);
            getApiResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getApiResponse, HttpStatus.OK);

        } catch (Exception ex) {
            LOGGER.error("Exception " + ex.getMessage());
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_API_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get API by Id
     *
     * @param apiId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Api + "/{apiId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetApiByIdResponse> getApiById(@PathVariable int apiId) throws Exception {
        LOGGER.info("Get Api by Id: ApiId" + apiId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetApiByIdResponse getApiByIdResponse = new GetApiByIdResponse();
            GetApiByIdRequest getApiByIdRequest = new GetApiByIdRequest();
            getApiByIdRequest.setApiId(BigInteger.valueOf(apiId));
            apiContext.setRequestContent(getApiByIdRequest);
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.getApiById(apiId);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getApiByIdResponse.setRequestId(response.requestId);
            getApiByIdResponse.setStatus(response.status);
            getApiByIdResponse.setStatusCode(response.statusCode);
            getApiByIdResponse.setTimeStamp(response.timeStamp);
            getApiByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getApiByIdResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_API_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Deletes an API
     *
     * @param apiId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Api + "/{apiId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<DeleteApiResponse> deleteApi(@PathVariable int apiId) throws Exception {
        LOGGER.info("Delete Api: ApiId - " + apiId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            DeleteApiResponse deleteApiResponse = new DeleteApiResponse();
            DeleteApiRequest deleteApiRequest = new DeleteApiRequest();
            deleteApiRequest.setApiId(BigInteger.valueOf(apiId));
            apiContext.setRequestContent(deleteApiRequest);
            _helper.setApiContext(apiContext);
            ApiResponse response = coreService.deleteApi(apiId);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            deleteApiResponse.setStatus(response.status);
            deleteApiResponse.setStatusCode(response.statusCode);
            deleteApiResponse.setResponseData(response.responseData);
            deleteApiResponse.setRequestId(response.requestId);
            deleteApiResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(deleteApiResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_DELETE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Add new resource
     *
     * @param resource
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Resources, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<AddResourceResponse> addResource(@RequestBody AddResourceRequest resource) throws Exception {
        LOGGER.info("Add Resource : ResourceName - " + resource.getResourceName() + ", ApiKey" + resource.getApiKey());
        ApiContext apiContext = _helper.getApiContext();
        try {
            AddResourceResponse addResourceResponse = new AddResourceResponse();
            apiContext.setRequestContent(resource);
            _helper.setApiContext(apiContext);
            ApiResponse response = coreService.addResource(resource);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            addResourceResponse.setRequestId(response.requestId);
            addResourceResponse.setStatus(response.status);
            addResourceResponse.setStatusCode(response.statusCode);
            addResourceResponse.setTimeStamp(response.timeStamp);
            addResourceResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(addResourceResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Update resource
     *
     * @param resourceId
     * @param resource
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Resources + "/{resourceId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<UpdateResourceResponse> updateResource(@PathVariable Integer resourceId, @RequestBody UpdateResourceRequest resource) throws Exception {
        LOGGER.info("Update Resource: Resourcename - " + resource.getResourceName() + ", ApiKey - " + resource.getApiKey());
        ApiContext apiContext = _helper.getApiContext();

        try {
            UpdateResourceResponse updateResourceResponse = new UpdateResourceResponse();
            apiContext.setRequestContent(resource);
            _helper.setApiContext(apiContext);
            ApiResponse response = coreService.updateResource(resourceId, resource);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            updateResourceResponse.setRequestId(response.requestId);
            updateResourceResponse.setStatus(response.status);
            updateResourceResponse.setStatusCode(response.statusCode);
            updateResourceResponse.setTimeStamp(response.timeStamp);
            updateResourceResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(updateResourceResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get resource
     *
     * @param resourceName
     * @param apiKey
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Resources, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetResourceResponse> getResource(@RequestParam(value = "resourceName", required = false) String resourceName, @RequestParam(value = "apiKey", required = false) String apiKey) throws Exception {
        LOGGER.info("Get resource by Criteria: ResourceName - " + resourceName + ", apiKey - " + apiKey);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetResourceResponse getResourceResponse = new GetResourceResponse();
            GetResourceRequest resource = new GetResourceRequest();
            resource.setResourceName(resourceName);
            resource.setApiKey(apiKey);
            apiContext.setRequestContent(resource);
            
            _helper.setApiContext(apiContext);

            ApiResponse response;
            response = coreService.getResource(resource);
            if (response.status) {
                ResourceList resourceList = new ResourceList();
                resourceList.resourceList = (List<Resources>) response.responseData;
                response.responseData = resourceList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getResourceResponse.setRequestId(response.requestId);
            getResourceResponse.setStatus(response.status);
            getResourceResponse.setStatusCode(response.statusCode);
            getResourceResponse.setTimeStamp(response.timeStamp);
            getResourceResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getResourceResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_RESOURCE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get resource by Id
     *
     * @param resourceId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Resources + "/{resourceId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetResourceByIdResponse> getResourcebyId(@PathVariable Integer resourceId) throws Exception {
        LOGGER.info("Get resource by Id : ResourceId - " + resourceId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetResourceByIdResponse getResourceByIdResponse = new GetResourceByIdResponse();
            ApiResponse response;
            GetResourceByIdRequest getResourceByIdRequest = new GetResourceByIdRequest();
            getResourceByIdRequest.setResourceId(BigInteger.valueOf(resourceId));
            apiContext.setRequestContent(getResourceByIdRequest);
            _helper.setApiContext(apiContext);
            response = coreService.getResourceById(resourceId);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getResourceByIdResponse.setRequestId(response.requestId);
            getResourceByIdResponse.setStatus(response.status);
            getResourceByIdResponse.setStatusCode(response.statusCode);
            getResourceByIdResponse.setTimeStamp(response.timeStamp);
            getResourceByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getResourceByIdResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_RESOURCE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Delete resource
     *
     * @param resourceId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Resources + "/{resourceId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<DeleteResourceResponse> deleteResource(@PathVariable Integer resourceId) throws Exception {
        LOGGER.info("Delete resource. ResourceId - " + resourceId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            DeleteResourceResponse deleteResourceResponse = new DeleteResourceResponse();
            ApiResponse response;
            DeleteResourceRequest deleteResourceRequest = new DeleteResourceRequest();
            deleteResourceRequest.setResourceId(BigInteger.valueOf(resourceId));
            apiContext.setRequestContent(deleteResourceRequest);
            _helper.setApiContext(apiContext);
            response = coreService.deleteResource(resourceId);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            deleteResourceResponse.setRequestId(response.requestId);
            deleteResourceResponse.setStatus(response.status);
            deleteResourceResponse.setStatusCode(response.statusCode);
            deleteResourceResponse.setTimeStamp(response.timeStamp);
            deleteResourceResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(deleteResourceResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_DELETE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Add new Lens settings
     *
     * @param settings
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Lens, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<AddLensSettingsResponse> addLensSettings(@RequestBody AddLensSettingsRequest settings) throws Exception {
        LOGGER.info("Add Lens settings: InstanceType - " + settings.getInstanceType() + ", HostName - " + settings.getHost() + ", Port - " + settings.getPort() + ", Locale - " + settings.getLocale());
        ApiContext apiContext = _helper.getApiContext();

        try {
            AddLensSettingsResponse addLensSettingsResponse = new AddLensSettingsResponse();
            ApiResponse response = coreService.addLensSettings(settings);
            apiContext.setRequestContent(settings);
            
            _helper.setApiContext(apiContext);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            addLensSettingsResponse.setRequestId(response.requestId);
            addLensSettingsResponse.setStatus(response.status);
            addLensSettingsResponse.setStatusCode(response.statusCode);
            addLensSettingsResponse.setTimeStamp(response.timeStamp);
            addLensSettingsResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(addLensSettingsResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Update Lens settings
     *
     * @param settingsId
     * @param settings
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Lens + "/{settingsId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<UpdateLensSettingsResponse> updateLensSettings(@PathVariable int settingsId, @RequestBody UpdateLensSettingsRequest settings) throws Exception {
        LOGGER.info("Update Lens settings: SettingsId - " + settingsId);
        settings.setLensSettingsId(new BigInteger(Integer.toString(settingsId)));
        ApiContext apiContext = _helper.getApiContext();

        try {
            UpdateLensSettingsResponse updateLensSettingsResponse = new UpdateLensSettingsResponse();
            apiContext.setRequestContent(settings);
            _helper.setApiContext(apiContext);
            ApiResponse response = coreService.updateLensSettings(settingsId, settings);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            updateLensSettingsResponse.setRequestId(response.requestId);
            updateLensSettingsResponse.setStatus(response.status);
            updateLensSettingsResponse.setStatusCode(response.statusCode);
            updateLensSettingsResponse.setTimeStamp(response.timeStamp);
            updateLensSettingsResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(updateLensSettingsResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Delete Lens settings
     *
     * @param settingsId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Lens + "/{settingsId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<DeleteLensSettingsResponse> deleteLensSettings(@PathVariable int settingsId) throws Exception {
        LOGGER.info("Delete Lens settings: SettingsId - " + settingsId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            DeleteLensSettingsResponse deleteLensSettingsResponse = new DeleteLensSettingsResponse();
            DeleteLensSettingsRequest deleteLensSettingsRequest = new DeleteLensSettingsRequest();
            deleteLensSettingsRequest.setLensSettingsId(BigInteger.valueOf(settingsId));
            apiContext.setRequestContent(deleteLensSettingsRequest);
            _helper.setApiContext(apiContext);
            ApiResponse response = coreService.deleteLensSettings(settingsId);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            deleteLensSettingsResponse.setRequestId(response.requestId);
            deleteLensSettingsResponse.setStatus(response.status);
            deleteLensSettingsResponse.setStatusCode(response.statusCode);
            deleteLensSettingsResponse.setTimeStamp(response.timeStamp);
            deleteLensSettingsResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(deleteLensSettingsResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_DELETE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get the Lens Settings
     *
     * @param host
     * @param port
     * @param timeout
     * @param status
     * @param version
     * @param charSet
     * @param type
     * @param locale
     * @param language
     * @param country
     * @param hrxmlVersion
     * @param hrxmlFileName
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Lens, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetLensSettingsResponse> getLensSettings(@RequestParam(value = "host", required = false) String host, @RequestParam(value = "port", required = false) Integer port, @RequestParam(value = "timeout", required = false) Integer timeout, @RequestParam(value = "status", required = false) Boolean status, @RequestParam(value = "version", required = false) String version, @RequestParam(value = "type", required = false) String type, @RequestParam(value = "locale", required = false) String locale, @RequestParam(value = "charSet", required = false) String charSet, @RequestParam(value = "language", required = false) String language, @RequestParam(value = "country", required = false) String country, @RequestParam(value = "hrxmlVersion", required = false) String hrxmlVersion, @RequestParam(value = "hrxmlFileName", required = false) String hrxmlFileName) throws Exception {
        LOGGER.info("Get Lens settings. Host - " + host + ", Port - " + port + ", TimeOut - " + timeout + ", Status - " + status + ", version - " + version);
        ApiContext apiContext = _helper.getApiContext();
        try {
            GetLensSettingsRequest settings = new GetLensSettingsRequest();
            settings.setCharSet(charSet);
            settings.setHost(host);
            settings.setInstanceType(type);
            settings.setLensVersion(version);
            settings.setLocale(locale);
            settings.setLanguage(language);
            settings.setCountry(country);
            settings.setPort(port != null ? new BigInteger(Integer.toString(port)) : BigInteger.ZERO);
            settings.setStatus(status);
            settings.setTimeOut(timeout != null ? new BigInteger(Integer.toString(timeout)) : BigInteger.ZERO);
            settings.setHRXMLFileName(hrxmlFileName);
            settings.setHRXMLVersion(hrxmlVersion);

            GetLensSettingsResponse getLensSettingsResponse = new GetLensSettingsResponse();
            apiContext.setRequestContent(settings);
            
            _helper.setApiContext(apiContext);

            ApiResponse response = coreService.getLensSettings(settings);
            if (response.status) {
                LensSettingsList settingsList = new LensSettingsList();
                settingsList.lensSettingsList = (List<LensSettings>) response.responseData;
                response.responseData = settingsList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getLensSettingsResponse.setRequestId(response.requestId);
            getLensSettingsResponse.setStatus(response.status);
            getLensSettingsResponse.setStatusCode(response.statusCode);
            getLensSettingsResponse.setTimeStamp(response.timeStamp);
            getLensSettingsResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getLensSettingsResponse, HttpStatus.OK);

        } catch (ApiException ex) {
            throw ex;
        } catch (RuntimeException e) {
            throw new ApiException(ExceptionUtils.getStackTrace(e), _helper.getErrorMessageWithURL(ApiErrors.GET_LENS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LENS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get client by Id
     *
     * @param instanceId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Lens + "/{instanceId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetLensSettingsByIdResponse> getLensSettingsById(@PathVariable int instanceId) throws Exception {
        LOGGER.info("Get Lens settings: SettingsId - " + instanceId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetLensSettingsByIdResponse getLensSettingsByIdResponse = new GetLensSettingsByIdResponse();
            GetLensSettingsByIdRequest getLensbyIdRequest = new GetLensSettingsByIdRequest();
            getLensbyIdRequest.setInstanceId(BigInteger.valueOf(instanceId));
            apiContext.setRequestContent(getLensbyIdRequest);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.getLensSettingsById(instanceId);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getLensSettingsByIdResponse.setRequestId(response.requestId);
            getLensSettingsByIdResponse.setStatus(response.status);
            getLensSettingsByIdResponse.setStatusCode(response.statusCode);
            getLensSettingsByIdResponse.setTimeStamp(response.timeStamp);
            getLensSettingsByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getLensSettingsByIdResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LENS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Creates a new consumer
     *
     * @param consumer
     * @return
     * @throws java.lang.Exception
     */
    @RequestMapping(value = UriConstants.Clients, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<CreateConsumerResponse> createConsumer(@RequestBody CreateConsumerRequest consumer) throws Exception {
            ApiContext apiContext = _helper.getApiContext();
        try {
            LOGGER.info("Create Consumer request : Key - " + consumer.getKey());
            apiContext.setRequestContent(consumer);
            
            _helper.setApiContext(apiContext);
            
            CreateConsumerResponse createConsumerResponse = new CreateConsumerResponse();
            ApiResponse response = coreService.createConsumer(consumer);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            createConsumerResponse.setRequestId(response.requestId);
            createConsumerResponse.setStatus(response.status);
            createConsumerResponse.setStatusCode(response.statusCode);
            createConsumerResponse.setTimeStamp(response.timeStamp);
            createConsumerResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(createConsumerResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
    }

    /**
     * Updates a consumer
     *
     * @param clientId
     * @param consumer
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Clients + "/{clientId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<UpdateConsumerResponse> updateConsumer(@PathVariable int clientId, @RequestBody UpdateConsumerRequest consumer) throws Exception {
        LOGGER.info("Update Consumer: ClientId - " + clientId);
        consumer.setClientId(new BigInteger(Integer.toString(clientId)));
        ApiContext apiContext = _helper.getApiContext();
        try {
            UpdateConsumerResponse updateConsumerResponse = new UpdateConsumerResponse();
            apiContext.setRequestContent(consumer);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.updateConsumer(clientId, consumer);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            updateConsumerResponse.setRequestId(response.requestId);
            updateConsumerResponse.setStatus(response.status);
            updateConsumerResponse.setStatusCode(response.statusCode);
            updateConsumerResponse.setTimeStamp(response.timeStamp);
            updateConsumerResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(updateConsumerResponse, HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
    /**
     * Updates a consumer
     *
     * @param clientId
     * @param consumer
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.ResellerClients + "/{clientId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<UpdateConsumerResponse> updateConsumer(@PathVariable int clientId, @RequestBody List<UpdateConsumerRequest> consumer) throws Exception {
        LOGGER.info("Update Consumer: ClientId - " + clientId);
//        consumer.setClientId(new BigInteger(Integer.toString(clientId)));
        ApiContext apiContext = _helper.getApiContext();
        try {
            UpdateConsumerResponse updateConsumerResponse = new UpdateConsumerResponse();
            apiContext.setRequestContent(consumer);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.updateConsumer(clientId, consumer);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = new Date();
            apiContext.setApiResponse(response);
            updateConsumerResponse.setRequestId(response.requestId);
            updateConsumerResponse.setStatus(response.status);
            updateConsumerResponse.setStatusCode(response.statusCode);
            updateConsumerResponse.setTimeStamp(response.timeStamp);
            updateConsumerResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + new Date());
            LOGGER.info("Sending response");

            return new ResponseEntity<>(updateConsumerResponse, HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Deletes a consumer
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Clients + "/{clientId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<DeleteConsumerResponse> deleteConsumer(@PathVariable int clientId) throws Exception {
        LOGGER.info("Delete Consumer: ClientId - " + clientId);
        ApiContext apiContext = _helper.getApiContext();
        try {
            DeleteConsumerResponse deleteConsumerResponse = new DeleteConsumerResponse();
            DeleteConsumerRequest deleteConsumerRequest = new DeleteConsumerRequest();
            deleteConsumerRequest.setClientId(BigInteger.valueOf(clientId));
            apiContext.setRequestContent(deleteConsumerRequest);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.deleteConsumer(clientId);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            deleteConsumerResponse.setRequestId(response.requestId);
            deleteConsumerResponse.setStatus(response.status);
            deleteConsumerResponse.setStatusCode(response.statusCode);
            deleteConsumerResponse.setTimeStamp(response.timeStamp);
            deleteConsumerResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(deleteConsumerResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_DELETE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get the list of clients
     *
     * @param key
     * @param name
     * @param dumpStatus
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Clients, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetClientsResponse> getClients(@RequestParam(value = "key", required = false) String key, @RequestParam(value = "name", required = false) String name, @RequestParam(value = "dumpStatus", required = false) Boolean dumpStatus) throws Exception {
        LOGGER.info("Get Client. Key - " + key + ", name - " + name + ", DumpStatus - " + dumpStatus);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetClientsRequest client = new GetClientsRequest();
            client.setClientKey(key);
            client.setClientName(name);
            client.setDumpStatus(dumpStatus);

            apiContext.setRequestContent(client);
            
            _helper.setApiContext(apiContext);
            
            GetClientsResponse getClientsResponse = new GetClientsResponse();
            ApiResponse response = coreService.getClients(client);

            if (response.status) {
                ClientList clientList = new ClientList();
                clientList.clientList = (List<Client>) response.responseData;
                response.responseData = clientList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getClientsResponse.setRequestId(response.requestId);
            getClientsResponse.setStatus(response.status);
            getClientsResponse.setStatusCode(response.statusCode);
            getClientsResponse.setTimeStamp(response.timeStamp);
            getClientsResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getClientsResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_CLIENTS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get client by Id
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.Clients + "/{clientId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetClientByIdResponse> getClientById(@PathVariable int clientId) throws Exception {
        LOGGER.info("Get Consumer. ClientId - " + clientId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetClientByIdResponse getClientByIdResponse = new GetClientByIdResponse();
            GetClientByIdRequest getClientByIdRequest = new GetClientByIdRequest();
            getClientByIdRequest.setClientId(BigInteger.valueOf(clientId));
            apiContext.setRequestContent(getClientByIdRequest);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.getClientById(clientId);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getClientByIdResponse.setRequestId(response.requestId);
            getClientByIdResponse.setStatus(response.status);
            getClientByIdResponse.setStatusCode(response.statusCode);
            getClientByIdResponse.setTimeStamp(response.timeStamp);
            getClientByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getClientByIdResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_CLIENTS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get license details of all the clients
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.LicenseAll, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetAllLicenseDetailsResponse> getLicenseDetails() throws Exception {
        LOGGER.info("Get License details");
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetAllLicenseDetailsResponse getAllLicenseDetailsResponse = new GetAllLicenseDetailsResponse();
            apiContext.setRequestContent(null);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = licenseService.getLicenseDetailsAll();

            if (response.status) {
                LicenseList licenseList = new LicenseList();
                licenseList.license = (List<License>) response.responseData;
                response.responseData = licenseList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getAllLicenseDetailsResponse.setRequestId(response.requestId);
            getAllLicenseDetailsResponse.setStatus(response.status);
            getAllLicenseDetailsResponse.setStatusCode(response.statusCode);
            getAllLicenseDetailsResponse.setTimeStamp(response.timeStamp);
            getAllLicenseDetailsResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getAllLicenseDetailsResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LICENSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get license details
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.License, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetLicenseResponse> getLicenseDetailsById() throws Exception {
        LOGGER.info("Get License details by client");
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetLicenseResponse getLicenseResponse = new GetLicenseResponse();
            ApiResponse response;

            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                apiContext.setRequestContent(null);
                response = licenseService.getLicenseDetailsById(apiContext.getClient().getId());
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.GET_LICENSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getLicenseResponse.setRequestId(response.requestId);
            getLicenseResponse.setStatus(response.status);
            getLicenseResponse.setStatusCode(response.statusCode);
            getLicenseResponse.setTimeStamp(response.timeStamp);
            getLicenseResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getLicenseResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LICENSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get usage logs
     *
     * @param from
     * @param to
     * @param apiKey
     * @param method
     * @param resource
     * @param raisedError
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.UsageLogs, method = RequestMethod.GET)
    @ResponseBody
    //@NoLogging
    public ResponseEntity<GetUsageLogsResponse> getUsageLogs(@RequestParam(value = "from", required = false) String from, 
            @RequestParam(value = "to", required = false) String to, @RequestParam(value = "apiKey", required = false) String apiKey, 
            @RequestParam(value = "method", required = false) String method, @RequestParam(value = "resource", required = false) String resource, 
            @RequestParam(value = "error", required = false) Boolean raisedError) throws Exception {
        LOGGER.info("Get Usage log. From - " + from + ", To - " + to + ", ApiKey - " + apiKey + ", Method - " + method + ", Resource - " + resource + ", RaisedError" + raisedError);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetUsageLogsRequest usageLogRequest = new GetUsageLogsRequest();
            usageLogRequest.setApiKey(apiKey);
            usageLogRequest.setFrom(from);
            usageLogRequest.setTo(to);
            usageLogRequest.setIsError(raisedError);
            usageLogRequest.setMethod(method);
            usageLogRequest.setResource(resource);

            apiContext.setRequestContent(usageLogRequest);
            
            _helper.setApiContext(apiContext);
            
            if (_helper.isNotNullOrEmpty(resource) && !_helper.isValidAPIResource(resource)) {
                LOGGER.error("Invalid resource name : " + resource);
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_NAME), HttpStatus.BAD_REQUEST);
            }
            
            GetUsageLogsResponse getUsageLogsResponse = new GetUsageLogsResponse();
            ApiResponse response;

            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getUsageLogs(usageLogRequest, apiContext.getClient().getId());
                if (response.status) {
                    UsageLogList logList = new UsageLogList();
                    logList.usageLogList = (List<UsageLog>) response.responseData;
                    response.responseData = logList;
                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.GET_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getUsageLogsResponse.setRequestId(response.requestId);
            getUsageLogsResponse.setStatus(response.status);
            getUsageLogsResponse.setStatusCode(response.statusCode);
            getUsageLogsResponse.setTimeStamp(response.timeStamp);
            getUsageLogsResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getUsageLogsResponse, HttpStatus.OK);

        } catch (ApiException ex) {
            throw ex;
        } catch (RuntimeException e) {
            throw new ApiException(ExceptionUtils.getStackTrace(e), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get usage log by Id
     *
     * @param usageLogId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.UsageLogs + "/{usageLogId}", method = RequestMethod.GET)
    @ResponseBody
    //@NoLogging
    public ResponseEntity<GetUsageLogByIdResponse> getUsageLogsById(@PathVariable String usageLogId) throws Exception {
        LOGGER.info("Get usage log: UsageLogId - " + usageLogId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetUsageLogByIdResponse getUsageLogByIdResponse = new GetUsageLogByIdResponse();
            GetUsageLogByIdRequest usageLogByIdRequest = new GetUsageLogByIdRequest();
            usageLogByIdRequest.setUsageLogId(usageLogId);
            apiContext.setRequestContent(usageLogByIdRequest);
            
            if (_helper.isNullOrEmpty(usageLogByIdRequest.getUsageLogId())) {
                throw new ApiException("", ApiErrors.INVALID_USAGELOG_ID, HttpStatus.BAD_REQUEST);
            }
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response;
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getUsageLogById(usageLogId, apiContext.getClient().getId());
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.GET_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getUsageLogByIdResponse.setRequestId(response.requestId);
            getUsageLogByIdResponse.setStatus(response.status);
            getUsageLogByIdResponse.setStatusCode(response.statusCode);
            getUsageLogByIdResponse.setTimeStamp(response.timeStamp);
            getUsageLogByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getUsageLogByIdResponse, HttpStatus.OK);

        } catch (ApiException ex) {
            throw ex;
        } catch (RuntimeException e) {
            throw new ApiException(ExceptionUtils.getStackTrace(e), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get error logs
     *
     * @param requestId
     * @param apiKey
     * @param statusCode
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.ErrorLogs, method = RequestMethod.GET)
    @ResponseBody
    //@NoLogging
    public ResponseEntity<GetErrorLogsResponse> getErrorLogs(@RequestParam(value = "requestId", required = false) String requestId, @RequestParam(value = "apiKey", required = false) String apiKey, @RequestParam(value = "statusCode", required = false) Integer statusCode) throws Exception {
        LOGGER.info("Get Error log. RequestId - " + requestId + ", apiKey - " + apiKey + ", StatusCode - " + statusCode);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetErrorLogsRequest errorLogRequest = new GetErrorLogsRequest();
            errorLogRequest.setApiKey(apiKey);
            errorLogRequest.setRequestId(requestId);
            errorLogRequest.setStatusCode(_helper.isNotNull(statusCode) ? new BigInteger(statusCode.toString()) : null);

            ApiResponse response;
            GetErrorLogsResponse getErrorLogsResponse = new GetErrorLogsResponse();
            apiContext.setRequestContent(errorLogRequest);
            
            _helper.setApiContext(apiContext);
            
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getErrorLogs(errorLogRequest, apiContext.getClient().getId());
                if (response.status) {
                    ErrorLogList logList = new ErrorLogList();
                    logList.errorLogList = (List<ErrorLog>) response.responseData;
                    response.responseData = logList;
                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.GET_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getErrorLogsResponse.setRequestId(response.requestId);
            getErrorLogsResponse.setStatus(response.status);
            getErrorLogsResponse.setStatusCode(response.statusCode);
            getErrorLogsResponse.setTimeStamp(response.timeStamp);
            getErrorLogsResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getErrorLogsResponse, HttpStatus.OK);

        } catch (ApiException ex) {
            throw ex;
        } catch (RuntimeException e) {
            throw new ApiException(ExceptionUtils.getStackTrace(e), _helper.getErrorMessageWithURL(ApiErrors.GET_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get error log by Id
     *
     * @param errorLogId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.ErrorLogs + "/{errorLogId}", method = RequestMethod.GET)
    @ResponseBody
    //@NoLogging
    public ResponseEntity<GetErrorLogByIdResponse> getErrorLogsById(@PathVariable String errorLogId) throws Exception {
        LOGGER.info("Get error log: ErrorLogId" + errorLogId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetErrorLogByIdResponse getErrorLogByIdResponse = new GetErrorLogByIdResponse();
            ApiResponse response;
            GetErrorLogByIdRequest errorLogByIdRequest = new GetErrorLogByIdRequest();
            errorLogByIdRequest.setErrorLogId(errorLogId);
            apiContext.setRequestContent(errorLogByIdRequest);
            
            _helper.setApiContext(apiContext);
            
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getErrorLogById(errorLogId, apiContext.getClient().getId());
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.GET_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getErrorLogByIdResponse.setRequestId(response.requestId);
            getErrorLogByIdResponse.setStatus(response.status);
            getErrorLogByIdResponse.setStatusCode(response.statusCode);
            getErrorLogByIdResponse.setTimeStamp(response.timeStamp);
            getErrorLogByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getErrorLogByIdResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get usage logs
     *
     * @param from
     * @param to
     * @param apiKey
     * @param method
     * @param resource
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.UsageCounters, method = RequestMethod.GET)
    @ResponseBody
    //@NoLogging
    public ResponseEntity<GetUsageCounterResponse> getUsageCounter(@RequestParam(value = "from", required = false) String from, @RequestParam(value = "to", required = false) String to, @RequestParam(value = "apiKey", required = false) String apiKey, @RequestParam(value = "method", required = false) String method, @RequestParam(value = "resource", required = false) String resource) throws Exception {
        LOGGER.info("Get Usage counter. From - " + from + ", To - " + to + ", ApiKey - " + apiKey + ", Method - " + method + ", Resource - " + resource);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetUsageCounterRequest usageCounterRequest = new GetUsageCounterRequest();
            usageCounterRequest.setApiKey(apiKey);
            usageCounterRequest.setFrom(_helper.isNotNull(from) ? from : null);
            usageCounterRequest.setTo(_helper.isNotNull(to) ? to : null);
            usageCounterRequest.setMethod(method);
            usageCounterRequest.setResource(resource);
            apiContext.setRequestContent(usageCounterRequest);
            
            _helper.setApiContext(apiContext);
            
            GetUsageCounterResponse getUsageCounterResponse = new GetUsageCounterResponse();
            ApiResponse response;
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getUsageCounter(usageCounterRequest, apiContext.getClient().getId());
                if (response.status) {
                    UsageCounterList counterList = new UsageCounterList();
                    counterList.usageCounterList = (List<UsageCounter>) response.responseData;
                    response.responseData = counterList;
                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.GET_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getUsageCounterResponse.setRequestId(response.requestId);
            getUsageCounterResponse.setStatus(response.status);
            getUsageCounterResponse.setStatusCode(response.statusCode);
            getUsageCounterResponse.setTimeStamp(response.timeStamp);
            getUsageCounterResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getUsageCounterResponse, HttpStatus.OK);

        } catch (ApiException ex) {
            throw ex;
        } catch (RuntimeException e) {
            throw new ApiException(ExceptionUtils.getStackTrace(e), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get usage log by Id
     *
     * @param usageCounterId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = UriConstants.UsageCounters + "/{usageCounterId}", method = RequestMethod.GET)
    @ResponseBody
    //@NoLogging
    public ResponseEntity<GetUsageCounterByIdResponse> getUsageCounterById(@PathVariable int usageCounterId) throws Exception {
        LOGGER.info("Get usage counter: UsageCounterId - " + usageCounterId);
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetUsageCounterByIdResponse getUsageCounterByIdResponse = new GetUsageCounterByIdResponse();
            ApiResponse response;
            GetUsageCounterByIdRequest usageCounterbyIdRequest = new GetUsageCounterByIdRequest();
            usageCounterbyIdRequest.setUsageCounterId(BigInteger.valueOf(usageCounterId));
            apiContext.setRequestContent(usageCounterbyIdRequest);
            
            _helper.setApiContext(apiContext);
            
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getUsageCounterById(usageCounterId, apiContext.getClient().getId());
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.GET_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            getUsageCounterByIdResponse.setRequestId(response.requestId);
            getUsageCounterByIdResponse.setStatus(response.status);
            getUsageCounterByIdResponse.setStatusCode(response.statusCode);
            getUsageCounterByIdResponse.setTimeStamp(response.timeStamp);
            getUsageCounterByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return new ResponseEntity<>(getUsageCounterByIdResponse, HttpStatus.OK);

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

}

        
