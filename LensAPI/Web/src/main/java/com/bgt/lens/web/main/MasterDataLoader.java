package com.bgt.lens.web.main;

import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

// Load app scope data to memory while starting servlet
@Component
public class MasterDataLoader implements ServletContextAware {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Override
    public void setServletContext(ServletContext servletContext) {

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);

    }

}
