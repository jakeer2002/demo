// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.rest;

import com.bgt.lens.model.rest.response.ApiResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * *
 * REST error handler
 */
@ControllerAdvice
public class RestErrorHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger(RestErrorHandler.class);

    /**
     * *
     * Handle REST exceptions
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ApiResponse> handleAnyException(Throwable ex, HttpServletRequest request) {
        ApiResponse response = new ApiResponse();
        response.status = false;
        response.responseData = ex;
        if (ex.getClass().equals(HttpMessageNotReadableException.class)) {
            response.responseData = "The request message cannot be processed";
        }
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
