// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.web.soap;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Enum.VariantType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.PerformanceLogging;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.DeleteApiRequest;
import com.bgt.lens.model.adminservice.request.DeleteConsumerRequest;
import com.bgt.lens.model.adminservice.request.DeleteLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.DeleteResourceRequest;
import com.bgt.lens.model.adminservice.request.GetApiByIdRequest;
import com.bgt.lens.model.adminservice.request.GetApiRequest;
import com.bgt.lens.model.adminservice.request.GetClientByIdRequest;
import com.bgt.lens.model.adminservice.request.GetClientsRequest;
import com.bgt.lens.model.adminservice.request.GetErrorLogByIdRequest;
import com.bgt.lens.model.adminservice.request.GetErrorLogsRequest;
import com.bgt.lens.model.adminservice.request.GetLensSettingsByIdRequest;
import com.bgt.lens.model.adminservice.request.GetLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.GetResourceByIdRequest;
import com.bgt.lens.model.adminservice.request.GetResourceRequest;
import com.bgt.lens.model.adminservice.request.GetUsageCounterByIdRequest;
import com.bgt.lens.model.adminservice.request.GetUsageCounterRequest;
import com.bgt.lens.model.adminservice.request.GetUsageLogByIdRequest;
import com.bgt.lens.model.adminservice.request.GetUsageLogsRequest;
import com.bgt.lens.model.adminservice.request.UpdateApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.UpdateResourceRequest;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.parserservice.request.CanonRequest;
import com.bgt.lens.model.parserservice.request.ConvertRequest;
import com.bgt.lens.model.parserservice.request.InfoRequest;
import com.bgt.lens.model.parserservice.request.ParseJobBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseJobRequest;
import com.bgt.lens.model.parserservice.request.ParseJobStructuredBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseJobWithHTMRequest;
import com.bgt.lens.model.parserservice.request.ParseJobWithRTFRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeHRXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeStructuredBGXMLRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeWithHTMRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeWithRTFRequest;
import com.bgt.lens.model.parserservice.request.PingRequest;
import com.bgt.lens.model.parserservice.request.ProcessLocaleRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiList;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.ClientList;
import com.bgt.lens.model.rest.response.ErrorLog;
import com.bgt.lens.model.rest.response.ErrorLogList;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.LensSettingsList;
import com.bgt.lens.model.rest.response.License;
import com.bgt.lens.model.rest.response.LicenseList;
import com.bgt.lens.model.rest.response.ResourceList;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.UsageCounter;
import com.bgt.lens.model.rest.response.UsageCounterList;
import com.bgt.lens.model.rest.response.UsageLog;
import com.bgt.lens.model.rest.response.UsageLogList;
import com.bgt.lens.model.soap.response.adminservice.AddLensSettingsResponse;
import com.bgt.lens.model.soap.response.adminservice.AddResourceResponse;
import com.bgt.lens.model.soap.response.adminservice.CreateApiResponse;
import com.bgt.lens.model.soap.response.adminservice.CreateConsumerResponse;
import com.bgt.lens.model.soap.response.adminservice.DeleteApiResponse;
import com.bgt.lens.model.soap.response.adminservice.DeleteConsumerResponse;
import com.bgt.lens.model.soap.response.adminservice.DeleteLensSettingsResponse;
import com.bgt.lens.model.soap.response.adminservice.DeleteResourceResponse;
import com.bgt.lens.model.soap.response.adminservice.GetAllLicenseDetailsResponse;
import com.bgt.lens.model.soap.response.adminservice.GetApiByIdResponse;
import com.bgt.lens.model.soap.response.adminservice.GetApiResponse;
import com.bgt.lens.model.soap.response.adminservice.GetClientByIdResponse;
import com.bgt.lens.model.soap.response.adminservice.GetClientsResponse;
import com.bgt.lens.model.soap.response.adminservice.GetErrorLogByIdResponse;
import com.bgt.lens.model.soap.response.adminservice.GetErrorLogsResponse;
import com.bgt.lens.model.soap.response.adminservice.GetLensSettingsByIdResponse;
import com.bgt.lens.model.soap.response.adminservice.GetLensSettingsResponse;
import com.bgt.lens.model.soap.response.adminservice.GetLicenseResponse;
import com.bgt.lens.model.soap.response.adminservice.GetResourceByIdResponse;
import com.bgt.lens.model.soap.response.adminservice.GetResourceResponse;
import com.bgt.lens.model.soap.response.adminservice.GetUsageCounterByIdResponse;
import com.bgt.lens.model.soap.response.adminservice.GetUsageCounterResponse;
import com.bgt.lens.model.soap.response.adminservice.GetUsageLogByIdResponse;
import com.bgt.lens.model.soap.response.adminservice.GetUsageLogsResponse;
import com.bgt.lens.model.soap.response.adminservice.UpdateApiResponse;
import com.bgt.lens.model.soap.response.adminservice.UpdateConsumerResponse;
import com.bgt.lens.model.soap.response.adminservice.UpdateLensSettingsResponse;
import com.bgt.lens.model.soap.response.adminservice.UpdateResourceResponse;
import com.bgt.lens.model.soap.response.parserservice.CanonResponse;
import com.bgt.lens.model.soap.response.parserservice.ConvertResponse;
import com.bgt.lens.model.soap.response.parserservice.InfoResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseJobBGXMLResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseJobResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseJobStructuredBGXMLResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseJobWithHTMResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseJobWithRTFResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseResumeBGXMLResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseResumeHRXMLResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseResumeResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseResumeStructuredBGXMLResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseResumeWithHTMResponse;
import com.bgt.lens.model.soap.response.parserservice.ParseResumeWithRTFResponse;
import com.bgt.lens.model.soap.response.parserservice.PingResponse;
import com.bgt.lens.model.soap.response.parserservice.ProcessLocaleResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.license.ILicenseManagementService;
import com.bgt.lens.services.logger.ILoggerService;
import com.bgt.lens.services.utility.IUtilityService;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 *
 * SOAP end point
 */
@Endpoint
public class SoapEndPoint {

    @Autowired
    ICoreService coreService;

    @Autowired
    IUtilityService utilityService;

    @Autowired
    ILoggerService loggerService;

    @Autowired
    ILicenseManagementService licenseService;

    private static final Logger LOGGER = LogManager.getLogger(SoapEndPoint.class);

    private static final String ADMINSERVICE_NAMESPACE_URI = "http://bgt.com/lens/model/adminservice/request";
    private static final String PARSERSERVICE_NAMESPACE_URI = "http://bgt.com/lens/model/parserservice/request";

    private final Helper _helper = new Helper();
    
    @Value("${EnableDocumentStoreService}")
    private Boolean enableDocumentStoreService;
    @Value("${DSSInstanceType}")
    private String dSSInstanceType;
    @Value("${DSSLocale}")
    private String dSSLocale;
    /**
     * Create a new consumer
     *
     * @param createConsumerRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "CreateConsumerRequest")
    @ResponsePayload
    public CreateConsumerResponse createConsumer(@RequestPayload CreateConsumerRequest createConsumerRequest) throws Exception {
        LOGGER.info("Create Consumer request : Key - " + createConsumerRequest.getKey());
        ApiContext apiContext = _helper.getApiContext();
        try {
            CreateConsumerResponse createConsumerResponse = new CreateConsumerResponse();
            apiContext.setRequestContent(createConsumerRequest);
            
            _helper.setApiContext(apiContext);

            ApiResponse response = coreService.createConsumer(createConsumerRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            createConsumerResponse.setRequestId(response.requestId);
            createConsumerResponse.setStatus(response.status);
            createConsumerResponse.setStatusCode(response.statusCode);
            createConsumerResponse.setTimeStamp(response.timeStamp);
            createConsumerResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return createConsumerResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.CONSUMER_CREATION_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
    }

    /**
     * Updates consumer
     *
     * @param updateConsumerRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "UpdateConsumerRequest")
    @ResponsePayload
    public UpdateConsumerResponse updateConsumer(@RequestPayload UpdateConsumerRequest updateConsumerRequest) throws Exception {
        if (_helper.isNull(updateConsumerRequest.getClientId())) {
            throw new ApiException("", ApiErrors.INVALID_CLIENT_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Update Consumer: ClientId - " + updateConsumerRequest.getClientId().intValue());
        ApiContext apiContext = _helper.getApiContext();
        try {
            UpdateConsumerResponse updateConsumerResponse = new UpdateConsumerResponse();
            apiContext.setRequestContent(updateConsumerRequest);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.updateConsumer(updateConsumerRequest.getClientId().intValue(), updateConsumerRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            updateConsumerResponse.setRequestId(response.requestId);
            updateConsumerResponse.setStatus(response.status);
            updateConsumerResponse.setStatusCode(response.statusCode);
            updateConsumerResponse.setTimeStamp(response.timeStamp);
            updateConsumerResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return updateConsumerResponse;
        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.CONSUMER_UPDATE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Deletes a consumer
     *
     * @param deleteConsumerRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "DeleteConsumerRequest")
    @ResponsePayload
    public DeleteConsumerResponse deleteConsumer(@RequestPayload DeleteConsumerRequest deleteConsumerRequest) throws Exception {
        if (_helper.isNull(deleteConsumerRequest.getClientId())) {
            throw new ApiException("", ApiErrors.INVALID_CLIENT_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Delete Consumer. clientId - " + deleteConsumerRequest.getClientId().intValue());
        ApiContext apiContext = _helper.getApiContext();
        try {
            DeleteConsumerResponse deleteConsumerResponse = new DeleteConsumerResponse();
            apiContext.setRequestContent(deleteConsumerRequest);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.deleteConsumer(deleteConsumerRequest.getClientId().intValue());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            deleteConsumerResponse.setRequestId(response.requestId);
            deleteConsumerResponse.setStatus(response.status);
            deleteConsumerResponse.setStatusCode(response.statusCode);
            deleteConsumerResponse.setTimeStamp(response.timeStamp);
            deleteConsumerResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return deleteConsumerResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.CONSUMER_DELETE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Creates a new API
     *
     * @param createApiRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "CreateApiRequest")
    @ResponsePayload
    public CreateApiResponse createApi(@RequestPayload CreateApiRequest createApiRequest) throws Exception {
        LOGGER.info("Create Api. ApiKey - " + createApiRequest.getKey());
        ApiContext apiContext = _helper.getApiContext();
        try {
            CreateApiResponse createApiResponse = new CreateApiResponse();
            apiContext.setRequestContent(createApiRequest);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.createApi(createApiRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());

            apiContext.setApiResponse(response);

            createApiResponse.setStatus(response.status);
            createApiResponse.setStatusCode(response.statusCode);
            createApiResponse.setResponseData(response.responseData);
            createApiResponse.setRequestId(response.requestId);
            createApiResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return createApiResponse;

        } catch (Exception ex) {
            LOGGER.error("Exception " + ex.getMessage());
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.API_CREATION_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Creates a new API
     *
     * @param updateApiRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "UpdateApiRequest")
    @ResponsePayload
    public UpdateApiResponse updateApi(@RequestPayload UpdateApiRequest updateApiRequest) throws Exception {
        if (_helper.isNull(updateApiRequest.getApiId())) {
            throw new ApiException("", ApiErrors.INVALID_API_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Update Api. ApiId - " + updateApiRequest.getApiId().intValue());
        ApiContext apiContext = _helper.getApiContext();
        try {
            UpdateApiResponse updateApiResponse = new UpdateApiResponse();
            apiContext.setRequestContent(updateApiRequest);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.updateApi(updateApiRequest.getApiId().intValue(), updateApiRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            updateApiResponse.setStatus(response.status);
            updateApiResponse.setStatusCode(response.statusCode);
            updateApiResponse.setResponseData(response.responseData);
            updateApiResponse.setRequestId(response.requestId);
            updateApiResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return updateApiResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.API_UPDATE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Deletes an API
     *
     * @param deleteApiRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "DeleteApiRequest")
    @ResponsePayload
    public DeleteApiResponse deleteApi(@RequestPayload DeleteApiRequest deleteApiRequest) throws Exception {
        if (_helper.isNull(deleteApiRequest.getApiId())) {
            throw new ApiException("", ApiErrors.INVALID_API_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Delete Api. ApiId - " + deleteApiRequest.getApiId().intValue());
        ApiContext apiContext = _helper.getApiContext();

        try {
            DeleteApiResponse deleteApiResponse = new DeleteApiResponse();
            apiContext.setRequestContent(deleteApiRequest);
            
            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.deleteApi(deleteApiRequest.getApiId().intValue());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            deleteApiResponse.setStatus(response.status);
            deleteApiResponse.setStatusCode(response.statusCode);
            deleteApiResponse.setResponseData(response.responseData);
            deleteApiResponse.setRequestId(response.requestId);
            deleteApiResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return deleteApiResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.API_DELETE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Add a new Lens settings
     *
     * @param addLensSettingsRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "AddLensSettingsRequest")
    @ResponsePayload
    public AddLensSettingsResponse addLensSettings(@RequestPayload AddLensSettingsRequest addLensSettingsRequest) throws Exception {
        LOGGER.info("Add Lens settings: InstanceType - " + addLensSettingsRequest.getInstanceType() + ", HostName - " + addLensSettingsRequest.getHost() + ", Port - " + addLensSettingsRequest.getPort() + ", Locale - " + addLensSettingsRequest.getLocale());
        ApiContext apiContext = _helper.getApiContext();

        try {
            AddLensSettingsResponse addLensSettingsResponse = new AddLensSettingsResponse();
            apiContext.setRequestContent(addLensSettingsRequest);
            _helper.setApiContext(apiContext);
            ApiResponse response = coreService.addLensSettings(addLensSettingsRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            addLensSettingsResponse.setRequestId(response.requestId);
            addLensSettingsResponse.setStatus(response.status);
            addLensSettingsResponse.setStatusCode(response.statusCode);
            addLensSettingsResponse.setTimeStamp(response.timeStamp);
            addLensSettingsResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return addLensSettingsResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.LENS_CREATION_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get Lens settings
     *
     * @param updateLensSettingsRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "UpdateLensSettingsRequest")
    @ResponsePayload
    public UpdateLensSettingsResponse updateLensSettings(@RequestPayload UpdateLensSettingsRequest updateLensSettingsRequest) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(updateLensSettingsRequest);
        _helper.setApiContext(apiContext);
     
        if (_helper.isNull(updateLensSettingsRequest.getLensSettingsId())) {
            throw new ApiException("", ApiErrors.EMPTY_LENSSETTINGS_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Update Lens settings. SettingsId - " + updateLensSettingsRequest.getLensSettingsId().intValue());

        try {
            UpdateLensSettingsResponse updateLensSettingsResponse = new UpdateLensSettingsResponse();
            
            ApiResponse response = coreService.updateLensSettings(updateLensSettingsRequest.getLensSettingsId().intValue(), updateLensSettingsRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            updateLensSettingsResponse.setRequestId(response.requestId);
            updateLensSettingsResponse.setStatus(response.status);
            updateLensSettingsResponse.setStatusCode(response.statusCode);
            updateLensSettingsResponse.setTimeStamp(response.timeStamp);
            updateLensSettingsResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return updateLensSettingsResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.LENS_UPDATE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Delete Lens settings
     *
     * @param deleteLensSettingsRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "DeleteLensSettingsRequest")
    @ResponsePayload
    public DeleteLensSettingsResponse deleteLensSettings(@RequestPayload DeleteLensSettingsRequest deleteLensSettingsRequest) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
         apiContext.setRequestContent(deleteLensSettingsRequest);
        _helper.setApiContext(apiContext);
        if (_helper.isNull(deleteLensSettingsRequest.getLensSettingsId())) {
            throw new ApiException("", ApiErrors.INVALID_LENSSETTINGS_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Delete Lens settings. SettingsId - " + deleteLensSettingsRequest.getLensSettingsId());
        
        try {
            DeleteLensSettingsResponse deleteLensSettingsResponse = new DeleteLensSettingsResponse();
           
            ApiResponse response = coreService.deleteLensSettings(deleteLensSettingsRequest.getLensSettingsId().intValue());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            deleteLensSettingsResponse.setRequestId(response.requestId);
            deleteLensSettingsResponse.setStatus(response.status);
            deleteLensSettingsResponse.setStatusCode(response.statusCode);
            deleteLensSettingsResponse.setTimeStamp(response.timeStamp);
            deleteLensSettingsResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return deleteLensSettingsResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.LENS_DELETE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get the clients
     *
     * @param getClientsRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetClientsRequest")
    @ResponsePayload
    public GetClientsResponse getClients(@RequestPayload GetClientsRequest getClientsRequest) throws Exception {
        LOGGER.info("Get Client. Key - " + getClientsRequest.getClientKey() + ", name - " + getClientsRequest.getClientName() + ", DumpStatus - " + getClientsRequest.isDumpStatus());
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetClientsResponse getClientsResponse = new GetClientsResponse();
            apiContext.setRequestContent(getClientsRequest);
            
            _helper.setApiContext(apiContext);

            ApiResponse response = coreService.getClients(getClientsRequest);

            if (response.status) {
                ClientList clientList = new ClientList();
                clientList.clientList = (List<Client>) response.responseData;
                response.responseData = clientList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getClientsResponse.setRequestId(response.requestId);
            getClientsResponse.setStatus(response.status);
            getClientsResponse.setStatusCode(response.statusCode);
            getClientsResponse.setTimeStamp(response.timeStamp);
            getClientsResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getClientsResponse;

        } catch (Exception ex) {
            LOGGER.error("Exception " + ex.getMessage());
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_CLIENTS_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get client by id
     *
     * @param getClientByIdRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetClientByIdRequest")
    @ResponsePayload
    public GetClientByIdResponse getClientsById(@RequestPayload GetClientByIdRequest getClientByIdRequest) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(getClientByIdRequest);
        _helper.setApiContext(apiContext);
        if (_helper.isNull(getClientByIdRequest.getClientId())) {
            throw new ApiException("", ApiErrors.INVALID_CLIENT_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Get Consumer. ClientId - " + getClientByIdRequest.getClientId());

        try {
            GetClientByIdResponse getClientByIdResponse = new GetClientByIdResponse();
            
            ApiResponse response = coreService.getClientById(getClientByIdRequest.getClientId().intValue());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getClientByIdResponse.setRequestId(response.requestId);
            getClientByIdResponse.setStatus(response.status);
            getClientByIdResponse.setStatusCode(response.statusCode);
            getClientByIdResponse.setTimeStamp(response.timeStamp);
            getClientByIdResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getClientByIdResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_CLIENTS_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get the API
     *
     * @param getApiRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetApiRequest")
    @ResponsePayload
    public GetApiResponse getApi(@RequestPayload GetApiRequest getApiRequest) throws Exception {
        LOGGER.info("Get Api. Key - " + getApiRequest.getApiKey() + ", " + getApiRequest.getApiName() + ", ");
        ApiContext apiContext = _helper.getApiContext();
        try {
            GetApiResponse getApiResponse = new GetApiResponse();
            apiContext.setRequestContent(getApiRequest);
            
            _helper.setApiContext(apiContext);

            ApiResponse response = coreService.getApi(getApiRequest);

            if (response.status) {
                ApiList apiList = new ApiList();
                apiList.setApiList((List<Api>) response.responseData);
                response.responseData = apiList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getApiResponse.setRequestId(response.requestId);
            getApiResponse.setStatus(response.status);
            getApiResponse.setStatusCode(response.statusCode);
            getApiResponse.setTimeStamp(response.timeStamp);
            getApiResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getApiResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_API_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get API by id
     *
     * @param getApiByIdRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetApiByIdRequest")
    @ResponsePayload
    public GetApiByIdResponse getApiById(@RequestPayload GetApiByIdRequest getApiByIdRequest) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(getApiByIdRequest);
        _helper.setApiContext(apiContext);
        if (_helper.isNull(getApiByIdRequest.getApiId())) {
            throw new ApiException("", ApiErrors.INVALID_API_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Get Api. ApiId" + getApiByIdRequest.getApiId().intValue());
        
        try {
            GetApiByIdResponse getApiByIdResponse = new GetApiByIdResponse();
            
            ApiResponse response = coreService.getApiById(getApiByIdRequest.getApiId().intValue());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getApiByIdResponse.setRequestId(response.requestId);
            getApiByIdResponse.setStatus(response.status);
            getApiByIdResponse.setStatusCode(response.statusCode);
            getApiByIdResponse.setTimeStamp(response.timeStamp);
            getApiByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getApiByIdResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_API_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get the Lens settings
     *
     * @param getLensSettingsRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetLensSettingsRequest")
    @ResponsePayload
    public GetLensSettingsResponse getLensSettings(@RequestPayload GetLensSettingsRequest getLensSettingsRequest) throws Exception {
        LOGGER.info("Get Lens settings. Host - " + getLensSettingsRequest.getHost());
        ApiContext apiContext = _helper.getApiContext();
        try {
            GetLensSettingsResponse getLensSettingsResponse = new GetLensSettingsResponse();
            apiContext.setRequestContent(getLensSettingsRequest);

            _helper.setApiContext(apiContext);
            
            ApiResponse response = coreService.getLensSettings(getLensSettingsRequest);
            if (response.status) {
                LensSettingsList settingsList = new LensSettingsList();
                settingsList.lensSettingsList = (List<LensSettings>) response.responseData;
                response.responseData = settingsList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getLensSettingsResponse.setRequestId(response.requestId);
            getLensSettingsResponse.setStatus(response.status);
            getLensSettingsResponse.setStatusCode(response.statusCode);
            getLensSettingsResponse.setTimeStamp(response.timeStamp);
            getLensSettingsResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getLensSettingsResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_LENS_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get Lens settings by id
     *
     * @param getLensSettingsByIdRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetLensSettingsByIdRequest")
    @ResponsePayload
    public GetLensSettingsByIdResponse getLensSettingsById(@RequestPayload GetLensSettingsByIdRequest getLensSettingsByIdRequest) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(getLensSettingsByIdRequest);
        _helper.setApiContext(apiContext);
        if (_helper.isNull(getLensSettingsByIdRequest.getInstanceId())) {
            throw new ApiException("", ApiErrors.INVALID_LENSSETTINGS_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Get Lens settings. SettingsId - " + getLensSettingsByIdRequest.getInstanceId().intValue());
        
        try {
            GetLensSettingsByIdResponse getLensSettingsByIdResponse = new GetLensSettingsByIdResponse();
            
            ApiResponse response = coreService.getLensSettingsById(getLensSettingsByIdRequest.getInstanceId().intValue());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getLensSettingsByIdResponse.setRequestId(response.requestId);
            getLensSettingsByIdResponse.setStatus(response.status);
            getLensSettingsByIdResponse.setStatusCode(response.statusCode);
            getLensSettingsByIdResponse.setTimeStamp(response.timeStamp);
            getLensSettingsByIdResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getLensSettingsByIdResponse;

        } catch (Exception ex) {
            LOGGER.error("Exception " + ex.getMessage());
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_LENS_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get All License details
     *
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetAllLicenseDetailsRequest")
    @ResponsePayload
    public GetAllLicenseDetailsResponse getAllLicenseDetails() throws Exception {
        LOGGER.info("Get All License details");
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetAllLicenseDetailsResponse getAllLicenseDetailsResponse = new GetAllLicenseDetailsResponse();
            apiContext.setRequestContent(null);
             _helper.setApiContext(apiContext);
            ApiResponse response = licenseService.getLicenseDetailsAll();

            if (response.status) {
                LicenseList licenseList = new LicenseList();
                licenseList.license = (List<License>) response.responseData;
                response.responseData = licenseList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getAllLicenseDetailsResponse.setRequestId(response.requestId);
            getAllLicenseDetailsResponse.setStatus(response.status);
            getAllLicenseDetailsResponse.setStatusCode(response.statusCode);
            getAllLicenseDetailsResponse.setTimeStamp(response.timeStamp);
            getAllLicenseDetailsResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getAllLicenseDetailsResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_LICENSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get license details
     *
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetLicenseRequest")
    @ResponsePayload
    public GetLicenseResponse getLicense() throws Exception {
        LOGGER.info("Get License details by client");
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetLicenseResponse getLicenseResponse = new GetLicenseResponse();
            ApiResponse response;

            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                apiContext.setRequestContent(null);
                response = licenseService.getLicenseDetailsById(apiContext.getClient().getId());
            } else {
                throw new ApiException("", ApiErrors.GET_LICENSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getLicenseResponse.setRequestId(response.requestId);
            getLicenseResponse.setStatus(response.status);
            getLicenseResponse.setStatusCode(response.statusCode);
            getLicenseResponse.setTimeStamp(response.timeStamp);
            getLicenseResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getLicenseResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_LICENSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get usage logs
     *
     * @param usageLogRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetUsageLogsRequest")
    @ResponsePayload
    //@NoLogging
    public GetUsageLogsResponse getUsageLogs(@RequestPayload GetUsageLogsRequest usageLogRequest) throws Exception {
        LOGGER.info("Get Usage log. From - " + usageLogRequest.getFrom() + ", To - " + usageLogRequest.getTo() + ", ApiKey - " + usageLogRequest.getApiKey() + ", Method - " + usageLogRequest.getMethod() + ", Resource - " + usageLogRequest.getResource() + ", RaisedError - " + usageLogRequest.isIsError());
        ApiContext apiContext = _helper.getApiContext();

        try {

            GetUsageLogsResponse getUsageLogsResponse = new GetUsageLogsResponse();
            apiContext.setRequestContent(usageLogRequest);
             _helper.setApiContext(apiContext);

            ApiResponse response;

            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getUsageLogs(usageLogRequest, apiContext.getClient().getId());
                if (response.status) {
                    UsageLogList logList = new UsageLogList();
                    logList.usageLogList = (List<UsageLog>) response.responseData;
                    response.responseData = logList;
                }
            } else {
                throw new ApiException("", ApiErrors.GET_USAGELOG_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getUsageLogsResponse.setRequestId(response.requestId);
            getUsageLogsResponse.setStatus(response.status);
            getUsageLogsResponse.setStatusCode(response.statusCode);
            getUsageLogsResponse.setTimeStamp(response.timeStamp);
            getUsageLogsResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getUsageLogsResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_USAGELOG_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get usage logs
     *
     * @param usageLogRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetUsageLogByIdRequest")
    @ResponsePayload
    //@NoLogging
    public GetUsageLogByIdResponse getUsageLogById(@RequestPayload GetUsageLogByIdRequest usageLogRequest) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(usageLogRequest);
         _helper.setApiContext(apiContext);
        if (_helper.isNullOrEmpty(usageLogRequest.getUsageLogId())) {
            throw new ApiException("", ApiErrors.INVALID_USAGELOG_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Get Usage log by id. UsageLogId - " + usageLogRequest.getUsageLogId());
        
        try {
            GetUsageLogByIdResponse getUsageLogByIdResponse = new GetUsageLogByIdResponse();
            
            ApiResponse response;
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getUsageLogById(String.valueOf(usageLogRequest.getUsageLogId()), apiContext.getClient().getId());
            } else {
                throw new ApiException("", ApiErrors.GET_USAGELOG_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getUsageLogByIdResponse.setRequestId(response.requestId);
            getUsageLogByIdResponse.setStatus(response.status);
            getUsageLogByIdResponse.setStatusCode(response.statusCode);
            getUsageLogByIdResponse.setTimeStamp(response.timeStamp);
            getUsageLogByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getUsageLogByIdResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_USAGELOG_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get error logs
     *
     * @param errorLogRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetErrorLogsRequest")
    @ResponsePayload
    //@NoLogging
    public GetErrorLogsResponse getErrorLogs(@RequestPayload GetErrorLogsRequest errorLogRequest) throws Exception {
        LOGGER.info("Get Error log. RequestId - " + errorLogRequest.getRequestId() + ", apiKey - " + errorLogRequest.getApiKey() + ", StatusCode - " + errorLogRequest.getStatusCode());
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetErrorLogsResponse getErrorLogsResponse = new GetErrorLogsResponse();
            ApiResponse response;

            apiContext.setRequestContent(errorLogRequest);
             _helper.setApiContext(apiContext);
             
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getErrorLogs(errorLogRequest, apiContext.getClient().getId());
                if (response.status) {
                    ErrorLogList logList = new ErrorLogList();
                    logList.errorLogList = (List<ErrorLog>) response.responseData;
                    response.responseData = logList;
                }
            } else {
                throw new ApiException("", ApiErrors.GET_ERRORLOG_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getErrorLogsResponse.setRequestId(response.requestId);
            getErrorLogsResponse.setStatus(response.status);
            getErrorLogsResponse.setStatusCode(response.statusCode);
            getErrorLogsResponse.setTimeStamp(response.timeStamp);
            getErrorLogsResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getErrorLogsResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_ERRORLOG_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get error log by Id
     *
     * @param errorLogRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetErrorLogByIdRequest")
    @ResponsePayload
    //@NoLogging
    public GetErrorLogByIdResponse getErrorLogById(@RequestPayload GetErrorLogByIdRequest errorLogRequest) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(errorLogRequest);
        _helper.setApiContext(apiContext);
        if (_helper.isNullOrEmpty(errorLogRequest.getErrorLogId())) {
            throw new ApiException("", ApiErrors.INVALID_ERRORLOG_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Get error log by id. ErrorLogId - " + errorLogRequest.getErrorLogId());
        
        try {
            GetErrorLogByIdResponse getErrorLogByIdResponse = new GetErrorLogByIdResponse();
            ApiResponse response;
            
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getErrorLogById(errorLogRequest.getErrorLogId(), apiContext.getClient().getId());
            } else {
                throw new ApiException("", ApiErrors.GET_ERRORLOG_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getErrorLogByIdResponse.setRequestId(response.requestId);
            getErrorLogByIdResponse.setStatus(response.status);
            getErrorLogByIdResponse.setStatusCode(response.statusCode);
            getErrorLogByIdResponse.setTimeStamp(response.timeStamp);
            getErrorLogByIdResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getErrorLogByIdResponse;

        } catch (Exception ex) {
            LOGGER.error("Exception " + ex.getMessage());
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_ERRORLOG_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get usage counter
     *
     * @param usageCounterRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetUsageCounterRequest")
    @ResponsePayload
    //@NoLogging
    public GetUsageCounterResponse getUsageCounter(@RequestPayload GetUsageCounterRequest usageCounterRequest) throws Exception {
        LOGGER.info("Get Usage counter. From - " + usageCounterRequest.getFrom() + ", To - " + usageCounterRequest.getTo() + ", ApiKey - " + usageCounterRequest.getApiKey() + ", Method - " + usageCounterRequest.getMethod() + ", Resource - " + usageCounterRequest.getResource());
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetUsageCounterResponse getUsageCounterResponse = new GetUsageCounterResponse();
            apiContext.setRequestContent(usageCounterRequest);
            
            _helper.setApiContext(apiContext);

            ApiResponse response;
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getUsageCounter(usageCounterRequest, apiContext.getClient().getId());
                if (response.status) {
                    UsageCounterList counterList = new UsageCounterList();
                    counterList.usageCounterList = (List<UsageCounter>) response.responseData;
                    response.responseData = counterList;
                }
            } else {
                throw new ApiException("", ApiErrors.GET_USAGECOUNTER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getUsageCounterResponse.setRequestId(response.requestId);
            getUsageCounterResponse.setStatus(response.status);
            getUsageCounterResponse.setStatusCode(response.statusCode);
            getUsageCounterResponse.setTimeStamp(response.timeStamp);
            getUsageCounterResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getUsageCounterResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_USAGECOUNTER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get usage counter by Id
     *
     * @param usageCounterRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetUsageCounterByIdRequest")
    @ResponsePayload
    //@NoLogging
    public GetUsageCounterByIdResponse getUsageCounterById(@RequestPayload GetUsageCounterByIdRequest usageCounterRequest) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(usageCounterRequest);
        _helper.setApiContext(apiContext);
        if (_helper.isNull(usageCounterRequest.getUsageCounterId())) {
            throw new ApiException("", ApiErrors.INVALID_USAGECOUNTER_ID, HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Get Usage counter by id. UsageCounterId - " + usageCounterRequest.getUsageCounterId().intValue());
        
        try {
            GetUsageCounterByIdResponse getUsageCounterByIdResponse = new GetUsageCounterByIdResponse();
            ApiResponse response;
            
            if (apiContext.getClient() != null && apiContext.getClient().getId() != null) {
                response = loggerService.getUsageCounterById(usageCounterRequest.getUsageCounterId().intValue(), apiContext.getClient().getId());
            } else {
                throw new ApiException("", ApiErrors.GET_USAGECOUNTER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getUsageCounterByIdResponse.setRequestId(response.requestId);
            getUsageCounterByIdResponse.setStatus(response.status);
            getUsageCounterByIdResponse.setStatusCode(response.statusCode);
            getUsageCounterByIdResponse.setTimeStamp(response.timeStamp);
            getUsageCounterByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getUsageCounterByIdResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_USAGECOUNTER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Add a new resource
     *
     * @param resource
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "AddResourceRequest")
    @ResponsePayload
    public AddResourceResponse addResource(@RequestPayload AddResourceRequest resource) throws Exception {
        LOGGER.info("Add Resource response. ResoureName - " + resource.getResourceName() + ", ApiKey - " + resource.getApiKey());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            AddResourceResponse addResourceResponse = new AddResourceResponse();
            apiContext.setRequestContent(resource);
            
            _helper.setApiContext(apiContext);
            
            response = coreService.addResource(resource);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            addResourceResponse.setRequestId(response.requestId);
            addResourceResponse.setStatus(response.status);
            addResourceResponse.setStatusCode(response.statusCode);
            addResourceResponse.setTimeStamp(response.timeStamp);
            addResourceResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return addResourceResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.RESOURCE_CREATION_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Update a resource
     *
     * @param resource
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "UpdateResourceRequest")
    @ResponsePayload
    public UpdateResourceResponse updateResource(@RequestPayload UpdateResourceRequest resource) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(resource);
        _helper.setApiContext(apiContext);
        if (_helper.isNull(resource.getResourceId())) {
            throw new ApiException("", ApiErrors.INVALID_RESOURCE_ID, HttpStatus.BAD_REQUEST);
        }

        LOGGER.info("Add Resource response. ResourceName - " + resource.getResourceName() + ", ApiKey - " + resource.getApiKey());
        
        try {
            UpdateResourceResponse updateResourceResponse = new UpdateResourceResponse();
            
            ApiResponse response = coreService.updateResource(resource.getResourceId().intValue(), resource);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            updateResourceResponse.setRequestId(response.requestId);
            updateResourceResponse.setStatus(response.status);
            updateResourceResponse.setStatusCode(response.statusCode);
            updateResourceResponse.setTimeStamp(response.timeStamp);
            updateResourceResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return updateResourceResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.RESOURCE_UPDATE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Delete a resource
     *
     * @param resource
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "DeleteResourceRequest")
    @ResponsePayload
    public DeleteResourceResponse deleteResource(@RequestPayload DeleteResourceRequest resource) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(resource);
        _helper.setApiContext(apiContext);
        if (_helper.isNull(resource.getResourceId())) {
            throw new ApiException("", ApiErrors.INVALID_RESOURCE_ID, HttpStatus.BAD_REQUEST);
        }

        LOGGER.info("Delete response. ResourceId - " + resource.getResourceId().intValue());
        
        try {
            ApiResponse response;
            DeleteResourceResponse deleteResourceResponse = new DeleteResourceResponse();
           
            response = coreService.deleteResource(resource.getResourceId().intValue());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            deleteResourceResponse.setRequestId(response.requestId);
            deleteResourceResponse.setStatus(response.status);
            deleteResourceResponse.setStatusCode(response.statusCode);
            deleteResourceResponse.setTimeStamp(response.timeStamp);
            deleteResourceResponse.setResponseData(response.responseData);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return deleteResourceResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.RESOURCE_DELETE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get a resource
     *
     * @param resource
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetResourceRequest")
    @ResponsePayload
    public GetResourceResponse getResource(@RequestPayload GetResourceRequest resource) throws Exception {
        LOGGER.info("Get resource. ResourceName - " + resource.getResourceName() + ", ApiKey - " + resource.getApiKey());
        ApiContext apiContext = _helper.getApiContext();

        try {
            GetResourceResponse getResourceResponse = new GetResourceResponse();
            apiContext.setRequestContent(resource);
            
            _helper.setApiContext(apiContext);

            ApiResponse response;
            response = coreService.getResource(resource);
            if (response.status) {
                ResourceList resourceList = new ResourceList();
                resourceList.resourceList = (List<Resources>) response.responseData;
                response.responseData = resourceList;
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getResourceResponse.setRequestId(response.requestId);
            getResourceResponse.setStatus(response.status);
            getResourceResponse.setStatusCode(response.statusCode);
            getResourceResponse.setTimeStamp(response.timeStamp);
            getResourceResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getResourceResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_RESOURCE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get resource by Id
     *
     * @param resource
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = ADMINSERVICE_NAMESPACE_URI, localPart = "GetResourceByIdRequest")
    @ResponsePayload
    public GetResourceByIdResponse getResourceById(@RequestPayload GetResourceByIdRequest resource) throws Exception {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestContent(resource);
        _helper.setApiContext(apiContext);
        if (_helper.isNull(resource.getResourceId())) {
            throw new ApiException("", ApiErrors.INVALID_RESOURCE_ID, HttpStatus.BAD_REQUEST);
        }

        LOGGER.info("Get resource. ResourceId - " + resource.getResourceId().intValue());
        
        try {
            GetResourceByIdResponse getResourceByIdResponse = new GetResourceByIdResponse();
            ApiResponse response;
            
            response = coreService.getResourceById(resource.getResourceId().intValue());

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            getResourceByIdResponse.setRequestId(response.requestId);
            getResourceByIdResponse.setStatus(response.status);
            getResourceByIdResponse.setStatusCode(response.statusCode);
            getResourceByIdResponse.setTimeStamp(response.timeStamp);
            getResourceByIdResponse.setResponseData(response.responseData);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return getResourceByIdResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_RESOURCE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get Lens/XRay status
     *
     * @param infoRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "InfoRequest")
    @ResponsePayload
    @PerformanceLogging
    public InfoResponse getInfo(@RequestPayload InfoRequest infoRequest) throws Exception {
        LOGGER.info("Get Info. Type - " + infoRequest.getInstanceType() + " Locale - " + infoRequest.getLocale());
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;

            InfoResponse infoResponse = new InfoResponse();
            
            apiContext.setRequestContent(infoRequest);
            apiContext.setInstanceType(infoRequest.getInstanceType());
            apiContext.setLocale(infoRequest.getLocale());
            
            _helper.setApiContext(apiContext);

            // Valid Instance Type
            if (!_helper.isValidInstanceType(infoRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + infoRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }

            response = utilityService.info(infoRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            infoResponse.setStatus(response.status);
            infoResponse.setStatusCode(response.statusCode);
            infoResponse.setResponseData(response.responseData);
            infoResponse.setRequestId(response.requestId);
            infoResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return infoResponse;
            //  return response;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_INFO_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Get Lens/XRay ping status
     *
     * @param pingRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "PingRequest")
    @ResponsePayload
    @PerformanceLogging
    public PingResponse getPing(@RequestPayload PingRequest pingRequest) throws Exception {
        LOGGER.info("Get Ping. Type - " + pingRequest.getInstanceType() + "Locale - " + pingRequest.getLocale());
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            PingResponse pingResponse = new PingResponse();
            
            apiContext.setRequestContent(pingRequest);
            apiContext.setInstanceType(pingRequest.getInstanceType());
            apiContext.setLocale(pingRequest.getLocale());
            
            _helper.setApiContext(apiContext);
            // Valid Instance Type
            if (!_helper.isValidInstanceType(pingRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + pingRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }

            response = utilityService.ping(pingRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            pingResponse.setStatus(response.status);
            pingResponse.setResponseData(response.responseData);
            pingResponse.setRequestId(response.requestId);
            pingResponse.setStatusCode(response.statusCode);
            pingResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return pingResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.GET_PING_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Convert binary data
     *
     * @param convertRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ConvertRequest")
    @ResponsePayload
    @PerformanceLogging
    public ConvertResponse convertBinaryData(@RequestPayload ConvertRequest convertRequest) throws Exception {
        LOGGER.info("Convert Binary data. Type - " + convertRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            ConvertResponse convertResponse = new ConvertResponse();
            
            apiContext.setRequestContent(convertRequest);
            apiContext.setInstanceType(convertRequest.getInstanceType());
            
            _helper.setApiContext(apiContext);
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(convertRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + convertRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }

            response = utilityService.convertBinaryData(convertRequest);

            if (_helper.isNotNullOrEmpty(convertRequest.getLocale())) {
                apiContext.setLocale(convertRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            convertResponse.setStatus(response.status);
            convertResponse.setResponseData(response.responseData);
            convertResponse.setRequestId(response.requestId);
            convertResponse.setStatusCode(response.statusCode);
            convertResponse.setTimeStamp(response.timeStamp);
            convertResponse.setProcessedWithLocale(response.processedWithLocale);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return convertResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.CONVERT_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Canon binary data
     *
     * @param canonRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "CanonRequest")
    @ResponsePayload
    @PerformanceLogging
    public CanonResponse canonBinaryData(@RequestPayload CanonRequest canonRequest) throws Exception {
        LOGGER.info("Canon binary tagged resume data. Type - " + canonRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            CanonResponse canonResponse = new CanonResponse();
            
            // Update requestContent
            canonRequest.setInstanceType(canonRequest.getInstanceType());
            apiContext.setRequestContent(canonRequest);
            apiContext.setInstanceType(canonRequest.getInstanceType());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(canonRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + canonRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }
            
            response = utilityService.canonBinaryData(canonRequest);

            if (_helper.isNotNullOrEmpty(canonRequest.getLocale())) {
                apiContext.setLocale(canonRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            canonResponse.setStatus(response.status);
            canonResponse.setResponseData(response.responseData);
            canonResponse.setRequestId(response.requestId);
            canonResponse.setStatusCode(response.statusCode);
            canonResponse.setTimeStamp(response.timeStamp);
            canonResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return canonResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.CANON_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume binary data
     *
     * @param parseResumeRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseResumeRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseResumeResponse parseResume(@RequestPayload ParseResumeRequest parseResumeRequest) throws Exception {
        LOGGER.info("Parse resume request");
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            ParseResumeResponse parseResumeResponse = new ParseResumeResponse();
            
            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            // resume parsing without variant
            apiContext.setParseVariant(null);
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }
             //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = parseResumeRequest.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,"");
               }catch(Exception e){
                     LOGGER.error("Exception - " + e.getMessage());
                     response = utilityService.parseResume(parseResumeRequest);
               }
            }else{               
            response = utilityService.parseResume(parseResumeRequest);
            }
            }
            else{
            response = utilityService.parseResume(parseResumeRequest);
            }
            if (_helper.isNotNullOrEmpty(parseResumeRequest.getLocale())) {
                apiContext.setLocale(parseResumeRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseResumeResponse.setStatus(response.status);
            parseResumeResponse.setStatusCode(response.statusCode);

            parseResumeResponse.setResponseData(response.responseData);
            parseResumeResponse.setRequestId(response.requestId);
            parseResumeResponse.setTimeStamp(response.timeStamp);
            parseResumeResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseResumeResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume binary data BGXML
     *
     * @param parseResumeBGXMLRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseResumeBGXMLRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseResumeBGXMLResponse parseResumeBGXML(@RequestPayload ParseResumeBGXMLRequest parseResumeBGXMLRequest) throws Exception {
        LOGGER.info("Parse resume BGXML. Type - " + parseResumeBGXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseResumeBGXMLResponse parseResumeBGXMLResponse = new ParseResumeBGXMLResponse();
            
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeBGXMLRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeBGXMLRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeBGXMLRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeBGXMLRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.bgtxml.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeBGXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeBGXMLRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }
             //getting COreclient details 
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = parseResumeRequest.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.bgtxml.toString());
               }catch(Exception e){
                   LOGGER.error("Exception - " + e.getMessage());
                   response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.bgtxml.toString());
               }
            }else{               
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.bgtxml.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.bgtxml.toString());
            }
            if (_helper.isNotNullOrEmpty(parseResumeRequest.getLocale())) {
                apiContext.setLocale(parseResumeRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }
            
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseResumeBGXMLResponse.setStatus(response.status);
            parseResumeBGXMLResponse.setStatusCode(response.statusCode);

            parseResumeBGXMLResponse.setResponseData(response.responseData);
            parseResumeBGXMLResponse.setRequestId(response.requestId);
            parseResumeBGXMLResponse.setTimeStamp(response.timeStamp);
            parseResumeBGXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeBGXMLResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseResumeBGXMLResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume binary data BGXML / structured
     *
     * @param parseResumeBGXMLRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseResumeStructuredBGXMLRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseResumeStructuredBGXMLResponse parseResumeStructuredBGXML(@RequestPayload ParseResumeStructuredBGXMLRequest parseResumeStructuredBGXMLRequest) throws Exception {
        LOGGER.info("Parse resume BGXML. Type - " + parseResumeStructuredBGXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseResumeStructuredBGXMLResponse parseResumeStructuredBGXMLResponse = new ParseResumeStructuredBGXMLResponse();
            
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeStructuredBGXMLRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeStructuredBGXMLRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeStructuredBGXMLRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeStructuredBGXMLRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.structuredbgtxml.toString());
            
            _helper.setApiContext(apiContext);
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeStructuredBGXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeStructuredBGXMLRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }
             //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
           
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = parseResumeRequest.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.structuredbgtxml.toString());
               }catch(Exception e){
                   LOGGER.error("Exception - " + e.getMessage());
                   response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.structuredbgtxml.toString());
               }
            }else{
                response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.structuredbgtxml.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.structuredbgtxml.toString());
            }
            
            if (_helper.isNotNullOrEmpty(parseResumeRequest.getLocale())) {
                apiContext.setLocale(parseResumeRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            parseResumeStructuredBGXMLResponse.setStatus(response.status);
            parseResumeStructuredBGXMLResponse.setStatusCode(response.statusCode);

            parseResumeStructuredBGXMLResponse.setResponseData(response.responseData);
            parseResumeStructuredBGXMLResponse.setRequestId(response.requestId);
            parseResumeStructuredBGXMLResponse.setTimeStamp(response.timeStamp);
            parseResumeStructuredBGXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeStructuredBGXMLResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseResumeStructuredBGXMLResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume binary data HRXML
     *
     * @param parseResumeHRXMLRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseResumeHRXMLRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseResumeHRXMLResponse parseResumeHRXML(@RequestPayload ParseResumeHRXMLRequest parseResumeHRXMLRequest) throws Exception {
        LOGGER.info("Parse resume HRXML. Type - " + parseResumeHRXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseResumeHRXMLResponse parseResumeHRXMLResponse = new ParseResumeHRXMLResponse();
            
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeHRXMLRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeHRXMLRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeHRXMLRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeHRXMLRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.hrxml.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeHRXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeHRXMLRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }
             //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
           
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = parseResumeRequest.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
                  try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.hrxml.toString());
               }catch(Exception e){
                   LOGGER.error("Exception - " + e.getMessage());
                   response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.hrxml.toString());
               }
            }else{
                response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.hrxml.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.hrxml.toString());
            }   
            if (_helper.isNotNullOrEmpty(parseResumeRequest.getLocale())) {
                apiContext.setLocale(parseResumeRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }
            
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseResumeHRXMLResponse.setStatus(response.status);
            parseResumeHRXMLResponse.setStatusCode(response.statusCode);

            parseResumeHRXMLResponse.setResponseData(response.responseData);
            parseResumeHRXMLResponse.setRequestId(response.requestId);
            parseResumeHRXMLResponse.setTimeStamp(response.timeStamp);
            parseResumeHRXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeHRXMLResponse.setProcessedWithLocale(response.processedWithLocale);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseResumeHRXMLResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume binary data with RTF
     *
     * @param parseResumeWithRTFRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseResumeWithRTFRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseResumeWithRTFResponse parseResumeWithRTF(@RequestPayload ParseResumeWithRTFRequest parseResumeWithRTFRequest) throws Exception {
        LOGGER.info("Parse resume RTF. Type - " + parseResumeWithRTFRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseResumeWithRTFResponse parseResumeWithRTFResponse = new ParseResumeWithRTFResponse();
            
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeWithRTFRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeWithRTFRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeWithRTFRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeWithRTFRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.rtf.toString());
            
            _helper.setApiContext(apiContext);
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeWithRTFRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeWithRTFRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }
            //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);  
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = parseResumeRequest.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.rtf.toString());
               }catch(Exception e){
                   LOGGER.error("Exception - " + e.getMessage());
                   response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.rtf.toString());
               }
            }else{
                response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.rtf.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.rtf.toString());
            }

            if (_helper.isNotNullOrEmpty(parseResumeRequest.getLocale())) {
                apiContext.setLocale(parseResumeRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseResumeWithRTFResponse.setStatus(response.status);
            parseResumeWithRTFResponse.setStatusCode(response.statusCode);

            parseResumeWithRTFResponse.setResponseData(response.responseData);
            parseResumeWithRTFResponse.setRequestId(response.requestId);
            parseResumeWithRTFResponse.setTimeStamp(response.timeStamp);
            parseResumeWithRTFResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeWithRTFResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseResumeWithRTFResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume binary data with HTM
     *
     * @param parseResumeWithHTMRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseResumeWithHTMRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseResumeWithHTMResponse parseResumeWithHTM(@RequestPayload ParseResumeWithHTMRequest parseResumeWithHTMRequest) throws Exception {
        LOGGER.info("Parse resume HTM. Type - " + parseResumeWithHTMRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseResumeWithHTMResponse parseResumeWithHTMResponse = new ParseResumeWithHTMResponse();
            
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(parseResumeWithHTMRequest.getInstanceType());
            parseResumeRequest.setBinaryData(parseResumeWithHTMRequest.getBinaryData());
            parseResumeRequest.setExtension(parseResumeWithHTMRequest.getExtension());
            parseResumeRequest.setLocale(parseResumeWithHTMRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseResumeRequest);
            apiContext.setInstanceType(parseResumeRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.htm.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidParsingInstanceType(parseResumeWithHTMRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseResumeWithHTMRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }
             //getting COreclient details
            CoreClient docClient=apiContext.getClient();
            int docClientId=docClient.getId();
            CoreClient dbClient=utilityService.getCoreClient(docClientId);          
            
            if(dbClient.isDocStoreService() && enableDocumentStoreService){
                String DSSLocale = parseResumeRequest.getLocale();
                if(DSSLocale==null){
                    DSSLocale = "en_us";
                }
               if(apiContext.getInstanceType().toLowerCase().equals(dSSInstanceType.trim()) && DSSLocale.toLowerCase().equals(dSSLocale.trim())){
               try{
                    response = utilityService.parseResumeVariantMicroservice(parseResumeRequest,VariantType.htm.toString());
               }catch(Exception e){
                    LOGGER.error("Exception - " + e.getMessage());
                    response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.htm.toString());
               }
            }else{
                response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.htm.toString());
            }
            }
            else{
            response = utilityService.parseResumeVariant(parseResumeRequest, VariantType.htm.toString());
            }
            
            if (_helper.isNotNullOrEmpty(parseResumeRequest.getLocale())) {
                apiContext.setLocale(parseResumeRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseResumeWithHTMResponse.setStatus(response.status);
            parseResumeWithHTMResponse.setStatusCode(response.statusCode);
            parseResumeWithHTMResponse.setResponseData(response.responseData);
            parseResumeWithHTMResponse.setRequestId(response.requestId);
            parseResumeWithHTMResponse.setTimeStamp(response.timeStamp);
            parseResumeWithHTMResponse.setIdentifiedLocale(response.identifiedLocale);
            parseResumeWithHTMResponse.setProcessedWithLocale(response.processedWithLocale);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseResumeWithHTMResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     *
     * @param parseJobRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseJobRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseJobResponse parseJob(@RequestPayload ParseJobRequest parseJobRequest) throws Exception {
        LOGGER.info("Parse Job request");
        ApiContext apiContext = _helper.getApiContext();
        try {
            ApiResponse response;
            ParseJobResponse parseJobResponse = new ParseJobResponse();
            
            // Update requestContent
            apiContext.setRequestContent(parseJobRequest);
            apiContext.setInstanceType(parseJobRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.json.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }

            response = utilityService.parseJob(parseJobRequest);

            if (_helper.isNotNullOrEmpty(parseJobRequest.getLocale())) {
                apiContext.setLocale(parseJobRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }
            
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseJobResponse.setStatus(response.status);
            parseJobResponse.setStatusCode(response.statusCode);
            parseJobResponse.setResponseData(response.responseData);
            parseJobResponse.setRequestId(response.requestId);
            parseJobResponse.setTimeStamp(response.timeStamp);
            parseJobResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseJobResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse Job binary data BGXML
     *
     * @param parseJobBGXMLRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseJobBGXMLRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseJobBGXMLResponse parseJobBGXML(@RequestPayload ParseJobBGXMLRequest parseJobBGXMLRequest) throws Exception {
        LOGGER.info("Parse job BGXML. Type - " + parseJobBGXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseJobBGXMLResponse parseJobBGXMLResponse = new ParseJobBGXMLResponse();
            
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(parseJobBGXMLRequest.getInstanceType());
            parseJobRequest.setBinaryData(parseJobBGXMLRequest.getBinaryData());
            parseJobRequest.setExtension(parseJobBGXMLRequest.getExtension());
            parseJobRequest.setLocale(parseJobBGXMLRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseJobRequest);
            apiContext.setInstanceType(parseJobRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.bgtxml.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobBGXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobBGXMLRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }

            response = utilityService.parseJobVariant(parseJobRequest, VariantType.bgtxml.toString());

            if (_helper.isNotNullOrEmpty(parseJobRequest.getLocale())) {
                apiContext.setLocale(parseJobRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseJobBGXMLResponse.setStatus(response.status);
            parseJobBGXMLResponse.setStatusCode(response.statusCode);
            parseJobBGXMLResponse.setResponseData(response.responseData);
            parseJobBGXMLResponse.setRequestId(response.requestId);
            parseJobBGXMLResponse.setTimeStamp(response.timeStamp);
            parseJobBGXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobBGXMLResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseJobBGXMLResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse Job binary data BGXML / structured
     *
     * @param parseJobStructuredBGXMLRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseJobStructuredBGXMLRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseJobStructuredBGXMLResponse parseJobStructuredBGXML(@RequestPayload ParseJobStructuredBGXMLRequest parseJobStructuredBGXMLRequest) throws Exception {
        LOGGER.info("Parse job BGXML. Type - " + parseJobStructuredBGXMLRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseJobStructuredBGXMLResponse parseJobStructuredBGXMLResponse = new ParseJobStructuredBGXMLResponse();
            
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(parseJobStructuredBGXMLRequest.getInstanceType());
            parseJobRequest.setBinaryData(parseJobStructuredBGXMLRequest.getBinaryData());
            parseJobRequest.setExtension(parseJobStructuredBGXMLRequest.getExtension());
            parseJobRequest.setLocale(parseJobStructuredBGXMLRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseJobRequest);
            apiContext.setInstanceType(parseJobRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.structuredbgtxml.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobStructuredBGXMLRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobStructuredBGXMLRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }

            response = utilityService.parseJobVariant(parseJobRequest, VariantType.structuredbgtxml.toString());

            if (_helper.isNotNullOrEmpty(parseJobRequest.getLocale())) {
                apiContext.setLocale(parseJobRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }
            
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseJobStructuredBGXMLResponse.setStatus(response.status);
            parseJobStructuredBGXMLResponse.setStatusCode(response.statusCode);
            parseJobStructuredBGXMLResponse.setResponseData(response.responseData);
            parseJobStructuredBGXMLResponse.setRequestId(response.requestId);
            parseJobStructuredBGXMLResponse.setTimeStamp(response.timeStamp);
            parseJobStructuredBGXMLResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobStructuredBGXMLResponse.setProcessedWithLocale(response.processedWithLocale);

            apiContext.setReturnType("ParseJobBGXMLResponse");
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseJobStructuredBGXMLResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume binary data with HTM
     *
     * @param parseJobWithHTMRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseJobWithHTMRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseJobWithHTMResponse parseJobWithHTM(@RequestPayload ParseJobWithHTMRequest parseJobWithHTMRequest) throws Exception {
        LOGGER.info("Parse job HTM Type - " + parseJobWithHTMRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseJobWithHTMResponse parseJobWithHTMResponse = new ParseJobWithHTMResponse();
            
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(parseJobWithHTMRequest.getInstanceType());
            parseJobRequest.setBinaryData(parseJobWithHTMRequest.getBinaryData());
            parseJobRequest.setExtension(parseJobWithHTMRequest.getExtension());
            parseJobRequest.setLocale(parseJobWithHTMRequest.getLocale());

            // Update requestContent
            apiContext.setRequestContent(parseJobRequest);
            apiContext.setInstanceType(parseJobRequest.getInstanceType());
            apiContext.setParseVariant(VariantType.htm.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobWithHTMRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobWithHTMRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }

            response = utilityService.parseJobVariant(parseJobRequest, VariantType.htm.toString());

            if (_helper.isNotNullOrEmpty(parseJobRequest.getLocale())) {
                apiContext.setLocale(parseJobRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }
            
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseJobWithHTMResponse.setStatus(response.status);
            parseJobWithHTMResponse.setStatusCode(response.statusCode);
            parseJobWithHTMResponse.setResponseData(response.responseData);
            parseJobWithHTMResponse.setRequestId(response.requestId);
            parseJobWithHTMResponse.setTimeStamp(response.timeStamp);
            parseJobWithHTMResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobWithHTMResponse.setProcessedWithLocale(response.processedWithLocale);
            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseJobWithHTMResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse Job binary data with RTF
     *
     * @param parseJobWithRTFRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ParseJobWithRTFRequest")
    @ResponsePayload
    @PerformanceLogging
    public ParseJobWithRTFResponse parseJobWithRTF(@RequestPayload ParseJobWithRTFRequest parseJobWithRTFRequest) throws Exception {
        LOGGER.info("Parse job RTF. Type - " + parseJobWithRTFRequest.getInstanceType());
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;
            ParseJobWithRTFResponse parseJobWithRTFResponse = new ParseJobWithRTFResponse();
            
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(parseJobWithRTFRequest.getInstanceType());
            parseJobRequest.setBinaryData(parseJobWithRTFRequest.getBinaryData());
            parseJobRequest.setExtension(parseJobWithRTFRequest.getExtension());
            parseJobRequest.setLocale(parseJobWithRTFRequest.getLocale());

            // Update requestContent
            parseJobRequest.setInstanceType(parseJobWithRTFRequest.getInstanceType());
            apiContext.setRequestContent(parseJobRequest);
            apiContext.setParseVariant(VariantType.rtf.toString());
            
            _helper.setApiContext(apiContext);
            
            // Valid Instance Type
            if (!_helper.isValidInstanceType(parseJobWithRTFRequest.getInstanceType())) {
                LOGGER.error("Invalid instance type - " + parseJobWithRTFRequest.getInstanceType());
                throw new ApiException("", ApiErrors.INVALID_INSTANCE_TYPE, HttpStatus.BAD_REQUEST);
            }

            response = utilityService.parseJobVariant(parseJobRequest, VariantType.rtf.toString());

            if (_helper.isNotNullOrEmpty(parseJobRequest.getLocale())) {
                apiContext.setLocale(parseJobRequest.getLocale());
            } else {
                apiContext.setLocale(response.processedWithLocale);
            }
            
            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);

            parseJobWithRTFResponse.setStatus(response.status);
            parseJobWithRTFResponse.setStatusCode(response.statusCode);
            parseJobWithRTFResponse.setResponseData(response.responseData);
            parseJobWithRTFResponse.setRequestId(response.requestId);
            parseJobWithRTFResponse.setTimeStamp(response.timeStamp);
            parseJobWithRTFResponse.setIdentifiedLocale(response.identifiedLocale);
            parseJobWithRTFResponse.setProcessedWithLocale(response.processedWithLocale);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return parseJobWithRTFResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Parse resume binary data with HTM
     *
     * @param processLocaleRequest
     * @return
     * @throws Exception
     */
    @PayloadRoot(namespace = PARSERSERVICE_NAMESPACE_URI, localPart = "ProcessLocaleRequest")
    @ResponsePayload
    @PerformanceLogging
    public ProcessLocaleResponse findLocale(@RequestPayload ProcessLocaleRequest processLocaleRequest) throws Exception {
        LOGGER.info("Process locale.");
        ApiContext apiContext = _helper.getApiContext();

        try {
            ApiResponse response;

            apiContext.setRequestContent(processLocaleRequest);
            apiContext.setInstanceType(com.bgt.lens.helpers.Enum.InstanceType.LD.toString());
            
            _helper.setApiContext(apiContext);

            ProcessLocaleResponse processLocaleResponse = new ProcessLocaleResponse();
            response = utilityService.findLocale(processLocaleRequest);

            response.requestId = apiContext.getRequestId().toString();
            response.timeStamp = Date.from(Instant.now());
            apiContext.setApiResponse(response);
            apiContext.setInstanceType(com.bgt.lens.helpers.Enum.InstanceType.LD.toString());
            apiContext.setRequestContent(processLocaleRequest);

            processLocaleResponse.setStatus(response.status);
            processLocaleResponse.setStatusCode(response.statusCode);
            processLocaleResponse.setResponseData(response.responseData);
            processLocaleResponse.setRequestId(response.requestId);
            processLocaleResponse.setTimeStamp(response.timeStamp);

            _helper.setApiContext(apiContext);

            LOGGER.info("Status:" + response.status);
            LOGGER.info("RequestId" + response.requestId);
            LOGGER.info("TimeStamp:" + Date.from(Instant.now()));
            LOGGER.info("Sending response");

            return processLocaleResponse;

        } catch (Exception ex) {
            if (ex instanceof ApiException) {
                throw ex;
            } else {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), ApiErrors.PARSE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}
