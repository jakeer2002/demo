<!-- 
    ************************************************************
    Copyright 2009, Burning Glass Technologies
    XSL for translating a BGTXML Resume to HR-XML SEP\Resume 2.4 Standards
    ************************************************************
    Version 2.0
    Country code : CN
	MPI-RQP HRXML XSL
	25/Mar/2013 TDR  Modified the XSL to emit first Institution instead of concatenating all institutions in a school
	10/FEB/2015 RIM  Updated to get corresponding degree name in degree section
	
-->
     
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt"  xmlns:bgt="http://bgt.com/bgtnamespace"  xmlns="http://ns.hr-xml.org/2006-02-28" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://ns.hr-xml.org/2006-02-28 http://ns.hr-xml.org/2_4/HR-XML-2_4/SEP/Resume.xsd" version="1.0">
    <msxsl:script language="JavaScript" implements-prefix="bgt">
	
        function positiontype(jobtype) 
        {
        var contractPattern = /\s*\w*\s*contract\s*\w*\s*/;
        var permanentPattern = /\s*\w*\s*permanent\s*\w*\s*/;
        var tempPattern = /\s*\w*\s*temp\s*\w*\s*/;
        var partTimePattern = /\s*\w*\s*part\s*[-]*\s*time\s*\w*\s*/;
        var fullTimePattern = /\s*\w*\s*full\s*[-]*\s*time\s*\w*\s*/;
        var internPattern = /\s*\w*\s*intern\s*\w*\s*/;
        var volunteerPattern = /\s*\w*\s*volunteer\s*\w*\s*/;
        var freeLancePattern = /\s*\w*\s*free\s*lance\s*\w*\s*/;
        var casualPattern = /\s*\w*\s*casual\s*\w*\s*/;
		
        if(contractPattern.test(jobtype))	
        return 'contract';
        if(permanentPattern.test(jobtype)||fullTimePattern.test(jobtype))
        return 'directHire';
        if(tempPattern.test(jobtype)||partTimePattern.test(jobtype))
        return 'temp';
        if(internPattern.test(jobtype))
        return 'internship';
        if(volunteerPattern.test(jobtype)||freeLancePattern.test(jobtype)||casualPattern.test(jobtype))
        return 'volunteer';
        else
        return 'bgtnull';
        }
    </msxsl:script>

    <xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes" encoding="utf-8"/>
    <xsl:preserve-space elements="*"/>

    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
    <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
    <xsl:variable name="nkType" select="'notKnown'"/>
    <xsl:variable name="naType" select="'notApplicable'"/>
    <xsl:variable name="countryCode" select="'CN'" />

    <xsl:template match="/">
        <Resume xmlns="http://ns.hr-xml.org/2006-02-28" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://ns.hr-xml.org/2006-02-28 http://ns.hr-xml.org/2_4/HR-XML-2_4/SEP/Resume.xsd"> 
            <xsl:apply-templates select="//resume" /> 
        </Resume>
    </xsl:template>

    <!-- resume transformation part -->
    <xsl:template match="resume">
        <StructuredXMLResume>
            <ContactInfo>
                <xsl:apply-templates select="(contact/name)[1]" />		
                <xsl:apply-templates select="(contact)[1]" />
            </ContactInfo>
            <xsl:if test="count(summary)>0">
                <ExecutiveSummary> 
                    <xsl:apply-templates select="summary" /> 
                </ExecutiveSummary>
            </xsl:if>	
            <Objective> 
                <xsl:value-of select="objective" /> 
            </Objective>
            <xsl:if test="count(experience)>0">
                <xsl:if test="count(experience/job|experience/employer|experience/title|experience/description|experience/daterange)>0">
                    <EmploymentHistory> 
                        <xsl:apply-templates select="experience" /> 
                    </EmploymentHistory>
                </xsl:if>
            </xsl:if>
            <xsl:if test="count(education)>0">
                <xsl:if test="count(education/school|education/institution|education/institution/address|education/degree|education/major|education/daterange|education/description|education/secondaryedu|education/higheredu)>0">
                    <EducationHistory> 
                        <xsl:apply-templates select="education" /> 
                    </EducationHistory>
                </xsl:if>
            </xsl:if>
            <xsl:if test="count(skills/courses|professional/courses)>0">
                <LicensesAndCertifications>
                    <xsl:apply-templates select="skills/courses" />
                    <xsl:apply-templates select="professional/courses" />
                </LicensesAndCertifications>
            </xsl:if>
            <xsl:if test="count(statements/honors)>0">
                <PatentHistory> 
                    <xsl:apply-templates select="statements/honors" /> 
                </PatentHistory>	
            </xsl:if>
            <xsl:if test="count(professional/publications)>0">
                <PublicationHistory> 
                    <xsl:apply-templates select="professional/publications" /> 
                </PublicationHistory>	
            </xsl:if>	
            <xsl:if test="(count(skills/description|skills/languages) > 0)">
                <SpeakingEventsHistory>
                    <xsl:apply-templates select="skills/description" />
                    <xsl:apply-templates select="skills/languages" />
                </SpeakingEventsHistory>
            </xsl:if>
            <xsl:if test="count(skills/skills|professional/skills)>0">
                <Qualifications> 
                    <QualificationSummary>
                        <xsl:apply-templates select="skills/skills" />
                        <xsl:value-of select="professional/skills"/>
                    </QualificationSummary> 
                </Qualifications>
            </xsl:if>
            <xsl:if test="count(contact/personal/citizenship)>0">
                <Achievements>
                    <Achievement>
                        <Description>
                            <xsl:text>Citizenship: </xsl:text>
                            <xsl:value-of select="contact/personal/citizenship"/>
                            <xsl:value-of select="contact/personal/citizenship/country"/>
                        </Description>
                    </Achievement>
                </Achievements>	
            </xsl:if>
            <xsl:if test="count(education/school/orientation)>0">
                <Achievements>
                    <Achievement>
                        <Description>
                            <xsl:text>Orientation: </xsl:text>
                            <xsl:value-of select="education/school/orientation"/>
				
                        </Description>
                    </Achievement>
                </Achievements>	
            </xsl:if>
            <xsl:if test="count(statements/references)>0">
                <References>
                    <xsl:apply-templates select="statements/references"/>
                </References>	
            </xsl:if>
            <xsl:if test="count(statements/eligibility)>0">
                <SecurityCredentials> 
                    <xsl:apply-templates select="statements/eligibility" />	
                </SecurityCredentials>	
            </xsl:if>			
            <xsl:if test="(count(statements/activities|statements/personal/availability|contact/personal/pob|contact/personal/maritalstatus|contact/personal/dob|experience/training|statements/personal/license|statements/license) > 0 or count(contact/email)>1)">
                <ResumeAdditionalItems>
                    <xsl:apply-templates select="(contact/personal/dob)[1]" />
                    <xsl:apply-templates select="statements/activities" />
                    <xsl:apply-templates select="statements/personal/license" />
                    <xsl:apply-templates select="statements/license" />
                    <xsl:apply-templates select="experience/training" />
                    <xsl:apply-templates select="contact/personal/pob" />
                    <xsl:apply-templates select="contact/personal/maritalstatus" />
                    <xsl:apply-templates select="statements/personal/availability" />
                    <xsl:if test="count(contact/email)>1">
                        <xsl:apply-templates select="(contact/email)[position()!=1]" mode="resumeadditionalitem"/>
                    </xsl:if>				
                </ResumeAdditionalItems>
            </xsl:if>
            <xsl:if test="(count(professional/affiliations) > 0)">
                <ProfessionalAssociations> 
                    <xsl:apply-templates select="professional" /> 
                </ProfessionalAssociations>	
            </xsl:if>	
        </StructuredXMLResume>
        <xsl:if test="count(../skillrollup)>0">
            <xsl:apply-templates select="../skillrollup"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="contact">
        <xsl:apply-templates select="phone"/>
        <xsl:if test="count(//contact/email)>0 or count(//contact/website)>0 or count(//contact/address)>0">
            <ContactMethod>
                <xsl:if test="count(//contact/email)>0">
                    <InternetEmailAddress>
                        <xsl:apply-templates select="(//contact/email)[1]" />
                    </InternetEmailAddress>
                </xsl:if>
                <xsl:if test="count(//contact/website)>0">
                    <InternetWebAddress>
                        <xsl:apply-templates select="(//contact/website)[1]" />
                    </InternetWebAddress>
                </xsl:if>
                <xsl:apply-templates select="(//contact/address)[1]" mode="postal" />		
            </ContactMethod>
        </xsl:if>	
    </xsl:template>

    <xsl:template match="name">
        <PersonName>
            <FormattedName> 
                <xsl:value-of select="." /> 
            </FormattedName>
            <xsl:apply-templates select="givenname[1]" mode="first" />
            <xsl:apply-templates select="nickname[1]" />
            <xsl:if  test="count(givenname) > 1">
                <MiddleName> 
                    <xsl:apply-templates select="givenname[position()!=1]" mode="middle" />	
                </MiddleName>
            </xsl:if>         
            <xsl:apply-templates select="surname[1]" />
            <xsl:apply-templates select="namesuffix[1]" />
            <xsl:apply-templates select="honorific[1]" />
        </PersonName>
    </xsl:template>


    <xsl:template match="givenname" mode="first">
        <GivenName> 
            <xsl:value-of select="." /> 
        </GivenName>
    </xsl:template>


    <xsl:template match="nickname">
        <PreferredGivenName>	
            <xsl:value-of select="." /> 
        </PreferredGivenName>
    </xsl:template>


    <xsl:template match="givenname" mode="middle">
        <xsl:text> </xsl:text>
        <xsl:value-of select="." />
    </xsl:template>


    <xsl:template match="surname">
        <FamilyName> 
            <xsl:value-of select="." /> 
        </FamilyName>
    </xsl:template>


    <xsl:template match="namesuffix">
        <Affix type='aristocraticTitle'> 
            <xsl:value-of select="." /> 
        </Affix>
    </xsl:template>


    <xsl:template match="honorific">
        <Affix type='formOfAddress'>	
            <xsl:value-of select="." /> 
        </Affix>
    </xsl:template>

    <xsl:template match="phone">
        <ContactMethod>
            <xsl:choose>
                <xsl:when test="@type='work'">
                    <Use>business</Use>
                    <Telephone>
                        <FormattedNumber>
                            <xsl:value-of select="." />
                        </FormattedNumber>
                    </Telephone>
                </xsl:when>
                <xsl:when test="@type='cell'">
                    <Use>personal</Use>
                    <Mobile>
                        <FormattedNumber>
                            <xsl:value-of select="." />
                        </FormattedNumber>
                    </Mobile>
                </xsl:when>
                <xsl:when test="@type='fax'">
                    <Fax>
                        <FormattedNumber>
                            <xsl:value-of select="." />
                        </FormattedNumber>
                    </Fax>
                </xsl:when>
                <xsl:when test="@type='pager'">
                    <Pager>
                        <FormattedNumber>
                            <xsl:value-of select="." />
                        </FormattedNumber>
                    </Pager>
                </xsl:when>
                <xsl:when test="@type='home'">
                    <Use>personal</Use>
                    <Telephone>
                        <FormattedNumber>
                            <xsl:value-of select="." />
                        </FormattedNumber>
                    </Telephone>
                </xsl:when>		
                <xsl:otherwise>
                    <Telephone>
                        <FormattedNumber>
                            <xsl:value-of select="." />
                        </FormattedNumber>
                    </Telephone>
                </xsl:otherwise>
            </xsl:choose>
        </ContactMethod>
    </xsl:template>

    <xsl:template match="email">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="contact/email" mode="resumeadditionalitem">
        <ResumeAdditionalItem type="Personal">
            <Description>Alternate Email ID: <xsl:value-of select="." /></Description>
        </ResumeAdditionalItem>
    </xsl:template>

    <xsl:template match="website">
        <xsl:value-of select="." />
        <xsl:text>      </xsl:text>
    </xsl:template>

    <xsl:template match="address" mode="postal">
        <PostalAddress type="streetAddress">
            <xsl:if test="count(country)=0">
                <CountryCode>
                    <xsl:value-of select="$countryCode"/>
                </CountryCode>
            </xsl:if>
            <xsl:if test="count(country)>0">
                <xsl:apply-templates select="country[1]" />
            </xsl:if>
            <xsl:if test="count(postalcode)=0 and count(//contact/address/postalcode)>0">
                <xsl:apply-templates select="(//contact/address/postalcode)[1]" />
            </xsl:if>
            <xsl:if test="count(postalcode)>0">
                <xsl:apply-templates select="postalcode[1]" />
            </xsl:if>
            <xsl:if test="count(state)=0 and count(//contact/address/state)>0">
                <Region> 
                    <xsl:value-of select="(//contact/address/state)[1]" /> 
                </Region>
            </xsl:if>
            <xsl:if test="count(state)>0">
                <xsl:apply-templates select="state[1]" />
            </xsl:if>
            <xsl:if test="count(city)=0 and count(//contact/address/city)>0">
                <Municipality> 
                    <xsl:value-of select="(//contact/address/city)[1]" /> 
                </Municipality>
            </xsl:if>
            <xsl:if test="count(city)>0">
                <xsl:apply-templates select="city[1]" />
            </xsl:if>
            <xsl:if test="count(county)=0 and count(//contact/address/county)>0">
                <Region> 
                    <xsl:value-of select="(//contact/address/county)[1]" /> 
                </Region>
            </xsl:if>
            <xsl:if test="count(county)>0">
                <xsl:apply-templates select="county[1]" />
            </xsl:if>
            <xsl:if test="count(town)=0 and count(//contact/address/town)>0">
                <Municipality> 
                    <xsl:value-of select="(//contact/address/town)[1]" /> 
                </Municipality>
            </xsl:if>
            <xsl:if test="count(town)>0">
                <xsl:apply-templates select="town[1]" />
            </xsl:if>
            <xsl:choose>
                <xsl:when test="count(street)>0">
                    <DeliveryAddress>
                        <AddressLine>
                            <xsl:value-of select="street" />
                        </AddressLine>
                    </DeliveryAddress>    
                </xsl:when>    
                <xsl:otherwise>
                    <DeliveryAddress>
                        <AddressLine>
                            <xsl:value-of select="text()" />
                        </AddressLine>
                    </DeliveryAddress>    
                </xsl:otherwise>
            </xsl:choose>    
        </PostalAddress>
    </xsl:template>


    <xsl:template match="address" mode="AddressSummary">
        <PostalAddress type="streetAddress">
            <xsl:if test="count(country)=0">
                <CountryCode>
                    <xsl:value-of select="$countryCode"/>
                </CountryCode>
            </xsl:if>
            <xsl:if test="count(country)>0">
                <xsl:apply-templates select="country[1]" />
            </xsl:if>
            <xsl:if test="count(postalcode)>0">
                <xsl:apply-templates select="postalcode[1]" />
            </xsl:if>
            <xsl:if test="count(state)>0">
                <xsl:apply-templates select="state[1]" />
            </xsl:if>
            <xsl:if test="count(city)>0">
                <xsl:apply-templates select="city[1]" />
            </xsl:if>
            <xsl:if test="count(county)>0">
                <xsl:apply-templates select="county[1]" />
            </xsl:if>
            <xsl:if test="count(town)>0">
                <xsl:apply-templates select="town[1]" />
            </xsl:if>
            <xsl:choose>
                <xsl:when test="count(street)>0">
                    <DeliveryAddress>
                        <AddressLine>
                            <xsl:value-of select="street" />
                        </AddressLine>
                    </DeliveryAddress>    
                </xsl:when>    
                <xsl:otherwise>
                    <DeliveryAddress>
                        <AddressLine>
                            <xsl:value-of select="text()" />
                        </AddressLine>
                    </DeliveryAddress>    
                </xsl:otherwise>
            </xsl:choose>    
        </PostalAddress>
    </xsl:template>

    <xsl:template match="postalcode">
        <xsl:choose>
            <xsl:when test="@inferred-zip">
                <PostalCode>
                    <xsl:value-of select="@inferred-zip"/>
                </PostalCode>
            </xsl:when>
            <xsl:otherwise>
                <PostalCode> 
                    <xsl:value-of select="." /> 
                </PostalCode>
            </xsl:otherwise>	
        </xsl:choose>	
    </xsl:template>


    <xsl:template match="state">
        <Region> 
            <xsl:value-of select="." /> 
        </Region>
    </xsl:template>


    <xsl:template match="city">
        <Municipality> 
            <xsl:value-of select="." /> 
        </Municipality>
    </xsl:template>

    <xsl:template match="town">
        <Municipality> 
            <xsl:value-of select="." /> 
        </Municipality>
    </xsl:template>

    <xsl:template match="state">
        <Region> 
            <xsl:value-of select="." /> 
        </Region>
    </xsl:template>

    <xsl:template match="county">
        <Region> 
            <xsl:value-of select="." /> 
        </Region>
    </xsl:template>

    <xsl:template match="summary">
        <xsl:value-of select="text()" />
        <xsl:text> </xsl:text>
        <xsl:choose>
            <xsl:when test="count(skills)>0">
                <xsl:text> SKILLS SUMMARY: </xsl:text>
                <xsl:value-of select="skills" />
            </xsl:when>
        </xsl:choose>    
        <xsl:text/>
    </xsl:template>


    <xsl:template match="education">
        <xsl:apply-templates select="school" />
        <xsl:choose>
            <xsl:when test="count(institution | institution/address | degree | major | daterange | description)>0">
                <SchoolOrInstitution>
                    <xsl:choose>
                        <xsl:when test="higheredu">
                            <xsl:attribute name='schoolType'>highschool</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name='schoolType'>trade</xsl:attribute>
                        </xsl:otherwise>				
                    </xsl:choose>		
                    <SchoolName> 
                        <xsl:apply-templates select="institution[1]" />	
                    </SchoolName>
                    <xsl:choose>
                        <xsl:when test="count((institution/address)[1])>0">
                            <xsl:apply-templates select="(institution/address)[1]" mode="AddressSummary" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="address[1]" mode="AddressSummary" />
                        </xsl:otherwise>
                    </xsl:choose>
                    <Degree>
                        <xsl:choose>
                            <xsl:when test="degree">
                                <DegreeName>
                                    <xsl:value-of select="degree[1]" />
                                </DegreeName>
                            </xsl:when>
                            <xsl:when test="higheredu">
                                <DegreeName>
                                    <xsl:value-of select="higheredu" />
                                </DegreeName>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:for-each select="major[1]">
                            <DegreeMajor>
                                <Name>
                                    <xsl:value-of select="." />
                                </Name>
                            </DegreeMajor>
                        </xsl:for-each>
                        <xsl:for-each select="minor[1]">
                            <DegreeMinor>
                                <Name>
                                    <xsl:value-of select="." />
                                </Name>
                            </DegreeMinor>
                        </xsl:for-each>
                        <xsl:for-each select="gpa[1]">
                            <DegreeMeasure>
                                <EducationalMeasure>
                                    <MeasureValue>
                                        <StringValue>
                                            <xsl:value-of select="." />
                                        </StringValue>
                                    </MeasureValue>
                                </EducationalMeasure>
                            </DegreeMeasure>
                        </xsl:for-each>
				
                        <xsl:variable name="NoOfDateRanges" select="count(daterange)" />
                        <xsl:if test="$NoOfDateRanges>0">
                            <xsl:choose>
                                <xsl:when test="$NoOfDateRanges = 1">
                                    <DatesOfAttendance>
                                        <xsl:variable name="NoOfStartDates" select="count(daterange/start)" />
                                        <xsl:choose>
                                            <xsl:when test="$NoOfStartDates>0">
                                                <xsl:apply-templates select="(daterange/start)[1]"/>							
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <StartDate>
                                                    <StringDate>
                                                        <xsl:value-of select="$nkType"/>
                                                    </StringDate>
                                                </StartDate>
                                            </xsl:otherwise>
                                        </xsl:choose>								
                                        <xsl:apply-templates select="(daterange/end)[1]"/>
                                    </DatesOfAttendance>	
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="higheredu and secondaryedu">
                                            <xsl:apply-templates select="daterange[1]" mode="education" />
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:apply-templates select="daterange" mode="education" />
                                        </xsl:otherwise>
                                    </xsl:choose>	
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                        <xsl:if test="$NoOfDateRanges=0">
                            <xsl:choose>
                                <xsl:when test="higheredu and secondaryedu and count(completiondate)>0">
                                    <xsl:apply-templates select="completiondate[1]" mode="education" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:apply-templates select="completiondate" mode="education" />
                                </xsl:otherwise>
                            </xsl:choose>					
                        </xsl:if>
                        <xsl:choose>
                            <xsl:when test="count(description)>0">
                                <Comments>
                                    <xsl:value-of select="description"/>
                                </Comments>
                            </xsl:when>
                            <xsl:when test="count(courses)>0">
                                <Comments>
                                    <xsl:text>courses: </xsl:text>
                                    <xsl:value-of select="courses"/>
                                </Comments>
                            </xsl:when>
                            <xsl:otherwise>
                                <Comments>
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of select="text()"/>
                                </Comments>
                            </xsl:otherwise>
                        </xsl:choose>
                    </Degree>				
                </SchoolOrInstitution>
                <xsl:if test="count(secondaryedu)> 0">
                    <SchoolOrInstitution>
                        <xsl:attribute name="schoolType">secondary</xsl:attribute>
                        <SchoolName> 
                            <xsl:apply-templates select="institution[1]" />	
                        </SchoolName>
                        <Degree>
                            <DegreeName>
                                <xsl:value-of select="secondaryedu"/>
                            </DegreeName>
                            <xsl:choose>
                                <xsl:when test="count(daterange)>1">
                                    <xsl:apply-templates select="daterange[2]" mode="education" />
                                </xsl:when>	
                                <xsl:when test="count(completiondate)>1">
                                    <xsl:apply-templates select="completiondate[2]" mode="education" />
                                </xsl:when>	
                            </xsl:choose>			
                        </Degree>
                    </SchoolOrInstitution>		
                </xsl:if>		
            </xsl:when>
            <xsl:when test="count(secondaryedu)> 0">
                <SchoolOrInstitution>
                    <xsl:attribute name='schoolType'>highschool</xsl:attribute>
                    <SchoolName> 
                        <xsl:apply-templates select="institution[1]" />	
                    </SchoolName>
                    <Degree>
                        <DegreeName>
                            <xsl:value-of select="secondaryedu"/>
                        </DegreeName>
                        <xsl:choose>
                            <xsl:when test="count(daterange)>1">
                                <DatesOfAttendance>
                                    <xsl:apply-templates select="daterange[2]" mode="education" />
                                </DatesOfAttendance>
                            </xsl:when>	
                            <xsl:when test="count(completiondate)>1">
                                <DatesOfAttendance>
                                    <xsl:apply-templates select="completiondate[2]" mode="education" />
                                </DatesOfAttendance>
                            </xsl:when>	
                        </xsl:choose>			
                    </Degree>
                </SchoolOrInstitution>		
            </xsl:when>	
            <xsl:otherwise>
                <xsl:if test="count(school)=0">
                    <SchoolOrInstitution>
                        <xsl:attribute name="schoolType">trade</xsl:attribute>
                    </SchoolOrInstitution>	
                </xsl:if>	
            </xsl:otherwise>	
        </xsl:choose>
    </xsl:template>

    <xsl:template match="degree">
        <SchoolOrInstitution>
            <xsl:variable name="DegreeLevel">
                <xsl:value-of select="@level" />
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$DegreeLevel=12">
                    <xsl:attribute name='schoolType'>highschool</xsl:attribute>
                </xsl:when>
                <xsl:when test="$DegreeLevel=16">
                    <xsl:attribute name='schoolType'>college</xsl:attribute>
                </xsl:when>
                <xsl:when test="$DegreeLevel=18">
                    <xsl:attribute name='schoolType'>university</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name='schoolType'>trade</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>	
            <SchoolName>
                <xsl:apply-templates select="../institution" />
            </SchoolName>
            <xsl:choose>
                <xsl:when test="count((../institution/address)[1])>0">
                    <xsl:apply-templates select="(../institution/address)[1]" mode="AddressSummary" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="../address[1]" mode="AddressSummary" />
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="count(../secondaryedu)>0">
                <Degree>
                    <DegreeName>
                        <xsl:value-of select="../secondaryedu" />
                    </DegreeName>
                </Degree>
            </xsl:if>	
            <Degree>
                <xsl:choose>
                    <xsl:when test="../degree">
                        <DegreeName>
                            <xsl:value-of select="." />
                        </DegreeName>
                    </xsl:when>
                    <xsl:when test="../higheredu">
                        <DegreeName>
                            <xsl:value-of select="../higheredu" />
                        </DegreeName>
                    </xsl:when>	
                </xsl:choose>		
                <xsl:for-each select="../major[1]">
                    <DegreeMajor>
                        <Name>
                            <xsl:value-of select="." />
                        </Name>
                    </DegreeMajor>
                </xsl:for-each>
                <xsl:for-each select="../minor[1]">
                    <DegreeMinor>
                        <Name>
                            <xsl:value-of select="." />
                        </Name>
                    </DegreeMinor>
                </xsl:for-each>
                <xsl:for-each select="../gpa[1]">
                    <DegreeMeasure>
                        <EducationalMeasure>
                            <MeasureValue>
                                <StringValue>
                                    <xsl:value-of select="." />
                                </StringValue>
                            </MeasureValue>
                        </EducationalMeasure>
                    </DegreeMeasure>
                </xsl:for-each>
                <xsl:variable name="NoOfDateRanges" select="count(../daterange)" />
                <xsl:if test="$NoOfDateRanges>0">
                    <xsl:choose>
                        <xsl:when test="$NoOfDateRanges = 1">
                            <DatesOfAttendance>
                                <xsl:variable name="NoOfStartDates" select="count(../daterange/start)" />
                                <xsl:choose>
                                    <xsl:when test="$NoOfStartDates>0">
                                        <xsl:apply-templates select="../daterange/start[1]"/>							
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <StartDate>
                                            <StringDate>
                                                <xsl:value-of select="$nkType"/>
                                            </StringDate>
                                        </StartDate>
                                    </xsl:otherwise>
                                </xsl:choose>					
                                <xsl:apply-templates select="../daterange/end[1]"/>
                            </DatesOfAttendance>	
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="../daterange" mode="education"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
                <xsl:if test="$NoOfDateRanges=0">
                    <xsl:if test="count(../completiondate)>0">
                        <xsl:apply-templates select="../completiondate" mode="education" />
                    </xsl:if>	
                </xsl:if>		
                <xsl:choose>
                    <xsl:when test="count(../description)>0">
                        <Comments>
                            <xsl:value-of select="../description"/>
                        </Comments>
                    </xsl:when>
                    <xsl:when test="count(../courses)>0">
                        <Comments>
                            <xsl:text>courses: </xsl:text>
                            <xsl:value-of select="../courses"/>
                        </Comments>
                    </xsl:when>			
                    <xsl:otherwise>
                        <Comments>
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="../text()"/>
                        </Comments>
                    </xsl:otherwise>
                </xsl:choose>
            </Degree>
        </SchoolOrInstitution>	
    </xsl:template>

    <xsl:template match="school">
        <xsl:choose>
            <xsl:when test="count(degree)>0">
                <xsl:apply-templates select="degree"/>
            </xsl:when>
            <xsl:otherwise>
                <SchoolOrInstitution>
                    <xsl:choose>
                        <xsl:when test="higheredu">
                            <xsl:attribute name='schoolType'>highschool</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name='schoolType'>trade</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <SchoolName>
                        <xsl:apply-templates select="institution[1]" />
                    </SchoolName>
                    <xsl:choose>
                        <xsl:when test="count((institution/address)[1])>0">
                            <xsl:apply-templates select="(institution/address)[1]" mode="AddressSummary" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="address[1]" mode="AddressSummary" />
                        </xsl:otherwise>
                    </xsl:choose>
                    <Degree>
                        <xsl:choose>
                            <xsl:when test="degree">
                                <DegreeName>
                                    <xsl:value-of select="degree[1]" />
                                </DegreeName>
                            </xsl:when>
                            <xsl:when test="higheredu">
                                <DegreeName>
                                    <xsl:value-of select="higheredu" />
                                </DegreeName>
                            </xsl:when>	
                        </xsl:choose>
                        <xsl:for-each select="major[1]">
                            <DegreeMajor>
                                <Name>
                                    <xsl:value-of select="." />
                                </Name>
                            </DegreeMajor>
                        </xsl:for-each>
                        <xsl:for-each select="minor[1]">
                            <DegreeMinor>
                                <Name>
                                    <xsl:value-of select="." />
                                </Name>
                            </DegreeMinor>
                        </xsl:for-each>
                        <xsl:for-each select="gpa[1]">
                            <DegreeMeasure>
                                <EducationalMeasure>
                                    <MeasureSystem>GPA</MeasureSystem>
                                    <MeasureValue>
                                        <StringValue>
                                            <xsl:value-of select="." />
                                        </StringValue>
                                    </MeasureValue>
                                </EducationalMeasure>
                            </DegreeMeasure>
                        </xsl:for-each>
                        <xsl:variable name="NoOfDateRanges" select="count(daterange)" />
                        <xsl:if test="$NoOfDateRanges>0">
                            <xsl:choose>
                                <xsl:when test="$NoOfDateRanges = 1">
                                    <DatesOfAttendance>
                                        <xsl:variable name="NoOfStartDates" select="count(daterange/start)" />
                                        <xsl:choose>
                                            <xsl:when test="$NoOfStartDates>0">
                                                <xsl:apply-templates select="(daterange/start)[1]"/>							
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <StartDate>
                                                    <StringDate>
                                                        <xsl:value-of select="$nkType"/>
                                                    </StringDate>
                                                </StartDate>
                                            </xsl:otherwise>
                                        </xsl:choose>								
                                        <xsl:apply-templates select="(daterange/end)[1]"/>
                                    </DatesOfAttendance>	
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="higheredu and secondaryedu">
                                            <xsl:apply-templates select="daterange[1]" mode="education" />
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:apply-templates select="daterange" mode="education" />
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                        <xsl:if test="$NoOfDateRanges=0">
                            <xsl:choose>
                                <xsl:when test="higheredu and secondaryedu and count(completiondate)>0">
                                    <xsl:apply-templates select="completiondate[1]" mode="education" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:apply-templates select="completiondate" mode="education" />
                                </xsl:otherwise>
                            </xsl:choose>						
                        </xsl:if>
                        <xsl:choose>
                            <xsl:when test="count(description)>0">
                                <Comments>
                                    <xsl:value-of select="description"/>
                                </Comments>
                            </xsl:when>
                            <xsl:when test="count(courses)>0">
                                <Comments>
                                    <xsl:text>courses: </xsl:text>
                                    <xsl:value-of select="courses"/>
                                </Comments>
                            </xsl:when>						
                            <xsl:otherwise>
                                <Comments>
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of select="text()"/>
                                </Comments>
                            </xsl:otherwise>
                        </xsl:choose>
                    </Degree>
                </SchoolOrInstitution>
                <xsl:if test="count(secondaryedu)> 0">
                    <SchoolOrInstitution>
                        <xsl:attribute name="schoolType">secondary</xsl:attribute>
                        <SchoolName> 
                            <xsl:apply-templates select="institution[1]" />	
                        </SchoolName>
                        <Degree>
                            <DegreeName>
                                <xsl:value-of select="secondaryedu"/>
                            </DegreeName>
                            <xsl:choose>
                                <xsl:when test="count(daterange)>1">
                                    <xsl:apply-templates select="daterange[2]" mode="education"/>
                                </xsl:when>	
                                <xsl:when test="count(completiondate)>1">
                                    <xsl:apply-templates select="completiondate[2]" mode="education" />
                                </xsl:when>	
                            </xsl:choose>			
                        </Degree>
                    </SchoolOrInstitution>		
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>	
    </xsl:template>


    <xsl:template match="institution">
        <xsl:value-of select="text()"/>
    </xsl:template>


    <xsl:template match="experience">
        <xsl:apply-templates select="job" />
        <xsl:if test="count(employer | title | description | daterange)>0">
            <EmployerOrg>
                <EmployerOrgName>
                    <xsl:value-of select="employer" />
                </EmployerOrgName>
                <EmployerContactInfo>
                    <xsl:if test="count(address)>0">
                        <ContactMethod>
                            <xsl:apply-templates select="address[1]" mode="AddressSummary" />
                        </ContactMethod>	
                    </xsl:if>
                    <InternetDomainName>
                        <xsl:value-of select="website"/>
                    </InternetDomainName>
                </EmployerContactInfo>					
                <PositionHistory>
                    <xsl:if test="count(jobtype)>0">
                        <xsl:attribute name="positionType">
                            <xsl:apply-templates select="jobtype"/>
                        </xsl:attribute>
                    </xsl:if>		
                    <Title>
                        <xsl:value-of select="title" />
                    </Title>
                    <OrgName>
                        <OrganizationName>
                            <xsl:value-of select="employer" />
                        </OrganizationName>
                    </OrgName>
                    <xsl:if test="@sic">
                        <OrgIndustry>
                            <xsl:attribute name="primaryIndicator">false</xsl:attribute>
                            <IndustryCode>
                                <xsl:value-of select="@sic"/>
                            </IndustryCode>	
                        </OrgIndustry>
                    </xsl:if>
                    <xsl:if test="count(description)=0">
                        <Description/>
                    </xsl:if>
                    <xsl:if test="count(description)>0">
                        <Description>
                            <xsl:value-of select="description" />
                            <xsl:text>   </xsl:text>
                            <xsl:if test="count(skills)>0">
                                <xsl:text>SKILLS DESCRIPTION:</xsl:text>
                                <xsl:value-of select="skills"/>
                            </xsl:if>	
                            <xsl:if test="count(courses)>0">
                                <xsl:text>COURSES DESCRIPTION:</xsl:text>
                                <xsl:value-of select="courses"/>
                            </xsl:if>
                        </Description>
                    </xsl:if>
                    <xsl:variable name="NoOfDateRanges" select="count(daterange)" />
                    <xsl:if test="$NoOfDateRanges=0">
                        <StartDate>
                            <StringDate>
                                <xsl:value-of select="$nkType"/>
                            </StringDate>
                        </StartDate>
                    </xsl:if>
                    <xsl:choose>
                        <xsl:when test="$NoOfDateRanges >= 1">
                            <xsl:variable name="NoOfStartDates" select="count(daterange/start)" />
                            <xsl:choose>
                                <xsl:when test="$NoOfStartDates>0">
                                    <xsl:apply-templates select="(daterange/start)[1]"/>							
                                </xsl:when>
                                <xsl:otherwise>
                                    <StartDate>
                                        <StringDate>
                                            <xsl:value-of select="$nkType"/>
                                        </StringDate>
                                    </StartDate>
                                </xsl:otherwise>
                            </xsl:choose>	
                            <xsl:apply-templates select="(daterange/end)[1]"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="daterange[position() = 1]" />
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="count(department)>0">
                        <JobCategory>
                            <CategoryDescription>
                                <xsl:value-of select="department"/>
                            </CategoryDescription>
                        </JobCategory>
                    </xsl:if>	
                </PositionHistory>			
            </EmployerOrg>		
        </xsl:if>	
    </xsl:template>


    <xsl:template match="job">
        <EmployerOrg>
            <EmployerOrgPosition>
                <xsl:value-of select="@pos"/>
            </EmployerOrgPosition>
            <EmployerOrgName>
                <xsl:value-of select="employer" />
            </EmployerOrgName>
            <EmployerContactInfo>
                <xsl:if test="count(address)>0">
                    <ContactMethod>
                        <xsl:apply-templates select="address[1]" mode="AddressSummary" />
                    </ContactMethod>	
                </xsl:if>
                <InternetDomainName>
                    <xsl:value-of select="website"/>
                </InternetDomainName>
            </EmployerContactInfo>
            <PositionHistory>
                <xsl:if test="count(jobtype)>0">
                    <xsl:attribute name="positionType">
                        <xsl:apply-templates select="jobtype"/>
                    </xsl:attribute>
                </xsl:if>			
                <Title>
                    <xsl:value-of select="title" />
                </Title>
                <OrgName>
                    <OrganizationName>
                        <xsl:value-of select="employer" />
                    </OrganizationName>
                </OrgName>
                <xsl:if test="@sic">
                    <OrgIndustry>
                        <xsl:attribute name="primaryIndicator">false</xsl:attribute>
                        <IndustryCode>
                            <xsl:value-of select="@sic"/>
                        </IndustryCode>	
                    </OrgIndustry>
                </xsl:if>
                <xsl:if test="count(description)=0">
                    <Description/>
                </xsl:if>
                <xsl:if test="count(description)>0">
                    <Description>
                        <xsl:value-of select="description" />
                        <xsl:text>   </xsl:text>
                        <xsl:if test="count(skills)>0">
                            <xsl:text>SKILLS DESCRIPTION:</xsl:text>
                            <xsl:value-of select="skills"/>
                        </xsl:if>
                        <xsl:if test="count(courses)>0">
                            <xsl:text>COURSES DESCRIPTION:</xsl:text>
                            <xsl:value-of select="courses"/>
                        </xsl:if>					
                    </Description>
                </xsl:if>
                <xsl:variable name="NoOfDateRanges" select="count(daterange)" />
                <xsl:if test="$NoOfDateRanges=0">
                    <StartDate>
                        <StringDate>
                            <xsl:value-of select="$nkType"/>
                        </StringDate>
                    </StartDate>
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="$NoOfDateRanges >= 1">
                        <xsl:variable name="NoOfStartDates" select="count(daterange/start)" />
                        <xsl:choose>
                            <xsl:when test="$NoOfStartDates>0">
                                <xsl:apply-templates select="(daterange/start)[1]"/>							
                            </xsl:when>
                            <xsl:otherwise>
                                <StartDate>
                                    <StringDate>
                                        <xsl:value-of select="$nkType"/>
                                    </StringDate>
                                </StartDate>
                            </xsl:otherwise>
                        </xsl:choose>	
                        <xsl:apply-templates select="(daterange/end)[1]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="daterange[position() = 1]" />
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="count(department)>0">
                    <JobCategory>
                        <CategoryDescription>
                            <xsl:value-of select="department"/>
                        </CategoryDescription>
                    </JobCategory>
                </xsl:if>	
            </PositionHistory>
        </EmployerOrg>
    </xsl:template>


    <xsl:template match="daterange" mode="education">
        <DatesOfAttendance>
            <xsl:apply-templates select="start[1]" />
            <xsl:apply-templates select="end[1]" />
        </DatesOfAttendance>	
    </xsl:template>

    <xsl:template match="daterange">
        <xsl:apply-templates select="start[1]" />
        <xsl:apply-templates select="end[1]" />
    </xsl:template>

    <xsl:template match="jobtype">
        <xsl:value-of select="." />
    </xsl:template>

    <!-- In order to maintain backward compatibility, date handling is more complex
    than merely utilizing the iso8601 attribute.  In the event that the date
    lacks the iso8601 attribute, we refer to whatever the text reads
    -->
    <xsl:template match="completiondate">
        <StartDate>
            <StringDate>
                <xsl:value-of select="$nkType"/>
            </StringDate>
        </StartDate>
        <EndDate>
            <StringDate>
                <xsl:value-of select="@iso8601" />
                <xsl:if test="not(@iso8601)">
                    <xsl:value-of select="$nkType"/>
                </xsl:if>
            </StringDate>
        </EndDate>
    </xsl:template>

    <xsl:template match="completiondate" mode="education">
        <DatesOfAttendance>
            <StartDate>
                <StringDate>
                    <xsl:value-of select="$nkType"/>
                </StringDate>
            </StartDate>
            <EndDate>
                <StringDate>
                    <xsl:value-of select="@iso8601" />
                    <xsl:if test="not(@iso8601)">
                        <xsl:value-of select="$nkType"/>
                    </xsl:if>
                </StringDate>
            </EndDate>
        </DatesOfAttendance>	
    </xsl:template>

    <xsl:template match="start">
        <StartDate>
            <StringDate>
                <xsl:value-of select="@iso8601" />
                <xsl:if test="not(@iso8601)">
                    <xsl:value-of select="$nkType"/>
                </xsl:if>
            </StringDate>
        </StartDate>
    </xsl:template>


    <xsl:template match="end">
        <EndDate>
            <xsl:choose>
                <xsl:when test="@days='present'">
                    <StringDate>current</StringDate>
                </xsl:when>
                <xsl:otherwise>
                    <StringDate> 
                        <xsl:value-of select="@iso8601" />
                        <xsl:if test="not(@iso8601)">
                            <xsl:value-of select="$nkType"/>
                        </xsl:if>
                    </StringDate>
                </xsl:otherwise>
            </xsl:choose>		
        </EndDate>
    </xsl:template>

    <xsl:template match="professional">
        <Association>
            <xsl:value-of select="affiliations" />
        </Association>
    </xsl:template>

    <xsl:template match="professional/publications">
        <FormattedPublicationDescription>
            <xsl:value-of select="." />
        </FormattedPublicationDescription>
    </xsl:template>

    <xsl:template match="statements/honors">
        <Patent>
            <Description>
                <xsl:value-of select="." />
            </Description>
        </Patent>	
    </xsl:template>

    <xsl:template match="skills/description|skills/languages">
        <SpeakingEvent>
            <EventName/>
            <Description>
                <xsl:value-of select="." />
            </Description>
        </SpeakingEvent>
    </xsl:template>

    <xsl:template match="statements/activities">
        <ResumeAdditionalItem>
            <Description>
                <xsl:value-of select="." />
            </Description>
        </ResumeAdditionalItem>
    </xsl:template>

    <xsl:template match="contact/personal/dob">
        <ResumeAdditionalItem type="Personal">
            <Description>DOB:<xsl:value-of select="@iso8601" />
                <xsl:if test="not(@iso8601)">
                    <xsl:value-of select="."/>
                </xsl:if>
            </Description>
        </ResumeAdditionalItem>
    </xsl:template>

    <xsl:template match="contact/personal/dob" mode="CandidateProfile">
        <xsl:value-of select="@iso8601" />
    </xsl:template>

    <xsl:template match="statements/personal/license|statements/license">
        <ResumeAdditionalItem type="Personal">
            <Description>License:<xsl:value-of select="." /></Description>
        </ResumeAdditionalItem>
    </xsl:template>
    <xsl:template match="contact/personal/maritalstatus">
        <ResumeAdditionalItem type="Personal">
            <Description>Marital Status:<xsl:value-of select="." /></Description>
        </ResumeAdditionalItem>
    </xsl:template>

    <xsl:template match="contact/personal/pob">
        <ResumeAdditionalItem type="Personal">
            <Description>POB:<xsl:value-of select="." /></Description>
        </ResumeAdditionalItem>
    </xsl:template>

    <xsl:template match="statements/personal/availability">
        <ResumeAdditionalItem type="Personal">
            <Description>Availability:<xsl:value-of select="." /></Description>
        </ResumeAdditionalItem>
    </xsl:template>


    <xsl:template match="statements/references">
        <Reference>
            <Comments>
                <xsl:value-of select="." />
            </Comments>
        </Reference>
    </xsl:template>

    <xsl:template match="statements/eligibility">
        <SecurityCredential>
            <Name>Eligibility</Name>
            <Description>
                <xsl:text>Visa: </xsl:text>
                <xsl:value-of select="visa" />
                <xsl:if test="not(visa)>0">
                    <xsl:value-of select="text()"/>
                </xsl:if>
            </Description>
        </SecurityCredential>
    </xsl:template>

    <xsl:template match="skills/skills">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="skills/courses|professional/courses">
        <LicenseOrCertification>
            <Name> BGTNULL </Name>
            <Description>
                <xsl:value-of select="." />
            </Description>    
        </LicenseOrCertification>
    </xsl:template>

    <xsl:template match="skillrollup">
        <xsl:if test="count(canonskill) > 0">
            <UserArea>
                <xsl:apply-templates select="canonskill" />
            </UserArea>
        </xsl:if>    
    </xsl:template>

    <xsl:template match="canonskill">
        <xsl:variable name="end" select="@end"/>
        <xsl:variable name="start" select="@start"/>
        <xsl:variable name="lastused" select="@lastused"/>
        <xsl:variable name="experience" select="@experience"/>
        <Competency>
            <xsl:attribute name='name'>
                <xsl:value-of select="@name"/>
            </xsl:attribute>
            <xsl:if test="@skillid">
                <CompetencyId>
                    <xsl:attribute name='id'>
                        <xsl:value-of select="@skillid"/>
                    </xsl:attribute>
                </CompetencyId>		
            </xsl:if>	
            <CompetencyWeight>
                <xsl:choose>
                    <xsl:when test="($end and $start)">
                        <StringValue>
                            <xsl:variable name="exp" select="((($end)-($start)) div 365.25)"/>
                            <xsl:choose>
                                <xsl:when test="$exp &lt; 0.3">&lt; 3mths</xsl:when>
                                <xsl:when test="($exp &gt; 0.3 or $exp = 0.3) and $exp &lt; 0.6">3 - 6 mths</xsl:when>
                                <xsl:when test="($exp &gt; 0.6 or $exp = 0.6) and $exp &lt; 1">6 - 12 mths</xsl:when>
                                <xsl:when test="($exp &gt; 1 or $exp = 1) and $exp &lt; 1.5">1 - 1.5 yrs</xsl:when>
                                <xsl:when test="($exp &gt; 1.5 or $exp = 1.5) and $exp &lt; 2">1.5 - 2 yrs</xsl:when>
                                <xsl:when test="($exp &gt; 2 or $exp = 2) and $exp &lt; 3">2 - 3 yrs</xsl:when>
                                <xsl:when test="($exp &gt; 3 or $exp = 3) and $exp &lt; 4">3 - 4 yrs</xsl:when>
                                <xsl:when test="($exp &gt; 4 or $exp = 4) and $exp &lt; 5">4 - 5 yrs</xsl:when>
                                <xsl:when test="$exp &gt; 5 or $exp = 5">&gt; 5 yrs</xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="round($exp)"/>
                                </xsl:otherwise>
                            </xsl:choose>	
                        </StringValue>
                    </xsl:when>
                    <xsl:otherwise>
                        <StringValue/>
                    </xsl:otherwise>	
                </xsl:choose>	
                <xsl:if test="$lastused">	
                    <SupportingInformation>					
                        <xsl:text>Year last used: </xsl:text>
                        <xsl:value-of select="$lastused"/>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@education">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: education</xsl:text>
                        <xsl:if test="@eduidrefs">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="@eduidrefs"/>
                        </xsl:if>
                        <xsl:if test="not(@eduidrefs) and idrefs">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="@idrefs"/>
                        </xsl:if>
                    </SupportingInformation>	
                </xsl:if>
                <xsl:if test="@experience">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: experience</xsl:text>
                        <xsl:if test="@expidrefs">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="@expidrefs"/>
                        </xsl:if>
                        <xsl:if test="not(@expidrefs) and idrefs">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="@idrefs"/>
                        </xsl:if>
                    </SupportingInformation>		
                </xsl:if>
                <xsl:if test="@contact">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: contact</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@title">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: title</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@employer">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: employer</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@objective">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: objective</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@references">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: references</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@summary">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: summary</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@statements">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: statements</xsl:text>
                    </SupportingInformation>
                </xsl:if>
            </CompetencyWeight>
        </Competency>
    </xsl:template>

    <xsl:template match="country">
        <CountryCode>
            <xsl:variable name="code" select="translate(.,$lowercase,$uppercase)" />
            <xsl:choose>
                <xsl:when test="string-length($code)=2">
                    <xsl:choose>
                        <xsl:when test="$code='UK'">
                            <xsl:value-of select="GB"/>
                        </xsl:when>
                        <xsl:when test="$code='FR'">
                            <xsl:value-of select="FR"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$code"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$code='AFGHANISTAN'">
                            <xsl:value-of select="'AF'"/>
                        </xsl:when>
                        <xsl:when test="$code='ALAND ISLANDS'">
                            <xsl:value-of select="'AX'"/>
                        </xsl:when>
                        <xsl:when test="$code='ALBANIA'">
                            <xsl:value-of select="'AL'"/>
                        </xsl:when>
                        <xsl:when test="$code='ALGERIA'">
                            <xsl:value-of select="'DZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='AMERICAN SAMOA'">
                            <xsl:value-of select="'AS'"/>
                        </xsl:when>
                        <xsl:when test="$code='ANDORRA'">
                            <xsl:value-of select="'AD'"/>
                        </xsl:when>
                        <xsl:when test="$code='ANGOLA'">
                            <xsl:value-of select="'AO'"/>
                        </xsl:when>
                        <xsl:when test="$code='ANGUILLA'">
                            <xsl:value-of select="'AI'"/>
                        </xsl:when>
                        <xsl:when test="$code='ANTARCTICA'">
                            <xsl:value-of select="'AQ'"/>
                        </xsl:when>
                        <xsl:when test="$code='ANTIGUA AND BARBUDA'">
                            <xsl:value-of select="'AG'"/>
                        </xsl:when>
                        <xsl:when test="$code='ARGENTINA'">
                            <xsl:value-of select="'AR'"/>
                        </xsl:when>
                        <xsl:when test="$code='ARMENIA'">
                            <xsl:value-of select="'AM'"/>
                        </xsl:when>
                        <xsl:when test="$code='ARUBA'">
                            <xsl:value-of select="'AW'"/>
                        </xsl:when>
                        <xsl:when test="$code='AUSTRALIA'">
                            <xsl:value-of select="'AU'"/>
                        </xsl:when>
                        <xsl:when test="$code='AUSTRIA'">
                            <xsl:value-of select="'AT'"/>
                        </xsl:when>
                        <xsl:when test="$code='AZERBAIJAN'">
                            <xsl:value-of select="'AZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='BAHAMAS'">
                            <xsl:value-of select="'BS'"/>
                        </xsl:when>
                        <xsl:when test="$code='BAHRAIN'">
                            <xsl:value-of select="'BH'"/>
                        </xsl:when>
                        <xsl:when test="$code='BANGLADESH'">
                            <xsl:value-of select="'BD'"/>
                        </xsl:when>
                        <xsl:when test="$code='BARBADOS'">
                            <xsl:value-of select="'BB'"/>
                        </xsl:when>
                        <xsl:when test="$code='BELARUS'">
                            <xsl:value-of select="'BY'"/>
                        </xsl:when>
                        <xsl:when test="$code='BELGIUM'">
                            <xsl:value-of select="'BE'"/>
                        </xsl:when>
                        <xsl:when test="$code='BELIZE'">
                            <xsl:value-of select="'BZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='BENIN'">
                            <xsl:value-of select="'BJ'"/>
                        </xsl:when>
                        <xsl:when test="$code='BERMUDA'">
                            <xsl:value-of select="'BM'"/>
                        </xsl:when>
                        <xsl:when test="$code='BHUTAN'">
                            <xsl:value-of select="'BT'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOLIVIA'">
                            <xsl:value-of select="'BO'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOSNIA AND HERZEGOVINA'">
                            <xsl:value-of select="'BA'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOTSWANA'">
                            <xsl:value-of select="'BW'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOUVET ISLAND'">
                            <xsl:value-of select="'BV'"/>
                        </xsl:when>
                        <xsl:when test="$code='BRAZIL'">
                            <xsl:value-of select="'BR'"/>
                        </xsl:when>
                        <xsl:when test="$code='BRITISH INDIAN OCEAN TERRITORY'">
                            <xsl:value-of select="'IO'"/>
                        </xsl:when>
                        <xsl:when test="$code='BRUNEI DARUSSALAM'">
                            <xsl:value-of select="'BN'"/>
                        </xsl:when>
                        <xsl:when test="$code='BULGARIA'">
                            <xsl:value-of select="'BG'"/>
                        </xsl:when>
                        <xsl:when test="$code='BURKINA FASO'">
                            <xsl:value-of select="'BF'"/>
                        </xsl:when>
                        <xsl:when test="$code='BURUNDI'">
                            <xsl:value-of select="'BI'"/>
                        </xsl:when>
                        <xsl:when test="$code='CAMBODIA'">
                            <xsl:value-of select="'KH'"/>
                        </xsl:when>
                        <xsl:when test="$code='CAMEROON'">
                            <xsl:value-of select="'CM'"/>
                        </xsl:when>
                        <xsl:when test="$code='CANADA'">
                            <xsl:value-of select="'CA'"/>
                        </xsl:when>
                        <xsl:when test="$code='CAPE VERDE'">
                            <xsl:value-of select="'CV'"/>
                        </xsl:when>
                        <xsl:when test="$code='CAYMAN ISLANDS'">
                            <xsl:value-of select="'KY'"/>
                        </xsl:when>
                        <xsl:when test="$code='CENTRAL AFRICAN REPUBLIC'">
                            <xsl:value-of select="'CF'"/>
                        </xsl:when>
                        <xsl:when test="$code='CHAD'">
                            <xsl:value-of select="'TD'"/>
                        </xsl:when>
                        <xsl:when test="$code='CHILE'">
                            <xsl:value-of select="'CL'"/>
                        </xsl:when>
                        <xsl:when test="$code='CHINA'">
                            <xsl:value-of select="'CN'"/>
                        </xsl:when>
                        <xsl:when test="$code='CHRISTMAS ISLAND'">
                            <xsl:value-of select="'CX'"/>
                        </xsl:when>
                        <xsl:when test="$code='COCOS (KEELING) ISLANDS'">
                            <xsl:value-of select="'CC'"/>
                        </xsl:when>
                        <xsl:when test="$code='COLOMBIA'">
                            <xsl:value-of select="'CO'"/>
                        </xsl:when>
                        <xsl:when test="$code='COMOROS'">
                            <xsl:value-of select="'KM'"/>
                        </xsl:when>
                        <xsl:when test="$code='CONGO'">
                            <xsl:value-of select="'CG'"/>
                        </xsl:when>
                        <xsl:when test="$code='CONGO, THE DEMOCRATIC REPUBLIC OF THE'">
                            <xsl:value-of select="'CD'"/>
                        </xsl:when>
                        <xsl:when test="$code='COOK ISLANDS'">
                            <xsl:value-of select="'CK'"/>
                        </xsl:when>
                        <xsl:when test="$code='COSTA RICA'">
                            <xsl:value-of select="'CR'"/>
                        </xsl:when>
                        <xsl:when test="$code='COTE DIVOIRE'">
                            <xsl:value-of select="'CI'"/>
                        </xsl:when>
                        <xsl:when test="$code='CROATIA'">
                            <xsl:value-of select="'HR'"/>
                        </xsl:when>
                        <xsl:when test="$code='CUBA'">
                            <xsl:value-of select="'CU'"/>
                        </xsl:when>
                        <xsl:when test="$code='CYPRUS'">
                            <xsl:value-of select="'CY'"/>
                        </xsl:when>
                        <xsl:when test="$code='CZECH REPUBLIC'">
                            <xsl:value-of select="'CZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='DENMARK'">
                            <xsl:value-of select="'DK'"/>
                        </xsl:when>
                        <xsl:when test="$code='DJIBOUTI'">
                            <xsl:value-of select="'DJ'"/>
                        </xsl:when>
                        <xsl:when test="$code='DOMINICA'">
                            <xsl:value-of select="'DM'"/>
                        </xsl:when>
                        <xsl:when test="$code='DOMINICAN REPUBLIC'">
                            <xsl:value-of select="'DO'"/>
                        </xsl:when>
                        <xsl:when test="$code='ECUADOR'">
                            <xsl:value-of select="'EC'"/>
                        </xsl:when>
                        <xsl:when test="$code='EGYPT'">
                            <xsl:value-of select="'EG'"/>
                        </xsl:when>
                        <xsl:when test="$code='EL SALVADOR'">
                            <xsl:value-of select="'SV'"/>
                        </xsl:when>
                        <xsl:when test="$code='EQUATORIAL GUINEA'">
                            <xsl:value-of select="'GQ'"/>
                        </xsl:when>
                        <xsl:when test="$code='ERITREA'">
                            <xsl:value-of select="'ER'"/>
                        </xsl:when>
                        <xsl:when test="$code='ESTONIA'">
                            <xsl:value-of select="'EE'"/>
                        </xsl:when>
                        <xsl:when test="$code='ETHIOPIA'">
                            <xsl:value-of select="'ET'"/>
                        </xsl:when>
                        <xsl:when test="$code='FALKLAND ISLANDS (MALVINAS)'">
                            <xsl:value-of select="'FK'"/>
                        </xsl:when>
                        <xsl:when test="$code='FAROE ISLANDS'">
                            <xsl:value-of select="'FO'"/>
                        </xsl:when>
                        <xsl:when test="$code='FIJI'">
                            <xsl:value-of select="'FJ'"/>
                        </xsl:when>
                        <xsl:when test="$code='FINLAND'">
                            <xsl:value-of select="'FI'"/>
                        </xsl:when>
                        <xsl:when test="$code='FRANCE'">
                            <xsl:value-of select="'FR'"/>
                        </xsl:when>
                        <xsl:when test="$code='FRENCH GUIANA'">
                            <xsl:value-of select="'GF'"/>
                        </xsl:when>
                        <xsl:when test="$code='FRENCH POLYNESIA'">
                            <xsl:value-of select="'PF'"/>
                        </xsl:when>
                        <xsl:when test="$code='FRENCH SOUTHERN TERRITORIES'">
                            <xsl:value-of select="'TF'"/>
                        </xsl:when>
                        <xsl:when test="$code='GABON'">
                            <xsl:value-of select="'GA'"/>
                        </xsl:when>
                        <xsl:when test="$code='GAMBIA'">
                            <xsl:value-of select="'GM'"/>
                        </xsl:when>
                        <xsl:when test="$code='GEORGIA'">
                            <xsl:value-of select="'GE'"/>
                        </xsl:when>
                        <xsl:when test="$code='GERMANY'">
                            <xsl:value-of select="'DE'"/>
                        </xsl:when>
                        <xsl:when test="$code='GHANA'">
                            <xsl:value-of select="'GH'"/>
                        </xsl:when>
                        <xsl:when test="$code='GIBRALTAR'">
                            <xsl:value-of select="'GI'"/>
                        </xsl:when>
                        <xsl:when test="$code='GREECE'">
                            <xsl:value-of select="'GR'"/>
                        </xsl:when>
                        <xsl:when test="$code='GREENLAND'">
                            <xsl:value-of select="'GL'"/>
                        </xsl:when>
                        <xsl:when test="$code='GRENADA'">
                            <xsl:value-of select="'GD'"/>
                        </xsl:when>
                        <xsl:when test="$code='GUADELOUPE'">
                            <xsl:value-of select="'GP'"/>
                        </xsl:when>
                        <xsl:when test="$code='GUAM'">
                            <xsl:value-of select="'GU'"/>
                        </xsl:when>
                        <xsl:when test="$code='GUATEMALA'">
                            <xsl:value-of select="'GT'"/>
                        </xsl:when>
                        <xsl:when test="$code='GUERNSEY'">
                            <xsl:value-of select="'GG'"/>
                        </xsl:when>
                        <xsl:when test="$code='GUINEA'">
                            <xsl:value-of select="'GN'"/>
                        </xsl:when>
                        <xsl:when test="$code='GUINEA-BISSAU'">
                            <xsl:value-of select="'GW'"/>
                        </xsl:when>
                        <xsl:when test="$code='GUYANA'">
                            <xsl:value-of select="'GY'"/>
                        </xsl:when>
                        <xsl:when test="$code='HAITI'">
                            <xsl:value-of select="'HT'"/>
                        </xsl:when>
                        <xsl:when test="$code='HEARD ISLAND AND MCDONALD ISLANDS'">
                            <xsl:value-of select="'HM'"/>
                        </xsl:when>
                        <xsl:when test="$code='HOLY SEE (VATICAN CITY STATE)'">
                            <xsl:value-of select="'VA'"/>
                        </xsl:when>
                        <xsl:when test="$code='HONDURAS'">
                            <xsl:value-of select="'HN'"/>
                        </xsl:when>
                        <xsl:when test="$code='HONG KONG'">
                            <xsl:value-of select="'HK'"/>
                        </xsl:when>
                        <xsl:when test="$code='HUNGARY'">
                            <xsl:value-of select="'HU'"/>
                        </xsl:when>
                        <xsl:when test="$code='ICELAND'">
                            <xsl:value-of select="'IS'"/>
                        </xsl:when>
                        <xsl:when test="$code='INDIA'">
                            <xsl:value-of select="'IN'"/>
                        </xsl:when>
                        <xsl:when test="$code='INDONESIA'">
                            <xsl:value-of select="'ID'"/>
                        </xsl:when>
                        <xsl:when test="$code='IRAN, ISLAMIC REPUBLIC OF'">
                            <xsl:value-of select="'IR'"/>
                        </xsl:when>
                        <xsl:when test="$code='IRAQ'">
                            <xsl:value-of select="'IQ'"/>
                        </xsl:when>
                        <xsl:when test="$code='IRELAND'">
                            <xsl:value-of select="'IE'"/>
                        </xsl:when>
                        <xsl:when test="$code='ISLE OF MAN'">
                            <xsl:value-of select="'IM'"/>
                        </xsl:when>
                        <xsl:when test="$code='ISRAEL'">
                            <xsl:value-of select="'IL'"/>
                        </xsl:when>
                        <xsl:when test="$code='ITALY'">
                            <xsl:value-of select="'IT'"/>
                        </xsl:when>
                        <xsl:when test="$code='JAMAICA'">
                            <xsl:value-of select="'JM'"/>
                        </xsl:when>
                        <xsl:when test="$code='JAPAN'">
                            <xsl:value-of select="'JP'"/>
                        </xsl:when>
                        <xsl:when test="$code='JERSEY'">
                            <xsl:value-of select="'JE'"/>
                        </xsl:when>
                        <xsl:when test="$code='JORDAN'">
                            <xsl:value-of select="'JO'"/>
                        </xsl:when>
                        <xsl:when test="$code='KAZAKHSTAN'">
                            <xsl:value-of select="'KZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='KENYA'">
                            <xsl:value-of select="'KE'"/>
                        </xsl:when>
                        <xsl:when test="$code='KIRIBATI'">
                            <xsl:value-of select="'KI'"/>
                        </xsl:when>
                        <xsl:when test="$code='KOREA, DEMOCRATIC PEOPLES REPUBLIC OF'">
                            <xsl:value-of select="'KP'"/>
                        </xsl:when>
                        <xsl:when test="$code='KOREA, REPUBLIC OF'">
                            <xsl:value-of select="'KR'"/>
                        </xsl:when>
                        <xsl:when test="$code='KUWAIT'">
                            <xsl:value-of select="'KW'"/>
                        </xsl:when>
                        <xsl:when test="$code='KYRGYZSTAN'">
                            <xsl:value-of select="'KG'"/>
                        </xsl:when>
                        <xsl:when test="$code='LAO PEOPLES DEMOCRATIC REPUBLIC'">
                            <xsl:value-of select="'LA'"/>
                        </xsl:when>
                        <xsl:when test="$code='LATVIA'">
                            <xsl:value-of select="'LV'"/>
                        </xsl:when>
                        <xsl:when test="$code='LEBANON'">
                            <xsl:value-of select="'LB'"/>
                        </xsl:when>
                        <xsl:when test="$code='LESOTHO'">
                            <xsl:value-of select="'LS'"/>
                        </xsl:when>
                        <xsl:when test="$code='LIBERIA'">
                            <xsl:value-of select="'LR'"/>
                        </xsl:when>
                        <xsl:when test="$code='LIBYAN ARAB JAMAHIRIYA'">
                            <xsl:value-of select="'LY'"/>
                        </xsl:when>
                        <xsl:when test="$code='LIECHTENSTEIN'">
                            <xsl:value-of select="'LI'"/>
                        </xsl:when>
                        <xsl:when test="$code='LITHUANIA'">
                            <xsl:value-of select="'LT'"/>
                        </xsl:when>
                        <xsl:when test="$code='LUXEMBOURG'">
                            <xsl:value-of select="'LU'"/>
                        </xsl:when>
                        <xsl:when test="$code='MACAO'">
                            <xsl:value-of select="'MO'"/>
                        </xsl:when>
                        <xsl:when test="$code='MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF'">
                            <xsl:value-of select="'MK'"/>
                        </xsl:when>
                        <xsl:when test="$code='MADAGASCAR'">
                            <xsl:value-of select="'MG'"/>
                        </xsl:when>
                        <xsl:when test="$code='MALAWI'">
                            <xsl:value-of select="'MW'"/>
                        </xsl:when>
                        <xsl:when test="$code='MALAYSIA'">
                            <xsl:value-of select="'MY'"/>
                        </xsl:when>
                        <xsl:when test="$code='MALDIVES'">
                            <xsl:value-of select="'MV'"/>
                        </xsl:when>
                        <xsl:when test="$code='MALI'">
                            <xsl:value-of select="'ML'"/>
                        </xsl:when>
                        <xsl:when test="$code='MALTA'">
                            <xsl:value-of select="'MT'"/>
                        </xsl:when>
                        <xsl:when test="$code='MARSHALL ISLANDS'">
                            <xsl:value-of select="'MH'"/>
                        </xsl:when>
                        <xsl:when test="$code='MARTINIQUE'">
                            <xsl:value-of select="'MQ'"/>
                        </xsl:when>
                        <xsl:when test="$code='MAURITANIA'">
                            <xsl:value-of select="'MR'"/>
                        </xsl:when>
                        <xsl:when test="$code='MAURITIUS'">
                            <xsl:value-of select="'MU'"/>
                        </xsl:when>
                        <xsl:when test="$code='MAYOTTE'">
                            <xsl:value-of select="'YT'"/>
                        </xsl:when>
                        <xsl:when test="$code='MEXICO'">
                            <xsl:value-of select="'MX'"/>
                        </xsl:when>
                        <xsl:when test="$code='MICRONESIA, FEDERATED STATES OF'">
                            <xsl:value-of select="'FM'"/>
                        </xsl:when>
                        <xsl:when test="$code='MOLDOVA, REPUBLIC OF'">
                            <xsl:value-of select="'MD'"/>
                        </xsl:when>
                        <xsl:when test="$code='MONACO'">
                            <xsl:value-of select="'MC'"/>
                        </xsl:when>
                        <xsl:when test="$code='MONGOLIA'">
                            <xsl:value-of select="'MN'"/>
                        </xsl:when>
                        <xsl:when test="$code='MONTSERRAT'">
                            <xsl:value-of select="'MS'"/>
                        </xsl:when>
                        <xsl:when test="$code='MOROCCO'">
                            <xsl:value-of select="'MA'"/>
                        </xsl:when>
                        <xsl:when test="$code='MOZAMBIQUE'">
                            <xsl:value-of select="'MZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='MYANMAR'">
                            <xsl:value-of select="'MM'"/>
                        </xsl:when>
                        <xsl:when test="$code='NAMIBIA'">
                            <xsl:value-of select="'NA'"/>
                        </xsl:when>
                        <xsl:when test="$code='NAURU'">
                            <xsl:value-of select="'NR'"/>
                        </xsl:when>
                        <xsl:when test="$code='NEPAL'">
                            <xsl:value-of select="'NP'"/>
                        </xsl:when>
                        <xsl:when test="$code='NETHERLANDS'">
                            <xsl:value-of select="'NL'"/>
                        </xsl:when>
                        <xsl:when test="$code='NETHERLANDS ANTILLES'">
                            <xsl:value-of select="'AN'"/>
                        </xsl:when>
                        <xsl:when test="$code='NEW CALEDONIA'">
                            <xsl:value-of select="'NC'"/>
                        </xsl:when>
                        <xsl:when test="$code='NEW ZEALAND'">
                            <xsl:value-of select="'NZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='NICARAGUA'">
                            <xsl:value-of select="'NI'"/>
                        </xsl:when>
                        <xsl:when test="$code='NIGER'">
                            <xsl:value-of select="'NE'"/>
                        </xsl:when>
                        <xsl:when test="$code='NIGERIA'">
                            <xsl:value-of select="'NG'"/>
                        </xsl:when>
                        <xsl:when test="$code='NIUE'">
                            <xsl:value-of select="'NU'"/>
                        </xsl:when>
                        <xsl:when test="$code='NORFOLK ISLAND'">
                            <xsl:value-of select="'NF'"/>
                        </xsl:when>
                        <xsl:when test="$code='NORTHERN MARIANA ISLANDS'">
                            <xsl:value-of select="'MP'"/>
                        </xsl:when>
                        <xsl:when test="$code='NORWAY'">
                            <xsl:value-of select="'NO'"/>
                        </xsl:when>
                        <xsl:when test="$code='OMAN'">
                            <xsl:value-of select="'OM'"/>
                        </xsl:when>
                        <xsl:when test="$code='PAKISTAN'">
                            <xsl:value-of select="'PK'"/>
                        </xsl:when>
                        <xsl:when test="$code='PALAU'">
                            <xsl:value-of select="'PW'"/>
                        </xsl:when>
                        <xsl:when test="$code='PALESTINIAN TERRITORY, OCCUPIED'">
                            <xsl:value-of select="'PS'"/>
                        </xsl:when>
                        <xsl:when test="$code='PANAMA'">
                            <xsl:value-of select="'PA'"/>
                        </xsl:when>
                        <xsl:when test="$code='PAPUA NEW GUINEA'">
                            <xsl:value-of select="'PG'"/>
                        </xsl:when>
                        <xsl:when test="$code='PARAGUAY'">
                            <xsl:value-of select="'PY'"/>
                        </xsl:when>
                        <xsl:when test="$code='PERU'">
                            <xsl:value-of select="'PE'"/>
                        </xsl:when>
                        <xsl:when test="$code='PHILIPPINES'">
                            <xsl:value-of select="'PH'"/>
                        </xsl:when>
                        <xsl:when test="$code='PITCAIRN'">
                            <xsl:value-of select="'PN'"/>
                        </xsl:when>
                        <xsl:when test="$code='POLAND'">
                            <xsl:value-of select="'PL'"/>
                        </xsl:when>
                        <xsl:when test="$code='PORTUGAL'">
                            <xsl:value-of select="'PT'"/>
                        </xsl:when>
                        <xsl:when test="$code='PUERTO RICO'">
                            <xsl:value-of select="'PR'"/>
                        </xsl:when>
                        <xsl:when test="$code='QATAR'">
                            <xsl:value-of select="'QA'"/>
                        </xsl:when>
                        <xsl:when test="$code='REUNION'">
                            <xsl:value-of select="'RE'"/>
                        </xsl:when>
                        <xsl:when test="$code='ROMANIA'">
                            <xsl:value-of select="'RO'"/>
                        </xsl:when>
                        <xsl:when test="$code='RUSSIAN FEDERATION'">
                            <xsl:value-of select="'RU'"/>
                        </xsl:when>
                        <xsl:when test="$code='RWANDA'">
                            <xsl:value-of select="'RW'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT HELENA'">
                            <xsl:value-of select="'SH'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT KITTS AND NEVIS'">
                            <xsl:value-of select="'KN'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT LUCIA'">
                            <xsl:value-of select="'LC'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT PIERRE AND MIQUELON'">
                            <xsl:value-of select="'PM'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT VINCENT AND THE GRENADINES'">
                            <xsl:value-of select="'VC'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAMOA'">
                            <xsl:value-of select="'WS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAN MARINO'">
                            <xsl:value-of select="'SM'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAO TOME AND PRINCIPE'">
                            <xsl:value-of select="'ST'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAUDI ARABIA'">
                            <xsl:value-of select="'SA'"/>
                        </xsl:when>
                        <xsl:when test="$code='SENEGAL'">
                            <xsl:value-of select="'SN'"/>
                        </xsl:when>
                        <xsl:when test="$code='SERBIA AND MONTENEGRO'">
                            <xsl:value-of select="'CS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SEYCHELLES'">
                            <xsl:value-of select="'SC'"/>
                        </xsl:when>
                        <xsl:when test="$code='SIERRA LEONE'">
                            <xsl:value-of select="'SL'"/>
                        </xsl:when>
                        <xsl:when test="$code='SINGAPORE'">
                            <xsl:value-of select="'SG'"/>
                        </xsl:when>
                        <xsl:when test="$code='SLOVAKIA'">
                            <xsl:value-of select="'SK'"/>
                        </xsl:when>
                        <xsl:when test="$code='SLOVENIA'">
                            <xsl:value-of select="'SI'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOLOMON ISLANDS'">
                            <xsl:value-of select="'SB'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOMALIA'">
                            <xsl:value-of select="'SO'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOUTH AFRICA'">
                            <xsl:value-of select="'ZA'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS'">
                            <xsl:value-of select="'GS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SPAIN'">
                            <xsl:value-of select="'ES'"/>
                        </xsl:when>
                        <xsl:when test="$code='SRI LANKA'">
                            <xsl:value-of select="'LK'"/>
                        </xsl:when>
                        <xsl:when test="$code='SUDAN'">
                            <xsl:value-of select="'SD'"/>
                        </xsl:when>
                        <xsl:when test="$code='SURINAME'">
                            <xsl:value-of select="'SR'"/>
                        </xsl:when>
                        <xsl:when test="$code='SVALBARD AND JAN MAYEN'">
                            <xsl:value-of select="'SJ'"/>
                        </xsl:when>
                        <xsl:when test="$code='SWAZILAND'">
                            <xsl:value-of select="'SZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='SWEDEN'">
                            <xsl:value-of select="'SE'"/>
                        </xsl:when>
                        <xsl:when test="$code='SWITZERLAND'">
                            <xsl:value-of select="'CH'"/>
                        </xsl:when>
                        <xsl:when test="$code='SYRIAN ARAB REPUBLIC'">
                            <xsl:value-of select="'SY'"/>
                        </xsl:when>
                        <xsl:when test="$code='TAIWAN, PROVINCE OF CHINA'">
                            <xsl:value-of select="'TW'"/>
                        </xsl:when>
                        <xsl:when test="$code='TAJIKISTAN'">
                            <xsl:value-of select="'TJ'"/>
                        </xsl:when>
                        <xsl:when test="$code='TANZANIA, UNITED REPUBLIC OF'">
                            <xsl:value-of select="'TZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='THAILAND'">
                            <xsl:value-of select="'TH'"/>
                        </xsl:when>
                        <xsl:when test="$code='TIMOR-LESTE'">
                            <xsl:value-of select="'TL'"/>
                        </xsl:when>
                        <xsl:when test="$code='TOGO'">
                            <xsl:value-of select="'TG'"/>
                        </xsl:when>
                        <xsl:when test="$code='TOKELAU'">
                            <xsl:value-of select="'TK'"/>
                        </xsl:when>
                        <xsl:when test="$code='TONGA'">
                            <xsl:value-of select="'TO'"/>
                        </xsl:when>
                        <xsl:when test="$code='TRINIDAD AND TOBAGO'">
                            <xsl:value-of select="'TT'"/>
                        </xsl:when>
                        <xsl:when test="$code='TUNISIA'">
                            <xsl:value-of select="'TN'"/>
                        </xsl:when>
                        <xsl:when test="$code='TURKEY'">
                            <xsl:value-of select="'TR'"/>
                        </xsl:when>
                        <xsl:when test="$code='TURKMENISTAN'">
                            <xsl:value-of select="'TM'"/>
                        </xsl:when>
                        <xsl:when test="$code='TURKS AND CAICOS ISLANDS'">
                            <xsl:value-of select="'TC'"/>
                        </xsl:when>
                        <xsl:when test="$code='TUVALU'">
                            <xsl:value-of select="'TV'"/>
                        </xsl:when>
                        <xsl:when test="$code='UGANDA'">
                            <xsl:value-of select="'UG'"/>
                        </xsl:when>
                        <xsl:when test="$code='UKRAINE'">
                            <xsl:value-of select="'UA'"/>
                        </xsl:when>
                        <xsl:when test="$code='UNITED ARAB EMIRATES'">
                            <xsl:value-of select="'AE'"/>
                        </xsl:when>
                        <xsl:when test="$code='UNITED KINGDOM'">
                            <xsl:value-of select="'GB'"/>
                        </xsl:when>
                        <xsl:when test="$code='UNITED STATES'">
                            <xsl:value-of select="'US'"/>
                        </xsl:when>
                        <xsl:when test="$code='UNITED STATES MINOR OUTLYING ISLANDS'">
                            <xsl:value-of select="'UM'"/>
                        </xsl:when>
                        <xsl:when test="$code='URUGUAY'">
                            <xsl:value-of select="'UY'"/>
                        </xsl:when>
                        <xsl:when test="$code='UZBEKISTAN'">
                            <xsl:value-of select="'UZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='VANUATU'">
                            <xsl:value-of select="'VU'"/>
                        </xsl:when>
                        <xsl:when test="$code='VENEZUELA'">
                            <xsl:value-of select="'VE'"/>
                        </xsl:when>
                        <xsl:when test="$code='VIET NAM'">
                            <xsl:value-of select="'VN'"/>
                        </xsl:when>
                        <xsl:when test="$code='VIRGIN ISLANDS, BRITISH'">
                            <xsl:value-of select="'VG'"/>
                        </xsl:when>
                        <xsl:when test="$code='VIRGIN ISLANDS, U.S.'">
                            <xsl:value-of select="'VI'"/>
                        </xsl:when>
                        <xsl:when test="$code='WALLIS AND FUTUNA'">
                            <xsl:value-of select="'WF'"/>
                        </xsl:when>
                        <xsl:when test="$code='WESTERN SAHARA'">
                            <xsl:value-of select="'EH'"/>
                        </xsl:when>
                        <xsl:when test="$code='YEMEN'">
                            <xsl:value-of select="'YE'"/>
                        </xsl:when>
                        <xsl:when test="$code='ZAMBIA'">
                            <xsl:value-of select="'ZM'"/>
                        </xsl:when>
                        <xsl:when test="$code='ZIMBABWE'">
                            <xsl:value-of select="'ZW'"/>
                        </xsl:when>
                        <!-- BGT included list -->
                        <xsl:when test="$code='EASTERN AUSTRALIA'">
                            <xsl:value-of select="'AU'"/>
                        </xsl:when>
                        <xsl:when test="$code='WESTERN AUSTRALIA'">
                            <xsl:value-of select="'AU'"/>
                        </xsl:when>
                        <xsl:when test="$code='CENTRAL AUSTRALIA'">
                            <xsl:value-of select="'AU'"/>
                        </xsl:when>
                        <xsl:when test="$code='AMERICA'">
                            <xsl:value-of select="'US'"/>
                        </xsl:when>
                        <xsl:when test="$code='ANTARCTICA'">
                            <xsl:value-of select="'AQ'"/>
                        </xsl:when>
                        <xsl:when test="$code='ANTIGUA &amp; BARBUDA'">
                            <xsl:value-of select="'AG'"/>
                        </xsl:when>
                        <xsl:when test="$code='ANTIGUA AND BARBUDA'">
                            <xsl:value-of select="'AG'"/>
                        </xsl:when>				
                        <xsl:when test="$code='ANTIGUA &amp;AMP; BARBUDA'">
                            <xsl:value-of select="'AG'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOSNIA &amp;AMP; HERZEGOVINA'">
                            <xsl:value-of select="'BA'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOSNIA &amp; HERZEGOVINA'">
                            <xsl:value-of select="'BA'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOSNIA AND HERZEGOVINA'">
                            <xsl:value-of select="'BA'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOSNIA AND HERZEGOWINA'">
                            <xsl:value-of select="'BA'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOUVET ISLAND'">
                            <xsl:value-of select="'BV'"/>
                        </xsl:when>
                        <xsl:when test="$code='BOUVETISLAND'">
                            <xsl:value-of select="'BV'"/>
                        </xsl:when>
                        <xsl:when test="$code='BRASIL'">
                            <xsl:value-of select="'BR'"/>
                        </xsl:when>
                        <xsl:when test="$code='BURKINA FASO'">
                            <xsl:value-of select="'BF'"/>
                        </xsl:when>
                        <xsl:when test="$code='BURKINAFASO'">
                            <xsl:value-of select="'BF'"/>
                        </xsl:when>
                        <xsl:when test="$code='BURMA'">
                            <xsl:value-of select="'MM'"/>
                        </xsl:when>
                        <xsl:when test="$code='COCOS (KEELING) ISLANDS'">
                            <xsl:value-of select="'CC'"/>
                        </xsl:when>
                        <xsl:when test="$code='COCOS ISLANDS'">
                            <xsl:value-of select="'CC'"/>
                        </xsl:when>
                        <xsl:when test="$code='COLUMBIA'">
                            <xsl:value-of select="'CO'"/>
                        </xsl:when>
                        <xsl:when test="$code='CONGO, DEMOCRATIC REPUBLIC'">
                            <xsl:value-of select="'CD'"/>
                        </xsl:when>
                        <xsl:when test="$code='COSTARICA'">
                            <xsl:value-of select="'CR'"/>
                        </xsl:when>
                        <xsl:when test="$code='CZECHOSLOVAKIA'">
                            <xsl:value-of select="'CZ'"/>
                        </xsl:when>
                        <xsl:when test="$code='DEMOCRATIC REPUBLIC OF THE CONGO'">
                            <xsl:value-of select="'CD'"/>
                        </xsl:when>
                        <xsl:when test="$code='EAST TIMOR'">
                            <xsl:value-of select="'TL'"/>
                        </xsl:when>
                        <xsl:when test="$code='EASTTIMOR'">
                            <xsl:value-of select="'TL'"/>
                        </xsl:when>
                        <xsl:when test="$code='ENGLAND'">
                            <xsl:value-of select="'GB'"/>
                        </xsl:when>
                        <xsl:when test="$code='FALKLAND ISLANDS'">
                            <xsl:value-of select="'FK'"/>
                        </xsl:when>
                        <xsl:when test="$code='FALKLAND ISLANDS (MALVINAS)'">
                            <xsl:value-of select="'FK'"/>
                        </xsl:when>
                        <xsl:when test="$code='FEDERAL REPUBLIC OF GERMANY'">
                            <xsl:value-of select="'DE'"/>
                        </xsl:when>
                        <xsl:when test="$code='FRG'">
                            <xsl:value-of select="'DE'"/>
                        </xsl:when>
                        <xsl:when test="$code='GREAT BRITAIN'">
                            <xsl:value-of select="'GB'"/>
                        </xsl:when>
                        <xsl:when test="$code='GUINEA - BISSAU'">
                            <xsl:value-of select="'GW'"/>
                        </xsl:when>
                        <xsl:when test="$code='GUINEA BISSAU'">
                            <xsl:value-of select="'GW'"/>
                        </xsl:when>
                        <xsl:when test="$code='HEARD &amp; MCDONALD ISLANDS'">
                            <xsl:value-of select="'HM'"/>
                        </xsl:when>
                        <xsl:when test="$code='HEARD &amp;AMP; MCDONALD ISLANDS'">
                            <xsl:value-of select="'HM'"/>
                        </xsl:when>
                        <xsl:when test="$code='HEARD AND MC DONALD ISLANDS'">
                            <xsl:value-of select="'HM'"/>
                        </xsl:when>
                        <xsl:when test="$code='HEARD AND MCDONALD ISLANDS'">
                            <xsl:value-of select="'HM'"/>
                        </xsl:when>
                        <xsl:when test="$code='HOLYSEE'">
                            <xsl:value-of select="'VA'"/>
                        </xsl:when>
                        <xsl:when test="$code='IVORY COAST'">
                            <xsl:value-of select="'CI'"/>
                        </xsl:when>
                        <xsl:when test="$code='KINGDOM OF SAUDI ARABIA'">
                            <xsl:value-of select="'SA'"/>
                        </xsl:when>
                        <xsl:when test="$code='KOREA'">
                            <xsl:value-of select="'KR'"/>
                        </xsl:when>
                        <xsl:when test="$code='KOREA NORTH'">
                            <xsl:value-of select="'KP'"/>
                        </xsl:when>
                        <xsl:when test="$code='KOREA SOUTH'">
                            <xsl:value-of select="'KR'"/>
                        </xsl:when>
                        <xsl:when test="$code='KOREA, NORTH'">
                            <xsl:value-of select="'KP'"/>
                        </xsl:when>
                        <xsl:when test="$code='KOREA, SOUTH'">
                            <xsl:value-of select="'KR'"/>
                        </xsl:when>
                        <xsl:when test="$code='KSA'">
                            <xsl:value-of select="'SA'"/>
                        </xsl:when>
                        <xsl:when test="$code='MALVINAS'">
                            <xsl:value-of select="'FK'"/>
                        </xsl:when>
                        <xsl:when test="$code='MARSHALL ISLANDS'">
                            <xsl:value-of select="'MH'"/>
                        </xsl:when>
                        <xsl:when test="$code='MICRONESIA'">
                            <xsl:value-of select="'FM'"/>
                        </xsl:when>
                        <xsl:when test="$code='MICRONESIA, FEDERATED STATES OF'">
                            <xsl:value-of select="'FM'"/>
                        </xsl:when>
                        <xsl:when test="$code='MONTENEGRO'">
                            <xsl:value-of select="'CS'"/>
                        </xsl:when>
                        <xsl:when test="$code='P R'">
                            <xsl:value-of select="'PR'"/>
                        </xsl:when>
                        <xsl:when test="$code='P R C'">
                            <xsl:value-of select="'CN'"/>
                        </xsl:when>
                        <xsl:when test="$code='P R CHINA'">
                            <xsl:value-of select="'CN'"/>
                        </xsl:when>
                        <xsl:when test="$code='PALESTINE'">
                            <xsl:value-of select="'PS'"/>
                        </xsl:when>
                        <xsl:when test="$code='PALESTINIAN TERRITORY'">
                            <xsl:value-of select="'PS'"/>
                        </xsl:when>
                        <xsl:when test="$code='PEOPLES REPUBLIC OF CHINA'">
                            <xsl:value-of select="'CN'"/>
                        </xsl:when>
                        <xsl:when test="$code='PHILIPPNES'">
                            <xsl:value-of select="'RP'"/>
                        </xsl:when>
                        <xsl:when test="$code='PITCAIRN'">
                            <xsl:value-of select="'PN'"/>
                        </xsl:when>
                        <xsl:when test="$code='PITCAIRN ISLANDS'">
                            <xsl:value-of select="'PN'"/>
                        </xsl:when>
                        <xsl:when test="$code='PR CHINA'">
                            <xsl:value-of select="'CN'"/>
                        </xsl:when>
                        <xsl:when test="$code='REPUBLIC OF KOREA'">
                            <xsl:value-of select="'KP'"/>
                        </xsl:when>
                        <xsl:when test="$code='REPUBLIC OF PANAMA'">
                            <xsl:value-of select="'PA'"/>
                        </xsl:when>
                        <xsl:when test="$code='ROK'">
                            <xsl:value-of select="'KP'"/>
                        </xsl:when>
                        <xsl:when test="$code='RUSSIA'">
                            <xsl:value-of select="'RU'"/>
                        </xsl:when>
                        <xsl:when test="$code='S A'">
                            <xsl:value-of select="'SA'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT HELENA'">
                            <xsl:value-of select="'SH'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT KITTS &amp; NEVIS'">
                            <xsl:value-of select="'KN'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT KITTS &amp;AMP; NEVIS'">
                            <xsl:value-of select="'KN'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT PIERRE AND MIQUELON'">
                            <xsl:value-of select="'PM'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT VINCENT &amp; THE GRENADINES'">
                            <xsl:value-of select="'VC'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAINT VINCENT &amp; THE GRENADINES'">
                            <xsl:value-of select="'VC'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAO TOME &amp; PRINCIPE'">
                            <xsl:value-of select="'ST'"/>
                        </xsl:when>
                        <xsl:when test="$code='SAO TOME &amp;AMP; PRINCIPE'">
                            <xsl:value-of select="'ST'"/>
                        </xsl:when>
                        <xsl:when test="$code='SCOTLAND'">
                            <xsl:value-of select="'GB'"/>
                        </xsl:when>
                        <xsl:when test="$code='SERBIA'">
                            <xsl:value-of select="'CS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SERBIA &amp; MONTENEGRO'">
                            <xsl:value-of select="'CS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SERBIA &amp;AMP; MONTENEGRO'">
                            <xsl:value-of select="'CS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOUTH GEORGIA &amp; SOUTH SANDWICH ISLANDS'">
                            <xsl:value-of select="'GS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOUTH GEORGIA &amp;AMP; SOUTH SANDWICH ISLANDS'">
                            <xsl:value-of select="'GS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOUTH GEORGIA AND SOUTH SANDWICH ISLANDS'">
                            <xsl:value-of select="'GS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS'">
                            <xsl:value-of select="'GS'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOUTH KOREA'">
                            <xsl:value-of select="'KR'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOUTH VIETNAM'">
                            <xsl:value-of select="'VN'"/>
                        </xsl:when>
                        <xsl:when test="$code='SOUTHERN AFRICA'">
                            <xsl:value-of select="'ZA'"/>
                        </xsl:when>
                        <xsl:when test="$code='ST . HELENA'">
                            <xsl:value-of select="'SH'"/>
                        </xsl:when>
                        <xsl:when test="$code='ST . PIERRE AND MIQUELON'">
                            <xsl:value-of select="'PM'"/>
                        </xsl:when>
                        <xsl:when test="$code='ST HELENA'">
                            <xsl:value-of select="'SH'"/>
                        </xsl:when>
                        <xsl:when test="$code='ST PIERRE &amp; MIQUELON'">
                            <xsl:value-of select="'PM'"/>
                        </xsl:when>
                        <xsl:when test="$code='ST PIERRE &amp;AMP; MIQUELON'">
                            <xsl:value-of select="'PM'"/>
                        </xsl:when>
                        <xsl:when test="$code='ST PIERRE AND MIQUELON'">
                            <xsl:value-of select="'PM'"/>
                        </xsl:when>
                        <xsl:when test="$code='SVALBARD &amp; JANMAYEN ISLANDS'">
                            <xsl:value-of select="'SJ'"/>
                        </xsl:when>
                        <xsl:when test="$code='SVALBARD &amp;AMP; JANMAYEN ISLANDS'">
                            <xsl:value-of select="'SJ'"/>
                        </xsl:when>
                        <xsl:when test="$code='SVALBARD AND JAN MAYEN'">
                            <xsl:value-of select="'SJ'"/>
                        </xsl:when>
                        <xsl:when test="$code='SVALBARD AND JANMAYEN ISLANDS'">
                            <xsl:value-of select="'SJ'"/>
                        </xsl:when>
                        <xsl:when test="$code='THE NETHERLANDS'">
                            <xsl:value-of select="'NL'"/>
                        </xsl:when>
                        <xsl:when test="$code='TIBET'">
                            <xsl:value-of select="'CN'"/>
                        </xsl:when>
                        <xsl:when test="$code='TIMOR LESTE'">
                            <xsl:value-of select="'TL'"/>
                        </xsl:when>
                        <xsl:when test="$code='TRINIDAD &amp; TOBAGO'">
                            <xsl:value-of select="'TT'"/>
                        </xsl:when>
                        <xsl:when test="$code='TRINIDAD &amp;AMP; TOBAGO'">
                            <xsl:value-of select="'TT'"/>
                        </xsl:when>
                        <xsl:when test="$code='TURKS &amp; CAICOS ISLANDS , TUVALU'">
                            <xsl:value-of select="'TC'"/>
                        </xsl:when>
                        <xsl:when test="$code='TURKS &amp;AMP; CAICOS ISLANDS , TUVALU'">
                            <xsl:value-of select="'TC'"/>
                        </xsl:when>
                        <xsl:when test="$code='TURKS AND CAICOS ISLANDS'">
                            <xsl:value-of select="'TC'"/>
                        </xsl:when>
                        <xsl:when test="$code='TURKS AND CAICOS ISLANDS , TUVALU'">
                            <xsl:value-of select="'TC'"/>
                        </xsl:when>
                        <xsl:when test="$code='U K'">
                            <xsl:value-of select="'GB'"/>
                        </xsl:when>
                        <xsl:when test="$code='U S'">
                            <xsl:value-of select="'US'"/>
                        </xsl:when>
                        <xsl:when test="$code='U S A'">
                            <xsl:value-of select="'US'"/>
                        </xsl:when>
                        <xsl:when test="$code='UK'">
                            <xsl:value-of select="'GB'"/>
                        </xsl:when>
                        <xsl:when test="$code='UNITED STATES MINOR OUTLYING ISLANDS'">
                            <xsl:value-of select="'UM'"/>
                        </xsl:when>
                        <xsl:when test="$code='UNITED STATES OF AMERICA'">
                            <xsl:value-of select="'US'"/>
                        </xsl:when>
                        <xsl:when test="$code='US'">
                            <xsl:value-of select="'US'"/>
                        </xsl:when>
                        <xsl:when test="$code='US OF A'">
                            <xsl:value-of select="'US'"/>
                        </xsl:when>
                        <xsl:when test="$code='USA'">
                            <xsl:value-of select="'US'"/>
                        </xsl:when>
                        <xsl:when test="$code='USSR'">
                            <xsl:value-of select="'RU'"/>
                        </xsl:when>
                        <xsl:when test="$code='VATICAN CITY STATE'">
                            <xsl:value-of select="'VA'"/>
                        </xsl:when>
                        <xsl:when test="$code='VATICAN CITY STATE (HOLY SEE)'">
                            <xsl:value-of select="'VA'"/>
                        </xsl:when>
                        <xsl:when test="$code='VIRGIN ISLANDS, BRITISH'">
                            <xsl:value-of select="'VG'"/>
                        </xsl:when>
                        <xsl:when test="$code='VIRGIN ISLANDS, U.S.'">
                            <xsl:value-of select="'VI'"/>
                        </xsl:when>
                        <xsl:when test="$code='W GERMANY'">
                            <xsl:value-of select="'DE'"/>
                        </xsl:when>
                        <xsl:when test="$code='WALES'">
                            <xsl:value-of select="'GB'"/>
                        </xsl:when>
                        <xsl:when test="$code='WALLIS AND FUTUNA'">
                            <xsl:value-of select="'WF'"/>
                        </xsl:when>
                        <xsl:when test="$code='WEST AUSTRALIA'">
                            <xsl:value-of select="'AU'"/>
                        </xsl:when>
                        <xsl:when test="$code='WEST GERMANY'">
                            <xsl:value-of select="'DE'"/>
                        </xsl:when>
                        <xsl:when test="$code='WEST INDIES'">
                            <xsl:value-of select="'WI'"/>
                        </xsl:when>
                        <xsl:when test="$code='TAHITI'">
                            <xsl:value-of select="'PF'"/>
                        </xsl:when>
                        <!-- BGT included, if the country does not match with the above list add the default country code based upon the version of Lens/XRay -->
                        <xsl:otherwise>
                            <xsl:value-of select="$countryCode"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </CountryCode>
    </xsl:template>

</xsl:stylesheet>
