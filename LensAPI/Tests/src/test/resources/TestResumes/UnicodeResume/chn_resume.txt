﻿
Any Candidate  CV goes to Apple Xu.
Dear Sir/Madam,  
      
        Parts: 1. cover letter, 2.English CV, 3.Chinese CV

        Thanks a lot for your help, pls check my CV__(english & chinese) followed carefully and introduce me this good job asap or  top positions in prestigious companies/etc, thanks! 
       
       I am at Guangdong, pls call me at my  Beijing # 137-18134069 or email me first. I will check it occasionally,
tks! Mark Liao
    
       Simply  I gained MSc_chemistry, organics (pharma) synthesis in 1995, top10% of Zhongshan Univ(top10 univ in China), and have been in pharma & chemicals & instruments & business consulting fields for about 10+ yrs, mainly dutied on R&D (out-patented & me-too & me-better drugs & specialty chemicals), drug preclinical & marketing licence-application from SFDA, pharma/fine chemicals/Chinese medicines/medical equipments market suvey/research, plant/operations/site mgmt, project mgmt, staffing training, college education(Univ & postgraduate level--drug design,etc), import & export ( for businessmen over 50 countries), translation, scale-up, operations(process/EHS/TQA+QC) mgmt, sales & marketing, PR & Gov. R, CRM, SCM, localization, I & E, HR, Hi-tech industralization, marketalization, management & business--financing consulting, government consulting (for provincial & central level), etc.
     
        I can speak English fluently and know some German, Japanese, Cantonese and some dialects in China. Good understanding of China/International cultures, economy, politics, varied government systems, business & social environments as well as pharma/chemicals & FDA/COS/DMF/ISO 9002/14001/etc_related standards & CA/BA/Medline/SCI/EI/USPTO/etc databases and industrial ERP_SAP/Exact  & sigma 6  & modern edge-cutting mgmt techniques & thoughts, etc.

       Interested area:  Top executives/International Business/Investment banking/M&A_integration/consulting/Venture capitals/ biopharma_INDs & drug discovery/functional chemicals/materials/nanoproducts/E-web-business/softwares/energy_power &  oil & gas/logistics/R&D & industralization/CxOs,sales & marketing, operations,service,etc. 

       Better to send me some job-related contract samples (salary vs job duties and benefits_housing/insurances/car/etc) and some more info about your mgmt team.

       Sorry for any inconvenience. 

 Sincerely yours,

 Mark Liao Mingxiang

 Guangzhou # 132-65982847  or Beijing # 137-18134069 , now at Guangdong preferably until  25/07/2007

PS:         CV----Mark Liao 

xmark1218@yahoo.com.cn  
xmark123@hotmail.com

Salary expectation: 20-100K+bonus/benefits or stock options preferably or negotiable

Date available: anytime 
Born: Dec.18, 1969, male, Guizhou, PRC.   Single, Guangzhou resident(Hukou).
Height: 169cm; weight: 58kg; health: excellent.
Short-sighted: 250 degree.

Education:
1992.9--1995.7   Zhongshan Univ.(ZSU) Organic(pharma) Chemistry, MSc, top 5% Vice-president, Head of academic dept.in ZSU Chemical Students Association; Outstanding Postgraduate Award of Guangdong Province (1995)

1986.9--1990.7  Sichuan Univ.  (organic) Chem. Major, BSc, top 10%

Working:
2006.10-      Stopped now for poor management & salary,  Process R&D Projects / Sales & Marketing & Business Mgr of I & E__fine chemicals & APIs, reported to GM (two USA_PhDs  with >14yrs R&D experience at Merck & Bristol-Myers Squibb), Nengte Pharma Inc,  www .nengte .com  &  Beijing Pewiyond Pharmaceutical Technology Development Co., Ltd, www.pewiyond.com

2005- 2006.9   Teaching on "Drug molecular design / polymers & biomaterials in Pharma Industry /Pharma Process Technologies"/etc, Pharm Dept, School of Life Science, Guizhou Univ.(GZU), Guiyang, China; initiator of Biopharma Industrial Park of GZU and R&D center of Guizhou provinicial Biopharma

2004-    Pharmaron Beijing Pharma Co,LTD, Duties: Project_NCMs synthesis & process optimizaton, Head/mgmt of lab; Partner of USA Kalypsys Inc & Memory Pharma; stopped for poor mgmt/EHS of family venture & low technologies of >15 yrs USA_working experience of PhDs: business-pharma CRO service, scale-about150 people

2003-   Wuxi Pharmatech Ltd_Shanghai R&D Centre, partner of Merck/Pfizer/etc: business-pharma CRO service & R&D & production, sacle-about 100 people; Duties: R&D project mgmt ( CRO moleculars);

       Guangdong Paper industry Institute__Paper Chemicals: business-paper chemicals & related technologies development & production, scale-around 200 people; duties: paper chemicals R&D, scale-up, production & operations, new product market launching;

       (USA_invested) Goldtorus Inc_Shanghai,  (ERP+GMP/GSP/etc): business--ERP+cGXP software development & service, scale-- around 20 people; duties: reseach of information system (mgmt ERP+cGXP softwares) on top 20 worldwide pharma companies, and software structure(model) design, and leading of software developments in cooperation with CTO, sales & marketing of pharma softwares, assisting on company website design;

      (Italy Owned)Beijing Crespi Synthetic Leather Co.,LTD ( business--synthetic leather production & trade, scale--around 100 people);duties: inside sales & export & QA+production+technical system; 

      (M&Aed by Fangzhen Scitech Group) Chongqing Southwest Synthetic Pharma LTD (business--synthetic pharma development & production & export, scale--around 1500 people); duties: production & scale-up & R&D project,etc;

2002   (German_owned) Vegra Shanghai Printing Materials & Supplies Co., Ltd (business--printing chemicals & related production & trade, scale--~100 people),  Tech Manager: duties--QA&R+D/Production/Tech service/helping on  MIS-CRM+ERP/SCM+logistics+I&E/HR/finance, etc. Report directly to Managing Director Mr Andreas from Germany.

2001  Guangzhou HPW Sci.& Tech Co. Ltd ( business--analytical instruments trade & distributor & agent, scale--~20 people ), duties: sales engineer/manager of science instruments--HPLC,GC,ICP,GC/HPLC--MS....;

       Canton Fair_China Export, Part-time translator/business associate for ~80 foreign businessmen from ~ 40 countries & export associate for ~30 local exporters

2000  (Swiss-owned) Ciba Guangdong Specialty Chemicals Co.Ltd (AsiaPacific center) ( business--textile chemicals production, scale--around 130 people), duties: process/QA/EHS/production mgmt & chemist,etc;

      (USA fund shared) Shenzhen Taita Pharm Ltd, business--pharma (Chinese medicine + chemical pharma) R&D & production & marketalization, scale--around 800 people;duties: senior R&D chemist/supervisor, report to a USA_returned Mgr with degree of USA_3Masters+MBA

1998.1--1999.12  China Chemical & New Material Company, Guangzhou Branch , business--chemicals import & distributor & agent, scale--around 100 people; duties: sales of Dow polyglycols, etc; 

      (German-partnered) Ackermann Guangzhou Medical Equipment Co.Ltd: business--medical instruments & equipments trade & agent, scale-- ~30 people; duties: sales & mgmt of medical instruments & equipments

      Beijing Aerospace Changfeng Medical Equipment Co.Ltd: business--medical equipment production  & development & marketng, scale-- around 200 people; duties: sales & mgmt of medical instruments & equipments  in south China

      Beijing Genosys Sci & Tech Co.Ltd: business-- scientific instruments trade & agent, scale-- ~30 people; duties: sales & marketing of Ackermann/etc brands of  Medical/scientific equipments, etc in south China;

      (USA-invested) Pan-America Guangzhou Business Consulting Co.Ltd: business--consulting & investing banking service, scale--around 20 people;  E-trade (import/export), investing/financing in USA, project management, etc consulting  ----

1995.7--1997.12  Guangzhou Pharma Industry Institute, business-- pharma R&D service & production, scale-- around 400 people, Dept. Mgr._Organics & Pharma Synthesis Lab ( ~10 people), duties: R&D project manager & lab mgr;

      Training at the 1st South China Advanced Biopharma Seminar (ZSU) held by USA_Sino Biopharma Association

1990.7--1992.9  Guizhou Lianjiang Chemical Plant (Guiyang): business-- chemicals & related production, scale--around 1000 people; duties: major project initiator of two joint-ventures(Sino-HK/Sino-Taiwan), associate engineer___tech/new product development, workshop mgmt, QA/EHS/factory mgmt___housecare & bodycare chemicals.

Skills:
English: CET-6/TOEFL/GRE passed and fluent in both oral and written
PC: Windows, internet, industrial softwares,etc
Know some Cantonese, German, Japanese,Shanghainese and other dialects in China. Fluent in Chinese (Mandarin).Open-minded, gentle, creative, curious, innovative, enduring, self-challenging & multi-interested.
Strong organizational and interpersonal ability, passionate, team-building and fast-learning skills,etc.
Know much MBA/Pharma/Bio/Chem/etc knowledge--McKinsey articles, HBS review, BCG, >10,000 books. Read many business/biopharma magazines--SCMP, Business week, Forbes, Bloomberg, Entrepreneur, Financial times, Fortune, Far east economic review, Economist, HR/people/finance management, Asia Wall Street, Channelnewsasia, International marketing and over >200 local Chinese magazines/newspapers, business/academics databases,etc.
Hobbies:
Music, cultures, languages, travel, reading, curious, innovation and swimming/etc.

Email：xmark1218@yahoo.com.cn


求  职  简  历 

廖明祥，男，未婚，1969年12月18日出生于贵州省江口县，现户口所在地：广州市天河区.

教育经历：
1983.09-1986.07  贵州省江口县一中高中部
1986.09-1990.07  四川大学化学系本科有机班，获理学士学位
1991.09-1995.07  中山大学化学化工学院，获有机化学专业硕士学历（有机/药物合成方向），先后任化学化工学院研究生会学术部长，校化学协会副主席等职，并获校级最高奖学金——曾宪梓奖、校优秀研究生称号及广东省南粤优秀研究生称号暨曾宪梓奖学金。硕士论文：甲壳素的降解、衍生合成及其应用研究；指导老师：王植材教授（林永成教授、郑其惶博士）。

工作经历：
1990.07-1992.09  分配至贵州涟江化工厂(久联发展下属企业)，先后任五车间技术员、技术科、开发办助理工程师等职。
1995.07-1997.12  分配至广州医药工业研究所(广州医药集团下属)，任合成药第二研究室工程师、代主任等职，负责或参与或立项多个国家二至四类新药研发项目如肌肉松驰药派库溴铵、抗抑郁药吗氯西胺、抗生素美洛培南等，精细化学品——新型增白剂、防晒剂Parsol 1789、Parsol MCX及防晒霜等，并组织实施市科委重点项目（国家科技部医药卫生重中之重课题领域）——超临界流体萃取、手性反应和酶工程技术应用于手性制药工业的研究。协助召开中国药学会第三届药物化学（1996广州）学术年会，参加过首届华南生物医药高级研讨班等。
1998.01-  中国化工新材料总公司广州分公司(广州汇普新材料前身), 负责Dow polyglycols聚醚类、抗菌剂等市场销售及技术服务;
1998-1999（瑞士独资）广东汽巴精化(现为亨斯迈纺织染化(中国)有限公司), 负责纺织化学品生产安全EHS工艺质量化学师, 汇报于瑞士总部技术总监及驻厂外派来的德国籍生产经理;
2000-2001（民营）深圳太太药业(健康元医药前身), 汇报于留美获四硕士学位归国的研究所张所长, 从事新药研究开发试制等;
2001-2002  广州中嘉科技开发有限公司, 负责化学分析仪器-HPLC/LC-MS等的代理市场销售及技术服务, 广交会从事多位外商翻译及商务助理;
2002-2003（德资）上海维格拉印刷器材有限公司, 负责印刷材料(光油/润版液等)的生产技术质量及市场服务等, 汇报于德国籍家族总裁;
2003-2004  无锡药明康德新药开发有限公司上海中心, 从事新药先导化合物合成及定制化学品放大试制,  汇报于留日归国博士化学部总监及留美归国博士总裁;
                 广东省造纸研究所,  从事造纸化学品(增强剂、脱墨剂等)生产研发建厂等, 汇报于造纸助剂室主任、总工及所长;
                上海网环信息科技有限公司, 从事医药行业ERP+cGxP软件市场调研、架构设计、管理咨询及市场推广销售服务等, 汇报于留美归国IT精英总裁;
                (意大利主资) 北京柯莱斯皮合成革有限公司, 从事合成革生产运营技术及国际国内市场销售等, 汇报于总部外派的意大利籍总经理;
                重庆西南合成制药股份有限公司(方正科技并购重组), 从事生产、质量及新品研发、中试管理等, 汇报于副总经理及总工、研究所所长;
2004-2005  康龙化成（北京）新药技术公司, 从事新药先导化合物合成及定制化学品放大试制,  汇报于留美博士总裁及总经理;
2005-2006  贵州大学生命科学学院, 负责新药分子设计、制药工艺学及药用高分子材料等课程设计教学及有关科研项目调研(含化药、生物医药、民族药、中药现代化等), 汇报于常务副院长及制药工程系主任等;
2006-2007  美中能特医药化学科技(北京)有限公司及北京百威昂医药技术开发有限公司,  从事新药研究开发中式放大、原料药精细化工品进出口业务等, 汇报于双留美博士总裁及中方总经理。

发表论文：
1、用HNMR法研究1.3-二酮== 烯醇式互变异构体系及其溶剂效应，四川大学学报（自然版）1992.3期
2、甲壳素的降解衍生合成，中国海洋药物杂志，1997.1期
3、大环内酯的合成，中国化学会有机合成学术研讨会论文集（1994）
4、甲壳素的研究进展，中山大学研究生学刊（自然版）1993
5、海洋大环内酯化合物的生物合成初探，广州地区第三届化学化工研究生学术研讨会论文集（1994）
6、氨基葡萄糖衍生化合成，欧亚化学大会（广州1996）论文集

技能：
语言上普通话流利，英语过CET-6级口语流畅，熟通SCI/BA/CA/EI/Medline/FDA等文献库及有关专业软件，通德、日语，懂粤沪川语等方言；研读中外文史哲/政经/法管/国际关系/外交/军事/理工等逾万册；历经大半个中国；熟练运用电脑Windows办公软件及互联网。喜欢读书，旅游，音乐，新奇，文化，游泳等。

电邮：xmark1218@yahoo.com.cn
我现在广东, 请求电Guangzhou # 132-65982847 or Beijing # 137-18134069 找我(25/07/2007止)。不便之处,敬请谅解!
廖明祥



