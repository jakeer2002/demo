// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.ConvertRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * *
 * Convert data test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ConvertTests {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Autowired
    IUtilityService utilityService;

    /**
     * *
     * API context
     */
    public ApiContext apiContext;

    /**
     * *
     * API
     */
    public Api api;

    /**
     * *
     * Lens settings
     */
    public LensSettings en_usLensSettings, en_ukLensSettings, localeSettings;

    /**
     * *
     * Resources
     */
    public Resources resource;

    /**
     * *
     * Client
     */
    public Client client;

    /**
     * *
     * Client
     */
    public CoreClient coreClient;

    /**
     * *
     * Test helper object
     */
    public TestHelper testHelper;

    /**
     * *
     * Helper object
     */
    public Helper _helper;

    /**
     * *
     * Extension
     */
    private String extension;

    /**
     * *
     * Binary data
     */
    private String binaryData;

    /**
     * *
     * Instance type
     */
    public static String InstanceType = "XRay";

    /**
     * *
     * Locale type US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * *
     * Local type UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * *
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * *
     * Mock HTTP servlet request
     */
    private MockHttpServletRequest request;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     *
     * @throws JAXBException
     */
    @Before
    public void setUp() throws JAXBException {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        try {
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            InstanceType = testHelper.instanceType;
            // create api
            CreateApiRequest createApi = testHelper.getCreateApiRequest("/input/CreateApi.xml");
            //CreateApiRequest createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(ConvertTests.class.getResourceAsStream("/input/CreateApi.xml"));
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettingsForJob.xml");

            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ConvertTests.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;

            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // create resource
            AddResourceRequest resourceRequest = testHelper.getAddResourceRequest("/input/CreateResource.xml");
            //AddResourceRequest resourceRequest = (AddResourceRequest) jaxbUnmarshaller.unmarshal(ConvertTests.class.getResourceAsStream("/input/CreateResource.xml"));
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            //CreateConsumerRequest consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(ConvertTests.class.getResourceAsStream("/input/CreateConsumer.xml"));

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();

            settingsList.getInstanceList().clear();

            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_usLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_ukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);

            consumer.setLensSettings(settingsList);

            consumer = testHelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;

            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);

            // en_us LensSettings
            CoreLenssettings en_usCoreLenssettings = new CoreLenssettings();
            en_usCoreLenssettings.setId(1);
            en_usCoreLenssettings.setHost(testHelper.hostname);
            en_usCoreLenssettings.setPort(testHelper.port);
            en_usCoreLenssettings.setCharacterSet(testHelper.encoding);
            en_usCoreLenssettings.setTimeout(testHelper.timeOut);
            en_usCoreLenssettings.setInstanceType(testHelper.instanceType);
            en_usCoreLenssettings.setLocale(testHelper.locale);
            en_usCoreLenssettings.setStatus(testHelper.status);

            // en_uk LensSettings
            CoreLenssettings en_ukCoreLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            en_ukCoreLenssettings.setId(2);
            en_ukCoreLenssettings.setLocale(LocaleTypeEn_uk);

            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();

            en_usCoreClientlenssettings.setCoreLenssettings(en_usCoreLenssettings);
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(en_ukCoreLenssettings);
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);

            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);

            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            //coreClientapis.add(clientapis.get(0));
            //coreClient.setCoreClientapis(coreClientapis);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("POST");
            apiContext.setResource("Converter");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = ConvertTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete", deleteApiResponse.status, true);
            // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete", deleteConsumerResponse.status, true);
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ConvertResumeWithValidBinaryResumeData
     */
    @Test
    public void convertResumeWithValidBinaryResumeData() {
        try {
            // ConvertResume
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);
            convertRequest.setLocale(TestHelper.Locale.en_us.name());
            convertRequest.setExtension(extension);
            ApiResponse response = utilityService.convertBinaryData(convertRequest);
            Assert.assertEquals("ConvertResume With ValidBinary ResumeData check", response.status, true);

            // ConvertResume Assert
            String expResult = "EDRISH HABIBULLAH";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());

            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ConvertResume Transaction Count Assert
            Assert.assertEquals("ConvertResume With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * convertWithValidBinaryResumeData
     */
    @Test
    public void convertWithValidBinaryResumeDataWithDefaultLocale() {
        try {
            // Canon
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry(TestHelper.Locale.en_us.name());
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);

            ApiResponse response = utilityService.convertBinaryData(convertRequest);
            Assert.assertEquals("Convert With ValidBinary ResumeData check", response.status, true);

            // Canon Assert
            String expResult = "EDRISH HABIBULLAH";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());

            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // Canon Transaction Count Assert
            Assert.assertEquals("Convert With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ConvertResumeWithConvertError
     */
    @Test
    public void convertResumeWithConvertError() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            InputStream resourceAsStream04 = ConvertTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ConvertResume
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);
            convertRequest.setLocale(TestHelper.Locale.en_us.name());
            convertRequest.setExtension(extension);
            ApiResponse response = utilityService.convertBinaryData(convertRequest);
            Assert.assertEquals("ConvertResume With Empty BinaryData check", response.status, false);

            // ConvertResume Assert           
            String convertResume = (String) response.responseData;

            String expResult = "Converters returned empty output";
            Assert.assertEquals("Lens TagBinaryData With convert error Check", apiErrors.getLensErrorMessage(expResult), convertResume);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());

            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ConvertResume Transaction Count Assert
            Assert.assertEquals("ConvertResume With Empty BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ConvertResumeWithConvertErrorStatus
     */
    @Test
    public void convertResumeWithConvertErrorStatus() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ConvertResume
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);
            convertRequest.setLocale(TestHelper.Locale.en_us.name());
            convertRequest.setExtension(extension);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.convertBinaryData(convertRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ConvertResume With Null BinaryData check", response.status, false);

            // ConvertResume Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());

            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ConvertResume Transaction Count Assert
            Assert.assertEquals("ConvertResume With Null BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ConvertResumeSpecificLensInstance
     */
    @Test
    public void convertResumeSpecificLensInstance() {
        try {
            // ConvertResume
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);
            convertRequest.setExtension(extension);
            convertRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.convertBinaryData(convertRequest);
            Assert.assertEquals("ConvertResume Specific Lens Instance check", response.status, true);

            // ConvertResume Assert
            String expResult = "EDRISH HABIBULLAH";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());

            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ConvertResume Transaction Count Assert
            Assert.assertEquals("ConvertResume With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ConvertResumeUnauthorizedInstance
     */
    @Test
    public void convertResumeUnauthorizedInstance() {
        try {
            // ConvertResume
            String localeType = "en_anz";
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);
            convertRequest.setLocale(TestHelper.Locale.en_us.name());
            convertRequest.setExtension(extension);
            convertRequest.setLocale(localeType);
            ApiResponse response = utilityService.convertBinaryData(convertRequest);
            Assert.assertEquals("ConvertResume Unauthorized Instance check", response.status, false);

            // ConvertResume en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ConvertResumeEmptyInstanceList
     */
    @Test
    public void convertResumeEmptyInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ConvertResume
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);
            convertRequest.setLocale(TestHelper.Locale.en_us.name());
            convertRequest.setExtension(extension);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.convertBinaryData(convertRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ConvertResume Empty Instance List check", response.status, false);

            // ConvertResume en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * convertResumeNullInstanceList
     */
    @Test
    public void convertResumeNullInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ConvertResume
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);
            convertRequest.setLocale(TestHelper.Locale.en_us.name());
            convertRequest.setExtension(extension);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.convertBinaryData(convertRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ConvertResume Null Instance List check", response.status, false);

            // ConvertResume en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * convertResumeInvalidLensList
     */
    @Test
    public void convertResumeInvalidLensList() {
        try {
            //Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ConvertResume
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);
            convertRequest.setExtension(extension);
            convertRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.convertBinaryData(convertRequest);
            Assert.assertEquals("ConvertResume Lens Invalid  check", response.status, false);

            // ConvertResume en_us Assert
            String instanceResponse = (String) response.responseData;
            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));

            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ConvertResumeInActiveLensList
     */
    @Test
    public void convertResumeInActiveLensList() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ConvertResume
            ConvertRequest convertRequest = new ConvertRequest();
            convertRequest.setInstanceType(InstanceType);
            convertRequest.setBinaryData(binaryData);
            convertRequest.setExtension(extension);
            convertRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.convertBinaryData(convertRequest);
            Assert.assertEquals("ConvertResume InActive Lens check", response.status, false);
            
            // ConvertResume inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            boolean inActiveInstance = instanceResponse.contains(_helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS));
            assertTrue(inActiveInstance);
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);

            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
