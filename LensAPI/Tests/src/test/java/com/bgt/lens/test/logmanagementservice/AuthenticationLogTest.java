// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.logmanagementservice;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.authentication.OAuthHeader;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreAuthenticationlog;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.logger.ILoggerService;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Authentication log test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class AuthenticationLogTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    

    @Autowired
    ICoreService coreService;

    @Autowired
    ILoggerService loggerService;

    @Autowired
    IDataRepositoryService dbService;

    /**
     * Client
     */
    public CoreClient client = null;

    /**
     * Client API
     */
    public CoreClientapi clientApi = null;

    /**
     * API
     */
    public CoreApi api = null;

    /**
     * API context
     */
    public ApiContext apiContext = null;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
           servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        client = new CoreClient();
        client.setId(1);

        clientApi = new CoreClientapi();
        api = new CoreApi();
        api.setId(1);
        clientApi.setCoreApi(api);

        apiContext = new ApiContext();

        OAuthHeader header = new OAuthHeader();
        header.setSignature("TEST");
        header.setTimeStamp((long) 123456);
        header.setNonce("TEST");

        apiContext.setClient(client);
        apiContext.setRequestId(UUID.randomUUID());
        apiContext.setClientApi(clientApi);
        apiContext.setoAuthHeader(header);
        apiContext.setAdvanceLog(new AdvanceLog());
        _helper = new Helper();
        _helper.setApiContext(apiContext);
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * addAuthenticationLog
     */
    @Test
    public void addAuthenticationLog() {
        try {
            loggerService.addAuthenticationLog(apiContext);

            // Get
            CoreAuthenticationlog getCoreAuthenticationlog = dbService.getAuthenticationlogByRequestId(apiContext.getRequestId().toString());
            // Get Assert
            Assert.assertNotEquals("Get authentication log check", getCoreAuthenticationlog, null);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteAuthenticationlog(getCoreAuthenticationlog);
            // Delete Assert
            Assert.assertEquals("Delete authentication log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addAuthenticationLogWithInvalidClientId
     */
    @Test
    public void addAuthenticationLogWithInvalidClientId() {
        try {
            // Add
            client.setId(-1);
             api.setId(1);
            clientApi.setCoreApi(api);
            apiContext.setClientApi(clientApi);
            apiContext.setClient(client);

            ApiResponse addApiResponse = new ApiResponse();
            try {
//                addApiResponse = 
                loggerService.addAuthenticationLog(apiContext);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("Add authentication log Invalid ClientId check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addAuthenticationLogWithInvalidApiId
     */
    @Test
    public void addAuthenticationLogWithInvalidApiId() {
        try {
            // Add
            client.setId(1);
            api.setId(-1);
            clientApi.setCoreApi(api);

            ApiResponse addApiResponse = new ApiResponse();
            try {
                loggerService.addAuthenticationLog(apiContext);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("Add authentication log Invalid ClientId check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addAuthenticationLog
     */
    @Test
    public void addAuthenticationLogWithLongSignatureExceptException() {
        try {

            // Long Signature
            OAuthHeader longheader = new OAuthHeader();
            // 104 Length
            longheader.setSignature("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
            longheader.setTimeStamp((long) 123456);
            longheader.setNonce("TEST");
            apiContext.setoAuthHeader(longheader);
            _helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            // Add 
            try {
                loggerService.addAuthenticationLog(apiContext);
            } catch (Exception ex) {
                apiResponse.status = false;
                apiResponse.responseData = ex.getMessage();
            }
            
            Assert.assertEquals("Add AuthenticationLog Except Exception", apiResponse.status, false);
            Assert.assertEquals("Add AuthenticationLog Except Exception", apiResponse.responseData, _helper.getErrorMessageWithURL(ApiErrors.ADD_AUTHENTICATIONLOG_ERROR));

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }
}
