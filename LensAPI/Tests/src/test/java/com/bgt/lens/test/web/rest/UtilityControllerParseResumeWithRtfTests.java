// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.ParseResumeWithRTFRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.test.UtilityServices.ParseResumeWithRtfTests;
import com.bgt.lens.web.rest.UriConstants;
import com.bgt.lens.web.rest.UtilityController;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

/**
 * Utility controller ParseResume With RTF test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class UtilityControllerParseResumeWithRtfTests {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Autowired
    IUtilityService utilityService;

    /**
     * *
     * API context
     */
    public ApiContext apiContext;

    /**
     * *
     * API
     */
    public Api api;

    /**
     * *
     * Lens settings
     */
    public LensSettings en_usLensSettings, en_ukLensSettings, localeSettings;

    /**
     * *
     * Resource
     */
    public Resources resource;

    /**
     * *
     * Client
     */
    public Client client;

    /**
     * *
     * Client
     */
    public CoreClient coreClient;

    /**
     * *
     * Test helper object
     */
    public TestHelper testHelper;

    /**
     * *
     * Helper object
     */
    public Helper _helper;

    /**
     * *
     * Extension
     */
    private String extension;

    /**
     * *
     * Binary data
     */
    private String binaryData;

    /**
     * *
     * Resume parsing variant
     */
    public static final String variant = "rtf";

    /**
     * *
     * Instance type
     */
    public static final String InstanceType = "SPECTRUM";

    /**
     * *
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * *
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * *
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final UtilityController utilityController = new UtilityController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     *
     * @throws JAXBException
     */
    @Before
    public void setUp() throws JAXBException {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(utilityController).build();
            ReflectionTestUtils
                    .setField(utilityController,
                            "utilityService",
                            utilityService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            // create api
            CreateApiRequest createApi = testHelper.getCreateApiRequest("/input/CreateApi.xml");
            //CreateApiRequest createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(ParseResumeWithHtmTests.class.getResourceAsStream("/input/CreateApi.xml"));
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ParseResumeWithHtmTests.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;

            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // create resource
            AddResourceRequest resourceRequest = testHelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper.getCreateConsumerRequest("/input/CreateConsumer.xml");

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();

            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_usLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_ukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);

            consumer.setLensSettings(settingsList);

            consumer = testHelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;

            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
           
            // en_us LensSettings
            CoreLenssettings en_usCoreLenssettings = new CoreLenssettings();
            en_usCoreLenssettings.setId(1);
            en_usCoreLenssettings.setHost(testHelper.hostname);
            en_usCoreLenssettings.setPort(testHelper.port);
            en_usCoreLenssettings.setCharacterSet(testHelper.encoding);
            en_usCoreLenssettings.setTimeout(testHelper.timeOut);
            en_usCoreLenssettings.setInstanceType(testHelper.instanceType);
            en_usCoreLenssettings.setLocale(testHelper.locale);
            en_usCoreLenssettings.setStatus(testHelper.status);

            // en_uk LensSettings
            CoreLenssettings en_ukCoreLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            en_ukCoreLenssettings.setId(2);
            en_ukCoreLenssettings.setLocale(LocaleTypeEn_uk);

            // en_us LensSettings
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);

            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(en_usCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(en_ukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);        
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);

            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("Post");
            apiContext.setResource("Resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = ParseResumeWithRtfTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete", deleteApiResponse.status, true);
            // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete", deleteConsumerResponse.status, true);
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);
            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithRtfWithValidBinaryResumeData() {
        try {
            // ParseResumeWithRtf
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);
            parseResumeWithRTFRequest.setLocale(TestHelper.Locale.en_us.name());

            String expResult = "ResDoc";
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResult)))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfWithValidBinaryResumeData_ReturnWithRtfCheck
     */
    @Test
    public void parseResumeWithRtfWithValidBinaryResumeData_ReturnWithRtfCheck() {
        try {
            // ParseResumeWithRtf
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeWithRTFRequest.setExtension(extension);

            String expResult = "rtfdoc";
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResult)))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf With ValidBinary TransactionCount ReturnWithRtfCheck", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithRtfValidBinaryResumeDataWithDefaultLocale() {
        try {
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry(TestHelper.Locale.en_us.name());
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);

            String expResult = "rtfdoc";
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResult)))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf ValidBinary TransactionCount ReturnWithRtfCheck", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithRtfValidBinaryResumeDataLocaleBasedParsing() {
        try {
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);

            String expResult = "rtfdoc";
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResult)))
                    .andExpect(jsonPath("$.identifiedLocale",
                                    is("en_us")))
                    .andExpect(jsonPath("$.processedWithLocale",
                                    is("en_us")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf ValidBinary TransactionCount ReturnWithRtfCheck", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithRtfValidBinaryResumeDataLocaleBasedParsingWithDefaultCountry() {
        try {
            String fileName = "/TestResumes/16343.doc";
            InputStream resourceAsStream04 = ParseResumeWithRtfTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);
            String expResult = "rtfdoc";
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResult)))
                    .andExpect(jsonPath("$.identifiedLocale",
                                    is("en_gb")))
                    .andExpect(jsonPath("$.processedWithLocale",
                                    is("en_us")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf ValidBinary TransactionCount ReturnWithRtfCheck", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithRtfValidBinaryResumeDataLocaleBasedParsingWithUnknownLocale() {
        try {
            String fileName = "/TestResumes/UnknownLocale.doc";
            InputStream resourceAsStream04 = ParseResumeWithRtfTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);

            String expResult = "rtfdoc";
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResult)))
                    .andExpect(jsonPath("$.identifiedLocale",
                                    is("xl_xx")))
                    .andExpect(jsonPath("$.processedWithLocale",
                                    is("en_us")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf ValidBinary TransactionCount ReturnWithRtfCheck", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithRtfValidBinaryResumeDataLocaleBasedParsingWithUnallocatedLocale() {
        try {
            String fileName = "/TestResumes/Resume_JPN.txt";
            InputStream resourceAsStream04 = ParseResumeWithRtfTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);

            String expResult = "rtfdoc";
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResult)))
                    .andExpect(jsonPath("$.identifiedLocale",
                                    is("ja_jp")))
                    .andExpect(jsonPath("$.processedWithLocale",
                                    is("en_us")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf ValidBinary TransactionCount ReturnWithRtfCheck", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithRtfValidBinaryResumeDataLocaleBasedParsingWithInactiveInstance() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);

            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());
            coreClientApiContext.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClientApiContext.setEnableLocaleDetection(true);
            apiContext.setClient(coreClientApiContext);
            _helper.setApiContext(apiContext);

            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);

            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print());
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            ApiResponse apiResponse = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", apiResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfWithConvertError
     */
    @Test
    public void parseResumeWithRtfWithConvertError() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            InputStream resourceAsStream04 = ParseResumeWithRtfTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeWithRtfTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeWithRtf
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeWithRTFRequest.setExtension(extension);

            String expResult = "Converters returned empty output";
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(apiErrors.getLensErrorMessage(expResult))))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf With Empty BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfWithConvertErrorStatus
     */
    @Test
    public void parseResumeWithRtfWithConvertErrorStatus() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeWithRtf
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeWithRTFRequest.setExtension(extension);
            ApiResponse response = new ApiResponse();
            try {
                mockMvc.perform(post(UriConstants.ResumeParserRTF)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper
                                .convertObjectToJsonBytes(parseResumeWithRTFRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isBadRequest())
                        .andDo(print());
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResumeWithRtf With Null BinaryData check", response.status, false);

            // ParseResumeWithRtf Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf With Null BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfSpecificLensInstance
     */
    @Test
    public void parseResumeWithRtfSpecificLensInstance() {
        try {
            // ParseResumeWithRtf
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);
            parseResumeWithRTFRequest.setLocale(LocaleTypeEn_us);

            String expResult = "rtfdoc";
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResult)))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeWithRtf Transaction Count Assert
            Assert.assertEquals("ParseResumeWithRtf With Specific Lens Instance TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfUnauthorizedInstance
     */
    @Test
    public void parseResumeWithRtfUnauthorizedInstance() {
        try {
            // ParseResumeWithRtf
            String localeType = "en_anz";
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);
            parseResumeWithRTFRequest.setLocale(localeType);

            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE))))
                    .andDo(print());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfEmptyInstanceList
     */
    @Test
    public void parseResumeWithRtfEmptyInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeWithRtf
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeWithRTFRequest.setExtension(extension);
            try{
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE))))
                    .andDo(print());
            }
            catch(NestedServletException ex){
                Assert.assertTrue(true);
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfNullInstanceList
     */
    @Test
    public void parseResumeWithRtfNullInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeWithRtf
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeWithRTFRequest.setExtension(extension);
            try{
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE))))
                    .andDo(print());
            }
            catch(NestedServletException ex){
                Assert.assertTrue(true);
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfInvalidLensList
     */
    @Test
    public void parseResumeWithRtfInvalidLensList() {
        try {
            //Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeWithRtf
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);
            parseResumeWithRTFRequest.setLocale(LocaleTypeEn_us);
            LensRepository repository = new LensRepository();
            String errorMessage = repository.lensErrorMessage;
            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE))))
                    .andDo(print());
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            ApiResponse apiResponse = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", apiResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithRtfInActiveLensList
     */
    @Test
    public void parseResumeWithRtfInActiveLensList() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeWithRtf
            ParseResumeWithRTFRequest parseResumeWithRTFRequest = new ParseResumeWithRTFRequest();
            parseResumeWithRTFRequest.setInstanceType(InstanceType);
            parseResumeWithRTFRequest.setBinaryData(binaryData);
            parseResumeWithRTFRequest.setExtension(extension);
            parseResumeWithRTFRequest.setLocale(LocaleTypeEn_us);

            mockMvc.perform(post(UriConstants.ResumeParserRTF)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeWithRTFRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS))))
                    .andDo(print());
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            ApiResponse apiResponse = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", apiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
