// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;


import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest.DefaultLocaleList;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.test.Helper.TestHelper.Locale;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Tag binary data test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryLDTagBinaryDataDataTest {
    
    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private Helper helper = new Helper();


    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;
    
    private MockHttpServletRequest request;

    /**
     * Set of LensSettings
     */
    private List<CoreLenssettings> lenssettingses;

    /**
     * US,UK,LD Set of LensSettings
     */
    private CoreLenssettings uslensSettings, uklensSettings, ldlensSettings;

    /**
     * Helper object
     */
    private TestHelper testHelper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private byte[] binaryData;

    /**
     * Doc type
     */
    private Character docType;

    /**
     * Default Locale
     */
    private String defaultLocale;

    /**
     * *
     * Helper object
     */
    private static final Helper _helper = new Helper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        
        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lenssettingses = new ArrayList<>(0);
        // US Instances
        uslensSettings = new CoreLenssettings();
        uslensSettings.setHost(testHelper.hostname);
        uslensSettings.setPort(testHelper.port);
        uslensSettings.setCharacterSet(testHelper.encoding);
        uslensSettings.setTimeout(testHelper.timeOut);
        uslensSettings.setInstanceType(testHelper.instanceType);
        uslensSettings.setLocale(Locale.en_us.name());
        uslensSettings.setStatus(testHelper.status);

        lenssettingses.add(uslensSettings);

        // UK Instances
        uklensSettings = new CoreLenssettings();
        uklensSettings.setHost(testHelper.hostname);
        uklensSettings.setPort(testHelper.port);
        uklensSettings.setCharacterSet(testHelper.encoding);
        uklensSettings.setTimeout(testHelper.timeOut);
        uklensSettings.setInstanceType(testHelper.instanceType);
        uklensSettings.setLocale(Locale.en_gb.name());
        uklensSettings.setStatus(testHelper.status);

        lenssettingses.add(uklensSettings);

        // Locale Detector Instances
        ldlensSettings = new CoreLenssettings();
        ldlensSettings.setHost(testHelper.ld_hostname);
        ldlensSettings.setPort(testHelper.ld_port);
        ldlensSettings.setCharacterSet(testHelper.ld_encoding);
        ldlensSettings.setTimeout(testHelper.timeOut);
        ldlensSettings.setInstanceType(Locale.LD.name());
        ldlensSettings.setLocale("NA");
        ldlensSettings.setStatus(testHelper.status);

        lenssettingses.add(ldlensSettings);

        DefaultLocaleList defaultLocaleList = new DefaultLocaleList();
        List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
        com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
        locale.setLanguage("en");
        locale.setCountry("us");
        localeList.add(locale);
        locale = new com.bgt.lens.model.adminservice.request.Locale();
        locale.setLanguage("*");
        locale.setCountry("en_us");
        localeList.add(locale);
        
        request = new MockHttpServletRequest();
        ApiContext apiContext = new ApiContext();
        apiContext.setResource("resume");
        apiContext.setInstanceType(testHelper.instanceType);
        request.setAttribute("ApiContext", apiContext);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        defaultLocaleList.getLocale().addAll(localeList);

        defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);

        String fileName = "/TestResumes/100008.doc";

        InputStream resourceAsStream = LensRepositoryLDTagBinaryDataDataTest.class.getResourceAsStream(fileName);
        try {
            if (resourceAsStream != null) {
                binaryData = testHelper.getBinaryData(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException ex) {
            resourceAsStream.close();
            throw ex;
        }
        extension = fileName.substring(fileName.indexOf('.') + 1);
        docType = 'R';
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataTestWithValidData() {
        try {
            // TagBinaryData            
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataTestWithValidData_LanguageBasedParsing() {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_*");
            lenssettingses.add(updateUSLensSettings);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataTestWithValidData_DefaultLocale() {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("*_*");
            lenssettingses.add(updateUSLensSettings);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataTestWithValidData_UnknownLocale() {
        try {
            String fileName = "/TestResumes/chn_resume.txt";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            String errorMessage = "Lens instance details are missing";
            boolean checkContains = lensResponse.responseData.toString().contains(errorMessage);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithInvalidHostName
     */
    @Test
    public void ldTagBinaryDataTestWithInvalidHostName() {
        try {
            // TagBinaryData
            uslensSettings.setHost("Invalid");

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);

            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Invalid Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithEmptyHostName
     */
    @Test
    public void ldtagBinaryDataTestWithEmptyHostName() {
        try {
            // TagBinaryData
            uslensSettings.setHost("");

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);

            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Empty Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithNullHostName
     */
    @Test
    public void ldtagBinaryDataTestWithNullHostName() {
        try {
            // TagBinaryData
            uslensSettings.setHost(null);

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);

            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Null Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithInvalidHostPort
     */
    @Test
    public void ldtagBinaryDataTestWithInvalidHostPort() {
        try {
            // TagBinaryData
            uslensSettings.setPort(0000);

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);

            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Invalid HostPort Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithInValidEncoding
     */
    @Test
    public void ldtagBinaryDataTestWithInValidEncoding() {
        try {
            // TagBinaryData
            uslensSettings.setCharacterSet("Invalid");

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);

            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With InValid Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithEmptyEncoding
     */
    @Test
    public void ldtagBinaryDataTestWithEmptyEncoding() {
        try {
            // TagBinaryData
            uslensSettings.setCharacterSet("");

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);

            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithInvalidTimeOut
     */
    @Test
    public void ldtagBinaryDataTestWithInvalidTimeOut() {
        try {
            // TagBinaryData
            uslensSettings.setTimeout(-10);

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);

            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithConvertError
     */
    @Test
    public void ldtagBinaryDataTestWithConvertError() {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_ConverterError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            String expResult = "error tagging: tag text not generated";
            
           Assert.assertEquals("Lens TagBinaryData With convert error Check", apiErrors.getLensErrorMessage(expResult), lensResponse.responseData); 

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithTaggingError
     */
    @Test
    public void ldtagBinaryDataTestWithTaggingError() {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_TaggingError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            String expResult = "Lens instance details are missing";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithLargeResumeFile
     */
    @Test
    public void ldtagBinaryDataTestWithLargeResumeFile() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);

            // English - Germany Instances
            CoreLenssettings delensSettings = new CoreLenssettings();
            delensSettings.setHost(testHelper.hostname);
            delensSettings.setPort(testHelper.port);
            delensSettings.setCharacterSet(testHelper.encoding);
            delensSettings.setTimeout(testHelper.timeOut);
            delensSettings.setInstanceType(testHelper.instanceType);
            delensSettings.setLocale(Locale.en_de.name());
            delensSettings.setStatus(testHelper.status);
            lenssettingses.add(delensSettings);

            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage =  (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithNullBytes
     */
    @Test
    public void ldtagBinaryDataTestWithNullBytes() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithUnicodeResume
     */
    @Test
    public void ldtagBinaryDataTestWithUnicodeResume() {
        try {
            String fileName = "/TestResumes/UnicodeResume/BEL_FRA_10370.tag.xsl.xml";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            // French - XXX Instances
            CoreLenssettings frlensSettings = new CoreLenssettings();
            frlensSettings.setHost(testHelper.hostname);
            frlensSettings.setPort(testHelper.port);
            frlensSettings.setCharacterSet(testHelper.encoding);
            frlensSettings.setTimeout(testHelper.timeOut);
            frlensSettings.setInstanceType(testHelper.instanceType);
            frlensSettings.setLocale("fr_*");
            frlensSettings.setStatus(testHelper.status);

            lenssettingses.add(frlensSettings);

            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResultUnicode = "túuýž„”—ácdéeínórŠTÚUÝŽ„”—ÁCDÉEÍNÓRôœùûüÿ«»—àâçéèê"
                    + "ëïîÔŒÙÛÜŸ«»—ÀÂÇÉÈÊËÏÎäöüßÄÖÜ—àèéìòóù«»—ÀÈÉÌÒÓÙ«»—„”—acelnós"
                    + "zz„”—ACELNÓSZZõóôúü—ãáâàçéêíÕÓÔÚÜ—ÃÁÂÀÇÉÊÍaâîst„”—«»AÂÎST„”—"
                    + "«»áéíñóúü¿¡—ÁÉÍÑÓÚÜ¿¡—äåàéö–»ÄÅÀÉÖ–»çgiIösü—ÇGIIÖSÜ—";
            String expResult = "ResDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithNullExtension
     */
    @Test
    public void ldtagBinaryDataTestWithNullExtension() {
        try {
            extension = null;
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithEmptyExtension
     */
    @Test
    public void ldtagBinaryDataTestWithEmptyExtension() {
        try {
            extension = "";
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithInvalidExtension
     */
    @Test
    public void ldtagBinaryDataTestWithInvalidExtension() {
        try {
            extension = "Invalid";
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

}
