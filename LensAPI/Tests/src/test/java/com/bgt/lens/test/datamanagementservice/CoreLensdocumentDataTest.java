// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.datamanagementservice;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreLensdocument;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.time.Instant;
import java.util.Date;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Lens document data test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CoreLensdocumentDataTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    @Autowired
    IDataRepositoryService dbService;

    /**
     * Lens document
     */
    private CoreLensdocument coreLensdocument = null;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        coreLensdocument = new CoreLensdocument();
        coreLensdocument.setClientId(10138);
        coreLensdocument.setApiId(25491);
        coreLensdocument.setMethod("POST");
        coreLensdocument.setResource("lens");
        coreLensdocument.setType(true);
        coreLensdocument.setOriginalFilename("Sample.txt");
        coreLensdocument.setMimeType("plain text");
        coreLensdocument.setByteSize(200);
        coreLensdocument.setFileExtension("doc");
        coreLensdocument.setBinaryContent("VGhpcyBpcyBTYW1wbGUgUmVzdW1lIHRleHQ=");
        coreLensdocument.setCreatedOn(Date.from(Instant.now()));
        coreLensdocument.setUpdatedOn(Date.from(Instant.now()));
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * addLensdocumentWithValidData_DB
     */
    @Test
    public void addLensdocumentWithValidData_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensdocument(coreLensdocument);
            // Add Assert
            Assert.assertEquals("DB: Add Lensdocument status check", addApiResponse.status, true);

            // Get
            CoreLensdocument getCoreLensdocument = dbService.getLensdocumentById(coreLensdocument.getId());
            // Get Assert
            Assert.assertNotNull("DB: Get Lensdocument status check", getCoreLensdocument);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensdocument(coreLensdocument);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lensdocument status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }

    }
}
