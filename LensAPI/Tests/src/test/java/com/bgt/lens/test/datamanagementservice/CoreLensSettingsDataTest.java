// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.datamanagementservice;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.LensSettingsCriteria;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Lens settings data test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CoreLensSettingsDataTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    @Autowired
    IDataRepositoryService dbService;

    Set<CoreCustomskillsettings> coreCustomskillsettings = new HashSet<>();

    /**
     * Lens settings
     */
    private CoreLenssettings coreLenssettings;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {

    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        coreLenssettings = new CoreLenssettings();
        coreLenssettings.setHost("localhost");
        coreLenssettings.setPort(2000);
        coreLenssettings.setCharacterSet("utf-8");
        coreLenssettings.setTimeout(6000);
        coreLenssettings.setLocale("en_uk");
        coreLenssettings.setLensVersion("5.3.34");
        coreLenssettings.setInstanceType("TM");
        coreLenssettings.setStatus(true);
        coreLenssettings.setCreatedOn(Date.from(Instant.now()));
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * AddInstanceWithValidData_DB
     */
    @Test
    public void addInstanceWithValidData_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * AddInstanceWithoutInstanceType_DB
     */
    @Test
    public void addInstanceWithoutInstanceType_DB() {
        try {
            // Add
            coreLenssettings.setInstanceType(null);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createLensSettings(coreLenssettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Without InstanceType check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * AddInstanceWithoutHostName_DB
     */
    @Test
    public void addInstanceWithoutHostName_DB() {
        try {
            // Add
            coreLenssettings.setHost(null);

            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createLensSettings(coreLenssettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Without HostName Check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * AddInstanceInvalidPort_DB
     */
    @Test
    public void addInstanceInvalidPort_DB() {
        try {
            // Add
            coreLenssettings.setPort(-10);

            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createLensSettings(coreLenssettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Invalid Port Check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * AddInstanceWithoutCharSet_DB
     */
    @Test
    public void addInstanceWithoutCharSet_DB() {
        try {
            // Add
            coreLenssettings.setCharacterSet(null);

            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createLensSettings(coreLenssettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Without Character Set Check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * AddInstanceInvalidTimeout_DB
     */
    @Test
    public void addInstanceInvalidTimeout_DB() {
        try {
            // Add
            coreLenssettings.setTimeout(-10);

            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createLensSettings(coreLenssettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Invalid Timeout Check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * AddInstanceWithoutLocale_DB
     */
    @Test
    public void addInstanceWithoutLocale_DB() {
        try {
            // Add
            coreLenssettings.setLocale(null);

            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createLensSettings(coreLenssettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Without Locale Check", addApiResponse.status, false);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * UpdateInstanceWithValidData_DB
     */
    @Test
    public void updateInstanceWithValidData_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Update
            String updateHost = "localhost1";
            int updatePort = 2001;
            String updateCharacterSet = "Iso-8";
            int updateTimeOut = 6001;
            String updateLocale = "eng_us";
            String updateLensVersion = "v6.0";
            String updateInstanceType = "JM";
            boolean updateStatus = false;
            Date updatecreatedDate = Date.from(Instant.now());

            coreLenssettings.setHost(updateHost);
            coreLenssettings.setPort(updatePort);
            coreLenssettings.setCharacterSet(updateCharacterSet);
            coreLenssettings.setTimeout(updateTimeOut);
            coreLenssettings.setLocale(updateLocale);
            coreLenssettings.setLensVersion(updateLensVersion);
            coreLenssettings.setInstanceType(updateInstanceType);
            coreLenssettings.setStatus(updateStatus);
            coreLenssettings.setCreatedOn(updatecreatedDate);

            // Update Assert
            ApiResponse updateApiResponse = dbService.updateLensSettings(coreLenssettings, coreCustomskillsettings);

            Assert.assertEquals("DB: Update Lens Settings Check", updateApiResponse.status, true);

            // Get
            coreLenssettings = dbService.getLensSettingsById(coreLenssettings.getId());
            // Get Assert
            Assert.assertEquals("DB: Update Lens Settings HostName Check", updateHost, coreLenssettings.getHost());
            Assert.assertEquals("DB: Update Lens Settings port Check", updatePort, coreLenssettings.getPort());
            Assert.assertEquals("DB: Update Lens Settings Character Set Check", updateCharacterSet, coreLenssettings.getCharacterSet());
            Assert.assertEquals("DB: Update Lens Settings Timeout Check", updateTimeOut, coreLenssettings.getTimeout());
            Assert.assertEquals("DB: Update Lens Settings Locale Check", updateLocale, coreLenssettings.getLocale());
            Assert.assertEquals("DB: Update Lens Settings Version Check", updateLensVersion, coreLenssettings.getLensVersion());
            Assert.assertEquals("DB: Update Lens Settings Instance Check", updateInstanceType, coreLenssettings.getInstanceType());
            Assert.assertEquals("DB: Update Lens Settings Status Check", updateStatus, coreLenssettings.isStatus());
            //Assert.assertEquals("DB: Update Lens Settings Creation date Check", updatecreatedDate, coreLenssettings.getCreatedOn());

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * UpdateInstanceWithInvalidInstanceId_DB
     */
    @Test
    public void updateInstanceWithInvalidInstanceId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);
            // Update
            CoreLenssettings invalidCoreLenssettings = new CoreLenssettings();
            invalidCoreLenssettings.setId(0);

            ApiResponse updateApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.updateLensSettings(invalidCoreLenssettings,coreCustomskillsettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Update Assert
            Assert.assertEquals("DB: Update Lens Settings with Invalid Instance Id Check", updateApiResponse.status, false);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * UpdateInstanceWithoutInstanceType_DB
     */
    @Test
    public void updateInstanceWithoutInstanceType_DB() {

        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Update
            coreLenssettings.setInstanceType(null);

            ApiResponse updateApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.updateLensSettings(coreLenssettings,coreCustomskillsettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Update Assert
            Assert.assertEquals("DB: Update Lens Settings Without InstanceType check", updateApiResponse.status, false);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * UpdateInstanceWithoutHostName_DB
     */
    @Test
    public void updateInstanceWithoutHostName_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Update
            coreLenssettings.setHost(null);

            ApiResponse updateApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.updateLensSettings(coreLenssettings,coreCustomskillsettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Update Assert
            Assert.assertEquals("DB: Update Lens Settings Without hostname check", updateApiResponse.status, false);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * UpdateInstanceInvalidPort_DB
     */
    @Test
    public void updateInstanceInvalidPort_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Update
            coreLenssettings.setPort(-10);

            ApiResponse updateApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.updateLensSettings(coreLenssettings,coreCustomskillsettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Update Assert
            Assert.assertEquals("DB: Update Lens Settings Invalid Port Check", updateApiResponse.status, false);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * UpdateInstanceWithoutCharSet_DB
     */
    @Test
    public void updateInstanceWithoutCharSet_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Update
            coreLenssettings.setCharacterSet(null);

            ApiResponse updateApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.updateLensSettings(coreLenssettings,coreCustomskillsettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Update Assert
            Assert.assertEquals("DB: Update Lens Settings without character set Check", updateApiResponse.status, false);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * UpdateInstanceInvalidTimeout_DB
     */
    @Test
    public void updateInstanceInvalidTimeout_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Update
            coreLenssettings.setPort(-10);

            ApiResponse updateApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.updateLensSettings(coreLenssettings,coreCustomskillsettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Update Assert
            Assert.assertEquals("DB: Update Lens Settings Invalid Timeout Check", updateApiResponse.status, false);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * UpdateInstanceWithoutLocale_DB
     */
    @Test
    public void updateInstanceWithoutLocale_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Update
            coreLenssettings.setLocale(null);

            ApiResponse updateApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.updateLensSettings(coreLenssettings,coreCustomskillsettings);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Update Assert
            Assert.assertEquals("DB: Update Lens Settings without Locale Check", updateApiResponse.status, false);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * DeleteInstanceWtihValidSettingsId_DB
     */
    @Test
    public void deleteInstanceWtihValidSettingsId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * DeleteInstanceWithInvalidSettingsId_DB
     */
    @Test
    public void deleteInstanceWithInvalidSettingsId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Delete Invalid
            CoreLenssettings invalidCoreLenssettings = new CoreLenssettings();
            invalidCoreLenssettings.setId(0);
            ApiResponse deleteInvalidApiResponse = new ApiResponse();
            try {
                deleteInvalidApiResponse = dbService.deleteLensSettings(invalidCoreLenssettings);
                deleteInvalidApiResponse.status = true;
            } catch (Exception ex) {
                deleteInvalidApiResponse.status = false;
            }
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteInvalidApiResponse.status, false);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * getListofAllApi_DB
     */
    @Test
    public void getListofAllApi_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();

            List<CoreLenssettings> coreLenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
            Assert.assertTrue("DB: Get All Lens Settings", coreLenssettingses.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetInstancebyIdWithValidInstanceId_DB
     */
    @Test
    public void getInstancebyIdWithValidInstanceId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // Get
            CoreLenssettings getCoreLenssettings = dbService.getLensSettingsById(coreLenssettings.getId());
            // Get Assert
            Assert.assertNotNull("DB: Get Lens Settings Check", getCoreLenssettings);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetInstancebyIdWithInvalidInstanceId_DB
     */
    @Test
    public void getInstancebyIdWithInvalidInstanceId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createLensSettings(coreLenssettings);

            // Add Assert
            Assert.assertEquals("DB: Add Lens Settings Check", addApiResponse.status, true);

            // GetById
            CoreLenssettings invalidCoreLenssettings = dbService.getLensSettingsById(0);
            // GetById Assert
            Assert.assertNull("DB : Get Lens Settings By Invalid Id status check", invalidCoreLenssettings);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteLensSettings(coreLenssettings);
            // Delete Assert
            Assert.assertEquals("DB: Delete Lens Settings Check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
