// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Tag binary data with RTF test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryLDTagBinaryDataWithRTFDataTest {
    
     @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;
    
    
    private ApiErrors apiErrors = new ApiErrors();   


    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Set of LensSettings
     */
    private List<CoreLenssettings> lenssettingses;
    /**
     * Set Mock http request
     */
    private MockHttpServletRequest request;

    /**
     * US,UK,LD Set of LensSettings
     */
    private CoreLenssettings uslensSettings, uklensSettings, ldlensSettings;

    /**
     * Helper object
     */
    private TestHelper testHelper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private byte[] binaryData;

    /**
     * Document type
     */
    private Character docType;

    /**
     * Default Locale
     */
    private String defaultLocale;

    /**
     * *
     * Helper object
     */
    private static final Helper _helper = new Helper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        
        
        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lenssettingses = new ArrayList<>(0);
        // US Instances
        uslensSettings = new CoreLenssettings();
        uslensSettings.setHost(testHelper.hostname);
        uslensSettings.setPort(testHelper.port);
        uslensSettings.setCharacterSet(testHelper.encoding);
        uslensSettings.setTimeout(testHelper.timeOut);
        uslensSettings.setInstanceType(testHelper.instanceType);
        uslensSettings.setLocale(TestHelper.Locale.en_us.name());
        uslensSettings.setStatus(testHelper.status);

        lenssettingses.add(uslensSettings);

        // UK Instances
        uklensSettings = new CoreLenssettings();
        uklensSettings.setHost(testHelper.hostname);
        uklensSettings.setPort(testHelper.port);
        uklensSettings.setCharacterSet(testHelper.encoding);
        uklensSettings.setTimeout(testHelper.timeOut);
        uklensSettings.setInstanceType(testHelper.instanceType);
        uklensSettings.setLocale(TestHelper.Locale.en_gb.name());
        uklensSettings.setStatus(testHelper.status);

        lenssettingses.add(uklensSettings);

        // Locale Detector Instances
        ldlensSettings = new CoreLenssettings();
        ldlensSettings.setHost(testHelper.ld_hostname);
        ldlensSettings.setPort(testHelper.ld_port);
        ldlensSettings.setCharacterSet(testHelper.ld_encoding);
        ldlensSettings.setTimeout(testHelper.timeOut);
        ldlensSettings.setInstanceType(TestHelper.Locale.LD.name());
        ldlensSettings.setLocale("NA");
        ldlensSettings.setStatus(testHelper.status);

        lenssettingses.add(ldlensSettings);

        CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
        List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
        com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
        locale.setLanguage("en");
        locale.setCountry("us");
        localeList.add(locale);
        locale = new com.bgt.lens.model.adminservice.request.Locale();
        locale.setLanguage("*");
        locale.setCountry("en_us");
        localeList.add(locale);
        
        request = new MockHttpServletRequest();
        ApiContext apiContext = new ApiContext();
        apiContext.setResource("resume");
        apiContext.setInstanceType(testHelper.instanceType);
        request.setAttribute("ApiContext", apiContext);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        defaultLocaleList.getLocale().addAll(localeList);

        defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);

        String fileName = "/TestResumes/100008.doc";
        InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
        try {
            if (resourceAsStream != null) {
                binaryData = testHelper.getBinaryData(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException ex) {
            resourceAsStream.close();
            throw ex;
        }
        //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
        extension = fileName.substring(fileName.indexOf('.') + 1);
        docType = 'R';
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * ldTagBinaryDataWithRTFTestWithVaildData
     */
    @Test
    public void ldtagBinaryDataWithRTFTestWithVaildData() {
        try {
            // TagBinaryDataWithRTF
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataTestWithValidData_LanguageBasedParsing() {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_*");
            lenssettingses.add(updateUSLensSettings);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "rtfdoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataTestWithValidData_DefaultLocale() {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("*_*");
            lenssettingses.add(updateUSLensSettings);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "rtfdoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataTestWithValidData_UnknownLocale() {
        try {
            String fileName = "/TestResumes/chn_resume.txt";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            String errorMessage = "Lens instance details are missing";
            boolean checkContains = ((String)lensResponse.responseData).contains(errorMessage);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithVaildData_withRTFTag
     */
    @Test
    public void tagBinaryDataWithRTFTestWithVaildData_withRTFTag() {
        try {
            // TagBinaryDataWithRTF
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, true);
            String expResult = "rtfdoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithInvalidHostName
     */
    @Test
    public void tagBinaryDataWithRTFTestWithInvalidHostName() {
        try {
            // TagBinaryDataWithRTF
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setHost("Invalid");
            lenssettingses.add(updateUSLensSettings);

            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Invalid Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithEmptyHostName
     */
    @Test
    public void tagBinaryDataWithRTFTestWithEmptyHostName() {
        try {
            // TagBinaryDataWithRTF
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setHost("");
            lenssettingses.add(updateUSLensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Empty Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithNullHostName
     */
    @Test
    public void tagBinaryDataWithRTFTestWithNullHostName() {
        try {
            // TagBinaryDataWithRTF
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setHost(null);
            lenssettingses.add(updateUSLensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Null Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithInvalidHostPort
     */
    @Test
    public void tagBinaryDataWithRTFTestWithInvalidHostPort() {
        try {
            // TagBinaryDataWithRTF
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setPort(0000);
            lenssettingses.add(updateUSLensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Invalid HostPort Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithInValidEncoding
     */
    @Test
    public void tagBinaryDataWithRTFTestWithInValidEncoding() {
        try {
            // TagBinaryDataWithRTF
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setCharacterSet("Invalid");
            lenssettingses.add(updateUSLensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithRTF With InValid Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithEmptyEncoding
     */
    @Test
    public void tagBinaryDataWithRTFTestWithEmptyEncoding() {
        try {
            // TagBinaryDataWithRTF
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setCharacterSet("");
            lenssettingses.add(updateUSLensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithNullEncoding
     */
    @Test
    public void tagBinaryDataWithRTFTestWithNullEncoding() {
        try {
            // TagBinaryDataWithRTF
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setCharacterSet(null);
            lenssettingses.add(updateUSLensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithInvalidTimeOut
     */
    @Test
    public void tagBinaryDataWithRTFTestWithInvalidTimeOut() {
        try {
            // TagBinaryDataWithRTF
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setTimeout(-10);
            lenssettingses.add(updateUSLensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithRTF With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithConvertError
     */
    @Test
    public void tagBinaryDataWithRTFTestWithConvertError() throws Exception {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_ConverterError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataWithRTF
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, false);
             String expResult = "(locale:2) error tagging: tag text not generated";
           Assert.assertEquals("Lens TagBinaryDataHRXML With convert error Check", apiErrors.getLensErrorMessage(expResult), lensResponse.responseData); 

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithTaggingError
     */
    @Test
    public void tagBinaryDataWithRTFTestWithTaggingError() throws Exception {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("*_*");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_TaggingError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataWithRTF
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, false);
            String expResult = "error";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithLargeResumeFile
     */
    @Test
    public void tagBinaryDataWithRTFTestWithLargeResumeFile() throws Exception {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_*");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataWithRTF
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithNullBytes
     */
    @Test
    public void tagBinaryDataWithRTFTestWithNullBytes() throws Exception {
        lenssettingses.remove(uslensSettings);
        CoreLenssettings updateUSLensSettings = uslensSettings;
        updateUSLensSettings.setLocale("*_*");
        lenssettingses.add(updateUSLensSettings);
        String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
        binaryData = null;
        extension = fileName.substring(fileName.indexOf('.') + 1);
        ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
        Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, false);
        Assert.assertEquals("Lens TagBinaryDataWithRTF With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);
    }

    /**
     * TagBinaryDataWithRTFTestWithUnicodeResume
     */
    @Test
    public void tagBinaryDataWithRTFTestWithUnicodeResume() throws Exception {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("fr_*");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/UnicodeResume/BEL_FRA_10370.tag.xsl.xml";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataWithRTF
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, true);
            String expResultUnicode = "túuýž„”—ácdéeínórŠTÚUÝŽ„”—ÁCDÉEÍNÓRôœùûüÿ«»—àâçéèê"
                    + "ëïîÔŒÙÛÜŸ«»—ÀÂÇÉÈÊËÏÎäöüßÄÖÜ—àèéìòóù«»—ÀÈÉÌÒÓÙ«»—„”—acelnós"
                    + "zz„”—ACELNÓSZZõóôúü—ãáâàçéêíÕÓÔÚÜ—ÃÁÂÀÇÉÊÍaâîst„”—«»AÂÎST„”—"
                    + "«»áéíñóúü¿¡—ÁÉÍÑÓÚÜ¿¡—äåàéö–»ÄÅÀÉÖ–»çgiIösü—ÇGIIÖSÜ—";
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithNullExtension
     */
    @Test
    public void tagBinaryDataWithRTFTestWithNullExtension() throws Exception {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_*");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = null;
            // TagBinaryDataWithRTF
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithEmptyExtension
     */
    @Test
    public void tagBinaryDataWithRTFTestWithEmptyExtension() throws Exception {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_*");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "";
            // TagBinaryDataWithRTF
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithRTFTestWithInvalidExtension
     */
    @Test
    public void tagBinaryDataWithRTFTestWithInvalidExtension() throws Exception {
        try {
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_*");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "Invalid";
            // TagBinaryDataWithRTF
            ApiResponse lensResponse = lensRepository.ldTagBinaryDataWithRTF(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataWithRTF Assert
            Assert.assertEquals("Lens TagBinaryDataWithRTF Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }
}
