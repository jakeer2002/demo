// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Enum.distanceUnits;
import com.bgt.lens.helpers.Enum.keywordContext;
import com.bgt.lens.helpers.Enum.keywordSearchTypes;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.SearchResult;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.AdditionalFilters;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.Keywords;
import com.bgt.lens.model.searchservice.request.ResumeSearchRequest;
import com.bgt.lens.model.searchservice.request.SearchCriteria;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Geography;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Job;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Keyword;
import com.bgt.lens.model.searchservice.request.SearchCriteria.YearsOfExperience;
import com.bgt.lens.model.soap.response.searchservice.ResumeSearchResponse;
import com.bgt.lens.repositories.ILensRepository;
import com.bgt.lens.repositories.impl.LensCommandBuilder;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.w3c.dom.NodeList;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.context.WebApplicationContext;

/**
 * Search Resume Test - AI Search (Job Vs Resume)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class JobVsResumeSearchTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;

    /**
     * Interface LensRespository service.
     */
    @Autowired
    private ILensRepository lensRepository;

    /**
     * Lens Command Builder
     */
    @Autowired
    private LensCommandBuilder lensCommandBuilder;

    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * Core Lens Settings Details
     */
    private CoreLenssettings enuscoreLenssettings;
    /**
     * vendor
     */
    private Vendor adminVendor;
    /**
     * External client vendor
     */
    private Vendor externalClientVendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Admin client
     */
    private CoreClient adminClient;
    /**
     * ExternalClient
     */
    private CoreClient externalClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Job PlainText.
     */
    private String jobBinaryData;
    /**
     * Job PlainText ByteArray.
     */
    private byte[] jobPlainTextByteArray;
    /**
     * Extension
     */
    private String extension;
    /**
     * Doc type
     */
    private Character docType;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.standaloneSetup(searchController).build();
            ReflectionTestUtils.setField(searchController, "searchService", searchService);

            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);

            CreateApiRequest createApi = testHelper.getCreateApiRequest("/input/CreateApi.xml");
            createApi.setKey("Search");
            createApi.setName("Search Api");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            ApiResponse enusLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", enusLensSettingsResponse.status, true);
            enusLensSettings = (LensSettings) enusLensSettingsResponse.responseData;

            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", enukLensSettingsResponse.status, true);
            enukLensSettings = (LensSettings) enukLensSettingsResponse.responseData;

            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", enukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            AddResourceRequest resourceRequest = testHelper.getAddResourceRequest("/input/CreateResource.xml");
            resourceRequest.setApiKey(api.getKey());
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            
            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            
            enuscoreLenssettings = enusCoreLenssettings;
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings = (CoreLenssettings) SerializationUtils.clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);            

            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            adminVendor = (Vendor) response.responseData;

            createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            externalClientVendor = (Vendor) response.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper.getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
			
            consumer.setLensSettings(settingsList);

            CreateConsumerRequest.VendorSettings settings = new CreateConsumerRequest.VendorSettings();
            settings.getVendorIdList().add(BigInteger.valueOf(adminVendor.getVendorId()));
            consumer.setVendorSettings(settings);

            consumer = testHelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
//            consumer.getVendorSettings().getVendorIdList().clear();
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);

            adminClient = dbService.getClientById(client.getId());
            adminClient.setTier(1);

            SearchSettings searchSettings = new SearchSettings();
            searchSettings.getResume().clear();
            SearchSettings.Resume resume = new SearchSettings.Resume();
            resume.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            resume.setVendorId(BigInteger.valueOf(adminVendor.getVendorId()));
            searchSettings.getResume().add(resume);

            String searchSettingsJson = helper.getJsonStringFromObject(searchSettings);

            CoreClient updatedAdminClient = adminClient;
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings searchVendorSettings = dbService.getVendorById(adminVendor.getVendorId());
            searchVendorSettingsSet.add(searchVendorSettings);
            updatedAdminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            updatedAdminClient.setSearchSettings(searchSettingsJson);
            ApiResponse updateClientResponse = dbService.updateClient(adminClient, updatedAdminClient);
            Assert.assertTrue("Update client check", updateClientResponse.status);

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            fileName = "/TestJobs/10001.txt";
            resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    jobBinaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            extension = fileName.substring(fileName.indexOf('.') + 1);
            docType = 'P';

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete", deleteApiResponse.status, true);
             // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete", deleteConsumerResponse.status, true);

            ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if (clientList != null && clientList.size() > 0) {
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister resume request", deleteExternalClientResponse.status);
            }
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService.deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService.deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings", localeDeleteSettingsResponse.status, true);
           

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchResume By JobDescription
     */
    @Test
    public final void searchResumeByJobDescription() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(jobBinaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                String postingXML = tagPostingAndExtractPostingXML(enuscoreLenssettings, helper.decodeBase64String(jobBinaryData), extension, docType, "");
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\">" + postingXML + "<include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResume By JobDescription With PostalCode
     */
    @Test
    public final void searchResumeByJobDescriptionWithPostalCode() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(jobBinaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                String xmlHeader = "<?xml version='1.0' encoding='" + testHelper.encoding + "'?>";
                String postingXML = tagPostingAndExtractPostingXML(enuscoreLenssettings, helper.decodeBase64String(jobBinaryData), extension, docType, "");
                postingXML = lensCommandBuilder.updatePostingXml(xmlHeader, postingXML, resumeSearchRequest.getSearchCriteria());
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\">" + postingXML + "<include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResume By JobDescription With GenericKeyword Search
     */
    @Test
    public final void searchResumeByJobDescriptionWithGenericKeywordSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(jobBinaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                String xmlHeader = "<?xml version='1.0' encoding='" + testHelper.encoding + "'?>";
                String postingXML = tagPostingAndExtractPostingXML(enuscoreLenssettings, helper.decodeBase64String(jobBinaryData), extension, docType, "");
                //postingXML = lensCommandBuilder.updatePostingXml(xmlHeader, postingXML, resumeSearchRequest);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job[pos='1']:(\"CWTS\")) and (resume/experience/job[pos='1']:(\"Apar Infotech\") or resume/experience/job[pos='2']:(\"Apar Infotech\")) ]]></keyword>" + postingXML + "<include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResume By JobDescription With CKSSearch
     */
/*    @Test
    public final void searchResumeByJobDescriptionWithCKSSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(jobBinaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(keywordSearchTypes.CKS.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                String xmlHeader = "<?xml version='1.0' encoding='" + testHelper.encoding + "'?>";
                String postingXML = tagPostingAndExtractPostingXML(enuscoreLenssettings, helper.decodeBase64String(jobBinaryData), extension, docType, "");
                //postingXML = lensCommandBuilder.updatePostingXml(xmlHeader, postingXML, resumeSearchRequest);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[CKS((\"CWTS\") and (\"Apar Infotech\") )]]></keyword>" + postingXML + "<include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }  */

    /**
     * searchResume By JobDescription With Custom Boolean Search
     */
    @Test
    public final void searchResumeByJobDescriptionWithCustomBooleanSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(jobBinaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))");
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                String xmlHeader = "<?xml version='1.0' encoding='" + testHelper.encoding + "'?>";
                String postingXML = tagPostingAndExtractPostingXML(enuscoreLenssettings, helper.decodeBase64String(jobBinaryData), extension, docType, "");
                //postingXML = lensCommandBuilder.updatePostingXml(xmlHeader, postingXML, resumeSearchRequest);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\">" + postingXML + "<keyword><![CDATA[(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResumeByJobDescriptionWithDistanceFilterSearch
     */
    @Test
    public final void searchResumeByJobDescriptionWithDistanceFilterSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(jobBinaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                String xmlHeader = "<?xml version='1.0' encoding='" + testHelper.encoding + "'?>";
                String postingXML = tagPostingAndExtractPostingXML(enuscoreLenssettings, helper.decodeBase64String(jobBinaryData), extension, docType, "");
                postingXML = lensCommandBuilder.updatePostingXml(xmlHeader, postingXML, resumeSearchRequest.getSearchCriteria());
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"10\" units=\"miles\"/>" + postingXML + "<include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResume By JobDescription With GenericKeyword And Distance
     * BasedSearch
     */
    @Test
    public final void searchResumeByJobDescriptionWithGenericKeywordAndDistanceBasedSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(jobBinaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                String xmlHeader = "<?xml version='1.0' encoding='" + testHelper.encoding + "'?>";
                String postingXML = tagPostingAndExtractPostingXML(enuscoreLenssettings, helper.decodeBase64String(jobBinaryData), extension, docType, "");
                postingXML = lensCommandBuilder.updatePostingXml(xmlHeader, postingXML, resumeSearchRequest.getSearchCriteria());
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"10\" units=\"miles\"/><keyword><![CDATA[(resume/experience/job[pos='1']:(\"CWTS\")) and (resume/experience/job[pos='1']:(\"Apar Infotech\") or resume/experience/job[pos='2']:(\"Apar Infotech\")) ]]></keyword>" + postingXML + "<include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchResume By JobDescription With TaggingError
     */
    @Test
    public final void searchResumeByJobDescriptionWithTaggingError() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            String errorFileName = "/TestJobs/InvalidDocuments/ExternalJob_TaggingError.docx";
            InputStream resourceAsStream = RegisterResumeTest.class.getResourceAsStream(errorFileName);
            try {
                if (resourceAsStream != null) {
                    jobBinaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream));
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(jobBinaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
                assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.PARSER_ERROR_SEARCH_JOBDESCRIPTION_BINARYDATA)));
            }

            Assert.assertFalse("JobDescription With TaggingError check", apiResponse.status);

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount-2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchResume By JobDescription With Empty JobDescription Criteria
     */
    @Test
    public final void searchResumeByJobDescriptionWithEmptyJobDescriptionCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
                assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.INVALID_SEARCHCRITERIA)));
            }

            Assert.assertFalse("Invalid Search Type check", apiResponse.status);

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount -2 , (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResume By YearsOfExperience Criteria With JobDescription Criteria
     */
    @Test
    public final void searchResumeByYearsOfExperienceCriteriaWithJobDescriptionCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(jobBinaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
            yearsOfExperienceCriteria.setMin((long) (19));
            yearsOfExperienceCriteria.setMax((long) (22));
            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                String postingXML = tagPostingAndExtractPostingXML(enuscoreLenssettings, helper.decodeBase64String(jobBinaryData), extension, docType, "");
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><yrsexp max=\"22\" min=\"19\"/>" + postingXML + "<include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchResume By JobId
     */
    @Test
    public final void searchResumeByJobId() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setJobId("1");
            searchCriteria.setJob(jobDocumentCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><doc id=\"1\" type=\"posting\" vendor=\"resvendor01\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResumeByJobIdWithGenericKeywordSearch
     */
    @Test
    public final void searchResumeByJobIdWithGenericKeywordSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setJobId("1");
            searchCriteria.setJob(jobDocumentCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job[pos='1']:(\"CWTS\")) and (resume/experience/job[pos='1']:(\"Apar Infotech\") or resume/experience/job[pos='2']:(\"Apar Infotech\")) ]]></keyword><doc id=\"1\" type=\"posting\" vendor=\"resvendor01\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchResume By JobId WithCKSSearch
     */
/*    @Test
    public final void searchResumeByJobIdWithCKSSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setJobId("1");
            searchCriteria.setJob(jobDocumentCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(keywordSearchTypes.CKS.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[CKS((\"CWTS\") and (\"Apar Infotech\") )]]></keyword><doc id=\"1\" type=\"posting\" vendor=\"resvendor01\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }  */

    /**
     * searchResumeByJobIdWithCustomBooleanSearch
     */
    @Test
    public final void searchResumeByJobIdWithCustomBooleanSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setJobId("1");
            searchCriteria.setJob(jobDocumentCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))");
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><doc id=\"1\" type=\"posting\" vendor=\"resvendor01\"/><keyword><![CDATA[(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResumeByJobIdWithDistanceFilterSearch
     */
    @Test
    public final void searchResumeByJobIdWithDistanceFilterSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setJobId("1");
            searchCriteria.setJob(jobDocumentCriteria);

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"10\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><doc id=\"1\" type=\"posting\" vendor=\"resvendor01\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResumeByJobIdWithGenericKeywordAndDistanceBasedSearch
     */
    @Test
    public final void searchResumeByJobIdWithGenericKeywordAndDistanceBasedSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setJobId("1");
            searchCriteria.setJob(jobDocumentCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"10\" units=\"miles\"/><keyword><![CDATA[(resume/experience/job[pos='1']:(\"CWTS\")) and (resume/experience/job[pos='1']:(\"Apar Infotech\") or resume/experience/job[pos='2']:(\"Apar Infotech\")) ]]></keyword><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><doc id=\"1\" type=\"posting\" vendor=\"resvendor01\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResumeByInvalidJobId
     */
    @Test
    public final void searchResumeByInvalidJobId() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setJobId("Invalid");
            searchCriteria.setJob(jobDocumentCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                String searchResponseString = mapper.readValue(json, String.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                // Frame the BGT Search command and then compare the with SearchDumpCommand
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" type=\"resume\" vendor=\"resvendor01\"><doc id=\"Invalid\" type=\"posting\" vendor=\"resvendor01\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                assertTrue(searchResponseString.contains("Document not found"));

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * searchResumeByEmptyJobId
     */
    @Test
    public final void searchResumeByEmptyJobId() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setJobId("");
            searchCriteria.setJob(jobDocumentCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
                assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.INVALID_SEARCHCRITERIA)));
            }

            Assert.assertFalse("Resume Search With EmptyResumeId  check", apiResponse.status);

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchResume By JobId Empty Resumes Like This Criteria
     */
    @Test
    public final void searchResumeByJobIdEmptyResumesLikeThisCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
                assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.INVALID_SEARCHCRITERIA)));
            }

            Assert.assertFalse("Resume Search With EmptyResumeId  check", apiResponse.status);

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2,(long) remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tag posting plainText and extract posting xml
     *
     * @param lensSettings
     * @param binaryData
     * @param extension
     * @param docType
     * @return
     * @throws Exception
     */
    public String tagPostingAndExtractPostingXML(CoreLenssettings lensSettings, byte[] binaryData, String extension, Character docType, String customSkillsKey) throws Exception {
        String postingXml = "";
        ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType, customSkillsKey);
        if (lensResponse.status) {
            if (helper.isNotNull(lensResponse) && helper.isNotNull(lensResponse.responseData)) {
                NodeList postingNodeList = testHelper.getNodeListFromXPath((String) lensResponse.responseData, "//JobDoc");
                if (helper.isNotNull(postingNodeList)) {
                    postingXml = testHelper.getOuterXml(postingNodeList);
                }
            }
        }
        return postingXml;
    }

}
