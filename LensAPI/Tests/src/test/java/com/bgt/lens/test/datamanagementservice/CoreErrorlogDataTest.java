// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.datamanagementservice;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.ErrorLogCriteria;
import com.bgt.lens.model.entity.CoreErrorlog;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Error log data test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CoreErrorlogDataTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    @Autowired
    IDataRepositoryService dbService;

    /**
     * Error log
     */
    private CoreErrorlog coreErrorlog = null;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        coreErrorlog = new CoreErrorlog();
        coreErrorlog.setRequestId("C23A4938-4D01-4A66-B170-24E426DC9C4D");
        coreErrorlog.setClientId(10138);
        coreErrorlog.setApiId(25491);
        coreErrorlog.setStatusCode(500);
        coreErrorlog.setUserMessage("Internal Server Error");
        coreErrorlog.setException("Internal server error GET against resource Search : Jobs - Details: document not found: couldn't get key");
        coreErrorlog.setTraceLog("[{\"message\":\"ApiHandler: SendAsync: Getting the PerRequestMessageUnitOfWorkScope<ApiUnitOfWork>\",\"exception\":\"\",\"category\":\"ApiInfrastructure\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"ApiHandler: SendAsync: Create an Api context and update it with the relevant request data\",\"exception\":\"\",\"category\":\"ApiInfrastructure\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"ApiHandler: SendAsync: Create an instance to the System Service\",\"exception\":\"\",\"category\":\"ApiInfrastructure\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"ApiHandler: SendAsync: Initialize the usage log entry from the Api context and ensure the InTime is set\",\"exception\":\"\",\"category\":\"ApiInfrastructure\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"ApiHandler: SendAsync: Update the Api context with all the relevant authentication data\",\"exception\":\"\",\"category\":\"ApiInfrastructure\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"ApiHandler: SendAsync: Validate the Api Context\",\"exception\":\"\",\"category\":\"ApiInfrastructure\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking for Replay Attack 1\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking IP Address isn't forbidden\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking Client exists for oAuth ConsumerKey\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking IP Address is allowed\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking API exists for oAuthToken\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking API matches controller\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking Client is allowed to use API\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking Client is licensed\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Generating the oAuth Signature\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking oAuth signatures match\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Checking Replay Attack 2\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Saving the authentication log\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Saving the usage counters\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"CoreService: ValidateApiContext: Saving Changes\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"ApiHandler: SendAsync: Starting Request\",\"exception\":\"\",\"category\":\"ApiInfrastructure\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: Constructor: Getting client settings\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: Constructor: Creating lens repository\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: GetJob\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: JobLookupItems: Add titles\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: JobLookupItems: Add countries\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: JobLookupItems: Add states\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: JobLookupItems: Add counties\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: JobLookupItems: Add currencies\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: JobLookupItems: Add frequencies\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"SearchService: JobLookupItems: Add qualification levels\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"LensExtensions: ExecuteLensCommand: Command: <?xml version=\\\"1.0\\\" encoding=\\\"iso-8859-1\\\"?><bgtcmd><fetch type=\\\"posting\\\" vendor=\\\"careerjobs\\\" id=\\\"132\\\" /></bgtcmd>\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"LensExtensions: CreateSession: Host: 172.16.2.78 Port: 2001 CharacterSet: iso-8859-1\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"LensExtensions: ExecuteLensCommand: Result: <?xml version='1.0' encoding='iso-8859-1'?>\\n<bgtres>\\n<error>document not found: couldn't get key</error>\\n</bgtres>\\n\",\"exception\":\"\",\"category\":\"Engine\",\"kind\":\"Trace\",\"level\":\"Info\"},{\"message\":\"ApiHandler: SendAsync: Logging Usage\",\"exception\":\"\",\"category\":\"ApiInfrastructure\",\"kind\":\"Trace\",\"level\":\"Info\"}]");
        coreErrorlog.setCreatedOn(Date.from(Instant.now()));
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * addErrorlogWithValidData_DB
     */
    @Test
    public void addErrorlogWithValidData_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            CoreErrorlog getCoreErrorlog = dbService.getErrorlogById(coreErrorlog.getRequestId());
            // Get Assert
            Assert.assertNotNull("DB: Get Error Log status check", getCoreErrorlog);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * addErrorlogWithoutClientId_DB
     */
    @Test
    public void addErrorlogWithoutClientId_DB() {
        try {
            // Add
            coreErrorlog.setClientId(null);
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log without ClientId check", addApiResponse.status, true);

            // Get
            CoreErrorlog getCoreErrorlog = dbService.getErrorlogById(coreErrorlog.getRequestId());
            // Get Assert
            Assert.assertNotNull("DB: Get Error Log without ClientId Check", getCoreErrorlog);
            Assert.assertNull("DB: Get Error Log without ClientId Check", getCoreErrorlog.getClientId());

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addErrorlogWithoutApiId_DB
     */
    @Test
    public void addErrorlogWithoutApiId_DB() {
        try {
            // Add
            coreErrorlog.setApiId(null);
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log without ApiId check", addApiResponse.status, true);

            // Get
            CoreErrorlog getCoreErrorlog = dbService.getErrorlogById(coreErrorlog.getRequestId());
            // Get Assert
            Assert.assertNotNull("DB: Get Error Log without ApiId Check", getCoreErrorlog);
            Assert.assertNull("DB: Get Error Log without ApiId Check", getCoreErrorlog.getApiId());

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addErrorlogWithoutRequestID_DB
     */
    @Test
    public void addErrorlogWithoutRequestID_DB() {
        try {
            // Add
            coreErrorlog.setRequestId(null);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createErrorlog(coreErrorlog);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add Error Log without RequestId check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addErrorlogInvalidStatusCode_DB
     */
    @Test
    public void addErrorlogInvalidStatusCode_DB() {
        try {
            // Add
            coreErrorlog.setStatusCode(-1);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createErrorlog(coreErrorlog);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add Error Log Invalid Status Code check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyIdWithValidId_DB
     */
    // ToDo : Update,Delete
    @Test
    public void getErrorlogbyIdWithValidId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            CoreErrorlog getCoreErrorlog = dbService.getErrorlogById(coreErrorlog.getRequestId());
            // Get Assert
            Assert.assertNotNull("DB: Get Error Log status check", getCoreErrorlog);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyIdWithInvalidId_DB
     */
    @Test
    public void getErrorlogbyIdWithInvalidId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            CoreErrorlog getCoreErrorlog = dbService.getErrorlogById("0");
            // Get Assert
            Assert.assertNull("DB: Get Error Log status check", getCoreErrorlog);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyIdWithValidClientId_DB
     */
    @Test
    public void getErrorlogbyIdWithValidClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            CoreErrorlog getCoreErrorlog = dbService.getErrorlogById(coreErrorlog.getRequestId(), coreErrorlog.getClientId());
            // Get Assert
            Assert.assertNotNull("DB: Get Error Log status check", getCoreErrorlog);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyIdWithInvalidClientId_DB
     */
    @Test
    public void getErrorlogbyIdWithInvalidClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            CoreErrorlog getCoreErrorlog = dbService.getErrorlogById(coreErrorlog.getRequestId(), 123);
            // Get Assert
            Assert.assertNull("DB: Get Error Log status check", getCoreErrorlog);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetListofAllErrorlog
     */
    @Test
    public void getListofAllErrorlog() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
            List<CoreErrorlog> coreErrorlogs = dbService.getErrorlogByCriteria(errorLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get Error Log status check", coreErrorlogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyCriteriaWithClientId_DB
     */
    @Test
    public void getErrorlogbyCriteriaWithClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
            errorLogCriteria.setClientId(coreErrorlog.getClientId());
            List<CoreErrorlog> coreErrorlogs = dbService.getErrorlogByCriteria(errorLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get Error Log status check", coreErrorlogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyCriteriaWithInvalidClientId_DB
     */
    @Test
    public void getErrorlogbyCriteriaWithInvalidClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
            errorLogCriteria.setClientId(123);
            List<CoreErrorlog> coreErrorlogs = dbService.getErrorlogByCriteria(errorLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get Error Log status check", coreErrorlogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyCriteriaWithApiId_DB
     */
    @Test
    public void getErrorlogbyCriteriaWithApiId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
            errorLogCriteria.setApiId(coreErrorlog.getApiId());
            List<CoreErrorlog> coreErrorlogs = dbService.getErrorlogByCriteria(errorLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get Error Log status check", coreErrorlogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyCriteriaWithInvalidApiId_DB
     */
    @Test
    public void getErrorlogbyCriteriaWithInvalidApiId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
            errorLogCriteria.setApiId(123);
            List<CoreErrorlog> coreErrorlogs = dbService.getErrorlogByCriteria(errorLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get Error Log status check", coreErrorlogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyCriteriaWithStatusCode_DB
     */
    @Test
    public void getErrorlogbyCriteriaWithStatusCode_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
            errorLogCriteria.setStatusCode(coreErrorlog.getStatusCode());
            List<CoreErrorlog> coreErrorlogs = dbService.getErrorlogByCriteria(errorLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get Error Log status check", coreErrorlogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyCriteriaWithInvalidStatusCode_DB
     */
    @Test
    public void getErrorlogbyCriteriaWithInvalidStatusCode_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
            errorLogCriteria.setStatusCode(123);
            List<CoreErrorlog> coreErrorlogs = dbService.getErrorlogByCriteria(errorLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get Error Log status check", coreErrorlogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyCriteriaWithRequestId_DB
     */
    @Test
    public void getErrorlogbyCriteriaWithRequestId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
            errorLogCriteria.setRequestId(coreErrorlog.getRequestId());
            List<CoreErrorlog> coreErrorlogs = dbService.getErrorlogByCriteria(errorLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get Error Log status check", coreErrorlogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetErrorlogbyCriteriaWithInValidRequestId_DB
     */
    @Test
    public void getErrorlogbyCriteriaWithInValidRequestId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createErrorlog(coreErrorlog);
            // Add Assert
            Assert.assertEquals("DB: Add Error Log status check", addApiResponse.status, true);

            // Get
            ErrorLogCriteria errorLogCriteria = new ErrorLogCriteria();
            errorLogCriteria.setRequestId("Invalid");
            List<CoreErrorlog> coreErrorlogs = dbService.getErrorlogByCriteria(errorLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get Error Log status check", coreErrorlogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Error Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

}
