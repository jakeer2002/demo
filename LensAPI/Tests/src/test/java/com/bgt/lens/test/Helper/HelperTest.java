// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.Helper;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.helpers.Helper;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testng.annotations.BeforeMethod;

/**
 *
 * Helper class tests
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
public class HelperTest {

    private Helper _helper;

    @Mock
    ApiContext apiContext;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    @BeforeMethod
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        _helper = new Helper();
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * HelperTestValidateHostName
     */
    @Test
    public void helperTestValidateHostName() {

        // IP Address
        // Valid IP Address
        Assert.assertTrue("Valid IP Address", _helper.isValidHostName("172.16.2.51"));
        Assert.assertTrue("Valid IP Address", _helper.isValidHostName("0.0.0.0"));

        // Invalid IP Address
        Assert.assertFalse("Valid IP Address", _helper.isValidHostName("259.16.2.51"));
        Assert.assertFalse("Valid IP Address", _helper.isValidHostName("01.102.103.104"));
        Assert.assertFalse("Valid IP Address", _helper.isValidHostName("192.168.0.2000000000"));
        Assert.assertFalse("Valid IP Address", _helper.isValidHostName(":54:sda54"));
        Assert.assertFalse("Valid IP Address", _helper.isValidHostName("01.102.103.104"));

        // Host Name
        // Vaild Host name
        Assert.assertTrue(_helper.isValidHostName("mydomain.com"));
        Assert.assertTrue(_helper.isValidHostName("test.mydomain.com"));
        Assert.assertTrue(_helper.isValidHostName("abc"));
        Assert.assertTrue(_helper.isValidHostName("3abc"));

        // Invalid Host name
        Assert.assertFalse(_helper.isValidHostName("28999"));
        Assert.assertFalse(_helper.isValidHostName("*hi*"));
        Assert.assertFalse(_helper.isValidHostName("-hi-"));
        Assert.assertFalse(_helper.isValidHostName("_domain"));
        Assert.assertFalse(_helper.isValidHostName("28999"));

    }
  

    @Test
    public void isWhiteSpaceTest() {
        Assert.assertFalse(_helper.isNullOrWhitespace("test"));
    }

    // test whether its returning an integer
    @Test
    public void safeLongToIntTest() {
        Integer _returnInteger = _helper.safeLongToInt(-214748364);

        Assert.assertSame(Integer.class, _returnInteger.getClass());
    }

    @Test
    public void stringToBoolTest() {
        Assert.assertTrue(_helper.stringToBool("true"));
    }

    // test whether its returning a date object
    @Test
    public void xmlGregorianCalendarToDateTest() {
        Date date = Date.from(Instant.now());
        XMLGregorianCalendar gregorianCalendar = _helper.toXMLGregorianCalendar(date);
        Date _date = _helper.xmlGregorianCalendarToDate(gregorianCalendar);

        Assert.assertSame(Date.class, _date.getClass());
    }

    @Test
    public void sizeGreaterThanZeroTest() {
        List<Object> list = new ArrayList<>();
        list.add("text");
        Assert.assertTrue(_helper.sizeGreaterThanZero(list));

    }

    @Test
    public void getListOfSoapResourcesTest() {
        String urlContext = "/parserservice/status/info";
        Assert.assertEquals("status", _helper.getResourceName(urlContext, apiContext));
    }

    class SMPLObject {

        public String str;
    }

    @Test
    public void isValidVariantTypeTest() {
        Assert.assertTrue(_helper.isValidVariantType("hrxml"));
    }

    @Test
    public void isPropertyDefinedTest() {
        SMPLObject object = new SMPLObject();
        Assert.assertTrue(_helper.isPropertyDefined(object, "str"));
    }

    @Test
    public void isValidHttpMethodTest() {
        Assert.assertTrue(_helper.isValidHttpMethod("GET"));
    }

    @Test
    public void getTimeDifferenceTest() {
        Date today = Date.from(Instant.now());
        Date tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 24));
        Assert.assertEquals("86400", _helper.getTimeDifference(today, tomorrow));
    }

}
