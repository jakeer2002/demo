// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest.FieldList;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.lang3.StringUtils;
import org.json.XML;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Parse resume without authentication test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ParseResumeWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    IUtilityService utilityService;
    /**
     * API context
     */
    public ApiContext apiContext;

    /**
     * Lens settings
     */
    public LensSettings en_usLensSettings = null, en_ukLensSettings = null, localeSettings = null;

    /**
     * Test helper object
     */
    public TestHelper testHelper = null;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private String binaryData;

    /**
     * Field list
     */
    public FieldList fieldList;

    /**
     * Instance type
     */
    public static String InstanceType = "SPECTRUM";

    /**
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        try {
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            InstanceType = testHelper.instanceType;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ParseResumeWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;

            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("Post");
            apiContext.setResource("Resume");
            apiContext.setClient(null);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Resume Data
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);

            // Fields List
            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("All");
            fieldList.getField().addAll(fields);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    
    @Test
    public void parseResumeWithValidBinaryResumeDataWithLocalewithMicroservice() {
        try {
            // ParseResumeBGTXML
            String fileName = "/TestResumes/BoldResume11.pdf";
            InputStream resourceAsStream04 = ParseResumeBGTXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResumeVariantMicroservice(parseResumeRequest, "");
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary ResumeData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetAllResumeFields
     */
    // All
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetAllResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetAllResumeFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");

            Assert.assertNotNull(resume);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetContactResumeFields
     */
    // Contact
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetContactResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Contact");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetContactResumeFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            Object contact = resume.get("contact");

            Assert.assertNotNull(contact);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithValidBinaryResumeDataWithLocaleBasedParsing() {
        try {
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary ResumeData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithValidBinaryResumeDataLocaleBasedParsingWithInvalidLocale() {
        try {
            // ParseResumeBGTXML
            String fileName = "/TestResumes/Resume_JPN.txt";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary ResumeData check", response.status, false);

            // ParseResumeBGTXML Assert
            String expResult = "missing";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetStatementsResumeFields
     */
    // Statements
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetStatementsResumeFields() {
        try {
            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Statements");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetStatementsResumeFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            Object statements = resume.get("statements");

            Assert.assertNotNull(statements);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetSkillsFields
     */
    // Skills
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetSkillsFields() {
        try {
            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Skills");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetSkillsFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            Object skills = resume.get("skills");

            Assert.assertNotNull(skills);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetProfessionalFields
     */
    // Professional
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetProfessionalFields() {
        try {
            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Professional");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetProfessionalFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            Object professional = resume.get("professional");

            Assert.assertNotNull(professional);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetEducationFields
     */
    // Education
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetEducationFields() {
        try {
            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Education");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetEducationFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            Object education = resume.get("education");

            Assert.assertNotNull(education);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetSummaryFields
     */
    // Summary
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetSummaryFields() {
        try {
            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Summary");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetSummaryFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            Object summary = resume.get("summary");

            Assert.assertNotNull(summary);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetExperienceFields
     */
    // Experience
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetExperienceFields() {
        try {
            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Experience");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetExperienceFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            Object experience = resume.get("experience");

            Assert.assertNotNull(experience);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetReferencesFields
     */
    // References
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetReferencesFields() {
        try {
            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("References");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetReferencesFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            Object references = resume.get("references");

            Assert.assertNotNull(references);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_GetSkillRollUpFields
     */
    // SkillRollUp
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_GetSkillRollUpFields() {
        try {
            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("SkillRollUp");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary GetExperienceFields check", response.status, true);

            // ParseResume Assert
            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            Object skillrollup = jsonRoot.get("skillrollup");

            Assert.assertNotNull(skillrollup);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_WithoutResumeFields
     */
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_WithoutResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary WithoutResumeFields check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            JSONObject contact = (JSONObject) resume.get("contact");

            JSONObject name = (JSONObject) contact.get("name");
            Object givenname = name.get("givenname");
            String gname = givenname.toString();

            String expResult = "EDRISH";
            boolean checkContains = gname.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_WithInvalidResumeFields
     */
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_WithInvalidResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            // Fields List
            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Invalid");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResume(parseResumeRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary WithInvalidResumeFields check", response.status, false);

            // ParseResume Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.INVALID_RESUME_FIELDLIST));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_WithEmptyResumeFields
     */
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_WithEmptyResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            // Fields List
            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResume(parseResumeRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary WithEmptyResumeFields check", response.status, false);

            // ParseResume Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.INVALID_RESUME_FIELDLIST));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithValidBinaryResumeData_WithNullResumeFields
     */
    @Test
    public void parseResumeWithoutAuthenticationWithValidBinaryResumeData_WithNullResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            // Fields List
            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add(null);
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResume(parseResumeRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResume WithoutAuthentication With ValidBinary WithNullResumeFields check", response.status, false);

            // ParseResume Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.INVALID_RESUME_FIELDLIST));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithConvertError
     */
    @Test
    public void parseResumeWithoutAuthenticationWithConvertError() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            InputStream resourceAsStream04 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResume WithoutAuthentication With Empty BinaryData check", response.status, false);

            // ParseResume Assert            
            String convertResume = (String) response.responseData;
            String expResult = "Converters returned empty output";
            Assert.assertEquals("Lens TagBinaryDataWithHTM With convert error Check", apiErrors.getLensErrorMessage(expResult), convertResume);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationWithConvertErrorStatus
     */
    @Test
    public void parseResumeWithoutAuthenticationWithConvertErrorStatus() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResume(parseResumeRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResume WithoutAuthentication With Null BinaryData check", response.status, false);

            // ParseResume Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationSpecificInstance
     */
    @Test
    public void parseResumeWithoutAuthenticationSpecificInstance() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_uk);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResumeBGTXML Without Authentication Specific Instance check", response.status, true);

            // ParseResume Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("ResDoc");
            JSONObject resume = (JSONObject) jsonRoot.get("resume");
            JSONObject contact = (JSONObject) resume.get("contact");

            JSONObject name = (JSONObject) contact.get("name");
            Object givenname = name.get("givenname");
            String gname = givenname.toString();

            String expResult = "EDRISH";
            boolean checkContains = gname.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationUnauthorizedInstance
     */
    @Test
    public void parseResumeWithoutAuthenticationUnauthorizedInstance() {
        try {
            // ParseResume
            String localeType = "en_anz";
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(localeType);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResumeBGTXML Without Authentication Unauthorized Instance check", response.status, false);

            // ParseResume Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationInvalidHostNameList
     */
    @Test
    public void parseResumeWithoutAuthenticationInvalidHostNameList() {
        try {
            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale("en_sgp");
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResumeBGTXML Without Authentication Invalid HostName check", response.status, false);

            // ParseResume en_us Assert
            String instanceResponse = (String) response.responseData;
            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseResumeWithoutAuthenticationInActiveLensList
     */
    @Test
    public void parseResumeWithoutAuthenticationInActiveLensList() {
        try {
            // create lens settings
            JAXBContext jaxbContext = JAXBContext.newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            InputStream resourceAsStream01 = ParseResumeWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(false);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale("en_sgp");
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseResume(parseResumeRequest);
            Assert.assertEquals("ParseResumeBGTXML Without Authentication InActive Lens check", response.status, false);

            // ParseResume inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.INACTIVE_LENS);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
