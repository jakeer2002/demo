// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Enum;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.FilterSettings;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.Job;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.SearchResult;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.AdditionalFilters;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.CustomFilters;
import com.bgt.lens.model.searchservice.request.Keywords;
import com.bgt.lens.model.searchservice.request.JobSearchRequest;
import com.bgt.lens.model.searchservice.request.SearchCriteria;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Geography;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Keyword;
import com.bgt.lens.model.searchservice.request.SearchCriteria.YearsOfExperience;
import com.bgt.lens.model.soap.response.searchservice.JobSearchResponse;
import com.bgt.lens.model.searchservice.request.Facets;
import com.bgt.lens.model.searchservice.request.ResultFilters;
import com.bgt.lens.model.searchservice.request.SelectedFilters;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Search Job Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class SearchJobTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings, spectrumSortSettings;
    /**
     * vendor
     */
    private Vendor adminVendor;
    /**
     * vendor
     */
    private Vendor spectrumSortVendor;
    /**
     * External client vendor
     */
    private Vendor externalClientVendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Admin client
     */
    private CoreClient adminClient;
    /**
     * ExternalClient
     */
    private CoreClient externalClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/JobTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            createApi.setKey("Search");
            createApi.setName("Search Api");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            settingsRequest.setVersion(testHelper.version);
            settingsRequest.setCountry(testHelper.country);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            
            //27-05-2021 adding settings for spectrum sort lens settings
 /*           settingsRequest.setHost(testHelper.SpectrumSort_Hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.SpectrumSort_Port)));
            settingsRequest.setCharSet(testHelper.SpectrumSort_Encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.SpectrumSort_TimeOut)));
            settingsRequest.setInstanceType(testHelper.SpectrumSort_InstanceType);
            settingsRequest.setLocale(testHelper.SpectrumSort_Locale);
            settingsRequest.setStatus(testHelper.SpectrumSort_Status);
            settingsRequest.setCountry(testHelper.SpectrumSort_Country);
            settingsRequest.setVersion(testHelper.SpectrumSort_Version);
            
            ApiResponse spectrumSortLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings Spectrum Sort check",
                    spectrumSortLensSettingsResponse.status, true);
            spectrumSortSettings
                    = (LensSettings) spectrumSortLensSettingsResponse.responseData; */
            
            
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");
            resourceRequest.setApiKey(api.getKey());
            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            
            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            
            // spectrumSortSettings LensSettings
     /*       CoreLenssettings spectrumSortCoreSettings = new CoreLenssettings();
            spectrumSortCoreSettings.setId(4);
            spectrumSortCoreSettings.setHost(testHelper.SpectrumSort_Hostname);
            spectrumSortCoreSettings.setPort(testHelper.SpectrumSort_Port);
            spectrumSortCoreSettings.setCharacterSet(testHelper.SpectrumSort_Encoding);
            spectrumSortCoreSettings.setTimeout(testHelper.SpectrumSort_TimeOut);
            spectrumSortCoreSettings.setInstanceType(testHelper.SpectrumSort_InstanceType);
            spectrumSortCoreSettings.setLocale(testHelper.SpectrumSort_Locale);
            spectrumSortCoreSettings.setStatus(testHelper.SpectrumSort_Status); */
            
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("posvendor01");
            createVendorRequest.setMaxDocumentCount((long) (500));
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            adminVendor = (Vendor) response.responseData;
            
   /*         createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(spectrumSortSettings.getId()));
            createVendorRequest.setVendor("jobquestbgttstp");
            createVendorRequest.setMaxDocumentCount((long) (20000));
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            spectrumSortVendor = (Vendor) response.responseData; */

            createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("posvendor0101");
            createVendorRequest.setMaxDocumentCount((long) (500));
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            externalClientVendor = (Vendor) response.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
          //  com.bgt.lens.model.adminservice.request.InstanceList spectrumSort_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
    /*        spectrumSort_InstanceList.setInstanceId(new BigInteger(Integer.toString(spectrumSortSettings.getId())));
            spectrumSort_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(spectrumSort_InstanceList); */
            
            consumer.setLensSettings(settingsList);

            CreateConsumerRequest.VendorSettings settings = new CreateConsumerRequest.VendorSettings();
        //    settings.getVendorIdList().add(BigInteger.valueOf(spectrumSortVendor.getVendorId()));
            settings.getVendorIdList().add(BigInteger.valueOf(adminVendor.getVendorId()));
            consumer.setVendorSettings(settings);

            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
//            consumer.getVendorSettings().getVendorIdList().clear();
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);

            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
       //     CoreClientlenssettings spectrumSortCoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);
         //   spectrumSortCoreClientlenssettings.setCoreLenssettings(spectrumSortCoreSettings);         
      //      coreClientlenssettingses.add(spectrumSortCoreClientlenssettings);
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("job");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);

            adminClient = dbService
                    .getClientById(client.getId());
            adminClient.setTier(1);

            SearchSettings searchSettings = new SearchSettings();
            searchSettings.getPosting().clear();
            SearchSettings.Posting job = new SearchSettings.Posting();
            job.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            job.setVendorId(BigInteger.valueOf(adminVendor.getVendorId()));
            searchSettings.getPosting().add(job);

            String searchSettingsJson = helper.getJsonStringFromObject(searchSettings);

            CoreClient updatedAdminClient = adminClient;
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings searchVendorSettings = dbService.getVendorById(adminVendor.getVendorId());
            searchVendorSettingsSet.add(searchVendorSettings);
            
      //      searchVendorSettings = dbService.getVendorById(spectrumSortVendor.getVendorId());
        //    searchVendorSettingsSet.add(searchVendorSettings);
            updatedAdminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            updatedAdminClient.setSearchSettings(searchSettingsJson);
            ApiResponse updateClientResponse = dbService.updateClient(adminClient, updatedAdminClient);
            Assert.assertTrue("Update client check", updateClientResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
             // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);

            ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if (clientList != null && clientList.size() > 0) {
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister job request", deleteExternalClientResponse.status);
            }

            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
     /*      ApiResponse spectrumSortDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(spectrumSortSettings.getId());
            Assert.assertEquals("Delete Spectrum Sort Lens settings delete",
                    spectrumSortDeleteLensSettingsResponse.status, true); */
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithSearchTypeAsKeywordGenericKeywordSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[((\"Comcast\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithoutSearchTypeGenericKeywordSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("or");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("CommTech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[((\"Comcast\")) or ((\"CommTech\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithKeywordContextAsFullResume() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("or");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("CommTech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[((\"Comcast\")) or ((\"CommTech\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithKeywordContextAsEmployer() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Google");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.Employer.toString());
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[(DataElementsRollup/CanonEmployer:(\"Google\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                
                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithInvalidKeywordContext() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywords.setContext("test");
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);
            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[((\"Comcast\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithDefaultVendor() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            adminClient = dbService.getClientById(adminClient.getId());
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[((\"Comcast\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithInvalidVendor() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor("test");
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            adminClient = dbService.getClientById(adminClient.getId());
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithoutDefaultVendor() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor("test");
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithoutInstanceType() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Invalid instance type check", apiResponse.status);
            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount -2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithInvalidInstanceType() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType("test");
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Invalid instance type check", apiResponse.status);
            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithoutLocale() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Invalid instance type check", apiResponse.status);
            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithInvalidLocale() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale("test");

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            //Assert.assertFalse("Invalid instance type check", apiResponse.status);
            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
/*    @Test
    public final void searchJobCKSSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CKS.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("or");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("CommTech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[CKS((\"Comcast\") or (\"CommTech\") )]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    } */

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobCustomBooleanSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("(Comcast or CommTech)");

            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[(Comcast or CommTech)]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobKeywordsAndCustomBooleanSearch() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywordCriteria.setCustomBooleanSearchKeyword("(Python or CommTech)");
            searchCriteria.setKeyword(keywordCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.INVALID_SEARCH_KEYWORD_COMBINATION)));
        }
    }    
    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithValidMaximumDocumentCount() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"10\" units=\"miles\"/><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithInvalidMaximumDocumentCount() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) (-1));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Invalid max document count check", apiResponse.status);
// unused dbcall
//            SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
//            dumpCriteria.setClientId(adminClient.getId());
//            List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithValidMinimumScore() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("Comcast or CommTech");
            searchCriteria.setKeyword(keywordCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (5));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"5\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[Comcast or CommTech]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithNegativeMiminumScore() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("Comcast or CommTech");
            searchCriteria.setKeyword(keywordCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (-1));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            // Register Assert
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Minimum LENS score invalid check", apiResponse.status);
            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithInvalidMiminumScore() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("Comcast or CommTech");
            searchCriteria.setKeyword(keywordCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (1005));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            // Register Assert
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Minimum LENS score invalid check", apiResponse.status);
            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithoutCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Search job without criteria", apiResponse.status);

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithValidOffset() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setOffset((long) (2));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"1000\" units=\"miles\"/><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithOffsetGreaterThanSearchResultCount() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setOffset((long) (3));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount -2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithNegativeOffset() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setOffset((long) (-1));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Negative offet count check", apiResponse.status);

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount -2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithDefaultOrder() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"1000\" units=\"miles\"/><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Search results order by check", searchResult.getJobs().get(0).getScore() >= searchResult.getJobs().get(1).getScore());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithDescScore() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortDirection(Enum.sortDirection.DESC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"1000\" units=\"miles\"/><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Search results order by check", searchResult.getJobs().get(0).getScore() <= searchResult.getJobs().get(1).getScore());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobOrderByIdASC() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy(Enum.orderResultsBy.id.toString());
            additionalFilters.setSortDirection(Enum.sortDirection.ASC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"1000\" units=\"miles\"/><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", searchResult.getJobs().get(0).getJobId(), "1");
                Assert.assertEquals("Search results order by check", searchResult.getJobs().get(1).getJobId(), "2");

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Spectrum Sort searchJobSortWithCustomFilterAsc.
     */
    @Test
    public final void searchJobSortWithCustomFilterAsc() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 3);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (5));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("01201");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSort("bgldlanguage|asc|min");
            additionalFilters.setMaximumDocumentCount((long) (2));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"2\" min=\"0\" scoring-mode=\"hard-filters-only\" sortby=\"bgldlanguage|asc|min\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"5\" units=\"miles\"/><resume><contact><address><postalcode>01201</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Spectrum Sort searchJobSortWithInvalidCustomFilter.
     */
    @Test
    public final void searchJobSortWithInvalidCustomFilter() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            
            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("sm_region");
            filter.setName("sm_region");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (5));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("01201");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSort("sm_timeframe|asc|min");
            additionalFilters.setMaximumDocumentCount((long) (2));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();


        } catch (Exception ex) {
            assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.INVALID_SEARCH_SORT_CUSTOMFILTER_REQUEST)));
        }
    }

    /**
     * Spectrum Sort and normal sort searchJobBothSortWithCustomFilterAsc.
     */
    @Test
    public final void searchJobBothSortWithCustomFilterAsc() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            
            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("sm_region");
            filter.setName("sm_region");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (5));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("01201");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSort("sm_region|asc|min");
            additionalFilters.setSortBy(Enum.orderResultsBy.id.toString());
            additionalFilters.setSortDirection(Enum.sortDirection.DESC.toString());
            additionalFilters.setMaximumDocumentCount((long) (2));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.INVALID_SEARCH_SORT_REQUEST)));
        }
    }

    /**
     * Spectrum Sort searchJobSortWithCustomFilterAscXPathScore.
     */
    @Test
    public final void searchJobSortWithCustomFilterAscXPathScore() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 3);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);

            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (5));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("01201");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSort("bgldlanguage|asc|min;xpath://posting/sic|desc;score");
            additionalFilters.setMaximumDocumentCount((long) (2));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"2\" min=\"0\" scoring-mode=\"hard-filters-only\" sortby=\"bgldlanguage|asc|min;xpath://posting/sic|desc;score\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"5\" units=\"miles\"/><resume><contact><address><postalcode>01201</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobOrderByIdDESC() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy(Enum.orderResultsBy.id.toString());
            additionalFilters.setSortDirection(Enum.sortDirection.DESC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"1000\" units=\"miles\"/><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", searchResult.getJobs().get(0).getJobId(), "2");
                Assert.assertEquals("Search results order by check", searchResult.getJobs().get(1).getJobId(), "1");

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobOrderByInvalidCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy("test");
            additionalFilters.setSortDirection("test");
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"1000\" units=\"miles\"/><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", "1", searchResult.getJobs().get(0).getJobId());
                Assert.assertEquals("Search results order by check", "2", searchResult.getJobs().get(1).getJobId());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchJob By YearsOfExperience Criteria Valid Minimum Yrsexp
     */
    @Test
    public final void searchJobByYearsOfExperienceCriteriaValidMinimumYrsexp() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
            yearsOfExperienceCriteria.setMin((long) (20));
            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (5));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"5\" type=\"posting\" vendor=\"posvendor01\"><yrsexp min=\"20\"/><posting/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchJob By YearsOfExperience Criteria Valid MaximumYrsexp
     */
    @Test
    public final void searchJobByYearsOfExperienceCriteriaValidMaximumYrsexp() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
            yearsOfExperienceCriteria.setMax((long) (21));
            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (5));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"5\" type=\"posting\" vendor=\"posvendor01\"><yrsexp max=\"21\"/><posting/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchJob By YearsOfExperience Criteria Valid MinimumYrsexp And
     * MaximumYrsexp
     */
    @Test
    public final void searchJobByYearsOfExperienceCriteriaValidMinimumYrsexpAndMaximumYrsexp() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
            yearsOfExperienceCriteria.setMin((long) (19));
            yearsOfExperienceCriteria.setMax((long) (21));
            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (5));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"5\" type=\"posting\" vendor=\"posvendor01\"><yrsexp max=\"21\" min=\"19\"/><posting/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchJob By YearsOfExperience Criteria Negative MinimumYrsexp
     */
    @Test
    public final void searchJobByYearsOfExperienceCriteriaNegativeMinimumYrsexp() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
            yearsOfExperienceCriteria.setMin((long) (-1));
            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (5));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            // Register Assert
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
                assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.INVALID_MINIMUM_YRSEXP)));
            }

            Assert.assertFalse("Negative MinimumYrsexp check", apiResponse.status);

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchJob By YearsOfExperience Criteria Negative MaximumYrsexp
     */
    @Test
    public final void searchJobByYearsOfExperienceCriteriaNegativeMaximumYrsexp() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
            yearsOfExperienceCriteria.setMax((long) (-1));
            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (5));
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            // Register Assert
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
                assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.INVALID_MAXIMUM_YRSEXP)));
            }

            Assert.assertFalse("Negative MinimumYrsexp check", apiResponse.status);

            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    /**
     * SearchJob By YearsOfExperience Criteria With GenericKeyword Criteria
     */
    @Test
    public final void searchJobByYearsOfExperienceCriteriaWithGenericKeywordCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("or");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("CommTech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);

            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
            yearsOfExperienceCriteria.setMin((long) (1));
            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><yrsexp min=\"1\"/><keyword><![CDATA[((\"Comcast\")) or ((\"CommTech\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * SearchJob By YearsOfExperience Criteria With DistanceBased SearchCriteria
     */
    @Test
    public final void searchJobByYearsOfExperienceCriteriaWithDistanceBasedSearchCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
            yearsOfExperienceCriteria.setMin((long) (1));
            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><yrsexp min=\"1\"/><distance max=\"10\" units=\"miles\"/><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    /**
     * SearchJob By YearsOfExperience Criteria With Distance Based
     * SearchCriteria
     */
    @Test
    public final void searchJobByYearsOfExperienceCriteriaWithGenericKeywordCriteriaAndDistanceBasedSearchCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            // Unregister already registered jobs with id 1 , 2
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Register jobs with id 1, 2
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            // Create a search request with required fields
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            // Keyword filter
            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("or");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("CommTech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            // Applicant Geography Filter
            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

            // Years Of Experience Filter
            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
            yearsOfExperienceCriteria.setMin((long) (1));
            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><yrsexp min=\"1\"/><distance max=\"10\" units=\"miles\"/><keyword><![CDATA[((\"Comcast\")) or ((\"CommTech\")) ]]></keyword><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    
    @Test
    public final void searchJobByFacetsCriteriaWithDistanceSearchCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            
            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.FacetFilters facetFilter = new com.bgt.lens.model.adminservice.request.FacetFilters();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("education");
            filter.setName("education");
            
            facetFilter.getPostingFilters().add(filter);
            
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setFacetFilters(facetFilter);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            //Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);
            
            // Unregister already registered jobs with id 1 , 2
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Register jobs with id 1, 2
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            // Create a search request with required fields
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());


            // Applicant Geography Filter
            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("1254");
            searchCriteria.setGeography(appGeoCriteria);

//            // Years Of Experience Filter
//            YearsOfExperience yearsOfExperienceCriteria = new YearsOfExperience();
//            yearsOfExperienceCriteria.setMin((long) (1));
//            yearsOfExperienceCriteria.setIncludeUnknown(Enum.locationUnknown.Include.toString());
//            searchCriteria.setYearsOfExperience(yearsOfExperienceCriteria);
            
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("education");
            boolean add = selectedFilters.getFilter().add("16");
            
            Facets facets = new Facets();
            facets.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facets);
            
            
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class);

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"10\" units=\"miles\"/><resume><contact><address><postalcode>1254</postalcode></address></contact></resume><facets><![CDATA[facets://education=[16]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }    
    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithCustomFiltersWithMinCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin((long) (2));
            filters.getFilter().add(filter01);

            searchCriteria.setCustomFilters(filters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><bgldlanguage min=\"2\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Job jobs = searchResult.getJobs().get(0);
                Assert.assertEquals("Job custom filter count check", jobs.getCustomFilters().size(), 1);

                com.bgt.lens.model.rest.response.Filter f = jobs.getCustomFilters().get(0);
                Assert.assertEquals("Job custom filter name check", f.getName(), "bgldlanguage");
                Assert.assertEquals("Job custom filter value check", (long) f.getValue(), 2);
                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithCustomFiltersWithMaxCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 3);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMax((long) (2));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><bgldlanguage max=\"2\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Job jobs = searchResult.getJobs().get(0);
                Assert.assertEquals("Job custom filter count check", jobs.getCustomFilters().size(), 1);

                com.bgt.lens.model.rest.response.Filter f = jobs.getCustomFilters().get(0);
                Assert.assertEquals("Job custom filter name check", f.getName(), "bgldlanguage");
                Assert.assertEquals("Job custom filter value check", (long) f.getValue(), 2);
                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithCustomFiltersWithMinandMaxCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin((long) (1));
            filter01.setMax((long) (2));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            jobSearchRequest.setSearchCriteria(searchCriteria);
            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><bgldlanguage max=\"2\" min=\"1\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());
                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithCustomFiltersWithValueCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setValue((long) (1));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><bgldlanguage max=\"1\" min=\"1\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());
                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithCustomFiltersWithArrayofValuesCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.getValues().add((long) (1));
            filter01.getValues().add((long) (2));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><bgldlanguage array=\"1 2\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());
                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithCustomFiltersWithEmptyCustomFilterNameCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("");
            filter01.setMin((long) (1));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Search job response check", response.status);
            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithCustomFiltersWithNullCustomFilterValueCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin(null);
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Search job response check", response.status);
            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithCustomFiltersWithInvalidCustomFilterNameCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("abcd");
            filter01.setMin((long) (1));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Search job response check", response.status);
            // Get remainingTransactionCount
            Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void searchJobWithCustomFiltersWithInvalidCustomFilterValueCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/129305.doc";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin((long) (5));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><bgldlanguage min=\"5\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());
                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * SearchJob with DocId disabled.
     */
    @Test
    public final void searchJobWithDocIdDisabled() {
        try {    
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            
            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("sm_region");
            filter.setName("sm_region");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (5));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("01201");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) (2));
            searchCriteria.setAdditionalFilters(additionalFilters);
            
            ResultFilters resultFilters =  new ResultFilters();
            resultFilters.setDocId(false);
            searchCriteria.setResultFilters(resultFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);
            

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"2\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"5\" units=\"miles\"/><resume><contact><address><postalcode>01201</postalcode></address></contact></resume><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

            }            
        
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * SearchJob with DocId Enabled.
     */
    @Test
    public final void searchJobWithDocIdEnabled() {
        try {    
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            
            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (5));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("01201");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) (2));
            searchCriteria.setAdditionalFilters(additionalFilters);
            
            ResultFilters resultFilters =  new ResultFilters();
            resultFilters.setDocId(true);
            searchCriteria.setResultFilters(resultFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);
            

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"2\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"5\" units=\"miles\"/><resume><contact><address><postalcode>01201</postalcode></address></contact></resume><include var=\"id\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

            }            
        
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with DocId and CustomXPath
     */
    @Test
    public final void searchJobWithDocIdEnabledWithCustomXpath() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (5));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.getCustomXpath().add("qualifications/skill");
            searchCriteria.setAdditionalFilters(additionalFilters);

            ResultFilters resultFilters = new ResultFilters();
            resultFilters.setDocId(true);
            searchCriteria.setResultFilters(resultFilters);            
            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><distance max=\"5\" units=\"miles\"/><include var=\"id\"/><include var=\"xpath://qualifications/skill\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
}
