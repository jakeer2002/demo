// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Tag binary data HRXML test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryLDTagBinaryDataHRXMLDataTest {
    
     @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;
    
    
    private ApiErrors apiErrors = new ApiErrors();
    

    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Set of LensSettings
     */
    private List<CoreLenssettings> lenssettingses;

    /**
     * US,UK,LD Set of LensSettings
     */
    private CoreLenssettings uslensSettings, uklensSettings, ldlensSettings;

    /**
     * Test helper object
     */
    private TestHelper testHelper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private byte[] binaryData;

    /**
     * Document type
     */
    private Character docType;

    /**
     * Default Locale
     */
    private String defaultLocale;

    /**
     * *
     * Helper object
     */
    private static final Helper _helper = new Helper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {        
              
        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        
        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lenssettingses = new ArrayList<>(0);
        // US Instances
        uslensSettings = new CoreLenssettings();
        uslensSettings.setHost(testHelper.hostname);
        uslensSettings.setPort(testHelper.port);
        uslensSettings.setCharacterSet(testHelper.encoding);
        uslensSettings.setTimeout(testHelper.timeOut);
        uslensSettings.setInstanceType(testHelper.instanceType);
        uslensSettings.setLanguage(testHelper.language);
        uslensSettings.setCountry(testHelper.country);
        uslensSettings.setHrxmlcontent(testHelper.hrxmlcontent);
        uslensSettings.setHrxmlversion(testHelper.hrxmlversion);
        uslensSettings.setHrxmlfilename(testHelper.hrxmlfilename);
        uslensSettings.setLocale(TestHelper.Locale.en_us.name());
        uslensSettings.setStatus(testHelper.status);

        lenssettingses.add(uslensSettings);

        // UK Instances
        uklensSettings = new CoreLenssettings();
        uklensSettings.setHost(testHelper.hostname);
        uklensSettings.setPort(testHelper.port);
        uklensSettings.setCharacterSet(testHelper.encoding);
        uklensSettings.setTimeout(testHelper.timeOut);
        uklensSettings.setInstanceType(testHelper.instanceType);
        uklensSettings.setLanguage(testHelper.language);
        uklensSettings.setCountry(testHelper.country);
        uklensSettings.setHrxmlcontent(testHelper.hrxmlcontent);
        uklensSettings.setHrxmlversion(testHelper.hrxmlversion);
        uklensSettings.setHrxmlfilename(testHelper.hrxmlfilename);
        uklensSettings.setLocale(TestHelper.Locale.en_gb.name());
        uklensSettings.setStatus(testHelper.status);

        lenssettingses.add(uklensSettings);

        // Locale Detector Instances
        ldlensSettings = new CoreLenssettings();
        ldlensSettings.setHost(testHelper.ld_hostname);
        ldlensSettings.setPort(testHelper.ld_port);
        ldlensSettings.setCharacterSet(testHelper.ld_encoding);
        ldlensSettings.setTimeout(testHelper.timeOut);
        ldlensSettings.setInstanceType(TestHelper.Locale.LD.name());
        ldlensSettings.setLocale("NA");
        ldlensSettings.setStatus(testHelper.status);

        lenssettingses.add(ldlensSettings);

        String fileName = "/TestResumes/100008.doc";
        InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
        try {
            if (resourceAsStream != null) {
                binaryData = testHelper.getBinaryData(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException ex) {
            resourceAsStream.close();
            throw ex;
        }
        extension = fileName.substring(fileName.indexOf('.') + 1);
        docType = 'R';
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * Locale detector Tag BinaryData HRXML Test With VaildData Returns Success
     */
    @Test
    public void ldtagBinaryDataHRXMLTestWithVaildDataReturnsSuccess() {
        try {
            // TagBinaryDataHRXML
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataHRXMLTestWithValidData_LanguageBasedParsing() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_us");
            lenssettingses.add(updateUSLensSettings);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataHRXMLTestWithValidData_DefaultLocale() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("*_*");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("*_*");
            lenssettingses.add(updateUSLensSettings);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ldTagBinaryDataTestWithVaildData
     */
    @Test
    public void ldTagBinaryDataHRXMLTestWithValidData_UnknownLocale() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            String fileName = "/TestResumes/chn_resume.txt";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            String errorMessage = "Lens instance details are missing";
            boolean checkContains = lensResponse.responseData.toString().contains(errorMessage);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid HostName Returns Failure
     */
    @Test
    public void ldtagBinaryDataHRXMLTestWithInvalidHostNameReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            // TagBinaryData HRXML
            uslensSettings.setHost("Invalid");

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Invalid Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Empty HostName Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithEmptyHostNameReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            // TagBinaryDataHRXML
            uslensSettings.setHost("");

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Null HostName Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullHostNameReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            // TagBinaryDataHRXML
            uslensSettings.setHost(null);

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Null Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid HostPort Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInvalidHostPortReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            // TagBinaryDataHRXML
            uslensSettings.setPort(0000);

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Invalid HostPort Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With InValid Encoding Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInValidEncodingReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            // TagBinaryDataHRXML
            uslensSettings.setCharacterSet("Invalid");

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With InValid Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Empty Encoding Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithEmptyEncodingReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            // TagBinaryDataHRXML
            uslensSettings.setCharacterSet("");

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Null Encoding Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullEncodingReturnsSuccess() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            // TagBinaryDataHRXML
            uslensSettings.setCharacterSet(null);

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid TimeOut Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInvalidTimeOutReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            // TagBinaryDataHRXML
            uslensSettings.setTimeout(-10);

            lenssettingses.remove(uslensSettings);
            lenssettingses.add(uslensSettings);
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With ConvertError Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithConvertErrorReturnsFailure() throws Exception {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_ConverterError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, false);
            
            String expResult = "(locale:2) error tagging: tag text not generated";
           Assert.assertEquals("Lens TagBinaryDataHRXML With convert error Check", apiErrors.getLensErrorMessage(expResult), lensResponse.responseData); 

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With TaggingError Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithTaggingErrorReturnsFailure() throws Exception {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("*_*");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_TaggingError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, false);
            String expResult = "error";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With LargeResumeFile Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithLargeResumeFileReturnsSuccess() throws Exception {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_us");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With NullBytes Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullBytesReturnsFailure() throws Exception {
        CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
        String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
        binaryData = null;
        extension = fileName.substring(fileName.indexOf('.') + 1);
        ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
        Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, false);
        Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);
    }

    /**
     * Tag BinaryData HRXML Test With UnicodeResume Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithUnicodeResumeReturnsSuccess() throws Exception {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("fr_xx");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("fr_xx");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/UnicodeResume/BEL_FRA_10370.tag.xsl.xml";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            String expResultUnicode = "túuýž„”—ácdéeínórŠTÚUÝŽ„”—ÁCDÉEÍNÓRôœùûüÿ«»—àâçéèê"
                    + "ëïîÔŒÙÛÜŸ«»—ÀÂÇÉÈÊËÏÎäöüßÄÖÜ—àèéìòóù«»—ÀÈÉÌÒÓÙ«»—„”—acelnós"
                    + "zz„”—ACELNÓSZZõóôúü—ãáâàçéêíÕÓÔÚÜ—ÃÁÂÀÇÉÊÍaâîst„”—«»AÂÎST„”—"
                    + "«»áéíñóúü¿¡—ÁÉÍÑÓÚÜ¿¡—äåàéö–»ÄÅÀÉÖ–»çgiIösü—ÇGIIÖSÜ—";
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With NullExtension Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullExtensionReturnsSuccess() throws Exception {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_us");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = null;
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Empty Extension Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithEmptyExtensionReturnsSuccess() throws Exception {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_us");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "";
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid Extension Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInvalidExtensionReturnsSuccess() throws Exception {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_us");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "Invalid";
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With JPNResume Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithJPNResumeReturnsSuccess() throws Exception {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("ja_*");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("ja_*");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/Resume_JPN.txt";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Null HRXML Content Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullHRXMLContentReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_us");
            updateUSLensSettings.setHrxmlcontent(null);
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }

            // TagBinaryDataHRXML
            lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);

        } catch (Exception ex) {
            if (ApiException.class.isInstance(ex)) {
                ApiException apiExp = (ApiException) ex;
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_HRXML_DATA), apiExp.getMessage());
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", apiExp.getStatusCode(), HttpStatus.METHOD_NOT_ALLOWED);
            } else {
                System.out.println(ex);
                Assert.fail();
            }
        }
    }

    /**
     * Tag BinaryData HRXML Test With Empty HRXML Content Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithEmptyHRXMLContentReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_us");
            updateUSLensSettings.setHrxmlcontent("");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            // TagBinaryDataHRXML
            lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);

        } catch (Exception ex) {
            if (ApiException.class.isInstance(ex)) {
                ApiException apiExp = (ApiException) ex;
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_HRXML_DATA), apiExp.getMessage());
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", apiExp.getStatusCode(), HttpStatus.METHOD_NOT_ALLOWED);
            } else {
                System.out.println(ex);
                Assert.fail();
            }
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid HRXML Content Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInvalidHRXMLContentReturnsFailure() {
        try {
            CreateConsumerRequest.DefaultLocaleList defaultLocaleList = new CreateConsumerRequest.DefaultLocaleList();
            List<com.bgt.lens.model.adminservice.request.Locale> localeList = new ArrayList<>();
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("en");
            locale.setCountry("us");
            localeList.add(locale);
            locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setLanguage("*");
            locale.setCountry("en_us");
            localeList.add(locale);

            defaultLocaleList.getLocale().addAll(localeList);

            defaultLocale = _helper.getJsonStringFromObject(defaultLocaleList);
            lenssettingses.remove(uslensSettings);
            CoreLenssettings updateUSLensSettings = uslensSettings;
            updateUSLensSettings.setLocale("en_us");
            updateUSLensSettings.setHrxmlcontent("Invalid");
            lenssettingses.add(updateUSLensSettings);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            // TagBinaryDataHRXML
            lensRepository.ldTagBinaryHRXMLData(lenssettingses, binaryData, extension, docType, defaultLocale);

        } catch (Exception ex) {
            if (ApiException.class.isInstance(ex)) {
                ApiException apiExp = (ApiException) ex;
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", _helper.getErrorMessageWithURL(ApiErrors.STYLESHEET_TRANSFORMATION_ERROR), apiExp.getMessage());
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", apiExp.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
            } else {
                System.out.println(ex);
                Assert.fail();
            }
        }
    }

}
