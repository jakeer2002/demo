// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.coreservice;

import com.bgt.lens.model.adminservice.request.ClientApi;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.GetApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateApiRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.test.Helper.TestHelper;
import java.util.List;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * *
 * API test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ApiTest {

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;
    
    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;
    
    @Autowired
    protected WebApplicationContext wac;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     */
    @Before
    public void setUp() {
           
        ServletContext servletContext = wac.getServletContext();
       
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);    
        
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
         
    }

    /**
     * *
     * CreateApiWithValidData
     */
    @Test
    public void createApiWithValidData() {
        try {
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            //CreateApiRequest createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(ClientTest.class.getResourceAsStream("/input/CreateApi.xml"));
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api = (Api) response.responseData;

            response = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateApiWithDuplicateKeyandName
     */
    @Test
    public void createApiWithDuplicateKeyandName() {
        try {
            //Create Api
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api = (Api) response.responseData;

            try {
                response = coreService.createApi(createApi);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create Api duplicate check", response.status, false);

            response = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateApiWithValidData
     */
    @Test
    public void updateApiWithValidData() {
        try {
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api = (Api) response.responseData;

            UpdateApiRequest request = _testhelper.getUpdateApiRequest("/input/UpdateApi.xml");
            response = coreService.updateApi(api.getId(), request);
            Assert.assertEquals("Update Api", response.status, true);

            response = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateApiWithInvalidApiId
     */
    @Test
    public void updateApiWithInvalidApiId() {
        try {
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api = (Api) response.responseData;

            UpdateApiRequest request = _testhelper.getUpdateApiRequest("/input/UpdateApi.xml");
            response = coreService.updateApi(0, request);
            Assert.assertEquals("Update Api", response.status, false);

            response = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateApiWithDuplicateApiKeyandName
     */
    @Test
    public void updateApiWithDuplicateApiKeyandName() {
        try {
            //Api 1
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api1 = (Api) response.responseData;

            createApi.setKey("Sample02");
            createApi.setName("Sample02");
            response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api2 = (Api) response.responseData;

            UpdateApiRequest request = _testhelper.getUpdateApiRequest("/input/UpdateApi.xml");
            request.setKey("Sample02");
            request.setName("Sample02");

            try {
                response = coreService.updateApi(api1.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Update Api", response.status, false);

            response = coreService.deleteApi(api1.getId());
            Assert.assertEquals("Create Api delete", response.status, true);

            response = coreService.deleteApi(api2.getId());
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * DeleteApiWithValidId
     */
    @Test
    public void deleteApiWithValidId() {
        try {
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api = (Api) response.responseData;

            response = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * DeleteApiWithInvalidId
     */
    @Test
    public void deleteApiWithInvalidId() {
        try {
            ApiResponse response = coreService.deleteApi(0);
            Assert.assertEquals("Create Api delete", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetListofApi
     */
    @Test
    public void getListofApi() {
        try {
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api = (Api) response.responseData;

            GetApiRequest apirequest = new GetApiRequest();

            response = coreService.getApi(apirequest);

            Assert.assertEquals("Get Api", response.status, true);

            List<ClientApi> clientApi = (List<ClientApi>) response.responseData;
            Assert.assertTrue("Get Api", clientApi.size() > 0);

            response = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetListofApiWithValidKeyandName
     */
    @Test
    public void getListofApiWithValidKeyandName() {
        try {
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api = (Api) response.responseData;

            GetApiRequest apirequest = new GetApiRequest();
            apirequest.setApiKey("Core");
            apirequest.setApiName("Core");

            response = coreService.getApi(apirequest);

            Assert.assertEquals("Get Api", response.status, true);

            List<ClientApi> clientApi = (List<ClientApi>) response.responseData;
            Assert.assertTrue("Get Api", clientApi.size() == 1);

            response = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetListofApiWithInvalidKeyandName
     */
    @Test
    public void getListofApiWithInvalidKeyandName() {
        try {
            GetApiRequest apirequest = new GetApiRequest();
            apirequest.setApiKey("xxxx");
            ApiResponse response = coreService.getApi(apirequest);
            Assert.assertEquals("Get Api", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetApibyIdWithValidApiId
     */
    @Test
    public void getApibyIdWithValidApiId() {
        try {
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            Api api = (Api) response.responseData;

            response = coreService.getApiById(api.getId());
            Assert.assertEquals("Get Api", response.status, true);

            response = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetApibyIdWithInvalidApiId
     */
    @Test
    public void getApibyIdWithInvalidApiId() {
        try {
            ApiResponse response = coreService.getApiById(0);
            Assert.assertEquals("Get Api", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }
}
