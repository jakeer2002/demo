// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.parserservice.request.ParseJobRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * *
 * Parse Job BGT XML test without authentication
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ParseJobBGTXMLWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    IUtilityService utilityService;

    /**
     * *
     * API context
     */
    public ApiContext apiContext;

    /**
     * *
     * API
     */
    public Api api;

    /**
     * *
     * Lens settings
     */
    public LensSettings en_usLensSettings, en_ukLensSettings, localeSettings;

    /**
     * *
     * Resource
     */
    public Resources resource;

    /**
     * *
     * Client
     */
    public Client client;

    /**
     * *
     * Test helper object
     */
    public TestHelper testHelper;

    /**
     * *
     * Helper object
     */
    public Helper _helper;

    /**
     * *
     * Extension
     */
    private String extension;

    /**
     * *
     * Binary data
     */
    private String binaryData;

    /**
     * *
     * Job parsing variant
     */
    public static final String variant = "bgtxml";

    /**
     * *
     * Instance type
     */
    public static String InstanceType = "Lens";

    /**
     * *
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * *
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * *
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * *
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        try {
            String propFileName = "/JobTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            InstanceType = testHelper.instanceType;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettingsForJob.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ParseJobBGTXMLWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;

            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("Post");
            apiContext.setResource("job");
            apiContext.setClient(null);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            String fileName = "/TestJobs/10002.txt";
            InputStream resourceAsStream04 = ParseJobBGTXMLWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);

        } catch (IOException ex) {
            System.out.print(ex);
            Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ParseJobBGTXMLWithoutAuthenticationTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithoutAuthenticationWithValidBinaryJobData
     */
    @Test
    public void parseJobBGTXMLWithoutAuthenticationWithValidBinaryJobData() {
        try {
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXML WithoutAuthentication With ValidBinary JobData check", response.status, true);

            // ParseJobBGTXML Assert
            String expResult = "JobDoc";
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobBGTXMLWithValidBinaryJobDataWithLocaleBasedParsing() {
        try {
            // ParseResumeBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXML WithoutAuthentication With ValidBinary JobData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "JobDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobBGTXMLWithValidBinaryJobDataLocaleBasedParsingWithInvalidLocale() {
        try {
            // ParseResumeBGTXML
            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = ParseJobBGTXMLWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXML WithoutAuthentication With ValidBinary JobData check", response.status, false);

            // ParseResumeBGTXML Assert
            String expResult = "missing";
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithoutAuthenticationWithConvertError
     */
    @Test
    public void parseJobBGTXMLWithoutAuthenticationWithConvertError() {
        try {
            String fileName = "/TestJobs/EmptyJob/ExternalJob.doc";
            InputStream resourceAsStream04 = ParseJobBGTXMLWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXML WithoutAuthentication With Empty BinaryData check", response.status, false);

            // ParseJobBGTXML Assert
            String convertJob = (String) response.responseData;
            String expResult = "Converters returned empty output";
            Assert.assertEquals("ParseJobBGTXML WithoutAuthentication With convert error Check", apiErrors.getLensErrorMessage(expResult), convertJob);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithoutAuthenticationWithConvertErrorStatus
     */
    @Test
    public void parseJobBGTXMLWithoutAuthenticationWithConvertErrorStatus() {
        try {
            String fileName = "/TestJobs/EmptyJob/ExternalJob.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseJobVariant(parseJobRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseJobBGTXML WithoutAuthentication With Null BinaryData check", response.status, false);

            // ParseJobBGTXML Assert
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithoutAuthenticationSpecificInstance
     */
    @Test
    public void parseJobBGTXMLWithoutAuthenticationSpecificInstance() {
        try {
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale(LocaleTypeEn_uk);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXML Without Authentication Specific Instance check", response.status, true);

            // ParseJobBGTXML Assert
            String expResult = "JobDoc";
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithoutAuthenticationUnauthorizedInstance
     */
    @Test
    public void parseJobBGTXMLWithoutAuthenticationUnauthorizedInstance() {
        try {
            // ParseJobBGTXML
            String localeType = "en_anz";
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale(localeType);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXML Without Authentication Unauthorized Instance check", response.status, false);

            // ParseJobBGTXML Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse,invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithoutAuthenticationInvalidHostNameList
     */
    @Test
    public void parseJobBGTXMLWithoutAuthenticationInvalidHostNameList() {
        try {
            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale("en_sgp");
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXML Without Authentication Invalid HostName check", response.status, false);

            // ParseJobBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            
            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithoutAuthenticationInActiveLensList
     */
    @Test
    public void parseJobBGTXMLWithoutAuthenticationInActiveLensList() {
        try {
            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(false);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale("en_sgp");
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXML Without Authentication InActive Lens check", response.status, false);

            // ParseJobBGTXML inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            Assert.assertEquals("ParseJobBGTXML Without Authentication With InActive Lens check",_helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS),instanceResponse);

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
