// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.PingRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.Instance;
import com.bgt.lens.model.rest.response.InstanceList;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Ping test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class PingTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();


    @Autowired
    ICoreService coreService;

    @Autowired
    IUtilityService utilityService;

    /**
     * API context
     */
    public ApiContext apiContext;

    /**
     * Helper object
     */
    public TestHelper testHelper = null;

    /**
     * *
     * API
     */
    public Api api;

    /**
     * *
     * Lens settings
     */
    public LensSettings en_usLensSettings, en_ukLensSettings, localeSettings;
    /**
     * *
     * Resources
     */
    public Resources resource;

    /**
     * *
     * Client
     */
    public Client client;

    /**
     * *
     * Client
     */
    public CoreClient coreClient;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Instance type
     */
    public static String InstanceType = "SPECTRUM";

    /**
     * Locale IS
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";
    
    /**
     * Country UK
     */
    public static final String CountryEn_uk = "GreatBritain";

    /**
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * Info success
     */
    public static final String pingSuccess = "true";

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     * @throws javax.xml.bindJAXBException
     */
    @Before
    public void setUp()  throws JAXBException {
        ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
           servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        try {

            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            InstanceType = testHelper.instanceType;
            // create api
            CreateApiRequest createApi = testHelper.getCreateApiRequest("/input/CreateApi.xml");
            //CreateApiRequest createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(ConvertTests.class.getResourceAsStream("/input/CreateApi.xml"));
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettingsForJob.xml");

            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ConvertTests.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;

            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // create resource
            AddResourceRequest resourceRequest = testHelper.getAddResourceRequest("/input/CreateResource.xml");
            //AddResourceRequest resourceRequest = (AddResourceRequest) jaxbUnmarshaller.unmarshal(ConvertTests.class.getResourceAsStream("/input/CreateResource.xml"));
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            //CreateConsumerRequest consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(ConvertTests.class.getResourceAsStream("/input/CreateConsumer.xml"));

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();

            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_usLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_ukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);

            consumer.setLensSettings(settingsList);

            consumer = testHelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;

            testHelper = new TestHelper(propFileName);
            CoreClient coreClientApicontext = new CoreClient();
            coreClientApicontext.setId(client.getId());
            coreClientApicontext.setClientKey(client.getKey());
            coreClientApicontext.setSecret(client.getSecret());
            coreClientApicontext.setDumpStatus(client.isDumpStatus());            

            // en_us LensSettings
            CoreLenssettings en_usCoreLenssettings = new CoreLenssettings();
            en_usCoreLenssettings.setId(1);
            en_usCoreLenssettings.setHost(testHelper.hostname);
            en_usCoreLenssettings.setPort(testHelper.port);
            en_usCoreLenssettings.setCharacterSet(testHelper.encoding);
            en_usCoreLenssettings.setTimeout(testHelper.timeOut);
            en_usCoreLenssettings.setInstanceType(testHelper.instanceType);
            en_usCoreLenssettings.setLocale(testHelper.locale);
            en_usCoreLenssettings.setStatus(testHelper.status);            

            // en_uk LensSettings
            CoreLenssettings en_ukCoreLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            en_ukCoreLenssettings.setId(2);
            en_ukCoreLenssettings.setLocale(LocaleTypeEn_uk);
            en_ukCoreLenssettings.setCountry(CountryEn_uk);            

            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings coreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            coreClientlenssettings.setCoreLenssettings(en_usCoreLenssettings);         
            coreClientlenssettingses.add(coreClientlenssettings);
            coreClientlenssettings.setCoreLenssettings(en_ukCoreLenssettings);  
            coreClientlenssettingses.add(coreClientlenssettings);           
            
            coreClientApicontext.setCoreClientlenssettingses(coreClientlenssettingses);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("GET");
            apiContext.setResource("Status");
            apiContext.setClient(coreClientApicontext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {

        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete", deleteApiResponse.status, true);
            // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete", deleteConsumerResponse.status, true);
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);
            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingAll
     */
    @Test
    public void getPingAll() {
        try {
            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping All Instance check", response.status, true);
            InstanceList instanceList = (InstanceList) response.responseData;
            // Ping en_us Assert
            boolean en_usStatusCheckContains = instanceList.instances.get(1).status.contains(pingSuccess);
            assertTrue(en_usStatusCheckContains);
            // Ping en_uk Assert
            boolean en_ukStatusCheckContains = instanceList.instances.get(0).status.contains(pingSuccess);
            assertTrue(en_ukStatusCheckContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingSpecificInstance
     */
    @Test
    public void getPingSpecificInstance() {
        try {
            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            pingRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping Specific Instance check", response.status, true);

            // Ping en_us Assert
            Instance instance = (Instance) response.responseData;
            boolean en_usCheckContains = instance.locale.contains(LocaleTypeEn_us);
            boolean en_usStatusCheckContains = instance.status.contains(pingSuccess);
            assertTrue(en_usCheckContains);
            assertTrue(en_usStatusCheckContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingUnauthorizedInstance
     */
    @Test
    public void getPingUnauthorizedInstance() {
        try {
            // Ping
            String localeType = "en_anz";
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            pingRequest.setLocale(localeType);
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping Unauthorized Instance check", response.status, false);

            // Ping en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingEmptyInstanceList
     */
    @Test
    public void getPingEmptyInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            ApiResponse apiResponse = new ApiResponse();
            try {
                apiResponse = utilityService.ping(pingRequest);
            } catch (Exception ex) {
                apiResponse.status = false;
                apiResponse.responseData = ex.getMessage();
            }
            Assert.assertEquals("XRay Ping Empty Instance List check", apiResponse.status, false);

            // Ping en_us Assert
            String instanceResponse = (String) apiResponse.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingNullInstanceList
     */
    @Test
    public void getPingNullInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            ApiResponse apiResponse = new ApiResponse();
            try {
                apiResponse = utilityService.ping(pingRequest);
            } catch (Exception ex) {
                apiResponse.status = false;
                apiResponse.responseData = ex.getMessage();
            }
            Assert.assertEquals("XRay Ping Null Instance List check", apiResponse.status, false);

            // Ping en_us Assert
            String instanceResponse = (String) apiResponse.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingInvalidLensList
     */
    @Test
    public void getPingInvalidLensList() {
        try {
            //Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            pingRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping Lens Invalid check", response.status, true);

            // Ping en_us Assert
            Instance instance = (Instance) response.responseData;
            boolean en_usCheckContains = instance.locale.contains(LocaleTypeEn_us);
            boolean en_usStatusCheckContains = instance.status.contains("false");
            assertTrue(en_usCheckContains);
            assertTrue(en_usStatusCheckContains);
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingInActiveLensList
     */
    @Test
    public void getPingInActiveLensList() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            pingRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping InActive Lens check", response.status, false);

            // Ping inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            Assert.assertEquals("XRay Ping InActive Lens check", _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS), instanceResponse);
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
