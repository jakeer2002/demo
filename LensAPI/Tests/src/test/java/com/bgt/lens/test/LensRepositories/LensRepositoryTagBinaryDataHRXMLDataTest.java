// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Tag binary data HRXML test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryTagBinaryDataHRXMLDataTest {
    
     @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * *
     * Helper object
     */
    private static final Helper _helper = new Helper();


    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Lens settings object
     */
    private CoreLenssettings lensSettings;

    /**
     * Test helper object
     */
    private TestHelper testHelper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private byte[] binaryData;

    /**
     * Document type
     */
    private Character docType;
    
    private String customSkillKey;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        
        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);

        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lensSettings = new CoreLenssettings();
        lensSettings.setHost(testHelper.hostname);
        lensSettings.setPort(testHelper.port);
        lensSettings.setCharacterSet(testHelper.encoding);
        lensSettings.setTimeout(testHelper.timeOut);
        lensSettings.setLanguage(testHelper.language);
        lensSettings.setCountry(testHelper.country);
        lensSettings.setHrxmlcontent(testHelper.hrxmlcontent);
        lensSettings.setHrxmlversion(testHelper.hrxmlversion);
        lensSettings.setHrxmlfilename(testHelper.hrxmlfilename);
        lensSettings.setInstanceType(testHelper.instanceType);
        lensSettings.setLocale(testHelper.locale);
        lensSettings.setStatus(testHelper.status);
        
        customSkillKey = testHelper.customSkillsKey;

        String fileName = "/TestResumes/100008.doc";
        InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
        try {
            if (resourceAsStream != null) {
                binaryData = testHelper.getBinaryData(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException ex) {
            resourceAsStream.close();
            throw ex;
        }
        extension = fileName.substring(fileName.indexOf('.') + 1);
        docType = 'R';
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * Tag BinaryData HRXML Test With VaildData Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithVaildDataReturnsSuccess() {
        try {
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid HostName Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInvalidHostNameReturnsFailure() {
        try {
            // TagBinaryDataHRXML
            lensSettings.setHost("Invalid");
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Invalid Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Empty HostName Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithEmptyHostNameReturnsFailure() {
        try {
            // TagBinaryDataHRXML
            lensSettings.setHost("");
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Null HostName Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullHostNameReturnsFailure() {
        try {
            // TagBinaryDataHRXML
            lensSettings.setHost(null);
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Null Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid HostPort Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInvalidHostPortReturnsFailure() {
        try {
            // TagBinaryDataHRXML
            lensSettings.setPort(0000);
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Invalid HostPort Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With InValid Encoding Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInValidEncodingReturnsFailure() {
        try {
            // TagBinaryDataHRXML
            lensSettings.setCharacterSet("Invalid");
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With InValid Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Empty Encoding Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithEmptyEncodingReturnsFailure() {
        try {
            // TagBinaryDataHRXML
            lensSettings.setCharacterSet("");
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Null Encoding Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullEncodingReturnsSuccess() {
        try {
            // TagBinaryDataHRXML
            lensSettings.setCharacterSet(null);
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Null Encoding Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid TimeOut Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInvalidTimeOutReturnsFailure() {
        try {
            // TagBinaryDataHRXML
            lensSettings.setTimeout(-10);
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With ConvertError Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithConvertErrorReturnsFailure() {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_ConverterError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, false);
            String expResult = "(tag:6) Converters returned empty output";
            Assert.assertEquals("Lens TagBinaryDataHRXML With convert error Check", apiErrors.getLensErrorMessage(expResult), lensResponse.responseData);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With TaggingError Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithTaggingErrorReturnsFailure() {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_TaggingError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, false);
            String expResult = "error";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With LargeResumeFile Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithLargeResumeFileReturnsSuccess() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With NullBytes Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullBytesReturnsFailure() {
        String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
        binaryData = null;
        extension = fileName.substring(fileName.indexOf('.') + 1);
        ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
        Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, false);
        Assert.assertEquals("Lens TagBinaryDataHRXML With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);
    }

    /**
     * Tag BinaryData HRXML Test With UnicodeResume Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithUnicodeResumeReturnsSuccess() {
        try {
            String fileName = "/TestResumes/UnicodeResume/BEL_FRA_10370.tag.xsl.xml";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            String expResultUnicode = "tÃºuÃ½Å¾â€žâ€�â€”Ã¡cdÃ©eÃ­nÃ³rÅ TÃšUÃ�Å½â€žâ€�â€”Ã�CDÃ‰EÃ�NÃ“RÃ´Å“Ã¹Ã»Ã¼Ã¿Â«Â»â€”Ã Ã¢Ã§Ã©Ã¨Ãª"
                    + "Ã«Ã¯Ã®Ã”Å’Ã™Ã›ÃœÅ¸Â«Â»â€”Ã€Ã‚Ã‡Ã‰ÃˆÃŠÃ‹Ã�ÃŽÃ¤Ã¶Ã¼ÃŸÃ„Ã–Ãœâ€”Ã Ã¨Ã©Ã¬Ã²Ã³Ã¹Â«Â»â€”Ã€ÃˆÃ‰ÃŒÃ’Ã“Ã™Â«Â»â€”â€žâ€�â€”acelnÃ³s"
                    + "zzâ€žâ€�â€”ACELNÃ“SZZÃµÃ³Ã´ÃºÃ¼â€”Ã£Ã¡Ã¢Ã Ã§Ã©ÃªÃ­Ã•Ã“Ã”ÃšÃœâ€”ÃƒÃ�Ã‚Ã€Ã‡Ã‰ÃŠÃ�aÃ¢Ã®stâ€žâ€�â€”Â«Â»AÃ‚ÃŽSTâ€žâ€�â€”"
                    + "Â«Â»Ã¡Ã©Ã­Ã±Ã³ÃºÃ¼Â¿Â¡â€”Ã�Ã‰Ã�Ã‘Ã“ÃšÃœÂ¿Â¡â€”Ã¤Ã¥Ã Ã©Ã¶â€“Â»Ã„Ã…Ã€Ã‰Ã–â€“Â»Ã§giIÃ¶sÃ¼â€”Ã‡GIIÃ–SÃœâ€”";
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With NullExtension Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullExtensionReturnsSuccess() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = null;
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Empty Extension Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithEmptyExtensionReturnsSuccess() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "";
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid Extension Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInvalidExtensionReturnsSuccess() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "Invalid";
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With JPNResume Returns Success
     */
    @Test
    public void tagBinaryDataHRXMLTestWithJPNResumeReturnsSuccess() {
        try {
            String fileName = "/TestResumes/Resume_JPN.txt";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            // TagBinaryDataHRXML
            ApiResponse lensResponse = lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataHRXML Assert
            Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", lensResponse.status, true);
            //String expResult = "Candidate";
            String lensResponseMessage = (String) lensResponse.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Tag BinaryData HRXML Test With Null HRXML Content Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithNullHRXMLContentReturnsFailure() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            lensSettings.setHrxmlcontent(null);
            // TagBinaryDataHRXML
            lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);

        } catch (Exception ex) {
            if (ApiException.class.isInstance(ex)) {
                ApiException apiExp = (ApiException) ex;
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_HRXML_DATA), apiExp.getMessage());
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", apiExp.getStatusCode(), HttpStatus.METHOD_NOT_ALLOWED);
            } else {
                System.out.println(ex);
                Assert.fail();
            }
        }
    }

    /**
     * Tag BinaryData HRXML Test With Empty HRXML Content Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithEmptyHRXMLContentReturnsFailure() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            lensSettings.setHrxmlcontent("");
            // TagBinaryDataHRXML
            lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);

        } catch (Exception ex) {
            if (ApiException.class.isInstance(ex)) {
                ApiException apiExp = (ApiException) ex;
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_HRXML_DATA), apiExp.getMessage());
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", apiExp.getStatusCode(), HttpStatus.METHOD_NOT_ALLOWED);
            } else {
                System.out.println(ex);
                Assert.fail();
            }
        }
    }

    /**
     * Tag BinaryData HRXML Test With Invalid HRXML Content Returns Failure
     */
    @Test
    public void tagBinaryDataHRXMLTestWithInvalidHRXMLContentReturnsFailure() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            lensSettings.setHrxmlcontent("Invalid");
            // TagBinaryDataHRXML
            lensRepository.tagBinaryHRXMLData(lensSettings, binaryData, extension, docType,customSkillKey);

        } catch (Exception ex) {
            if (ApiException.class.isInstance(ex)) {
                ApiException apiExp = (ApiException) ex;
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", _helper.getErrorMessageWithURL(ApiErrors.STYLESHEET_TRANSFORMATION_ERROR), apiExp.getMessage());
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", apiExp.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
            } else {
                System.out.println(ex);
                Assert.fail();
            }
        }
    }

}
