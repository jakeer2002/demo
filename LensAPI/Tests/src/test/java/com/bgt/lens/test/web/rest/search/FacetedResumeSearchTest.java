// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Enum;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.Filter;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.SearchResult;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.AdditionalFilters;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.CustomFilters;
import com.bgt.lens.model.searchservice.request.Facets;
import com.bgt.lens.model.searchservice.request.Keywords;
import com.bgt.lens.model.searchservice.request.PaginateResults;
import com.bgt.lens.model.searchservice.request.ResultFacets;
import com.bgt.lens.model.searchservice.request.ResumeSearchRequest;
import com.bgt.lens.model.searchservice.request.SearchCriteria;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Job;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Keyword;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Resume;
import com.bgt.lens.model.searchservice.request.SelectedFilters;
import com.bgt.lens.model.soap.response.searchservice.ResumeSearchResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Search Resume Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class FacetedResumeSearchTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor adminVendor;
    /**
     * External client vendor
     */
    private Vendor externalClientVendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Admin client
     */
    private CoreClient adminClient;
    /**
     * ExternalClient
     */
    private CoreClient externalClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Job BinaryData.
     */
    private String jobBinaryData;
    /**
     * Extension
     */
    private String extension;
    /**
     * Doc type
     */
    private Character docType;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            createApi.setKey("Search");
            createApi.setName("Search Api");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettings.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            Filter filter = new Filter();
            filter.setKey(testHelper.hypercubeDimensionName01);
            filter.setName("Jobtitle");
            Filter filter01 = new Filter();
            filter01.setKey(testHelper.hypercubeDimensionName02);
            filter01.setName("Skills");
            Filter filter02 = new Filter();
            filter02.setKey(testHelper.hypercubeDimensionName03);
            filter02.setName("Employer");
            settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter01);
            settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter02);

            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setFilterSettings(null);
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");
            resourceRequest.setApiKey(api.getKey());
            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
           
            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
           
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
           
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor02");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
//            createVendorRequest.setHypercubeServerName(testHelper.hypercubeServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            adminVendor = (Vendor) response.responseData;

            createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0202");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
//            createVendorRequest.setHypercubeServerName(testHelper.hypercubeServerName);
            response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            externalClientVendor = (Vendor) response.responseData;

            SearchSettings searchSettings = new SearchSettings();
            searchSettings.getResume().clear();
            SearchSettings.Resume resume = new SearchSettings.Resume();
            resume.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            resume.setVendorId(BigInteger.valueOf(adminVendor.getVendorId()));
            searchSettings.getResume().add(resume);

            String searchSettingsJson = helper.getJsonStringFromObject(searchSettings);

            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            consumer.setLensSettings(settingsList);

            CreateConsumerRequest.VendorSettings settings = new CreateConsumerRequest.VendorSettings();
            settings.getVendorIdList().add(BigInteger.valueOf(adminVendor.getVendorId()));
            consumer.setVendorSettings(settings);
            consumer.setSearchSettings(searchSettings);

            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
//            consumer.getVendorSettings().getVendorIdList().clear();
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);

            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);

            adminClient = dbService
                    .getClientById(client.getId());
            adminClient.setTier(1);

            CoreClient updatedAdminClient = adminClient;
            ApiResponse updateClientResponse = dbService.updateClient(adminClient, updatedAdminClient);
            Assert.assertTrue("Update client check", updateClientResponse.status);

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            String jobfileName = "/TestJobs/10001.txt";
            InputStream resourceAsStream05 = RegisterResumeTest.class.getResourceAsStream(jobfileName);
            try {
                if (resourceAsStream05 != null) {
                    jobBinaryData = helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream05));
                    resourceAsStream05.close();
                }
            } catch (IOException ex) {
                resourceAsStream05.close();
                throw ex;
            }

            extension = jobfileName.substring(jobfileName.indexOf('.') + 1);
            docType = 'P';
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
               // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);

            ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if (clientList != null && clientList.size() > 0) {
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister resume request", deleteExternalClientResponse.status);
            }
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
         

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidFacetCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithMultipleSelectedFiltersWithAndContionReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            selectedFilters.getFilter().add("Visual InterDev");
            selectedFilters.setOperator("And");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/skills=[Visual InterDev]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithEmptyFacetCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            searchCriteria.setFacets(facetCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = true;
            }
            Assert.assertTrue("Search response status check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithInvalidFacetDimensionNameReturnsFailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("sample");
            selectedFilters.getFilter().add("Writing");
            selectedFilters.getFilter().add("Accounts Payable and Receivable");
            selectedFilters.setOperator("And");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = true;
            }
            Assert.assertTrue("Search response status check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithEmptySelectedDimensionReturnsFailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = true;
            }
            Assert.assertTrue("Search response status check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithEmptySelectedFilterListReturnsFailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = true;
            }
            Assert.assertTrue("Search response status check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidFacetCriteriaAndKeywordCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor02\"><keyword><![CDATA[(resume/experience/job[pos='1']:(\"CWTS\")) and (resume/experience/job[pos='1']:(\"Apar Infotech\") or resume/experience/job[pos='2']:(\"Apar Infotech\")) ]]></keyword><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
/*    @Test
    public final void searchResumeWithValidFacetCriteriaAndKeywordCriteriaCKSReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CKS.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);
            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor02\"><keyword><![CDATA[CKS((\"CWTS\") and (\"Apar Infotech\") )]]></keyword><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    } */

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidFacetCriteriaAndKeywordCriteriaCstomBooleanSearchReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))");
            searchCriteria.setKeyword(keywordCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor02\"><keyword><![CDATA[(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))]]></keyword><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidFacetCriteriaAndCustomFiltersReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = testHelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setHost(testhelper.hostname);
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            updateRequest.setPort(BigInteger.valueOf(enusLensSettings.getPort()));

            filter = new Filter();
            filter.setKey(testHelper.hypercubeDimensionName01);
            filter.setName("Jobtitle");
            Filter filter001 = new Filter();
            filter001.setKey(testHelper.hypercubeDimensionName02);
            filter001.setName("Skills");
            Filter filter02 = new Filter();
            filter02.setKey(testHelper.hypercubeDimensionName03);
            filter02.setName("Employer");
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter001);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter02);
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 3);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin((long)2);
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor02\"><facets><![CDATA[facets://skills=[Writing]/]]></facets><bgldlanguage min=\"2\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidFacetCriteriaAndJobDescriptionReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(binaryData);
            searchCriteria.setJob(jobDocumentCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertTrue("Search command check", searchCommandDumpList.get(0).getSearchCommand().contains("</posting>"));
                Assert.assertTrue("Search command check", searchCommandDumpList.get(0).getSearchCommand().contains("<facets>"));
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidFacetCriteriaAndResumeDocumentCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Resume resumeDocumentCriteria = new Resume();
            resumeDocumentCriteria.setBinaryData(binaryData);
            resumeDocumentCriteria.setExtension(extension);
            searchCriteria.setResume(resumeDocumentCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertTrue("Search command check", searchCommandDumpList.get(0).getSearchCommand().contains("</resume>"));
                Assert.assertTrue("Search command check", searchCommandDumpList.get(0).getSearchCommand().contains("<facets>"));
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidFacetCriteriaAndResumeLikeThisCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Resume resumesLikeThisCriteria = new Resume();
            resumesLikeThisCriteria.setResumeId("1");
            searchCriteria.setResume(resumesLikeThisCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"resume\" vendor=\"resvendor02\"><doc id=\"1\" type=\"resume\" vendor=\"resvendor02\"/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidResultFacetCriteriaWithoutSelectedDimensionReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri><![CDATA[facets://skills=[Writing]/*]]></uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 1);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(0).getFilterCount() == 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidResultFacetCriteriaWithSelectedDimensionAloneReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedFilters().remove(selectedFilters);
            facetCriteria.getSelectedDimensions().add("skills");

            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri>facets://skills</uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 1);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(0).getFilterCount() > 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidMultipleResultFacetCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("JobTitle");
            selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            selectedFilters.getFilter().add("Accounts Payable and Receivable");
            selectedFilters.setOperator("And");
            facetCriteria.getSelectedFilters().add(selectedFilters);

            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("Skills");
            selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("Employer");
            selectedFilters.getFilter().add("VPG HONING INDUSTRIES");
            selectedFilters.getFilter().add("Apar Infotech Corporation");
            selectedFilters.setOperator("Or");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri><![CDATA[facets://skills=[Writing]/skills=[Accounts Payable and Receivable]/title]]></uri><uri><![CDATA[facets://employer=[VPG HONING INDUSTRIES][Apar Infotech Corporation]/skills]]></uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 2);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(0).getFilterCount() == 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithEmptyResultFacetCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();

            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() == null);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithInvalidDimensionNameReturnsFailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("sample");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();

            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("Faceted search with result facet distribution check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());
                Assert.assertEquals("SearchResult score Check", 5, (long) searchResult.getCandidates().get(0).getScore());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithoutScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());
                Assert.assertTrue("SearchResult score Check", (long) searchResult.getCandidates().get(0).getScore() > 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithInvalidScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode("Sample");
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("Faceted search response with invalid scoring mode check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidShowRecordsFilterandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setShowRecords("false");
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" show-records=\"false\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri>facets://skills</uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());
                Assert.assertTrue("SearchResult score Check", searchResult.getCandidates() == null);

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 1);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(0).getFilterCount() > 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidShowRecordsMinandMaxFilterandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMinIndex((long) 1);
            paginateResults.setMaxIndex((long) 1);
            additionalFilters.setPaginateResults(paginateResults);

            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" show-records=\"1,1\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri>facets://skills</uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 1);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(0).getFilterCount() > 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithInvalidMinandMaxFilterandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMinIndex((long) 1);
            paginateResults.setMaxIndex((long) -1);
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("faceted search show records check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithWithoutMaxFilterIndexandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMinIndex((long) 1);
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("faceted search show records check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithWithoutMinFilterIndexandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMaxIndex((long) 1);
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("faceted search show records check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithWithoutMinFilterIndexandInvalidScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMinIndex((long) 1);
            paginateResults.setMaxIndex((long) 1);
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithInvalidShowRecordsFilterandScoringModeReturnsfailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setShowRecords("test");
            additionalFilters.setPaginateResults(paginateResults);

            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("faceted search show records check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search resume with facets
     */
    @Test
    public final void searchResumeWithValidCustomXpathReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = testHelper.registerResume(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Writing");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.getCustomXpath().add("employer");
            searchCriteria.setAdditionalFilters(additionalFilters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"resume\" vendor=\"resvendor02\"><posting/><facets><![CDATA[facets://skills=[Writing]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include var=\"xpath://employer\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());
                Assert.assertTrue("SearchResult Count Check", searchResult.getCandidates().get(0).getCustomXpathData().get(0).getXpathData() != null);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterResume(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
