// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * Parse resume HRXML test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ParseResumeHRXMLTests {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Autowired
    IUtilityService utilityService;

    /**
     * API context
     */
    public ApiContext apiContext;

    /**
     * API
     */
    public Api api;

    /**
     * Lens settings
     */
    public LensSettings en_usLensSettings, en_ukLensSettings, localeSettings;

    /**
     * Resource
     */
    public Resources resource;

    /**
     * Client
     */
    public Client client;

    /**
     * Core client
     */
    private CoreClient coreClient;

    /**
     * Test helper object
     */
    public TestHelper testHelper;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private String binaryData;

    /**
     * Resume parsing variant
     */
    public static final String variant = "hrxml";

    /**
     * Instance type
     */
    public static String InstanceType = "XRay";

    /**
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws JAXBException
     */
    @Before
    public void setUp() throws JAXBException {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        try {
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            InstanceType = testHelper.instanceType;
            // create api
            CreateApiRequest createApi = testHelper.getCreateApiRequest("/input/CreateApi.xml");
            //CreateApiRequest createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(ParseResumeHRXMLTests.class.getResourceAsStream("/input/CreateApi.xml"));
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ParseResumeHRXMLTests.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setLanguage(testHelper.language);
            settingsRequest.setCountry(testHelper.country);
            settingsRequest.setHRXMLContent(testHelper.hrxmlcontent);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;

            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // create resource
            AddResourceRequest resourceRequest = testHelper.getAddResourceRequest("/input/CreateResource.xml");
            //AddResourceRequest resourceRequest = (AddResourceRequest) jaxbUnmarshaller.unmarshal(ParseResumeHRXMLTests.class.getResourceAsStream("/input/CreateResource.xml"));
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            //CreateConsumerRequest consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(ParseResumeHRXMLTests.class.getResourceAsStream("/input/CreateConsumer.xml"));

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();

            settingsList.getInstanceList().clear();

            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_usLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_ukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);

            consumer.setLensSettings(settingsList);
            consumer = testHelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;

            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);

            // en_us LensSettings
            CoreLenssettings en_usCoreLenssettings = new CoreLenssettings();
            en_usCoreLenssettings.setId(1);
            en_usCoreLenssettings.setHost(testHelper.hostname);
            en_usCoreLenssettings.setPort(testHelper.port);
            en_usCoreLenssettings.setCharacterSet(testHelper.encoding);
            en_usCoreLenssettings.setTimeout(testHelper.timeOut);
            en_usCoreLenssettings.setInstanceType(testHelper.instanceType);
            en_usCoreLenssettings.setLocale(testHelper.locale);
            en_usCoreLenssettings.setStatus(testHelper.status);
            en_usCoreLenssettings.setLanguage(testHelper.language);
            en_usCoreLenssettings.setCountry(testHelper.country);
            en_usCoreLenssettings.setHrxmlcontent(testHelper.hrxmlcontent);

            // en_uk LensSettings
            CoreLenssettings en_ukCoreLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            en_ukCoreLenssettings.setId(2);
            en_ukCoreLenssettings.setLocale(LocaleTypeEn_uk);

            // en_us LensSettings
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(3);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);

            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();

            en_usCoreClientlenssettings.setCoreLenssettings(en_usCoreLenssettings);
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(en_ukCoreLenssettings);
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);
            coreClientlenssettingses.add(locale_CoreClientlenssettings);

            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);

            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            //coreClientapis.add(clientapis.get(0));
            //coreClient.setCoreClientapis(coreClientapis);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("Post");
            apiContext.setResource("resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = ParseResumeHRXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeHRXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete", deleteApiResponse.status, true);
            // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete", deleteConsumerResponse.status, true);
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    
    @Test
    public void parseResumeHRXMLWithValidBinaryResumeDataWithLocalewithMicroservice() {
        try {
            // ParseResumeHRXML
            
            String fileName = "/TestResumes/BoldResume11.pdf";
            InputStream resourceAsStream04 = ParseResumeBGTXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariantMicroservice(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);
            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    
    /**
     * Parse Resume HRXML With ValidBinary ResumeData Return Success
     */
    @Test
    public void parseResumeHRXMLWithValidBinaryResumeDataReturnSuccess() {
        try {
            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeHRXMLWithValidBinaryResumeDataWithDefaultLocale() {
        try {
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry(TestHelper.Locale.en_us.name());
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeHRXMLWithValidBinaryResumeDataLocaleBasedParsing() {
        try {
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "en_us", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeHRXMLWithValidBinaryResumeDataLocaleBasedParsingWithDefaultCountry() {
        try {
            String fileName = "/TestResumes/16343.doc";
            InputStream resourceAsStream04 = ParseResumeHRXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "en_gb", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeHRXMLWithValidBinaryResumeDataLocaleBasedParsingWithUnknownLocale() {
        try {
            String fileName = "/TestResumes/UnknownLocale.doc";
            InputStream resourceAsStream04 = ParseResumeHRXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "xl_xx", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeHRXMLWithValidBinaryResumeDataLocaleBasedParsingWithUnallocatedLocale() {
        try {
            String fileName = "/TestResumes/Resume_JPN.txt";
            InputStream resourceAsStream04 = ParseResumeHRXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "ja_jp", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeHRXMLWithValidBinaryResumeDataLocaleBasedParsingWithInactiveInstance() {
        try {

            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);

            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());
            coreClientApiContext.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClientApiContext.setEnableLocaleDetection(true);
            apiContext.setClient(coreClientApiContext);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With InActive Lens check", response.status, false);
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Parse Resume HRXML With ConvertError Return Failure
     */
    @Test
    public void parseResumeHRXMLWithConvertErrorReturnFailure() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            InputStream resourceAsStream04 = ParseResumeHRXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeHRXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With Empty BinaryData check", response.status, false);

            // ParseResumeHRXML Assert            
            String convertResume = (String) response.responseData;
            String expResult = "Converters returned empty output";
            Assert.assertEquals("ParseResumeHRXML With convert error Check", apiErrors.getLensErrorMessage(expResult), convertResume);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With Empty BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Parse Resume HRXML With ConvertError Status Return Failure
     */
    @Test
    public void parseResumeHRXMLWithConvertErrorStatusReturnFailure() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResumeHRXML With Null BinaryData check", response.status, false);

            // ParseResumeHRXML Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With Null BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Parse Resume HRXML Specific LensInstance Return Success
     */
    @Test
    public void parseResumeHRXMLSpecificLensInstanceReturnSuccess() {
        try {
            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML Specific Lens Instance check", response.status, true);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Parse Resume HRXML Unauthorized Instance Return Failure
     */
    @Test
    public void parseResumeHRXMLUnauthorizedInstanceReturnFailure() {
        try {
            // ParseResumeHRXML
            String localeType = "en_au";
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(localeType);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With Unauthorized Instance check", response.status, false);

            // ParseResumeHRXML en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Parse Resume HRXML Empty Instance List Return Failure
     */
    @Test
    public void parseResumeHRXMLEmptyInstanceListReturnFailure() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResumeHRXML With Empty Instance List check", response.status, false);

            // ParseResumeHRXML en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Parse Resume HRXML Null Instance List Return Failure
     */
    @Test
    public void parseResumeHRXMLNullInstanceListReturnFailure() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResumeHRXML With Null Instance List check", response.status, false);

            // ParseResumeHRXML en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * parse Resume HRXML Invalid Lens List Return Failure
     */
    @Test
    public void parseResumeHRXMLInvalidLensListReturnFailure() {
        try {
            //Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With Lens Invalid  check", response.status, false);

            // ParseResumeHRXML en_us Assert
            String instanceResponse = (String) response.responseData;
            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Parse Resume HRXML InActive Lens List Return Failure
     */
    @Test
    public void parseResumeHRXMLInActiveLensListReturnFailure() {
        UpdateConsumerRequest updaterequest = new UpdateConsumerRequest();
        ApiResponse updateResponse = new ApiResponse();
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With InActive Lens check", response.status, false);

            // ParseResumeHRXML inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            Assert.assertEquals("ParseResumeHRXML With InActive Lens check", _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS), instanceResponse);
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Parse Resume HRXML With Null HRXML Content Return Failure
     *
     * @throws java.lang.Exception
     */
    @Test
    public void parseResumeHRXMLWithNullHRXMLContentReturnFailure() throws Exception {
        int deleteId = 0;
        UpdateConsumerRequest updaterequest = new UpdateConsumerRequest();
        ApiResponse updateResponse = new ApiResponse();
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(true);
            settingsRequest.setVersion(testHelper.version);
            settingsRequest.setHRXMLContent(null);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;
            deleteId = settings.getId();

            // Update the client
            updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            if (ApiException.class.isInstance(ex)) {
                ApiException apiExp = (ApiException) ex;
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_HRXML_DATA), apiExp.getMessage());
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", apiExp.getStatusCode(), HttpStatus.METHOD_NOT_ALLOWED);
                updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
                updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
                updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
                updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
                ApiResponse response = coreService.deleteLensSettings(deleteId);
                Assert.assertEquals("Create Lens settings delete", response.status, true);
            } else {
                System.out.println(ex);
                Assert.fail();
            }
        }
    }

    /**
     * Parse Resume HRXML With Null Empty Content Return Failure
     *
     * @throws java.lang.Exception
     */
    @Test
    public void parseResumeHRXMLWithEmptyHRXMLContentReturnFailure() throws Exception {
        int deleteId = 0;
        UpdateConsumerRequest updaterequest = new UpdateConsumerRequest();
        ApiResponse updateResponse = new ApiResponse();
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(true);
            settingsRequest.setVersion(testHelper.version);
            settingsRequest.setHRXMLContent("");
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;
            deleteId = settings.getId();

            // Update the client
            updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            if (ApiException.class.isInstance(ex)) {
                ApiException apiExp = (ApiException) ex;
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_HRXML_DATA), apiExp.getMessage());
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", apiExp.getStatusCode(), HttpStatus.METHOD_NOT_ALLOWED);

                updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
                updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
                updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
                updateResponse = coreService.updateConsumer(client.getId(), updaterequest);

                ApiResponse response = coreService.deleteLensSettings(deleteId);
                Assert.assertEquals("Create Lens settings delete", response.status, true);
            } else {
                System.out.println(ex);
                Assert.fail();
            }
        }
    }

    /**
     * Parse Resume HRXML With Null Invalid Content Return Failure
     *
     * @throws java.lang.Exception
     */
    @Test
    public void parseResumeHRXMLWithInvalidHRXMLContentReturnFailure() throws Exception {
        int deleteId = 0;
        UpdateConsumerRequest updaterequest = new UpdateConsumerRequest();
        ApiResponse updateResponse = new ApiResponse();
        try {

            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(true);
            settingsRequest.setVersion(testHelper.version);
            settingsRequest.setHRXMLContent("Invalid");
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;
            deleteId = settings.getId();

            // Update the client
            updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            if (ApiException.class.isInstance(ex)) {
                ApiException apiExp = (ApiException) ex;
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", _helper.getErrorMessageWithURL(ApiErrors.STYLESHEET_TRANSFORMATION_ERROR), apiExp.getMessage());
                Assert.assertEquals("Lens TagBinaryDataHRXML Status Check", apiExp.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
                updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
                updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
                updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
                updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
                ApiResponse response = coreService.deleteLensSettings(deleteId);
                Assert.assertEquals("Create Lens settings delete", response.status, true);

            } else {
                System.out.println(ex);
                Assert.fail();
            }
        }
    }

    // HRXML Object model
    /**
     * parseResumeHRXMLObjectModelAustraliaWithValidBinaryResumeDataReturnSuccess
     */
    @Test
    public void parseResumeHRXMLObjectModel_en_au_WithValidBinaryResumeDataReturnSuccess() {
        int deleteId = 0;
        try {

            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_au");
            settingsRequest.setCountry("Australia-hrxml");
            settingsRequest.setStatus(true);
            settingsRequest.setHRXMLVersion("2.5");
            settingsRequest.setVersion(testHelper.version);
            settingsRequest.setHRXMLContent(IOUtils.toString(TestHelper.class.getResourceAsStream("/XSL/Resume_SEP_HRXML_eng-AUS_2_7.xsl")));
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;
            deleteId = settings.getId();

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClientApiContext);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale("en_au");
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(deleteId);
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * parseResumeHRXMLObjectModelAustraliaWithValidBinaryResumeDataReturnSuccess
     */
    @Test
    public void parseResumeHRXMLObjectModel_en_nz_WithValidBinaryResumeDataReturnSuccess() {
        int deleteId = 0;
        try {

            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_nz");
            settingsRequest.setCountry("New Zealand-hrxml");
            settingsRequest.setStatus(true);
            settingsRequest.setHRXMLVersion("2.5");
            settingsRequest.setVersion(testHelper.version);
            settingsRequest.setHRXMLContent(IOUtils.toString(TestHelper.class.getResourceAsStream("/XSL/Resume_SEP_HRXML_eng-NZL_2_7.xsl")));
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;
            deleteId = settings.getId();

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClientApiContext);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale("en_nz");
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(deleteId);
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * parseResumeHRXMLObjectModelAustraliaWithValidBinaryResumeDataReturnSuccess
     */
    @Test
    public void parseResumeHRXMLObjectModel_en_gb_WithValidBinaryResumeDataReturnSuccess() {
        int deleteId = 0;
        try {

            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_gb");
            settingsRequest.setHRXMLVersion("2.5");
            settingsRequest.setCountry("Great Britain-hrxml-fc");
            settingsRequest.setStatus(true);
            settingsRequest.setVersion(testHelper.version);
            settingsRequest.setHRXMLContent(IOUtils.toString(TestHelper.class.getResourceAsStream("/XSL/Resume_SEP_HRXML_eng-NZL_2_7.xsl")));
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;
            deleteId = settings.getId();

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClientApiContext);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeHRXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale("en_gb");
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeHRXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeHRXML Assert
            //String expResult = "Candidate";
            String lensResponseMessage = (String) response.responseData;
            //boolean checkContains = lensResponseMessage.contains(expResult);
            Assert.assertNotNull(lensResponseMessage);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeHRXML Transaction Count Assert
            Assert.assertEquals("ParseResumeHRXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(deleteId);
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
