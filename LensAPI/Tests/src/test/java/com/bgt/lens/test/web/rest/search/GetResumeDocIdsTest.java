// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.adminservice.request.SearchSettings.Resume;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.SearchResult;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.rest.response.searchservice.GetResumeDocIdsResponse;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.RegisterResumeRequest;
import com.bgt.lens.model.searchservice.request.UnregisterResumeRequest;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Get resume document ids with authentication test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class GetResumeDocIdsTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor adminVendor;
    /**
     * External client vendor
     */
    private Vendor externalClientVendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Admin client
     */
    private CoreClient adminClient;
    /**
     * ExternalClient
     */
    private CoreClient externalClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.SpectrumGetdocids_Hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.SpectrumGetdocids_Port)));
            settingsRequest.setCharSet(testHelper.SpectrumGetdocids_Encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.SpectrumGetdocids_TimeOut)));
            settingsRequest.setInstanceType(testHelper.SpectrumGetdocids_InstanceType);
            settingsRequest.setLocale(testHelper.SpectrumGetdocids_Locale);
            settingsRequest.setStatus(testHelper.SpectrumGetdocids_Status);

            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");

            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            
            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor02");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            adminVendor = (Vendor) response.responseData;

            createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0202");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            externalClientVendor = (Vendor) response.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            
            consumer.setLensSettings(settingsList);

            SearchSettings searchSettings = new SearchSettings();
            searchSettings.getResume().clear();
            Resume resume = new Resume();
            resume.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            resume.setVendorId(BigInteger.valueOf(adminVendor.getVendorId()));
            searchSettings.getResume().add(resume);

            String searchSettingsJson = helper.getJsonStringFromObject(searchSettings);

            CreateConsumerRequest.VendorSettings settings = new CreateConsumerRequest.VendorSettings();
            settings.getVendorIdList().add(BigInteger.valueOf(adminVendor.getVendorId()));
            consumer.setVendorSettings(settings);
            consumer.setSearchSettings(searchSettings);

            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
//            consumer.getVendorSettings().getVendorIdList().clear();
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);

            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);

            adminClient = dbService.getClientById(client.getId());
            adminClient.setTier(1);

            CoreClient updatedAdminClient = adminClient;
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings searchVendorSettings = dbService.getVendorById(adminVendor.getVendorId());
            searchVendorSettingsSet.add(searchVendorSettings);
            updatedAdminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            updatedAdminClient.setSearchSettings(searchSettingsJson);
            ApiResponse updateClientResponse = dbService.updateClient(adminClient, updatedAdminClient);
            Assert.assertTrue("Update client check", updateClientResponse.status);

            //externalClient = consumer; 
            searchVendorSettingsSet = new HashSet<>();
            searchVendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(searchVendorSettings);
            searchSettings = new SearchSettings();
            searchSettings.getResume().clear();
            resume = new Resume();
            resume.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            resume.setVendorId(BigInteger.valueOf(externalClientVendor.getVendorId()));
            searchSettings.getResume().add(resume);
            consumer.setSearchSettings(searchSettings);
            consumer.setKey("Test01");
            consumer.setName("test01");
            consumer.setTier(BigInteger.valueOf(2));
            CreateConsumerRequest.VendorSettings vendorSettingsList
                    = consumer.getVendorSettings();
            vendorSettingsList.getVendorIdList().add(new BigInteger(
                    Integer.toString(externalClientVendor.getVendorId())));
            ApiResponse externalClientCreateresponse = coreService.createConsumer(consumer);
            Assert.assertEquals("Create external consumer check", externalClientCreateresponse.status, true);

            Client external = (Client) externalClientCreateresponse.responseData;

            externalClient = dbService
                    .getClientById(external.getId());
            externalClient.setTier(2);

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
            // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);
             ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if (clientList != null && clientList.size() > 0) {
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister resume request", deleteExternalClientResponse.status);
            }

            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get resume document ids .
     */
    @Test
    public final void getResumeDocumentIdsAsAdmin() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "2").param("vendor", adminVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetResumeDocIdsResponse getDocIdsResumeResponse = (GetResumeDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetResumeDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;
            
            if (getDocIdsResumeResponse != null && getDocIdsResumeResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getDocIdsResumeResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get resume document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get resume document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='resume' name='resvendor02' count='2' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

        /**
     * Get all the resume document ids.
     */
    @Test
    public final void getResumeDocumentIdsAsClient() {
        try {
            apiContext.setClient(externalClient);
//            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "2").param("vendor", externalClientVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetResumeDocIdsResponse getDocIdsResumeResponse = (GetResumeDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetResumeDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;
            
            if (getDocIdsResumeResponse != null && getDocIdsResumeResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getDocIdsResumeResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(externalClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get resume document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get resume document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='resume' name='resvendor0202' count='2' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Get all the resume document ids with last document id.
     */
    @Test
    public final void getResumeDocumentIdsAsAdminWithLastDocIds() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "1").param("vendor", adminVendor.getVendor()).param("lastDocId", "1"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetResumeDocIdsResponse getDocIdsResumeResponse = (GetResumeDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetResumeDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;
            
            if (getDocIdsResumeResponse != null && getDocIdsResumeResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getDocIdsResumeResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get resume document ids  with lastDocIds command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get resume document ids with lastDocIds command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='resume' name='resvendor02' count='1' lastdocid='1' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Get all the resume document ids with last document id.
     */
    @Test
    public final void getResumeDocumentIdsAsClientWithLastDocIds() {
        try {
            apiContext.setClient(externalClient);
//            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
             ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "1").param("vendor", externalClientVendor.getVendor()).param("lastDocId", "1"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetResumeDocIdsResponse getDocIdsResumeResponse = (GetResumeDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetResumeDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;
            
            if (getDocIdsResumeResponse != null && getDocIdsResumeResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getDocIdsResumeResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(externalClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get resume document ids  with lastDocIds command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get resume document ids with lastDocIds command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='resume' name='resvendor0202' count='1' lastdocid='1' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Get resume document ids with invalid vendor.
     */
    @Test
    public final void getResumeDocumentIdsAsAdminWithInvalidVendor() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("vendor", "test"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

     /**
     * Get resume document ids with invalid vendor.
     */
    @Test
    public final void getResumeDocumentIdsAsClientWithInvalidVendor() {
        try {
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("vendor", "vendor"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get resume document ids with invalid locale.
     */
    @Test
    public final void getResumeDocumentIdsAsAdminWithInvalidLocale() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", "test").param("vendor", adminVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Get resume document ids with invalid locale.
     */
    @Test
    public final void getResumeDocumentIdsAsClientWithInvalidLocale() {
        try {
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.FetchResume + "/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", "test").param("vendor", externalClientVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get resume document ids with invalid instance type.
     */
    @Test
    public final void getResumeDocumentIdsAsAdminWithInvalidInstanceType() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            ApiResponse response = new ApiResponse();
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("instanceType", "test").param("locale", TestHelper.Locale.en_us.name()).param("vendor", adminVendor.getVendor()))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Get resume document ids with invalid instance type check", response.status);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Get resume document ids with invalid instance type.
     */
    @Test
    public final void getResumeDocumentIdsAsClientWithInvalidInstanceType() {
        try {
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            ApiResponse response = new ApiResponse();
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("instanceType", "test").param("locale", TestHelper.Locale.en_us.name()).param("vendor", externalClientVendor.getVendor()))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Get resume document ids withinvalid instance type check", response.status);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
        /**
     * Get resume document ids with count greater than 10000.
     */
    @Test
    public final void getResumeDocumentIdsAsAdminWithCountGreaterThan10000() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "10001").param("vendor", adminVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetResumeDocIdsResponse getDocIdsResumeResponse = (GetResumeDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetResumeDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;
            
            if (getDocIdsResumeResponse != null && getDocIdsResumeResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getDocIdsResumeResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get resume document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get resume document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='resume' name='resvendor02' count='10000' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get all the resume document ids with count greater than 10000.
     */
    @Test
    public final void getResumeDocumentIdsAsClientWithCountGreaterThan10000() {
        try {
            apiContext.setClient(externalClient);
//            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterResume(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterResume(searchService, "2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetResumeDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "10001").param("vendor", externalClientVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetResumeDocIdsResponse getDocIdsResumeResponse = (GetResumeDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetResumeDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;
            
            if (getDocIdsResumeResponse != null && getDocIdsResumeResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getDocIdsResumeResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(externalClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get resume document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get resume document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='resume' name='resvendor0202' count='10000' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    
    /**
     * unregisterResume
     *
     * @param resumeId
     * @param instanceType
     * @param vendorName
     * @param locale
     * @return
     * @throws Exception
     */
    public ApiResponse unregisterResume(String resumeId, String instanceType, String vendorName, String locale) throws Exception {
        UnregisterResumeRequest unregisterRequest = new UnregisterResumeRequest();
        unregisterRequest.setResumeId(resumeId);
        unregisterRequest.setVendor(vendorName);
        unregisterRequest.setInstanceType(instanceType);
        unregisterRequest.setLocale(locale);

        ApiResponse unregisterResponse = searchService.unregisterResume(unregisterRequest);
        return unregisterResponse;
    }

    /**
     * Register resume
     *
     * @param instanceType
     * @param locale
     * @param binaryData
     * @param resumeId
     * @param vendorName
     * @param client
     * @return
     */
    public ApiResponse registerResume(String instanceType, String locale, String binaryData, String resumeId, String vendorName, CoreClient client) throws Exception {
        RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
        registerResumeRequest.setInstanceType(instanceType);
        registerResumeRequest.setLocale(locale);
        registerResumeRequest.setBinaryData(binaryData);
        registerResumeRequest.setResumeId(resumeId);
        registerResumeRequest.setVendor(vendorName);
        registerResumeRequest.setErrorOnDuplicateorOverwrite(com.bgt.lens.helpers.Enum.registerDuplicate.overwrite.toString());
        apiContext.setClient(client);
        helper.setApiContext(apiContext);
        ApiResponse registerResumeResponse = searchService.registerResume(registerResumeRequest);
        return registerResumeResponse;
    }
}
