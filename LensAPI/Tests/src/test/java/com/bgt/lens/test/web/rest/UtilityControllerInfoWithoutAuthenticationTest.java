// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.UriConstants;
import com.bgt.lens.web.rest.UtilityController;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.containsString;
import org.springframework.beans.factory.annotation.Value;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.context.WebApplicationContext;

/**
 * Utility controller info without authentication test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class UtilityControllerInfoWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    /**
     * Instance type.
     */
    private final String instanceType = "SPECTRUM";
    /**
     * Locale US.
     */
    private final String localeTypeEnus = "en_us";
    /**
     * Locale UK.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Invalid instance type.
     */
    private final String invalidType = "Invalid Instance Type";
    /**
     * Info success.
     */
    private final String infoSuccess = "Services are available";
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface Utility Service.
     */
    @Autowired
    private IUtilityService utilityService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Utility Controller object.
     */
    private final UtilityController utilityController = new UtilityController();

    /**
     * Setup class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup.
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
           servletContext.setAttribute("portalErrorURL", portalErrorURL);

        mockMvc = MockMvcBuilders.
                standaloneSetup(utilityController).build();
        ReflectionTestUtils
                .setField(utilityController,
                        "utilityService",
                        utilityService);

        try {
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);

            // create lens settings
            JAXBContext jaxbContext = JAXBContext
                    .newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest
                    = new AddLensSettingsRequest();
            InputStream resourceAsStream01
                    = UtilityControllerInfoWithoutAuthenticationTest.class
                    .getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest
                            = (AddLensSettingsRequest) jaxbUnmarshaller
                            .unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("GET");
            apiContext.setResource("Status");
            apiContext.setClient(null);
            apiContext.setAdvanceLog(new AdvanceLog());

        } catch (IOException ex) {
            System.out.print(ex);
            Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(
                    UtilityControllerInfoWithoutAuthenticationTest.class
                    .getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Tear down.
     */
    @After
    public final void tearDown() {
        try {
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * get UtilityController Info Without Authentication DefaultLens.
     */
    @Test
    public final void
            getUtilityControllerInfoWithoutAuthenticationDefaultLens() {
        try {
            mockMvc.perform(get(UriConstants.Info)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    })
                    .param("type", instanceType))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData.locale",
                                    is(localeTypeEnus)))
                    .andExpect(jsonPath("$.responseData.status",
                                    is(infoSuccess)))
                    .andDo(print());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * get UtilityController Info Without Authentication Specific Instance.
     */
    @Test
    public final void
            getUtilityControllerInfoWithoutAuthenticationSpecificInstance() {
        try {
            mockMvc.perform(get(UriConstants.Info)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    })
                    .param("type", instanceType).param("locale",
                            localeTypeEnuk))
                    .andExpect(content().contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData.locale",
                                    is(localeTypeEnuk)))
                    .andExpect(jsonPath("$.responseData.status",
                                    is(infoSuccess)))
                    .andDo(print());

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * get UtilityController Info Without Authentication UnauthorizedInstance.
     */
    @Test
    public final void
            utilityControllerInfoWithoutAuthenticationUnauthorizedInstance() {
        try {
            String localeType = "en_anz";
            mockMvc.perform(get(UriConstants.Info)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    })
                    .param("type", instanceType).param("locale", localeType))
                    .andExpect(content().contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE))))
                    .andDo(print());

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Utility Controller Info Without Authentication Invalid HostName List.
     */
    @Test
    public final void
            getUtilityControllerInfoWithoutAuthenticationInvalidHostNameList() {
        try {
            // create lens settings
            JAXBContext jaxbContext = JAXBContext
                    .newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest
                    = new AddLensSettingsRequest();
            InputStream resourceAsStream01
                    = UtilityControllerInfoWithoutAuthenticationTest.class
                    .getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest
                            = (AddLensSettingsRequest) jaxbUnmarshaller
                            .unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse ensgpLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    ensgpLensSettingsResponse.status, true);
            LensSettings ensgpLensSettings
                    = (LensSettings) ensgpLensSettingsResponse.responseData;

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setAdvanceLog(new AdvanceLog());

            LensRepository repository = new LensRepository();
            String errorMessage = repository.lensErrorMessage;

            mockMvc.perform(get(UriConstants.Info)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    })
                    .param("type", instanceType)
                    .param("locale", "en_sgp"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData.locale",
                                    is("en_sgp")))
                    .andExpect(jsonPath("$.responseData.status",
                                    containsString(_helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE))))
                    .andDo(print());

            // Delete LensSetting
            ApiResponse ensgpDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(ensgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    ensgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get UtilityController Info Without Authentication InActive LensList.
     */
    @Test
    public final void
            getUtilityControllerInfoWithoutAuthenticationInActiveLensList() {
        try {
            // create lens settings
            JAXBContext jaxbContext = JAXBContext
                    .newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest
                    = new AddLensSettingsRequest();
            InputStream resourceAsStream01
                    = UtilityControllerInfoWithoutAuthenticationTest.class
                    .getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest
                            = (AddLensSettingsRequest) jaxbUnmarshaller
                            .unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(false);
            //en_us
            ApiResponse ensgpLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    ensgpLensSettingsResponse.status, true);
            LensSettings ensgpLensSettings
                    = (LensSettings) ensgpLensSettingsResponse.responseData;

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setAdvanceLog(new AdvanceLog());

            mockMvc.perform(get(UriConstants.Info)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    })
                    .param("type", instanceType)
                    .param("locale", "en_sgp"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS))))
                    .andDo(print());

            // Delete LensSetting
            ApiResponse ensgpDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(ensgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    ensgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
