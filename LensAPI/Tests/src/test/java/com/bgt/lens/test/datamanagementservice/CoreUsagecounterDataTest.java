// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.datamanagementservice;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.entity.CoreUsagecounter;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Usage counter data test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CoreUsagecounterDataTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    @Autowired
    IDataRepositoryService dbService;

    /**
     * Usage counter
     */
    private CoreUsagecounter coreUsageCounter = null;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        coreUsageCounter = new CoreUsagecounter();
        coreUsageCounter.setClientId(10138);
        coreUsageCounter.setApiId(25491);
        coreUsageCounter.setMethod("POST");
        coreUsageCounter.setResource("ResumeParser");
        coreUsageCounter.setUsageCount(26);
        coreUsageCounter.setCreatedOn(Date.from(Instant.now()));
        coreUsageCounter.setUpdatedOn(Date.from(Instant.now()));
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * addUsagecounterWithValidData_DB
     */
    @Test
    public void addUsagecounterWithValidData_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            CoreUsagecounter getCoreUsageCounter = dbService.getUsagecounterById(coreUsageCounter.getId());
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter status check", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * addUsagecounterWithoutMethod_DB
     */
    @Test
    public void addUsagecounterWithoutMethod_DB() {
        try {
            // Add
            coreUsageCounter.setMethod(null);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagecounter(coreUsageCounter);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter without Method check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagecounterWithoutResource_DB
     */
    @Test
    public void addUsagecounterWithoutResource_DB() {
        try {
            // Add
            coreUsageCounter.setResource(null);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagecounter(coreUsageCounter);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter without Resource check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagecounterInvalidClientId_DB
     */
    @Test
    public void addUsagecounterInvalidClientId_DB() {
        try {
            // Add
            coreUsageCounter.setClientId(-1);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagecounter(coreUsageCounter);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter Invalid ClientId check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagecounterInvalidApiId_DB
     */
    @Test
    public void addUsagecounterInvalidApiId_DB() {
        try {
            // Add
            coreUsageCounter.setApiId(-1);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagecounter(coreUsageCounter);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter Invalid ApiId check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * updateUsagecounterWithValidData_DB
     */
    @Test
    public void updateUsagecounterWithValidData_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Update
            coreUsageCounter.setUsageCount(coreUsageCounter.getUsageCount() + 1);

            ApiResponse updateApiResponse = dbService.updateUsagecounter(coreUsageCounter);
            // Update Assert
            Assert.assertEquals("DB: update UsageCounter status check", updateApiResponse.status, true);
            Assert.assertEquals("DB: Get Usage Counter check", 27, coreUsageCounter.getUsageCount());

            // Get
            CoreUsagecounter getCoreUsageCounter = dbService.getUsagecounterById(coreUsageCounter.getId());
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter status check", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * GetUsagecounterbyIdWithValidId_DB
     */
    // GetById
    @Test
    public void getUsagecounterbyIdWithValidId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            CoreUsagecounter getCoreUsageCounter = dbService.getUsagecounterById(coreUsageCounter.getId());
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter By Valid id status check", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * GetUsagecounterbyIdWithInvalidId_DB
     */
    @Test
    public void getUsagecounterbyIdWithInvalidId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            CoreUsagecounter getCoreUsageCounter = dbService.getUsagecounterById(0);
            // Get Assert
            Assert.assertNull("DB: Get UsageCounter By Invalid Id status check", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyIdWithValidClientId_DB
     */
    @Test
    public void getUsagecounterbyIdWithValidClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            CoreUsagecounter getCoreUsageCounter = dbService.getUsagecounterById(coreUsageCounter.getId(), coreUsageCounter.getClientId());
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter By Valid Client id status check", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyIdWithInvalidClientId_DB
     */
    @Test
    public void getUsagecounterbyIdWithInvalidClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            CoreUsagecounter getCoreUsageCounter = dbService.getUsagecounterById(coreUsageCounter.getId(), 123);
            // Get Assert
            Assert.assertNull("DB: Get UsageCounter By By Invalid Client id status check", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetListofAllUsagecounter
     */
    // GetByCriteria
    @Test
    public void getListofAllUsagecounter_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter by Criteria", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithClientId_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            usagecounterCriteria.setClientId(coreUsageCounter.getClientId());
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter by Criteria Valid Client Id", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithInvalidClientId_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithInvalidClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            usagecounterCriteria.setClientId(123);
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageCounter by Criteria Invalid Client Id", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithApiId_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithApiId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            usagecounterCriteria.setApiId(coreUsageCounter.getApiId());
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter by Criteria Valid Api Id", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithInvalidApiId_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithInvalidApiId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            usagecounterCriteria.setApiId(123);
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageCounter by Criteria Invalid Api Id", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithHttpMethod_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithHttpMethod_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            usagecounterCriteria.setMethod(coreUsageCounter.getMethod());
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter by Criteria HTTP Method", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * GetUsagecounterbyCriteriaWithInvalidHttpMethod_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithInvalidHttpMethod_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            usagecounterCriteria.setMethod("Invalid");
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageCounter by Criteria Invalid HTTP Method", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithResource_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithResource_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            usagecounterCriteria.setResource(coreUsageCounter.getResource());
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter by Criteria Resource", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithInvalidResource_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithInvalidResource_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            usagecounterCriteria.setResource("Invalid");
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageCounter by Criteria Invalid Resource", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithInTime_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithInTime_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();

            //DateFormat df = _helper.getDateFormat();
            //Date dateFrom = df.parse(df.format(Date.from(Instant.now())));
            //usagecounterCriteria.setFromDate(dateFrom);
            DateTimeFormatter df = _helper.getDateFormat();
            Date dateFrom = df.parseLocalDate(new LocalDate().toString()).toDateTimeAtStartOfDay().toDate();
            usagecounterCriteria.setFromDate(dateFrom);

            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter by Criteria From Date", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagecounterDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithInvalidInTime_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithInvalidInTime_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();

            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, 2); // Adding 1 day
            //Date dateFrom = df.parse(df.format(c.getTime()));
            DateTimeFormatter df = _helper.getDateFormat();
            Date dateFrom = df.parseLocalDate(new LocalDate().plusDays(2).toString()).toDateTimeAtStartOfDay().toDate();
            usagecounterCriteria.setFromDate(dateFrom);

            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageCounter by Criteria Invalid From Date", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagecounterDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithOutTime_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithOutTime_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();
            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //Date dateTo = df.parse(df.format(c.getTime()));
            //usagecounterCriteria.setToDate(dateTo);

            DateTimeFormatter df = _helper.getDateFormat();
            Date dateTo = df.parseLocalDate(new LocalDate().plusDays(1).toString()).toDateTimeAtStartOfDay().toDate();
            usagecounterCriteria.setToDate(dateTo);

            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter by Criteria To Date", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagecounterDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithInvalidOutTime_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithInvalidOutTime_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();

            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, -1); // Adding 1 day
            //Date dateTo = df.parse(df.format(c.getTime()));
            //usagecounterCriteria.setToDate(dateTo);
            DateTimeFormatter df = _helper.getDateFormat();
            Date dateTo = df.parseLocalDate(new LocalDate().plusDays(-1).toString()).toDateTimeAtStartOfDay().toDate();
            usagecounterCriteria.setToDate(dateTo);

            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageCounter by Criteria Invalid To Date", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagecounterDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithInAndOutTime_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithInAndOutTime_DB() {
        try {
            // Add
            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //Date outTime = df.parse(df.format(c.getTime()));

            DateTimeFormatter df = _helper.getDateFormat();
            Date outTime = df.parseLocalDate(new LocalDate().plusDays(1).toString()).toDateTimeAtStartOfDay().toDate();

            // Add
            coreUsageCounter.setCreatedOn(outTime);
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert
            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);

            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();

            Date today = df.parseLocalDate(new LocalDate().toString()).toDateTimeAtStartOfDay().toDate();

            usagecounterCriteria.setFromDate(today);
            usagecounterCriteria.setToDate(outTime);
            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert
            Assert.assertNotNull("DB: Get UsageCounter by From & To Resource", getCoreUsageCounter);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagecounterDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsagecounterbyCriteriaWithInvalidInAndOutTime_DB
     */
    @Test
    public void getUsagecounterbyCriteriaWithInvalidInAndOutTime_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagecounter(coreUsageCounter);
            // Add Assert

            Assert.assertEquals("DB: Add UsageCounter status check", addApiResponse.status, true);
            System.out.println("1");
            // Get
            UsagecounterCriteria usagecounterCriteria = new UsagecounterCriteria();

            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, -1); // Adding 1 day
            //Date outTime = df.parse(df.format(c.getTime()));
            DateTimeFormatter df = _helper.getDateFormat();
            Date outTime = df.parseLocalDate(new LocalDate().plusDays(-1).toString()).toDateTimeAtStartOfDay().toDate();

            Date today = df.parseLocalDate(new LocalDate().toString()).toDateTimeAtStartOfDay().toDate();
            usagecounterCriteria.setFromDate(today);
            usagecounterCriteria.setToDate(outTime);

            List<CoreUsagecounter> getCoreUsageCounter = dbService.getUsagecounterByCriteria(usagecounterCriteria);
            // Get Assert

            Assert.assertNull("DB: Get UsageCounter by From & To Resource", getCoreUsageCounter);
            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsageCounter);
            // Delete Assert
            Assert.assertEquals("DB: Delete UsageCounter status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagecounterDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

}
