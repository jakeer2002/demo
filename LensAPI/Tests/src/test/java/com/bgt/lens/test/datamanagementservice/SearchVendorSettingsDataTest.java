// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.datamanagementservice;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * Search vendor settings data test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class SearchVendorSettingsDataTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    /**
     * Usage log
     */
    private SearchVendorsettings searchVendorSettings;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * *
     * Lens settings response
     */
    public ApiResponse lensSettingsResponse;

    /**
     * *
     * Lens settings
     */
    public LensSettings settings;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() throws JAXBException, IOException, Exception {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        searchVendorSettings = new SearchVendorsettings();
        //Add Lens settings
        AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
        lensSettingsResponse = coreService.addLensSettings(settingsRequest);
        Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
        settings = (LensSettings) lensSettingsResponse.responseData;
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() throws Exception {
        settings = (LensSettings) lensSettingsResponse.responseData;
        lensSettingsResponse = coreService.deleteLensSettings(settings.getId());
        Assert.assertEquals("Create Lens settings delete", lensSettingsResponse.status, true);
    }

    /**
     * addUsagelogWithValidData_DB
     */
    @Test
    public void createVendorWithValidData_DB() throws Exception {
        searchVendorSettings.setVendorName("sample");
        searchVendorSettings.setType("resume");
        searchVendorSettings.setMaxDocumentCount(500);
        searchVendorSettings.setRegisteredDocumentCount(0);
        searchVendorSettings.setStatus("Open");
//        searchVendorSettings.setDocServer("bglens-rds1");
//        searchVendorSettings.setHyperCubeServer("bglens-rhcube1");
        LocalDate today = new LocalDate();
        searchVendorSettings.setCreatedOn(today.toDate());
        searchVendorSettings.setUpdatedOn(today.toDate());
//        CoreLenssettings lensSettings = new CoreLenssettings();
//        lensSettings.setId(settings.getId());
        CoreLenssettings coreLensSettings = dbService.getLensSettingsById(settings.getId());
        searchVendorSettings.setCoreLenssettings(coreLensSettings);

        ApiResponse createVendorResponse = dbService.createVendor(searchVendorSettings);
        // Add Assert
        Assert.assertEquals("DB: Add Usage Log check", createVendorResponse.status, true);

    }

}
