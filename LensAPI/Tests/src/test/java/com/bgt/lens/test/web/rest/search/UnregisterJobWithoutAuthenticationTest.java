// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.criteria.SearchRegisterLogCriteria;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.SearchRegisterlog;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.RegisterDocument;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.RegisterJobRequest;
import com.bgt.lens.model.searchservice.request.UnregisterJobRequest;
import com.bgt.lens.model.soap.response.searchservice.UnregisterJobResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletContext;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Unregister job without authentication test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class UnregisterJobWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Instance type.
     */
    private final String instanceType = "SPECTRUM";
    /**
     * Locale type.
     */
    private final String localeTypeEnus = "en_us";
    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Invalid type.
     */
    private final String invalidType = "Invalid Instance Type";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor vendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Unregister Job
     */
    private boolean unregisterJob;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/JobTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");

            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            //Create vendor
            CreateVendorRequest createVendor = new CreateVendorRequest();
//            createVendor.setDocServerName(testHelper.postingDocServerName);
            createVendor.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendor.setMaxDocumentCount((long) (500));
            createVendor.setVendor("resvendor01");
            createVendor.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendor);
            Assert.assertEquals("Add vendor check",
                    createVendorResponse.status, true);
            vendor = (Vendor) createVendorResponse.responseData;
            helper = new Helper();
//            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("job");
//            apiContext.setClient(coreClient);
//            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);
            String fileName = "/TestJobs/10001.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterJobWithValidId
     */
    @Test
    public final void unregisterJobWithValidId() {
        try {
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            ApiResponse response = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job status check", response.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());

            MvcResult result = mockMvc.perform(delete(UriConstants.UnregisterJob + "/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("locale", TestHelper.Locale.en_us.name()).param("instanceType", instanceType).param("vendor", vendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            UnregisterJobResponse unregisterJobResponse = (UnregisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new UnregisterJobResponse());

            ObjectMapper mapper;
            byte[] json;

            if (unregisterJobResponse != null && unregisterJobResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(unregisterJobResponse.responseData);
                RegisterDocument registerDocument = mapper.readValue(json, RegisterDocument.class
                );

                Assert.assertEquals("Unregister document Id", "1", registerDocument.getId());
                Assert.assertEquals("Unregister document count", 0, (long) registerDocument.getRegisteredDocumentCount());

//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
//                Assert.assertEquals("Unregister job check log size", searchRegisterLogList.size()+1 , updatedSearchRegisterLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterJobWithInvalidId
     */
    @Test
    public final void unregisterJobWithInvalidId() {
        try {
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            MvcResult result = mockMvc.perform(delete(UriConstants.UnregisterJob + "/12345")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("locale", TestHelper.Locale.en_us.name()).param("instanceType", instanceType).param("vendor", vendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            UnregisterJobResponse unregisterJobResponse = (UnregisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new UnregisterJobResponse());

            if (unregisterJobResponse != null && (!unregisterJobResponse.status)) {

//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
//                Assert.assertEquals("Unregister job check log size", searchRegisterLogList.size(), updatedSearchRegisterLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterJobWithValidIdFromDefaultVendor
     */
    @Test
    public final void unregisterJobWithValidIdWithoutVendor() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, null, TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job status check", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());

            MvcResult result = mockMvc.perform(delete(UriConstants.UnregisterJob + "/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("locale", TestHelper.Locale.en_us.name()).param("instanceType", instanceType))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            UnregisterJobResponse unregisterJobResponse = (UnregisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new UnregisterJobResponse());

            if (unregisterJobResponse != null && (!unregisterJobResponse.status)) {
//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
//                Assert.assertEquals("Unregister job check log size", searchRegisterLogList.size(), updatedSearchRegisterLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterJobWithInvalidVendor
     */
    @Test
    public final void unregisterJobWithInvalidVendor() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job status check", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());

            MvcResult result = mockMvc.perform(delete(UriConstants.UnregisterJob + "/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("locale", TestHelper.Locale.en_us.name()).param("instanceType", instanceType).param("vendor", "InvalidVendor"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            UnregisterJobResponse unregisterJobResponse = (UnregisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new UnregisterJobResponse());

            if (unregisterJobResponse != null && (!unregisterJobResponse.status)) {
//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
//                Assert.assertEquals("Unregister job check log size", searchRegisterLogList.size(), updatedSearchRegisterLogList.size());
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterJobWithoutInstanceType
     */
    @Test
    public final void unregisterJobWithoutInstanceType() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job status check", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());

            MvcResult result = mockMvc.perform(delete(UriConstants.UnregisterJob + "/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("locale", TestHelper.Locale.en_us.name()).param("vendor", vendor.getVendor()))
                    .andExpect(status().isBadRequest())
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
//            Assert.assertEquals("Unregister job check log size", searchRegisterLogList.size(), updatedSearchRegisterLogList.size());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterJobWithoutLocale
     */
    @Test
    public final void unregisterJobWithoutLocale() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job status check", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());

            MvcResult result = mockMvc.perform(delete(UriConstants.UnregisterJob + "/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", instanceType).param("vendor", vendor.getVendor()))
                    .andExpect(status().isBadRequest())
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
//            Assert.assertEquals("Unregister job check log size", searchRegisterLogList.size(), updatedSearchRegisterLogList.size());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterJobWithoutLocale
     */
    @Test
    public final void unregisterJobWithInvalidInstanceType() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job status check", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());

            try {
                MvcResult result = mockMvc.perform(delete(UriConstants.UnregisterJob + "/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("instanceType", "XRAY").param("locale", TestHelper.Locale.en_us.toString()).param("vendor", vendor.getVendor()))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
//                Assert.assertEquals("Unregister job check log size", searchRegisterLogList.size(), updatedSearchRegisterLogList.size());

                response.status = false;
            }

            Assert.assertFalse("Unregister job with invalid instance type check", response.status);

            UnregisterJobRequest unregisterJobRequest = new UnregisterJobRequest();
            unregisterJobRequest.setInstanceType(registerJobRequest.getInstanceType());
            unregisterJobRequest.setLocale(registerJobRequest.getLocale());
            unregisterJobRequest.setJobId(registerJobRequest.getJobId());
            unregisterJobRequest.setVendor(registerJobRequest.getVendor());

            ApiResponse unRegisterResponse = searchService.unregisterJob(unregisterJobRequest);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterJobWithInvalidLocale
     */
    @Test
    public final void unregisterJobWithInvalidLocale() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job status check", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());

            MvcResult result = mockMvc.perform(delete(UriConstants.UnregisterJob + "/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", instanceType).param("locale", TestHelper.Locale.en_gb.toString()).param("vendor", vendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            UnregisterJobResponse unregisterJobResponse = (UnregisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new UnregisterJobResponse());

            if (unregisterJobResponse != null && (!unregisterJobResponse.status)) {
//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
//                Assert.assertEquals("Unregister job check log size", searchRegisterLogList.size(), updatedSearchRegisterLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Unregister job
     *
     * @param jobId
     * @param instanceType
     * @param vendorName
     * @param locale
     * @return
     * @throws Exception
     */
    public ApiResponse unregisterJob(String jobId, String instanceType, String vendorName, String locale) throws Exception {
        UnregisterJobRequest unregisterRequest = new UnregisterJobRequest();
        unregisterRequest.setJobId(jobId);
        unregisterRequest.setVendor(vendorName);
        unregisterRequest.setInstanceType(instanceType);
        unregisterRequest.setLocale(locale);

        ApiResponse unregisterResponse = searchService.unregisterJob(unregisterRequest);
        return unregisterResponse;
    }
}
