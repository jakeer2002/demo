// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.coreservice;

import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.Filter;
import com.bgt.lens.model.adminservice.request.GetLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * Lens settings test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
@SpringBootTest
public class LensSettingsTest {

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {

        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);

    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * Add Lens Instance With Valid Data Returns Success
     */
    @Test
    public void addLensInstanceWithValidDataReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

//            AddLensSettingsRequest.FailoverLensSettings failoverLens = new AddLensSettingsRequest.FailoverLensSettings();
//            failoverLens.getFailoverLensSettingsIdList().add(BigInteger.valueOf(settings.getId()));
//            settingsRequest.setFailoverLensSettings(failoverLens);
//            settingsRequest.setInstanceType("Spectrum");
//            
//            response = coreService.addLensSettings(settingsRequest);
//            Assert.assertEquals("Add Lens settings check", response.status, true);
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without InstanceType Returns Success
     */
    @Test
    public void addLensInstanceWithoutInstanceTypeReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType(null);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without HostName Returns Failure
     */
    @Test
    public void addLensInstanceWithoutHostNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setHost(null);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without Port Returns Failure
     */
    @Test
    public void addLensInstanceWithoutPortReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setPort(null);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without CharSet Returns Success
     */
    @Test
    public void addLensInstanceWithoutCharSetReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setCharSet(null);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without Timeout Returns Success
     */
    @Test
    public void addLensInstanceWithoutTimeoutReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setTimeOut(null);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without Locale Returns Failure
     */
    @Test
    public void addLensInstanceWithoutLocaleReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setLocale(null);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without Language Returns Success
     */
    @Test
    public void addLensInstanceWithoutLanguageReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setLanguage(null);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without Country Returns Success
     */
    @Test
    public void addLensInstanceWithoutCountryReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setCountry(null);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without HRXMLContent Returns Success
     */
    @Test
    public void addLensInstanceWithoutHRXMLContentReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setHRXMLContent(null);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Without LensVersion Returns Failure
     */
    @Test
    public void addLensInstanceWithoutLensVersionReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setVersion(null);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Status with true Returns Success
     */
    @Test
    public void addLensInstanceStatusWithTrueReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setStatus(true);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance Status with false Returns Success
     */
    @Test
    public void addLensInstanceStatusWithFalseReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setStatus(false);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Resume Custom Filters Returns Success
     */
    @Test
    public void addLensInstanceWithValidCustomAndFacetFiltersReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            Assert.assertTrue("Resume custom filters size check", settings.getFilterSettings().getCustomFilters().getResumeFilters().size() > 0);
            Assert.assertTrue("Resume custom filters size check", settings.getFilterSettings().getFacetFilters().getResumeFilters().size() > 0);
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Resume Custom Filters Returns Success
     */
    @Test
    public void addLensInstanceWithValidEmptyCustomAndFacetFiltersReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");

            settingsRequest.setStatus(false);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            Assert.assertTrue("Filter settings size check", settings.getFilterSettings() == null);
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Resume Custom Filters Returns Success
     */
    @Test
    public void addLensInstanceWithValidResumeCustomFiltersReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            Assert.assertTrue("Resume custom filters size check", settings.getFilterSettings().getCustomFilters().getResumeFilters().size() > 0);
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Resume Custom Filters Returns Success
     */
    @Test
    public void addLensInstanceWithValidResumeFacetFiltersReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            Assert.assertTrue("Resume facet filters size check", settings.getFilterSettings().getFacetFilters().getResumeFilters().size() > 0);
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Posting Custom Filters Returns Success
     */
    @Test
    public void addLensInstanceWithValidPostingCustomFiltersReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            Assert.assertTrue("Resume custom filters size check", settings.getFilterSettings().getCustomFilters().getPostingFilters().size() > 0);
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Posting Custom Filters Returns Success
     */
    @Test
    public void addLensInstanceWithValidPostingFacetFiltersReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            Assert.assertTrue("Resume facet filters size check", settings.getFilterSettings().getFacetFilters().getPostingFilters().size() > 0);
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Invalid Instance Type Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingCustomFiltersAndInvalidInstanceReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
            response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Invalid Instance Type Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingFacetFiltersAndInvalidInstanceReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Empty custom filter key Type Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingCustomFiltersAndEmptyKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Empty custom filter key Type Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingFacetFiltersAndEmptyKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Null Key Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingCustomFiltersAndNullKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey(null);
            filter.setName("samples");
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Null Key Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingFacetFiltersAndNullKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey(null);
            filter.setName("samples");
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Empty custom filter name Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingCustomFiltersAndEmptyNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("");
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Empty custom filter name Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingFacetFiltersAndEmptyNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("");
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Null Value Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingCustomFiltersAndNullNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName(null);
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Null Value Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingFacetFiltersAndNullNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName(null);
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Duplicate custom filter key Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingCustomFiltersAndDuplicateKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");
            Filter filter01 = new Filter();
            filter.setKey("Sample");
            filter.setName("test");
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter01);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Duplicate custom filter key Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingFacetFiltersAndDuplicateKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");
            Filter filter01 = new Filter();
            filter.setKey("Sample");
            filter.setName("test");
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Duplicate custom filter value Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingCustomFiltersAndDuplicateNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");
            Filter filter01 = new Filter();
            filter.setKey("Sample01");
            filter.setName("sample");
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter01);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Add Lens Instance With Duplicate custom filter value Returns Failure
     */
    @Test
    public void addLensInstanceWithValidPostingFacetFiltersAndDuplicateNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //CustomFilters customFilters = new CustomFilters();
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");
            Filter filter01 = new Filter();
            filter.setKey("Sample01");
            filter.setName("sample");
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);
            settingsRequest.setStatus(false);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addLensSettings(settingsRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Add Lens settings check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance With Valid Data Returns Success
     */
    @Test
    public void updateLensInstanceWithValidDataReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

//            AddLensSettingsRequest.FailoverLensSettings failoverLens = new AddLensSettingsRequest.FailoverLensSettings();
//            failoverLens.getFailoverLensSettingsIdList().add(BigInteger.valueOf(settings.getId()));
//            settingsRequest.setFailoverLensSettings(failoverLens);
//            settingsRequest.setInstanceType("Spectrum");
//            
//            response = coreService.addLensSettings(settingsRequest);
//            LensSettings settings1 = (LensSettings) response.responseData;
//            Assert.assertEquals("Add Lens settings check", response.status, true);
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
//            UpdateLensSettingsRequest.FailoverLensSettings failoverLens1 = new UpdateLensSettingsRequest.FailoverLensSettings();
//            failoverLens1.getFailoverLensSettingsIdList().clear();
//            failoverLens1.getFailoverLensSettingsIdList().add(BigInteger.valueOf(settings1.getId()));
//            updateRequest.setFailoverLensSettings(failoverLens1);
//            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance With Invalid Lens Instance Id Returns Failure
     */
    @Test
    public void updateLensInstanceWithInvalidLensInstanceIdReturnsFailure() {
        try {
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            ApiResponse response = coreService.updateLensSettings(0, updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Without InstanceType Returns Success
     */
    @Test
    public void updateLensInstanceWithoutInstanceTypeReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType(null);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Without HostName Returns Success
     */
    @Test
    public void updateLensInstanceWithoutHostNameReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setHost(null);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Without Port Returns Success
     */
    @Test
    public void updateLensInstanceWithoutPortReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setPort(null);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Without CharSet Returns Success
     */
    @Test
    public void updateLensInstanceWithoutCharSetReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setCharSet(null);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Without Langauage Returns Success
     */
    @Test
    public void updateLensInstanceWithoutLocaleReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setLocale(null);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Without Language Returns Success
     */
    @Test
    public void updateLensInstanceWithoutLanguageReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setLanguage(null);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Without Country Returns Success
     */
    @Test
    public void updateLensInstanceWithoutCountryReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setCountry(null);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Without HRXMLContent Returns Success
     */
    @Test
    public void updateLensInstanceWithoutHRXMLContentReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setHRXMLContent(null);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Without LensVersion Returns Success
     */
    @Test
    public void updateLensInstanceWithoutLensVersionReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setVersion(null);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Status with true Returns Success
     */
    @Test
    public void updateLensInstanceStatusWithTrueReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setStatus(true);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens Instance Status with false Returns Success
     */
    @Test
    public void updateLensInstanceStatusWithFalseReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setStatus(false);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter (NOT NULL)
     */
    @Test
    public void updateLensInstanceWithCustomFiltersNotNullRemoveNotSpecifiedFilterReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            LensSettings updatedSettings = (LensSettings) response.responseData;
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getCustomFilters().getResumeFilters() != null && updatedSettings.getFilterSettings().getCustomFilters().getResumeFilters().size() == 0);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getCustomFilters().getPostingFilters().size() == 1);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter (NOT NULL)
     */
    @Test
    public void updateLensInstanceWithFacetFiltersNotNullRemoveNotSpecifiedFilterReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            LensSettings updatedSettings = (LensSettings) response.responseData;
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getFacetFilters().getResumeFilters() != null && updatedSettings.getFilterSettings().getFacetFilters().getResumeFilters().size() == 0);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getFacetFilters().getPostingFilters().size() == 1);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter (NOT NULL)
     */
    @Test
    public void updateLensInstanceWithCustomFiltersNotNullReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            LensSettings updatedSettings = (LensSettings) response.responseData;
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getCustomFilters().getResumeFilters().size() == 1);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getCustomFilters().getPostingFilters().size() == 1);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter (NOT NULL)
     */
    @Test
    public void updateLensInstanceWithFacetFiltersNotNullReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            LensSettings updatedSettings = (LensSettings) response.responseData;
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getFacetFilters().getResumeFilters().size() == 1);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getFacetFilters().getPostingFilters().size() == 1);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter (NULL)
     */
    @Test
    public void updateLensInstanceWithCustomFiltersNullFilterReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            LensSettings updatedSettings = (LensSettings) response.responseData;
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getCustomFilters().getResumeFilters().size() == 1);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getCustomFilters().getPostingFilters().size() == 1);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter (NULL)
     */
    @Test
    public void updateLensInstanceWithFacetFiltersNullFilterReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            LensSettings updatedSettings = (LensSettings) response.responseData;
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getFacetFilters().getResumeFilters().size() == 1);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getFacetFilters().getPostingFilters().size() == 1);
            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Invalid Instance type
     */
    @Test
    public void updateLensInstanceWithCustomFiltersInvalidInstanceTypeReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");

            updateRequest.setInstanceType("Invalid");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Invalid Instance type
     */
    @Test
    public void updateLensInstanceWithFacetFiltersInvalidInstanceTypeReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");

            updateRequest.setInstanceType("Invalid");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            response = coreService.updateLensSettings(settings.getId(), updateRequest);
            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Empty Key
     */
    @Test
    public void updateLensInstanceWithCustomFiltersEmptyKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey("");
            filter.setName("samples");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Empty Key
     */
    @Test
    public void updateLensInstanceWithFacetFiltersEmptyKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey("");
            filter.setName("samples");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Null key
     */
    @Test
    public void updateLensInstanceWithCustomFiltersNullKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey(null);
            filter.setName("samples");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Null key
     */
    @Test
    public void updateLensInstanceWithFacetFiltersNullKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey(null);
            filter.setName("samples");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Empty name
     */
    @Test
    public void updateLensInstanceWithCustomFiltersEmptyNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Empty name
     */
    @Test
    public void updateLensInstanceWithFacetFiltersEmptyNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Null name
     */
    @Test
    public void updateLensInstanceWithCustomFiltersNullNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName(null);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - Null name
     */
    @Test
    public void updateLensInstanceWithFacetFiltersNullNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName(null);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - duplicate key
     */
    @Test
    public void updateLensInstanceWithCustomFiltersDuplicateKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("test");

            Filter filter01 = new Filter();
            filter01.setKey("sample");
            filter01.setName("sample");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter01);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - duplicate key
     */
    @Test
    public void updateLensInstanceWithFacetFiltersDuplicateKeyReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("test");

            Filter filter01 = new Filter();
            filter01.setKey("sample");
            filter01.setName("sample");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter01);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - duplicate value
     */
    @Test
    public void updateLensInstanceWithCustomFiltersDuplicateNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter01);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - duplicate value
     */
    @Test
    public void updateLensInstanceWithFacetFiltersDuplicateNameReturnsFailure() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter01);

            response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(settings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Create Lens settings delete", response.status, false);

            response = coreService.deleteLensSettings(settings.getId());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - duplicate value
     */
    @Test
    public void updateLensInstanceAddedWithCustomFilterRemoveAndAddFacetFiltersReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("samples01");
            settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter01);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            LensSettings updatedSettings = (LensSettings) response.responseData;
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getFacetFilters().getResumeFilters().size() == 1);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getFacetFilters().getPostingFilters().size() == 1);
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getCustomFilters() != null && updatedSettings.getFilterSettings().getCustomFilters().getResumeFilters() != null && updatedSettings.getFilterSettings().getCustomFilters().getResumeFilters().size() == 0);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getCustomFilters() != null && updatedSettings.getFilterSettings().getCustomFilters().getPostingFilters() != null && updatedSettings.getFilterSettings().getCustomFilters().getPostingFilters().size() == 0);

            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - duplicate value
     */
    @Test
    public void updateLensInstanceAddedWithFacetFilterRemoveAndAddCustomFiltersReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("samples01");
            settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter01);

            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            LensSettings updatedSettings = (LensSettings) response.responseData;
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getCustomFilters().getResumeFilters().size() == 1);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getCustomFilters().getPostingFilters().size() == 1);
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getFacetFilters() != null && updatedSettings.getFilterSettings().getFacetFilters().getResumeFilters() != null && updatedSettings.getFilterSettings().getFacetFilters().getResumeFilters().size() == 0);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getFacetFilters() != null && updatedSettings.getFilterSettings().getFacetFilters().getPostingFilters() != null && updatedSettings.getFilterSettings().getFacetFilters().getPostingFilters().size() == 0);

            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Update Lens with custom filter - duplicate value
     */
    @Test
    public void updateLensInstanceAddedWithCustomAndFacetFilterEmptyFilterUpdateReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            settingsRequest.setInstanceType("SPECTRUM");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("samples");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("samples01");
            settingsRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);
            settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            settingsRequest.getFilterSettings().getCustomFilters().getPostingFilters().add(filter01);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);

            response = coreService.updateLensSettings(settings.getId(), updateRequest);

            LensSettings updatedSettings = (LensSettings) response.responseData;
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getCustomFilters().getResumeFilters().size() == 0);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getCustomFilters().getPostingFilters().size() == 0);
            Assert.assertTrue("Check Resume filter size", updatedSettings.getFilterSettings().getFacetFilters().getResumeFilters().size() == 0);
            Assert.assertTrue("Check Posting filter size", updatedSettings.getFilterSettings().getFacetFilters().getPostingFilters().size() == 0);

            Assert.assertEquals("Update Lens settings", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Delete Lens Instance With Valid LensSettings Id Returns Success
     */
    @Test
    public void deleteLensInstanceWtihValidLensSettingsIdReturnSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;
            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Delete Lens Instance With Invalid LensSettings Id Returns Failure
     */
    @Test
    public void deleteLensInstanceWithInvalidLensSettingsIdReturnsFailure() {
        try {
            ApiResponse response = coreService.deleteLensSettings(0);
            Assert.assertEquals("Create Lens settings delete", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Id With Valid Lens Instance Id Returns Success
     */
    @Test
    public void getLensInstancebyIdWithValidLensInstanceIdReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            //GetLensSettingsRequest request = new GetLensSettingsRequest();
            response = coreService.getLensSettingsById(settings.getId());
            Assert.assertEquals("Get Lens settings ", response.status, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Id With Valid Lens Instance Id Returns Success
     */
    @Test
    public void getLensInstancebyIdWithValidLensInstanceIdWithCustomFilterReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");
            settingsRequest.setInstanceType("SPECTRUM");
            settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            //GetLensSettingsRequest request = new GetLensSettingsRequest();
            response = coreService.getLensSettingsById(settings.getId());
            Assert.assertEquals("Get Lens settings ", response.status, true);
            Assert.assertEquals("Get Lens settings custom filter size check", settings.getFilterSettings().getCustomFilters().getResumeFilters().size(), 1);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Id With Invalid Lens Instance Id Returns Failure
     */
    @Test
    public void getLensInstancebyIdWithInvalidInstanceIdReturnsFailure() {
        try {
            ApiResponse response = coreService.getLensSettingsById(0);
            Assert.assertEquals("Get Lens settings ", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithCustomFilterReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");
            settingsRequest.setInstanceType("SPECTRUM");
            settingsRequest.getFilterSettings().getCustomFilters().getResumeFilters().add(filter);
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);
            Assert.assertEquals("Get Lens settings custom filter size check", settingsList.get(0).getFilterSettings().getCustomFilters().getResumeFilters().size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria with InstanceType Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithInstanceTypeReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();
            request.setInstanceType(settingsRequest.getInstanceType());

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria with HostName Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithHostNameReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();
            request.setHost(settingsRequest.getHost());

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria with Port Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithPortReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();
            request.setPort(settingsRequest.getPort());

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria with CharSet Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithCharSetReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();
            request.setCharSet(settingsRequest.getCharSet());

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria with Locale Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithLocaleReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();
            request.setLocale(settingsRequest.getLocale());

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria with Language Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithLanguageReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();
            request.setLanguage(settingsRequest.getLanguage());

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria with Country Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithCountryReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();
            request.setCountry(settingsRequest.getCountry());

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria with LensVersion Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithLensVersionReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();
            request.setLensVersion(settingsRequest.getVersion());

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Get Lens Instance by Criteria with Status Returns Success
     */
    @Test
    public void getLensInstanceByCriteriaWithStatusReturnsSuccess() {
        try {
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            ApiResponse response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);

            LensSettings settings = (LensSettings) response.responseData;

            GetLensSettingsRequest request = new GetLensSettingsRequest();
            request.setStatus(settingsRequest.isStatus());

            response = coreService.getLensSettings(request);
            Assert.assertEquals("Get Lens settings ", response.status, true);

            List<LensSettings> settingsList = (List<LensSettings>) response.responseData;
            Assert.assertEquals("Get Lens settings ", settingsList.size() > 0, true);

            response = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

}
