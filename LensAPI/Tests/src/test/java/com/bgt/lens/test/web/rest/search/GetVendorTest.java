// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchVendorSettingsCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.rest.response.VendorList;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.soap.response.searchservice.GetVendorByCriteriaResponse;
import com.bgt.lens.model.soap.response.searchservice.GetVendorByIdResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Get Vendor Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class GetVendorTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();


    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor vendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;
    
    Set<CoreCustomskillsettings> coreCustomskillsettings = new HashSet<>();

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {       
        ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
           servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");

            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;
                        
            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");
            
            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            consumer.setLensSettings(settingsList);
            
            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
            consumer.setVendorSettings(null);
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            Set<CoreLenssettings> coreLenssettingses = new HashSet<>();
            
            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
             // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);
            
            ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if(clientList != null && clientList.size() > 0){
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister resume request", deleteExternalClientResponse.status);
            }
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
           

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByIdWithValidVendorId() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);
            
            Vendor vendor = (Vendor) response.responseData;
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendor+"/"+vendor.getVendorId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByIdResponse getVendorResponse = (GetVendorByIdResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByIdResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                Vendor openVendor = mapper.readValue(json, Vendor.class
                );

                Assert.assertEquals("Create vendor max document count check", 500000, (long)openVendor.getMaxDocumentCount());
                Assert.assertEquals("Create vendor registered document count", 0, (long)openVendor.getRegisteredDocumentCount());
                Assert.assertEquals("Create vendor status", com.bgt.lens.helpers.Enum.vendorStatus.Open.toString(), openVendor.getStatus());
                Assert.assertEquals("Create vendor type", com.bgt.lens.helpers.Enum.docType.resume.toString(), openVendor.getType().trim());
                
                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 1, searchVendorSettingsList.size());
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(vendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByIdWithInvalidVendorId() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);
            
            Vendor vendor = (Vendor) response.responseData;
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendor+"/0")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 1, searchVendorSettingsList.size());
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(vendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithoutCriteria() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Create vendor max document count check", 2, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithoutCriteria() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Get vendor count check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithType() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("type", "resume"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Vendor list check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithValidType() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("type", "posting"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Get vendor count check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithInvalidType() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("type", "test"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {                

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
     /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithInvalidType() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("type", "resume"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {
                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithconsumerKey() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
//            apiContext.setClient(adminClient);
//            helper.setApiContext(apiContext);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("consumerKey", "Test01"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Create vendor max document count check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithValidConsumerKey() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("consumerKey", "Test01"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Get vendor count check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithInvalidconsumerKey() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
//            apiContext.setClient(adminClient);
//            helper.setApiContext(apiContext);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("consumerKey", "test"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {
                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithInvalidConsumerKey() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("consumerKey", "sample"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithVendorName() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("vendor", "resvendor0101"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Vendor list check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithValidVendorName() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("vendor", "resvendor0101"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Get vendor count check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithInvalidVendorName() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("vendor", "test"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());
            if (getVendorResponse != null && (!getVendorResponse.status)) {
                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithInvalidVendorName() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("vendor", "resvendor01"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithInstanceType() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            CoreLenssettings lensSettings = dbService.getLensSettingsById(enukLensSettings.getId());
            lensSettings.setInstanceType(com.bgt.lens.helpers.Enum.InstanceType.SPECTRUM.toString());
            ApiResponse updateLensettingsResponse = dbService.updateLensSettings(lensSettings,coreCustomskillsettings);
            Assert.assertTrue("Update LENS settings status check", updateLensettingsResponse.status);
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("instanceType", "Spectrum"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Vendor list check", 2, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithValidInstanceType() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            CoreLenssettings lensSettings = dbService.getLensSettingsById(enukLensSettings.getId());
            lensSettings.setInstanceType(com.bgt.lens.helpers.Enum.InstanceType.SPECTRUM.toString());
            ApiResponse updateLensettingsResponse = dbService.updateLensSettings(lensSettings,coreCustomskillsettings);
            Assert.assertTrue("Update LENS settings status check", updateLensettingsResponse.status);
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("instanceType", "Spectrum"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Get vendor count check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithInvalidInstanceType() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            CoreLenssettings lensSettings = dbService.getLensSettingsById(enukLensSettings.getId());
            lensSettings.setInstanceType(com.bgt.lens.helpers.Enum.InstanceType.SPECTRUM.toString());
            ApiResponse updateLensettingsResponse = dbService.updateLensSettings(lensSettings,coreCustomskillsettings);
            Assert.assertTrue("Update LENS settings status check", updateLensettingsResponse.status);
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("instanceType", "test"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {
                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithInvalidInstanceType() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            CoreLenssettings enuslensSettings = dbService.getLensSettingsById(enusLensSettings.getId());
            enuslensSettings.setInstanceType(com.bgt.lens.helpers.Enum.InstanceType.SPECTRUM.toString());
            ApiResponse updateenusLensettingsResponse = dbService.updateLensSettings(enuslensSettings,coreCustomskillsettings);
            Assert.assertTrue("Update LENS settings status check", updateenusLensettingsResponse.status);
            
            CoreLenssettings lensSettings = dbService.getLensSettingsById(enukLensSettings.getId());
            lensSettings.setInstanceType(com.bgt.lens.helpers.Enum.InstanceType.SPECTRUM.toString());
            ApiResponse updateLensettingsResponse = dbService.updateLensSettings(lensSettings,coreCustomskillsettings);
            Assert.assertTrue("Update LENS settings status check", updateLensettingsResponse.status);
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("instanceType", "Lens"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithLocale() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("locale", "en_us"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Vendor list check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithValidLocale() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("locale", "en_uk"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Get vendor count check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithInvalidLocale() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("locale", "en_au"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {                

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithInvalidLocale() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("locale", "en_us"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithStatus() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            SearchVendorsettings externalvendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
            externalvendorSettings.setStatus(com.bgt.lens.helpers.Enum.vendorStatus.Close.toString());
            ApiResponse closeVendorResponse = dbService.updateVendor(externalvendorSettings);
            Assert.assertTrue("Close vendor status check", closeVendorResponse.status);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("status", "close"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Vendor list check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithValidStatus() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("status", "open"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(true)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getVendorResponse != null && getVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                VendorList vendorList = mapper.readValue(json, VendorList.class
                );
                
                Assert.assertEquals("Get vendor count check", 1, (long)vendorList.getVendorList().size());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsAdminWithInvalidStatus() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            SearchVendorsettings externalvendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
            externalvendorSettings.setStatus(com.bgt.lens.helpers.Enum.vendorStatus.Close.toString());
            ApiResponse closeVendorResponse = dbService.updateVendor(externalvendorSettings);
            Assert.assertTrue("Close vendor status check", closeVendorResponse.status);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("status", "test"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Create Vendor
     */
    @Test
    public final void getVendorByCriteriaAsClientWithInvalidStatus() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create admin vendor status check", response.status);
            
            Vendor adminVendor = (Vendor) response.responseData;
            
            createVendorRequest.setInstanceId(BigInteger.valueOf(enukLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            ApiResponse createVendorResponse = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create external client vendor status check", createVendorResponse.status);
            Vendor externalClientVendor = (Vendor) createVendorResponse.responseData;  
            
            CoreClient adminClient = dbService
                    .getClientById(client.getId());    
            
            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings settings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(settings);
            adminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            
            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            
            // Register Assert            
            MvcResult result =mockMvc.perform(get(UriConstants.GetVendorList)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(testHelper.
                        convertObjectToJsonBytes(createVendorRequest))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(
                        final MockHttpServletRequest request) {
                            request.setAttribute("ApiContext",
                                    apiContext);
                            return request;
                        }
                }).param("status", "close"))
                .andExpect(content()
                        .contentTypeCompatibleWith(
                                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)))
                .andExpect(jsonPath("$.statusCode", is("OK")))
                .andExpect(jsonPath("$.requestId", notNullValue()))
                .andExpect(jsonPath("$.timeStamp", notNullValue()))
                .andReturn();
            
            GetVendorByCriteriaResponse getVendorResponse = (GetVendorByCriteriaResponse)helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetVendorByCriteriaResponse());

            if (getVendorResponse != null && (!getVendorResponse.status)) {

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 2, searchVendorSettingsList.size());                
                
                SearchVendorsettings vendorSettings = dbService.getVendorById(adminVendor.getVendorId());
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                
                vendorSettings = dbService.getVendorById(externalClientVendor.getVendorId());
                deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
