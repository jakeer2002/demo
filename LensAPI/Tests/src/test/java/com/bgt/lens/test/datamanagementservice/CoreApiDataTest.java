// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.datamanagementservice;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * API data test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CoreApiDataTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    @Autowired
    IDataRepositoryService dbService;

    /**
     * API
     */
    private CoreApi coreApi = null;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        coreApi = new CoreApi();
        coreApi.setApiKey("TestApiKey");
        coreApi.setName("Test Api");
        coreApi.setCreatedOn(Date.from(Instant.now()));
        coreApi.setUpdatedOn(Date.from(Instant.now()));
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * addApiWithValidData_DB
     */
    @Test
    public void addApiWithValidData_DB() {
        try {
            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addApiWithDuplicateKeyAndNameValidData_DB
     */
    @Test
    public void addApiWithDuplicateKeyAndNameValidData_DB() {
        try {
            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);
            // DuplicateApi
            ApiResponse addDuplicateApiResponse = new ApiResponse();
            try {
                addDuplicateApiResponse = createApi(coreApi);
                addDuplicateApiResponse.status = true;
            } catch (Exception ex) {
                addDuplicateApiResponse.status = false;
            }
            // DuplicateApi Assert
            Assert.assertEquals("DB: Create Duplicate Api status check", addDuplicateApiResponse.status, false);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * updateApiWithValidData_DB
     */
    @Test
    public void updateApiWithValidData_DB() {
        try {

            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            // UpdateApi
            CoreApi updateCoreApi = new CoreApi();
            updateCoreApi.setId(coreApi.getId());
            updateCoreApi.setApiKey("UpdateTestApiKey");
            updateCoreApi.setName("UpdateTest Api");
            updateCoreApi.setCreatedOn(Date.from(Instant.now()));
            updateCoreApi.setUpdatedOn(Date.from(Instant.now()));
            // UpdateApi Assert
            ApiResponse apiResponse = dbService.updateApi(updateCoreApi);
            Assert.assertEquals("DB: Update Api status check", apiResponse.status, true);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * udateApiWithInvalidApiId_DB
     */
    @Test
    public void udateApiWithInvalidApiId_DB() {
        try {

            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            // UpdateApi
            CoreApi updateCoreApi = new CoreApi();
            updateCoreApi.setId(000);
            updateCoreApi.setApiKey("UpdateTestApiKey");
            updateCoreApi.setName("UpdateTest Api");
            updateCoreApi.setCreatedOn(Date.from(Instant.now()));
            updateCoreApi.setUpdatedOn(Date.from(Instant.now()));
            // UpdateApi Assert
            ApiResponse apiResponse = new ApiResponse();
            try {
                apiResponse = dbService.updateApi(updateCoreApi);
                apiResponse.status = true;
            } catch (Exception ex) {
                apiResponse.status = false;
            }
            Assert.assertEquals("DB: Update Api Invalid status check", apiResponse.status, false);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * updateApiWithDuplicateApiKeyandName_DB
     */
    @Test
    public void updateApiWithDuplicateApiKeyandName_DB() {
        try {
            // AddApi
            CoreApi coreApi01 = new CoreApi();
            coreApi01.setApiKey("TestApiKey");
            coreApi01.setName("Test Api");
            coreApi01.setCreatedOn(Date.from(Instant.now()));
            coreApi01.setUpdatedOn(Date.from(Instant.now()));
            ApiResponse addApiResponse = createApi(coreApi01);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            // Add another Api
            CoreApi coreApi02 = new CoreApi();
            coreApi02.setApiKey("TestApiKey01");
            coreApi02.setName("Test Api01");
            coreApi02.setCreatedOn(Date.from(Instant.now()));
            coreApi02.setUpdatedOn(Date.from(Instant.now()));

            ApiResponse addApiResponse01 = createApi(coreApi02);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api another status check", addApiResponse01.status, true);

            // UpdateApi
            CoreApi updateCoreApi = new CoreApi();
            updateCoreApi.setId(coreApi01.getId());
            updateCoreApi.setApiKey("TestApiKey01");
            updateCoreApi.setName("Test Api01");
            updateCoreApi.setCreatedOn(Date.from(Instant.now()));
            updateCoreApi.setUpdatedOn(Date.from(Instant.now()));
            // UpdateApi Assert
            ApiResponse updateApiResponse = new ApiResponse();
            try {
                updateApiResponse = dbService.updateApi(updateCoreApi);
                updateApiResponse.status = true;
            } catch (Exception ex) {
                updateApiResponse.status = false;
            }
            Assert.assertEquals("DB: Update Api Invalid status check", updateApiResponse.status, false);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi01);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

            // DeleteApi 01
            ApiResponse deleteApiResponse01 = deleteApi(coreApi02);
            // DeleteApi 01  Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse01.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * deleteApiWithValidData
     */
    @Test
    public void deleteApiWithValidData() {
        try {
            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * deleteApiWithInvalidData
     */
    @Test
    public void deleteApiWithInvalidData() {
        try {
            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            // DeleteApi01 Assert
            CoreApi coreApi01 = new CoreApi();
            coreApi01.setId(000);
            coreApi01.setApiKey("TestApiKey");
            coreApi01.setName("Test Api");
            coreApi01.setCreatedOn(Date.from(Instant.now()));
            coreApi01.setUpdatedOn(Date.from(Instant.now()));
            ApiResponse deleteApiResponse01 = new ApiResponse();
            try {
                deleteApiResponse01 = deleteApi(coreApi01);
                deleteApiResponse01.status = true;
            } catch (Exception ex) {
                deleteApiResponse01.status = false;
            }
            // DeleteApi01 Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse01.status, false);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getListofAllApi
     */
    @Test
    public void getListofAllApi() {
        try {
            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            ApiCriteria apiCriteria = new ApiCriteria();

            List<CoreApi> coreApiResponse = dbService.getApiByCriteria(apiCriteria);
            Assert.assertTrue("DB: Get Api", coreApiResponse.size() > 0);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getApiByIdWithValidData
     */
    @Test
    public void getApiByIdWithValidData() {
        try {
            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            CoreApi coreApiIdResponse = dbService.getApiById(coreApi.getId());
            Assert.assertEquals(coreApi.getApiKey(), coreApiIdResponse.getApiKey());
            Assert.assertNotNull("DB: GetApiById status check", coreApiIdResponse);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getApiByIdWithInvalidData
     */
    @Test
    public void getApiByIdWithInvalidData() {
        try {
            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            CoreApi coreApiIdResponse = dbService.getApiById(0);
            Assert.assertNull("DB: GetApiById status check", coreApiIdResponse);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getApiByCriteriaWithApiKeyValidData
     */
    @Test
    public void getApiByCriteriaWithApiKeyValidData() {
        try {
            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            ApiCriteria apiCriteria = new ApiCriteria();
            apiCriteria.setApiKey("TestApiKey");
            List<CoreApi> coreApiResponse = dbService.getApiByCriteria(apiCriteria);

            Assert.assertNotNull("DB: GetApiByCriteriaWithApiKeyValidData status check", coreApiResponse);
            Assert.assertTrue("DB: Get Api", coreApiResponse.size() == 1);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getApiByCriteriaWithNameValidData
     */
    @Test
    public void getApiByCriteriaWithNameValidData() {
        try {
            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            ApiCriteria apiCriteria = new ApiCriteria();
            apiCriteria.setName("Test Api");

            List<CoreApi> coreApiResponse = dbService.getApiByCriteria(apiCriteria);

            Assert.assertNotNull("DB: GetApiByCriteriaWithNameValidData status check", coreApiResponse);
            Assert.assertTrue("Get Api", coreApiResponse.size() == 1);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getApiByCriteriaWithApiKeyNameValidData
     */
    @Test
    public void getApiByCriteriaWithApiKeyNameValidData() {
        try {

            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            ApiCriteria apiCriteria = new ApiCriteria();
            apiCriteria.setApiKey("TestApiKey");
            apiCriteria.setName("Test Api");

            List<CoreApi> coreApiResponse = dbService.getApiByCriteria(apiCriteria);

            Assert.assertNotNull("DB: GetApiByCriteriaWithApiKeyNameValidData check", coreApiResponse);
            Assert.assertTrue("DB: Get Api", coreApiResponse.size() == 1);
            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getApiByCriteriaWithKeyInvalidData
     */
    @Test
    public void getApiByCriteriaWithKeyInvalidData() {
        try {

            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            ApiCriteria apiCriteria = new ApiCriteria();
            apiCriteria.setApiKey("InvalidData");

            List<CoreApi> coreApiResponse = dbService.getApiByCriteria(apiCriteria);

            Assert.assertNull("DB: GetApiByCriteriaWithInValidData check", coreApiResponse);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getApiByCriteriaWithNameInvalidData
     */
    @Test
    public void getApiByCriteriaWithNameInvalidData() {
        try {

            // AddApi
            ApiResponse addApiResponse = createApi(coreApi);
            // AddApi Assert
            Assert.assertEquals("DB: Create Api status check", addApiResponse.status, true);

            ApiCriteria apiCriteria = new ApiCriteria();
            apiCriteria.setName("InvalidData");

            List<CoreApi> coreApiResponse = dbService.getApiByCriteria(apiCriteria);

            Assert.assertNull("DB: GetApiByCriteriaWithInValidData check", coreApiResponse);

            // DeleteApi
            ApiResponse deleteApiResponse = deleteApi(coreApi);
            // DeleteApi Assert
            Assert.assertEquals("DB: Delete Api status check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * Create API
     *
     * @param coreApi
     * @return
     * @throws Exception
     */
    public ApiResponse createApi(CoreApi coreApi) throws Exception {
        return dbService.createApi(coreApi);
    }

    /**
     * Delete API
     *
     * @param coreApi
     * @return
     * @throws Exception
     */
    public ApiResponse deleteApi(CoreApi coreApi) throws Exception {
        return dbService.deleteApi(coreApi);

    }

}
