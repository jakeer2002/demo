// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.CanonRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * *
 * Canon Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CanonTests {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Autowired
    IUtilityService utilityService;

    /**
     * *
     * API context
     */
    public ApiContext apiContext;
    /**
     * *
     * API
     */
    public Api api;
    /**
     * *
     * Lens settings
     */
    public LensSettings en_usLensSettings, en_ukLensSettings, localeSettings, unicodeLensSettings;

    /**
     * *
     * Resource
     */
    public Resources resource;

    /**
     * *
     * Client
     */
    public Client client;

    /**
     * *
     * Client
     */
    public CoreClient coreClient;

    /**
     * *
     * Helper object
     */
    public TestHelper testHelper;

    /**
     * *
     * Helper
     */
    public Helper _helper;

    /**
     * *
     * Binary data
     */
    private String binaryData;

    /**
     * *
     * Instance type
     */
    public String instanceType = "XRay";

    /**
     * *
     * Locale type
     */
    public String localeTypeEn_us = "en_us";

    /**
     * *
     * Locale type
     */
    public String localeTypeEn_uk = "en_uk";

    /**
     * *
     * Invalid type
     */
    public String invalidType = "Invalid Instance Type";

    /**
     * *
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     *
     * @throws JAXBException
     */
    @Before
    public void setUp() throws JAXBException {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        try {
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            instanceType = testHelper.instanceType;
            // create api
            CreateApiRequest createApi = testHelper.getCreateApiRequest("/input/CreateApi.xml");

            //CreateApiRequest createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(CanonTests.class.getResourceAsStream("/input/CreateApi.xml"));
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ParseResumeWithHtmTests.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;
            
            //en_uk
            settingsRequest.setLocale("en_us");
            settingsRequest.setCountry("UnitedStates");
            settingsRequest.setHost(testHelper.unicodeInstanceHost);
            settingsRequest.setInstanceType(testHelper.unicodeInstanceType);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.unicodeInstancePort)));
            settingsRequest.setLocale(testHelper.unicodeInstanceLocale);
            settingsRequest.setCharSet("utf-8");
            ApiResponse en_us_unicode_LensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings unicode instance check", en_us_unicode_LensSettingsResponse.status, true);
            unicodeLensSettings = (LensSettings) en_us_unicode_LensSettingsResponse.responseData;
            
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // create resource
            AddResourceRequest resourceRequest = testHelper.getAddResourceRequest("/input/CreateResource.xml");
            //AddResourceRequest resourceRequest = (AddResourceRequest) jaxbUnmarshaller.unmarshal(ParseResumeWithHtmTests.class.getResourceAsStream("/input/CreateResource.xml"));
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            //CreateConsumerRequest consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(ParseResumeWithHtmTests.class.getResourceAsStream("/input/CreateConsumer.xml"));

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();

            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList unicode_en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_usLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);         
            unicode_en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(unicodeLensSettings.getId())));
            unicode_en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(unicode_en_us_InstanceList); 
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_ukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);

            consumer.setLensSettings(settingsList);

            consumer = testHelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;

            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            
            // en_us LensSettings
            CoreLenssettings en_usCoreLenssettings = new CoreLenssettings();
            en_usCoreLenssettings.setId(1);
            en_usCoreLenssettings.setHost(testHelper.hostname);
            en_usCoreLenssettings.setPort(testHelper.port);
            en_usCoreLenssettings.setCharacterSet(testHelper.encoding);
            en_usCoreLenssettings.setTimeout(testHelper.timeOut);
            en_usCoreLenssettings.setInstanceType(testHelper.instanceType);
            en_usCoreLenssettings.setLocale(testHelper.locale);
            en_usCoreLenssettings.setStatus(testHelper.status);            

            // en_uk LensSettings
            CoreLenssettings en_ukCoreLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            en_ukCoreLenssettings.setId(2);
            en_ukCoreLenssettings.setLocale(localeTypeEn_uk);         
            
            // en_uk LensSettings
            CoreLenssettings unicodeLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            unicodeLenssettings.setId(3);
            unicodeLenssettings.setLocale(testHelper.locale);   
            unicodeLenssettings.setHost(testHelper.unicodeInstanceHost);
            unicodeLenssettings.setPort(testHelper.unicodeInstancePort);
            unicodeLenssettings.setCharacterSet("utf-8");
            unicodeLenssettings.setInstanceType(testHelper.unicodeInstanceType);
            unicodeLenssettings.setLocale(testHelper.unicodeInstanceLocale);
            
            // en_us LensSettings
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(4);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);            

            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings unicodeClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(en_usCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(en_ukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);    
            unicodeClientlenssettings.setCoreLenssettings(unicodeLenssettings);  
            coreClientlenssettingses.add(unicodeClientlenssettings);   
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);        
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);

            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            //coreClientapis.add(clientapis.get(0));
            //coreClient.setCoreClientapis(coreClientapis);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("POST");
            apiContext.setResource("Canonicalizer");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            String fileName = "/TestResumes/CanonCommand.txt";
            InputStream resourceAsStream04 = CanonTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete", deleteApiResponse.status, true);
            // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete", deleteConsumerResponse.status, true);
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse unicodeDeleteLensSettingsResponse = coreService.deleteLensSettings(unicodeLensSettings.getId());
            Assert.assertEquals("Delete unicode Lens settings delete", unicodeDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * canonWithValidBinaryResumeData
     */
    @Test
    public void canonWithValidBinaryResumeDataInvalidXmlChar() {
        try {
            // Canon
            CanonRequest canonRequest = new CanonRequest();
            String fileName = "/TestResumes/CanonCommand_Unicode.txt";
            InputStream resourceAsStream04 = CanonTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    canonRequest.setBinaryData(_helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04)));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            
            canonRequest.setInstanceType(testHelper.unicodeInstanceType);
            canonRequest.setLocale(testHelper.unicodeInstanceLocale);
            ApiResponse response = utilityService.canonBinaryData(canonRequest);
            Assert.assertEquals("Canon With ValidBinary ResumeData check", response.status, true);

            // Canon Assert
            String expResult = "canonversion";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());

            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // Canon Transaction Count Assert
            Assert.assertEquals("Canon With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * canonWithValidBinaryResumeData
     */
    @Test
    public void canonWithValidBinaryResumeData() {
        try {
            // Canon
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setLocale(TestHelper.Locale.en_us.name());
            canonRequest.setBinaryData(binaryData);
            ApiResponse response = utilityService.canonBinaryData(canonRequest);
            Assert.assertEquals("Canon With ValidBinary ResumeData check", response.status, true);

            // Canon Assert
            String expResult = "canonversion";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());

            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // Canon Transaction Count Assert
            Assert.assertEquals("Canon With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * canonWithValidBinaryResumeData
     */
    @Test
    public void canonWithValidBinaryResumeDataWithDefaultLocale() {
        try {
            // Canon
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry(TestHelper.Locale.en_us.name());
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setBinaryData(binaryData);

            ApiResponse response = utilityService.canonBinaryData(canonRequest);
            Assert.assertEquals("Canon With ValidBinary ResumeData check", response.status, true);

            // Canon Assert
            String expResult = "canonversion";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // Canon Transaction Count Assert
            Assert.assertEquals("Canon With ValidBinary with default locale TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * CanonWithEmptyBinaryData
     */
    @Test
    public void canonWithEmptyBinaryData() {
        try {
            String fileName = "/TestResumes/EmptyResume/CanonCommandEmpty.txt";
            InputStream resourceAsStream = CanonTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream));
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            // Canon
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setLocale(TestHelper.Locale.en_us.name());
            canonRequest.setBinaryData(binaryData);
            ApiResponse response = utilityService.canonBinaryData(canonRequest);
            Assert.assertEquals("Canon With Empty BinaryData check", response.status, false);

            // Canon Assert
            String expResult = "no doc found";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // Canon Transaction Count Assert
            Assert.assertEquals("Canon With Empty BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * CanonWithNullBinaryData
     */
    @Test
    public void canonWithNullBinaryData() {
        try {
            binaryData = null;
            // Canon
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setLocale(TestHelper.Locale.en_us.name());
            canonRequest.setBinaryData(binaryData);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.canonBinaryData(canonRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("Canon With Null BinaryData check", response.status, false);

            // Canon Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // Canon Transaction Count Assert
            Assert.assertEquals("Canon With Null BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * CanonSpecificLensInstance
     */
    @Test
    public void canonSpecificLensInstance() {
        try {
            // Canon
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setBinaryData(binaryData);
            canonRequest.setLocale(localeTypeEn_us);
            ApiResponse response = utilityService.canonBinaryData(canonRequest);
            Assert.assertEquals("ParseResumeBGTXML Specific Lens Instance check", response.status, true);

            // Canon Assert
            String expResult = "canonversion";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // Canon Transaction Count Assert
            Assert.assertEquals("Canon With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * CanonUnauthorizedInstance
     */
    @Test
    public void canonUnauthorizedInstance() {
        try {
            // Canon
            String localeType = "en_anz";
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setBinaryData(binaryData);
            canonRequest.setLocale(localeType);
            ApiResponse response = utilityService.canonBinaryData(canonRequest);
            Assert.assertEquals("Canon With Unauthorized Instance check", response.status, false);

            // Canon en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * CanonEmptyInstanceList
     */
    @Test
    public void canonEmptyInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Canon
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setLocale(TestHelper.Locale.en_us.name());
            canonRequest.setBinaryData(binaryData);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.canonBinaryData(canonRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("Canon With Empty Instance List check", response.status, false);

            // Canon en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * CanonNullInstanceList
     */
    @Test
    public void canonNullInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Canon
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setLocale(TestHelper.Locale.en_us.name());
            canonRequest.setBinaryData(binaryData);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.canonBinaryData(canonRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("Canon With Null Instance List check", response.status, false);

            // Canon en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * CanonInvalidLensList
     */
    @Test
    public void canonInvalidLensList() {
        try {
            //Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Canon
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setBinaryData(binaryData);
            canonRequest.setLocale(localeTypeEn_us);
            ApiResponse response = utilityService.canonBinaryData(canonRequest);
            Assert.assertEquals("Canon With Lens Invalid  check", response.status, false);

            // Canon en_us Assert
            String instanceResponse = (String) response.responseData;
            LensRepository repository = new LensRepository();

            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));
            
             // Remove the lens settings with the client            
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * CanonInActiveLensList
     */
    @Test
    public void canonInActiveLensList() {
        try {
            //Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Canon
            CanonRequest canonRequest = new CanonRequest();
            canonRequest.setInstanceType(instanceType);
            canonRequest.setBinaryData(binaryData);
            canonRequest.setLocale(localeTypeEn_us);
            ApiResponse response = utilityService.canonBinaryData(canonRequest);
            Assert.assertEquals("Canon With InActive Lens check", response.status, false);

            // Canon inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            boolean inActiveInstance = instanceResponse.contains(_helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS));
            assertTrue(inActiveInstance);
            
             // Remove the lens settings with the client            
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
