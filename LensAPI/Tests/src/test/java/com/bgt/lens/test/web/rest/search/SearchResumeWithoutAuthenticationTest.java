// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Enum;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.FilterSettings;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Candidate;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.SearchResult;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.AdditionalFilters;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.CustomFilters;
import com.bgt.lens.model.searchservice.request.Filter;
import com.bgt.lens.model.searchservice.request.Keywords;
import com.bgt.lens.model.searchservice.request.RegisterResumeRequest;
import com.bgt.lens.model.searchservice.request.ResumeSearchRequest;
import com.bgt.lens.model.searchservice.request.SearchCriteria;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Geography;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Keyword;
import com.bgt.lens.model.searchservice.request.UnregisterResumeRequest;
import com.bgt.lens.model.soap.response.searchservice.ResumeSearchResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Search Resume Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class SearchResumeWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor adminVendor;
    /**
     * External client vendor
     */
    private Vendor externalClientVendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Admin client
     */
    private CoreClient adminClient;
    /**
     * ExternalClient
     */
    private CoreClient externalClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            createApi.setKey("Search");
            createApi.setName("Search Api");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");
            resourceRequest.setApiKey(api.getKey());
            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            
            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            adminVendor = (Vendor) response.responseData;

            createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            externalClientVendor = (Vendor) response.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            settingsList.getInstanceList().clear();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            consumer.setLensSettings(settingsList);

            CreateConsumerRequest.VendorSettings settings = new CreateConsumerRequest.VendorSettings();
            settings.getVendorIdList().add(BigInteger.valueOf(adminVendor.getVendorId()));
            consumer.setVendorSettings(settings);

            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
//            consumer.getVendorSettings().getVendorIdList().clear();
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);

            adminClient = dbService
                    .getClientById(client.getId());
            adminClient.setTier(1);

            SearchSettings searchSettings = new SearchSettings();
            searchSettings.getResume().clear();
            SearchSettings.Resume resume = new SearchSettings.Resume();
            resume.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            resume.setVendorId(BigInteger.valueOf(adminVendor.getVendorId()));
            searchSettings.getResume().add(resume);

            String searchSettingsJson = helper.getJsonStringFromObject(searchSettings);

            CoreClient updatedAdminClient = adminClient;
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings searchVendorSettings = dbService.getVendorById(adminVendor.getVendorId());
            searchVendorSettingsSet.add(searchVendorSettings);
            updatedAdminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            updatedAdminClient.setSearchSettings(searchSettingsJson);
            ApiResponse updateClientResponse = dbService.updateClient(adminClient, updatedAdminClient);
            Assert.assertTrue("Update client check", updateClientResponse.status);

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
              // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);

            ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if (clientList != null && clientList.size() > 0) {
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister resume request", deleteExternalClientResponse.status);
            }
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
           

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithSearchTypeAsKeywordGenericKeywordSearch() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job[pos='1']:(\"CWTS\")) and (resume/experience/job[pos='1']:(\"Apar Infotech\") or resume/experience/job[pos='2']:(\"Apar Infotech\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithoutSearchTypeGenericKeywordSearch() {
        try {
            
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);
               
            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            //apiContext.setClient(null);
            //helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job[pos='1']:(\"CWTS\")) and (resume/experience/job[pos='1']:(\"Apar Infotech\") or resume/experience/job[pos='2']:(\"Apar Infotech\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

             Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsFullResume() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.Generic.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[((\"CWTS\")) and ((\"Apar Infotech\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(searchCommandDumpList.size() - 1).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsExperienceSection() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.ExperienceSection.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.ExperienceSection.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience:(\"CWTS\")) and (resume/experience:(\"Apar Infotech\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsLastJob() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job[pos='1']:(\"CWTS\")) or (resume/experience/job[pos='1']:(\"Apar Infotech\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsLastTwoJobs() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job[pos='1']:(\"CWTS\") or resume/experience/job[pos='2']:(\"CWTS\")) or (resume/experience/job[pos='1']:(\"Apar Infotech\") or resume/experience/job[pos='2']:(\"Apar Infotech\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsLastJobTitle() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Online Trainer/Mentor");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.LastJobTitle.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job[pos='1']/title:(\"Online Trainer Mentor\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsJobTitle() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Online Trainer/Mentor");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.JobTitle.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job/title:(\"Online Trainer Mentor\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsEmployer() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Amherst School District");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.Employer.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job/employer:(\"Amherst School District\") or resume/experience/employer:(\"Amherst School District\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsMostRecentEmployer() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("MAJOR ASSIGNMENTS");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.MostRecentEmployer.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job[id='1']/employer:(\"MAJOR ASSIGNMENTS\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsEducation() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Mechanical Engineering");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.Education.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/education:(\"Mechanical Engineering\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsDegree() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Bachelor's Degree");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.Degree.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/education/degree:(\"Bachelor's Degree\") or resume/education/school/degree:(\"Bachelor's Degree\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsMajor() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Mechanical Engineering");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.Major.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/education/major:(\"Mechanical Engineering\") or resume/education/school/major:(\"Mechanical Engineering\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsSIC() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("73");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.SIC.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/experience/job[sic2='73']) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithKeywordContextAsContactSection() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("EDRISH");
            keywords.setLogicalOperator("Or");
            keywords.setContext(Enum.keywordContext.ContactSection.name());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(resume/contact:(\"EDRISH\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithInvalidKeywordContext() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("EDRISH");
            keywords.setLogicalOperator("Or");
            keywords.setContext("test");
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[((\"EDRISH\")) ]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithDefaultVendor() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("EDRISH");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithInvalidVendor() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor("test");
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("EDRISH");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithoutDefaultVendor() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor("test");
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("EDRISH");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithoutInstanceType() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("EDRISH");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Invalid instance type check", apiResponse.status);
            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithInvalidInstanceType() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType("test");
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("EDRISH");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Invalid instance type check", apiResponse.status);
            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithoutLocale() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("EDRISH");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Invalid instance type check", apiResponse.status);
            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithInvalidLocale() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale("test");

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("EDRISH");
            keywords.setLogicalOperator("Or");
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            //Assert.assertFalse("Invalid instance type check", apiResponse.status);
            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
/*    @Test
    public final void searchResumeCKSSearch() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CKS.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("CWTS");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastJob.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("Apar Infotech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.LastTwoJobs.toString());
            keywordCriteria.getKeywords().add(keywords);

            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[CKS((\"CWTS\") and (\"Apar Infotech\") )]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }  */

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeCustomBooleanSearch() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))");

            searchCriteria.setKeyword(keywordCriteria);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithValidMaximumDocumentCount() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"1\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With InValidPostalCode.
     */
    @Test
    public final void searchResumeWithInValidPostalCode() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17#$112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) 1);
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();            
        } catch (Exception ex) {
            assertTrue(ex.getMessage().contains(helper.getErrorMessageWithURL(ApiErrors.POSTAL_CODE_INVALID_CHAR)));
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithInvalidMaximumDocumentCount() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (10));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMaximumDocumentCount((long) (-1));
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Invalid max document count check", apiResponse.status);

            SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
            dumpCriteria.setClientId(adminClient.getId());
            List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithValidMinimumScore() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))");
            searchCriteria.setKeyword(keywordCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (5));
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"5\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><keyword><![CDATA[(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))]]></keyword><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithNegativeMiminumScore() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))");
            searchCriteria.setKeyword(keywordCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (-1));
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            // Register Assert
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Minimum LENS score invalid check", apiResponse.status);
            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithInvalidMiminumScore() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("(job[pos='1']:(\"CWTS\")) and (job[pos='1']:(\"Apar Infotech\") or job[pos='2']:(\"Apar Infotech\"))");
            searchCriteria.setKeyword(keywordCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setMinimumScore((long) (1005));
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            // Register Assert
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Minimum LENS score invalid check", apiResponse.status);
            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithoutCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Search resume without criteria", apiResponse.status);

            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithValidOffset() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setOffset((long) (2));
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithOffsetGreaterThanSearchResultCount() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setOffset((long) (3));
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithNegativeOffset() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setOffset((long) (-1));
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Negative offet count check", apiResponse.status);
            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithDefaultOrder() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Search results order by check", searchResult.getCandidates().get(0).getScore() >= searchResult.getCandidates().get(1).getScore());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithDescScore() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortDirection(Enum.sortDirection.DESC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Search results order by check", searchResult.getCandidates().get(0).getScore() <= searchResult.getCandidates().get(1).getScore());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeOrderByIdASC() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy(Enum.orderResultsBy.id.toString());
            additionalFilters.setSortDirection(Enum.sortDirection.ASC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", searchResult.getCandidates().get(0).getResumeId(), "1");
                Assert.assertEquals("Search results order by check", searchResult.getCandidates().get(1).getResumeId(), "2");

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeOrderByIdDESC() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy(Enum.orderResultsBy.id.toString());
            additionalFilters.setSortDirection(Enum.sortDirection.DESC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", searchResult.getCandidates().get(0).getResumeId(), "2");
                Assert.assertEquals("Search results order by check", searchResult.getCandidates().get(1).getResumeId(), "1");

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeOrderByExperienceASC() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy(Enum.orderResultsBy.experience.toString());
            additionalFilters.setSortDirection(Enum.sortDirection.ASC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", 24, (long) searchResult.getCandidates().get(0).getYrsExp());
                Assert.assertEquals("Search results order by check", 27, (long) searchResult.getCandidates().get(1).getYrsExp());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeOrderByExperienceDESC() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy(Enum.orderResultsBy.experience.toString());
            additionalFilters.setSortDirection(Enum.sortDirection.DESC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", 27, (long) searchResult.getCandidates().get(0).getYrsExp());
                Assert.assertEquals("Search results order by check", 24, (long) searchResult.getCandidates().get(1).getYrsExp());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeOrderByNameASC() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy(Enum.orderResultsBy.name.toString());
            additionalFilters.setSortDirection(Enum.sortDirection.ASC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", "EDRISH HABIBULLAH", searchResult.getCandidates().get(0).getName());
                Assert.assertEquals("Search results order by check", "Karen-Lynne Joseph-Beeston", searchResult.getCandidates().get(1).getName());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeOrderByNameDESC() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy(Enum.orderResultsBy.name.toString());
            additionalFilters.setSortDirection(Enum.sortDirection.DESC.toString());
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", "Karen-Lynne Joseph-Beeston", searchResult.getCandidates().get(0).getName());
                Assert.assertEquals("Search results order by check", "EDRISH HABIBULLAH", searchResult.getCandidates().get(1).getName());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeOrderByInvalidCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Geography appGeoCriteria = new Geography();
            appGeoCriteria.setDistance((long) (1000));
            appGeoCriteria.setDistanceUnits(Enum.distanceUnits.Miles.toString());
            appGeoCriteria.setPostalCode("17112");
            searchCriteria.setGeography(appGeoCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setSortBy("test");
            additionalFilters.setSortDirection("test");
            searchCriteria.setAdditionalFilters(additionalFilters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><distance max=\"1000\" units=\"miles\"/><posting><contact><address><postalcode>17112</postalcode></address></contact></posting><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertEquals("Search results order by check", "1", searchResult.getCandidates().get(0).getResumeId());
                Assert.assertEquals("Search results order by check", "2", searchResult.getCandidates().get(1).getResumeId());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithCustomFiltersWithMinCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(),new CoreClient() , true, 1);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin((long) (2));
            filters.getFilter().add(filter01);

            searchCriteria.setCustomFilters(filters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><bgldlanguage min=\"2\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Candidate candidate = searchResult.getCandidates().get(0);
                Assert.assertEquals("candidate custom filter count check", candidate.getCustomFilters().size(), 1);

                com.bgt.lens.model.rest.response.Filter f = candidate.getCustomFilters().get(0);
                Assert.assertEquals("candidate custom filter name check", f.getName(), "bgldlanguage");
                Assert.assertEquals("candidate custom filter value check", (long) f.getValue(), 2);

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithCustomFiltersWithMaxCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 3);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMax((long) (2));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><bgldlanguage max=\"2\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Candidate candidate = searchResult.getCandidates().get(0);
                Assert.assertEquals("candidate custom filter count check", candidate.getCustomFilters().size(), 1);

                com.bgt.lens.model.rest.response.Filter f = candidate.getCustomFilters().get(0);
                Assert.assertEquals("candidate custom filter name check", f.getName(), "bgldlanguage");
                Assert.assertEquals("candidate custom filter value check", (long) f.getValue(), 2);

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithCustomFiltersWithMinandMaxCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin((long) (1));
            filter01.setMax((long) (2));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><bgldlanguage max=\"2\" min=\"1\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithCustomFiltersWithValueCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setValue((long) (1));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><bgldlanguage max=\"1\" min=\"1\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithCustomFiltersWithArrayofValuesCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.getValues().add((long) (1));
            filter01.getValues().add((long) (2));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><bgldlanguage array=\"1 2\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
//                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithCustomFiltersWithEmptyCustomFilterNameCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("");
            filter01.setMin((long) (1));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Search resume response check", response.status);

            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithCustomFiltersWithNullCustomFilterValueCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin(null);
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Search resume response check", response.status);

            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithCustomFiltersWithInvalidCustomFilterNameCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("abcd");
            filter01.setMin((long) (1));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            response.status = true;
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(resumeSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Search resume response check", response.status);

            Integer remainingTransactionCount = 0;
            CoreClient client = dbService.getClientById(adminClient.getId());
            condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

            ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume With ValidBinaryResumeData.
     */
    @Test
    public final void searchResumeWithCustomFiltersWithInvalidCustomFilterValueCriteria() {
        try {
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            unregisterResume("2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 1);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            Thread.sleep(1000);
            ResumeSearchRequest resumeSearchRequest = new ResumeSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin((long) (5));
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);
            resumeSearchRequest.setSearchCriteria(searchCriteria);

            Integer currentTransactionCount = 0;

            Predicate condition = new Predicate() {
                @Override
                public boolean evaluate(Object clientApi) {
                    return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                }
            };
            List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

            if (coreClientApi != null && coreClientApi.size() > 0) {
                currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
            }

            // Register Assert
            apiContext.setClient(null);
            helper.setApiContext(apiContext);
            MvcResult result = mockMvc.perform(post(UriConstants.SearchResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(resumeSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ResumeSearchResponse resumeSearchResponse = (ResumeSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new ResumeSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (resumeSearchResponse != null && resumeSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(resumeSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setLensSettingsId(enusLensSettings.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='" + testHelper.encoding + "'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"resume\" vendor=\"resvendor01\"><bgldlanguage min=\"5\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"name\"/><include var=\"yrsexp\"/><include var=\"jobs\"/><include var=\"schools\"/><include var=\"xpath://contact/address\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());

                Integer remainingTransactionCount = 0;
                CoreClient client = dbService.getClientById(adminClient.getId());
                condition = new Predicate() {
                    @Override
                    public boolean evaluate(Object clientApi) {
                        return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
                    }
                };
                coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

                if (coreClientApi != null && coreClientApi.size() > 0) {
                    remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
                }

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 2, (long)remainingTransactionCount);

                ApiResponse unregisterResponse = unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterResume
     *
     * @param resumeId
     * @param instanceType
     * @param vendorName
     * @param locale
     * @return
     * @throws Exception
     */
    public ApiResponse unregisterResume(String resumeId, String instanceType, String vendorName, String locale) throws Exception {
        UnregisterResumeRequest unregisterRequest = new UnregisterResumeRequest();
        unregisterRequest.setResumeId(resumeId);
        unregisterRequest.setVendor(vendorName);
        unregisterRequest.setInstanceType(instanceType);
        unregisterRequest.setLocale(locale);

        ApiResponse unregisterResponse = searchService.unregisterResume(unregisterRequest);
        return unregisterResponse;
    }

    /**
     * Register resume
     *
     * @param instanceType
     * @param locale
     * @param binaryData
     * @param resumeId
     * @param vendorName
     * @param client
     * @return
     */
    public ApiResponse registerResume(String instanceType, String locale, String binaryData, String resumeId, String vendorName, CoreClient client, boolean addCustomFilters, Integer customFilterValue) throws Exception {
        RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
        registerResumeRequest.setInstanceType(instanceType);
        registerResumeRequest.setLocale(locale);
        registerResumeRequest.setBinaryData(binaryData);
        registerResumeRequest.setResumeId(resumeId);
        registerResumeRequest.setVendor(vendorName);
        registerResumeRequest.setErrorOnDuplicateorOverwrite(Enum.registerDuplicate.overwrite.toString());

        if (addCustomFilters) {
            Filter filter01 = new Filter();
            filter01.setName("bgldlanguage");
            filter01.setValue((long) (customFilterValue));

            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerResumeRequest.setCustomFilters(filters);
            registerResumeRequest.setRegisterWithCustomFilters(true);
        }
        
        //apiContext.setClient(client);
        helper.setApiContext(apiContext);
        ApiResponse registerResumeResponse = searchService.registerResume(registerResumeRequest);
        return registerResumeResponse;
    }
}
