// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Lens ping test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryPingTest {
    
    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;
    
    
    private ApiErrors apiErrors = new ApiErrors();   

    
    /**
     * *
     * Helper object
     */
    private static final Helper _helper = new Helper();

    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Lens settings object
     */
    private CoreLenssettings lensSettings;

    /**
     * helper object
     */
    private TestHelper testHelper;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        
         ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);       
                
        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lensSettings = new CoreLenssettings();
        lensSettings.setHost(testHelper.hostname);
        lensSettings.setPort(testHelper.port);
        lensSettings.setCharacterSet(testHelper.encoding);
        lensSettings.setTimeout(testHelper.timeOut);
        lensSettings.setInstanceType(testHelper.instanceType);
        lensSettings.setLocale(testHelper.locale);
        lensSettings.setStatus(testHelper.status);
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * pingTestWithVaildData
     */
    @Test
    public void pingTestWithVaildData() {
        try {
            // Ping
            ApiResponse lensResponse = lensRepository.ping(lensSettings);
            // Ping Assert
            Assert.assertEquals("Lens Ping Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * pingTestWithInvalidHostName
     */
    @Test
    public void pingTestWithInvalidHostName() {
        try {
            // Ping
            lensSettings.setHost("Invalid");
            ApiResponse lensResponse = lensRepository.ping(lensSettings);
            // Ping Assert
            Assert.assertEquals("Lens Ping With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Ping With Invalid Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * PingTestWithEmptyHostName
     */
    @Test
    public void pingTestWithEmptyHostName() {
        try {
            // Ping
            lensSettings.setHost("");
            ApiResponse lensResponse = lensRepository.ping(lensSettings);
            // Ping Assert
            Assert.assertEquals("Lens Ping With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Ping With Empty Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * PingTestWithNullHostName
     */
    @Test
    public void pingTestWithNullHostName() {
        try {
            // Ping
            lensSettings.setHost(null);
            ApiResponse lensResponse = lensRepository.ping(lensSettings);
            // Ping Assert
            Assert.assertEquals("Lens Ping With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Ping With Null Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * PingTestWithInvalidHostPort
     */
    @Test
    public void pingTestWithInvalidHostPort() {
        try {
            // Ping
            lensSettings.setPort(0000);
            ApiResponse lensResponse = lensRepository.ping(lensSettings);
            // Ping Assert
            Assert.assertEquals("Lens Ping With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Ping With Invalid HostPort Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * PingTestWithInValidEncoding
     */
    @Test
    public void pingTestWithInValidEncoding() {
        try {
            // Ping
            lensSettings.setCharacterSet("Invalid");
            ApiResponse lensResponse = lensRepository.ping(lensSettings);
            // Ping Assert
            Assert.assertEquals("Lens Ping With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Ping With InValid Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * PingTestWithEmptyEncoding
     */
    @Test
    public void pingTestWithEmptyEncoding() {
        try {
            // Ping
            lensSettings.setCharacterSet("");
            ApiResponse lensResponse = lensRepository.ping(lensSettings);
            // Ping Assert
            Assert.assertEquals("Lens Ping With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Ping With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * PingTestWithNullEncoding
     */
    @Test
    public void pingTestWithNullEncoding() {
        try {
            // Ping
            lensSettings.setCharacterSet(null);
            ApiResponse lensResponse = lensRepository.ping(lensSettings);
            // Ping Assert
            Assert.assertEquals("Lens Ping With Null Encoding Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * PingTestWithInvalidTimeOut
     */
    @Test
    public void pingTestWithInvalidTimeOut() {
        try {
            // Ping
            lensSettings.setTimeout(-10);
            ApiResponse lensResponse = lensRepository.ping(lensSettings);
            // Ping Assert
            Assert.assertEquals("Lens Ping With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Ping With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }
}
