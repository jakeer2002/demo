// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.logmanagementservice;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.GetUsageLogsRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.UsageLogCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreUsagelog;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.logger.ILoggerService;
import com.bgt.lens.test.Helper.TestHelper;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Usage log test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class UsageLogTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    ILoggerService loggerService;

    @Autowired
    IDataRepositoryService dbService;

    /**
     * Client
     */
    public CoreClient client = null;

    /**
     * Client API
     */
    public CoreClientapi clientApi = null;

    /**
     * API
     */
    public CoreApi api = null;

    /**
     * API response
     */
    public Api apiResponse = null;

    /**
     * LENS settings response
     */
    public LensSettings lensSettingsResponse = null;

    /**
     * API context
     */
    public ApiContext apiContext = null;

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * Helper object
     */
    private static final Helper _helper = new Helper();

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        try {
            // Create Api
            CreateApiRequest createApi = new CreateApiRequest();
            createApi.setKey("Core");
            createApi.setName("Core");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            apiResponse = (Api) response.responseData;

            //Add Lens settings
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);
            lensSettingsResponse = (LensSettings) response.responseData;

            //Add Resource
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            Resources resource = (Resources) resourceResponse.responseData;

            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, lensSettingsResponse.getId());
            consumer = _testhelper.updateApiandResource(consumer, apiResponse.getId(), apiResponse.getKey(), resource.getResourceId().toString());
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = dbService.getClientById(((Client) response.responseData).getId());

            CoreApi coreApi = dbService.getApiById(apiResponse.getId());
            ClientapiCriteria criteria = new ClientapiCriteria();
            criteria.setCoreApi(coreApi);
            List<CoreClientapi> clientApiList = dbService.getClientApiByCriteria(criteria);

            if (clientApiList != null && clientApiList.size() > 0) {
                clientApi = clientApiList.get(0);
            }

            // ApiContext
            apiContext = new ApiContext();
            apiContext.setClient(client);
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClientApi(clientApi);
            apiContext.setRequestHeader("TEST");
            apiContext.setRequestUri("http://localhost:8080/SaaSLensAPI");
            apiContext.setMethod("GET");
            apiContext.setResource("clients");
            apiContext.setInstanceType("XRAY");
            apiContext.setLocale("en_us");
            apiContext.setParseVariant("JSON");
            apiContext.setApiResponse(new ApiResponse());
            apiContext.setInTime(new Date());
            apiContext.setRequestContent("TEST");
            apiContext.setAccept("application/json");
            apiContext.setContentType("application/json");
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper.setApiContext(apiContext);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
        try {

            ApiResponse deleteCconsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer", deleteCconsumerResponse.status, true);

            ApiResponse deleteApiResponse = coreService.deleteApi(apiResponse.getId());
            Assert.assertEquals("Create Api delete", deleteApiResponse.status, true);

            ApiResponse deleteLensSettingsResponse = coreService.deleteLensSettings(lensSettingsResponse.getId());
            Assert.assertEquals("Delete Lens settings", deleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagelogWithValidData
     */
    @Test
    public void addUsagelogWithValidData() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            UsageLogCriteria criteria = new UsageLogCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagelog> usageLogList = dbService.getUsagelogByCriteria(criteria);
            if (usageLogList != null && usageLogList.size() > 0) {
                CoreUsagelog getCoreUsagelog = dbService.getUsagelogById(usageLogList.get(0).getRequestId());

                // Get Assert
                Assert.assertNotNull("Get Usage Log check", getCoreUsagelog);

                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLogList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);
            } else {
                Assert.fail();
            }

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addUsageLogWithInvalidClientId
     */
    @Test
    public void addUsageLogWithInvalidClientId() {
        try {
            // Add
            CoreClient client = new CoreClient();
            client.setId(-1);
            apiContext.setClient(client);
//            api.setId(1);
//            clientApi.setCoreApi(api);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                loggerService.addUsageLog(apiContext);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("Add Usage Log Invalid ClientId check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addUsageLogWithInvalidApiId
     */
    @Test
    public void addUsageLogWithInvalidApiId() {
        try {
            CoreClient client = new CoreClient();
            client.setId(1);
            apiContext.setClient(client);
            CoreApi api = new CoreApi();
            api.setId(-1);
            CoreClientapi clientApi = new CoreClientapi();
            clientApi.setCoreApi(api);
            apiContext.setClientApi(clientApi);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                loggerService.addUsageLog(apiContext);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Add Assert
            Assert.assertEquals("Add Usage Log with Invalid ApiId check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyIdWithValidLogId
     */
    // GetById
    @Test
    public void getUsageLogbyIdWithValidLogId() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            ApiResponse getCoreUsagelog = loggerService.getUsageLogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Delete Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyIdWithInvalidLogId
     */
    @Test
    public void getUsageLogbyIdWithInvalidLogId() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            ApiResponse getCoreUsagelog = loggerService.getUsageLogById("123", apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Delete Usage Log check", getCoreUsagelog.status, false);

            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyIdWithInvalidLogId
     */
    @Test
    public void getUsageLogbyIdWithInvalidLogIdasZero() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            ApiResponse getCoreUsagelog = loggerService.getUsageLogById("0", apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Delete Usage Log check", getCoreUsagelog.status, false);

            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyIdWithValidClientId
     */
    @Test
    public void getUsageLogbyIdWithValidClientId() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            ApiResponse getCoreUsagelog = loggerService.getUsageLogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Delete Usage Log check", getCoreUsagelog.status, true);

            // Get Assert
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyIdWithInvalidClientId
     */
    @Test
    public void getUsageLogbyIdWithInvalidClientId() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            ApiResponse getCoreUsagelog = new ApiResponse();
            try {
                // Get
                getCoreUsagelog = loggerService.getUsageLogById(apiContext.getRequestId().toString(), 123);
                Assert.fail();
            } catch (Exception ex) {
                // Get Assert
                Assert.assertEquals("Delete Usage Log check", getCoreUsagelog.status, false);
            }

            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getListofAllUsageLog
     */
    // GetByCriteria
    @Test
    public void getListofAllUsageLog() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithApiKey
     */
    @Test
    public void getUsageLogbyCriteriaWithApiKey() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setApiKey("Core");

            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithInvalidApiKey
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidApiKey() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setApiKey("Invalid");
            ApiResponse getCoreUsagelog = new ApiResponse();

            try {
                getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());
                getCoreUsagelog.status = true;
            } catch (Exception ex) {
                getCoreUsagelog.status = false;
            }
            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, false);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithHttpMethod
     */
    @Test
    public void getUsageLogbyCriteriaWithHttpMethod() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setMethod(apiContext.getMethod());
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithInvalidHttpMethod
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidHttpMethod() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setMethod("Invalid");

            ApiResponse getCoreUsagelog = new ApiResponse();

            try {
                getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());
                // Get Assert
                Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);
            } catch (ApiException ex) {
                // Get Assert
                Assert.assertEquals(_helper.getErrorMessageWithURL(ApiErrors.INVALID_HTTP_METHOD), ex.getMessage());
            }
            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithResource
     */
    @Test
    public void getUsageLogbyCriteriaWithResource() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setResource(apiContext.getResource());
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithInvalidResource
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidResource() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setResource("Invalid");

            ApiResponse getCoreUsagelog = new ApiResponse();

            try {
                getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());
                // Get Assert
                Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);
            } catch (ApiException ex) {
                // Get Assert
                Assert.assertEquals(_helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_NAME), ex.getMessage());
            }

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithRaisedErrorTrue
     */
    @Test
    public void getUsageLogbyCriteriaWithRaisedErrorTrue() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setIsError(Boolean.TRUE);
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithUsageLogDisabled
     */
    @Test
    public void getUsageLogbyCriteriaWithUsageLogDataDisabled() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setIsError(Boolean.TRUE);
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithUsageLogEnabled
     */
    @Test
    public void getUsageLogbyCriteriaWithUsageLogDataEnabled() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setIsError(Boolean.TRUE);
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithRaisedErrorFalse
     */
    @Test
    public void getUsageLogbyCriteriaWithRaisedErrorFalse() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setIsError(Boolean.FALSE);
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, false);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithEmptyInTime
     */
    @Test
    public void getUsageLogbyCriteriaWithEmptyInTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setFrom("");
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithNullInTime
     */
    @Test
    public void getUsageLogbyCriteriaWithNullInTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            getUsageLogsRequest.setFrom(null);
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithInTime
     */
    @Test
    public void getUsageLogbyCriteriaWithInTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            // DateFormat df = _helper.getDateFormat();
            // getUsageLogsRequest.setFrom(df.format(new Date()));

            DateTimeFormatter df = _helper.getDateFormat();
            getUsageLogsRequest.setFrom(new LocalDate().toString(df));
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithInvalidInTimeFormat
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidInTimeFormat() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            //DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            //df.setLenient(false);
            //getUsageLogsRequest.setFrom(df.format(new Date()));
            DateTimeFormatter df = DateTimeFormat.forPattern("dd/MM/yy");
            getUsageLogsRequest.setFrom(new LocalDate().toString(df));

            ApiResponse getCoreUsagelog = new ApiResponse();

            try {
                getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());
                getCoreUsagelog.status = true;
            } catch (Exception ex) {
                getCoreUsagelog.status = false;
            }

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, false);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithEmptyOutTime
     */
    @Test
    public void getUsageLogbyCriteriaWithEmptyOutTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(new Date()); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //getUsageLogsRequest.setTo(df.format(c.getTime()));
            getUsageLogsRequest.setTo("");
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithNullOutTime
     */
    @Test
    public void getUsageLogbyCriteriaWithNullOutTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(new Date()); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //getUsageLogsRequest.setTo(df.format(c.getTime()));
            getUsageLogsRequest.setTo(null);
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithOutTime
     */
    @Test
    public void getUsageLogbyCriteriaWithOutTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(new Date()); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //getUsageLogsRequest.setTo(df.format(c.getTime()));
            DateTimeFormatter df = _helper.getDateFormat();
            getUsageLogsRequest.setTo(new LocalDate().plusDays(1).toString(df));

            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithInvalidOutTime
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidOutTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();
            //DateFormat df = new SimpleDateFormat("dd/MM/yy");
            //Calendar c = Calendar.getInstance();
            //c.setTime(new Date()); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //getUsageLogsRequest.setTo(df.format(c.getTime()));

            DateTimeFormatter df = DateTimeFormat.forPattern("dd/MM/yy");
            getUsageLogsRequest.setTo(new LocalDate().plusDays(1).toString(df));

            ApiResponse getCoreUsagelog = new ApiResponse();
            try {
                getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());
                getCoreUsagelog.status = true;
            } catch (Exception ex) {
                getCoreUsagelog.status = false;
            }

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, false);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithEmptyInAndOutTime
     */
    @Test
    public void getUsageLogbyCriteriaWithEmptyInAndOutTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();

            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(new Date()); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //Date outTime = df.parse(df.format(c.getTime()));
            //LocalDate outTime = new LocalDate().plusDays(1);
            getUsageLogsRequest.setFrom("");
            getUsageLogsRequest.setTo("");
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithNullInAndOutTime
     */
    @Test
    public void getUsageLogbyCriteriaWithNullInAndOutTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();

            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(new Date()); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //Date outTime = df.parse(df.format(c.getTime()));
            getUsageLogsRequest.setFrom(null);
            getUsageLogsRequest.setTo(null);
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithInAndOutTime
     */
    @Test
    public void getUsageLogbyCriteriaWithInAndOutTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();

            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(new Date()); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //Date outTime = df.parse(df.format(c.getTime()));
            DateTimeFormatter df = _helper.getDateFormat();
            String outTime = new LocalDate().plusDays(1).toString(df);

            getUsageLogsRequest.setFrom(new LocalDate().toString(df));
            getUsageLogsRequest.setTo(outTime);
            ApiResponse getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, true);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageLogbyCriteriaWithInvalidInAndOutTime
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidInAndOutTime() {
        try {
            // Add 
            loggerService.addUsageLog(apiContext);

            // Get
            GetUsageLogsRequest getUsageLogsRequest = new GetUsageLogsRequest();

            //DateFormat df = new SimpleDateFormat("dd/MM/yy");
            //Calendar c = Calendar.getInstance();
            //c.setTime(new Date()); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //Date outTime = df.parse(df.format(c.getTime()));
            DateTimeFormatter df = DateTimeFormat.forPattern("dd/MM/yy");
            String outTime = new LocalDate().plusDays(1).toString(df);

            getUsageLogsRequest.setFrom(new LocalDate().toString(df));
            getUsageLogsRequest.setTo(outTime);
            ApiResponse getCoreUsagelog = new ApiResponse();

            try {
                getCoreUsagelog = loggerService.getUsageLogs(getUsageLogsRequest, apiContext.getClient().getId());
                getCoreUsagelog.status = true;
            } catch (Exception ex) {
                getCoreUsagelog.status = false;
            }

            // Get Assert
            Assert.assertEquals("Get Usage Log check", getCoreUsagelog.status, false);

            // Delete
            CoreUsagelog usageLog = dbService.getUsagelogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(usageLog);
            // Delete Assert
            Assert.assertEquals("Delete Usage Log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

}
