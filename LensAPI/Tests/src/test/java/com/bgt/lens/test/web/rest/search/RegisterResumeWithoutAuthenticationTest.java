// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.FilterSettings;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.criteria.SearchRegisterLogCriteria;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.SearchRegisterlog;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.FetchResume;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.RegisterDocument;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.CustomFilters;
import com.bgt.lens.model.searchservice.request.FetchResumeRequest;
import com.bgt.lens.model.searchservice.request.Filter;
import com.bgt.lens.model.searchservice.request.RegisterResumeRequest;
import com.bgt.lens.model.searchservice.request.UnregisterResumeRequest;
import com.bgt.lens.model.soap.response.searchservice.RegisterResumeResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletContext;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Register resume without authentication test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class RegisterResumeWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Instance type.
     */
    private final String instanceType = "SPECTRUM";
    /**
     * Locale type.
     */
    private final String localeTypeEnus = "en_us";
    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Invalid type.
     */
    private final String invalidType = "Invalid Instance Type";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor vendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Unregister Resume
     */
    private boolean unregisterResume;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");

            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            //Create vendor
            CreateVendorRequest createVendor = new CreateVendorRequest();
//            createVendor.setDocServerName(testHelper.docServerName);
            createVendor.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendor.setMaxDocumentCount((long) (500));
            createVendor.setVendor("resvendor01");
            createVendor.setType(com.bgt.lens.helpers.Enum.docType.resume.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendor);
            Assert.assertEquals("Add vendor check",
                    createVendorResponse.status, true);
            vendor = (Vendor) createVendorResponse.responseData;
            helper = new Helper();
//            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("resume");
//            apiContext.setClient(coreClient);
//            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            searchService.registerResume(registerResumeRequest);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeWithValidBinaryResumeData
     */
    @Test
    public final void registerResumeWithValidBinaryResumeData() {
        try {
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            //registerResumeRequest.setErrorOnDuplicateorOverwrite(com.bgt.lens.helpers.Enum.registerDuplicate.overwrite.toString());
            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            unregisterResume = true;

            RegisterResumeResponse registerResumeResponse = (RegisterResumeResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterResumeResponse());

            ObjectMapper mapper;
            byte[] json;

            if (registerResumeResponse != null && registerResumeResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(registerResumeResponse.responseData);
                RegisterDocument registerDocument = mapper.readValue(json, RegisterDocument.class
                );

                Assert.assertEquals("Register document Id", "1", registerDocument.getId());
                Assert.assertEquals("Register document count", 1, (long) registerDocument.getRegisteredDocumentCount());

//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
                int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register resume check log size", searchRegisterLogCount + 1, updatedSearchRegisterLogCount);

                ApiResponse unregisterResponse = unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeOverWriteDuplicateResumeId
     */
    @Test
    public final void registerResumeOverWriteDuplicateResumeId() {
        try {
            // Canon
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            ApiResponse registerResponse = searchService.registerResume(registerResumeRequest);
            Assert.assertTrue("Register resume ", registerResponse.status);

            // Set overwrite flag
            registerResumeRequest.setErrorOnDuplicateorOverwrite(com.bgt.lens.helpers.Enum.registerDuplicate.overwrite.toString());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            RegisterResumeResponse registerResumeResponse = (RegisterResumeResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterResumeResponse());

            ObjectMapper mapper;
            byte[] json;

            if (registerResumeResponse != null && registerResumeResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(registerResumeResponse.responseData);
                RegisterDocument registerDocument = mapper.readValue(json, RegisterDocument.class
                );

                Assert.assertEquals("Register document Id", "1", registerDocument.getId());
                Assert.assertEquals("Register document count", 1, (long) registerDocument.getRegisteredDocumentCount());

//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
                int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register resume check log size", searchRegisterLogCount + 1, updatedSearchRegisterLogCount);

                ApiResponse unregisterResponse = unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);

            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeErrorDuplicateResumeId
     */
    @Test
    public final void registerResumeErrorDuplicateResumeId() {
        try {
            // Canon
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            ApiResponse registerResponse = searchService.registerResume(registerResumeRequest);
            Assert.assertTrue("Register resume ", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            RegisterResumeResponse registerResumeResponse = (RegisterResumeResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterResumeResponse());

            if (registerResumeResponse != null && (!registerResumeResponse.status)) {
                Assert.assertTrue("Register resume duplicate response check", ((String) registerResumeResponse.responseData).contains("duplicate id"));
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            ApiResponse unregisterResponse = unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            Assert.assertTrue("Unregister resume request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeEmptyBinaryData
     */
    @Test
    public final void registerResumeEmptyBinaryData() {
        try {
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData("");
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register resume empty binary data", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeNullBinaryData
     */
    @Test
    public final void registerResumeNullBinaryData() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(null);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register resume null binary data", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeWithoutVendor
     */
    @Test
    public final void registerResumeWithoutVendor() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterResume("1", instanceType, "", TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor("");

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertTrue("Register resume without vendor", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeWithoutLocale
     */
    @Test
    public final void registerResumeWithoutLocale() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(null);
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register resume without locale", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeWithoutInstanceType
     */
    @Test
    public final void registerResumeWithoutInstanceType() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(null);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register resume without instance type", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeWithInvalidInstanceType
     */
    @Test
    public final void registerResumeWithInvalidInstanceType() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType("XRAY");
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register resume invalid instance type", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeWithInvalidVendor
     */
    @Test
    public final void registerResumeWithInvalidVendor() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor("sample");

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeMoreThanMaxAllowedCount
     */
    @Test
    public final void registerResumeMoreThanMaxAllowedCount() {
        try {
            // Canon
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            ApiResponse updateVendorCountResponse = searchService.updateVendor(vendor.getVendorId(), 1, 0, false);
            Assert.assertTrue("Update vendor maximum document count", updateVendorCountResponse.status);
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());

            ApiResponse registerResponse = searchService.registerResume(registerResumeRequest);
            Assert.assertTrue("Register resume response", registerResponse.status);

            // Register Assert
            registerResumeRequest.setResumeId("2");
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeWithInvalidLENSSettings
     */
    @Test
    public final void registerResumeWithInvalidLENSSettings() {
        try {
            // Canon
            UpdateLensSettingsRequest lensSettings = new UpdateLensSettingsRequest();
            lensSettings.setHost("EmptyHost");
            lensSettings.setInstanceType("Spectrum");
            lensSettings.setStatus(true);
            ApiResponse updateLensSettingsResponse = coreService.updateLensSettings(enusLensSettings.getId(), lensSettings);
            Assert.assertTrue("Update LENS settings", updateLensSettingsResponse.status);
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerResumeWithInvalidBinaryDataResume
     */
    @Test
    public final void registerResumeWithInvalidBinaryDataResume() {
        try {
            // Canon
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_TaggingError.doc";
            InputStream resourceAsStream = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream));
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            String extension = fileName.substring(fileName.indexOf('.') + 1);
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setExtension(extension);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register resume check log size", searchRegisterLogCount , updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume with valid custom filters
     */
    @Test
    public final void registerResumeWithValidCustomFilters() {
        try {
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            updateRequest.setHost(testhelper.hostname);
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            registerResumeRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("bgldlanguage");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerResumeRequest.setCustomFilters(filters);

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            unregisterResume = true;
            Thread.sleep(1000);
            RegisterResumeResponse registerResumeResponse = (RegisterResumeResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterResumeResponse());

            if (registerResumeResponse != null && registerResumeResponse.status) {
                FetchResumeRequest fetchResumeRequest = new FetchResumeRequest();
                fetchResumeRequest.setInstanceType(instanceType);
                fetchResumeRequest.setLocale(TestHelper.Locale.en_us.name());
                fetchResumeRequest.setVendor(vendor.getVendor());
                fetchResumeRequest.setResumeId("1");
                response = searchService.fetchResume(fetchResumeRequest);
                Assert.assertEquals("Fetch resume response status check", true, response.status);

                FetchResume resume = (FetchResume) response.responseData;
                Assert.assertTrue("Fetch resume response message check", ((String) resume.getResume()).contains("<bgldlanguage>1</bgldlanguage>"));

                ApiResponse unregisterResponse = unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister resume request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume with valid custom filters
     */
    @Test
    public final void registerResumeWithoutCustomFiltersReturnsFailure() {
        try {
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            registerResumeRequest.setRegisterWithCustomFilters(true);
//            Filter filter01 = new Filter();
//            filter01.setName("Language");
//            filter01.setValue(BigInteger.valueOf(1));
//            CustomFilters filters = new CustomFilters();
//            filters.getFilter().add(filter01);
//            registerResumeRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register resume response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume with custom filters without custom filter configurated
     * LENS settings
     */
    @Test
    public final void registerResumeWithCustomFiltersWithEmptyCustomFilterLensReturnsFailure() {
        try {
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            registerResumeRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("Language");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerResumeRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            ApiResponse response = new ApiResponse();
            response.status = true;

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume with invalid custom filters
     */
    @Test
    public final void registerResumeWithInvalidCustomFilterReturnsFailure() {
        try {
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("language");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            registerResumeRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("test");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerResumeRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register resume response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);
            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume with invalid custom filter name
     */
    @Test
    public final void registerResumeWithEmptyCustomFilterNameReturnsFailure() {
        try {
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("language");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            registerResumeRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerResumeRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register resume response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);
            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume with null custom filter value
     */
    @Test
    public final void registerResumeWithNullCustomFilterValueReturnsFailure() {
        try {
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("language");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            registerResumeRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("language");
            filter01.setValue(null);
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerResumeRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register resume response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);
            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register resume with duplicate custom filter value
     */
    @Test
    public final void registerResumeWithDuplicateCustomFilterReturnsFailure() {
        try {
            unregisterResume("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("language");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getResumeFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
            registerResumeRequest.setInstanceType(instanceType);
            registerResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            registerResumeRequest.setBinaryData(binaryData);
            registerResumeRequest.setResumeId("1");
            registerResumeRequest.setVendor(vendor.getVendor());
            registerResumeRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("language");
            filter01.setValue((long) (1));
            Filter filter02 = new Filter();
            filter01.setName("language");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            filters.getFilter().add(filter02);
            registerResumeRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();

            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterResume)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register resume response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register resume check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);
            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Unregister resume
     *
     * @param resumeId
     * @param instanceType
     * @param vendorName
     * @param locale
     * @return
     * @throws Exception
     */
    public ApiResponse unregisterResume(String resumeId, String instanceType, String vendorName, String locale) throws Exception {
        UnregisterResumeRequest unregisterRequest = new UnregisterResumeRequest();
        unregisterRequest.setResumeId(resumeId);
        unregisterRequest.setVendor(vendorName);
        unregisterRequest.setInstanceType(instanceType);
        unregisterRequest.setLocale(locale);

        ApiResponse unregisterResponse = searchService.unregisterResume(unregisterRequest);
        return unregisterResponse;
    }
}
