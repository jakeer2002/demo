/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.ParseJobRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * *
 * Parse Job Structured BGT XML test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ParseJobStructuredBGXMLTests {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Autowired
    IUtilityService utilityService;

    /**
     * API context
     */
    public ApiContext apiContext;

    /**
     * API
     */
    public Api api;

    /**
     * Lens settings
     */
    public LensSettings en_usLensSettings, en_ukLensSettings, localeSettings, JMLensSettings, JM_GB_LensSettings, JM_SG_LensSettings;

    /**
     * Resource
     */
    public Resources resource;

    /**
     * Client
     */
    public Client client;

    /**
     * Client
     */
    public CoreClient coreClient;

    /**
     * Test helper object
     */
    public TestHelper testHelper;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private String binaryData;

    /**
     * Job parsing variant
     */
    public static final String variant = "structuredbgtxml";

    /**
     * Instance type
     */
    public static final String InstanceType = "SPECTRUM";

    /**
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     *
     * @throws JAXBException
     */
    @Before
    public void setUp() throws JAXBException {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        try {
            String propFileName = "/JobTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            // create api
            CreateApiRequest createApi = testHelper.getCreateApiRequest("/input/CreateApi.xml");
            //CreateApiRequest createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(ParseJobStructuredBGXMLTests.class.getResourceAsStream("/input/CreateApi.xml"));
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettingsForJob.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ParseJobStructuredBGXMLTests.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setCountry("UnitedStates");
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;

            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;
            
            //creating a LENS Setting for Optic Jobs
            settingsRequest.setHost(testHelper.JM_Hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.JM_Port)));
            settingsRequest.setCharSet(testHelper.JM_Encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.JM_TimeOut)));
            settingsRequest.setInstanceType(testHelper.JM_InstanceType);
            settingsRequest.setLocale(testHelper.JM_Locale);
            settingsRequest.setStatus(testHelper.JM_Status);
            settingsRequest.setCountry(testHelper.JM_Country);
            // creating Settings Response for Optic Jobs
            ApiResponse JMLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", JMLensSettingsResponse.status, true);
            JMLensSettings = (LensSettings) JMLensSettingsResponse.responseData;
			
			//creating a LENS Setting for GB Optic Jobs
            settingsRequest.setHost(testHelper.JM_GB_Hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.JM_GB_Port)));
            settingsRequest.setCharSet(testHelper.JM_Encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.JM_TimeOut)));
            settingsRequest.setInstanceType(testHelper.JM_InstanceType);
            settingsRequest.setLocale(testHelper.JM_GB_Locale);
            settingsRequest.setStatus(testHelper.JM_Status);
            settingsRequest.setCountry(testHelper.JM_Country);
            // creating Settings Response for GB Optic Jobs
            ApiResponse JMGBLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", JMGBLensSettingsResponse.status, true);
            JM_GB_LensSettings = (LensSettings) JMGBLensSettingsResponse.responseData;

			//creating a LENS Setting for SG Optic Jobs
            settingsRequest.setHost(testHelper.JM_SG_Hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.JM_SG_Port)));
            settingsRequest.setCharSet(testHelper.JM_Encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.JM_TimeOut)));
            settingsRequest.setInstanceType(testHelper.JM_InstanceType);
            settingsRequest.setLocale(testHelper.JM_SG_Locale);
            settingsRequest.setStatus(testHelper.JM_Status);
            settingsRequest.setCountry("Singapore");
            // creating Settings Response for SG Optic Jobs
            ApiResponse JMSGLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_sg check", JMSGLensSettingsResponse.status, true);
            JM_SG_LensSettings = (LensSettings) JMSGLensSettingsResponse.responseData;            
            
            // create resource
            AddResourceRequest resourceRequest = testHelper.getAddResourceRequest("/input/CreateResource.xml");
            //AddResourceRequest resourceRequest = (AddResourceRequest) jaxbUnmarshaller.unmarshal(ParseJobStructuredBGXMLTests.class.getResourceAsStream("/input/CreateResource.xml"));
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            //CreateConsumerRequest consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(ParseJobStructuredBGXMLTests.class.getResourceAsStream("/input/CreateConsumer.xml"));

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();

            settingsList.getInstanceList().clear();

            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList JMinstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList JMGBinstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList JMSGinstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_usLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_ukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            JMinstanceList.setInstanceId(new BigInteger(Integer.toString(JMLensSettings.getId())));
            JMinstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(JMinstanceList);
			
            JMGBinstanceList.setInstanceId(new BigInteger(Integer.toString(JM_GB_LensSettings.getId())));
            JMGBinstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(JMGBinstanceList);
            JMSGinstanceList.setInstanceId(new BigInteger(Integer.toString(JM_SG_LensSettings.getId())));
            JMSGinstanceList.setRateLimitEnabled(false);            
            settingsList.getInstanceList().add(JMSGinstanceList);

            consumer.setLensSettings(settingsList);

            consumer = testHelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;

            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);

            // en_us LensSettings
            CoreLenssettings en_usCoreLenssettings = new CoreLenssettings();
            en_usCoreLenssettings.setId(1);
            en_usCoreLenssettings.setHost(testHelper.hostname);
            en_usCoreLenssettings.setPort(testHelper.port);
            en_usCoreLenssettings.setCharacterSet(testHelper.encoding);
            en_usCoreLenssettings.setTimeout(testHelper.timeOut);
            en_usCoreLenssettings.setInstanceType(testHelper.instanceType);
            en_usCoreLenssettings.setLocale(testHelper.locale);
            en_usCoreLenssettings.setStatus(testHelper.status);

            // en_uk LensSettings
            CoreLenssettings en_ukCoreLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            en_ukCoreLenssettings.setId(2);
            en_ukCoreLenssettings.setLocale(LocaleTypeEn_uk);

            // LD LensSettings
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(3);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            
            // JM LensSettings
            CoreLenssettings JMCoreLenssettings = new CoreLenssettings();
            JMCoreLenssettings.setId(4);
            JMCoreLenssettings.setHost(testHelper.JM_Hostname);
            JMCoreLenssettings.setPort(testHelper.JM_Port);
            JMCoreLenssettings.setCharacterSet(testHelper.JM_Encoding);
            JMCoreLenssettings.setTimeout(testHelper.JM_TimeOut);
            JMCoreLenssettings.setInstanceType(testHelper.JM_InstanceType);
            JMCoreLenssettings.setLocale(testHelper.JM_Locale);
            JMCoreLenssettings.setStatus(testHelper.JM_Status);
			
			// JM GB LensSettings
            CoreLenssettings JMGBCoreLenssettings = new CoreLenssettings();
            JMGBCoreLenssettings.setId(5);
            JMGBCoreLenssettings.setHost(testHelper.JM_GB_Hostname);
            JMGBCoreLenssettings.setPort(testHelper.JM_GB_Port);
            JMGBCoreLenssettings.setCharacterSet(testHelper.JM_Encoding);
            JMGBCoreLenssettings.setTimeout(testHelper.JM_TimeOut);
            JMGBCoreLenssettings.setInstanceType(testHelper.JM_InstanceType);
            JMGBCoreLenssettings.setLocale(testHelper.JM_GB_Locale);
            JMGBCoreLenssettings.setStatus(testHelper.JM_Status);

            // JM SG LensSettings
            CoreLenssettings JMSGCoreLenssettings = new CoreLenssettings();
            JMSGCoreLenssettings.setId(6);
            JMSGCoreLenssettings.setHost(testHelper.JM_SG_Hostname);
            JMSGCoreLenssettings.setPort(testHelper.JM_SG_Port);
            JMSGCoreLenssettings.setCharacterSet(testHelper.JM_Encoding);
            JMSGCoreLenssettings.setTimeout(testHelper.JM_TimeOut);
            JMSGCoreLenssettings.setInstanceType(testHelper.JM_InstanceType);
            JMSGCoreLenssettings.setLocale(testHelper.JM_SG_Locale);
            JMSGCoreLenssettings.setStatus(testHelper.JM_Status);    
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings JMCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings JMGBCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings JMSGCoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();

            en_usCoreClientlenssettings.setCoreLenssettings(en_usCoreLenssettings);
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(en_ukCoreLenssettings);
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);
            coreClientlenssettingses.add(locale_CoreClientlenssettings);
            JMCoreClientlenssettings.setCoreLenssettings(JMCoreLenssettings);
            coreClientlenssettingses.add(JMCoreClientlenssettings);
            JMGBCoreClientlenssettings.setCoreLenssettings(JMGBCoreLenssettings);
            coreClientlenssettingses.add(JMGBCoreClientlenssettings);
            JMSGCoreClientlenssettings.setCoreLenssettings(JMSGCoreLenssettings);
            coreClientlenssettingses.add(JMSGCoreClientlenssettings);
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);

            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            //coreClientapis.add(clientapis.get(0));
            //coreClient.setCoreClientapis(coreClientapis);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("Post");
            apiContext.setResource("job");
            apiContext.setInstanceType(InstanceType);
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            String fileName = "/TestJobs/10002.txt";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete", deleteApiResponse.status, true);
            // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete", deleteConsumerResponse.status, true);
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);
            ApiResponse JMDeleteSettingsResponse = coreService.deleteLensSettings(JMLensSettings.getId());
            Assert.assertEquals("Delete locale settings delete", JMDeleteSettingsResponse.status, true);
	    ApiResponse JMGBDeleteSettingsResponse = coreService.deleteLensSettings(JM_GB_LensSettings.getId());
            Assert.assertEquals("Delete locale settings delete", JMGBDeleteSettingsResponse.status, true);
            ApiResponse JMSGDeleteSettingsResponse = coreService.deleteLensSettings(JM_SG_LensSettings.getId());
            Assert.assertEquals("Delete en_sg locale settings delete", JMSGDeleteSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    
    /**
     * *
     * ParseJobBGTXMLWithSubOccElement
     */
    
    @Test
    public void parseJobStructuredBGTXMLWithSubOccElementOpticJobs() {
        try {
            
            String fileName = "/TestJobs/Jobpdf.pdf";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(testHelper.JM_InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXMLWithSubOccElement With ValidBinary JobData check", response.status, true);
            // ParseJobBGTXML Assert
            String expResult = "SubOcc";
            //System.out.println("response.responseData");
            //System.out.println(response.responseData);
            String convertJob = (String) response.responseData;
            //System.out.println(convertJob);
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobBGTXMLWithSubOccElement With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithSubOccElement
     */
    @Test
    public void parseJobStructuredBGTXMLWithWfhOpticJobs() {
        try {
            
            String fileName = "/TestJobs/Jobpdf.pdf";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(testHelper.JM_InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXMLWithSubOccElement With ValidBinary JobData check", response.status, true);
            // ParseJobBGTXML Assert
            String expResult = "wfh";
            //System.out.println("response.responseData");
            //System.out.println(response.responseData);
            String convertJob = (String) response.responseData;
            //System.out.println(convertJob);
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobBGTXMLWithSubOccElement With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseJobBGTXMLWithSubOccElement
     */
    
    @Test
    public void parseJobStructuredBGTXMLWithApprenticeshipFlagOpticJobs() {
        try {
            
            String fileName = "/TestJobs/Jobpdf.pdf";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(testHelper.JM_InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_gb.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXMLWithApprenticeshipFlagElement With ValidBinary JobData check", response.status, true);
            // ParseJobBGTXML Assert
            String expResult = "ApprenticeshipFlag";
            //System.out.println("response.responseData");
            //System.out.println(response.responseData);
            String convertJob = (String) response.responseData;
            //System.out.println(convertJob);
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobBGTXMLWithApprenticeshipFlagElement With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseJobBGTXMLWithSubOccElement
     */
    @Test
    public void parseJobStructuredBGTXMLWithCanonSkillTypeOpticJobs() {
        try {
            
            String fileName = "/TestJobs/job.txt";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(testHelper.JM_InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("parseJobStructuredBGTXMLWithCanonSkillTypeOpticJobs With ValidBinary JobData check", response.status, true);
            // ParseJobBGTXML Assert
            String expResult = "canonskilltype";
            //System.out.println("response.responseData");
            //System.out.println(response.responseData);
            String convertJob = (String) response.responseData;
            //System.out.println(convertJob);
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("parseJobStructuredBGTXMLWithCanonSkillTypeOpticJobs With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    
    @Test
    public void parseJobStructuredBGTXMLWithCanonSkillTypeOpticJobsGBR() {
        try {
            
            String fileName = "/TestJobs/Jobtext.txt";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(testHelper.JM_InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_gb.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("parseJobStructuredBGTXMLWithCanonSkillTypeOpticJobs With ValidBinary JobData check", response.status, true);
            // ParseJobBGTXML Assert
            String expResult = "canonskilltype";
            //System.out.println("response.responseData");
            //System.out.println(response.responseData);
            String convertJob = (String) response.responseData;
            //System.out.println(convertJob);
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("parseJobStructuredBGTXMLWithCanonSkillTypeOpticJobs With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    @Test
    public void parseJobStructuredBGTXMLWithCanonSkillTypeOpticJobsSGP() {
        try {
            
            String fileName = "/TestJobs/Jobtext.txt";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(testHelper.JM_InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_sg.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("parseJobStructuredBGTXMLWithCanonSkillTypeOpticJobs With ValidBinary JobData check", response.status, true);
            // ParseJobBGTXML Assert
            String expResult = "canonskilltype";
            //System.out.println("response.responseData");
            //System.out.println(response.responseData);
            String convertJob = (String) response.responseData;
            //System.out.println(convertJob);
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("parseJobStructuredBGTXMLWithCanonSkillTypeOpticJobs With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * parseJobStructuredBGTXMLWithCanonSkillIDElement
     */
    @Test
    public void parseJobStructuredBGTXMLWithCanonSkillIDElementOpticJobs() {
        try {
            
            String fileName = "/TestJobs/job.txt";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(testHelper.JM_InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("parseJobStructuredBGTXMLWithCanonSkillIDElement With ValidBinary JobData check", response.status, true);
            
            // ParseJobBGTXML Assert
            String expResult = "canonskillid";
            //System.out.println("response.responseData");
            //System.out.println(response.responseData);
            String convertJob = (String) response.responseData;
            //System.out.println(convertJob);
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            
            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("parseJobStructuredBGTXMLWithCanonSkillIDElement With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseJobBGTXMLWithValidBinaryJobData
     */
    @Test
    public void parseJobStructuredBGTXMLWithValidBinaryJobData() {
        try {
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary JobData check", response.status, true);

            // ParseJobBGTXML Assert
            String expResult = "JobDoc";
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobStructuredBGTXMLWithValidBinaryJobDataWithDefaultLocale() {
        try {
            // ParseJobBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry(TestHelper.Locale.en_us.name());
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary JobData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "JobDoc";
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobStructuredBGTXMLWithValidBinaryJobDataLocaleBasedParsing() {
        try {
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "en_us", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeBGTXML Assert
            String expResult = "JobDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobStructuredBGTXMLWithValidBinaryJobDataLocaleBasedParsingWithDefaultCountry() {
        try {
            String fileName = "/TestJobs/10001.txt";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "en_xx", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeBGTXML Assert
            String expResult = "JobDoc";
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobStructuredBGTXMLWithValidBinaryJobDataLocaleBasedParsingWithUnknownLocale() {
        try {
            String fileName = "/TestResumes/UnknownLocale.doc";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "xl_xx", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeBGTXML Assert
            String expResult = "JobDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobStructuredBGTXMLWithValidBinaryJobDataLocaleBasedParsingWithUnallocatedLocale() {
        try {
            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "en_au", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeBGTXML Assert
            String expResult = "JobDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobStructuredBGTXMLWithValidBinaryJobDataLocaleBasedParsingWithInactiveInstance() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);

            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());
            coreClientApiContext.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClientApiContext.setEnableLocaleDetection(true);
            apiContext.setClient(coreClientApiContext);
            _helper.setApiContext(apiContext);

            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With InActive Lens check", response.status, false);
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            
            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithConvertError
     */
    @Test
    public void parseJobStructuredBGTXMLWithConvertError() {
        try {
            String fileName = "/TestJobs/EmptyJob/ExternalJob.doc";
            InputStream resourceAsStream04 = ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseJobStructuredBGXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With Empty BinaryData check", response.status, false);

            // ParseJobBGTXML Assert
            String convertJob = (String) response.responseData;
            String expResult = "Converters returned empty output";
            Assert.assertEquals("ParseJobStructuredBGTXML With convert error Check", apiErrors.getLensErrorMessage(expResult), convertJob);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobStructuredBGTXML With Empty BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLWithConvertErrorStatus
     */
    @Test
    public void parseJobStructuredBGTXMLWithConvertErrorStatus() {
        try {
            String fileName = "/TestJobs/EmptyJob/ExternalJob.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseJobVariant(parseJobRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseJobStructuredBGTXML With Null BinaryData check", response.status, false);

            // ParseJobBGTXML Assert
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobStructuredBGTXML With Null BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLSpecificLensInstance
     */
    @Test
    public void parseJobStructuredBGTXMLSpecificLensInstance() {
        try {
            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobBGTXML Specific Lens Instance check", response.status, true);

            // ParseJobBGTXML Assert
            String expResult = "JobDoc";
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);

            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);

            // ParseJobBGTXML Transaction Count Assert
            Assert.assertEquals("ParseJobStructuredBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLUnauthorizedInstance
     */
    @Test
    public void parseJobStructuredBGTXMLUnauthorizedInstance() {
        try {
            // ParseJobBGTXML
            String localeType = "en_anz";
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale(localeType);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With Unauthorized Instance check", response.status, false);

            // ParseJobBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLEmptyInstanceList
     */
    @Test
    public void parseJobStructuredBGTXMLEmptyInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseJobVariant(parseJobRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseJobStructuredBGTXML With Empty Instance List check", response.status, false);

            // ParseJobBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLNullInstanceList
     */
    @Test
    public void parseJobStructuredBGTXMLNullInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseJobVariant(parseJobRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseJobStructuredBGTXML With Null Instance List check", response.status, false);

            // ParseJobBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLInvalidLensList
     */
    @Test
    public void parseJobStructuredBGTXMLInvalidLensList() {
        try {
            //Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With Lens Invalid  check", response.status, false);

            // ParseJobBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);

            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobBGTXMLInActiveLensList
     */
    @Test
    public void parseJobStructuredBGTXMLInActiveLensList() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseJobBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.parseJobVariant(parseJobRequest, variant);
            Assert.assertEquals("ParseJobStructuredBGTXML With InActive Lens check", response.status, false);

            // ParseJobBGTXML inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            Assert.assertEquals("ParseJobStructuredBGTXML With InActive Lens check", _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS), instanceResponse);
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);

            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
