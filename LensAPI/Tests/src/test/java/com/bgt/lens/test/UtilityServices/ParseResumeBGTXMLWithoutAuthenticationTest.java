// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * *
 * Parse resume BGT XML test without authentication
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ParseResumeBGTXMLWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();


    @Autowired
    ICoreService coreService;

    @Autowired
    IUtilityService utilityService;

    /**
     * *
     * API context
     */
    public ApiContext apiContext;

    /**
     * *
     * API
     */
    public Api api;

    /**
     * *
     * Lens settings
     */
    public LensSettings en_usLensSettings, en_ukLensSettings, localeSettings;

    /**
     * *
     * Resource
     */
    public Resources resource;

    /**
     * *
     * Client
     */
    public Client client;

    /**
     * *
     * Test helper object
     */
    public TestHelper testHelper;

    /**
     * *
     * Helper object
     */
    public Helper _helper;

    /**
     * *
     * Extension
     */
    private String extension;

    /**
     * *
     * Binary data
     */
    private String binaryData;

    /**
     * *
     * Resume parsing variant
     */
    public static final String variant = "bgtxml";

    /**
     * *
     * Instance type
     */
    public static String InstanceType = "XRay";

    /**
     * *
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * *
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * *
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * *
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     */
    @Before
    public void setUp() {
           ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
           servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        try {
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            InstanceType = testHelper.instanceType;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ParseResumeBGTXMLWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;
            
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("Post");
            apiContext.setResource("resume");
            apiContext.setClient(null);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = ParseResumeBGTXMLWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);

        } catch (IOException ex) {
            System.out.print(ex);
            Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ParseResumeBGTXMLWithoutAuthenticationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    
    @Test
    public void parseResumeBGTXMLWithoutAuthenticationWithLocalewithMicroservice() {
        try {
            // ParseResumeBGTXML
            String fileName = "/TestResumes/BoldResume11.pdf";
            InputStream resourceAsStream04 = ParseResumeBGTXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariantMicroservice(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML WithoutAuthentication With ValidBinary ResumeData check", response.status, true);
            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseResumeBGTXMLWithoutAuthenticationWithValidBinaryResumeData
     */
    @Test
    public void parseResumeBGTXMLWithoutAuthenticationWithValidBinaryResumeData() {
        try {
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML WithoutAuthentication With ValidBinary ResumeData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeBGTXMLWithValidBinaryResumeDataWithLocaleBasedParsing() {
        try {
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML WithoutAuthentication With ValidBinary ResumeData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeBGTXMLWithValidBinaryResumeDataLocaleBasedParsingWithInvalidLocale() {
        try {
            // ParseResumeBGTXML
            String fileName = "/TestResumes/Resume_JPN.txt";
            InputStream resourceAsStream04 = ParseResumeBGTXMLWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML WithoutAuthentication With ValidBinary ResumeData check", response.status, false);

            // ParseResumeBGTXML Assert
            String expResult = "missing";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithoutAuthenticationWithConvertError
     */
    @Test
    public void parseResumeBGTXMLWithoutAuthenticationWithConvertError() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            InputStream resourceAsStream04 = ParseResumeBGTXMLWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML WithoutAuthentication With Empty BinaryData check", response.status, false);

            // ParseResumeBGTXML Assert            
            String convertResume = (String) response.responseData;
            String expResult = "Converters returned empty output";
            Assert.assertEquals("ParseResumeBGTXML WithoutAuthentication With convert error Check", apiErrors.getLensErrorMessage(expResult), convertResume);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithoutAuthenticationWithConvertErrorStatus
     */
    @Test
    public void parseResumeBGTXMLWithoutAuthenticationWithConvertErrorStatus() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResumeBGTXML WithoutAuthentication With Null BinaryData check", response.status, false);

            // ParseResumeBGTXML Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithoutAuthenticationSpecificInstance
     */
    @Test
    public void parseResumeBGTXMLWithoutAuthenticationSpecificInstance() {
        try {
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_uk);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML Without Authentication Specific Instance check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithoutAuthenticationUnauthorizedInstance
     */
    @Test
    public void parseResumeBGTXMLWithoutAuthenticationUnauthorizedInstance() {
        try {
            // ParseResumeBGTXML
            String localeType = "en_anz";
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(localeType);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML Without Authentication Unauthorized Instance check", response.status, false);

            // ParseResumeBGTXML Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithoutAuthenticationInvalidHostNameList
     */
    @Test
    public void parseResumeBGTXMLWithoutAuthenticationInvalidHostNameList() {
        try {
            // create lens settings
            JAXBContext jaxbContext = JAXBContext.newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            InputStream resourceAsStream01 = ParseResumeBGTXMLWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale("en_sgp");
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML Without Authentication Invalid HostName check", response.status, false);

            // ParseResumeBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));            

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithoutAuthenticationInActiveLensList
     */
    @Test
    public void parseResumeBGTXMLWithoutAuthenticationInActiveLensList() {
        try {
            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(false);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale("en_sgp");
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML Without Authentication InActive Lens check", response.status, false);

            // ParseResumeBGTXML inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            Assert.assertEquals("ParseResumeBGTXML WithoutAuthentication InActive Lens check", _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS), instanceResponse);

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
