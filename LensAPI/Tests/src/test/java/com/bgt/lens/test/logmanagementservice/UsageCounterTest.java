// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.logmanagementservice;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.GetUsageCounterRequest;
import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreUsagecounter;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.logger.ILoggerService;
import java.util.List;
import javax.servlet.ServletContext;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Usage counter test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class UsageCounterTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    

    @Autowired
    ICoreService coreService;

    @Autowired
    ILoggerService loggerService;

    @Autowired
    IDataRepositoryService dbService;

    /**
     * Client
     */
    public CoreClient client = null;

    /**
     * Client API
     */
    public CoreClientapi clientApi = null;

    /**
     * API
     */
    public CoreApi api = null;

    /**
     * API response
     */
    public Api apiResponse = null;

    /**
     * API context
     */
    public ApiContext apiContext = null;

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * Helper object
     */
    private static final Helper _helper = new Helper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
           servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        try {
            // Create Api
            CreateApiRequest createApi = new CreateApiRequest();
            createApi.setKey("Core");
            createApi.setName("Core");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            apiResponse = (Api) response.responseData;

            // Client Id
            client = new CoreClient();
            client.setId(1);

            // Api Id
            clientApi = new CoreClientapi();
            api = new CoreApi();
            api.setId(apiResponse.getId());
            api.setApiKey(apiResponse.getKey());
            clientApi.setCoreApi(api);

            // ApiContext
            apiContext = new ApiContext();

            apiContext.setClient(client);
            apiContext.setClientApi(clientApi);
            apiContext.setMethod("GET");
            apiContext.setResource("clients");
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper.setApiContext(apiContext);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            ApiResponse deleteApiResponse = coreService.deleteApi(apiResponse.getId());
            Assert.assertEquals("Create Api delete", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsageCounterWithValidData
     */
    @Test
    public void addUsageCounterWithValidData() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                ApiResponse getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), coreUsagecounterList.get(0).getClientId());
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
            
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addUsageCounterWithInvalidClientId
     */
    @Test
    public void addUsageCounterWithInvalidClientId() {
        try {
            // Add
            CoreClient client = new CoreClient();
            client.setId(-1);
            apiContext.setClient(client);
//            api.setId(1);
//            clientApi.setCoreApi(api);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                loggerService.updateUsageCounter(apiContext);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Add Assert
            Assert.assertEquals("Add Usage Counter Invalid Client Id check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addUsagecounterWithInvalidApiId
     */
    @Test
    public void addUsagecounterWithInvalidApiId() {
        try {
            // Add
//            client.setId(1);
            api.setId(-1);
            clientApi.setCoreApi(api);

            ApiResponse addApiResponse = new ApiResponse();
            try {
                loggerService.updateUsageCounter(apiContext);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("Add Usage Counter Invalid Api Id check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * updateUsageCounterWithValidData
     */
    @Test
    public void updateUsageCounterWithValidData() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);
            
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                ApiResponse getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), coreUsagecounterList.get(0).getClientId());
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);
            }
            else{
                Assert.fail();
            }

            // Update usage counter
            loggerService.updateUsageCounter(apiContext);

            criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                ApiResponse getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), coreUsagecounterList.get(0).getClientId());
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);
                
                getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), coreUsagecounterList.get(0).getClientId());
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);
                Assert.assertEquals("Get Usage Counter check", 2, coreUsagecounterList.get(0).getUsageCount());

                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyIdWithValidLogId
     */
    // GetById
    @Test
    public void getUsageCounterbyIdWithValidLogId() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                ApiResponse getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), coreUsagecounterList.get(0).getClientId());
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
            
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyIdWithInvalidLogId
     */
    @Test
    public void getUsageCounterbyIdWithInvalidLogId() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            ApiResponse getCoreUsagecounter = loggerService.getUsageCounterById(123, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, false);

            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), coreUsagecounterList.get(0).getClientId());
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyIdWithValidClientId
     */
    @Test
    public void getUsageCounterbyIdWithValidClientId() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                ApiResponse getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), coreUsagecounterList.get(0).getClientId());
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyIdWithInvalidClientId
     */
    @Test
    public void getUsageCounterbyIdWithInvalidClientId() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                
                ApiResponse getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), 123);
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, false);
                
                getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), coreUsagecounterList.get(0).getClientId());
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getListofAllUsageCounter
     */
    // GetByCriteria
    @Test
    public void getListofAllUsageCounter() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                
                ApiResponse getCoreUsagecounter = loggerService.getUsageCounterById(coreUsagecounterList.get(0).getId(), coreUsagecounterList.get(0).getClientId());
                // Get Assert
                Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithApiKey
     */
    @Test
    public void getUsageCounterbyCriteriaWithApiKey() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setApiKey(api.getApiKey());
            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithInvalidApiKey
     */
    @Test
    public void getUsageCounterbyCriteriaWithInvalidApiKey() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setApiKey("Invalid");
            ApiResponse getCoreUsagecounter = new ApiResponse();
            try {
                getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
                getCoreUsagecounter.status = true;
            } catch (Exception ex) {
                getCoreUsagecounter.status = false;
            }
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, false);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithHttpMethod
     */
    @Test
    public void getUsageCounterbyCriteriaWithHttpMethod() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setMethod(apiContext.getMethod());
            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithInvalidHttpMethod
     */
    @Test
    public void getUsageCounterbyCriteriaWithInvalidHttpMethod() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setMethod("Invalid");
            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, false);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithResource
     */
    @Test
    public void getUsageCounterbyCriteriaWithResource() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setResource(apiContext.getResource());
            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithInvalidResource
     */
    @Test
    public void getUsageCounterbyCriteriaWithInvalidResource() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setResource("Invalid");
            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, false);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithEmptyTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithEmptyTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setFrom("");
            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithNullTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithNullTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setFrom(null);
            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithInTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithInTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            DateTimeFormatter df = _helper.getDateFormat();
            getUsageCounterRequest.setFrom(new LocalDate().toString(df));
            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithInvalidInTimeFormat
     */
    @Test
    public void getUsageCounterbyCriteriaWithInvalidInTimeFormat() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            DateTimeFormatter df = DateTimeFormat.forPattern("dd/MM/yy");
            getUsageCounterRequest.setFrom(new LocalDate().plusDays(1).toString(df));
            ApiResponse getCoreUsagecounter = new ApiResponse();
            try {
                getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
                getCoreUsagecounter.status = true;
            } catch (Exception ex) {
                getCoreUsagecounter.status = false;
            }
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, false);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithEmptyOutTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithEmptyOutTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setTo("");

            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithNullOutTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithNullOutTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setTo(null);

            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithOutTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithOutTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            DateTimeFormatter df = _helper.getDateFormat();
            getUsageCounterRequest.setTo(new LocalDate().plusDays(1).toString(df));

            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithInvalidOutTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithInvalidOutTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            DateTimeFormatter df = DateTimeFormat.forPattern("dd/MM/yy");
            getUsageCounterRequest.setTo(new LocalDate().plusDays(1).toString(df));

            ApiResponse getCoreUsagecounter = new ApiResponse();
            try {
                getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
                getCoreUsagecounter.status = true;
            } catch (Exception ex) {
                getCoreUsagecounter.status = false;
            }
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, false);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithEmptyInAndOutTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithEmptyInAndOutTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setFrom("");
            getUsageCounterRequest.setTo("");

            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithNullInAndOutTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithNullInAndOutTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            getUsageCounterRequest.setFrom(null);
            getUsageCounterRequest.setTo(null);

            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithInAndOutTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithInAndOutTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            DateTimeFormatter df = _helper.getDateFormat();
            String outTime = new LocalDate().plusDays(1).toString(df);

            getUsageCounterRequest.setFrom(new LocalDate().toString(df));
            getUsageCounterRequest.setTo(outTime);

            ApiResponse getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, true);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getUsageCounterbyCriteriaWithInvalidInAndOutTime
     */
    @Test
    public void getUsageCounterbyCriteriaWithInvalidInAndOutTime() {
        try {
            // Add
            loggerService.updateUsageCounter(apiContext);

            // Get
            GetUsageCounterRequest getUsageCounterRequest = new GetUsageCounterRequest();
            DateTimeFormatter df = DateTimeFormat.forPattern("dd/MM/yy");
            String outTime = new LocalDate().plusDays(1).toString(df);

            getUsageCounterRequest.setFrom(new LocalDate().toString(df));
            getUsageCounterRequest.setTo(outTime);

            ApiResponse getCoreUsagecounter = new ApiResponse();
            try {
                getCoreUsagecounter = loggerService.getUsageCounter(getUsageCounterRequest, apiContext.getClient().getId());
                getCoreUsagecounter.status = true;
            } catch (Exception ex) {
                getCoreUsagecounter.status = false;
            }
            // Get Assert
            Assert.assertEquals("Get Usage Counter check", getCoreUsagecounter.status, false);

            // Get
            UsagecounterCriteria criteria = new UsagecounterCriteria();
            criteria.setClientId(apiContext.getClient().getId());
            List<CoreUsagecounter> coreUsagecounterList = dbService.getUsagecounterByCriteria(criteria);
            if(coreUsagecounterList != null && coreUsagecounterList.size() > 0){
                // Delete
                ApiResponse deleteApiResponse = dbService.deleteUsagecounter(coreUsagecounterList.get(0));
                // Delete Assert
                Assert.assertEquals("Delete Usage Counter check", deleteApiResponse.status, true);
            }
            else{
                Assert.fail();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }
}
