// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.datamanagementservice;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.UsageLogCriteria;
import com.bgt.lens.model.entity.CoreUsagelog;
import com.bgt.lens.model.entity.CoreUsagelogdata;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Usage log test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CoreUsagelogDataTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    @Autowired
    IDataRepositoryService dbService;

    /**
     * Usage log
     */
    private CoreUsagelog coreUsagelog;

    /**
     * Usage log data
     */
    private CoreUsagelogdata coreUsagelogdata;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);

        coreUsagelog = new CoreUsagelog();
        coreUsagelog.setRequestId("C23A4938-4D01-4A66-B170-24E426DC9C4D");
        coreUsagelog.setClientId(10138);
        coreUsagelog.setApiId(25491);
        coreUsagelog.setUri("http://localhost:8080/v1/rest/core/clients/85");
        coreUsagelog.setMethod("POST");
        coreUsagelog.setResource("ResumeParser");
        coreUsagelog.setInstanceType("XRAY");
        coreUsagelog.setLocale("en_us");
        coreUsagelog.setParseVariant("json");
        //coreUsagelog.setRaisedError(false);
        coreUsagelog.setInTime(Date.from(Instant.now()));
        coreUsagelog.setOutTime(Date.from(Instant.now()));
        coreUsagelog.setMilliseconds(23456L);

        coreUsagelogdata = new CoreUsagelogdata();
        coreUsagelogdata.setRequestContent(null);
        coreUsagelogdata.setResponseContent("{\"errorCode\":\"GEN001\",\"developerMessage\":\"Internal server error GET against resource Search : Jobs - Details: document not found: couldn't get key\",\"userMessage\":\"Internal Server Error\",\"moreInfo\":\"http://developer.burning-glass.com/error/GEN001\",\"requestId\":\"c23a4938-4d01-4a66-b170-24e426dc9c4d\",\"timestamp\":1413894517,\"status\":\"failure\",\"statusCode\":\"InternalServerError\"}");
        coreUsagelogdata.setHeaders("Connection: keep-alive  Accept: application/json  Accept-Encoding: gzip, deflate  Accept-Language: null  Authorization: OAuth oauth_version=\"1.0\", oauth_signature_method=\"HMAC-SHA1\", oauth_nonce=\"JGNhxCdPNdNuc8T\", oauth_timestamp=\"1413894514\", oauth_consumer_key=\"BGT\", oauth_token=\"Search\", oauth_signature=\"Lxj7KFtnZkhIhM7CWMaZFvAwoXM%3D\"  Host: localhost:1357  User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0  ");

        coreUsagelog.setCoreUsagelogdata(coreUsagelogdata);

    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * addUsagelogWithValidData_DB
     */
    @Test
    public void addUsagelogWithValidData_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            CoreUsagelog getCoreUsagelog = dbService.getUsagelogById(coreUsagelog.getRequestId());
            // Get Assert
            Assert.assertNotNull("DB: Get Usage Log Check", getCoreUsagelog);
            
            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagelogWithoutClientId_DB
     */
    @Test
    public void addUsagelogWithoutClientId_DB() {
        try {
            // Add
            coreUsagelog.setClientId(null);
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log without ClientId check", addApiResponse.status, true);

            // Get
            CoreUsagelog getCoreUsagelog = dbService.getUsagelogById(coreUsagelog.getRequestId());
            // Get Assert
            Assert.assertNotNull("DB: Get Usage Log without ClientId Check", getCoreUsagelog);
            Assert.assertNull("DB: Get Usage Log without ClientId Check", getCoreUsagelog.getClientId());

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log without ClientId check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagelogWithoutApiId_DB
     */
    @Test
    public void addUsagelogWithoutApiId_DB() {
        try {
            // Add
           coreUsagelog.setApiId(null);
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log without ApiId check", addApiResponse.status, true);

            // Get
            CoreUsagelog getCoreUsagelog = dbService.getUsagelogById(coreUsagelog.getRequestId());
            // Get Assert
            Assert.assertNotNull("DB: Get Usage Log without ApiId Check", getCoreUsagelog);
            Assert.assertNull("DB: Get Usage Log without ApiId Check", getCoreUsagelog.getApiId());

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log without ApiId check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagelogWithoutRequestID_DB
     */
    @Test
    public void addUsagelogWithoutRequestID_DB() {
        try {
            // Add
            coreUsagelog.setRequestId(null);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagelog(coreUsagelog);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log without RequestId check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagelogWithoutUri_DB
     */
    @Test
    public void addUsagelogWithoutUri_DB() {
        try {
            // Add
            coreUsagelog.setUri(null);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagelog(coreUsagelog);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log without Uri check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagelogWithoutMethod_DB
     */
    @Test
    public void addUsagelogWithoutMethod_DB() {
        try {
            // Add
            coreUsagelog.setMethod(null);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagelog(coreUsagelog);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log without Method check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagelogWithoutResource_DB
     */
    @Test
    public void addUsagelogWithoutResource_DB() {
        try {
            // Add
            coreUsagelog.setResource(null);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagelog(coreUsagelog);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log without Resource check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addUsagelogInvalidClientId_DB
     */
    @Test
    public void addUsagelogInvalidClientId_DB() {
        try {
            // Add
            coreUsagelog.setClientId(-10);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagelog(coreUsagelog);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log Invalid ClientId check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * addUsagelogInvalidApiId_DB
     */
    @Test
    public void addUsagelogInvalidApiId_DB() {
        try {
            // Add
            coreUsagelog.setApiId(-10);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                addApiResponse = dbService.createUsagelog(coreUsagelog);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log Invalid ApiId check", addApiResponse.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * GetUsageLogbyIdWithValidId_DB
     */
    // ToDo : Update,Delete
    // GetById
    @Test
    public void getUsageLogbyIdWithValidId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            CoreUsagelog getCoreUsagelog = dbService.getUsagelogById(coreUsagelog.getRequestId());
            // Get Assert
            Assert.assertNotNull("DB: Get Usage Log By Valid id Check", getCoreUsagelog);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyIdWithInvalidId_DB
     */
    @Test
    public void getUsageLogbyIdWithInvalidId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            CoreUsagelog getCoreUsagelog = dbService.getUsagelogById("0");
            // Get Assert
            Assert.assertNull("DB: Get Usage Log By Invalid Id Check", getCoreUsagelog);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyIdWithValidClientId_DB
     */
    @Test
    public void getUsageLogbyIdWithValidClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            CoreUsagelog getCoreUsagelog = dbService.getUsagelogById(coreUsagelog.getRequestId(), coreUsagelog.getClientId());
            // Get Assert
            Assert.assertNotNull("DB: Get Usage Log By Valid Client id Check", getCoreUsagelog);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyIdWithInvalidClientId_DB
     */
    @Test
    public void getUsageLogbyIdWithInvalidClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            CoreUsagelog getCoreUsagelog = dbService.getUsagelogById(coreUsagelog.getRequestId(), 123);
            // Get Assert
            Assert.assertNull("DB: Get Usage Log By Invalid Client id Check", getCoreUsagelog);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetListofAllUsageLog
     */
    // GetByCriteria
    @Test
    public void getListofAllUsageLog() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get Usage Log", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithInTime_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithInTime_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            //DateFormat df = _helper.getDateFormat();
            //Date dateFrom = df.parse(df.format(Date.from(Instant.now())));

            DateTimeFormatter df = _helper.getDateFormat();
            Date dateFrom = df.parseLocalDate(new LocalDate().toString()).toDateTimeAtStartOfDay().toDate();

            usageLogCriteria.setFrom(dateFrom);

            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get Usage Log by From Date", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagelogDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithInvalidInTime_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidInTime_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, 2); // Adding 1 day
            //Date dateFrom = df.parse(df.format(c.getTime()));

            DateTimeFormatter df = _helper.getDateFormat();
            Date dateFrom = df.parseLocalDate(new LocalDate().plusDays(2).toString()).toDateTimeAtStartOfDay().toDate();

            usageLogCriteria.setFrom(dateFrom);

            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get Usage Log by Invalid From Date", coreUsagelogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagelogDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithOutTime_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithOutTime_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();

            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //Date dateTo = df.parse(df.format(c.getTime()));
            DateTimeFormatter df = _helper.getDateFormat();
            Date dateTo = df.parseLocalDate(new LocalDate().plusDays(2).toString()).toDateTimeAtStartOfDay().toDate();

            usageLogCriteria.setTo(dateTo);

            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get Usage Log by To Date", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagelogDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithInvalidOutTime_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidOutTime_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, -1); // Adding 1 day
            //Date dateTo = df.parse(df.format(c.getTime()));

            DateTimeFormatter df = _helper.getDateFormat();
            Date dateTo = df.parseLocalDate(new LocalDate().plusDays(-1).toString()).toDateTimeAtStartOfDay().toDate();

            usageLogCriteria.setTo(dateTo);

            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get Usage Log by Invalid To Date", coreUsagelogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagelogDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithInAndOutTime_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithInAndOutTime_DB() {
        try {
            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //Date outTime = df.parse(df.format(c.getTime()));

            DateTimeFormatter df = _helper.getDateFormat();
            Date outTime = df.parseLocalDate(new LocalDate().plusDays(1).toString()).toDateTimeAtStartOfDay().toDate();

            // Add
            coreUsagelog.setOutTime(outTime);
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();

            //Date dateFrom = df.parse(df.format(Date.from(Instant.now())));
            Date dateFrom = df.parseLocalDate(new LocalDate().toString()).toDateTimeAtStartOfDay().toDate();

            usageLogCriteria.setFrom(dateFrom);

            //Date dateTo = df.parse(df.format(c.getTime()));
            usageLogCriteria.setTo(outTime);

            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get Usage Log by From & To Date", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagelogDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithInvalidInAndOutTime_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidInAndOutTime_DB() {
        try {

            //DateFormat df = _helper.getDateFormat();
            //Calendar c = Calendar.getInstance();
            //c.setTime(Date.from(Instant.now())); // Now use today date.
            //c.add(Calendar.DATE, 1); // Adding 1 day
            //Date outTime = df.parse(df.format(c.getTime()));
            DateTimeFormatter df = _helper.getDateFormat();
            Date outTime = df.parseLocalDate(new LocalDate().plusDays(1).toString()).toDateTimeAtStartOfDay().toDate();

            // Add
            coreUsagelog.setOutTime(outTime);
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();

            //Date dateFrom = df.parse(df.format(Date.from(Instant.now())));
            Date dateFrom = df.parseLocalDate(new LocalDate().toString()).toDateTimeAtStartOfDay().toDate();

            usageLogCriteria.setFrom(dateFrom);

            //Date dateTo = df.parse(df.format(Date.from(Instant.now())));
            Date dateTo = df.parseLocalDate(new LocalDate().toString()).toDateTimeAtStartOfDay().toDate();

            usageLogCriteria.setTo(dateTo);

            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get Usage Log by Invalid From & To Date", coreUsagelogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Logger.getLogger(CoreUsagelogDataTest.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithClientId_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setClientId(coreUsagelog.getClientId());
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get UsageLog by Criteria Client Id", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithInvalidClientId_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidClientId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setClientId(123);
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageLog by Criteria Invalid Client Id", coreUsagelogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithApiId_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithApiId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setApiId(coreUsagelog.getApiId());
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get UsageLog by Criteria Api Id", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithInvalidApiId_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidApiId_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setApiId(123);
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageLog by Criteria Invalid Api Id", coreUsagelogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithHttpMethod_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithHttpMethod_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setMethod(coreUsagelog.getMethod());
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get UsageLog by Criteria HTTP Method", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithInvalidHttpMethod_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidHttpMethod_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setMethod("Invalid");
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageLog by Criteria Invalid HTTP Method", coreUsagelogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithResource_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithResource_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setResource(coreUsagelog.getResource());
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get UsageLog by Criteria Resource", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithInvalidResource_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithInvalidResource_DB() {
        try {
            // Add
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setResource("Invalid");
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertNull("DB: Get UsageLog by Criteria Invalid Resource", coreUsagelogs);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithRaisedErrorTrue_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithRaisedErrorTrue_DB() {
        try {
            // Add
            coreUsagelog.setRaisedError(true);
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setRaisedError(coreUsagelog.isRaisedError());
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get UsageLog by Criteria RaisedError True", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetUsageLogbyCriteriaWithRaisedErrorFalse_DB
     */
    @Test
    public void getUsageLogbyCriteriaWithRaisedErrorFalse_DB() {
        try {
            // Add
            coreUsagelog.setRaisedError(false);
            ApiResponse addApiResponse = dbService.createUsagelog(coreUsagelog);
            // Add Assert
            Assert.assertEquals("DB: Add Usage Log check", addApiResponse.status, true);

            // Get
            UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
            usageLogCriteria.setRaisedError(coreUsagelog.isRaisedError());
            List<CoreUsagelog> coreUsagelogs = dbService.getUsagelogByCriteria(usageLogCriteria);
            // Get Assert
            Assert.assertTrue("DB: Get UsageLog by Criteria RaisedError True", coreUsagelogs.size() > 0);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteUsagelog(coreUsagelog);
            // Delete Assert
            Assert.assertEquals("DB: Delete Usage Log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

}
