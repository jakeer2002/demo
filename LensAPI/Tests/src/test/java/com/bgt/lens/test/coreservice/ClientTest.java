// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.coreservice;

import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.GetClientsRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * *
 * Client management test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ClientTest {

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    /**
     * *
     * API response
     */
    public ApiResponse apiResponse;

    /**
     * *
     * Lens settings response
     */
    public ApiResponse lensSettingsResponse;

    /**
     * *
     * Resource response
     */
    public ApiResponse resourceResponse;

    /**
     * *
     * API
     */
    public Api api;

    /**
     * *
     * Lens settings
     */
    public LensSettings settings;

    /**
     * *
     * Resources
     */
    public Resources resource;

    /**
     * *
     * Helper object
     */
    private static final Helper _helper = new Helper();

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {

    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {

    }

    /**
     * *
     * Setup
     */
    @Before
    public void setUp() {
        try {

            ServletContext servletContext = wac.getServletContext();

            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            //Create Api
            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            //Add Lens settings
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            settings = (LensSettings) lensSettingsResponse.responseData;

            //Add Resource
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            api = (Api) apiResponse.responseData;
            apiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", apiResponse.status, true);

            settings = (LensSettings) lensSettingsResponse.responseData;
            lensSettingsResponse = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", lensSettingsResponse.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithValidData
     *
     * @throws JAXBException
     */
    @Test
    public void createClientWithValidData() throws JAXBException {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithDuplicateKey
     */
    @Test
    public void createClientWithDuplicateKey() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Client client = (Client) response.responseData;
            Assert.assertEquals("Create consumer duplicate check", response.status, true);

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer duplicate check", response.status, false);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithInvalidApiId
     */
    //Invalid Test Case
    //@Test
    public void createClientWithInvalidApiId() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, 0, api.getKey(), resource.getResourceId().toString());
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer invalid Api Id", response.status, false);

        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithInvalidLensSettingsId
     */
    @Test
    public void createClientWithInvalidLensSettingsId() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, 0);
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer invalid LensSettings Id check", response.status, false);
        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithoutAPIDetails
     */
    @Test
    public void createClientWithoutAPIDetails() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer.setClientApiList(null);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer empty client API check", response.status, false);
        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithoutLensSettings
     */
    @Test
    public void createClientWithoutLensSettings() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            consumer.setLensSettings(null);
            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer empty Lens Settings check", response.status, false);
        } catch (IOException | JAXBException ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithoutLicenseDetails
     */
    @Test
    public void createClientWithoutLicenseDetails() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
                consumer.getClientApiList().getClientApi().get(i).setActivationDate(null);
                consumer.getClientApiList().getClientApi().get(i).setAllowedTransactions(null);
            }
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer empty license details check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithInvalidResources
     */
    @Test
    public void createClientWithInvalidResources() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), "0");

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer invalid resource check", response.status, false);
        } catch (IOException | JAXBException ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithoutResources
     */
    @Test
    public void createClientWithoutResources() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumerEmptyLicensedResource.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
                consumer.getClientApiList().getClientApi().get(i).setApiId(new BigInteger(Integer.toString(api.getId())));
                consumer.getClientApiList().getClientApi().get(i).setApiKey(api.getKey());
            }
            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer empty resource check", response.status, false);
        } catch (IOException | JAXBException ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithLicenseLimitedByDate
     */
    @Test
    public void createClientWithLicenseLimitedByDate() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            LocalDate today = new LocalDate();
            LocalDate today30 = today.plusDays(30);
            consumer = _testhelper.updateLicense(consumer, today.toString(), today30.toString(), "1");
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer License by date check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithLicenseLimitedByTransactionCount
     */
    @Test
    public void createClientWithLicenseLimitedByTransactionCount() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            consumer = _testhelper.updateLicense(consumer, null, null, "1");
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer License by transaction count check", response.status, true);

            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithInvalidLicenseDate
     */
    @Test
    public void createClientWithInvalidLicenseDate() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            LocalDate today = new LocalDate();
            LocalDate today30 = today.minusDays(30);

            consumer = _testhelper.updateLicense(consumer, today.toString(), today30.toString(), "1");

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer License by date check", response.status, false);
        } catch (IOException | JAXBException ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithEmptyActivationDate
     */
    @Test
    public void createClientWithEmptyActivationDate() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            LocalDate today = new LocalDate();
            LocalDate today30 = today.plusDays(30);

            consumer = _testhelper.updateLicense(consumer, today.toString(), today30.toString(), "1");

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer empty ActivationDate check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithNullActivationDate
     */
    @Test
    public void createClientWithNullActivationDate() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            LocalDate today = new LocalDate();
            LocalDate today30 = today.plusDays(30);

            consumer = _testhelper.updateLicense(consumer, null, today30.toString(), "1");

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer Null ActivationDate check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithEmptyExpiryDate
     */
    @Test
    public void createClientWithEmptyExpiryDate() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            LocalDate today = new LocalDate();
            consumer = _testhelper.updateLicense(consumer, today.toString(), "", "1");
            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer empty ExpiryDate check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithNULLExpiryDate
     */
    @Test
    public void createClientWithNULLExpiryDate() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            LocalDate today = new LocalDate();
            consumer = _testhelper.updateLicense(consumer, today.toString(), null, "1");
            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer NULL ExpiryDate check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * CreateClientWithInvalidTransactionCount
     */
    @Test
    public void createClientWithInvalidTransactionCount() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            consumer = _testhelper.updateLicense(consumer, null, null, "-1");
            ApiResponse response = new ApiResponse();

            try {
                response = coreService.createConsumer(consumer);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create consumer License by date check", response.status, false);
        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithValidData
     */
    @Test
    public void updateClientWithValidData() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            response = coreService.updateConsumer(client.getId(), request);

            Assert.assertEquals("Update consumer check", response.status, true);

            client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithInvalidConsumerId
     */
    @Test
    public void updateClientWithInvalidConsumerId() {
        try {
            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            //UpdateConsumerRequest request = (UpdateConsumerRequest) jaxbUnmarshaller.unmarshal(ClientTest.class.getResourceAsStream("/input/UpdateConsumer.xml"));
            request.setClientId(BigInteger.ZERO);

            ApiResponse response = coreService.updateConsumer(0, request);
            Assert.assertEquals("Update consumer Invalid consumer Id check", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithInvalidConsumerKey
     */
    @Test
    public void updateClientWithInvalidConsumerKey() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client1 = (Client) response.responseData;

            //Create consumer 2
            consumer.setKey("Sample3");
            consumer.setName("Sample3");
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client2 = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setKey("Sample3");
            request.setClientId(new BigInteger(Integer.toString(client1.getId())));

            try {
                response = coreService.updateConsumer(client1.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Update consumer with invalid key", response.status, false);

            //delete consumer
            response = coreService.deleteEntireConsumerDetails(client1.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);

            response = coreService.deleteEntireConsumerDetails(client2.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithInvalidConsumerName
     */
    @Test
    public void updateClientWithInvalidConsumerName() {
        try {
            //Create consumer 1
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client1 = (Client) response.responseData;

            //Create consumer 2
            consumer.setKey("Sample3");
            consumer.setName("Sample3");
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client2 = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setName("Sample3");
            request.setClientId(new BigInteger(Integer.toString(client1.getId())));

            try {
                response = coreService.updateConsumer(client1.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Update consumer with invalid name", response.status, false);

            //delete consumer
            response = coreService.deleteEntireConsumerDetails(client1.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);

            response = coreService.deleteEntireConsumerDetails(client2.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithInvalidApiId
     */
    // Invalid Test Case
    //@Test
    public void updateClientWithInvalidApiId() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));
            request.getClientApiList().getClientApi().get(0).setApiId(BigInteger.ZERO);

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Update consumer check", response.status, false);

            //client = (Client) response.ResponseData;
            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithInvalidLensSettingsId
     */
    @Test
    public void updateClientWithInvalidLensSettingsId() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, 0);
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update consumer check", response.status, false);

            //client = (Client) response.ResponseData;
            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithInvalidLicenseDate
     */
    @Test
    public void updateClientWithInvalidLicenseDate() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            LocalDate today = new LocalDate();
            LocalDate today30 = today.minusDays(30);
            request = _testhelper.updateLicense(request, today.toString(), today30.toString(), "1");

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update consumer check", response.status, false);

            //client = (Client) response.ResponseData;
            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithEmptyActivationDate
     */
    @Test
    public void updateClientWithEmptyActivationDate() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            LocalDate today = new LocalDate();
            LocalDate today30 = today.plusDays(30);
            request = _testhelper.updateLicense(request, "", today30.toString(), "1");

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update consumer check Empty ActivationDate", response.status, true);

            //client = (Client) response.ResponseData;
            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithNullActivationDate
     */
    @Test
    public void updateClientWithNullActivationDate() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            LocalDate today = new LocalDate();
            LocalDate today30 = today.plusDays(30);
            request = _testhelper.updateLicense(request, null, today30.toString(), "1");

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update consumer check Null ActivationDate", response.status, true);

            //client = (Client) response.ResponseData;
            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithEmptyExpiryDate
     */
    @Test
    public void updateClientWithEmptyExpiryDate() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            LocalDate today = new LocalDate();
            LocalDate today30 = today.minusDays(30);
            request = _testhelper.updateLicense(request, today.toString(), "", "1");

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update consumer check Empty ExpiryDate", response.status, true);

            //client = (Client) response.ResponseData;
            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithNullExpiryDate
     */
    @Test
    public void updateClientWithNullExpiryDate() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            LocalDate today = new LocalDate();
            LocalDate today30 = today.minusDays(30);
            request = _testhelper.updateLicense(request, today.toString(), null, "1");

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update consumer check null ExpiryDate", response.status, true);

            //client = (Client) response.ResponseData;
            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithValidNegativeTransactionCount
     */
    @Test
    public void updateClientWithValidNegativeTransactionCount() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            LocalDate today = new LocalDate();
            LocalDate today30 = today.minusDays(30);
            request = _testhelper.updateLicense(request, today.toString(), null, "-10");

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update consumer check", response.status, true);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientInvalidNegativeTransactionCount
     */
    @Test
    public void updateClientWithInvalidNegativeTransactionCount() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            LocalDate today = new LocalDate();
            LocalDate today30 = today.minusDays(30);
            request = _testhelper.updateLicense(request, today.toString(), null, "-101");

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update consumer check", response.status, false);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateandDisableClientApi
     */
    @Test
    public void updateandDisableClientApi() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            for (int i = 0; i < request.getClientApiList().getClientApi().size(); i++) {
                request.getClientApiList().getClientApi().get(i).setEnabled(false);
            }

            response = coreService.updateConsumer(client.getId(), request);

            Assert.assertEquals("Update consumer check", response.status, true);

            client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithoutAPIDetails
     */
    @Test
    public void updateClientWithoutAPIDetails() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request.setClientId(new BigInteger(Integer.toString(client.getId())));
            request.setClientApiList(null);

            response = coreService.updateConsumer(client.getId(), request);

            Assert.assertEquals("Update consumer check", response.status, true);

            client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithoutLensSettings
     */
    @Test
    public void updateClientWithoutLensSettings() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));
            request.setLensSettings(null);

            response = coreService.updateConsumer(client.getId(), request);

            Assert.assertEquals("Update consumer check", response.status, true);

            client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithoutLicenseDetails
     */
    @Test
    public void updateClientWithoutLicenseDetails() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());

            request.setClientId(new BigInteger(Integer.toString(client.getId())));
            // update transaction count 
            request = _testhelper.updateLicense(request, null, null, "1");

            response = coreService.updateConsumer(client.getId(), request);

            Assert.assertEquals("Update consumer check", response.status, true);

            client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * UpdateClientWithInvalidResourceName
     */
    @Test
    public void updateClientWithInvalidResourceName() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), "0");

            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            try {
                response = coreService.updateConsumer(client.getId(), request);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update consumer check", response.status, false);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * DeleteClientWithValidId
     */
    @Test
    public void deleteClientWithValidId() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * DeleteClientWithInvalidId
     */
    @Test
    public void deleteClientWithInvalidId() {
        try {
            ApiResponse response = coreService.deleteEntireConsumerDetails(0);
            Assert.assertEquals("Create consumer delete", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetListofConsumersByKey
     */
    @Test
    public void getListofConsumersByKey() {

        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            GetClientsRequest request = new GetClientsRequest();
            request.setClientKey(consumer.getKey());
            response = coreService.getClients(request);

            Assert.assertEquals("Get consumer details", response.status, true);
            List<Client> clientList = (List<Client>) response.responseData;

            Assert.assertEquals("Get consumer check", 1, clientList.size());

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetListofConsumersByName
     */
    @Test
    public void getListofConsumersByName() {

        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            GetClientsRequest request = new GetClientsRequest();
            request.setClientName(consumer.getName());
            response = coreService.getClients(request);

            Assert.assertEquals("Get consumer details", response.status, true);
            List<Client> clientList = (List<Client>) response.responseData;

            Assert.assertEquals("Get consumer check", 1, clientList.size());

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetListofConsumers
     */
    @Test
    public void getListofConsumers() {

        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            GetClientsRequest request = new GetClientsRequest();
            response = coreService.getClients(request);

            Assert.assertEquals("Get consumer details", response.status, true);
            List<Client> clientList = (List<Client>) response.responseData;

            Assert.assertEquals("Get consumer check", 1, clientList.size());

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetListofConsumersByDumpStatus
     */
    @Test
    public void getListofConsumersByDumpStatus() {

        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            consumer.setDumpStatus(true);
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            GetClientsRequest request = new GetClientsRequest();
            request.setDumpStatus(true);
            response = coreService.getClients(request);

            Assert.assertEquals("Get consumer details", response.status, true);
            List<Client> clientList = (List<Client>) response.responseData;

            Assert.assertEquals("Get consumer check", 1, clientList.size());

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetListofConsumersInvalidCriteria
     */
    @Test
    public void getListofConsumersInvalidCriteria() {

        try {
            GetClientsRequest request = new GetClientsRequest();
            request.setClientKey("xxxx");
            ApiResponse response = coreService.getClients(request);

            Assert.assertEquals("Get consumer details", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetconsumerbyIdWithValidConsumerId
     */
    @Test
    public void getconsumerbyIdWithValidConsumerId() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            consumer.setDumpStatus(true);
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.getClientById(client.getId());

            Assert.assertEquals("Get consumer details", response.status, true);
            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * GetconsumerbyIdWithInvalidConsumerId
     */
    @Test
    public void getconsumerbyIdWithInvalidConsumerId() {
        try {
            ApiResponse response = coreService.getClientById(0);
            Assert.assertEquals("Get consumer details", response.status, false);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void createClientWithLocaleDetection() throws JAXBException {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumerWithEnableLocaleDetection.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void createClientWithEmptyLocaleDetection() throws JAXBException {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumerWithEmptyEnableLocaleDetection.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer ", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void createClientWithDefaultLocale() throws JAXBException {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumerWithDefaultLocale.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer ", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void createClientWithDefaultLocaleEmptyLanguage() throws JAXBException {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumerWithDefaultLocale.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            consumer.getDefaultLocaleList().getLocale().get(0).setLanguage("");
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer ", response.status, true);
        } catch (Exception ex) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void createClientWithDefaultLocaleEmptyCountry() throws JAXBException {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumerWithDefaultLocale.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            consumer.getDefaultLocaleList().getLocale().get(0).setCountry("");
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer ", response.status, true);
        } catch (Exception ex) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void createClientWithDefaultLocaleWithoutLanguage() throws JAXBException {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumerWithDefaultLocale.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            consumer.getDefaultLocaleList().getLocale().get(0).setLanguage(null);
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer ", response.status, true);
        } catch (Exception ex) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void createClientWithDefaultLocaleWithoutCountry() throws JAXBException {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumerWithDefaultLocale.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            consumer.getDefaultLocaleList().getLocale().get(0).setCountry(null);
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer", response.status, true);
        } catch (Exception ex) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void updateClientWithEnableLocaleDetection() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumerWithEnableLocaleDetection.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());
            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            response = coreService.updateConsumer(client.getId(), request);

            Assert.assertEquals("Update consumer check", response.status, true);

            client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void updateClientWithEmptyEnableLocaleDetection() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumerWithEmptyEnableLocaleDetection.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());
            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            response = coreService.updateConsumer(client.getId(), request);

            Assert.assertEquals("Update consumer check", response.status, true);

            client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void updateClientWithDefaultLocale() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumerWithDefaultLocale.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());
            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            response = coreService.updateConsumer(client.getId(), request);

            Assert.assertEquals("Update consumer check", response.status, true);

            client = (Client) response.responseData;

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void updateClientWithDefaultLocaleEmptyLanguage() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumerWithDefaultLocale.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());
            request.getDefaultLocaleList().getLocale().get(0).setLanguage("");
            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            try {
                response = coreService.updateConsumer(client.getId(), request);
            } catch (Exception ex) {
                Assert.assertTrue(true);
            }

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void updateClientWithDefaultLocaleWithoutLanguage() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumerWithDefaultLocale.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());
            request.getDefaultLocaleList().getLocale().get(0).setLanguage(null);
            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            try {
                response = coreService.updateConsumer(client.getId(), request);
            } catch (Exception ex) {
                Assert.assertTrue(true);
            }

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void updateClientWithDefaultLocaleEmptyCountry() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumerWithDefaultLocale.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());
            request.getDefaultLocaleList().getLocale().get(0).setCountry("");
            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            try {
                response = coreService.updateConsumer(client.getId(), request);
            } catch (Exception ex) {
                Assert.assertTrue(true);
            }

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void updateClientWithDefaultLocaleWithoutCountry() {
        try {
            //Create a client
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);

            Client client = (Client) response.responseData;

            //Update the client
            UpdateConsumerRequest request = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumerWithDefaultLocale.xml");
            request = _testhelper.updateLensSettings(request, settings.getId());
            request = _testhelper.updateApiandResource(request, api.getId(), api.getKey(), resource.getResourceId().toString());
            request.getDefaultLocaleList().getLocale().get(0).setCountry(null);
            request.setClientId(new BigInteger(Integer.toString(client.getId())));

            try {
                response = coreService.updateConsumer(client.getId(), request);
            } catch (Exception ex) {
                Assert.assertTrue(true);
            }

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Update consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }
}
