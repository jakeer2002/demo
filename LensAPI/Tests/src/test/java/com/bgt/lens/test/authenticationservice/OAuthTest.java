// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.authenticationservice;

import com.bgt.lens.authentication.OAuth;
import com.bgt.lens.helpers.Helper;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.servlet.ServletContext;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * OAuth test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:springtest.xml"})
@WebAppConfiguration
public class OAuthTest {

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request = null;

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Setup
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {

        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        
        request = new MockHttpServletRequest();
    }

    // No need to test private methods when other classes, functions consume them 
    // see http://lassekoskela.com/thoughts/24/test-everything-but-not-private-methods/
    /**
     * GenerateSignatureBaseTest
     */
    @Test
    public void generateSignatureBaseTest() throws UnsupportedEncodingException {

        request.setScheme("http");
        request.setServerPort(8085);
        request.setServerName("localhost");
        request.setRequestURI("v1/rest/");

        String _mockConsumerKey = "TestConsumerKey";
        String _mockToken = "TestToken";
        String _mockTokenSecret = "TestTokenSecret";
        String _mockHttpMethod = "GET";
        long _mockTimeStamp = 1418832966;
        String _mockNonce = "randomString";
        String _mockSignatureType = "HMAC-SHA1";

        OAuth auth = new OAuth();
        String _expected_signatureBase = "GET&http%3A%2F%2Flocalhost%3A8085v1%2"
                + "Frest%2F&oauth_consumer_key%3DTestConsumerKey%26oauth_nonce"
                + "%3DrandomString%26oauth_signature_method%3DHMAC-SHA1"
                + "%26oauth_timestamp%3D1418832966%26oauth_token"
                + "%3DTestToken%26oauth_version%3D1.0";

        String _actual_signatureBase = auth.generateSignatureBase(request,
                _mockConsumerKey, _mockToken, _mockTokenSecret, _mockHttpMethod,
                _mockTimeStamp, _mockNonce, _mockSignatureType);
        try {
            Assert.assertEquals(_expected_signatureBase, _actual_signatureBase);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * GenerateSignatureUsingHashTest
     */
    @Test
    public void generateSignatureUsingHashTest() {

        String _expected_signatureHash = "yr2xpDz0PtI097kyy0hApbZloXo=";
        OAuth auth = new OAuth();
        try {
            Assert.assertEquals(_expected_signatureHash, auth.generateSignatureUsingHash("Signature", "Signature".getBytes("ASCII")));
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeyException ex) {
            Assert.fail();
        }
    }

    /**
     * GenerateSignatureTest
     */
    @Test
    public void generateSignatureTest() {

        request.setScheme("http");
        request.setServerPort(8085);
        request.setServerName("localhost");
        request.setRequestURI("v1/rest/");

        String _mockConsumerKey = "TestConsumerKey";
        String _mockConsumerSecret = "TestConsumerSecret";
        String _mockToken = "TestToken";
        String _mockTokenSecret = "TestTokenSecret";
        String _mockHttpMethod = "GET";
        long _mockTimeStamp = 1418832966;
        String _mockNonce = "randomString";
        OAuth oAuth = new OAuth();
        OAuth.SignatureMethods _mockSignatureMethod = oAuth.parseSignatureMethod("HMAC-SHA1");

        OAuth auth = new OAuth();
        String _expected_signature = "6dfRGFiEMZBPm0eFXVM1BhYSpwg=";

        try {
            Assert.assertEquals(_expected_signature, auth.generateSignature(request, _mockConsumerKey, _mockConsumerSecret, _mockToken, _mockTokenSecret, _mockHttpMethod, _mockTimeStamp, _mockNonce, _mockSignatureMethod));
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeyException ex) {
            Assert.fail();
        }

        Assert.assertEquals(true, true);

    }

    /**
     * GenerateTimeStampTest
     */
    @Test
    public void generateTimeStampTest() {

        String expected = "1417466445";//for "2014-12-1T20:40:45.372+05:30";
        OAuth auth = new OAuth();
        String _actual = auth.generateTimeStamp(new DateTime(2014, 12, 1, 20, 40, 45, 372)).toString();
        try {
            Assert.assertEquals(expected, _actual);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * ParseSignatureMethodTest
     */
    @Test
    public void parseSignatureMethodTest() {

        String _expected_signatureMethod = "HMACSHA1";

        try {
            OAuth oAuth = new OAuth();
            Assert.assertEquals(_expected_signatureMethod, oAuth.parseSignatureMethod("HMAC-SHA1").toString());
        } catch (Exception ex) {
            Assert.fail();
        }
    }

}
