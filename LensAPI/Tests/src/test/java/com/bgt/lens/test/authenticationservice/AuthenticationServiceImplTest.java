// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.authenticationservice;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.authentication.OAuth;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.ClientApi;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.authentication.IAuthenticationService;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;
import javax.xml.bind.JAXBContext;

import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.testng.Assert;

/**
 *
 * Authentication service test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:springtest.xml"})
@WebAppConfiguration
public class AuthenticationServiceImplTest {
    
    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;
    
    @Autowired
    protected WebApplicationContext wac;

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    @Autowired
    IAuthenticationService authenticationService;

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    /**
     * API response
     */
    public ApiResponse apiResponse = null;

    /**
     * Lens settings response
     */
    public ApiResponse lensSettingsResponse = null;

    /**
     * API
     */
    public ApiResponse resourceResponse = null;

    /**
     * API
     */
    public Api api = null;

    /**
     * Lens settings
     */
    public LensSettings settings = null;

    /**
     * Resource
     */
    public Resources resource = null;

    /**
     * Client
     */
    public Client client = null;

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Setup
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        
        ServletContext servletContext = wac.getServletContext();
       
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);    
        
        request = new MockHttpServletRequest();

        try {
            // create api
            JAXBContext jaxbContext = JAXBContext.newInstance(CreateApiRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            CreateApiRequest createApi = null;
            InputStream resourceAsStream = AuthenticationServiceImplTest.class.getResourceAsStream("/input/CreateApi.xml");
            try {
                if (resourceAsStream != null) {
                    createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //CreateApiRequest createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(ClientTest.class.getResourceAsStream("/input/CreateApi.xml"));
            apiResponse = coreService.createApi(createApi);

            api = (Api) apiResponse.responseData;

            // create lens settings
            jaxbContext = JAXBContext.newInstance(AddLensSettingsRequest.class);
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            InputStream resourceAsStream01 = AuthenticationServiceImplTest.class.getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ClientTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            lensSettingsResponse = coreService.addLensSettings(settingsRequest);

            settings = (LensSettings) lensSettingsResponse.responseData;

            // create resource
            jaxbContext = JAXBContext.newInstance(AddResourceRequest.class);
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            AddResourceRequest resourceRequest = null;
            InputStream resourceAsStream02 = AuthenticationServiceImplTest.class.getResourceAsStream("/input/CreateResource.xml");
            try {
                if (resourceAsStream02 != null) {
                    resourceRequest = (AddResourceRequest) jaxbUnmarshaller.unmarshal(resourceAsStream02);
                    resourceAsStream02.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream02.close();
                throw ex;
            }
            //AddResourceRequest resourceRequest = (AddResourceRequest) jaxbUnmarshaller.unmarshal(ClientTest.class.getResourceAsStream("/input/CreateResource.xml"));

            resourceResponse = coreService.addResource(resourceRequest);

            resource = (Resources) resourceResponse.responseData;

            // create consumer
            jaxbContext = JAXBContext.newInstance(CreateConsumerRequest.class);
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            CreateConsumerRequest consumer = new CreateConsumerRequest();
            InputStream resourceAsStream03 = AuthenticationServiceImplTest.class.getResourceAsStream("/input/CreateConsumer.xml");
            try {
                if (resourceAsStream03 != null) {
                    consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(resourceAsStream03);
                    resourceAsStream03.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream03.close();
                throw ex;
            }
            //CreateConsumerRequest consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(ClientTest.class.getResourceAsStream("/input/CreateConsumer.xml"));

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();
            
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            instanceList.setInstanceId(new BigInteger(Integer.toString(settings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);           

            consumer.setLensSettings(settingsList);

            for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
                consumer.getClientApiList().getClientApi().get(i).setApiId(new BigInteger(Integer.toString(api.getId())));
                consumer.getClientApiList().getClientApi().get(i).setApiKey(api.getKey());
                ClientApi.LicensedResources resourceList = consumer.getClientApiList().getClientApi().get(i).getLicensedResources();
                resourceList.getResourceList().clear();
                resourceList.getResourceList().add(new BigInteger(resource.getResourceId().toString()));
                consumer.getClientApiList().getClientApi().get(i).setLicensedResources(resourceList);
            }
            consumer.setVendorSettings(null);

            ApiResponse consumerResponse = coreService.createConsumer(consumer);

            client = (Client) consumerResponse.responseData;

        } catch (Exception ex) {
            System.out.println(ex.toString());
            ex.printStackTrace();
            org.junit.Assert.fail();
        }
    }

    /**
     * Tear down
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {

        try {
            api = (Api) apiResponse.responseData;

            apiResponse = coreService.deleteApi(api.getId());

            settings = (LensSettings) lensSettingsResponse.responseData;            

            apiResponse = coreService.deleteEntireConsumerDetails(client.getId());
            
            lensSettingsResponse = coreService.deleteLensSettings(settings.getId());

        } catch (Exception ex) {
            org.junit.Assert.fail();
        }
    }

    /**
     * ValidateApiContextWithOldRequest
     */
    @Test
    public void validateApiContextWithOldRequest() {
        try {
            String _authorization_header = "OAuth oauth_version=\"1.0\", "
                    + "oauth_signature_method=\"HMAC-SHA1\", oauth_nonce=\"5yBi89sDfjDMnXh\", "
                    + "oauth_timestamp=\"1418825451\", oauth_consumer_key=\"Sample\", "
                    + "oauth_token=\"Core\", oauth_signature=\"P2idu4GXAn1UeMF7ITWISStGKcc%3D\"";

            request.addHeader("authorization", _authorization_header);
            request.setContentType("application/json");
            RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
            ApiContext apiContext = new ApiContext();
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper.setApiContext(apiContext);
            ApiResponse apiResponseout = new ApiResponse();
            try {
                apiResponseout = authenticationService.validateApiContext(request);
                apiResponseout.status = true;
            } catch (Exception ex) {
                apiResponseout.status = false;
            }

            Assert.assertEquals(false, apiResponseout.status);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * ValidateApiContextWithNewRequest
     *
     * @throws NoSuchAlgorithmException
     */
    @Test
    public void validateApiContextWithNewRequest() throws Exception {

        request.setScheme("http");
        request.setMethod("GET");
        request.setServerPort(8085);
        request.setServerName("localhost");
        // test against Sample resource
        request.setRequestURI("/v2/adminservice/Sample/100");
        request.setServletPath("v2/");

        String _mockConsumerKey = "Sample";
        String _mockConsumerSecret = "Abcdefghijklmnopqrstuvwxyz123456";
        String _mockToken = "Core";
        String _mockTokenSecret = "Abcdefghijklmnopqrstuvwxyz123456";
        String _mockHttpMethod = "GET";
        long _mockTimeStamp = System.currentTimeMillis() / 1000L;
        String _mockNonce = "randomString";
        OAuth oAuth = new OAuth();
        OAuth.SignatureMethods _mockSignatureMethod = oAuth.parseSignatureMethod("HMAC-SHA1");

        OAuth auth = new OAuth();
        String _generatedSignature = "";

        try {
            _generatedSignature = URLEncoder.encode(auth.generateSignature(request, _mockConsumerKey,
                    _mockConsumerSecret, _mockToken, _mockTokenSecret,
                    _mockHttpMethod, _mockTimeStamp, _mockNonce, _mockSignatureMethod), "UTF-8");
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeyException ex) {
            throw ex;
        }

        String _authorization_header = "OAuth oauth_version=\"1.0\", "
                + "oauth_signature_method=\"HMAC-SHA1\", oauth_nonce=\"randomString\", "
                + "oauth_timestamp=" + _mockTimeStamp + ", oauth_consumer_key=\"Sample\", "
                + "oauth_token=\"Core\", oauth_signature=" + _generatedSignature;

        request.addHeader("authorization", _authorization_header);
        request.setContentType("application/json");
        request.addHeader("accept", "application/json");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        // set api context
        long startTime = System.currentTimeMillis();
        Date startDateTime = Date.from(Instant.now());

        request.setAttribute("startTime", startTime);

        ApiContext apiContext = new ApiContext();
        apiContext.setRequestId(UUID.randomUUID());
        apiContext.setApi("Core");
        apiContext.setInTime(startDateTime);
        apiContext.setRequestUri(request.getRequestURL().toString());
        apiContext.setMethod(request.getMethod());
        String accept = request.getHeader("accept");
        String contentType = request.getHeader("content-type");

        if (accept != null) {
            apiContext.setAccept(accept);
        } else {
            apiContext.setAccept("application/xml");
        }

        if (contentType != null) {
            apiContext.setContentType(contentType);
        } else {
            apiContext.setContentType("application/xml");
        }
        apiContext.setAdvanceLog(new AdvanceLog());
        _helper.setApiContext(apiContext);

        // Get urlContext - Action name
        String urlContext = request.getRequestURI().replace(request.getServletPath(), "");
        apiContext.setResource(_helper.getResourceName(urlContext, apiContext));
        request.setAttribute("ApiContext", apiContext);
        try {
            ApiResponse _response = authenticationService.validateApiContext(request);

            Assert.assertEquals(true, _response.status);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * getClientDetailsTest
     */
    @Test
    public void getClientDetailsTest() {
        try {
            assert authenticationService.getClientDetails("Sample").getClass().equals(CoreClient.class);
        } catch (Exception ex) {
            Assert.fail();
        }

    }

    /**
     * getClientApiDetailsTest
     */
    @Test
    public void getClientApiDetailsTest() {
        try {

            CoreClient coreClient = authenticationService.getClientDetails("Sample");

            assert authenticationService.getClientApiDetails("Core", coreClient).getClass().equals(CoreClientapi.class);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    // TODO : check with invalid data and expect exception
}
