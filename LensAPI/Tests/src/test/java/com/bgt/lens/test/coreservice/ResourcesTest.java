// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.coreservice;

import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.GetResourceRequest;
import com.bgt.lens.model.adminservice.request.UpdateResourceRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Resources test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ResourcesTest {

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    /**
     * API response
     */
    public ApiResponse apiResponse = null;

    /**
     * API
     */
    public Api api;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        try {

            ServletContext servletContext = wac.getServletContext();

            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);

            api = (Api) apiResponse.responseData;

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            api = (Api) apiResponse.responseData;

            apiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", apiResponse.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * AddResourceWithValidData
     */
    @Test
    public void addResourceWithValidData() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);
            Resources resource = (Resources) response.responseData;

            response = coreService.deleteResource(resource.getResourceId());
            Assert.assertEquals("Create resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * AddResourceWithoutResourceName
     */
    @Test
    public void addResourceWithoutResourceName() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            resourceRequest.setResourceName(null);

            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addResource(resourceRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create resource check", response.status, false);

        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * AddResourceWithoutApiKey
     */
    @Test
    public void addResourceWithoutApiKey() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            resourceRequest.setApiKey(null);
            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addResource(resourceRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create resource check", response.status, false);

        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * AddResourceWithInvalidApiKey
     */
    @Test
    public void addResourceWithInvalidApiKey() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            resourceRequest.setApiKey("XXXX");

            ApiResponse response = new ApiResponse();
            try {
                response = coreService.addResource(resourceRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create resource check", response.status, false);

        } catch (JAXBException | IOException ex) {
            Assert.fail();
        }
    }

    /**
     * CreateResourceWithDuplicateResourceName
     */
    @Test
    public void createResourceWithDuplicateResourceName() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);
            Resources resource = (Resources) response.responseData;

            try {
                response = coreService.addResource(resourceRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create resource check", response.status, false);

            response = coreService.deleteResource(resource.getResourceId());
            Assert.assertEquals("Create resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * UpdateResourceWithValidData
     */
    @Test
    public void updateResourceWithValidData() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);

            Resources resource = (Resources) response.responseData;

            UpdateResourceRequest updateResourceRequest = new UpdateResourceRequest();
            updateResourceRequest.setApiKey(resource.getApiKey());
            updateResourceRequest.setResourceName("Sample01");

            response = coreService.updateResource(resource.getResourceId(), updateResourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);

            response = coreService.deleteResource(resource.getResourceId());
            Assert.assertEquals("Create resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * UpdateResourceWithoutResourceName
     */
    @Test
    public void updateResourceWithoutResourceName() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);

            Resources resource = (Resources) response.responseData;

            UpdateResourceRequest updateResourceRequest = new UpdateResourceRequest();
            updateResourceRequest.setApiKey(resource.getApiKey());
            updateResourceRequest.setResourceName(null);

            try {
                response = coreService.updateResource(resource.getResourceId(), updateResourceRequest);
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create resource check", response.status, false);

            response = coreService.deleteResource(resource.getResourceId());
            Assert.assertEquals("Create resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * UpdateResourceWithoutApiKey
     */
    @Test
    public void updateResourceWithoutApiKey() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);

            Resources resource = (Resources) response.responseData;

            UpdateResourceRequest updateResourceRequest = new UpdateResourceRequest();
            updateResourceRequest.setApiKey(null);
            updateResourceRequest.setResourceName("Sample01");

            response = coreService.updateResource(resource.getResourceId(), updateResourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);

            response = coreService.deleteResource(resource.getResourceId());
            Assert.assertEquals("Create resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * UpdateResourceWithInvalidApiKey
     */
    @Test
    public void updateResourceWithInvalidApiKey() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);

            Resources resource = (Resources) response.responseData;

            UpdateResourceRequest updateResourceRequest = new UpdateResourceRequest();
            updateResourceRequest.setApiKey("XXXX");
            updateResourceRequest.setResourceName("Sample01");

            try {
                response = coreService.updateResource(resource.getResourceId(), updateResourceRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create resource check", response.status, false);

            response = coreService.deleteResource(resource.getResourceId());
            Assert.assertEquals("Create resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * UpdateResourceWithDuplicateResourceName
     */
    @Test
    public void updateResourceWithDuplicateResourceName() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);

            Resources resource01 = (Resources) response.responseData;

            resourceRequest.setResourceName("Sample01");
            response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);

            Resources resource02 = (Resources) response.responseData;

            UpdateResourceRequest updateResourceRequest = new UpdateResourceRequest();
            updateResourceRequest.setApiKey(resource01.getApiKey());
            updateResourceRequest.setResourceName("Sample01");

            try {
                response = coreService.updateResource(resource01.getResourceId(), updateResourceRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create resource check", response.status, false);

            response = coreService.deleteResource(resource01.getResourceId());
            Assert.assertEquals("Create resource delete", response.status, true);

            response = coreService.deleteResource(resource02.getResourceId());
            Assert.assertEquals("Create resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * DeleteResourceWithValidResourceId
     */
    @Test
    public void deleteResourceWithValidResourceId() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);
            Resources resource = (Resources) response.responseData;

            response = coreService.deleteResource(resource.getResourceId());
            Assert.assertEquals("Delete resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * DeleteResourceWithInvalidResourceId
     */
    @Test
    public void deleteResourceWithInvalidResourceId() {
        try {
            ApiResponse response = coreService.deleteResource(0);
            Assert.assertEquals("Delete resource delete", response.status, false);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetResource
     */
    @Test
    public void getResource() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);
            Resources resource = (Resources) response.responseData;

            GetResourceRequest request = new GetResourceRequest();
            request.setApiKey(null);
            request.setResourceName(null);

            response = coreService.getResource(request);

            List<Resources> resources = (List<Resources>) response.responseData;
            Assert.assertTrue("Get Resource check", resources.size() > 0);

            response = coreService.deleteResource(resource.getResourceId());
            Assert.assertEquals("Delete resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetResourceWithInvalidResourceName
     */
    @Test
    public void getResourceWithInvalidResourceName() {
        try {
            GetResourceRequest request = new GetResourceRequest();
            request.setApiKey(null);
            request.setResourceName("XXXX");

            ApiResponse response = coreService.getResource(request);
            Assert.assertEquals("Get Resource", response.status, false);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetResourceWithInvalidApiKey
     */
    @Test
    public void getResourceWithInvalidApiKey() {
        try {
            GetResourceRequest request = new GetResourceRequest();
            request.setApiKey("XXXX");
            request.setResourceName(null);

            ApiResponse response = coreService.getResource(request);
            Assert.assertEquals("Get Resource", response.status, false);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetResourceWithValidResourceId
     */
    @Test
    public void getResourceWithValidResourceId() {
        try {
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse response = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", response.status, true);
            Resources resource = (Resources) response.responseData;

            response = coreService.getResourceById(resource.getResourceId());
            Assert.assertEquals("Get resource", response.status, true);

            response = coreService.deleteResource(resource.getResourceId());
            Assert.assertEquals("Delete resource delete", response.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetResourceWithInvalidResourceId
     */
    @Test
    public void getResourceWithInvalidResourceId() {
        try {
            ApiResponse response = coreService.getResourceById(0);
            Assert.assertEquals("Get resource", response.status, false);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

}
