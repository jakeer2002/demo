// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Tag Job binary data test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryTagJobBinaryDataTest {
    
     @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * *
     * Helper object
     */
    private static final Helper _helper = new Helper();

    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Lens settings
     */
    private CoreLenssettings lensSettings;

    /**
     * Helper object
     */
    private TestHelper testHelper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private byte[] binaryData;

    /**
     * Doc type
     */
    private Character docType;
    
    private String customSkillKey;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        
        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);

        String propFileName = "/JobTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lensSettings = new CoreLenssettings();
        lensSettings.setHost(testHelper.hostname);
        lensSettings.setPort(testHelper.port);
        lensSettings.setCharacterSet(testHelper.encoding);
        lensSettings.setTimeout(testHelper.timeOut);
        lensSettings.setInstanceType(testHelper.instanceType);
        lensSettings.setLocale(testHelper.locale);
        lensSettings.setStatus(testHelper.status);
        
        customSkillKey="";

        String fileName = "/TestJobs/1001257.txt";

        InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
        try {
            if (resourceAsStream != null) {
                binaryData = testHelper.getBinaryData(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException ex) {
            resourceAsStream.close();
            throw ex;
        }

        //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
        extension = fileName.substring(fileName.indexOf('.') + 1);
        docType = 'P';
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * TagBinaryDataTestWithVaildData
     */
    @Test
    public void tagBinaryDataTestWithVaildData() {
        try {
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "JobDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithInvalidHostName
     */
    @Test
    public void tagBinaryDataTestWithInvalidHostName() {
        try {
            // TagBinaryData
            lensSettings.setHost("Invalid");
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Invalid Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithEmptyHostName
     */
    @Test
    public void tagBinaryDataTestWithEmptyHostName() {
        try {
            // TagBinaryData
            lensSettings.setHost("");
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Empty Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithNullHostName
     */
    @Test
    public void tagBinaryDataTestWithNullHostName() {
        try {
            // TagBinaryData
            lensSettings.setHost(null);
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Null Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithInvalidHostPort
     */
    @Test
    public void tagBinaryDataTestWithInvalidHostPort() {
        try {
            // TagBinaryData
            lensSettings.setPort(0000);
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Invalid HostPort Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithInValidEncoding
     */
    @Test
    public void tagBinaryDataTestWithInValidEncoding() {
        try {
            // TagBinaryData
            lensSettings.setCharacterSet("Invalid");
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With InValid Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithEmptyEncoding
     */
    @Test
    public void tagBinaryDataTestWithEmptyEncoding() {
        try {
            // TagBinaryData
            lensSettings.setCharacterSet("");
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithNullEncoding
     */
    @Test
    public void tagBinaryDataTestWithNullEncoding() {
        try {
            // TagBinaryData
            lensSettings.setCharacterSet(null);
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Null Encoding Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithInvalidTimeOut
     */
    @Test
    public void tagBinaryDataTestWithInvalidTimeOut() {
        try {
            // TagBinaryData
            lensSettings.setTimeout(-10);
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithConvertError
     */
    @Test
    public void tagBinaryDataTestWithConvertError() {
        try {
            String fileName = "/TestJobs/InvalidDocuments/ExternalJob_ConverterError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
              String expResult = "(tag:6) Converters returned empty output";
            Assert.assertEquals("Lens TagBinaryData With convert error Check", apiErrors.getLensErrorMessage(expResult), lensResponse.responseData);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithTaggingError
     */
    @Test
    public void tagBinaryDataTestWithTaggingError() {
        try {
            String fileName = "/TestJobs/InvalidDocuments/ExternalJob_TaggingError.docx";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            String expResult = "error";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithLargeResumeFile
     */
    @Test
    public void tagBinaryDataTestWithLargePostingFile() {
        try {
            String fileName = "/TestJobs/LargeJobs/ExternalJob.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResult = "JobDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithNullBytes
     */
    @Test
    public void tagBinaryDataTestWithNullBytes() {
        String fileName = "/TestJobs/LargeJob/ExternalJob.doc";
        binaryData = null;
        extension = fileName.substring(fileName.indexOf('.') + 1);
        ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
        Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
        Assert.assertEquals("Lens TagBinaryData With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);
    }

    /**
     * TagBinaryDataTestWithUnicodeResume
     */
    @Test
    public void tagBinaryDataTestWithUnicodePosting() {
        try {
            String fileName = "/TestJobs/UnicodeJob/BEL_FRA_10370.tag.xsl.xml";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, true);
            String expResultUnicode = "tÃºuÃ½Å¾â€žâ€�â€”Ã¡cdÃ©eÃ­nÃ³rÅ TÃšUÃ�Å½â€žâ€�â€”Ã�CDÃ‰EÃ�NÃ“RÃ´Å“Ã¹Ã»Ã¼Ã¿Â«Â»â€”Ã Ã¢Ã§Ã©Ã¨Ãª"
                    + "Ã«Ã¯Ã®Ã”Å’Ã™Ã›ÃœÅ¸Â«Â»â€”Ã€Ã‚Ã‡Ã‰ÃˆÃŠÃ‹Ã�ÃŽÃ¤Ã¶Ã¼ÃŸÃ„Ã–Ãœâ€”Ã Ã¨Ã©Ã¬Ã²Ã³Ã¹Â«Â»â€”Ã€ÃˆÃ‰ÃŒÃ’Ã“Ã™Â«Â»â€”â€žâ€�â€”acelnÃ³s"
                    + "zzâ€žâ€�â€”ACELNÃ“SZZÃµÃ³Ã´ÃºÃ¼â€”Ã£Ã¡Ã¢Ã Ã§Ã©ÃªÃ­Ã•Ã“Ã”ÃšÃœâ€”ÃƒÃ�Ã‚Ã€Ã‡Ã‰ÃŠÃ�aÃ¢Ã®stâ€žâ€�â€”Â«Â»AÃ‚ÃŽSTâ€žâ€�â€”"
                    + "Â«Â»Ã¡Ã©Ã­Ã±Ã³ÃºÃ¼Â¿Â¡â€”Ã�Ã‰Ã�Ã‘Ã“ÃšÃœÂ¿Â¡â€”Ã¤Ã¥Ã Ã©Ã¶â€“Â»Ã„Ã…Ã€Ã‰Ã–â€“Â»Ã§giIÃ¶sÃ¼â€”Ã‡GIIÃ–SÃœâ€”";
            String expResult = "JobDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithNullExtension
     */
    @Test
    public void tagBinaryDataTestWithNullExtension() {
        try {
            String fileName = "/TestJobs/LargeJob/ExternalJob.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = null;
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            String expResult = "JobDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(!checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataTestWithEmptyExtension
     */
    @Test
    public void tagBinaryDataTestWithEmptyExtension() {
        try {
            String fileName = "/TestJobs/LargeJob/ExternalJob.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "";
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            String expResult = "JobDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(!checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * tagBinaryDataTestWithInvalidExtension
     */
    @Test
    public void tagBinaryDataTestWithInvalidExtension() {
        try {
            String fileName = "/TestJobs/LargeJob/ExternalJob.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "Invalid";
            // TagBinaryData
            ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryData Assert
            Assert.assertEquals("Lens TagBinaryData Status Check", lensResponse.status, false);
            String expResult = "JobDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(!checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

}
