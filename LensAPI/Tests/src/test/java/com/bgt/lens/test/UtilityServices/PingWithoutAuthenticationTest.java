// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.parserservice.request.PingRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Instance;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Ping without authentication test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class PingWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();


    @Autowired
    ICoreService coreService;

    @Autowired
    IUtilityService utilityService;

    /**
     * ApiContext
     */
    public ApiContext apiContext;

    /**
     * Lens settings
     */
    public LensSettings en_usLensSettings = null, en_ukLensSettings = null;

    /**
     * Helper object
     */
    public TestHelper testHelper = null;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Instance type
     */
    public static String InstanceType = "SPECTRUM";

    /**
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * Ping success
     */
    public static final String pingSuccess = "true";

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
           ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
           servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        try {
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            InstanceType = testHelper.instanceType;

            // create lens settings
            JAXBContext jaxbContext = JAXBContext.newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            InputStream resourceAsStream01 = PingWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(PingWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("GET");
            apiContext.setResource("Status");
            apiContext.setClient(null);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingWithoutAuthenticationDefaultLens
     */
    @Test
    public void getPingWithoutAuthenticationDefaultLens() {
        try {
            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping Default Lens Instance check", response.status, true);

            // Ping Assert
            Instance instance = (Instance) response.responseData;
            boolean en_usCheckContains = instance.locale.contains(LocaleTypeEn_us);
            boolean en_usStatusCheckContains = instance.status.contains(pingSuccess);
            assertTrue(en_usCheckContains);
            assertTrue(en_usStatusCheckContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingWithoutAuthenticationSpecificInstance
     */
    @Test
    public void getPingWithoutAuthenticationSpecificInstance() {
        try {
            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            pingRequest.setLocale(LocaleTypeEn_uk);
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping Without Authentication Specific Instance check", response.status, true);

            // Ping Assert
            Instance instance = (Instance) response.responseData;
            boolean en_ukCheckContains = instance.locale.contains(LocaleTypeEn_uk);
            boolean en_ukStatusCheckContains = instance.status.contains(pingSuccess);
            assertTrue(en_ukCheckContains);
            assertTrue(en_ukStatusCheckContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingWithoutAuthenticationUnauthorizedInstance
     */
    @Test
    public void getPingWithoutAuthenticationUnauthorizedInstance() {
        try {
            // Ping
            String localeType = "en_anz";
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            pingRequest.setLocale(localeType);
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping Without Authentication Unauthorized Instance check", response.status, false);

            // Ping Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingWithoutAuthenticationInvalidHostNameList
     */
    @Test
    public void getPingWithoutAuthenticationInvalidHostNameList() {
        try {
            // create lens settings
            JAXBContext jaxbContext = JAXBContext.newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            InputStream resourceAsStream01 = PingWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            pingRequest.setLocale("en_sgp");
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping Without Authentication Invalid HostName check", response.status, true);

            // Ping en_us Assert
            Instance instance = (Instance) response.responseData;
            boolean en_usCheckContains = instance.locale.contains("en_sgp");
            boolean en_usStatusCheckContains = instance.status.contains("false");
            assertTrue(en_usCheckContains);
            assertTrue(en_usStatusCheckContains);

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * GetPingWithoutAuthenticationInActiveLensList
     */
    @Test
    public void getPingWithoutAuthenticationInActiveLensList() {
        try {
            // create lens settings
            JAXBContext jaxbContext = JAXBContext.newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            InputStream resourceAsStream01 = PingWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(false);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // Ping
            PingRequest pingRequest = new PingRequest();
            pingRequest.setInstanceType(InstanceType);
            pingRequest.setLocale("en_sgp");
            ApiResponse response = utilityService.ping(pingRequest);
            Assert.assertEquals("XRay Ping Without Authentication InActive Lens check", response.status, false);

            // Ping inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            Assert.assertEquals("XRay Ping InActive Lens check", _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS), instanceResponse);

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
