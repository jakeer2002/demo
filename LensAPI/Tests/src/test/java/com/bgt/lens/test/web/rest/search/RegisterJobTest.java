// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.FilterSettings;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.adminservice.request.SearchSettings.Posting;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchRegisterLogCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchRegisterlog;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.FetchJob;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.RegisterDocument;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.CustomFilters;
import com.bgt.lens.model.searchservice.request.FetchJobRequest;
import com.bgt.lens.model.searchservice.request.Filter;
import com.bgt.lens.model.searchservice.request.RegisterJobRequest;
import com.bgt.lens.model.searchservice.request.UnregisterJobRequest;
import com.bgt.lens.model.soap.response.searchservice.RegisterJobResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Register job with authentication test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class RegisterJobTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Instance type.
     */
    private final String instanceType = "SPECTRUM";
    /**
     * Locale type.
     */
    private final String localeTypeEnus = "en_us";
    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Invalid type.
     */
    private final String invalidType = "Invalid Instance Type";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor vendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Unregister Job
     */
    private boolean unregisterJob;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");

            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            //Create vendor
            CreateVendorRequest createVendor = new CreateVendorRequest();
//            createVendor.setDocServerName(testHelper.postingDocServerName);
            createVendor.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendor.setMaxDocumentCount((long) (500));
            createVendor.setVendor("posvendor01");
            createVendor.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse createVendorResponse = searchService.createVendor(createVendor);
            Assert.assertEquals("Add vendor check",
                    createVendorResponse.status, true);
            vendor = (Vendor) createVendorResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            
            consumer.setLensSettings(settingsList);
            CreateConsumerRequest.VendorSettings vendorSettings = consumer.getVendorSettings();
            vendorSettings.getVendorIdList().clear();
            vendorSettings.getVendorIdList().add(BigInteger.valueOf(vendor.getVendorId()));
            SearchSettings settings = new SearchSettings();
            settings.getPosting().clear();
            Posting posting = new Posting();
            posting.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            posting.setVendorId(BigInteger.valueOf(vendor.getVendorId()));
            settings.getPosting().add(posting);

            consumer.setSearchSettings(settings);

            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            helper = new Helper();
            coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            Set<CoreLenssettings> coreLenssettingses = new HashSet<>();

            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            coreLenssettingses.add(enusCoreLenssettings);
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            coreLenssettingses.add(enukCoreLenssettings);
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            coreLenssettingses.add(localeCoreLenssettings);
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("posting");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);
            String fileName = "/TestJobs/10001.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
             // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
           

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job With ValidBinaryJobData.
     */
    @Test
    public final void registerJobWithValidBinaryJobData() {
        try {
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            //registerJobRequest.setErrorOnDuplicateorOverwrite(com.bgt.lens.helpers.Enum.registerDuplicate.overwrite.toString());
            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            unregisterJob = true;

            RegisterJobResponse registerJobResponse = (RegisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterJobResponse());

            ObjectMapper mapper;
            byte[] json;

            if (registerJobResponse != null && registerJobResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(registerJobResponse.responseData);
                RegisterDocument registerDocument = mapper.readValue(json, RegisterDocument.class
                );

                Assert.assertEquals("Register document Id", "1", registerDocument.getId());
                Assert.assertEquals("Register document count", 1, (long) registerDocument.getRegisteredDocumentCount());

//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
                int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register job check log size", searchRegisterLogCount + 1, updatedSearchRegisterLogCount);

                ApiResponse unregisterResponse = unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobOverWriteDuplicateJobId
     */
    @Test
    public final void registerJobOverWriteDuplicateJobId() {
        try {
            // Canon
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job ", registerResponse.status);

            // Set overwrite flag
            registerJobRequest.setErrorOnDuplicateorOverwrite(com.bgt.lens.helpers.Enum.registerDuplicate.overwrite.toString());
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            // Register Assert
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            RegisterJobResponse registerJobResponse = (RegisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterJobResponse());

            ObjectMapper mapper;
            byte[] json;

            if (registerJobResponse != null && registerJobResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(registerJobResponse.responseData);
                RegisterDocument registerDocument = mapper.readValue(json, RegisterDocument.class
                );

                Assert.assertEquals("Register document Id", "1", registerDocument.getId());
                Assert.assertEquals("Register document count", 1, (long) registerDocument.getRegisteredDocumentCount());

//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
                int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register job check log size", searchRegisterLogCount + 1, updatedSearchRegisterLogCount);

                ApiResponse unregisterResponse = unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
                Assert.assertTrue("Unregister job request", unregisterResponse.status);

            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobErrorDuplicateJobId
     */
    @Test
    public final void registerJobErrorDuplicateJobId() {
        try {
            // Canon
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job ", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            RegisterJobResponse registerJobResponse = (RegisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterJobResponse());

            if (registerJobResponse != null && (!registerJobResponse.status)) {
                Assert.assertTrue("Register job duplicate response check", ((String) registerJobResponse.responseData).contains("duplicate id"));
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            ApiResponse unregisterResponse = unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobEmptyBinaryData
     */
    @Test
    public final void registerJobEmptyBinaryData() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData("");
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
//            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
//            Assert.assertTrue("Register job ", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }
//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register job empty binary data", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobNullBinaryData
     */
    @Test
    public final void registerJobNullBinaryData() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(null);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }
//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register Job Null Binary Data", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobInDefaultVendor
     */
    @Test
    public final void registerJobInDefaultVendor() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, null, TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            //registerJobRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            unregisterJob = true;

            RegisterJobResponse registerJobResponse = (RegisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterJobResponse());

            ObjectMapper mapper;
            byte[] json;

            if (registerJobResponse != null && registerJobResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(registerJobResponse.responseData);
                RegisterDocument registerDocument = mapper.readValue(json, RegisterDocument.class
                );

                Assert.assertEquals("Register document Id", "1", registerDocument.getId());
                Assert.assertEquals("Register document count", 1, (long) registerDocument.getRegisteredDocumentCount());

//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
                int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register job check log size", searchRegisterLogCount + 1, updatedSearchRegisterLogCount);

                ApiResponse unregisterResponse = unregisterJob("1", instanceType, null, TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobInDefaultVendorOverwriteDuplicate
     */
    @Test
    public final void registerJobInDefaultVendorOverwriteDuplicate() {
        try {
            // Canon
            unregisterJob("1", instanceType, null, TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job ", registerResponse.status);

            // Set overwrite flag
            registerJobRequest.setErrorOnDuplicateorOverwrite(com.bgt.lens.helpers.Enum.registerDuplicate.overwrite.toString());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            RegisterJobResponse registerJobResponse = (RegisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterJobResponse());

            ObjectMapper mapper;
            byte[] json;

            if (registerJobResponse != null && registerJobResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(registerJobResponse.responseData);
                RegisterDocument registerDocument = mapper.readValue(json, RegisterDocument.class
                );

                Assert.assertEquals("Register document Id", "1", registerDocument.getId());
                Assert.assertEquals("Register document count", 1, (long) registerDocument.getRegisteredDocumentCount());

//                List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
                int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register job check log size", searchRegisterLogCount + 1, updatedSearchRegisterLogCount);

                ApiResponse unregisterResponse = unregisterJob("1", instanceType, null, TestHelper.Locale.en_us.toString());
                Assert.assertTrue("Unregister job request", unregisterResponse.status);

            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobInDefaultVendorErrorOnDuplicate
     */
    @Test
    public final void registerJobInDefaultVendorErrorOnDuplicate() {
        try {
            // Canon
            unregisterJob("1", instanceType, null, TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job ", registerResponse.status);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            RegisterJobResponse registerJobResponse = (RegisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterJobResponse());

            if (registerJobResponse != null && (!registerJobResponse.status)) {
                Assert.assertTrue("Register job duplicate response check", ((String) registerJobResponse.responseData).contains("duplicate id"));
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            ApiResponse unregisterResponse = unregisterJob("1", instanceType, null, TestHelper.Locale.en_us.toString());
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobWithoutLocale
     */
    @Test
    public final void registerJobWithoutLocale() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(null);
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
                // List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
                int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
                Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

                Assert.assertFalse("Register Job Without Locale", response.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobWithoutInstanceType
     */
    @Test
    public final void registerJobWithoutInstanceType() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(null);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register Job Without Instance Type", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobWithInvalidInstanceType
     */
    @Test
    public final void registerJobWithInvalidInstanceType() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType("XRAY");
            registerJobRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register Job Invalid Instance Type", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobWithInvalidVendor
     */
    @Test
    public final void registerJobWithInvalidVendor() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor("sample");

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertTrue("Register Job Invalid Vendor", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobWithoutDefaultVendor
     */
    @Test
    public final void registerJobWithoutDefaultVendor() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, "", TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor("");

            coreClient.setSearchSettings("");
            apiContext.setClient(coreClient);
            helper.setApiContext(apiContext);

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertTrue("Register job Without Default Vendor", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobWithoutJobId
     */
    @Test
    public final void registerJobWithoutJobId() {
        try {
            // Canon
            ApiResponse response = new ApiResponse();
            response.status = true;
            unregisterJob("1", instanceType, "", TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("");
            registerJobRequest.setVendor(vendor.getVendor());

            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = false;
            }

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

            Assert.assertFalse("Register job without job id", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobMoreThanMaxAllowedCount
     */
    @Test
    public final void registerJobMoreThanMaxAllowedCount() {
        try {
            // Canon
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            ApiResponse updateVendorCountResponse = searchService.updateVendor(vendor.getVendorId(), 1, 0, false);
            Assert.assertTrue("Update vendor maximum document count", updateVendorCountResponse.status);
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());

            ApiResponse registerResponse = searchService.registerJob(registerJobRequest);
            Assert.assertTrue("Register job response", registerResponse.status);

            // Register Assert
            registerJobRequest.setJobId("2");
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobWithInvalidLENSSettings
     */
    @Test
    public final void registerJobWithInvalidLENSSettings() {
        try {
            // Canon
            UpdateLensSettingsRequest lensSettings = new UpdateLensSettingsRequest();
            lensSettings.setHost("EmptyHost");
            lensSettings.setInstanceType("Spectrum");
            lensSettings.setStatus(true);
            ApiResponse updateLensSettingsResponse = coreService.updateLensSettings(enusLensSettings.getId(), lensSettings);
            Assert.assertTrue("Update LENS settings", updateLensSettingsResponse.status);
            CoreClient getCoreClient = dbService.getClientById(coreClient.getId());
            coreClient = getCoreClient;
            apiContext.setClient(coreClient);
            helper.setApiContext(apiContext);
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * registerJobWithInvalidBinaryDataJob
     */
    @Test
    public final void registerJobWithInvalidBinaryDataJob() {
        try {
            // Canon
            String fileName = "/TestJobs/InvalidDocuments/ExternalJob_TaggingError.docx";
            InputStream resourceAsStream = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream));
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            String extension = fileName.substring(fileName.indexOf('.') + 1);
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.toString());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setExtension(extension);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            // Register Assert
//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount,updatedSearchRegisterLogCount );

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job with valid custom filters
     */
    @Test
    public final void registerJobWithValidCustomFilters() {
        try {
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            updateRequest.setHost(testhelper.hostname);
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            registerJobRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("bgldlanguage");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerJobRequest.setCustomFilters(filters);

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();
            Thread.sleep(1000);
            unregisterJob = true;

            RegisterJobResponse registerJobResponse = (RegisterJobResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new RegisterJobResponse());

            if (registerJobResponse != null && registerJobResponse.status) {
                FetchJobRequest fetchJobRequest = new FetchJobRequest();
                fetchJobRequest.setInstanceType(instanceType);
                fetchJobRequest.setLocale(TestHelper.Locale.en_us.name());
                fetchJobRequest.setJobId("1");
                response = searchService.fetchJob(fetchJobRequest);
                Assert.assertEquals("Fetch job response status check", true, response.status);

                FetchJob job = (FetchJob) response.responseData;
                Assert.assertTrue("Fetch job response message check", ((String) job.getJob()).contains("<bgldlanguage>1</bgldlanguage>"));

                ApiResponse unregisterResponse = unregisterJob("1", instanceType, null, TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job with valid custom filters
     */
    @Test
    public final void registerJobWithoutCustomFiltersReturnsFailure() {
        try {
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("language");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            registerJobRequest.setRegisterWithCustomFilters(true);
//            Filter filter01 = new Filter();
//            filter01.setName("Language");
//            filter01.setValue(BigInteger.valueOf(1));
//            CustomFilters filters = new CustomFilters();
//            filters.getFilter().add(filter01);
//            registerJobRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register job response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job with custom filters without custom filter configurated LENS
     * settings
     */
    @Test
    public final void registerJobWithCustomFiltersWithEmptyCustomFilterLensReturnsFailure() {
        try {
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            registerJobRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("Language");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerJobRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            ApiResponse response = new ApiResponse();
            response.status = true;

            MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(registerJobRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();  
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job with invalid custom filters
     */
    @Test
    public final void registerJobWithInvalidCustomFilterReturnsFailure() {
        try {
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("language");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            registerJobRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("test");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerJobRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register job response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job with invalid custom filter name
     */
    @Test
    public final void registerJobWithEmptyCustomFilterNameReturnsFailure() {
        try {
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("language");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            registerJobRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerJobRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register job response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job with null custom filter value
     */
    @Test
    public final void registerJobWithNullCustomFilterValueReturnsFailure() {
        try {
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("language");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            registerJobRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("language");
            filter01.setValue(null);
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerJobRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register job response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Register job with duplicate custom filter value
     */
    @Test
    public final void registerJobWithDuplicateCustomFilterReturnsFailure() {
        try {
            unregisterJob("1", instanceType, vendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = new UpdateLensSettingsRequest();
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("language");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.setFilterSettings(new FilterSettings());
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            RegisterJobRequest registerJobRequest = new RegisterJobRequest();
            registerJobRequest.setInstanceType(instanceType);
            registerJobRequest.setLocale(TestHelper.Locale.en_us.name());
            registerJobRequest.setBinaryData(binaryData);
            registerJobRequest.setJobId("1");
            registerJobRequest.setVendor(vendor.getVendor());
            registerJobRequest.setRegisterWithCustomFilters(true);
            Filter filter01 = new Filter();
            filter01.setName("language");
            filter01.setValue((long) (1));
            Filter filter02 = new Filter();
            filter01.setName("language");
            filter01.setValue((long) (1));
            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            filters.getFilter().add(filter02);
            registerJobRequest.setCustomFilters(filters);

//            List<SearchRegisterlog> searchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int searchRegisterLogCount = dbService.getRegisterLogCount();
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(post(UriConstants.RegisterJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(registerJobRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Register job response check", false, response.status);

//            List<SearchRegisterlog> updatedSearchRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());
            int updatedSearchRegisterLogCount = dbService.getRegisterLogCount();              
            Assert.assertEquals("Register job check log size", searchRegisterLogCount, updatedSearchRegisterLogCount);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterJob
     *
     * @param jobId
     * @param instanceType
     * @param vendorName
     * @param locale
     * @return
     * @throws Exception
     */
    public ApiResponse unregisterJob(String jobId, String instanceType, String vendorName, String locale) throws Exception {
        UnregisterJobRequest unregisterRequest = new UnregisterJobRequest();
        unregisterRequest.setJobId(jobId);
        unregisterRequest.setVendor(vendorName);
        unregisterRequest.setInstanceType(instanceType);
        unregisterRequest.setLocale(locale);

        ApiResponse unregisterResponse = searchService.unregisterJob(unregisterRequest);
        return unregisterResponse;
    }
}
