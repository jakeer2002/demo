// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Enum.InstanceType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * FindLocale Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryFindLocaleTest {
    
    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;


    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Lens settings
     */
    private CoreLenssettings lensSettings;
    
      
    private ApiErrors apiErrors = new ApiErrors();
        
    private Helper helper =  new Helper();

    /**
     * Helper object
     */
    private TestHelper testHelper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private byte[] binaryData;

    /**
     * Doc type
     */
    private Character docType;

    private String customSkillKey;
    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        
        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        
        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lensSettings = new CoreLenssettings();
        lensSettings.setHost(testHelper.ld_hostname);
        lensSettings.setPort(testHelper.ld_port);
        lensSettings.setCharacterSet(testHelper.encoding);
        lensSettings.setTimeout(testHelper.timeOut);
        lensSettings.setInstanceType(testHelper.instanceType);
        lensSettings.setLocale(testHelper.locale);
        lensSettings.setStatus(testHelper.status);
        
        customSkillKey = testHelper.customSkillsKey;

        String fileName = "/TestResumes/100008.doc";

        InputStream resourceAsStream = LensRepositoryFindLocaleTest.class.getResourceAsStream(fileName);
        try {
            if (resourceAsStream != null) {
                binaryData = testHelper.getBinaryData(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException ex) {
            resourceAsStream.close();
            throw ex;
        }

        //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
        extension = fileName.substring(fileName.indexOf('.') + 1);
        docType = 'R';
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * FindlocaleWithVaildData
     */
    @Test
    public void findLocaleTestWithVaildData() {
        try {
            // FindLocale
            lensSettings.setInstanceType(InstanceType.LD.toString());
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale Status Check", lensResponse.status, true);
            String expResult = "locale";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithInvalidHostName
     */
    @Test
    public void findLocaleWithInvalidHostName() {
        try {
            // FindLocale
            lensSettings.setHost("Invalid");
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens FindLocale With Invalid Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithEmptyHostName
     */
    @Test
    public void findLocaleWithEmptyHostName() {
        try {
            // FindLocale
            lensSettings.setHost("");
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens FindLocale With Invalid Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithNullHostName
     */
    @Test
    public void findLocaleWithNullHostName() {
        try {
            // FindLocale
            lensSettings.setHost(null);
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens FindLocale With Invalid Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithInvalidHostPort
     */
    @Test
    public void findLocaleWithInvalidHostPort() {
        try {
            // FindLocale
            lensSettings.setPort(0000);
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens FindLocale With Invalid HostPort Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithInValidEncoding
     */
    @Test
    public void findLocaleWithInValidEncoding() {
        try {
            // FindLocale
            lensSettings.setCharacterSet("invalid");
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens FindLocale With InValid Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithEmptyEncoding
     */
    @Test
    public void findLocaleWithEmptyEncoding() {
        try {
            // FindLocale
            lensSettings.setCharacterSet("");
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens FindLocale With Empty Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithNullEncoding
     */
    @Test
    public void findLocaleWithNullEncoding() {
        try {
            // FindLocale
            lensSettings.setCharacterSet(null);
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale With Null Encoding Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithInvalidTimeOut
     */
    @Test
    public void findLocaleWithInvalidTimeOut() {
        try {
            // FindLocale
            lensSettings.setTimeout(-10);
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale With invalid timeout Check", lensResponse.status, false);
            Assert.assertEquals("Lens FindLocale With invalid timeout Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithTaggingError
     */
    @Test
    public void findLocaleWithTaggingError() {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_ConverterError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // FindLocale
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale Status Check", lensResponse.status, false);            
            
            String errorMessage = "(locale:2) error tagging: tag text not generated";
            
            Assert.assertEquals("Lens Response with tagging error check", apiErrors.getLensErrorMessage(errorMessage), lensResponse.responseData);
         
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithTaggingError
     */
    @Test
    public void findLocaleWithInvalidFileFormat() {
        try {
            String fileName = "/TestResumes/InvalidDocuments/MBCS.7z";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // FindLocale
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale Status Check", lensResponse.status, false);
            String expResult = "error tagging: tag text not generated";
           
            Assert.assertEquals("Lens Response with tagging error check", apiErrors.getLensErrorMessage(expResult), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithLargeResumeFile
     */
    @Test
    public void findLocaleWithLargeResumeFile() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // FindLocale
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale Status Check", lensResponse.status, true);           
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithNullBytes
     */
    @Test
    public void findLocaleWithNullBytes() {
        String fileName = "/TestResumes/UnicodeResume/resume.doc";
        binaryData = null;
        extension = fileName.substring(fileName.indexOf('.') + 1);
        ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType, customSkillKey);
        Assert.assertEquals("Lens FindLocale with null data Check", lensResponse.status, false);
        Assert.assertEquals("Lens FindLocale with null data Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);
    }

    /**
     * FindLocaleWithNullBytes
     */
    @Test
    public void findLocaleWithEmptyBytes() {
        String fileName = "/TestResumes/UnicodeResume/resume.doc";
        binaryData = new byte[0];
        extension = fileName.substring(fileName.indexOf('.') + 1);
        ApiResponse lensResponse = lensRepository.tagBinaryData(lensSettings, binaryData, extension, docType, customSkillKey);
        String expResult = "(tag:2) error converting: text not generated";
        String lensResponseMessage = (String) lensResponse.responseData;        
        Assert.assertEquals("Lens Response with tagging error check", apiErrors.getLensErrorMessage(expResult), lensResponse.responseData);
    }

    /**
     * FindLocaleWithUnicodeResume
     */
    @Test
    public void findWithUnicodeResume() {
        try {
            String fileName = "/TestResumes/UnicodeResume/BEL_FRA_10370.tag.xsl.xml";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // FindLocale
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale Status Check", lensResponse.status, true);
            String expResult = "locale";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithNullExtension
     */
    @Test
    public void findlocaleWithNullExtension() {
        try {
            String fileName = "/TestResumes/UnicodeResume/resume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = null;
            // FindLocale
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale Status Check", lensResponse.status, true);
            String expResult = "locale";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithEmptyExtension
     */
    @Test
    public void findLocaleWithEmptyExtension() {
        try {
            String fileName = "/TestResumes/UnicodeResume/resume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "";
            // FindLocale
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale Status Check", lensResponse.status, true);
            String expResult = "locale";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * FindLocaleWithInvalidExtension
     */
    @Test
    public void findLocaleWithInvalidExtension() {
        try {
            String fileName = "/TestResumes/UnicodeResume/resume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
//                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "Invalid";
            // FindLocale
            ApiResponse lensResponse = lensRepository.findLocale(lensSettings, binaryData, extension, docType);
            // FindLocale Assert
            Assert.assertEquals("Lens FindLocale Status Check", lensResponse.status, true);
            String expResult = "locale";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }
}
