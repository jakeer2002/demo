// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.Helper;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.helpers.Enum.registerDuplicate;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.ClientApi;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.InstanceList;
import com.bgt.lens.model.adminservice.request.UpdateApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.UpdateResourceRequest;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.searchservice.request.CustomFilters;
import com.bgt.lens.model.searchservice.request.Filter;
import com.bgt.lens.model.searchservice.request.RegisterJobRequest;
import com.bgt.lens.model.searchservice.request.RegisterResumeRequest;
import com.bgt.lens.model.searchservice.request.UnregisterJobRequest;
import com.bgt.lens.model.searchservice.request.UnregisterResumeRequest;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.List;
import java.util.Properties;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * Test helper configuration
 */
public class TestHelper {

    /**
     * Host name
     */
    public String hostname = null;

    /**
     * Port
     */
    public int port;

    /**
     * Encoding
     */
    public String encoding = null;

    /**
     * Connection timeout
     */
    public int timeOut;

    /**
     * Lens locale
     */
    public String locale = null;

    /**
     * Language
     */
    public String language;

    /**
     * Country
     */
    public String country;

    /**
     * HRXML Content
     */
    public String hrxmlcontent;
    /**
     * HRXML Content
     */
    public String hrxmlversion;
        /**
     * HRXML Content
     */
    public String hrxmlfilename;

    /**
     * Lens version
     */
    public String version = null;

    /**
     * Instance type
     */
    public String instanceType = null;

    /**
     * Instance status
     */
    public boolean status;

    /**
     * LD host name
     */
    public String ld_hostname = null;

    /**
     * LD port
     */
    public int ld_port = 0;

    /**
     * LD Encoding
     */
    public String ld_encoding = null;

    /**
     * Doc server name
     */
    public String docServerName = "";

    /**
     * Doc server name
     */
    public String postingDocServerName = "";

    /**
     * HypercubeServer name
     */
    public String hypercubeServerName = "";

    /**
     * Hypercube server name
     */
    public String postingHypercubeServerName = "";
    
    /**
     * Hypercube dimension name
     */
    public String hypercubeDimensionName01 = "";
    
    /**
     * Hypercube dimension name
     */
    public String hypercubeDimensionName02 = "";
    
    /**
     * Hypercube dimension name
     */
    public String hypercubeDimensionName03 = "";
    
    /**
     * Custom Skills Key
     */
    public String customSkillsKey = "";
    
    public String unicodeInstanceHost = "";
    
    public int unicodeInstancePort = 0;
    
    public String unicodeInstanceType = "";
    
    public String unicodeInstanceLocale = "";
    
    // NewlyAdded
    public String OpticCustomRollupInstanceHost = "";
    
    public int OpticCustomRollupInstancePort = 0;
    
    public String OpticCustomRollupInstanceType = "";
    
    public String OpticCustomRollupInstanceLocale = "";
    
    //SIB //07-03-2021 ADDED variables for JM and TM details
    public String JM_Hostname = "";
    public int JM_Port = 0;
    
    public String JM_GB_Hostname = "";
    public int JM_GB_Port = 0;
    public String JM_GB_Locale = "";
    
    public String JM_Encoding = "";
    
    public int JM_TimeOut = 0;
    
    public String JM_Locale = "";
    
    public String JM_Language = "";
    
    public String JM_Country = "";
    
    public String JM_Version = "";
    
    public String JM_InstanceType = "";
    
    public boolean JM_Status;
    
    public String TM_Hostname = "";
    
    public int TM_Port = 0;
    
    public String TM_Encoding = "";
    
    public int TM_TimeOut = 0;
    
    public String TM_Locale = "";
    
    public String TM_Language = "";
    
    public String TM_Country = "";
    
    public String TM_Version = "";
    
    public String TM_InstanceType = "";
    
    public boolean TM_Status;
    
    public String TM_GB_Hostname = "";
    public int TM_GB_Port = 0;
    public String TM_GB_Locale = "";
    
    public String JM_SG_Hostname = "";
    public int JM_SG_Port = 0;
    public String JM_SG_Locale = "";
    
    //KIS //27-05-2021 ADDED variables for Spectrum Sort details
    public String SpectrumSort_Hostname = "";
    
    public int SpectrumSort_Port = 0;
    
    public String SpectrumSort_Encoding = "";
    
    public int SpectrumSort_TimeOut = 0;
    
    public String SpectrumSort_Locale = "";
    
    public String SpectrumSort_Language = "";
    
    public String SpectrumSort_Country = "";
    
    public String SpectrumSort_Version = "";
    
    public String SpectrumSort_InstanceType = "";
    
    public boolean SpectrumSort_Status;
    
    //KVSR //29-11-2021 ADDED variables for Spectrum getdocids details
    public String SpectrumGetdocids_Hostname = "";
    
    public int SpectrumGetdocids_Port = 0;
    
    public String SpectrumGetdocids_Encoding = "";
    
    public int SpectrumGetdocids_TimeOut = 0;
    
    public String SpectrumGetdocids_Locale = "";
    
    public String SpectrumGetdocids_Language = "";
    
    public String SpectrumGetdocids_Country = "";
    
    public String SpectrumGetdocids_Version = "";
    
    public String SpectrumGetdocids_InstanceType = "";
    
    public boolean SpectrumGetdocids_Status;    
    //SIB //16-03-2021 ADDED variables for XRay details
    public String XRay_Hostname = "";
    public int XRay_Port = 0;
    
    public String XRay_Encoding = "";
    
    public int XRay_TimeOut = 0;
    
    public String XRay_Locale = "";
    
    public String XRay_Language = "";
    
    public String XRay_Country = "";
    
    public String XRay_Version = "";
    
    public String XRay_InstanceType = "";
    
    public boolean XRay_Status;
    /**
     * Locale
     */
    public enum Locale {

        en_us,
        en_gb,
        en_sg,
        en_de,
        LD
    }

    public TestHelper() {

    }

    /**
     * Initialize test helper
     *
     * @param PropertiesFileName
     * @throws IOException
     */
    public TestHelper(String PropertiesFileName) throws IOException {
        Properties prop = new Properties();

        InputStream resourceAsStream = TestHelper.class.getResourceAsStream(PropertiesFileName);
        try {
            if (resourceAsStream != null) {
                prop.load(resourceAsStream);
                hostname = prop.getProperty("Hostname");
                port = Integer.parseInt(prop.getProperty("Port"));
                encoding = prop.getProperty("Encoding");
                timeOut = Integer.parseInt(prop.getProperty("TimeOut"));
                locale = prop.getProperty("Locale");
                language = prop.getProperty("Language");
                country = prop.getProperty("Country");
                hrxmlcontent = IOUtils.toString(TestHelper.class.getResourceAsStream(prop.getProperty("HRXMLContent")));
                hrxmlversion = prop.getProperty("HRXMLVersion");
                hrxmlfilename = prop.getProperty("HRXMLFileName");
                version = prop.getProperty("Version");
                instanceType = prop.getProperty("InstanceType");
                status = stringToBool(prop.getProperty("Status"));
                hrxmlversion = prop.getProperty("HRXMLVersion");
                hrxmlfilename = prop.getProperty("HRXMLFileName");
                ld_hostname = prop.getProperty("LD_Hostname");
                ld_port = Integer.parseInt(prop.getProperty("LD_Port"));
                ld_encoding = prop.getProperty("LD_Encoding");
                docServerName = prop.getProperty("docserver");
                hypercubeServerName = prop.getProperty("hypercubeserver");
                postingDocServerName = prop.getProperty("docserverposting");
                postingHypercubeServerName = prop.getProperty("hypercubeserverposting");
                hypercubeDimensionName01 = prop.getProperty("dimension01");
                hypercubeDimensionName02 = prop.getProperty("dimension02");
                hypercubeDimensionName03 = prop.getProperty("dimension03");
                customSkillsKey = prop.getProperty("customskillskey");
                unicodeInstanceHost = prop.getProperty("UnicodeInstanceHost");
                unicodeInstancePort = Integer.parseInt(prop.getProperty("UnicodeInstancePort"));
                unicodeInstanceType = prop.getProperty("UnicodeInstanceType"); 
                unicodeInstanceLocale = prop.getProperty("UnicodeInstanceLocale"); 
                // NewlyAdded
                OpticCustomRollupInstanceHost = prop.getProperty("OpticCustomRollupInstanceHost");
                OpticCustomRollupInstancePort = Integer.parseInt(prop.getProperty("OpticCustomRollupInstancePort"));
                OpticCustomRollupInstanceType = prop.getProperty("OpticCustomRollupInstanceType"); 
                OpticCustomRollupInstanceLocale = prop.getProperty("OpticCustomRollupInstanceLocale");
                //KIS - Spectrum Sort Details 
                SpectrumSort_Hostname = prop.getProperty("SpectrumSort_Hostname");
                SpectrumSort_Port = Integer.parseInt(prop.getProperty("SpectrumSort_Port"));
                SpectrumSort_Encoding = prop.getProperty("SpectrumSort_Encoding");
                SpectrumSort_TimeOut = Integer.parseInt(prop.getProperty("SpectrumSort_TimeOut"));
                SpectrumSort_Locale = prop.getProperty("SpectrumSort_Locale");
                SpectrumSort_Language = prop.getProperty("SpectrumSort_Language");
                SpectrumSort_Country = prop.getProperty("SpectrumSort_Country");
                SpectrumSort_Version = prop.getProperty("SpectrumSort_Version");
                SpectrumSort_InstanceType = prop.getProperty("SpectrumSort_InstanceType");
                SpectrumSort_Status = stringToBool(prop.getProperty("SpectrumSort_Status"));
                //KVSR - Spectrum getdocids Details 
                SpectrumGetdocids_Hostname = prop.getProperty("SpectrumGetdocids_Hostname");
                SpectrumGetdocids_Port = Integer.parseInt(prop.getProperty("SpectrumGetdocids_Port"));
                SpectrumGetdocids_Encoding = prop.getProperty("SpectrumGetdocids_Encoding");
                SpectrumGetdocids_TimeOut = Integer.parseInt(prop.getProperty("SpectrumGetdocids_TimeOut"));
                SpectrumGetdocids_Locale = prop.getProperty("SpectrumGetdocids_Locale");
                SpectrumGetdocids_Language = prop.getProperty("SpectrumGetdocids_Language");
                SpectrumGetdocids_Country = prop.getProperty("SpectrumGetdocids_Country");
                SpectrumGetdocids_Version = prop.getProperty("SpectrumGetdocids_Version");
                SpectrumGetdocids_InstanceType = prop.getProperty("SpectrumGetdocids_InstanceType");
                SpectrumGetdocids_Status = stringToBool(prop.getProperty("SpectrumGetdocids_Status"));                               
                //SIB - Added the variables to support TM and JM parsing
                if (PropertiesFileName.contains("Job")){
                    JM_Hostname = prop.getProperty("JM_Hostname");
                    JM_Port = Integer.parseInt(prop.getProperty("JM_Port"));
                    JM_GB_Hostname = prop.getProperty("JM_GB_Hostname");
                    JM_SG_Hostname = prop.getProperty("JM_SG_Hostname");
                    JM_GB_Port = Integer.parseInt(prop.getProperty("JM_GB_Port"));
                    JM_SG_Port = Integer.parseInt(prop.getProperty("JM_SG_Port"));
                    JM_GB_Locale = prop.getProperty("JM_GB_Locale");
                    JM_SG_Locale = prop.getProperty("JM_SG_Locale");
                    JM_Encoding = prop.getProperty("JM_Encoding"); 
                    JM_TimeOut = Integer.parseInt(prop.getProperty("JM_TimeOut"));
                    JM_Locale = prop.getProperty("JM_Locale"); 
                    JM_Language = prop.getProperty("JM_Language"); 
                    JM_Country = prop.getProperty("JM_Country");
                    JM_Version = prop.getProperty("JM_Version"); 
                    JM_InstanceType = prop.getProperty("JM_InstanceType"); 
                    JM_Status =  stringToBool(prop.getProperty("JM_Status"));
                }
                else{
                    TM_Hostname = prop.getProperty("TM_Hostname");
                    TM_Port = Integer.parseInt(prop.getProperty("TM_Port"));
                    
                    TM_GB_Hostname = prop.getProperty("TM_GB_Hostname");
                    TM_GB_Port = Integer.parseInt(prop.getProperty("TM_GB_Port"));
                    TM_GB_Locale = prop.getProperty("TM_GB_Locale"); 
                    
                    TM_Encoding = prop.getProperty("TM_Encoding"); 
                    TM_TimeOut = Integer.parseInt(prop.getProperty("TM_TimeOut"));
                    TM_Locale = prop.getProperty("TM_Locale"); 
                    TM_Language = prop.getProperty("TM_Language"); 
                    TM_Country = prop.getProperty("TM_Country"); 
                    TM_Version = prop.getProperty("TM_Version"); 
                    TM_InstanceType = prop.getProperty("TM_InstanceType"); 
                    TM_Status =  stringToBool(prop.getProperty("TM_Status"));
                    //Xray
                    XRay_Hostname = prop.getProperty("XRay_Hostname");
                    XRay_Port = Integer.parseInt(prop.getProperty("XRay_Port"));
                    XRay_Encoding = prop.getProperty("XRay_Encoding"); 
                    XRay_TimeOut = Integer.parseInt(prop.getProperty("XRay_TimeOut"));
                    XRay_Locale = prop.getProperty("XRay_Locale"); 
                    XRay_Language = prop.getProperty("XRay_Language"); 
                    XRay_Country = prop.getProperty("XRay_Country"); 
                    XRay_Version = prop.getProperty("XRay_Version"); 
                    XRay_InstanceType = prop.getProperty("XRay_InstanceType"); 
                    XRay_Status =  stringToBool(prop.getProperty("XRay_Status"));
                }
                
                resourceAsStream.close();
            }
        } catch (IOException | NumberFormatException ex) {
            resourceAsStream.close();
            throw ex;
        }

    }

    /**
     * String to boolean conversion
     *
     * @param string
     * @return
     */
    public static boolean stringToBool(String string) {
        if (string.equalsIgnoreCase("true")) {
            return true;
        }
        if (string.equalsIgnoreCase("false")) {
            return false;
        }
        throw new IllegalArgumentException(string + " is not a bool. Only true and false are.");
    }

    /**
     * Get binary data from the given file path
     *
     * @param filePath
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public byte[] getBinaryData(String filePath) throws
            FileNotFoundException, IOException {
        File file = new File(filePath);
        byte[] fileContent;
        int length = 0;
        try (FileInputStream fin = new FileInputStream(file)) {
            fileContent = new byte[(int) file.length()];
            length = fin.read(fileContent);

            if (length == 0) {
                fileContent = null;
            }
        }
        return fileContent;
    }

    /**
     * Get binary data from input stream
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public byte[] getBinaryData(InputStream inputStream) throws IOException {
        if (inputStream != null) {
            return IOUtils.toByteArray(inputStream);
        } else {
            throw new IllegalArgumentException("Inputstream is null");
        }
    }
    
    public String getFileContent(InputStream inputStream) throws IOException {
        if (inputStream != null) {
            return IOUtils.toString(inputStream, Charsets.UTF_8);
        } else {
            throw new IllegalArgumentException("Inputstream is null");
        }
    }


    public CreateApiRequest getCreateApiRequest(String filePath) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(CreateApiRequest.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        CreateApiRequest createApi = null;
        InputStream resourceAsStream = TestHelper.class.getResourceAsStream(filePath);
        try {
            if (resourceAsStream != null) {
                createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException | JAXBException ex) {
            resourceAsStream.close();
            throw ex;
        }
        return createApi;
    }

    public UpdateApiRequest getUpdateApiRequest(String filePath) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(UpdateApiRequest.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        UpdateApiRequest updateApi = null;
        InputStream resourceAsStream = TestHelper.class.getResourceAsStream(filePath);
        try {
            if (resourceAsStream != null) {
                updateApi = (UpdateApiRequest) jaxbUnmarshaller.unmarshal(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException | JAXBException ex) {
            resourceAsStream.close();
            throw ex;
        }
        return updateApi;
    }

    public AddLensSettingsRequest getAddLensSettingsRequest(String filePath) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(AddLensSettingsRequest.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        AddLensSettingsRequest addLensSettings = null;
        InputStream resourceAsStream = TestHelper.class.getResourceAsStream(filePath);
        try {
            if (resourceAsStream != null) {
                addLensSettings = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException | JAXBException ex) {
            resourceAsStream.close();
            throw ex;
        }
        return addLensSettings;
    }

    public UpdateLensSettingsRequest getUpdateLensSettingsRequest(String filePath) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(UpdateLensSettingsRequest.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        UpdateLensSettingsRequest addLensSettings = null;
        InputStream resourceAsStream = TestHelper.class.getResourceAsStream(filePath);
        try {
            if (resourceAsStream != null) {
                addLensSettings = (UpdateLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException | JAXBException ex) {
            resourceAsStream.close();
            throw ex;
        }
        return addLensSettings;
    }

    public AddResourceRequest getAddResourceRequest(String filePath) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(AddResourceRequest.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        AddResourceRequest addResource = null;
        InputStream resourceAsStream = TestHelper.class.getResourceAsStream(filePath);
        try {
            if (resourceAsStream != null) {
                addResource = (AddResourceRequest) jaxbUnmarshaller.unmarshal(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException | JAXBException ex) {
            resourceAsStream.close();
            throw ex;
        }
        return addResource;
    }

    public UpdateResourceRequest getUpdateResourceRequest(String filePath) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(UpdateResourceRequest.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        UpdateResourceRequest updateResource = null;
        InputStream resourceAsStream = TestHelper.class.getResourceAsStream(filePath);
        try {
            if (resourceAsStream != null) {
                updateResource = (UpdateResourceRequest) jaxbUnmarshaller.unmarshal(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException | JAXBException ex) {
            resourceAsStream.close();
            throw ex;
        }
        return updateResource;
    }

    public CreateConsumerRequest getCreateConsumerRequest(String filePath) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(CreateConsumerRequest.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        CreateConsumerRequest consumer = null;
        InputStream resourceAsStream = TestHelper.class.getResourceAsStream(filePath);
        try {
            if (resourceAsStream != null) {
                consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException | JAXBException ex) {
            resourceAsStream.close();
            throw ex;
        }
        return consumer;
    }

    public CreateConsumerRequest updateLensSettings(CreateConsumerRequest consumer, Integer id) {
        CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();
        
        settingsList.getInstanceList().clear();
        InstanceList instanceList = new InstanceList();
        instanceList.setInstanceId(new BigInteger(Integer.toString(id)));        
        instanceList.setRateLimitEnabled(false);
        settingsList.getInstanceList().add(instanceList);              

        consumer.setLensSettings(settingsList);

        return consumer;
    }

    public CreateConsumerRequest updateApiandResource(CreateConsumerRequest consumer, Integer apiId, String apiKey, String resourceId) {
        for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
            consumer.getClientApiList().getClientApi().get(i).setApiId(new BigInteger(Integer.toString(apiId)));
            consumer.getClientApiList().getClientApi().get(i).setApiKey(apiKey);
            ClientApi.LicensedResources resourceList = consumer.getClientApiList().getClientApi().get(i).getLicensedResources();
            resourceList.getResourceList().clear();
            resourceList.getResourceList().add(new BigInteger(resourceId));
            consumer.getClientApiList().getClientApi().get(i).setLicensedResources(resourceList);
        }
        return consumer;
    }

    public CreateConsumerRequest updateLicense(CreateConsumerRequest consumer, String activationDate, String expiryDate, String allowedTransaction) {
        for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
            consumer.getClientApiList().getClientApi().get(i).setActivationDate(activationDate);
            consumer.getClientApiList().getClientApi().get(i).setExpiryDate(expiryDate);
            consumer.getClientApiList().getClientApi().get(i).setAllowedTransactions(new BigInteger(allowedTransaction));
        }
        return consumer;
    }

    public UpdateConsumerRequest getUpdateConsumerRequest(String filePath) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(UpdateConsumerRequest.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        UpdateConsumerRequest consumer = null;
        InputStream resourceAsStream = TestHelper.class.getResourceAsStream(filePath);
        try {
            if (resourceAsStream != null) {
                consumer = (UpdateConsumerRequest) jaxbUnmarshaller.unmarshal(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException | JAXBException ex) {
            resourceAsStream.close();
            throw ex;
        }
        return consumer;
    }

    public UpdateConsumerRequest updateLensSettings(UpdateConsumerRequest consumer, Integer id) {
        UpdateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();

        settingsList.getInstanceList().clear();
        InstanceList instanceList = new InstanceList();
        instanceList.setInstanceId(new BigInteger(Integer.toString(id)));
        instanceList.setRateLimitEnabled(false);        
        settingsList.getInstanceList().add(instanceList);  
        
        consumer.setLensSettings(settingsList);

        return consumer;
    }

    public UpdateConsumerRequest updateApiandResource(UpdateConsumerRequest consumer, Integer apiId, String apiKey, String resourceId) {
        for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
            consumer.getClientApiList().getClientApi().get(i).setApiId(new BigInteger(Integer.toString(apiId)));
            consumer.getClientApiList().getClientApi().get(i).setApiKey(apiKey);
            ClientApi.LicensedResources resourceList = consumer.getClientApiList().getClientApi().get(i).getLicensedResources();
            resourceList.getResourceList().clear();
            resourceList.getResourceList().add(new BigInteger(resourceId));
            consumer.getClientApiList().getClientApi().get(i).setLicensedResources(resourceList);
        }
        return consumer;
    }

    public UpdateConsumerRequest updateLicense(UpdateConsumerRequest consumer, String activationDate, String expiryDate, String allowedTransaction ) {
        for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
            consumer.getClientApiList().getClientApi().get(i).setActivationDate(activationDate);
            consumer.getClientApiList().getClientApi().get(i).setExpiryDate(expiryDate);
            consumer.getClientApiList().getClientApi().get(i).setAllowedTransactions(new BigInteger(allowedTransaction));
        }
        return consumer;
    }

    public byte[] convertObjectToJsonBytes(Object object) throws IOException {
        // Convert the Test Model To JSon
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsBytes(object);
    }

    public String getXMLStringFromObject(Object obj) throws JAXBException{

        JAXBContext jc = JAXBContext.newInstance(obj.getClass());
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter stringWriter = new StringWriter();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.marshal(obj, stringWriter);

        return stringWriter.toString();
    }
    
    public NodeList getNodeListFromXPath(String inputString, String xpath) throws XPathExpressionException {
        InputSource is = new InputSource(new StringReader(inputString));
        XPathFactory factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();
        NodeList nodeList = (NodeList) xPath.evaluate(xpath, is, XPathConstants.NODESET);
        return nodeList;
    }

    public String getOuterXml(NodeList nodeList) throws TransformerConfigurationException, TransformerException {
        String response = "";
        if (nodeList != null && nodeList.getLength() > 0) {
            for (int i = 0; i < nodeList.getLength(); ++i) {
                Node nNode = nodeList.item(i);
                StreamResult xmlOutput = new StreamResult(new StringWriter());
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.transform(new DOMSource(nNode), xmlOutput);
                response += xmlOutput.getWriter().toString();
            }
        }
        return response;
    }

    /**
     * Unregister Resume
     *
     * @param searchService
     * @param resumeId
     * @param instanceType
     * @param vendorName
     * @param locale
     * @return
     * @throws Exception
     */
    public ApiResponse unregisterResume(ISearchService searchService, String resumeId, String instanceType, String vendorName, String locale)
            throws Exception {
        UnregisterResumeRequest unregisterRequest = new UnregisterResumeRequest();
        unregisterRequest.setResumeId(resumeId);
        unregisterRequest.setVendor(vendorName);
        unregisterRequest.setInstanceType(instanceType);
        unregisterRequest.setLocale(locale);

        ApiResponse unregisterResponse = searchService.unregisterResume(unregisterRequest);
        return unregisterResponse;
    }

    /**
     * Register resume
     *
     * @param searchService
     * @param apiContext
     * @param helper
     * @param instanceType
     * @param locale
     * @param binaryData
     * @param resumeId
     * @param vendorName
     * @param client
     * @param addCustomFilters
     * @param customFilterValue
     * @return
     * @throws java.lang.Exception
     */
    public ApiResponse registerResume(ISearchService searchService, ApiContext apiContext, Helper helper,
            String instanceType, String locale, String binaryData, String resumeId, String vendorName, CoreClient client, boolean addCustomFilters, Integer customFilterValue)
            throws Exception {
        RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
        registerResumeRequest.setInstanceType(instanceType);
        registerResumeRequest.setLocale(locale);
        registerResumeRequest.setBinaryData(binaryData);
        registerResumeRequest.setResumeId(resumeId);
        registerResumeRequest.setVendor(vendorName);
        registerResumeRequest.setErrorOnDuplicateorOverwrite(registerDuplicate.overwrite.toString());

        if (addCustomFilters) {
            Filter filter01 = new Filter();
            filter01.setName("bgldlanguage");
            filter01.setValue((long)customFilterValue);

            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerResumeRequest.setCustomFilters(filters);
            registerResumeRequest.setRegisterWithCustomFilters(true);
        }

        apiContext.setClient(client);
        helper.setApiContext(apiContext);
        ApiResponse registerResumeResponse = searchService.registerResume(registerResumeRequest);
        return registerResumeResponse;
    }
    
    
    /**
     * Register job
     *
     * @param searchService
     * @param apiContext
     * @param helper
     * @param instanceType
     * @param locale
     * @param binaryData
     * @param jobId
     * @param vendorName
     * @param client
     * @param addCustomFilters
     * @param customFilterValue
     * @return
     * @throws java.lang.Exception
     */
    public ApiResponse registerJob(ISearchService searchService, ApiContext apiContext, Helper helper,
            String instanceType, String locale, String binaryData, String jobId,
            String vendorName, CoreClient client, boolean addCustomFilters, Integer customFilterValue)
            throws Exception {
        RegisterJobRequest registerJobRequest = new RegisterJobRequest();
        registerJobRequest.setInstanceType(instanceType);
        registerJobRequest.setLocale(locale);
        registerJobRequest.setBinaryData(binaryData);
        registerJobRequest.setJobId(jobId);
        registerJobRequest.setVendor(vendorName);
        registerJobRequest.setErrorOnDuplicateorOverwrite(registerDuplicate.overwrite.toString());

        if (addCustomFilters) {
            Filter filter01 = new Filter();
            filter01.setName("bgldlanguage");
            filter01.setValue((long)customFilterValue);

            CustomFilters filters = new CustomFilters();
            filters.getFilter().add(filter01);
            registerJobRequest.setCustomFilters(filters);
            registerJobRequest.setRegisterWithCustomFilters(true);
        }

        apiContext.setClient(client);
        helper.setApiContext(apiContext);
        ApiResponse registerJobResponse = searchService.registerJob(registerJobRequest);
        return registerJobResponse;
    }

    /**
     * Unregister Job
     *
     * @param searchService
     * @param jobId
     * @param instanceType
     * @param vendorName
     * @param locale
     * @return
     * @throws Exception
     */
    public ApiResponse unregisterJob(ISearchService searchService, String jobId, 
            String instanceType, String vendorName, String locale)
            throws Exception {
        UnregisterJobRequest unregisterRequest = new UnregisterJobRequest();
        unregisterRequest.setJobId(jobId);
        unregisterRequest.setVendor(vendorName);
        unregisterRequest.setInstanceType(instanceType);
        unregisterRequest.setLocale(locale);

        ApiResponse unregisterResponse = searchService.unregisterJob(unregisterRequest);
        return unregisterResponse;
    }
    
    /**
     * Get Current Transaction Count
     *
     * @param adminClient
     * @return
     */
    public Integer getCurrentTransactionCount(CoreClient adminClient) {

        Integer currentTransactionCount = 0;

        Predicate condition = new Predicate() {
            @Override
            public boolean evaluate(Object clientApi) {
                return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
            }
        };
        List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(adminClient.getCoreClientapis(), condition);

        if (coreClientApi != null && coreClientApi.size() > 0) {
            currentTransactionCount = coreClientApi.get(0).getRemainingTransactions();
        }

        return currentTransactionCount;

    }

    public Integer getRemainingTransactionCount(CoreClient adminClient, IDataRepositoryService dbService) throws Exception {
        Integer remainingTransactionCount = 0;

        CoreClient client = dbService.getClientById(adminClient.getId());

        Predicate condition = new Predicate() {
            @Override
            public boolean evaluate(Object clientApi) {
                return (((CoreClientapi) clientApi).getCoreApi().getApiKey().equalsIgnoreCase("Search"));
            }
        };
        List<CoreClientapi> coreClientApi = (List<CoreClientapi>) CollectionUtils.select(client.getCoreClientapis(), condition);

        if (coreClientApi != null && coreClientApi.size() > 0) {
            remainingTransactionCount = coreClientApi.get(0).getRemainingTransactions();
        }
        return remainingTransactionCount;
    }
}
