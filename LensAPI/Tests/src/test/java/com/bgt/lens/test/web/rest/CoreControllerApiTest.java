// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateApiRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.CoreController;
import com.bgt.lens.web.rest.UriConstants;
import com.jayway.jsonpath.JsonPath;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import org.springframework.beans.factory.annotation.Value;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.context.WebApplicationContext;

/**
 * Core controller api test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CoreControllerApiTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    /**
     * Core service.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final CoreController coreController = new CoreController();
    /**
     * API context.
     */
    private ApiContext apiContext;

    /**
     * Setup class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        try {
            mockMvc = MockMvcBuilders.
                    standaloneSetup(coreController).build();
            ReflectionTestUtils
                    .setField(coreController,
                            "coreService",
                            coreService);

            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
    }

    /**
     * CoreController CreateApi With ValidData.
     */
    @Test
    public final void coreControllerCreateApiWithValidData() {
        try {
            CreateApiRequest createApi = testhelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            MvcResult result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String content = result.getResponse().getContentAsString();
            assertNotNull(content);
            int apiId = JsonPath.read(content, "$.responseData.id");

            ApiResponse response = coreService.deleteApi(apiId);
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * CreateApiWithDuplicateKeyandName.
     */
    @Test
    public final void createApiWithDuplicateKeyandName() {
        try {
            //Create Api
            CreateApiRequest createApi = testhelper
                    .getCreateApiRequest("/input/CreateApi.xml");

            MvcResult result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String content = result.getResponse().getContentAsString();
            assertNotNull(content);
            int apiId = JsonPath.read(content, "$.responseData.id");

            ApiResponse response = new ApiResponse();

            try {
                mockMvc.perform(post(UriConstants.Api)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testhelper.
                                convertObjectToJsonBytes(createApi))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andDo(print())
                        .andReturn();
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Create Api duplicate check",
                    response.status, false);
            response = coreService.deleteApi(apiId);
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * UpdateApiWithValidData.
     */
    @Test
    public final void updateApiWithValidData() {
        try {
            CreateApiRequest createApi = testhelper
                    .getCreateApiRequest("/input/CreateApi.xml");

            MvcResult result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String content = result.getResponse().getContentAsString();
            assertNotNull(content);
            int apiId = JsonPath.read(content, "$.responseData.id");

            UpdateApiRequest updateApiRequest = testhelper
                    .getUpdateApiRequest("/input/UpdateApi.xml");

            MvcResult updateApiResult = mockMvc.perform(put(UriConstants.Api
                    + "/" + apiId)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(updateApiRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            ApiResponse response = coreService.deleteApi(apiId);
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * UpdateApiWithInvalidApiId.
     */
    @Test
    public final void updateApiWithInvalidApiId() {
        try {
            CreateApiRequest createApi = testhelper
                    .getCreateApiRequest("/input/CreateApi.xml");

            MvcResult result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String content = result.getResponse().getContentAsString();
            assertNotNull(content);
            int apiId = JsonPath.read(content, "$.responseData.id");

            UpdateApiRequest updateApiRequest = testhelper
                    .getUpdateApiRequest("/input/UpdateApi.xml");

            mockMvc.perform(put(UriConstants.Api
                    + "/" + 0)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(updateApiRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            ApiResponse response = coreService.deleteApi(apiId);
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * UpdateApiWithDuplicateApiKeyandName.
     */
    @Test
    public final void updateApiWithDuplicateApiKeyandName() {
        try {
            //Api 1
            CreateApiRequest createApi = testhelper
                    .getCreateApiRequest("/input/CreateApi.xml");

            MvcResult api1Result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String api1Content = api1Result.getResponse().getContentAsString();
            assertNotNull(api1Content);
            int apiId1 = JsonPath.read(api1Content, "$.responseData.id");

            createApi.setKey("Sample02");
            createApi.setName("Sample02");

            MvcResult api2Result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String api2Content = api2Result.getResponse().getContentAsString();
            assertNotNull(api2Content);
            int apiId2 = JsonPath.read(api2Content, "$.responseData.id");

            ApiResponse response = new ApiResponse();
            UpdateApiRequest updateApiRequest = testhelper
                    .getUpdateApiRequest("/input/UpdateApi.xml");
            updateApiRequest.setKey("Sample02");
            updateApiRequest.setName("Sample02");

            try {
                MvcResult updateApiResult = mockMvc.perform(put(UriConstants.Api
                        + "/" + apiId1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testhelper.
                                convertObjectToJsonBytes(updateApiRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andDo(print())
                        .andReturn();

                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }
            Assert.assertEquals("Update Api", response.status, false);

            ApiResponse response1 = coreService.deleteApi(apiId1);
            Assert.assertEquals("Create Api delete", response1.status, true);

            ApiResponse response2 = coreService.deleteApi(apiId2);
            Assert.assertEquals("Create Api delete", response2.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * DeleteApiWithValidId.
     */
    @Test
    public final void deleteApiWithValidId() {
        try {
            CreateApiRequest createApi = testhelper
                    .getCreateApiRequest("/input/CreateApi.xml");

            MvcResult result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String content = result.getResponse().getContentAsString();
            assertNotNull(content);
            int apiId = JsonPath.read(content, "$.responseData.id");

            mockMvc.perform(delete(UriConstants.Api
                    + "/" + apiId)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * DeleteApiWithInvalidId.
     */
    @Test
    public final void deleteApiWithInvalidId() {
        try {
            mockMvc.perform(delete(UriConstants.Api
                    + "/" + 0)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetListofApi.
     */
    @Test
    public final void getListofApi() {
        try {
            CreateApiRequest createApi = testhelper
                    .getCreateApiRequest("/input/CreateApi.xml");

            MvcResult result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String content = result.getResponse().getContentAsString();
            assertNotNull(content);
            int apiId = JsonPath.read(content, "$.responseData.id");

            MvcResult getResult = mockMvc.perform(get(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData.apiList").isArray())
                    .andDo(print())
                    .andReturn();

            String getApiContent = getResult.getResponse().getContentAsString();
            assertNotNull(getApiContent);

            ApiResponse response = coreService.deleteApi(apiId);
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetListofApiWithValidKeyandName.
     */
    @Test
    public final void getListofApiWithValidKeyandName() {
        try {
            CreateApiRequest createApi = testhelper
                    .getCreateApiRequest("/input/CreateApi.xml");

            MvcResult result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String content = result.getResponse().getContentAsString();
            assertNotNull(content);
            int apiId = JsonPath.read(content, "$.responseData.id");

            mockMvc.perform(get(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .param("key", "Core")
                    .param("name", "Core")
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData.apiList").isArray())
                    .andDo(print())
                    .andReturn();

            ApiResponse response = coreService.deleteApi(apiId);
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetListofApiWithInvalidKeyandName.
     */
    @Test
    public final void getListofApiWithInvalidKeyandName() {
        try {
            mockMvc.perform(get(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .param("key", "xxxx")
                    .param("name", "xxxx")
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetApibyIdWithValidApiId.
     */
    @Test
    public final void getApibyIdWithValidApiId() {
        try {
            CreateApiRequest createApi = testhelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            MvcResult result = mockMvc.perform(post(UriConstants.Api)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testhelper.
                            convertObjectToJsonBytes(createApi))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print())
                    .andReturn();

            String content = result.getResponse().getContentAsString();
            assertNotNull(content);
            int apiId = JsonPath.read(content, "$.responseData.id");

            mockMvc.perform(get(UriConstants.Api
                    + "/" + apiId)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData").exists())
                    .andDo(print())
                    .andReturn();

            ApiResponse response = coreService.deleteApi(apiId);
            Assert.assertEquals("Create Api delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * GetApibyIdWithInvalidApiId.
     */
    @Test
    public final void getApibyIdWithInvalidApiId() {
        try {
            mockMvc.perform(get(UriConstants.Api
                    + "/" + 0)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData").exists())
                    .andDo(print())
                    .andReturn();
        } catch (Exception ex) {
            Assert.fail();
        }
    }
}
