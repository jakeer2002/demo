// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.webflow.test.MockRequestContext;

/**
 *
 * Tag binary data with HTM test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryTagBinaryDataWithHTMDataTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * *
     * Helper object
     */
    private static final Helper _helper = new Helper();

    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Lens settings object
     */
    private CoreLenssettings lensSettings;

    /**
     * Helper object
     */
    private TestHelper testHelper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private byte[] binaryData;

    /**
     * Document type
     */
    private Character docType;
    
    private String customSkillKey;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    protected MockHttpServletRequest request;

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {

        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);

        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lensSettings = new CoreLenssettings();
        lensSettings.setHost(testHelper.hostname);
        lensSettings.setPort(testHelper.port);
        lensSettings.setCharacterSet(testHelper.encoding);
        lensSettings.setTimeout(testHelper.timeOut);
        lensSettings.setInstanceType(testHelper.instanceType);
        lensSettings.setLocale(testHelper.locale);
        lensSettings.setStatus(testHelper.status);
        
        customSkillKey = testHelper.customSkillsKey;        

        request = new MockHttpServletRequest();
        ApiContext apiContext = new ApiContext();
        apiContext.setResource("resume");
        apiContext.setInstanceType(testHelper.instanceType);
        request.setAttribute("ApiContext", apiContext);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        String fileName = "/TestResumes/100008.doc";
        InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
        try {
            if (resourceAsStream != null) {
                binaryData = testHelper.getBinaryData(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException ex) {
            resourceAsStream.close();
            throw ex;
        }
        //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
        extension = fileName.substring(fileName.indexOf('.') + 1);
        docType = 'R';
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * TagBinaryDataWithHTMTestWithVaildData
     */
    @Test
    public void tagBinaryDataWithHTMTestWithVaildData() {
        try {
            // TagBinaryDataWithHTM
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithVaildData_WithHtmTag
     */
    @Test
    public void tagBinaryDataWithHTMTestWithVaildData_WithHtmTag() {
        try {
            // TagBinaryDataWithHTM
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, true);
            String expResult = "htmdoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithInvalidHostName
     */
    @Test
    public void tagBinaryDataWithHTMTestWithInvalidHostName() {
        try {
            // TagBinaryDataWithHTM
            lensSettings.setHost("Invalid");
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Invalid Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithEmptyHostName
     */
    @Test
    public void tagBinaryDataWithHTMTestWithEmptyHostName() {
        try {
            // TagBinaryDataWithHTM
            lensSettings.setHost("");
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Empty Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithNullHostName
     */
    @Test
    public void tagBinaryDataWithHTMTestWithNullHostName() {
        try {
            // TagBinaryDataWithHTM
            lensSettings.setHost(null);
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Null Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithInvalidHostPort
     */
    @Test
    public void tagBinaryDataWithHTMTestWithInvalidHostPort() {
        try {
            // TagBinaryDataWithHTM
            lensSettings.setPort(0000);
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Invalid HostPort Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithInValidEncoding
     */
    @Test
    public void tagBinaryDataWithHTMTestWithInValidEncoding() {
        try {
            // TagBinaryDataWithHTM
            lensSettings.setCharacterSet("Invalid");
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithHTM With InValid Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithEmptyEncoding
     */
    @Test
    public void tagBinaryDataWithHTMTestWithEmptyEncoding() {
        try {
            // TagBinaryDataWithHTM
            lensSettings.setCharacterSet("");
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithNullEncoding
     */
    @Test
    public void tagBinaryDataWithHTMTestWithNullEncoding() {
        try {
            // TagBinaryDataWithHTM
            lensSettings.setCharacterSet(null);
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Null Encoding Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithInvalidTimeOut
     */
    @Test
    public void tagBinaryDataWithHTMTestWithInvalidTimeOut() {
        try {
            // TagBinaryDataWithHTM
            lensSettings.setTimeout(-10);
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens TagBinaryDataWithHTM With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithConvertError
     */
    @Test
    public void tagBinaryDataWithHTMTestWithConvertError() throws JAXBException {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_ConverterError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataWithHTM
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, false);
            String expResult = "(tag:6) Converters returned empty output";
            Assert.assertEquals("Lens TagBinaryDataWithHTM With convert error Check", apiErrors.getLensErrorMessage(expResult), lensResponse.responseData);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithTaggingError
     */
    @Test
    public void tagBinaryDataWithHTMTestWithTaggingError() throws JAXBException {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_TaggingError.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataWithHTM
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, false);
            String expResult = "error";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithLargeResumeFile
     */
    @Test
    public void tagBinaryDataWithHTMTestWithLargeResumeFile() throws JAXBException {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataWithHTM
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithNullBytes
     */
    @Test
    public void tagBinaryDataWithHTMTestWithNullBytes() {
        String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
        binaryData = null;
        extension = fileName.substring(fileName.indexOf('.') + 1);
        ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
        Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, false);
        Assert.assertEquals("Lens TagBinaryDataWithHTM With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);
    }

    /**
     * TagBinaryDataWithHTMTestWithUnicodeResume
     */
    @Test
    public void tagBinaryDataWithHTMTestWithUnicodeResume() throws JAXBException {
        try {
            String fileName = "/TestResumes/UnicodeResume/BEL_FRA_10370.tag.xsl.xml";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // TagBinaryDataWithHTM
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, true);
            String expResultUnicode = "tÃºuÃ½Å¾â€žâ€�â€”Ã¡cdÃ©eÃ­nÃ³rÅ TÃšUÃ�Å½â€žâ€�â€”Ã�CDÃ‰EÃ�NÃ“RÃ´Å“Ã¹Ã»Ã¼Ã¿Â«Â»â€”Ã Ã¢Ã§Ã©Ã¨Ãª"
                    + "Ã«Ã¯Ã®Ã”Å’Ã™Ã›ÃœÅ¸Â«Â»â€”Ã€Ã‚Ã‡Ã‰ÃˆÃŠÃ‹Ã�ÃŽÃ¤Ã¶Ã¼ÃŸÃ„Ã–Ãœâ€”Ã Ã¨Ã©Ã¬Ã²Ã³Ã¹Â«Â»â€”Ã€ÃˆÃ‰ÃŒÃ’Ã“Ã™Â«Â»â€”â€žâ€�â€”acelnÃ³s"
                    + "zzâ€žâ€�â€”ACELNÃ“SZZÃµÃ³Ã´ÃºÃ¼â€”Ã£Ã¡Ã¢Ã Ã§Ã©ÃªÃ­Ã•Ã“Ã”ÃšÃœâ€”ÃƒÃ�Ã‚Ã€Ã‡Ã‰ÃŠÃ�aÃ¢Ã®stâ€žâ€�â€”Â«Â»AÃ‚ÃŽSTâ€žâ€�â€”"
                    + "Â«Â»Ã¡Ã©Ã­Ã±Ã³ÃºÃ¼Â¿Â¡â€”Ã�Ã‰Ã�Ã‘Ã“ÃšÃœÂ¿Â¡â€”Ã¤Ã¥Ã Ã©Ã¶â€“Â»Ã„Ã…Ã€Ã‰Ã–â€“Â»Ã§giIÃ¶sÃ¼â€”Ã‡GIIÃ–SÃœâ€”";
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithNullExtension
     */
    @Test
    public void tagBinaryDataWithHTMTestWithNullExtension() throws JAXBException {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = null;
            // TagBinaryDataWithHTM
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithEmptyExtension
     */
    @Test
    public void tagBinaryDataWithHTMTestWithEmptyExtension() throws JAXBException {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "";
            // TagBinaryDataWithHTM
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * TagBinaryDataWithHTMTestWithInvalidExtension
     */
    @Test
    public void tagBinaryDataWithHTMTestWithInvalidExtension() throws JAXBException {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }
            //binaryData = testHelper.getBinaryData(LensRepositoryInfoTest.class.getResourceAsStream(fileName));
            extension = "Invalid";
            // TagBinaryDataWithHTM
            ApiResponse lensResponse = lensRepository.tagBinaryDataWithHTM(lensSettings, binaryData, extension, docType,customSkillKey);
            // TagBinaryDataWithHTM Assert
            Assert.assertEquals("Lens TagBinaryDataWithHTM Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String)lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

}
