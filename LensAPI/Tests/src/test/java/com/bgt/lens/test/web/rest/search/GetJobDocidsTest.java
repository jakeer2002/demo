/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.adminservice.request.SearchSettings.Posting;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.SearchResult;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.rest.response.searchservice.GetJobDocIdsResponse;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.RegisterJobRequest;
import com.bgt.lens.model.searchservice.request.UnregisterJobRequest;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Get job document ids from a vendor with authentication test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class GetJobDocidsTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor adminVendor;
    /**
     * External client vendor
     */
    private Vendor externalClientVendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Admin client
     */
    private CoreClient adminClient;
    /**
     * ExternalClient
     */
    private CoreClient externalClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/JobTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.SpectrumGetdocids_Hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.SpectrumGetdocids_Port)));
            settingsRequest.setCharSet(testHelper.SpectrumGetdocids_Encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.SpectrumGetdocids_TimeOut)));
            settingsRequest.setInstanceType(testHelper.SpectrumGetdocids_InstanceType);
            settingsRequest.setLocale(testHelper.SpectrumGetdocids_Locale);
            settingsRequest.setStatus(testHelper.SpectrumGetdocids_Status);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");

            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            
            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);            

            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("posvendor01");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            adminVendor = (Vendor) response.responseData;

            createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("posvendor0101");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
            createVendorRequest.setType(com.bgt.lens.helpers.Enum.docType.posting.toString());
            response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            externalClientVendor = (Vendor) response.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            consumer.setLensSettings(settingsList);

            SearchSettings searchSettings = new SearchSettings();
            searchSettings.getPosting().clear();
            Posting posting = new Posting();
            posting.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            posting.setVendorId(BigInteger.valueOf(adminVendor.getVendorId()));
            searchSettings.getPosting().add(posting);

            String searchSettingsJson = helper.getJsonStringFromObject(searchSettings);

            CreateConsumerRequest.VendorSettings settings = new CreateConsumerRequest.VendorSettings();
            settings.getVendorIdList().add(BigInteger.valueOf(adminVendor.getVendorId()));
            consumer.setVendorSettings(settings);
            consumer.setSearchSettings(searchSettings);

            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
//            consumer.getVendorSettings().getVendorIdList().clear();
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("job");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);

            adminClient = dbService.getClientById(client.getId());
            adminClient.setTier(1);

            CoreClient updatedAdminClient = adminClient;
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings searchVendorsettings = dbService.getVendorById(adminVendor.getVendorId());
            searchVendorSettingsSet.add(searchVendorsettings);
            updatedAdminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            updatedAdminClient.setSearchSettings(searchSettingsJson);
            ApiResponse updateClientResponse = dbService.updateClient(adminClient, updatedAdminClient);
            Assert.assertTrue("Update client check", updateClientResponse.status);

            //externalClient = consumer; 
            searchVendorSettingsSet = new HashSet<>();
            searchVendorsettings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(searchVendorsettings);
            searchSettings = new SearchSettings();
            searchSettings.getPosting().clear();
            posting = new Posting();
            posting.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            posting.setVendorId(BigInteger.valueOf(externalClientVendor.getVendorId()));
            searchSettings.getPosting().add(posting);
            consumer.setSearchSettings(searchSettings);
            consumer.setKey("Test01");
            consumer.setName("test01");
            consumer.setTier(BigInteger.valueOf(2));
            CreateConsumerRequest.VendorSettings vendorSettingsList
                    = consumer.getVendorSettings();
            vendorSettingsList.getVendorIdList().add(new BigInteger(
                    Integer.toString(externalClientVendor.getVendorId())));
            ApiResponse externalClientCreateresponse = coreService.createConsumer(consumer);
            Assert.assertEquals("Create external consumer check", externalClientCreateresponse.status, true);

            Client external = (Client) externalClientCreateresponse.responseData;

            externalClient = dbService
                    .getClientById(external.getId());
            externalClient.setTier(2);

            String fileName = "/TestJobs/10001.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
             // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);
            ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if (clientList != null && clientList.size() > 0) {
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister job request", deleteExternalClientResponse.status);
            }
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
           

            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get Job document ids.
     */
    @Test
    public final void getJobDocumentIdsAsAdmin() {
       try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "2").param("vendor", adminVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetJobDocIdsResponse getJobDocIdsResponse = (GetJobDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetJobDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getJobDocIdsResponse != null && getJobDocIdsResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getJobDocIdsResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get job document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get job document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='posting' name='posvendor01' count='2' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
            
    
    /**
     * Get Job document ids.
     */
    @Test
    public final void getJobDocumentIdsAsClient() {
       try {
            apiContext.setClient(externalClient);
//            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Get currentTransactionCount

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "2").param("vendor", externalClientVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetJobDocIdsResponse getJobDocIdsResponse = (GetJobDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetJobDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getJobDocIdsResponse != null && getJobDocIdsResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getJobDocIdsResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(externalClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get job document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get job document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='posting' name='posvendor0101' count='2' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get Job document ids with lastDocId.
     */
    @Test
    public final void getJobDocumentIdsAsClientWithLastDocId() {
       try {
            apiContext.setClient(externalClient);
//            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "1").param("vendor", externalClientVendor.getVendor()).param("lastDocId", "1"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetJobDocIdsResponse getJobDocIdsResponse = (GetJobDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetJobDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getJobDocIdsResponse != null && getJobDocIdsResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getJobDocIdsResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(externalClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get job document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get job document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='posting' name='posvendor0101' count='1' lastdocid='1' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }    
        
    /**
     * Get Job document ids with lastDocId.
     */
    @Test
    public final void getJobDocumentIdsAsAdminWithLastDocId() {
       try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "1").param("vendor", adminVendor.getVendor()).param("lastDocId", "1"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetJobDocIdsResponse getJobDocIdsResponse = (GetJobDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetJobDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getJobDocIdsResponse != null && getJobDocIdsResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getJobDocIdsResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get job document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get job document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='posting' name='posvendor01' count='1' lastdocid='1' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }    
    
    /**
     * Get the job document ids with invalid instance type.
     */
    @Test
    public final void getJobDocumentIdsAsClientWithInvalidInstanceType() {
        try {
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Register Assert
            ApiResponse response = new ApiResponse();
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("instanceType", "test").param("locale", TestHelper.Locale.en_us.name()).param("vendor", externalClientVendor.getVendor()))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Fetch job invalid instance type check", response.status);

            ApiResponse unregisterResponse = unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    
    /**
     * Get the job document ids with invalid instance type.
     */
    @Test
    public final void getJobDocumentIdsAsAdminWithInvalidInstanceType() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Register Assert
            ApiResponse response = new ApiResponse();
            response.status = true;
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("instanceType", "test").param("locale", TestHelper.Locale.en_us.name()).param("vendor", adminVendor.getVendor()))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertFalse("Get job document ids with invalid instance type check", response.status);

            ApiResponse unregisterResponse = unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
   
    /**
     * Get the job document ids with invalid local..
     */
    @Test
    public final void getJobDocumentIdsAsClientWithInvalidLocale() {
        try {
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", "test").param("vendor", externalClientVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ApiResponse unregisterResponse = unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * Get the job document ids with invalid local.
     */
    @Test
    public final void getJobDocumentIdsAsAdminWithInvalidLocale() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", "test").param("vendor", adminVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ApiResponse unregisterResponse = unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
 
    /**
     * Get job document ids with invalid vendor.
     */
    @Test
    public final void getJobDocumentIdsAsClientWithInvalidVendor() {
        try {
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("vendor", "vendor"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ApiResponse unregisterResponse = unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get job document ids with invalid vendor.
     */
    @Test
    public final void getJobDocumentIdsAsAdminWithInvalidVendor() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("vendor", "test"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            ApiResponse unregisterResponse = unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
            Assert.assertTrue("Unregister job request", unregisterResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

       /**
     * Get Job document ids with count greater than 10000.
     */
    @Test
    public final void getJobDocumentIdsAsClientWithCountGreaterThan10000() {
       try {
            apiContext.setClient(externalClient);
//            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = registerJob(testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "10001").param("vendor", externalClientVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetJobDocIdsResponse getJobDocIdsResponse = (GetJobDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetJobDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getJobDocIdsResponse != null && getJobDocIdsResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getJobDocIdsResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(externalClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get job document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get job document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='posting' name='posvendor0101' count='10000' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = unregisterJob("1", testHelper.SpectrumGetdocids_InstanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }    
 
     /**
     * Get Job document ids with count greater than 10000.
     */
    @Test
    public final void getJobDocIdsAsAdminWithCountGreaterThan10000() {
       try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.SpectrumGetdocids_InstanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(get(UriConstants.GetJobDocumentsIds)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("instanceType", testHelper.SpectrumGetdocids_InstanceType).param("locale", TestHelper.Locale.en_us.name()).param("count", "10001").param("vendor", adminVendor.getVendor()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetJobDocIdsResponse getJobDocIdsResponse = (GetJobDocIdsResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetJobDocIdsResponse());

            ObjectMapper mapper;
            byte[] json;

            if (getJobDocIdsResponse != null && getJobDocIdsResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(getJobDocIdsResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Get job document ids command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Get job document ids command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><vendor type='posting' name='posvendor01' count='10000' command='getdocids'/></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.SpectrumGetdocids_InstanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }    
   

    /**
     * unregisterJob
     *
     * @param jobId
     * @param instanceType
     * @param vendorName
     * @param locale
     * @return
     * @throws Exception
     */
    public ApiResponse unregisterJob(String jobId, String instanceType, String vendorName, String locale) throws Exception {
        UnregisterJobRequest unregisterRequest = new UnregisterJobRequest();
        unregisterRequest.setJobId(jobId);
        unregisterRequest.setVendor(vendorName);
        unregisterRequest.setInstanceType(instanceType);
        unregisterRequest.setLocale(locale);

        ApiResponse unregisterResponse = searchService.unregisterJob(unregisterRequest);
        return unregisterResponse;
    }

    /**
     * Register job
     *
     * @param instanceType
     * @param locale
     * @param binaryData
     * @param jobId
     * @param vendorName
     * @param client
     * @return
     */
    public ApiResponse registerJob(String instanceType, String locale, String binaryData, String jobId, String vendorName, CoreClient client) throws Exception {
        RegisterJobRequest registerJobRequest = new RegisterJobRequest();
        registerJobRequest.setInstanceType(instanceType);
        registerJobRequest.setLocale(locale);
        registerJobRequest.setBinaryData(binaryData);
        registerJobRequest.setJobId(jobId);
        registerJobRequest.setVendor(vendorName);
        registerJobRequest.setErrorOnDuplicateorOverwrite(com.bgt.lens.helpers.Enum.registerDuplicate.overwrite.toString());
        apiContext.setClient(client);
        helper.setApiContext(apiContext);
        ApiResponse registerJobResponse = searchService.registerJob(registerJobRequest);
        return registerJobResponse;
    }
}
