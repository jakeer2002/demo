/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest.CustomSkillsSettings;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.CustomSkillsList;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import com.jayway.jsonpath.JsonPath;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Parse Resume Structured BGT XML test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ParseResumeStructuredBGXMLTests {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Autowired
    IUtilityService utilityService;

    /**
     * API context
     */
    public ApiContext apiContext;

    /**
     * API
     */
    public Api api;

    /**
     * Lens settings
     */
    public LensSettings en_usLensSettings, en_ukLensSettings, localeSettings,unicodeLensSettings, TM_GB_LensSettings;

    /**
     * Resource
     */
    public Resources resource;

    /**
     * Client
     */
    public Client client;

    /**
     * Test helper object
     */
    public TestHelper testHelper;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private String binaryData;

    /**
     * Resume parsing variant
     */
    public static final String variant = "structuredbgtxml";

    /**
     * Instance type
     */
    public static final String InstanceType = "SPECTRUM";

    /**
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";
    
    /**
     * Locale UK
     */
    public static final String LocaleType = "NA";

    /**
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * Core client
     */
    private CoreClient coreClient;
    
    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     *
     * @throws JAXBException
     */
    @Before
    public void setUp()  throws JAXBException {
        ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
           servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        try {
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
             //Create Api
            CreateApiRequest createApi = testHelper.getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;
            
            settingsRequest.setLocale("en_us");
            settingsRequest.setCountry("UnitedStates");
            settingsRequest.setHost(testHelper.OpticCustomRollupInstanceHost);
            settingsRequest.setInstanceType(testHelper.OpticCustomRollupInstanceType);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.OpticCustomRollupInstancePort)));
            settingsRequest.setLocale(testHelper.OpticCustomRollupInstanceLocale);
            settingsRequest.setCharSet("utf-8");
            CustomSkillsSettings customSkillSettings = new CustomSkillsSettings();
            List<CustomSkillsList> customSkillList = new ArrayList<>();
            CustomSkillsList custom = new CustomSkillsList();
            custom.setKey("rand002");
            custom.setName("rand002");
            customSkillList.add(custom);
            customSkillSettings.getCustomSkillsList().addAll(customSkillList);
            settingsRequest.setCustomSkillsSettings(customSkillSettings);
            ApiResponse en_us_unicode_LensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings unicode instance check", en_us_unicode_LensSettingsResponse.status, true);
            unicodeLensSettings = (LensSettings) en_us_unicode_LensSettingsResponse.responseData;
            
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            settingsRequest.setCustomSkillsSettings(null);
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;
            
            //creating a LENS Setting for GB Optic Jobs
            settingsRequest.setHost(testHelper.TM_GB_Hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.TM_GB_Port)));
            settingsRequest.setCharSet(testHelper.TM_Encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.TM_TimeOut)));
            settingsRequest.setInstanceType(testHelper.TM_InstanceType);
            settingsRequest.setLocale(testHelper.TM_GB_Locale);
            settingsRequest.setStatus(testHelper.TM_Status);
            settingsRequest.setCountry(testHelper.TM_Country);
            // creating Settings Response for GB Optic Jobs
            ApiResponse TMGBLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", TMGBLensSettingsResponse.status, true);
            TM_GB_LensSettings = (LensSettings) TMGBLensSettingsResponse.responseData;
            
            // create resource
            AddResourceRequest resourceRequest = testHelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            
            //CreateConsumerRequest consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(ParseResumeStructuredBGTXMLTests.class.getResourceAsStream("/input/CreateConsumer.xml"));

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();
            
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList unicode_en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList(); 
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList TMGBinstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_usLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);
            unicode_en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(unicodeLensSettings.getId())));
            unicode_en_us_InstanceList.setCustomSkillsId(new BigInteger(Integer.toString(unicodeLensSettings.getCustomSkillsSettings().getCustomSkillsList().get(0).getId())));
            unicode_en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(unicode_en_us_InstanceList); 
            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_ukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList); 
            
            TMGBinstanceList.setInstanceId(new BigInteger(Integer.toString(TM_GB_LensSettings.getId())));
            TMGBinstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(TMGBinstanceList);
            
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);

            consumer.setLensSettings(settingsList);
            consumer = testHelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;

            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            
            // en_us LensSettings
            CoreLenssettings en_usCoreLenssettings = new CoreLenssettings();
            en_usCoreLenssettings.setId(1);
            en_usCoreLenssettings.setHost(testHelper.hostname);
            en_usCoreLenssettings.setPort(testHelper.port);
            en_usCoreLenssettings.setCharacterSet(testHelper.encoding);
            en_usCoreLenssettings.setTimeout(testHelper.timeOut);
            en_usCoreLenssettings.setInstanceType(testHelper.instanceType);
            en_usCoreLenssettings.setLocale(testHelper.locale);
            en_usCoreLenssettings.setStatus(testHelper.status);

            // en_uk LensSettings
            CoreLenssettings en_ukCoreLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            en_ukCoreLenssettings.setId(2);
            en_ukCoreLenssettings.setLocale(LocaleTypeEn_uk);
            
            CoreLenssettings unicodeCoreLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            unicodeCoreLenssettings.setId(4);
            unicodeCoreLenssettings.setLocale(testHelper.locale);   
            unicodeCoreLenssettings.setHost(testHelper.OpticCustomRollupInstanceHost);
            unicodeCoreLenssettings.setPort(testHelper.OpticCustomRollupInstancePort);
            unicodeCoreLenssettings.setCharacterSet("utf-8");
            unicodeCoreLenssettings.setInstanceType(testHelper.OpticCustomRollupInstanceType);
            unicodeCoreLenssettings.setLocale(testHelper.OpticCustomRollupInstanceLocale);
            
            // en_us LensSettings
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(3);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            
            // TM GB LensSettings
            CoreLenssettings TMGBCoreLenssettings = new CoreLenssettings();
            TMGBCoreLenssettings.setId(4);
            TMGBCoreLenssettings.setHost(testHelper.TM_GB_Hostname);
            TMGBCoreLenssettings.setPort(testHelper.TM_GB_Port);
            TMGBCoreLenssettings.setCharacterSet(testHelper.TM_Encoding);
            TMGBCoreLenssettings.setTimeout(testHelper.TM_TimeOut);
            TMGBCoreLenssettings.setInstanceType(testHelper.TM_InstanceType);
            TMGBCoreLenssettings.setLocale(testHelper.TM_GB_Locale);
            TMGBCoreLenssettings.setStatus(testHelper.TM_Status);

            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings unicode_CoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings TMGBCoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(en_usCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(en_ukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            unicode_CoreClientlenssettings.setCoreLenssettings(unicodeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings); 
            coreClientlenssettingses.add(unicode_CoreClientlenssettings);
            TMGBCoreClientlenssettings.setCoreLenssettings(TMGBCoreLenssettings);
            coreClientlenssettingses.add(TMGBCoreClientlenssettings);
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);


            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            apiContext = new ApiContext();
            apiContext.setMethod("Post");
            apiContext.setResource("resume");
            apiContext.setInstanceType(InstanceType);
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = ParseResumeStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeStructuredBGXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete", deleteApiResponse.status, true);
            // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete", deleteConsumerResponse.status, true);
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse OpticCustomRollupDeleteLensSettingsResponse = coreService.deleteLensSettings(unicodeLensSettings.getId());
            Assert.assertEquals("Delete unicode Lens settings delete", OpticCustomRollupDeleteLensSettingsResponse.status, true);
   
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);
            
            ApiResponse TMGBDeleteSettingsResponse = coreService.deleteLensSettings(TM_GB_LensSettings.getId());
            Assert.assertEquals("Delete locale settings delete", TMGBDeleteSettingsResponse.status, true);
            

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    @Test
    public void parseResumeStructuredBGTXMLWithLanguageProficiencyOpticJobsGB() {
        try {
            String fileName = "/TestResumes/EngSampleResume.cln";
            InputStream resourceAsStream04 = ParseResumeStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(testHelper.TM_InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_gb.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            //System.out.println("Response");
            //System.out.print(response);
            // ParseResumeBGTXML Assert
            String expResultProficiency = "proficiency";
            String expResultProficiencyLevel = "proficiency-level";
            String convertResume = (String) response.responseData;
            //System.out.println("Response convertResume");
            //System.out.print(convertResume);
            boolean checkContainsProficiency = convertResume.contains(expResultProficiency);
            assertTrue(checkContainsProficiency);
            boolean checkContainsProficiencylevel = convertResume.contains(expResultProficiencyLevel);
            assertTrue(checkContainsProficiencylevel);
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("parseResumeStructuredBGTXMLWithLanguageProficiencyOpticJobsGB With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    @Test
    public void parseResumeStructuredBGTXMLWithCanonSkilltypeOpticJobsGB() {
        try {
            String fileName = "/TestResumes/EngSampleResume.cln";
            InputStream resourceAsStream04 = ParseResumeStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(testHelper.TM_InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_gb.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            //System.out.println("Response");
            //System.out.print(response);
            // ParseResumeBGTXML Assert
            String expResult = "canonskilltype";
            String convertResume = (String) response.responseData;
            
            boolean checkContainsProficiency = convertResume.contains(expResult);
            assertTrue(checkContainsProficiency);
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria);
            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("parseResumeStructuredBGTXMLWithCanonSkilltypeOpticJobsGB With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    @Test
    public void parseResumeStructuredBGTXMLWithLocalewithMicroservice() {
        try {
            String fileName = "/TestResumes/BoldResume11.pdf";
            InputStream resourceAsStream04 = ParseResumeBGTXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariantMicroservice(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseResumeStructuredBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeStructuredBGTXMLWithValidBinaryResumeDataWithLocale() {
        try {
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    @Test
    public void CustomRollupCheck_EmitSingleSkillRollupAsList() {
        try {
            // CustomRollupCheck
            apiContext.setAccept("application/json");
            apiContext.setInstanceType("TM");
            _helper.setApiContext(apiContext);
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType("TM");
            String fileName = "/TestResumes/resume1.doc";
            InputStream resourceAsStream04 = ParseResumeWithHtmTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    parseResumeRequest.setBinaryData(_helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04)));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            
            parseResumeRequest.setExtension(fileName.substring(fileName.indexOf('.') + 1));
//            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            net.sf.json.JSONObject resDoc = (net.sf.json.JSONObject)response.responseData;
            Object customrollup = JsonPath.read(resDoc,"$.ResDoc.customrollup");
            assert(customrollup instanceof net.sf.json.JSONArray);
            Object skillrollup = JsonPath.read(resDoc,"$.ResDoc.customrollup[0].skillrollup");
            assert(skillrollup instanceof net.sf.json.JSONArray);
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    } 
    
    @Test
    public void CustomRollupCheck_EmitMultipleSkillRollupAsList() {
        try {
            // CustomRollupCheck
            apiContext.setAccept("application/json");
            apiContext.setInstanceType("TM");
            _helper.setApiContext(apiContext);
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType("TM");
            String fileName = "/TestResumes/resume2.docx";
            InputStream resourceAsStream04 = ParseResumeWithHtmTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    parseResumeRequest.setBinaryData(_helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04)));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            
            parseResumeRequest.setExtension(fileName.substring(fileName.indexOf('.') + 1));
//            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            net.sf.json.JSONObject resDoc = (net.sf.json.JSONObject)response.responseData;
            Object customrollup = JsonPath.read(resDoc,"$.ResDoc.customrollup");
            assert(customrollup instanceof net.sf.json.JSONArray);
            Object skillrollup = JsonPath.read(resDoc,"$.ResDoc.customrollup[0].skillrollup");
            assert(skillrollup instanceof net.sf.json.JSONArray);
            
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    } 

    /**
     * *
     * ParseResumeStructuredBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeStructuredBGTXMLWithValidBinaryResumeDataWithDefaultLocale() {
        try {
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry(TestHelper.Locale.en_us.name());
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseResumeStructuredBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeStructuredBGTXMLWithValidBinaryResumeDataLocaleBasedParsing() {
        try {
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "en_us", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseResumeStructuredBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeStructuredBGTXMLWithValidBinaryResumeDataLocaleBasedParsingWithDefaultCountry() {
        try {
            String fileName = "/TestResumes/16343.doc";
            InputStream resourceAsStream04 = ParseResumeStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "en_gb", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
     /**
     * *
     * ParseResumeStructuredBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeStructuredBGTXMLWithValidBinaryResumeDataLocaleBasedParsingWithUnknownLocale() {
        try {
            String fileName = "/TestResumes/UnknownLocale.doc";
            InputStream resourceAsStream04 = ParseResumeStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "xl_xx", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
     /**
     * *
     * ParseResumeStructuredBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeStructuredBGTXMLWithValidBinaryResumeDataLocaleBasedParsingWithUnallocatedLocale() {
        try {
            String fileName = "/TestResumes/Resume_JPN.txt";
            InputStream resourceAsStream04 = ParseResumeStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary ResumeData check", response.status, true);
            Assert.assertEquals("Locale check", "ja_jp", response.identifiedLocale);
            Assert.assertEquals("Locale check", "en_us", response.processedWithLocale);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseResumeStructuredBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeStructuredBGTXMLWithValidBinaryResumeDataLocaleBasedParsingWithInactiveInstance() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;
            
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());
            coreClientApiContext.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClientApiContext.setEnableLocaleDetection(true);
            apiContext.setClient(coreClientApiContext);
            _helper.setApiContext(apiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With InActive Lens check", response.status, false);
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeStructuredBGTXMLWithConvertError
     */
    @Test
    public void parseResumeStructuredBGTXMLWithConvertError() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            InputStream resourceAsStream04 = ParseResumeStructuredBGXMLTests.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeStructuredBGXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With Empty BinaryData check", response.status, false);

            // ParseResumeBGTXML Assert            
            String convertResume = (String) response.responseData;
            String expResult = "Converters returned empty output";
            Assert.assertEquals("ParseResumeBGTXML With convert error Check", apiErrors.getLensErrorMessage(expResult), convertResume);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With Empty BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeStructuredBGTXMLWithConvertErrorStatus
     */
    @Test
    public void parseResumeStructuredBGTXMLWithConvertErrorStatus() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResumeBGTXML With Null BinaryData check", response.status, false);

            // ParseResumeBGTXML Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With Null BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeStructuredBGTXMLSpecificLensInstance
     */
    @Test
    public void parseResumeStructuredBGTXMLSpecificLensInstance() {
        try {
            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML Specific Lens Instance check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "ResDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResumeBGTXML With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeStructuredBGTXMLUnauthorizedInstance
     */
    @Test
    public void parseResumeStructuredBGTXMLUnauthorizedInstance() {
        try {
            // ParseResumeBGTXML
            String localeType = "en_anz";
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(localeType);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With Unauthorized Instance check", response.status, false);

            // ParseResumeBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeStructuredBGTXMLEmptyInstanceList
     */
    @Test
    public void parseResumeStructuredBGTXMLEmptyInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResumeBGTXML With Empty Instance List check", response.status, false);

            // ParseResumeBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeStructuredBGTXMLNullInstanceList
     */
    @Test
    public void parseResumeStructuredBGTXMLNullInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);
            coreClientApiContext.setCoreClientlenssettingses(null);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResumeBGTXML With Null Instance List check", response.status, false);

            // ParseResumeBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeStructuredBGTXMLInvalidLensList
     */
    @Test
    public void parseResumeStructuredBGTXMLInvalidLensList() {
        try {
           //Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClient);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With Lens Invalid  check", response.status, false);

            // ParseResumeBGTXML en_us Assert
            String instanceResponse = (String) response.responseData;
            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeStructuredBGTXMLInActiveLensList
     */
    @Test
    public void parseResumeStructuredBGTXMLInActiveLensList() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setClient(coreClient);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // ParseResumeBGTXML
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_us);
            ApiResponse response = utilityService.parseResumeVariant(parseResumeRequest, variant);
            Assert.assertEquals("ParseResumeBGTXML With InActive Lens check", response.status, false);

            // ParseResumeBGTXML inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            Assert.assertEquals("ParseResumeBGTXML With InActive Lens check", _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS), instanceResponse);
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            response = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", response.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
