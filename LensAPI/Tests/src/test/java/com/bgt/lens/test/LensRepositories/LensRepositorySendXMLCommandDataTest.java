// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Send XML command test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositorySendXMLCommandDataTest {

    
     @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;
    
    
    private ApiErrors apiErrors = new ApiErrors();
    
    /**
     * *
     * Helper object
     */
    private static final Helper _helper = new Helper();
    
    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Lens settings object
     */
    private CoreLenssettings lensSettings;

    /**
     * Helper object
     */
    private TestHelper testHelper;

    /**
     * Xml Command
     */
    private static final String XMLCommand = "<bgtcmd><tag type='resume'><![CDATA[ this is simple test]]></tag></bgtcmd>";

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
            
        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        
        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lensSettings = new CoreLenssettings();
        lensSettings.setHost(testHelper.hostname);
        lensSettings.setPort(testHelper.port);
        lensSettings.setCharacterSet(testHelper.encoding);
        lensSettings.setTimeout(testHelper.timeOut);
        lensSettings.setInstanceType(testHelper.instanceType);
        lensSettings.setLocale(testHelper.locale);
        lensSettings.setStatus(testHelper.status);
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * SendXMLCommandTestWithVaildData
     */
    @Test
    public void sendXMLCommandTestWithVaildData() {
        try {
            // SendXMLCommand
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, XMLCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand Status Check", lensResponse.status, true);
            String expResult = "ResDoc";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * SendXMLCommandTestWithInvalidHostName
     */
    @Test
    public void sendXMLCommandTestWithInvalidHostName() {
        try {
            // SendXMLCommand
            lensSettings.setHost("Invalid");
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, XMLCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens SendXMLCommand With Invalid Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * SendXMLCommandTestWithEmptyHostName
     */
    @Test
    public void sendXMLCommandTestWithEmptyHostName() {
        try {
            // SendXMLCommand
            lensSettings.setHost("");
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, XMLCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens SendXMLCommand With Empty Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * SendXMLCommandTestWithNullHostName
     */
    @Test
    public void sendXMLCommandTestWithNullHostName() {
        try {
            // SendXMLCommand
            lensSettings.setHost(null);
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, XMLCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens SendXMLCommand With Null Hostname Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * SendXMLCommandTestWithInvalidHostPort
     */
    @Test
    public void sendXMLCommandTestWithInvalidHostPort() {
        try {
            // SendXMLCommand
            lensSettings.setPort(0000);
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, XMLCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens SendXMLCommand With Invalid HostPort Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * SendXMLCommandTestWithInValidEncoding
     */
    @Test
    public void sendXMLCommandTestWithInValidEncoding() {
        try {
            // SendXMLCommand
            lensSettings.setCharacterSet("Invalid");
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, XMLCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens SendXMLCommand With InValid Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * SendXMLCommandTestWithEmptyEncoding
     */
    @Test
    public void sendXMLCommandTestWithEmptyEncoding() {
        try {
            // SendXMLCommand
            lensSettings.setCharacterSet("");
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, XMLCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens SendXMLCommand With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * SendXMLCommandTestWithNullEncoding
     */
    @Test
    public void sendXMLCommandTestWithNullEncoding() {
        try {
            // SendXMLCommand
            lensSettings.setCharacterSet(null);
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, XMLCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand With Null Encoding Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * SendXMLCommandTestWithInvalidTimeOut
     */
    @Test
    public void sendXMLCommandTestWithInvalidTimeOut() {
        try {
            // SendXMLCommand
            lensSettings.setTimeout(-10);
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, XMLCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens SendXMLCommand With Empty Encoding Status Check", _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * SendXMLCommandTestWithEmptyXMLCommand
     */
    @Test
    public void sendXMLCommandTestWithEmptyXMLCommand() {
        try {
            // SendXMLCommand
            lensRepository.sendXMLCommand(lensSettings, null);
        } catch (Exception ex) {
            if (ex instanceof NullPointerException) {
                Assert.assertTrue(true);
            }
        }
    }

    /**
     * SendXMLCommandTestWithInvalidXMLCommand
     */
    @Test
    public void sendXMLCommandTestWithInvalidXMLCommand() {
        try {
            String xmlCommand = "<bgtcmd><tag type='resume'><![CDATA[ this is simple test]]></tag>";
            // SendXMLCommand
            ApiResponse lensResponse = lensRepository.sendXMLCommand(lensSettings, xmlCommand);
            // SendXMLCommand Assert
            Assert.assertEquals("Lens SendXMLCommand Status Check", lensResponse.status, false);
            String expResult = "error";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

}
