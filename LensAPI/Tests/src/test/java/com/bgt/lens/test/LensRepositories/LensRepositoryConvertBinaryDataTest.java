// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * LensRepositoryConvertBinaryDataTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryConvertBinaryDataTest {

    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Lens Settings object
     */
    private CoreLenssettings lensSettings;

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    /**
     * Test helper object
     */
    private TestHelper testHelper;
    
   
    private ApiErrors apiErrors = new ApiErrors();
    
    
    private Helper helper =  new Helper();

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private byte[] binaryData;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {

        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);

        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lensSettings = new CoreLenssettings();
        lensSettings.setHost(testHelper.hostname);
        lensSettings.setPort(testHelper.port);
        lensSettings.setCharacterSet(testHelper.encoding);
        lensSettings.setTimeout(testHelper.timeOut);
        lensSettings.setInstanceType(testHelper.instanceType);
        lensSettings.setLocale(testHelper.locale);
        lensSettings.setStatus(testHelper.status);

        String fileName = "/TestResumes/100008.doc";

        InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
        try {
            if (resourceAsStream != null) {
                binaryData = testHelper.getBinaryData(resourceAsStream);
                resourceAsStream.close();
            }
        } catch (IOException ex) {
            resourceAsStream.close();
            throw ex;
        }
        extension = fileName.substring(fileName.indexOf('.') + 1);
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * ConvertBinaryTestWithVaildData
     */
    @Test
    public void convertBinaryTestWithVaildData() {
        try {
            // ConvertBinary
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary Status Check", lensResponse.status, true);
            String expResult = "EDRISH HABIBULLAH";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryTestWithInvalidHostName
     */
    @Test
    public void convertBinaryTestWithInvalidHostName() {
        try {
            // ConvertBinary
            lensSettings.setHost("Invalid");
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary With Invalid Hostname Status Check", lensResponse.status, false);            
            Assert.assertEquals("Lens ConvertBinary With Invalid Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryTestWithEmptyHostName
     */
    //@Test
    public void convertBinaryTestWithEmptyHostName() {
        try {
            // ConvertBinary
            lensSettings.setHost("");
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens ConvertBinary With Empty Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryTestWithNullHostName
     */
    //@Test
    public void convertBinaryTestWithNullHostName() {
        try {
            // ConvertBinary
            lensSettings.setHost(null);
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens ConvertBinary With Null Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryTestWithInvalidHostPort
     */
    @Test
    public void convertBinaryTestWithInvalidHostPort() {
        try {
            // ConvertBinary
            lensSettings.setPort(0000);
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens ConvertBinary With Invalid HostPort Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryTestWithInValidEncoding
     */
    @Test
    public void convertBinaryTestWithInValidEncoding() {
        try {
            // ConvertBinary
            lensSettings.setCharacterSet("Invalid");
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens ConvertBinary With InValid Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryTestWithEmptyEncoding
     */
    @Test
    public void convertBinaryTestWithEmptyEncoding() {
        try {
            // ConvertBinary
            lensSettings.setCharacterSet("");
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens ConvertBinary With Empty Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryTestWithNullEncoding
     */
    @Test
    public void convertBinaryTestWithNullEncoding() {
        try {
            // ConvertBinary
            lensSettings.setCharacterSet(null);
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary With Null Encoding Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryTestWithInvalidTimeOut
     */
    @Test
    public void convertBinaryTestWithInvalidTimeOut() {
        try {
            // ConvertBinary
            lensSettings.setTimeout(-10);
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary With Empty Encoding Status Check", lensResponse.status, false);           
            Assert.assertEquals("Lens ConvertBinary With Empty Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryConvertError
     */
    @Test
    public void convertBinaryConvertError() {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_ConverterError.doc";

            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }

            //binaryData = testHelper.getBinaryData(LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ConvertBinary
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary Status Check", lensResponse.status, false);           
            String lensResponseMessage = (String) lensResponse.responseData;
            String expResult = "Converters returned empty output";
            Assert.assertEquals("Lens ConvertBinary With convert error Check", apiErrors.getLensErrorMessage(expResult), lensResponseMessage);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryConvertError_Space
     */
    @Test
    public void convertBinaryConvertError_Space() {
        try {
            String fileName = "/TestResumes/InvalidDocuments/ExternalResume_ConverterError.doc";

            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }

            //binaryData = testHelper.getBinaryData(LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ConvertBinary
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary Status Check", lensResponse.status, false);            
            String lensResponseMessage = (String) lensResponse.responseData;
            String expResult = "Converters returned empty output";
            Assert.assertEquals("Lens ConvertBinary With convert error Check", apiErrors.getLensErrorMessage(expResult), lensResponseMessage);
            

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryLargeResumeFile
     */
    @Test
    public void convertBinaryLargeResumeFile() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";

            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }

            //binaryData = testHelper.getBinaryData(LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ConvertBinary
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary Status Check", lensResponse.status, true);
            String expResult = "Consult ABLE Team GmbH";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryNullBytes
     */
    @Test
    public void convertBinaryNullBytes() {
        String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
        binaryData = null;
        extension = fileName.substring(fileName.indexOf('.') + 1);
        ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
        Assert.assertEquals("Lens ConvertBinary Status Check", lensResponse.status, false);
        Assert.assertEquals("Lens ConvertBinary With Empty Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);
    }

    /**
     * ConvertBinaryUnicodeResume
     */
    @Test
    public void convertBinaryUnicodeResume() {
        try {
            String fileName = "/TestResumes/UnicodeResume/BEL_FRA_10370.tag.xsl.xml";

            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }

            //binaryData = testHelper.getBinaryData(LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ConvertBinary
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary Status Check", lensResponse.status, true);
            String expResultUnicode = "tÃºuÃ½Å¾â€žâ€�â€”Ã¡cdÃ©eÃ­nÃ³rÅ TÃšUÃ�Å½â€žâ€�â€”Ã�CDÃ‰EÃ�NÃ“RÃ´Å“Ã¹Ã»Ã¼Ã¿Â«Â»â€”Ã Ã¢Ã§Ã©Ã¨Ãª"
                    + "Ã«Ã¯Ã®Ã”Å’Ã™Ã›ÃœÅ¸Â«Â»â€”Ã€Ã‚Ã‡Ã‰ÃˆÃŠÃ‹Ã�ÃŽÃ¤Ã¶Ã¼ÃŸÃ„Ã–Ãœâ€”Ã Ã¨Ã©Ã¬Ã²Ã³Ã¹Â«Â»â€”Ã€ÃˆÃ‰ÃŒÃ’Ã“Ã™Â«Â»â€”â€žâ€�â€”acelnÃ³s"
                    + "zzâ€žâ€�â€”ACELNÃ“SZZÃµÃ³Ã´ÃºÃ¼â€”Ã£Ã¡Ã¢Ã Ã§Ã©ÃªÃ­Ã•Ã“Ã”ÃšÃœâ€”ÃƒÃ�Ã‚Ã€Ã‡Ã‰ÃŠÃ�aÃ¢Ã®stâ€žâ€�â€”Â«Â»AÃ‚ÃŽSTâ€žâ€�â€”"
                    + "Â«Â»Ã¡Ã©Ã­Ã±Ã³ÃºÃ¼Â¿Â¡â€”Ã�Ã‰Ã�Ã‘Ã“ÃšÃœÂ¿Â¡â€”Ã¤Ã¥Ã Ã©Ã¶â€“Â»Ã„Ã…Ã€Ã‰Ã–â€“Â»Ã§giIÃ¶sÃ¼â€”Ã‡GIIÃ–SÃœâ€”";
            String expResult = "BEL_FRA_10370.tag Avenue chazal";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryNullExtension
     */
    @Test
    public void convertBinaryNullExtension() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";

            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }

            //binaryData = testHelper.getBinaryData(LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName));
            extension = null;
            // ConvertBinary
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary Status Check", lensResponse.status, true);
            String expResult = "Consult ABLE Team GmbH";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryEmptyExtension
     */
    @Test
    public void convertBinaryEmptyExtension() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";

            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }

            //binaryData = testHelper.getBinaryData(LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName));
            extension = "";
            // ConvertBinary
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary Status Check", lensResponse.status, true);
            String expResult = "Consult ABLE Team GmbH";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * ConvertBinaryInvalidExtension
     */
    @Test
    public void convertBinaryInvalidExtension() {
        try {
            String fileName = "/TestResumes/LargeResume/ExternalResume.doc";
            InputStream resourceAsStream = LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream != null) {
                    binaryData = testHelper.getBinaryData(resourceAsStream);
                    resourceAsStream.close();
                }
            } catch (IOException ex) {
                resourceAsStream.close();
                throw ex;
            }

            //binaryData = testHelper.getBinaryData(LensRepositoryConvertBinaryDataTest.class.getResourceAsStream(fileName));
            extension = "Invalid";
            // ConvertBinary
            ApiResponse lensResponse = lensRepository.convertBinaryData(lensSettings, binaryData, extension);
            // ConvertBinary Assert
            Assert.assertEquals("Lens ConvertBinary Status Check", lensResponse.status, true);
            String expResult = "Consult ABLE Team GmbH";
            String lensResponseMessage = (String) lensResponse.responseData;
            boolean checkContains = lensResponseMessage.contains(expResult);
            assertTrue(checkContains);

        } catch (IOException ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

}
