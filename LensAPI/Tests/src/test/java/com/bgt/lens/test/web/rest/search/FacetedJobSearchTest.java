// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Enum;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest.VendorSettings;
import com.bgt.lens.model.adminservice.request.Filter;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.SearchResult;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.AdditionalFilters;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.CustomFilters;
import com.bgt.lens.model.searchservice.request.Facets;
import com.bgt.lens.model.searchservice.request.JobSearchRequest;
import com.bgt.lens.model.searchservice.request.Keywords;
import com.bgt.lens.model.searchservice.request.PaginateResults;
import com.bgt.lens.model.searchservice.request.ResultFacets;
import com.bgt.lens.model.searchservice.request.SearchCriteria;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Job;
import com.bgt.lens.model.searchservice.request.SearchCriteria.Keyword;
import com.bgt.lens.model.searchservice.request.SelectedFilters;
import com.bgt.lens.model.soap.response.searchservice.JobSearchResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Search Job Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class FacetedJobSearchTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor adminVendor;
    /**
     * External client vendor
     */
    private Vendor externalClientVendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Admin client
     */
    private CoreClient adminClient;
    /**
     * ExternalClient
     */
    private CoreClient externalClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Extension
     */
    private String extension;
    /**
     * Doc type
     */
    private Character docType;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/JobTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            createApi.setKey("Search");
            createApi.setName("Search Api");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            Filter filter = new Filter();
            filter.setKey(testHelper.hypercubeDimensionName01);
            filter.setName("Jobtitle");
            Filter filter01 = new Filter();
            filter01.setKey(testHelper.hypercubeDimensionName02);
            filter01.setName("Skills");
            Filter filter02 = new Filter();
            filter02.setKey(testHelper.hypercubeDimensionName03);
            filter02.setName("Employer");
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);
            settingsRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter02);

            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setFilterSettings(null);
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");
            resourceRequest.setApiKey(api.getKey());
            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            
            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            

            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("posvendor01");
            createVendorRequest.setType("posting");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
//            createVendorRequest.setHypercubeServerName(testHelper.postingHypercubeServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            adminVendor = (Vendor) response.responseData;

            createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("posvendor0202");
            createVendorRequest.setType("posting");
//            createVendorRequest.setDocServerName(testHelper.postingDocServerName);
//            createVendorRequest.setHypercubeServerName(testHelper.postingHypercubeServerName);
            response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            externalClientVendor = (Vendor) response.responseData;

            SearchSettings searchSettings = new SearchSettings();
            searchSettings.getPosting().clear();
            SearchSettings.Posting posting = new SearchSettings.Posting();
            posting.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            posting.setVendorId(BigInteger.valueOf(adminVendor.getVendorId()));
            searchSettings.getPosting().add(posting);

            String searchSettingsJson = helper.getJsonStringFromObject(searchSettings);

            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
           settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            consumer.setLensSettings(settingsList);

            VendorSettings settings = new VendorSettings();
            settings.getVendorIdList().add(BigInteger.valueOf(adminVendor.getVendorId()));
            consumer.setVendorSettings(settings);
            consumer.setSearchSettings(searchSettings);

            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
//            consumer.getVendorSettings().getVendorIdList().clear();
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("job");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);

            adminClient = dbService
                    .getClientById(client.getId());
            adminClient.setTier(1);

            CoreClient updatedAdminClient = adminClient;

            ApiResponse updateClientResponse = dbService.updateClient(adminClient, updatedAdminClient);
            Assert.assertTrue("Update client check", updateClientResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            extension = fileName.substring(fileName.indexOf('.') + 1);
            docType = 'P';
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
            // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);

            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
            
            ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if (clientList != null && clientList.size() > 0) {
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister job request", deleteExternalClientResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidFacetCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithMultipleSelectedFiltersWithAndContionReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            selectedFilters.getFilter().add("Hand Tools");
            selectedFilters.setOperator("And");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/skills=[Hand Tools]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithEmptyFacetCriteria() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            searchCriteria.setFacets(facetCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = true;
            }
            Assert.assertTrue("Search response status check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithInvalidFacetDimensionNameReturnsFailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("sample");
            selectedFilters.getFilter().add("Wiring");
            selectedFilters.getFilter().add("Accounts Payable and Receivable");
            selectedFilters.setOperator("And");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = true;
            }
            Assert.assertTrue("Search response status check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithEmptySelectedDimensionReturnsFailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = true;
            }
            Assert.assertTrue("Search response status check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithEmptySelectedFilterListReturnsFailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();

            } catch (Exception ex) {
                response.status = true;
            }
            Assert.assertTrue("Search response status check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidFacetCriteriaAndKeywordCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Keyword keywordCriteria = new Keyword();
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("or");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("CommTech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[((\"Comcast\")) or ((\"CommTech\")) ]]></keyword><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
/*    @Test
    public final void searchJobWithValidFacetCriteriaAndKeywordCriteriaCKSReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CKS.toString());
            Keywords keywords = new Keywords();
            keywords.setValue("Comcast");
            keywords.setLogicalOperator("or");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            keywords = new Keywords();
            keywords.setValue("CommTech");
            keywords.setLogicalOperator("And");
            keywords.setContext(Enum.keywordContext.FullResume.toString());
            keywordCriteria.getKeywords().add(keywords);
            searchCriteria.setKeyword(keywordCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);
            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[CKS((\"Comcast\") or (\"CommTech\") )]]></keyword><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }	*/

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidFacetCriteriaAndKeywordCriteriaCstomBooleanSearchReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, null);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Keyword keywordCriteria = new Keyword();
            keywordCriteria.setSearchType(Enum.keywordSearchTypes.CustomBooleanSearch.toString());
            keywordCriteria.setCustomBooleanSearchKeyword("(Comcast or CommTech)");
            searchCriteria.setKeyword(keywordCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><keyword><![CDATA[(Comcast or CommTech)]]></keyword><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidFacetCriteriaAndCustomFiltersReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            UpdateLensSettingsRequest updateRequest = testHelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setHost(testhelper.hostname);
            com.bgt.lens.model.adminservice.request.Filter filter = new com.bgt.lens.model.adminservice.request.Filter();
            filter.setKey("bgldlanguage");
            filter.setName("bgldlanguage");
            com.bgt.lens.model.adminservice.request.CustomFilters customFilters = new com.bgt.lens.model.adminservice.request.CustomFilters();
            customFilters.getPostingFilters().add(filter);
            updateRequest.getFilterSettings().setCustomFilters(customFilters);
            updateRequest.setPort(BigInteger.valueOf(enusLensSettings.getPort()));

            filter = new Filter();
            filter.setKey(testHelper.hypercubeDimensionName01);
            filter.setName("Jobtitle");
            Filter filter001 = new Filter();
            filter001.setKey(testHelper.hypercubeDimensionName02);
            filter001.setName("Skills");
            Filter filter02 = new Filter();
            filter02.setKey(testHelper.hypercubeDimensionName03);
            filter02.setName("Employer");
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter001);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter02);
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(true);
            ApiResponse response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
            Assert.assertEquals("Update LENS settings with Cutom Filters check", true, response.status);

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, true, 2);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, true, 3);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            CustomFilters filters = new CustomFilters();
            com.bgt.lens.model.searchservice.request.Filter filter01 = new com.bgt.lens.model.searchservice.request.Filter();
            filter01.setName("bgldlanguage");
            filter01.setMin((long) 2);
            filters.getFilter().add(filter01);
            searchCriteria.setCustomFilters(filters);

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><facets><![CDATA[facets://skills=[Wiring]/]]></facets><bgldlanguage min=\"2\"/><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include var=\"special_bgldlanguage\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidFacetCriteriaAndJobDescriptionReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(binaryData);
            searchCriteria.setJob(jobDocumentCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertTrue("Search command check", searchCommandDumpList.get(0).getSearchCommand().contains("</posting>"));
                Assert.assertTrue("Search command check", searchCommandDumpList.get(0).getSearchCommand().contains("<facets>"));
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidFacetCriteriaAndJobDocumentCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Job jobDocumentCriteria = new Job();
            jobDocumentCriteria.setBinaryData(binaryData);
            jobDocumentCriteria.setExtension(extension);
            searchCriteria.setJob(jobDocumentCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertTrue("Search command check", searchCommandDumpList.get(0).getSearchCommand().contains("</posting>"));
                Assert.assertTrue("Search command check", searchCommandDumpList.get(0).getSearchCommand().contains("<facets>"));
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidFacetCriteriaAndJobLikeThisCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            Job jobsLikeThisCriteria = new Job();
            jobsLikeThisCriteria.setJobId("1");
            searchCriteria.setJob(jobsLikeThisCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><doc id=\"1\" type=\"posting\" vendor=\"posvendor01\"/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidResultFacetCriteriaWithoutSelectedDimensionReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri><![CDATA[facets://skills=[Wiring]/*]]></uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 1);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(0).getFilterCount() == 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidResultFacetCriteriaWithSelectedDimensionAloneReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedFilters().remove(selectedFilters);
            facetCriteria.getSelectedDimensions().add("skills");

            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri>facets://skills</uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 1);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(0).getFilterCount() > 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidMultipleResultFacetCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("Skills");
            selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("Skills");
            selectedFilters.getFilter().add("Mathematics");
            selectedFilters.setOperator("Or");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri><![CDATA[facets://skills=[Wiring]/*]]></uri><uri><![CDATA[facets://skills=[Mathematics]/skills]]></uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 2);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(1).getFilterCount() == 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithEmptyResultFacetCriteriaReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);
            
            facetCriteria = new Facets();
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() == null);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithInvalidDimensionNameReturnsFailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("sample");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();

            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("Faceted search with result facet distribution check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            searchCriteria.setAdditionalFilters(additionalFilters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());
                Assert.assertEquals("SearchResult score Check", 5, (long) searchResult.getJobs().get(0).getScore());

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithoutScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());
                Assert.assertTrue("SearchResult score Check", (long) searchResult.getJobs().get(0).getScore() > 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithInvalidScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode("Sample");
            searchCriteria.setAdditionalFilters(additionalFilters);
            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("Faceted search response with invalid scoring mode check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidShowRecordsFilterandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setShowRecords("false");
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" show-records=\"false\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri>facets://skills</uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 0, (long) searchResult.getResultsCount());
                Assert.assertTrue("SearchResult score Check", searchResult.getJobs() == null);

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 1);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(0).getFilterCount() > 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidShowRecordsMinandMaxFilterandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMinIndex((long) 1);
            paginateResults.setMaxIndex((long) 1);
            additionalFilters.setPaginateResults(paginateResults);

            searchCriteria.setAdditionalFilters(additionalFilters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" scoring-mode=\"hard-filters-only\" show-records=\"1,1\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include count=\"20\" var=\"facets\"><uri>facets://skills</uri></include></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 1, (long) searchResult.getResultsCount());

                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution() != null);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().size() >= 1);
                Assert.assertTrue("Facet distribution response", searchResult.getFacetDistribution().get(0).getFilterCount() > 0);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithInvalidMinandMaxFilterandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMinIndex((long) 1);
            paginateResults.setMaxIndex((long) -1);
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("faceted search show records check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithWithoutMaxFilterIndexandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMinIndex((long) 1);
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("faceted search show records check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithWithoutMinFilterIndexandScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMaxIndex((long) 1);
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("faceted search show records check", response.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithWithoutMinFilterIndexandInvalidScoringModeReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setMinIndex((long) 1);
            paginateResults.setMaxIndex((long) 1);
            additionalFilters.setPaginateResults(paginateResults);
            searchCriteria.setAdditionalFilters(additionalFilters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithInvalidShowRecordsFilterandScoringModeReturnsfailure() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            facetCriteria = new Facets();
            facetCriteria.getSelectedDimensions().add("skills");
            ResultFacets resultFacets = new ResultFacets();
            resultFacets.getFacets().add(facetCriteria);
            searchCriteria.setResultFacets(resultFacets);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.setScoringMode(Enum.scoringMode.hardfiltersonly.name());
            PaginateResults paginateResults = new PaginateResults();
            paginateResults.setShowRecords("test");
            additionalFilters.setPaginateResults(paginateResults);

            searchCriteria.setAdditionalFilters(additionalFilters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            ApiResponse response = new ApiResponse();
            try {
                // Register Assert
                MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(jobSearchRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                response.status = true;
            }

            Assert.assertTrue("faceted search show records check", response.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Search job with facets
     */
    @Test
    public final void searchJobWithValidCustomXpathReturnsSuccess() {
        try {
            apiContext.setClient(adminClient);
            apiContext.setClientApi(adminClient.getCoreClientapis().iterator().next());
            helper.setApiContext(apiContext);
            testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());
            testHelper.unregisterJob(searchService, "2", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            // Canon
            ApiResponse registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = RegisterJobTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }

            registerJobResponse = testHelper.registerJob(searchService, apiContext, helper, testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", adminVendor.getVendor(), coreClient, false, 0);
            Assert.assertTrue("Register job response check", registerJobResponse.status);
            Thread.sleep(1000);
            JobSearchRequest jobSearchRequest = new JobSearchRequest();
            SearchCriteria searchCriteria = new SearchCriteria();

            searchCriteria.setVendor(adminVendor.getVendor());
            searchCriteria.setInstanceType(testHelper.instanceType);
            searchCriteria.setLocale(TestHelper.Locale.en_us.toString());

            Facets facetCriteria = new Facets();
            SelectedFilters selectedFilters = new SelectedFilters();
            selectedFilters.setDimensionName("skills");
            selectedFilters.getFilter().add("Wiring");
            facetCriteria.getSelectedFilters().add(selectedFilters);
            searchCriteria.setFacets(facetCriteria);

            AdditionalFilters additionalFilters = new AdditionalFilters();
            additionalFilters.getCustomXpath().add("skill");
            searchCriteria.setAdditionalFilters(additionalFilters);

            jobSearchRequest.setSearchCriteria(searchCriteria);

            // Get currentTransactionCount
            Integer currentTransactionCount = testHelper.getCurrentTransactionCount(adminClient);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.SearchJob)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(jobSearchRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            JobSearchResponse jobSearchResponse = (JobSearchResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new JobSearchResponse());

            ObjectMapper mapper;
            byte[] json;

            if (jobSearchResponse != null && jobSearchResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(jobSearchResponse.responseData);
                SearchResult searchResult = mapper.readValue(json, SearchResult.class
                );

                SearchCommandDumpCriteria dumpCriteria = new SearchCommandDumpCriteria();
                dumpCriteria.setClientId(adminClient.getId());
                List<SearchCommanddump> searchCommandDumpList = dbService.getSearchCommandDumpByCriteria(dumpCriteria);

                Assert.assertNotNull("Search command dump list count check", searchCommandDumpList);
                Assert.assertEquals("Search command check", "<?xml version='1.0' encoding='utf-8'?><bgtcmd><search count=\"100\" min=\"0\" type=\"posting\" vendor=\"posvendor01\"><posting/><facets><![CDATA[facets://skills=[Wiring]/]]></facets><include var=\"id\"/><include var=\"lens\"/><include var=\"keyword\"/><include var=\"title\"/><include var=\"employer\"/><include var=\"xpath://DataElementsRollup/CanonJobTitle\"/><include var=\"xpath://DataElementsRollup/CanonEmployer\"/><include var=\"xpath://skillrollup/canonskill\"/><include var=\"matchexplanation\"/><include var=\"xpath://skill\"/></search></bgtcmd>", searchCommandDumpList.get(0).getSearchCommand());
                Assert.assertEquals("SearchResult Count Check", 2, (long) searchResult.getResultsCount());
                Assert.assertTrue("SearchResult Count Check", searchResult.getJobs().get(0).getCustomXpathData().get(0).getXpathData() != null);

                // Get remainingTransactionCount
                Integer remainingTransactionCount = testHelper.getRemainingTransactionCount(adminClient, dbService);

                Assert.assertEquals("Search remaining transaction count", currentTransactionCount - 3, (long) remainingTransactionCount);

                ApiResponse unregisterResponse = testHelper.unregisterJob(searchService, "1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());;
                Assert.assertTrue("Unregister job request", unregisterResponse.status);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
