// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.UtilityServices;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.parserservice.request.ParseJobRequest;
import com.bgt.lens.model.parserservice.request.ParseJobRequest.FieldList;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.lang3.StringUtils;
import org.json.XML;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Parse job without authentication test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ParseJobWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();


    @Autowired
    ICoreService coreService;

    @Autowired
    IUtilityService utilityService;
    /**
     * API context
     */
    public ApiContext apiContext;

    /**
     * Lens settings
     */
    public LensSettings en_usLensSettings = null, en_ukLensSettings = null, localeSettings = null;

    /**
     * Test helper object
     */
    public TestHelper testHelper = null;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private String binaryData;

    /**
     * Field list
     */
    public FieldList fieldList;

    /**
     * Instance type
     */
    public static final String InstanceType = "SPECTRUM";

    /**
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
           ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
           servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        try {
            String propFileName = "/JobTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettingsForJob.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ParseJobWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;
            
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setMethod("Post");
            apiContext.setResource("Job");
            apiContext.setClient(null);
            apiContext.setAdvanceLog(new AdvanceLog());
            apiContext.setInstanceType(InstanceType);
            _helper = new Helper();
            _helper.setApiContext(apiContext);

            // Job Data
            String fileName = "/TestJobs/10002.txt";
            InputStream resourceAsStream04 = ParseJobWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);

            // Fields List
            fieldList = new ParseJobRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("All");
            fieldList.getField().addAll(fields);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseJobWithoutAuthenticationWithValidBinaryJobData_GetContactJobFields
     */
    // Contact
    @Test
    public void parseJobWithoutAuthenticationWithValidBinaryJobData_GetContactJobFields() {
        try {
            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Contact");
            fieldList.getField().addAll(fields);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJob WithoutAuthentication With ValidBinary GetContactJobFields check", response.status, true);

            // ParseJob Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("JobDoc");
            JSONObject posting = (JSONObject) jsonRoot.get("posting");
            Object contact = posting.get("contact");

            Assert.assertNotNull(contact);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobValidBinaryJobDataWithLocaleBasedParsing() {
        try {
            // ParseResumeBGTXML
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJob WithoutAuthentication With ValidBinary JobData check", response.status, true);

            // ParseResumeBGTXML Assert
            String expResult = "JobDoc";
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
    
    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseJobValidBinaryJobDataLocaleBasedParsingWithInvalidLocale() {
        try {
            // ParseResumeBGTXML
            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = ParseJobWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJob WithoutAuthentication With ValidBinary JobData check", response.status, false);

            // ParseResumeBGTXML Assert
            String expResult = "missing";
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(expResult);
            assertTrue(checkContains);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobWithValidBinaryJobData_GetBackgroundFields
     */
    // Education
    @Test
    public void parseJobWithoutAuthenticationWithValidBinaryJobData_GetBackgroundFields() {
        try {

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = ParseJobWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseJobBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("background");
            fieldList.getField().addAll(fields);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJob With ValidBinary GetBackgroundFields check", response.status, true);

            // ParseJob Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("JobDoc");
            JSONObject posting = (JSONObject) jsonRoot.get("posting");
            Object background = posting.get("background");

            Assert.assertNotNull(background);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobWithValidBinaryJobData_GetDutiesFields
     */
    // Summary
    @Test
    public void parseJobWithoutAuthenticationWithValidBinaryJobData_GetDutiesFields() {
        try {

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = ParseJobWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseJobBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Duties");
            fieldList.getField().addAll(fields);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJob With ValidBinary GetDutiesFields check", response.status, true);

            // ParseJob Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("JobDoc");
            JSONObject posting = (JSONObject) jsonRoot.get("posting");
            Object duties = posting.get("duties");

            Assert.assertNotNull(duties);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseJobWithValidBinaryJobData_GetSkillRollUpFields
     */
    // SkillRollUp
    @Test
    public void parseJobWithoutAuthenticationWithValidBinaryJobData_GetSkillRollUpFields() {
        try {

            String fileName = "/TestJobs/1001257.txt";
            InputStream resourceAsStream04 = ParseJobWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseJobBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);

            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("SkillRollUp");
            fieldList.getField().addAll(fields);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJob With ValidBinary GetSkillRollUpFields check", response.status, true);

            // ParseJob Assert
            org.json.JSONObject xmlJsonObject = XML.toJSONObject((String) response.responseData);
            JSONObject json = (JSONObject) new JSONParser().parse(xmlJsonObject.toString());
            JSONObject jsonRoot = (JSONObject) json.get("JobDoc");
            JSONObject posting = (JSONObject) jsonRoot.get("posting");
            Object skillrollup = jsonRoot.get("skillrollup");

            Assert.assertNotNull(skillrollup);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseJobWithoutAuthenticationWithValidBinaryJobData_WithInvalidJobFields
     */
    @Test
    public void parseJobWithoutAuthenticationWithValidBinaryJobData_WithInvalidJobFields() {
        try {
            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            // Fields List
            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Invalid");
            fieldList.getField().addAll(fields);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseJob(parseJobRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseJob WithoutAuthentication With ValidBinary WithInvalidJobFields check", response.status, false);

            // ParseJob Assert
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(_helper.getErrorMessageWithURL(ApiErrors.INVALID_JOB_FIELDLIST));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseJobWithoutAuthenticationWithValidBinaryJobData_WithEmptyJobFields
     */
    @Test
    public void parseJobWithoutAuthenticationWithValidBinaryJobData_WithEmptyJobFields() {
        try {
            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            // Fields List
            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("");
            fieldList.getField().addAll(fields);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseJob(parseJobRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseJob WithoutAuthentication With ValidBinary WithEmptyJobFields check", response.status, false);

            // ParseJob Assert
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(_helper.getErrorMessageWithURL(ApiErrors.INVALID_JOB_FIELDLIST));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseJobWithoutAuthenticationWithValidBinaryJobData_WithNullJobFields
     */
    @Test
    public void parseJobWithoutAuthenticationWithValidBinaryJobData_WithNullJobFields() {
        try {
            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            // Fields List
            fieldList = new FieldList();
            List<String> fields = new ArrayList<>();
            fields.add(null);
            fieldList.getField().addAll(fields);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseJob(parseJobRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseJob WithoutAuthentication With ValidBinary WithNullJobFields check", response.status, false);

            // ParseJob Assert
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(_helper.getErrorMessageWithURL(ApiErrors.INVALID_JOB_FIELDLIST));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseJobWithoutAuthenticationWithConvertError
     */
    @Test
    public void parseJobWithoutAuthenticationWithConvertError() {
        try {
            String fileName = "/TestJobs/EmptyJob/ExternalJob.doc";
            InputStream resourceAsStream04 = ParseJobWithoutAuthenticationTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ConvertTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJob WithoutAuthentication With Empty BinaryData check", response.status, false);

            // ParseJob Assert            
            String convertJob = (String) response.responseData;
            String expResult = "Converters returned empty output";
            Assert.assertEquals("ParseJob WithoutAuthentication With convert error Check", apiErrors.getLensErrorMessage(expResult), convertJob);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseJobWithoutAuthenticationWithConvertErrorStatus
     */
    @Test
    public void parseJobWithoutAuthenticationWithConvertErrorStatus() {
        try {
            String fileName = "/TestJobs/EmptyJob/ExternalJob.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setLocale(TestHelper.Locale.en_us.name());
            parseJobRequest.setExtension(extension);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                response = utilityService.parseJob(parseJobRequest);
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseJob WithoutAuthentication With Null BinaryData check", response.status, false);

            // ParseJob Assert
            String convertJob = (String) response.responseData;
            boolean checkContains = convertJob.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseJobWithoutAuthenticationUnauthorizedInstance
     */
    @Test
    public void parseJobWithoutAuthenticationUnauthorizedInstance() {
        try {
            // ParseJob
            String localeType = "en_anz";
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale(localeType);
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJobBGTXML Without Authentication Unauthorized Instance check", response.status, false);

            // ParseJob Assert
            String instanceResponse = (String) response.responseData;
            boolean invalidInstance = StringUtils.containsIgnoreCase(instanceResponse, invalidType);
            assertTrue(invalidInstance);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseJobWithoutAuthenticationInvalidHostNameList
     */
    @Test
    public void parseJobWithoutAuthenticationInvalidHostNameList() {
        try {
            // create lens settings
            JAXBContext jaxbContext = JAXBContext.newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            InputStream resourceAsStream01 = ParseJobWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale("en_sgp");
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJobBGTXML Without Authentication Invalid HostName check", response.status, false);

            // ParseJob en_us Assert
            String instanceResponse = (String) response.responseData;            
            String lensErrorMessage = _helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE);
            assertTrue(instanceResponse.contains(_helper.getErrorMessageWithURL(lensErrorMessage)));

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * ParseJobWithoutAuthenticationInActiveLensList
     */
    @Test
    public void parseJobWithoutAuthenticationInActiveLensList() {
        try {
            // create lens settings
            JAXBContext jaxbContext = JAXBContext.newInstance(AddLensSettingsRequest.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            InputStream resourceAsStream01 = ParseJobWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml");
            try {
                if (resourceAsStream01 != null) {
                    settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(resourceAsStream01);
                    resourceAsStream01.close();
                }
            } catch (IOException | JAXBException ex) {
                resourceAsStream01.close();
                throw ex;
            }
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(InfoWithoutAuthenticationTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale("en_sgp");
            settingsRequest.setStatus(false);
            //en_us
            ApiResponse en_sgpLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_sgpLensSettingsResponse.status, true);
            LensSettings en_sgpLensSettings = (LensSettings) en_sgpLensSettingsResponse.responseData;

            // ParseJob
            ParseJobRequest parseJobRequest = new ParseJobRequest();
            parseJobRequest.setInstanceType(InstanceType);
            parseJobRequest.setBinaryData(binaryData);
            parseJobRequest.setExtension(extension);
            parseJobRequest.setLocale("en_sgp");
            parseJobRequest.setFieldList(fieldList);
            ApiResponse response = utilityService.parseJob(parseJobRequest);
            Assert.assertEquals("ParseJobBGTXML Without Authentication InActive Lens check", response.status, false);

            // ParseJob inActive Instance Assert
            String instanceResponse = (String) response.responseData;
            Assert.assertEquals("ParseJobBGTXML WithoutAuthentication InActive Lens check", _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS), instanceResponse);

            // Delete LensSetting
            ApiResponse en_sgpDeleteLensSettingsResponse = coreService.deleteLensSettings(en_sgpLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_sgpDeleteLensSettingsResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

}
