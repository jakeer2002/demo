// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LicenseManagementService;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.License;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.license.ILicenseManagementService;
import com.bgt.lens.test.Helper.TestHelper;
import java.util.List;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * *
 * License management test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LicenseManagementTest {
    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

     @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();


    @Autowired
    ICoreService coreService;

    @Autowired
    ILicenseManagementService licenseService;

    /**
     * *
     * API response
     */
    public ApiResponse apiResponse;

    /**
     * *
     * Lens settings response
     */
    public ApiResponse lensSettingsResponse;

    /**
     * *
     * Resource response
     */
    public ApiResponse resourceResponse;

    /**
     * *
     * API
     */
    public Api api;

    /**
     * *
     * Lens settings
     */
    public LensSettings settings;

    /**
     * *
     * Resource
     */
    public Resources resource;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     */
    @Before
    public void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            CreateApiRequest createApi = _testhelper.getCreateApiRequest("/input/CreateApi.xml");
            apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);

            api = (Api) apiResponse.responseData;

            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ClientTest.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);

            settings = (LensSettings) lensSettingsResponse.responseData;

            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            //AddResourceRequest resourceRequest = (AddResourceRequest) jaxbUnmarshaller.unmarshal(ClientTest.class.getResourceAsStream("/input/CreateResource.xml"));
            resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);

            resource = (Resources) resourceResponse.responseData;

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            api = (Api) apiResponse.responseData;

            apiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Create Api delete", apiResponse.status, true);

            settings = (LensSettings) lensSettingsResponse.responseData;
            lensSettingsResponse = coreService.deleteLensSettings(settings.getId());

            Assert.assertEquals("Create Lens settings delete", lensSettingsResponse.status, true);

        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * checkLicenseExpiryWithValidClientId
     */
    @Test
    public void checkLicenseExpiryWithValidClientId() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            int ApiId = client.getClientApi().get(0).getApiId();

            boolean status = licenseService.checkLicenseExpiryDate(client.getId(), ApiId);
            Assert.assertTrue("Check license status", status);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * checkLicenseExpiryWithInvalidClientId
     */
    @Test
    public void checkLicenseExpiryWithInvalidClientId() {
        try {
            boolean status = licenseService.checkLicenseExpiryDate(0, api.getId());
            Assert.assertFalse("Check license status", status);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * checkLicenseActivationWithValidClientId
     */
    @Test
    public void checkLicenseActivationWithValidClientId() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            int ApiId = client.getClientApi().get(0).getApiId();

            boolean status = licenseService.checkLicenseActivationDate(client.getId(), ApiId);
            Assert.assertTrue("Check license status", status);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * checkLicenseActivationWithInvalidClientId
     */
    @Test
    public void checkLicenseActivationWithInvalidClientId() {
        try {
            boolean status = licenseService.checkLicenseActivationDate(0, api.getId());
            Assert.assertFalse("Check license status", status);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * checkLicenseTransactionCountExpiryWithValidClientId
     */
    @Test
    public void checkLicenseTransactionCountExpiryWithValidClientId() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            int ApiId = client.getClientApi().get(0).getApiId();

            boolean status = licenseService.checkRemainingTransactionCount(client.getId(), ApiId);
            Assert.assertTrue("Check license status", status);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * checkLicenseTransactionCountExpiryWithInvalidClientId
     */
    @Test
    public void checkLicenseTransactionCountExpiryWithInvalidClientId() {
        try {
            boolean status = licenseService.checkRemainingTransactionCount(0, api.getId());
            Assert.assertFalse("Check license status", status);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * updateLicenseTransactionCountWithValidClientId
     */
    @Test
    public void updateLicenseTransactionCountWithValidClientId() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            int ApiId = client.getClientApi().get(0).getApiId();

            response = licenseService.updateRemainingTransactionCount(client.getId(), ApiId);
            Assert.assertTrue("Check license status", response.status);

            License updatedClient = (License) response.responseData;
            int remaining = 0;
            for (com.bgt.lens.model.rest.response.ClientApi api : updatedClient.getClientApi()) {
                if (api.getApiId().equals(ApiId)) {
                    remaining = api.getRemainingTransactions();
                }
            }

            Assert.assertEquals(client.getClientApi().get(0).getRemainingTransactions() - 1, remaining);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * updateLicenseTransactionCountWithInvalidClientId
     */
    @Test
    public void updateLicenseTransactionCountWithInvalidClientId() {
        try {
            ApiResponse response = licenseService.updateRemainingTransactionCount(0, api.getId());
            Assert.assertFalse("Check license status", response.status);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * getLicenseDetailsWithValidClientId
     */
    @Test
    public void getLicenseDetailsWithValidClientId() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            //int ApiId = client.getClientApi().get(0).getApiId();
            response = licenseService.getLicenseDetailsById(client.getId());
            Assert.assertTrue("Check license status", response.status);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * getLicenseDetailsWithInvalidClientId
     */
    @Test
    public void getLicenseDetailsWithInvalidClientId() {
        try {
            ApiResponse response = licenseService.getLicenseDetailsById(0);
            Assert.assertFalse("Check license status", response.status);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * *
     * getLicenseDetailsofAllClients
     */
    @Test
    public void getLicenseDetailsofAllClients() {
        try {
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, settings.getId());
            consumer = _testhelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            Client client = (Client) response.responseData;

            //int ApiId = client.ClientApi.get(0).ApiId;
            response = licenseService.getLicenseDetailsAll();
            Assert.assertTrue("Check license status", response.status);

            List<License> licenseList = (List<License>) response.responseData;
            Assert.assertTrue(licenseList.size() > 0);

            response = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Create consumer delete", response.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }
}
