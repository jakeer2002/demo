// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.datamanagementservice;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreAuthenticationlog;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.time.Instant;
import java.util.Date;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Authentication log test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CoreAuthenticationlogDataTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private static final Helper _helper = new Helper();

    @Autowired
    IDataRepositoryService dbService;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * addAuthenticationlogWithValidData
     */
    @Test
    public void addAuthenticationlogWithValidData() {
        try {
            // Add
            CoreAuthenticationlog coreAuthenticationlog = new CoreAuthenticationlog();
            coreAuthenticationlog.setRequestId("C23A4938-4D01-4A66-B170-24E426DC9C4D");
            coreAuthenticationlog.setClientId(10138);
            coreAuthenticationlog.setApiId(25491);
            coreAuthenticationlog.setSignature("Lxj7KFtnZkhIhM7CWMaZFvAwoXM=");
            coreAuthenticationlog.setTimestamp(1413894514);
            coreAuthenticationlog.setNonce("JGNhxCdPNdNuc8T");
            Date now = Date.from(Instant.now());
            coreAuthenticationlog.setCreatedOn(now);
            ApiResponse addApiResponse = dbService.createAuthenticationlog(coreAuthenticationlog);

            // Add Assert
            Assert.assertEquals("Add Authentication Log status check", addApiResponse.status, true);

            // Get
            coreAuthenticationlog = dbService.getAuthenticationlogById(coreAuthenticationlog.getId());
            // Get Assert
            Assert.assertEquals("Get Authentication Log status check", addApiResponse.status, true);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteAuthenticationlog(coreAuthenticationlog);
            // Delete Assert
            Assert.assertEquals("Delete Authentication Log status check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

}
