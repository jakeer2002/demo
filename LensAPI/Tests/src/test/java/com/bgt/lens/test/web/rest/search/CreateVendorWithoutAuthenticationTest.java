// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.criteria.SearchVendorSettingsCriteria;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.soap.response.searchservice.CreateVendorResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletContext;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Create Vendor without authentication
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class CreateVendorWithoutAuthenticationTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor vendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");

            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;
//            // Add ApiConext
            helper = new Helper();
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("resume");
//            apiContext.setClient(coreClient);
//            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void createVendorWithValidVendorName() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.CreateVendor)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(createVendorRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            CreateVendorResponse createVendorResponse = (CreateVendorResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new CreateVendorResponse());

            ObjectMapper mapper;
            byte[] json;

            if (createVendorResponse != null && createVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(createVendorResponse.responseData);
                Vendor vendor = mapper.readValue(json, Vendor.class
                );

                Assert.assertEquals("Create vendor max document count check", 500000, (long) vendor.getMaxDocumentCount());
                Assert.assertEquals("Create vendor registered document count", 0, (long) vendor.getRegisteredDocumentCount());
                Assert.assertEquals("Create vendor status", com.bgt.lens.helpers.Enum.vendorStatus.Open.toString(), vendor.getStatus());
                Assert.assertEquals("Create vendor type", com.bgt.lens.helpers.Enum.docType.resume.toString(), vendor.getType());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 1, searchVendorSettingsList.size());

                SearchVendorsettings vendorSettings = dbService.getVendorById(vendor.getVendorId());

                if (vendorSettings != null) {
                    ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                    Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                }
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void createVendorWithInvalidLENSSettingsId() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(0));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.CreateVendor)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(createVendorRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
            searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
            Assert.assertNull("Create vendor DB entry check", searchVendorSettingsList);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void createVendorWithInvalidType() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            createVendorRequest.setType("test");

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.CreateVendor)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(createVendorRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
            searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
            Assert.assertNull("Create vendor DB entry check", searchVendorSettingsList);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void createVendorWithValidMaximumDocumentCount() {
        try {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            createVendorRequest.setMaxDocumentCount((long) 500);

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.CreateVendor)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(createVendorRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            CreateVendorResponse createVendorResponse = (CreateVendorResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new CreateVendorResponse());

            ObjectMapper mapper;
            byte[] json;

            if (createVendorResponse != null && createVendorResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(createVendorResponse.responseData);
                Vendor vendor = mapper.readValue(json, Vendor.class
                );

                Assert.assertEquals("Create vendor max document count check", 500, (long) vendor.getMaxDocumentCount());
                Assert.assertEquals("Create vendor registered document count", 0, (long) vendor.getRegisteredDocumentCount());
                Assert.assertEquals("Create vendor status", com.bgt.lens.helpers.Enum.vendorStatus.Open.toString(), vendor.getStatus());
                Assert.assertEquals("Create vendor type", com.bgt.lens.helpers.Enum.docType.resume.toString(), vendor.getType());

                List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
                searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
                Assert.assertEquals("Create vendor DB entry check", 1, searchVendorSettingsList.size());

                SearchVendorsettings vendorSettings = dbService.getVendorById(vendor.getVendorId());

                if (vendorSettings != null) {
                    ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                    Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
                }
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void createVendorWithMoreThanLimit() {
        try {
            apiConfig.setDefaultVendorCount(1);
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Vendor vendor = (Vendor) response.responseData;
            Assert.assertTrue("Create vendor check", response.status);

            // Register Assert
            createVendorRequest.setVendor("resvendor0101");
            MvcResult result = mockMvc.perform(post(UriConstants.CreateVendor)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(createVendorRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
            searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
            Assert.assertEquals("Create vendor DB entry check", 1, searchVendorSettingsList.size());

            SearchVendorsettings vendorSettings = dbService.getVendorById(vendor.getVendorId());

            if (vendorSettings != null) {
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

            searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
            Assert.assertNull("Create vendor DB entry check", searchVendorSettingsList);
            apiConfig.setDefaultVendorCount(100);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void createVendorWithNotRunninglENSServer() {
        try {
            UpdateLensSettingsRequest lensSettings = new UpdateLensSettingsRequest();
            lensSettings.setInstanceType("Spectrum");
            lensSettings.setHost("EmptyHost");
            lensSettings.setStatus(true);
            ApiResponse updateLensSettingsResponse = coreService.updateLensSettings(enusLensSettings.getId(), lensSettings);
            Assert.assertTrue("Update LENS settings", updateLensSettingsResponse.status);
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);

            // Register Assert
            createVendorRequest.setVendor("resvendor01");
            MvcResult result = mockMvc.perform(post(UriConstants.CreateVendor)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(createVendorRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
            searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
            Assert.assertNull("Create vendor DB entry check", searchVendorSettingsList);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor duplicate
     */
    @Test
    public final void createAlreadyExistingVendor() {
        try {
            // Canon
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setHypercubeServerName("");
//            createVendorRequest.setDocServerName(testHelper.docServerName);

            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create Vendor Check", response.status);
            Vendor vendor = (Vendor) response.responseData;

            // Register Assert
            MvcResult result = mockMvc.perform(post(UriConstants.CreateVendor)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(createVendorRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            List<SearchVendorsettings> searchVendorSettingsList = new ArrayList<>();
            searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
            Assert.assertEquals("Create vendor DB entry check", 1, searchVendorSettingsList.size());

            SearchVendorsettings vendorSettings = dbService.getVendorById(vendor.getVendorId());

            if (vendorSettings != null) {
                ApiResponse deleteVendorResponse = dbService.deleteVendor(vendorSettings);
                Assert.assertTrue("Unregister resume request", deleteVendorResponse.status);
            }

            searchVendorSettingsList = dbService.getVendorsByCriteria(new SearchVendorSettingsCriteria());
            Assert.assertNull("Create vendor DB entry check", searchVendorSettingsList);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
