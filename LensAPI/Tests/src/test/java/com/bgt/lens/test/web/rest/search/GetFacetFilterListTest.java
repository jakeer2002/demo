// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.Filter;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.FacetFiltersList;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.rest.response.searchservice.GetFacetFilterListResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Get facet filter list
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class GetFacetFilterListTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings;
    /**
     * vendor
     */
    private Vendor adminVendor;
    /**
     * External client vendor
     */
    private Vendor externalClientVendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Admin client
     */
    private CoreClient adminClient;
    /**
     * ExternalClient
     */
    private CoreClient externalClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;

            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");

            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            instanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            consumer.setLensSettings(settingsList);

            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
            consumer.setVendorSettings(null);
            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            
            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);
            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
             // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);

            ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if (clientList != null && clientList.size() > 0) {
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister resume request", deleteExternalClientResponse.status);
            }
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);

           

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithoutCriteriaAsAdminReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidDocTypeAsAdminReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?docType=resume")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", null, facetFilterList.getFacetFiltersList().get(0).getPostingFilters());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidDocTypeAsAdminReturnsFailure() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?docType=test")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidInstanceTypeAsAdminReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?instanceType=SPECTRUM")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidInstanceTypeAsAdminReturnsFailure() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?instanceType=xray")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidLocaleAsAdminReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?locale=en_us")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidLocaleAsAdminReturnsFailure() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setHost(testHelper.hostname);
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?locale=en_uk")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidClientIdAsAdminReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?clientId=" + client.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidClientIdAsAdminReturnsFailure() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?clientId=0")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidLensSettingsIdAsAdminReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?lensSettingsId=" + enusLensSettings.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidLensSettingsIdAsAdminReturnsFailure() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?lensSettingsId=0")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithoutCriteriaAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidDocTypeAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?docType=resume")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", null, facetFilterList.getFacetFiltersList().get(0).getPostingFilters());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidInstanceTypeAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?instanceType=SPECTRUM")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidDocTypeAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?docType=test")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidInstanceTypeAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?instanceType=test")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidLocaleAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?locale=en_us")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidLocaleAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?locale=en_uk")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidClientIdAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?clientId=" + adminClient.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithValidLensSettingsIdAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?lensSettingsId=" + enusLensSettings.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetFacetFilterListResponse facetFilterResponse = (GetFacetFilterListResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetFacetFilterListResponse());

            ObjectMapper mapper;
            byte[] json;

            if (facetFilterResponse != null && facetFilterResponse.status) {
                mapper = new ObjectMapper();
                json = mapper.writeValueAsBytes(facetFilterResponse.responseData);
                FacetFiltersList facetFilterList = mapper.readValue(json, FacetFiltersList.class
                );

                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getResumeFilters().size());
                Assert.assertEquals("Get facet filter list count check", 1, facetFilterList.getFacetFiltersList().get(0).getPostingFilters().size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidLensSettingsIdAsClientReturnsFailure() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list            
            MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?lensSettingsId=0")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Get facet filter list
     */
    @Test
    public final void GetFacetFilterListWithInvalidClientIdAsClientReturnsSuccess() {
        try {
            Filter filter = new Filter();
            filter.setKey("sample");
            filter.setName("sample");

            Filter filter01 = new Filter();
            filter01.setKey("sample01");
            filter01.setName("sample");
            UpdateLensSettingsRequest updateRequest = _testhelper.getUpdateLensSettingsRequest("/input/UpdateLensSettings.xml");
            updateRequest.setInstanceType("SPECTRUM");
            updateRequest.setStatus(false);
            updateRequest.getFilterSettings().getFacetFilters().getResumeFilters().add(filter);
            updateRequest.getFilterSettings().getFacetFilters().getPostingFilters().add(filter01);

            ApiResponse response = new ApiResponse();

            try {
                response = coreService.updateLensSettings(enusLensSettings.getId(), updateRequest);
                response.status = true;
            } catch (Exception ex) {
                response.status = false;
            }

            Assert.assertEquals("Update Lens Settings", response.status, true);

            CoreClient adminClient = dbService
                    .getClientById(client.getId());

            adminClient.setClientKey("Test01");
            adminClient.setName("test01");
            adminClient.setTier(2);
            adminClient.setCreatedOn(new Date());
            adminClient.setUpdatedOn(new Date());

            ApiResponse createExternalClientResponse = dbService.createClient(adminClient);
            Assert.assertTrue("Create external client check", createExternalClientResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Get facet filter list 
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetFacetFilterList + "?clientId=" + client.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                Assert.assertTrue("Invalid client Id exceoption check", true);
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
