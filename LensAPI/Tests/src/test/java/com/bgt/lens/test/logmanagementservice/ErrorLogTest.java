// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.logmanagementservice;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.GetErrorLogsRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreErrorlog;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.logger.ILoggerService;
import com.bgt.lens.test.Helper.TestHelper;
import java.math.BigInteger;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Error log test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class ErrorLogTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Client
     */
    public CoreClient client = null;

    /**
     * Client API
     */
    public CoreClientapi clientApi = null;

    /**
     * API
     */
    public CoreApi api = null;

    /**
     * API response
     */
    public Api apiResponse = null;
    
    /**
     * LENS settings response
     */
    public LensSettings lensSettingsResponse = null;

    /**
     * API context
     */
    public ApiContext apiContext = null;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    @Autowired
    ICoreService coreService;

    @Autowired
    ILoggerService loggerService;

    @Autowired
    IDataRepositoryService dbService;
    
    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     */
    @Before
    public void setUp() {
        ServletContext servletContext = wac.getServletContext();
        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        try {
            // Create Api
            CreateApiRequest createApi = new CreateApiRequest();
            createApi.setKey("Core");
            createApi.setName("Core");
            ApiResponse response = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", response.status, true);
            apiResponse = (Api) response.responseData;
            
            //Add Lens settings
            AddLensSettingsRequest settingsRequest = _testhelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            response = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", response.status, true);
            lensSettingsResponse = (LensSettings) response.responseData;

            //Add Resource
            AddResourceRequest resourceRequest = _testhelper.getAddResourceRequest("/input/CreateResource.xml");
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            Resources resource = (Resources) resourceResponse.responseData;
            
            CreateConsumerRequest consumer = _testhelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            consumer = _testhelper.updateLensSettings(consumer, lensSettingsResponse.getId());
            consumer = _testhelper.updateApiandResource(consumer, apiResponse.getId(), apiResponse.getKey(), resource.getResourceId().toString());
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = dbService.getClientById(((Client) response.responseData).getId());
            
            CoreApi coreApi = dbService.getApiById(apiResponse.getId());
            ClientapiCriteria criteria = new ClientapiCriteria();
            criteria.setCoreApi(coreApi);
            List<CoreClientapi> clientApiList= dbService.getClientApiByCriteria(criteria);
            
            if(clientApiList != null && clientApiList.size() > 0){
                clientApi = clientApiList.get(0);
            }

            // ApiContext
            apiContext = new ApiContext();
            apiContext.setClient(client);
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClientApi(clientApi);
            apiContext.setStatusCode(500);
            apiContext.setException(new Exception("Test"));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();
            _helper.setApiContext(apiContext);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            
            ApiResponse deleteCconsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer", deleteCconsumerResponse.status, true);
            
            ApiResponse deleteApiResponse = coreService.deleteApi(apiResponse.getId());
            Assert.assertEquals("Create Api delete", deleteApiResponse.status, true);
            
            ApiResponse deleteLensSettingsResponse = coreService.deleteLensSettings(lensSettingsResponse.getId());
            Assert.assertEquals("Delete Lens settings", deleteLensSettingsResponse.status, true);
            
           
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    /**
     * addErrorLogWithValidData
     */
    @Test
    public void addErrorLogWithValidData() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get
            CoreErrorlog coreErrorlog =  dbService.getErrorlogById(apiContext.getRequestId().toString());
            // Get Assert
            Assert.assertNotEquals("Get error log check", coreErrorlog, null);

            // Delete
            ApiResponse deleteLogResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteLogResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addErrorLogWithInvalidClientId
     */
    @Test
    public void addErrorLogWithInvalidClientId() {
        try {
            // Add
            CoreClient invalidClient = new CoreClient();
            invalidClient.setId(-1);
            apiContext.setClient(invalidClient);
            ApiResponse addApiResponse = new ApiResponse();
            try {
                loggerService.addErrorLog(apiContext);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }
            apiContext.setClient(client);

            // Add Assert
            Assert.assertEquals("Add Error Log Invalid ClientId check", addApiResponse.status, false);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * addErrorLogWithInvalidApiId
     */
    @Test
    public void addErrorLogWithInvalidApiId() {
        try {
            // Add
            CoreClientapi clientApi = new CoreClientapi();
            CoreApi api= new CoreApi();
            api.setId(-1);
            clientApi.setCoreApi(api);
            apiContext.setClientApi(clientApi);

            ApiResponse addApiResponse = new ApiResponse();
            try {
                loggerService.addErrorLog(apiContext);
                addApiResponse.status = true;
            } catch (Exception ex) {
                addApiResponse.status = false;
            }

            // Add Assert
            Assert.assertEquals("Add Error Log with Invalid ApiId check", addApiResponse.status, false);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorLogWithLogId
     */
    @Test
    public void getErrorLogWithLogId() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString());
            // Get Assert
            Assert.assertNotEquals("Get error log check", coreErrorlog, null);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorLogWithInvalidLogId
     */
    @Test
    public void getErrorLogWithInvalidLogId() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get
            CoreErrorlog coreErrorlog = dbService.getErrorlogById("123");
            // Get Assert
            Assert.assertEquals("Get error log check", coreErrorlog, null);
            
            // Delete
            coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString());
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorLogWithValidClientId
     */
    @Test
    public void getErrorLogWithValidClientId() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            // Get Assert
            Assert.assertNotEquals("Get error log check", coreErrorlog, null);

            // Delete
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorLogWithInvalidClientId
     */
    @Test
    public void getErrorLogWithInvalidClientId() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), 123);
            // Get Assert
            Assert.assertEquals("Get error log check", coreErrorlog, null);

            // Delete
            coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getListofAllErrorlog
     */
    @Test
    public void getListofAllErrorlog() {
        try {            
            // Add 
            loggerService.addErrorLog(apiContext);
            
            GetErrorLogsRequest errorLogs = new GetErrorLogsRequest();
            ApiResponse getCoreErrorlog = loggerService.getErrorLogs(errorLogs, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get error log check", getCoreErrorlog.status, true);

            // Delete
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);
        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorLogWithValidApiKey
     */
    @Test
    public void getErrorLogWithValidApiKey() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get Api
            GetErrorLogsRequest errorLogs = new GetErrorLogsRequest();
            errorLogs.setApiKey(apiResponse.getKey());
            ApiResponse getCoreErrorlog = loggerService.getErrorLogs(errorLogs, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get error log check", getCoreErrorlog.status, true);

            // Delete
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorLogWithInvalidApiKey
     */
    @Test
    public void getErrorLogWithInvalidApiKey() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get Api
            GetErrorLogsRequest errorLogs = new GetErrorLogsRequest();
            errorLogs.setApiKey("Invalid");

            ApiResponse getCoreErrorlog = new ApiResponse();

            try {
                getCoreErrorlog = loggerService.getErrorLogs(errorLogs, apiContext.getClient().getId());
                getCoreErrorlog.status = true;
            } catch (Exception ex) {
                getCoreErrorlog.status = false;
            }
            // Get Assert
            Assert.assertEquals("Get error log check", getCoreErrorlog.status, false);

            // Delete
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorlogbyCriteriaWithRequestId
     */
    @Test
    public void getErrorlogbyCriteriaWithRequestId() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get Api
            GetErrorLogsRequest errorLogs = new GetErrorLogsRequest();
            errorLogs.setRequestId(apiContext.getRequestId().toString());
            ApiResponse getCoreErrorlog = loggerService.getErrorLogs(errorLogs, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get error log check", getCoreErrorlog.status, true);

            // Delete
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorlogbyCriteriaWithInValidRequestId
     */
    @Test
    public void getErrorlogbyCriteriaWithInValidRequestId() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get Api
            GetErrorLogsRequest errorLogs = new GetErrorLogsRequest();
            errorLogs.setRequestId("Invalid");
            ApiResponse getCoreErrorlog = loggerService.getErrorLogs(errorLogs, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get error log check", getCoreErrorlog.status, false);

            // Delete
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorlogbyCriteriaWithStatusCode
     */
    @Test
    public void getErrorlogbyCriteriaWithStatusCode() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);
            
            // Get Api
            GetErrorLogsRequest errorLogs = new GetErrorLogsRequest();
            errorLogs.setStatusCode(new BigInteger(Integer.toString(apiContext.getStatusCode())));
            ApiResponse getCoreErrorlog = loggerService.getErrorLogs(errorLogs, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get error log check", getCoreErrorlog.status, true);

            // Delete
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * getErrorlogbyCriteriaWithInvalidStatusCode
     */
    @Test
    public void getErrorlogbyCriteriaWithInvalidStatusCode() {
        try {
            // Add 
            loggerService.addErrorLog(apiContext);

            // Get Api
            GetErrorLogsRequest errorLogs = new GetErrorLogsRequest();
            errorLogs.setStatusCode(new BigInteger(Integer.toString(123)));
            ApiResponse getCoreErrorlog = loggerService.getErrorLogs(errorLogs, apiContext.getClient().getId());
            // Get Assert
            Assert.assertEquals("Get error log check", getCoreErrorlog.status, false);

            // Delete
            CoreErrorlog coreErrorlog = dbService.getErrorlogById(apiContext.getRequestId().toString(), apiContext.getClient().getId());
            ApiResponse deleteApiResponse = dbService.deleteErrorlog(coreErrorlog);
            // Delete Assert
            Assert.assertEquals("Delete error log check", deleteApiResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }
}
