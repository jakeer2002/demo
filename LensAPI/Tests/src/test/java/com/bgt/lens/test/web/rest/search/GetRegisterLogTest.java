// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest.search;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.SearchRegisterLogCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchRegisterlog;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.RegisterLog;
import com.bgt.lens.model.rest.response.RegisterLogList;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.model.rest.response.Vendor;
import com.bgt.lens.model.searchservice.request.CreateVendorRequest;
import com.bgt.lens.model.searchservice.request.RegisterResumeRequest;
import com.bgt.lens.model.searchservice.request.UnregisterResumeRequest;
import com.bgt.lens.model.soap.response.searchservice.GetRegisterLogByCriteriaResponse;
import com.bgt.lens.model.soap.response.searchservice.GetRegisterLogByIdResponse;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.search.ISearchService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.web.rest.SearchController;
import com.bgt.lens.web.rest.UriConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.SerializationUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Get Register log test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class GetRegisterLogTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Locale type.
     */
    private final String localeTypeEnuk = "en_uk";
    /**
     * Interface CoreService.
     */
    @Autowired
    private ICoreService coreService;
    /**
     * Interface DataRepository Service.
     */
    @Autowired
    private IDataRepositoryService dbService;
    /**
     * Api Config
     */
    @Autowired
    private ApiConfiguration apiConfig;
    /**
     * Interface Search Service.
     */
    @Autowired
    private ISearchService searchService;
    /**
     * API context.
     */
    private ApiContext apiContext;
    /**
     * API.
     */
    private Api api;
    /**
     * Lens settings.
     */
    private LensSettings enusLensSettings, enukLensSettings, localeSettings;
    /**
     * vendor
     */
    private Vendor adminVendor;
    /**
     * External client vendor
     */
    private Vendor externalClientVendor;
    /**
     * Resource.
     */
    private Resources resource;
    /**
     * Client.
     */
    private Client client;
    /**
     * Client.
     */
    private CoreClient coreClient;
    /**
     * Admin client
     */
    private CoreClient adminClient;
    /**
     * ExternalClient
     */
    private CoreClient externalClient;
    /**
     * Helper object.
     */
    private TestHelper testHelper;
    /**
     * Helper.
     */
    private Helper helper;
    /**
     * Binary data.
     */
    private String binaryData;
    /**
     * Helper object.
     */
    private final TestHelper testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final SearchController searchController = new SearchController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * Setup Class.
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down Class.
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup method.
     */
    @Before
    public final void setUp() {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(searchController).build();
            ReflectionTestUtils
                    .setField(searchController,
                            "searchService",
                            searchService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            CreateApiRequest createApi = testHelper
                    .getCreateApiRequest("/input/CreateApi.xml");
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;
            AddLensSettingsRequest settingsRequest
                    = testHelper.getAddLensSettingsRequest(
                            "/input/CreateLensSettingsForJob.xml");
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(
                    Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(
                    Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setDocServerResume(testHelper.docServerName);
            settingsRequest.setHypercubeServerResume(testHelper.hypercubeServerName);
            settingsRequest.setDocServerPostings(testHelper.postingDocServerName);
            settingsRequest.setHypercubeServerPostings(testHelper.postingHypercubeServerName);
            ApiResponse enusLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check",
                    enusLensSettingsResponse.status, true);
            enusLensSettings
                    = (LensSettings) enusLensSettingsResponse.responseData;
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse enukLensSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            enukLensSettings
                    = (LensSettings) enukLensSettingsResponse.responseData;
            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService
                    .addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check",
                    enukLensSettingsResponse.status, true);
            localeSettings
                    = (LensSettings) localeSettingsResponse.responseData;
            AddResourceRequest resourceRequest = testHelper
                    .getAddResourceRequest("/input/CreateResource.xml");

            ApiResponse resourceResponse = coreService
                    .addResource(resourceRequest);
            Assert.assertEquals("Create resource check",
                    resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            helper = new Helper();
            //coreClient.setSearchSettings(helper.getJsonStringFromObject(client.getSearchSettings()));
            Set<CoreLenssettings> coreLenssettingses = new HashSet<>();

            // en_us LensSettings
            CoreLenssettings enusCoreLenssettings = new CoreLenssettings();
            enusCoreLenssettings.setId(1);
            enusCoreLenssettings.setHost(testHelper.hostname);
            enusCoreLenssettings.setPort(testHelper.port);
            enusCoreLenssettings.setCharacterSet(testHelper.encoding);
            enusCoreLenssettings.setTimeout(testHelper.timeOut);
            enusCoreLenssettings.setInstanceType(testHelper.instanceType);
            enusCoreLenssettings.setLocale(testHelper.locale);
            enusCoreLenssettings.setStatus(testHelper.status);
            coreLenssettingses.add(enusCoreLenssettings);
            // en_uk LensSettings
            CoreLenssettings enukCoreLenssettings
                    = (CoreLenssettings) SerializationUtils
                    .clone(enusCoreLenssettings);
            enukCoreLenssettings.setId(2);
            enukCoreLenssettings.setLocale(localeTypeEnuk);
            coreLenssettingses.add(enukCoreLenssettings);
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            coreLenssettingses.add(localeCoreLenssettings);

            CreateVendorRequest createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor01");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            ApiResponse response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            adminVendor = (Vendor) response.responseData;

            createVendorRequest = new CreateVendorRequest();
            createVendorRequest.setInstanceId(BigInteger.valueOf(enusLensSettings.getId()));
            createVendorRequest.setVendor("resvendor0101");
//            createVendorRequest.setDocServerName(testHelper.docServerName);
            response = searchService.createVendor(createVendorRequest);
            Assert.assertTrue("Create vendor status check", response.status);

            externalClientVendor = (Vendor) response.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper
                    .getCreateConsumerRequest("/input/CreateConsumerSearch.xml");

            CreateConsumerRequest.LensSettings settingsList
                    = consumer.getLensSettings();
            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(enusLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(enukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);
            
            consumer.setLensSettings(settingsList);

            CreateConsumerRequest.VendorSettings settings = new CreateConsumerRequest.VendorSettings();
            settings.getVendorIdList().add(BigInteger.valueOf(adminVendor.getVendorId()));
            consumer.setVendorSettings(settings);

            consumer = testHelper.updateApiandResource(consumer, api.getId(),
                    api.getKey(), resource.getResourceId().toString());
//            consumer.getVendorSettings().getVendorIdList().clear();
            response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;
            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(enusCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(enukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);       
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);
            
            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService
                    .getClientApiByCriteria(clientapiCriteria);
            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("POST");
            apiContext.setResource("resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            helper.setApiContext(apiContext);

            adminClient = dbService
                    .getClientById(client.getId());
            adminClient.setTier(1);
            CoreClient updatedAdminClient = adminClient;
            Set<SearchVendorsettings> searchVendorSettingsSet = new HashSet<>();
            SearchVendorsettings searchVendorsettings = dbService.getVendorById(adminVendor.getVendorId());
            searchVendorSettingsSet.add(searchVendorsettings);
            updatedAdminClient.setSearchVendorsettingses(searchVendorSettingsSet);
            ApiResponse updateClientResponse = dbService.updateClient(adminClient, updatedAdminClient);
            Assert.assertTrue("Update client check", updateClientResponse.status);

            //externalClient = consumer; 
            searchVendorSettingsSet = new HashSet<>();
            searchVendorsettings = dbService.getVendorById(externalClientVendor.getVendorId());
            searchVendorSettingsSet.add(searchVendorsettings);
            consumer.setKey("Test01");
            consumer.setName("test01");
            consumer.setTier(BigInteger.valueOf(2));
            CreateConsumerRequest.VendorSettings vendorSettingsList
                    = consumer.getVendorSettings();
            vendorSettingsList.getVendorIdList().add(new BigInteger(
                    Integer.toString(externalClientVendor.getVendorId())));
            ApiResponse externalClientCreateresponse = coreService.createConsumer(consumer);
            Assert.assertEquals("Create external consumer check", externalClientCreateresponse.status, true);

            Client external = (Client) externalClientCreateresponse.responseData;

            externalClient = dbService
                    .getClientById(external.getId());
            externalClient.setTier(2);

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = RegisterResumeTest.class
                    .getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = helper.encodeBase64String(
                            testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down method.
     */
    @After
    public final void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService
                    .deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete",
                    deleteApiResponse.status, true);
             // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService
                    .deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete",
                    deleteConsumerResponse.status, true);

            ClientCriteria criteria = new ClientCriteria();
            criteria.setClientKey("Test01");
            List<CoreClient> clientList = dbService.getClientByCriteria(criteria);
            if (clientList != null && clientList.size() > 0) {
                ApiResponse deleteExternalClientResponse = dbService.deleteClient(clientList.get(0));
                Assert.assertTrue("Unregister resume request", deleteExternalClientResponse.status);
            }
            // Delete LensSettings
            ApiResponse enusDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enusLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete",
                    enusDeleteLensSettingsResponse.status, true);
            ApiResponse enukDeleteLensSettingsResponse = coreService
                    .deleteLensSettings(enukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete",
                    enukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService
                    .deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Deletevendor settings",
                    localeDeleteSettingsResponse.status, true);
           

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogIdWithValidLogId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            SearchRegisterLogCriteria registerLogCriteria = new SearchRegisterLogCriteria();
            registerLogCriteria.setClientId(coreClient.getId());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            registerLogCriteria = new SearchRegisterLogCriteria();
            registerLogCriteria.setClientId(coreClient.getId());
            registerLogCriteria.setRegisterOrUnregister(true);
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(registerLogCriteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs + "/" + updatedRegisterLogList.get(0).getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByIdResponse getRegisterLogResponse = (GetRegisterLogByIdResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByIdResponse());

            if (getRegisterLogResponse != null && getRegisterLogResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getRegisterLogResponse.responseData);
                RegisterLog registerLog = mapper.readValue(json, RegisterLog.class
                );

                Assert.assertEquals("Register log client Id check", (long) coreClient.getId(), (long) registerLog.getClientId());
                Assert.assertEquals("Register log lens settings Id check", (long) enusLensSettings.getId(), (long) registerLog.getLensSettingsId());
                Assert.assertEquals("Register log vendor Id check", (long) adminVendor.getVendorId(), (long) registerLog.getVendorId());
                Assert.assertEquals("Register log registration check", com.bgt.lens.helpers.Enum.registerLogType.Register.toString(), registerLog.getRegisterOrUnregister());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogWithInvalidId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);


            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs + "/0")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByIdResponse getRegisterLogResponse = (GetRegisterLogByIdResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByIdResponse());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithoutCriteria() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            int updatedRegisterLogCount = dbService.getRegisterLogCount();
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogCount, (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithoutCriteria() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithValidRegisterLogId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(new SearchRegisterLogCriteria());

            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("id", updatedRegisterLogList.get(0).getId().toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", 1, (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithInvalidRegisterLogId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("id", "0"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidRegisterLogId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("id", updatedRegisterLogList.get(0).getId().toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", 1, (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidRegisterLogId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
   
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("id", "0"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithValidClientId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(adminClient.getId());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("clientId", String.valueOf(adminClient.getId())))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithInvalidClientId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("clientId", "-1"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidClientId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("clientId", externalClient.getId().toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidClientId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert  
            ApiResponse apiResponse = new ApiResponse();
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("clientId", "-1"))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Register log invalid client id check", apiResponse.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithVendorId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setVendorId(adminVendor.getVendorId());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("vendorId", String.valueOf(adminVendor.getVendorId())))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithInvalidVendorId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("vendorId", "0"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidVendorId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            criteria.setVendorId(externalClientVendor.getVendorId());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("vendorId", externalClientVendor.getVendorId().toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidVendorId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert  
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("vendorId", "0"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithValidLensSettingsId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setLensSettingsId(enusLensSettings.getId());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("lensSettingsId", String.valueOf(enusLensSettings.getId())))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithInvalidLensSettingsId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("lensSettingsId", "0"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidLensSettingsId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            criteria.setLensSettingsId(enusLensSettings.getId());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("lensSettingsId", enusLensSettings.getId().toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidLensSettingsId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert  
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("lensSettingsId", "0"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithValidDocType() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setType(com.bgt.lens.helpers.Enum.docType.resume.toString());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("docType", "resume"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithInvalidDocType() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);
            
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("docType", "test"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidDocType() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            criteria.setType(com.bgt.lens.helpers.Enum.docType.resume.toString());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("docType", "resume"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidDocType() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert  
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("docType", "test"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithValidStoreType() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setRegisterOrUnregister(true);
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("storeType", "Register"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithInvalidStoreType() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert 
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("storeType", "test"))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Get register log invalid store typr check", apiResponse.status);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidStoreType() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            criteria.setRegisterOrUnregister(true);
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("storeType", "Register"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidStoretype() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert  
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("storeType", "test"))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Get register log invalid store type status check", apiResponse.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithValidFailoverStatus() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setFailoverUpdateStatus(false);
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("failoverUpdate", "false"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidFailoverUpdate() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            criteria.setDocId("2");
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("failoverUpdate", "false"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithValidDocId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setDocId("1");
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("docId", "1"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithInvalidDocId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            adminClient = dbService
                    .getClientById(client.getId());
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("docId", "0"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidDocId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            criteria.setDocId("2");
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("docId", "2"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidDocId() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // Register Assert  
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("docType", "0"))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithValidFrom() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            LocalDate dt = helper.getDateFormat().parseLocalDate(new LocalDate().toString());
            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setFrom(helper.parseDate(dt.toString()));
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert    

            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("from", dt.toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithInvalidFrom() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert
            LocalDate dt = helper.getDateFormat().parseLocalDate(new LocalDate().toString());
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("from", dt.plusDays(10).toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidFrom() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            LocalDate dt = helper.getDateFormat().parseLocalDate(new LocalDate().toString());
            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            criteria.setFrom(helper.parseDate(dt.toString()));
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("from", dt.toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidFrom() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            LocalDate dt = helper.getDateFormat().parseLocalDate(new LocalDate().toString());
            // Register Assert  
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("from", dt.plusDays(10).toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithValidTo() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            LocalDate dt = helper.getDateFormat().parseLocalDate(new LocalDate().toString());
            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setTo(new LocalDate(new Date()).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            adminClient = dbService
                    .getClientById(client.getId());
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert    

            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("to", dt.toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsAdminWithInvalidTo() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);


            adminClient = dbService
                    .getClientById(client.getId());
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            // Register Assert
            LocalDate dt = helper.getDateFormat().parseLocalDate(new LocalDate().toString());
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("to", dt.minusYears(10).toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithValidTo() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            LocalDate dt = helper.getDateFormat().parseLocalDate(new LocalDate().toString());
            SearchRegisterLogCriteria criteria = new SearchRegisterLogCriteria();
            criteria.setClientId(externalClient.getId());
            criteria.setTo(new LocalDate(new Date()).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
            List<SearchRegisterlog> updatedRegisterLogList = dbService.getRegisterLogByCriteria(criteria);

            // Register Assert            
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("to", dt.toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

            GetRegisterLogByCriteriaResponse getVendorResponse = (GetRegisterLogByCriteriaResponse) helper.getObjectFromJson(result.getResponse().getContentAsString(), new GetRegisterLogByCriteriaResponse());

            if (getVendorResponse != null && getVendorResponse.status) {
                ObjectMapper mapper = new ObjectMapper();
                byte[] json = mapper.writeValueAsBytes(getVendorResponse.responseData);
                RegisterLogList registerLogList = mapper.readValue(json, RegisterLogList.class
                );

                Assert.assertEquals("Register log count check", updatedRegisterLogList.size(), (long) registerLogList.registerLogList.size());
            }

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidTo() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);


            LocalDate dt = helper.getDateFormat().parseLocalDate(new LocalDate().toString());
            // Register Assert  
            MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }).param("to", dt.minusYears(10).toString()))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andReturn();

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidFromFormat() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);


            // Register Assert  
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("from", "2015/09/30"))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Get register log invalid from format status check", apiResponse.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Create Vendor
     */
    @Test
    public final void getRegisterLogByCriteriaAsClientWithInvalidToFormat() {
        try {
            apiContext.setClient(adminClient);
            helper.setApiContext(apiContext);
            unregisterResume("1", testHelper.instanceType, adminVendor.getVendor(), TestHelper.Locale.en_us.toString());

            ApiResponse registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "1", adminVendor.getVendor(), adminClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

            // External client vendor create and register            
            apiContext.setClient(externalClient);
            helper.setApiContext(apiContext);
            unregisterResume("2", testHelper.instanceType, externalClientVendor.getVendor(), TestHelper.Locale.en_us.toString());

            registerResumeResponse = registerResume(testHelper.instanceType, TestHelper.Locale.en_us.name(), binaryData, "2", externalClientVendor.getVendor(), externalClient);
            Assert.assertTrue("Register resume response check", registerResumeResponse.status);

         
            // Register Assert  
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.status = true;
            try {
                MvcResult result = mockMvc.perform(get(UriConstants.GetRegisterLogs)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }).param("to", "2015/09/30"))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(false)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andReturn();
            } catch (Exception ex) {
                apiResponse.status = false;
            }

            Assert.assertFalse("Get register log invalid from format status check", apiResponse.status);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * unregisterResume
     *
     * @param resumeId
     * @param instanceType
     * @param vendorName
     * @param locale
     * @return
     * @throws Exception
     */
    public ApiResponse unregisterResume(String resumeId, String instanceType, String vendorName, String locale) throws Exception {
        UnregisterResumeRequest unregisterRequest = new UnregisterResumeRequest();
        unregisterRequest.setResumeId(resumeId);
        unregisterRequest.setVendor(vendorName);
        unregisterRequest.setInstanceType(instanceType);
        unregisterRequest.setLocale(locale);

        ApiResponse unregisterResponse = searchService.unregisterResume(unregisterRequest);
        return unregisterResponse;
    }

    /**
     * Register resume
     *
     * @param instanceType
     * @param locale
     * @param binaryData
     * @param resumeId
     * @param vendorName
     * @param client
     * @return
     */
    public ApiResponse registerResume(String instanceType, String locale, String binaryData, String resumeId, String vendorName, CoreClient client) throws Exception {
        RegisterResumeRequest registerResumeRequest = new RegisterResumeRequest();
        registerResumeRequest.setInstanceType(instanceType);
        registerResumeRequest.setLocale(locale);
        registerResumeRequest.setBinaryData(binaryData);
        registerResumeRequest.setResumeId(resumeId);
        registerResumeRequest.setVendor(vendorName);
        apiContext.setClient(client);
        helper.setApiContext(apiContext);
        ApiResponse registerResumeResponse = searchService.registerResume(registerResumeRequest);
        return registerResumeResponse;
    }
}
