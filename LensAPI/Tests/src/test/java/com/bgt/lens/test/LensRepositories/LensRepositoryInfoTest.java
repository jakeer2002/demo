// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.LensRepositories;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.test.Helper.TestHelper;
import java.io.IOException;
import javax.servlet.ServletContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * Lens Info Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class LensRepositoryInfoTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;

    private ApiErrors apiErrors = new ApiErrors();

    private Helper helper = new Helper();

    /**
     * Lens repository object
     */
    @Autowired
    private LensRepository lensRepository;

    /**
     * Lens settings object
     */
    private CoreLenssettings lensSettings;

    /**
     * Helper object
     */
    private TestHelper testHelper;

    /**
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {

        ServletContext servletContext = wac.getServletContext();

        servletContext.setAttribute("applicationVersion", applicationVersion);
        servletContext.setAttribute("portalErrorURL", portalErrorURL);
        String propFileName = "/ResumeTestConfiguration.properties";
        testHelper = new TestHelper(propFileName);
        lensSettings = new CoreLenssettings();
        lensSettings.setHost(testHelper.hostname);
        lensSettings.setPort(testHelper.port);
        lensSettings.setCharacterSet(testHelper.encoding);
        lensSettings.setTimeout(testHelper.timeOut);
        lensSettings.setInstanceType(testHelper.instanceType);
        lensSettings.setLocale(testHelper.locale);
        lensSettings.setStatus(testHelper.status);
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * InfoTestWithVaildData
     */
    @Test
    public void infoTestWithVaildData() {
        try {
            // Info
            ApiResponse lensResponse = lensRepository.info(lensSettings);
            // Info Assert
            Assert.assertEquals("Lens Info Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * infoTestWithInvalidHostName
     */
    @Test
    public void infoTestWithInvalidHostName() {
        try {
            // Info
            lensSettings.setHost("Invalid");
            ApiResponse lensResponse = lensRepository.info(lensSettings);
            // Info Assert
            Assert.assertEquals("Lens Info With Invalid Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Info With Invalid Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * infoTestWithEmptyHostName
     */
    @Test
    public void infoTestWithEmptyHostName() {
        try {
            // Info
            lensSettings.setHost("");
            ApiResponse lensResponse = lensRepository.info(lensSettings);
            // Info Assert
            Assert.assertEquals("Lens Info With Empty Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Info With Empty Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * infoTestWithNullHostName
     */
    @Test
    public void infoTestWithNullHostName() {
        try {
            // Info
            lensSettings.setHost(null);
            ApiResponse lensResponse = lensRepository.info(lensSettings);
            // Info Assert
            Assert.assertEquals("Lens Info With Null Hostname Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Info With Null Hostname Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * InfoTestWithInvalidHostPort
     */
    @Test
    public void infoTestWithInvalidHostPort() {
        try {
            // Info
            lensSettings.setPort(0000);
            ApiResponse lensResponse = lensRepository.info(lensSettings);
            // Info Assert
            Assert.assertEquals("Lens Info With Invalid HostPort Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Info With Invalid HostPort Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * InfoTestWithInValidEncoding
     */
    @Test
    public void infoTestWithInValidEncoding() {
        try {
            // Info
            lensSettings.setCharacterSet("Invalid");
            ApiResponse lensResponse = lensRepository.info(lensSettings);
            // Info Assert
            Assert.assertEquals("Lens Info With InValid Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Info With InValid Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * InfoTestWithEmptyEncoding
     */
    @Test
    public void infoTestWithEmptyEncoding() {
        try {
            // Info
            lensSettings.setCharacterSet("");
            ApiResponse lensResponse = lensRepository.info(lensSettings);
            // Info Assert
            Assert.assertEquals("Lens Info With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Info With Empty Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * InfoTestWithNullEncoding
     */
    @Test
    public void infoTestWithNullEncoding() {
        try {
            // Info
            lensSettings.setCharacterSet(null);
            ApiResponse lensResponse = lensRepository.info(lensSettings);
            // Info Assert
            Assert.assertEquals("Lens Info With Null Encoding Status Check", lensResponse.status, true);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

    /**
     * InfoTestWithInvalidTimeOut
     */
    @Test
    public void infoTestWithInvalidTimeOut() {
        try {
            // Info
            lensSettings.setTimeout(-10);
            ApiResponse lensResponse = lensRepository.info(lensSettings);
            // Info Assert
            Assert.assertEquals("Lens Info With Empty Encoding Status Check", lensResponse.status, false);
            Assert.assertEquals("Lens Info With Empty Encoding Status Check", helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE), lensResponse.responseData);

        } catch (Exception ex) {
            System.out.println(ex);
            Assert.fail();
        }
    }

}
