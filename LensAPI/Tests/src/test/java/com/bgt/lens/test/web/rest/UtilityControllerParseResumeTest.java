// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.test.web.rest;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.rest.response.Api;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.model.rest.response.Resources;
import com.bgt.lens.repositories.impl.LensRepository;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.utility.IUtilityService;
import com.bgt.lens.test.Helper.TestHelper;
import com.bgt.lens.test.UtilityServices.ParseResumeTest;
import com.bgt.lens.web.rest.UriConstants;
import com.bgt.lens.web.rest.UtilityController;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

/**
 * UtilityController Parse resume test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:springtest.xml"})
@WebAppConfiguration
public class UtilityControllerParseResumeTest {

    @Value("${ApplicationVersion}")
    private String applicationVersion;

    @Value("${error.portalErrorURL}")
    private String portalErrorURL;

    @Autowired
    protected WebApplicationContext wac;
   
    private ApiErrors apiErrors = new ApiErrors();

    @Autowired
    ICoreService coreService;

    @Autowired
    IDataRepositoryService dbService;

    @Autowired
    IUtilityService utilityService;

    /**
     * API context
     */
    public ApiContext apiContext;

    /**
     * API
     */
    public Api api;

    /**
     * Lens settings
     */
    public LensSettings en_usLensSettings = null, en_ukLensSettings = null, localeSettings = null;

    /**
     * Resources
     */
    public Resources resource;

    /**
     * Client
     */
    public Client client;

    /**
     * Client
     */
    public CoreClient coreClient;

    /**
     * Test helper object
     */
    public TestHelper testHelper = null;

    /**
     * Helper object
     */
    public Helper _helper;

    /**
     * Extension
     */
    private String extension;

    /**
     * Binary data
     */
    private String binaryData;

    /**
     * Field list
     */
    public ParseResumeRequest.FieldList fieldList;

    /**
     * Instance type
     */
    public static final String InstanceType = "SPECTRUM";

    /**
     * Locale US
     */
    public static final String LocaleTypeEn_us = "en_us";

    /**
     * Locale UK
     */
    public static final String LocaleTypeEn_uk = "en_uk";

    /**
     * Invalid instance type
     */
    public static final String invalidType = "Invalid Instance Type";

    /**
     * Mock HTTP request
     */
    private MockHttpServletRequest request;

    /**
     * *
     * Helper object
     */
    private static final TestHelper _testhelper = new TestHelper();
    /**
     * Mock MVC.
     */
    private MockMvc mockMvc;
    /**
     * Utility Controller object.
     */
    private final UtilityController utilityController = new UtilityController();
    /**
     * Core Lenssettings Id.
     */
    private final int localeCoreLenssettingsId = 3;

    /**
     * *
     * Setup
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * *
     * Tear down
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * *
     * Setup
     *
     * @throws JAXBException
     */
    @Before
    public void setUp() throws JAXBException {
        try {
            ServletContext servletContext = wac.getServletContext();
            servletContext.setAttribute("applicationVersion", applicationVersion);
            servletContext.setAttribute("portalErrorURL", portalErrorURL);

            mockMvc = MockMvcBuilders.
                    standaloneSetup(utilityController).build();
            ReflectionTestUtils
                    .setField(utilityController,
                            "utilityService",
                            utilityService);
            String propFileName = "/ResumeTestConfiguration.properties";
            testHelper = new TestHelper(propFileName);
            // create api
            CreateApiRequest createApi = testHelper.getCreateApiRequest("/input/CreateApi.xml");
            //CreateApiRequest createApi = (CreateApiRequest) jaxbUnmarshaller.unmarshal(ParseResumeBGTXMLTests.class.getResourceAsStream("/input/CreateApi.xml"));
            ApiResponse apiResponse = coreService.createApi(createApi);
            Assert.assertEquals("Create Api check", apiResponse.status, true);
            api = (Api) apiResponse.responseData;

            // create lens settings
            AddLensSettingsRequest settingsRequest = testHelper.getAddLensSettingsRequest("/input/CreateLensSettings.xml");
            //AddLensSettingsRequest settingsRequest = (AddLensSettingsRequest) jaxbUnmarshaller.unmarshal(ParseResumeBGTXMLTests.class.getResourceAsStream("/input/CreateLensSettings.xml"));
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            //en_us
            ApiResponse en_usLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_us check", en_usLensSettingsResponse.status, true);
            en_usLensSettings = (LensSettings) en_usLensSettingsResponse.responseData;
            //en_uk
            settingsRequest.setLocale("en_uk");
            settingsRequest.setCountry("GreatBritain");
            ApiResponse en_ukLensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            en_ukLensSettings = (LensSettings) en_ukLensSettingsResponse.responseData;

            settingsRequest.setHost(testHelper.ld_hostname);
            settingsRequest.setPort(BigInteger.valueOf(testHelper.ld_port));
            settingsRequest.setCharSet(testHelper.ld_encoding);
            settingsRequest.setLocale("NA");
            settingsRequest.setInstanceType("LD");
            ApiResponse localeSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings en_uk check", en_ukLensSettingsResponse.status, true);
            localeSettings = (LensSettings) localeSettingsResponse.responseData;

            // create resource
            AddResourceRequest resourceRequest = testHelper.getAddResourceRequest("/input/CreateResource.xml");
            //AddResourceRequest resourceRequest = (AddResourceRequest) jaxbUnmarshaller.unmarshal(ParseResumeBGTXMLTests.class.getResourceAsStream("/input/CreateResource.xml"));
            ApiResponse resourceResponse = coreService.addResource(resourceRequest);
            Assert.assertEquals("Create resource check", resourceResponse.status, true);
            resource = (Resources) resourceResponse.responseData;

            // create consumer
            CreateConsumerRequest consumer = testHelper.getCreateConsumerRequest("/input/CreateConsumer.xml");
            //CreateConsumerRequest consumer = (CreateConsumerRequest) jaxbUnmarshaller.unmarshal(ParseResumeBGTXMLTests.class.getResourceAsStream("/input/CreateConsumer.xml"));

            CreateConsumerRequest.LensSettings settingsList = consumer.getLensSettings();

            settingsList.getInstanceList().clear();
            
            com.bgt.lens.model.adminservice.request.InstanceList en_us_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList en_gb_InstanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            com.bgt.lens.model.adminservice.request.InstanceList instanceList = new com.bgt.lens.model.adminservice.request.InstanceList();
            en_us_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_usLensSettings.getId())));
            en_us_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_us_InstanceList);            
            en_gb_InstanceList.setInstanceId(new BigInteger(Integer.toString(en_ukLensSettings.getId())));
            en_gb_InstanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(en_gb_InstanceList);     
            instanceList.setInstanceId(new BigInteger(Integer.toString(localeSettings.getId())));
            instanceList.setRateLimitEnabled(false);
            settingsList.getInstanceList().add(instanceList);

            consumer.setLensSettings(settingsList);

            consumer = testHelper.updateApiandResource(consumer, api.getId(), api.getKey(), resource.getResourceId().toString());

            ApiResponse response = coreService.createConsumer(consumer);
            Assert.assertEquals("Create consumer check", response.status, true);
            client = (Client) response.responseData;

            coreClient = new CoreClient();
            coreClient.setId(client.getId());
            coreClient.setClientKey("Sample");
            coreClient.setSecret("Sample");
            coreClient.setDumpStatus(true);
            

            // en_us LensSettings
            CoreLenssettings en_usCoreLenssettings = new CoreLenssettings();
            en_usCoreLenssettings.setId(1);
            en_usCoreLenssettings.setHost(testHelper.hostname);
            en_usCoreLenssettings.setPort(testHelper.port);
            en_usCoreLenssettings.setCharacterSet(testHelper.encoding);
            en_usCoreLenssettings.setTimeout(testHelper.timeOut);
            en_usCoreLenssettings.setInstanceType(testHelper.instanceType);
            en_usCoreLenssettings.setLocale(testHelper.locale);
            en_usCoreLenssettings.setStatus(testHelper.status);

            // en_uk LensSettings
            CoreLenssettings en_ukCoreLenssettings = (CoreLenssettings) org.apache.commons.lang3.SerializationUtils.clone(en_usCoreLenssettings);
            en_ukCoreLenssettings.setId(2);
            en_ukCoreLenssettings.setLocale(LocaleTypeEn_uk);
            
            // en_us LensSettings
            CoreLenssettings localeCoreLenssettings = new CoreLenssettings();
            localeCoreLenssettings.setId(localeCoreLenssettingsId);
            localeCoreLenssettings.setHost(testHelper.ld_hostname);
            localeCoreLenssettings.setPort(testHelper.ld_port);
            localeCoreLenssettings.setCharacterSet(testHelper.ld_encoding);
            localeCoreLenssettings.setTimeout(testHelper.timeOut);
            localeCoreLenssettings.setInstanceType("LD");
            localeCoreLenssettings.setLocale("NA");
            localeCoreLenssettings.setStatus(testHelper.status);
            
            Set<CoreClientlenssettings> coreClientlenssettingses = new HashSet<>();
            CoreClientlenssettings en_usCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings en_ukCoreClientlenssettings = new CoreClientlenssettings();
            CoreClientlenssettings locale_CoreClientlenssettings = new CoreClientlenssettings();
            coreClientlenssettingses.clear();
            
            en_usCoreClientlenssettings.setCoreLenssettings(en_usCoreLenssettings);         
            coreClientlenssettingses.add(en_usCoreClientlenssettings);
            en_ukCoreClientlenssettings.setCoreLenssettings(en_ukCoreLenssettings);  
            coreClientlenssettingses.add(en_ukCoreClientlenssettings);            
            locale_CoreClientlenssettings.setCoreLenssettings(localeCoreLenssettings);  
            coreClientlenssettingses.add(locale_CoreClientlenssettings);        
            
            coreClient.setCoreClientlenssettingses(coreClientlenssettingses);

            ClientapiCriteria clientapiCriteria = new ClientapiCriteria();
            CoreApi coreApi = new CoreApi();
            coreApi.setId(api.getId());
            coreApi.setApiKey(api.getKey());
            clientapiCriteria.setCoreApi(coreApi);
            clientapiCriteria.setCoreClient(coreClient);
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(clientapiCriteria);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setMethod("Post");
            apiContext.setResource("Resume");
            apiContext.setClient(coreClient);
            apiContext.setClientApi(clientapis.get(0));
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();

            String fileName = "/TestResumes/100008.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);

            // Fields List
            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("All");
            fieldList.getField().addAll(fields);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * Tear down
     */
    @After
    public void tearDown() {
        try {
            // Delete Api
            ApiResponse deleteApiResponse = coreService.deleteApi(api.getId());
            Assert.assertEquals("Delete Api delete", deleteApiResponse.status, true);
             // Delete Consumer
            ApiResponse deleteConsumerResponse = coreService.deleteEntireConsumerDetails(client.getId());
            Assert.assertEquals("Delete consumer delete", deleteConsumerResponse.status, true);
            // Delete LensSettings
            ApiResponse en_usDeleteLensSettingsResponse = coreService.deleteLensSettings(en_usLensSettings.getId());
            Assert.assertEquals("Delete en_us Lens settings delete", en_usDeleteLensSettingsResponse.status, true);
            ApiResponse en_ukDeleteLensSettingsResponse = coreService.deleteLensSettings(en_ukLensSettings.getId());
            Assert.assertEquals("Delete en_uk Lens settings delete", en_ukDeleteLensSettingsResponse.status, true);
            ApiResponse localeDeleteSettingsResponse = coreService.deleteLensSettings(localeSettings.getId());
            Assert.assertEquals("Delete locale settings delete", localeDeleteSettingsResponse.status, true);

           

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetAllResumeFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_GetAllResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals(
                    "ParseResume With ValidBinary TransactionCount"
                    + " GetAllResumeFields Check",
                    apiContext.getClientApi().getAllowedTransactions() - 1,
                    clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithValidBinaryResumeDataWithDefaultLocale() {
        try {
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry(TestHelper.Locale.en_us.name());
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            apiContext.setClient(coreClient);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);

            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithValidBinaryResumeDataLocaleBasedParsing() {
        try {
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.identifiedLocale",
                                    is("en_us")))
                    .andExpect(jsonPath("$.processedWithLocale",
                                    is("en_us")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithValidBinaryResumeDataLocaleBasedParsingWithDefaultCountry() {
        try {
            String fileName = "/TestResumes/16343.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("us");
            locale.setLanguage("en");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.identifiedLocale",
                                    is("en_gb")))
                    .andExpect(jsonPath("$.processedWithLocale",
                                    is("en_us")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithValidBinaryResumeDataLocaleBasedParsingWithUnknownLocale() {
        try {
            String fileName = "/TestResumes/UnknownLocale.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.identifiedLocale",
                                    is("xl_xx")))
                    .andExpect(jsonPath("$.processedWithLocale",
                                    is("en_us")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithValidBinaryResumeDataLocaleBasedParsingWithUnallocatedLocale() {
        try {
            String fileName = "/TestResumes/Resume_JPN.txt";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);
            coreClient.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClient.setEnableLocaleDetection(true);
            apiContext.setClient(coreClient);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.identifiedLocale",
                                    is("ja_jp")))
                    .andExpect(jsonPath("$.processedWithLocale",
                                    is("en_us")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResumeBGTXML Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeBGTXMLWithValidBinaryResumeData
     */
    @Test
    public void parseResumeWithValidBinaryResumeDataLocaleBasedParsingWithInactiveInstance() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // ParseResumeBGTXML
            com.bgt.lens.model.adminservice.request.Locale locale = new com.bgt.lens.model.adminservice.request.Locale();
            locale.setCountry("en_us");
            locale.setLanguage("*");
            CreateConsumerRequest.DefaultLocaleList defaultLocale = new CreateConsumerRequest.DefaultLocaleList();
            defaultLocale.getLocale().add(locale);

            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());
            coreClientApiContext.setDefaultLocale(_helper.getJsonStringFromObject(defaultLocale));
            coreClientApiContext.setEnableLocaleDetection(true);
            apiContext.setClient(coreClientApiContext);

            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setFieldList(fieldList);
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andDo(print());
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            ApiResponse apiResponse = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", apiResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetContactResumeFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_GetContactResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Contact");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("contact")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount GetContactResumeFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetStatementsResumeFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_GetStatementsResumeFields() {
        try {

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Statements");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("statements")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount GetStatementsResumeFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetSkillsFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_GetSkillsFields() {
        try {

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Skills");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);

            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("skills")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount GetSkillsFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetProfessionalFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_GetProfessionalFields() {
        try {

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Professional");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("professional")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount GetProfessionalFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetEducationFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_GetEducationFields() {
        try {

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Education");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("education")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount GetEducationFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetSummaryFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_GetSummaryFields() {
        try {

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Summary");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("summary")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount GetSummaryFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetExperienceFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_GetExperienceFields() {
        try {

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Experience");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("experience")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 
            
            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount GetExperienceFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetReferencesFields
     */
    // References
    @Test
    public void parseResumeWithValidBinaryResumeData_GetReferencesFields() {
        try {

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("References");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("references")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount GetReferencesFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_GetSkillRollUpFields
     */
    // SkillRollUp
    @Test
    public void parseResumeWithValidBinaryResumeData_GetSkillRollUpFields() {
        try {

            String fileName = "/TestResumes/129305.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("SkillRollUp");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("skillrollup")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount GetSkillRollUpFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_WithoutResumeFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_WithoutResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("EDRISH")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount WithoutResumeFields Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_WithInvalidResumeFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_WithInvalidResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            // Fields List
            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("Invalid");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                mockMvc.perform(post(UriConstants.ResumeParser)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(parseResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andDo(print());
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResume With ValidBinary ResumeData Invalid ResumeFields check", response.status, false);

            // ParseResume Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.INVALID_RESUME_FIELDLIST));
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary ResumeData Invalid ResumeFields TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_WithEmptyResumeFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_WithEmptyResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            // Fields List
            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add("");
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                mockMvc.perform(post(UriConstants.ResumeParser)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(parseResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andDo(print());
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResume With ValidBinary ResumeData Empty ResumeFields check", response.status, false);

            // ParseResume Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.INVALID_RESUME_FIELDLIST));
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary ResumeData Empty ResumeFields TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithValidBinaryResumeData_WithNullResumeFields
     */
    @Test
    public void parseResumeWithValidBinaryResumeData_WithNullResumeFields() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());

            // Fields List
            fieldList = new ParseResumeRequest.FieldList();
            List<String> fields = new ArrayList<>();
            fields.add(null);
            fieldList.getField().addAll(fields);
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                mockMvc.perform(post(UriConstants.ResumeParser)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(parseResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andDo(print());
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResume With ValidBinary ResumeData Null ResumeFields check", response.status, false);

            // ParseResume Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.INVALID_RESUME_FIELDLIST));
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary ResumeData Null ResumeFields TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithConvertError
     */
    @Test
    public void parseResumeWithConvertError() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            InputStream resourceAsStream04 = ParseResumeTest.class.getResourceAsStream(fileName);
            try {
                if (resourceAsStream04 != null) {
                    binaryData = _helper.encodeBase64String(testHelper.getBinaryData(resourceAsStream04));
                    resourceAsStream04.close();
                }
            } catch (IOException ex) {
                resourceAsStream04.close();
                throw ex;
            }
            //binaryData = _helper.encodeBase64String(testHelper.getBinaryData(ParseResumeBGTXMLTests.class.getResourceAsStream(fileName)));
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setFieldList(fieldList);

            String expResult = "Converters returned empty output";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(apiErrors.getLensErrorMessage(expResult))))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With Empty BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeWithConvertErrorStatus
     */
    @Test
    public void parseResumeWithConvertErrorStatus() {
        try {
            String fileName = "/TestResumes/EmptyResume/ExternalResume.doc";
            binaryData = null;
            extension = fileName.substring(fileName.indexOf('.') + 1);
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setFieldList(fieldList);
            ApiResponse response = new ApiResponse();
            try {
                mockMvc.perform(post(UriConstants.ResumeParser)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(testHelper.
                                convertObjectToJsonBytes(parseResumeRequest))
                        .with(new RequestPostProcessor() {
                            @Override
                            public MockHttpServletRequest postProcessRequest(
                                    final MockHttpServletRequest request) {
                                        request.setAttribute("ApiContext",
                                                apiContext);
                                        return request;
                                    }
                        }))
                        .andExpect(content()
                                .contentTypeCompatibleWith(
                                        MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.status", is(true)))
                        .andExpect(jsonPath("$.statusCode", is("OK")))
                        .andExpect(jsonPath("$.requestId", notNullValue()))
                        .andExpect(jsonPath("$.timeStamp", notNullValue()))
                        .andDo(print());
            } catch (Exception ex) {
                response.status = false;
                response.responseData = ex.getMessage();
            }
            Assert.assertEquals("ParseResume With Null BinaryData check", response.status, false);

            // ParseResume Assert
            String convertResume = (String) response.responseData;
            boolean checkContains = convertResume.contains(_helper.getErrorMessageWithURL(ApiErrors.EMPTY_BINARY_DATA));
            assertTrue(checkContains);
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With Null BinaryData TransactionCount Check", apiContext.getClientApi().getAllowedTransactions(), clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeSpecificLensInstance
     */
    @Test
    public void parseResumeSpecificLensInstance() {
        try {
            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_us);
            parseResumeRequest.setFieldList(fieldList);
            String expResultResDoc = "ResDoc";
            String expResultResume = "resume";
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(true)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResDoc)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(expResultResume)))
                    .andExpect(jsonPath("$.responseData",
                                    containsString("EDRISH")))
                    .andDo(print());
            
            ClientapiCriteria apiCriteria = new ClientapiCriteria();
            apiCriteria.setCoreApi(apiContext.getClientApi().getCoreApi());
            List<CoreClientapi> clientapis = dbService.getClientApiByCriteria(apiCriteria); 

            // ParseResume Transaction Count Assert
            Assert.assertEquals("ParseResume With ValidBinary TransactionCount Check", apiContext.getClientApi().getAllowedTransactions() - 1, clientapis.get(0).getRemainingTransactions());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeUnauthorizedInstance
     */
    @Test
    public void parseResumeUnauthorizedInstance() {
        try {
            // ParseResume
            String localeType = "en_anz";
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(localeType);
            parseResumeRequest.setFieldList(fieldList);
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE))))
                    .andDo(print());

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeEmptyInstanceList
     */
    @Test
    public void parseResumeEmptyInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();

            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setFieldList(fieldList);
            try{
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE))))
                    .andDo(print());
            }
            catch(NestedServletException ex){
                Assert.assertTrue(true);
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeNullInstanceList
     */
    @Test
    public void parseResumeNullInstanceList() {
        try {
            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(1);
            coreClientApiContext.setClientKey("Sample");
            coreClientApiContext.setSecret("Sample");
            coreClientApiContext.setDumpStatus(true);
            coreClientApiContext.setCoreClientlenssettingses(null);

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();

            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setFieldList(fieldList);
            try{
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE))))
                    .andDo(print());
            }
            catch(NestedServletException ex){
                Assert.assertTrue(true);
            }
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeInvalidLensList
     */
    @Test
    public void parseResumeInvalidLensList() {
        try {
            //Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost("Invalid");
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(testHelper.status);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();

            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(TestHelper.Locale.en_us.name());
            parseResumeRequest.setLocale(LocaleTypeEn_us);
            parseResumeRequest.setFieldList(fieldList);
            LensRepository repository = new LensRepository();
            String errorMessage = repository.lensErrorMessage;
            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(apiErrors.LENS_SERVICE_UNAVAILABLE))))
                    .andDo(print());
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            ApiResponse apiResponse = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", apiResponse.status, true);

        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }

    /**
     * *
     * ParseResumeInActiveLensList
     */
    @Test
    public void parseResumeInActiveLensList() {
        try {
            // Add Lens settings
            AddLensSettingsRequest settingsRequest = new AddLensSettingsRequest();
            settingsRequest.setHost(testHelper.hostname);
            settingsRequest.setPort(new BigInteger(Integer.toString(testHelper.port)));
            settingsRequest.setCharSet(testHelper.encoding);
            settingsRequest.setTimeOut(new BigInteger(Integer.toString(testHelper.timeOut)));
            settingsRequest.setInstanceType(testHelper.instanceType);
            settingsRequest.setLocale(testHelper.locale);
            settingsRequest.setStatus(false);
            settingsRequest.setVersion(testHelper.version);
            ApiResponse lensSettingsResponse = coreService.addLensSettings(settingsRequest);
            Assert.assertEquals("Add Lens settings check", lensSettingsResponse.status, true);
            LensSettings settings = (LensSettings) lensSettingsResponse.responseData;

            // Update the client
            UpdateConsumerRequest updaterequest = _testhelper.getUpdateConsumerRequest("/input/UpdateConsumer.xml");
            updaterequest = _testhelper.updateLensSettings(updaterequest, settings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            ApiResponse updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            Assert.assertEquals("Update consumer check", updateResponse.status, true);
            client = (Client) updateResponse.responseData;

            // Create CoreClient
            CoreClient coreClientApiContext = new CoreClient();
            coreClientApiContext.setId(client.getId());
            coreClientApiContext.setClientKey(client.getKey());
            coreClientApiContext.setSecret(client.getSecret());
            coreClientApiContext.setDumpStatus(client.isDumpStatus());

            // Add ApiConext
            apiContext = new ApiContext();
            apiContext.setRequestId(UUID.randomUUID());
            apiContext.setClient(coreClientApiContext);
            apiContext.setAdvanceLog(new AdvanceLog());
            _helper = new Helper();

            // ParseResume
            ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
            parseResumeRequest.setInstanceType(InstanceType);
            parseResumeRequest.setBinaryData(binaryData);
            parseResumeRequest.setExtension(extension);
            parseResumeRequest.setLocale(LocaleTypeEn_us);
            parseResumeRequest.setFieldList(fieldList);

            mockMvc.perform(post(UriConstants.ResumeParser)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(testHelper.
                            convertObjectToJsonBytes(parseResumeRequest))
                    .with(new RequestPostProcessor() {
                        @Override
                        public MockHttpServletRequest postProcessRequest(
                                final MockHttpServletRequest request) {
                                    request.setAttribute("ApiContext",
                                            apiContext);
                                    return request;
                                }
                    }))
                    .andExpect(content()
                            .contentTypeCompatibleWith(
                                    MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.status", is(false)))
                    .andExpect(jsonPath("$.statusCode", is("OK")))
                    .andExpect(jsonPath("$.requestId", notNullValue()))
                    .andExpect(jsonPath("$.timeStamp", notNullValue()))
                    .andExpect(jsonPath("$.responseData",
                                    containsString(_helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS))))
                    .andDo(print());
            updaterequest = _testhelper.updateLensSettings(updaterequest, en_usLensSettings.getId());
            updaterequest = _testhelper.updateApiandResource(updaterequest, api.getId(), api.getKey(), resource.getResourceId().toString());
            updaterequest.setClientId(new BigInteger(Integer.toString(client.getId())));
            updateResponse = coreService.updateConsumer(client.getId(), updaterequest);
            ApiResponse apiResponse = coreService.deleteLensSettings(settings.getId());
            Assert.assertEquals("Create Lens settings delete", apiResponse.status, true);
        } catch (Exception ex) {
            System.out.print(ex);
            Assert.fail();
        }
    }
}
