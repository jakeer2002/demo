'Comcast Cable Communications
Comm Tech 1 - 3, Anne Arundel, Charles and Calvert Counties
Millersville MD  US
Position Summary Under limited supervision the CommTech 1 performs routine disconnects, pre-wires, change of service, service calls, cable leakage monitoring and log and MDU wiring for residential and business customers in accordance with Comcast procedures and practices. Major Duties: (May perform any or all of the following duties) Perform requested and non-pay disconnects and changes of service while adhering to Comcast procedures and safe work practices, NEC and NESC requirements, and local ordinances in order to provide or remove requested services. Pre-wire single dwelling units and multiple dwelling units in order to provide  ready hook-up  capabilities. Perform entry level service calls. Troubleshoot the drop from the tap to the customer s equipment. Monitor for cable leakage and log. Repair and/or replace damaged aerial/underground plant. Install and remove converters in order to provide customers with upgrades or downgrades in service. Clean, maintain and stock vehicle and equipment in order to be prepared to perform required duties. Inspect existing ground or make new ground according to the National Electrical Code (NEC) in order to protect employees, customers, and equipment from electrical shock or damage. Complete associated paperwork with each work order in a timely manner in order to ensure all details of the work are recorded for entry in the customer s account once the work is checked in. Determine acceptability of service by reviewing picture quality following installation of cable service in order to provide the best possible service for the customer. Repair and/or replace damaged aerial/underground plant. Provide customer with materials regarding channel line up, use of converter and company policies as they related to the customer as well ad demonstrate these skills in order to educate the customer on the use of the equipment and company guidelines. Properly operate and maintain installation tools and equipment. Report need for vehicle repair or service when required and/or prescribed and complete IUSP forms. Report any accidents, losses, injuries or property damage to supervisor and customer when appropriate. Perform other duties as requested by supervisor in order to achieve departmental goals and objectives. Punctual, regular, and consistent attendance required.
Minimum Requirements
Preferred Qualifications:
Basic wiring experience.
Knowledge of cable tv products and services.
Knowledge of basic mathematics.
Entry level knowledge of the workings of computer modems.
Ability to communicate with customers in a clear and straight forward manner.
Ability to work independently.
Ability to prioritize and organize effectively.
Ability to apply common sense, theory and experience to decision-making; able to recognize similarities between past and present situations; able to identify key issues or use inductive reasoning in complex situations.
Education/Requirements:
High School Diploma or equivalent and a valid drivers license with satisfactory driving record.
Experience:
Minimum 12 months or the recognized equivalent in work experience or a combination of work experience and education.
Training:
In order to perform as a CommTech 1 level; an employee will be given training and instruction that will assist with progression to the next CommTech level.
Working Conditions:
Ability to use basic cable installation tools and hand tools.
Ability
to perform job from high places (on poles and roofs);
climb poles with proper equipment (safety belt, strap, climbers), ladders and bucket trucks, 18 to 20 feet, as determined by the system s requirements.
Lift
and carry loads of 70 lbs or more.
Work in confined spaces such as crawl spaces or attics.
Work while standing 50 -70% of the time.
Perform work near power lines and electricity.
Manipulate connectors, fasteners, wire; use hand tools.
Drive company vehicle in performance of duties in a safe and responsible manner.
Work and travel in inclement weather.
Vision ability: close vision, peripheral vision and ability to adjust focus.
Ability to work overtime including weekends, evenings and holidays as needed.
Exposure to moderate noise level.
Exposure to dogs or other animals, construction areas, or the public in general, may be a concern.
Exit Point -Upon completion of 90 days (maximum) of service, an employee must complete the certification required for CommTech 2.
The employee must successfully complete the training programs as described on this Job Description and pass the CT-2 certification exams.
In order to be certified as a CommTech 1, the employee must
Pass the Written Assessment for CT-1,
Pass the Skills Assessment for CT-1, Meet Required Performance Standards Standards, Meet time in grade (Certification to CommTech 2 must take place within the first 90 days from date of hire.),Acquire
Supervisor approval;
After the employee is certified as a CommTech 2, training shall begin as described on the CommTech 3 Job Description.
Comcast is an'
