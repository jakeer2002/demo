<!--
    ************************************************************
    Copyright 2011, Burning Glass Technologies
    XSL for translating a BGTXML Resume to HR-XML  3.0 Standards
    Default United Kingdom - Great Britain
    ************************************************************
	Change Log:
	
11/07/2011 NIA Created HR-XML 3.0 Standard for Candidate Resume [ Developed based upon Candidate.xsd Schema ]
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.hr-xml.org/3" xmlns:oa="http://www.openapplications.org/oagis/9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0">
    <xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes" encoding="utf-8"/>
    <xsl:preserve-space elements="*"/>
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
    <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
    <xsl:variable name="nkType" select="'notKnown'"/>
    <xsl:variable name="naType" select="'notApplicable'"/>
    <xsl:variable name="countryCode" select="'GB'"/>
    <!-- Resume Transformation Part -->
    <xsl:template match="/">
        <Candidate>			
            <xsl:apply-templates select="//resume"/>
        </Candidate>
    </xsl:template>
    <!--************************* Resume Template Declaration *** Mapping of BGT xml tags to HRXML tags********************************-->
    <xsl:template match="resume">
        <CandidatePerson>
            <PersonName>
                <xsl:apply-templates select="(contact/name)[1]"/>
            </PersonName>
            <FatherName>
                <xsl:if test="count(contact/name/surname)> 0">
                    <FormattedName>
                        <xsl:value-of select="contact/name/surname[1]"/>
                    </FormattedName>
                </xsl:if>
            </FatherName>
            <Communication>
                <xsl:if test="count(contact/address)>0">
                    <Address>
                        <xsl:apply-templates select="contact[1]/address[1]" mode="AddressSummary"/>
                    </Address>
                </xsl:if>
            </Communication>
            <xsl:apply-templates select="//contact/phone"/>
            <xsl:if test="count(//contact/email) > 0">
                <Communication>
                    <ChannelCode>Email</ChannelCode>
                    <UseCode>personal</UseCode>
                    <oa:URI>
                        <xsl:value-of select="//contact/email"/>
                    </oa:URI>
                </Communication>
            </xsl:if>
            <xsl:if test="count(//contact/website) > 0">
                <Communication>
                    <ChannelCode>Website</ChannelCode>
                    <UseCode>personal</UseCode>
                    <oa:URI>
                        <xsl:value-of select="//contact/website"/>
                    </oa:URI>
                </Communication>
            </xsl:if>
            <Visa>
                <xsl:if test="count(statements/eligibility/visa) > 0">
                    <UserArea>
                        <VisaDescription>
                            <xsl:value-of select="statements/eligibility/visa"/>
                        </VisaDescription>
                    </UserArea>
                </xsl:if>
            </Visa>
            <CitizenshipCountryCode>
                <xsl:call-template name="countrycode">
                    <xsl:with-param name="code" select="translate(contact/personal/citizenship,$lowercase,$uppercase)"/>
                </xsl:call-template>
            </CitizenshipCountryCode>
            <ResidencyCountryCode>
                <xsl:call-template name="countrycode">
                    <xsl:with-param name="code" select="translate(contact/address/country,$lowercase,$uppercase)"/>
                </xsl:call-template>
            </ResidencyCountryCode>
            <xsl:if test="count(contact/personal/pob) > 0">
                <BirthPlace>
                    <xsl:value-of select="contact/personal/pob"/>
                </BirthPlace>
            </xsl:if>
            <xsl:if test="count(contact/personal/dob/@iso8601) > 0">
                <BirthDate>
                    <xsl:value-of select="contact/personal/dob/@iso8601"/>
                </BirthDate>
            </xsl:if>
            <MaritalStatusCode>
                <xsl:choose>
                    <xsl:when test="count(contact/personal/maritalstatus) > 0">
                        <xsl:value-of select="contact/personal/maritalstatus"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>notReported</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </MaritalStatusCode>
        </CandidatePerson>
        <!--******************************End of Candidate Person**********************************-->
        <!--***********************************************************************************************************************************************************************************-->
        <CandidateProfile>
            <ProfileName>
                <xsl:if test="count(summary) > 0 and string-length(summary) > 0">
                    <xsl:value-of select="summary"/>
                </xsl:if>
            </ProfileName>
            <CandidateObjective>
                <xsl:if test="count(objective) > 0 and string-length(objective) > 0">
                    <xsl:value-of select="objective"/>
                </xsl:if>
            </CandidateObjective>
            <ExperienceSummary>
                <xsl:if test="count(//experience/description) > 0 or string-length(//experience/description) > 0">
                    <ExperienceCategory>
                        <oa:Description>
                            <xsl:value-of select="//experience/description"/>
                        </oa:Description>
                    </ExperienceCategory>
                </xsl:if>
            </ExperienceSummary>
            <EmploymentHistory>
                <xsl:apply-templates select="experience"/>
            </EmploymentHistory>
            <EducationHistory>
                <xsl:apply-templates select="education"/>
            </EducationHistory>
            <xsl:if test="count(statements/eligibility)>0">
                <SecurityCredentials>
                    <SecurityCredential>
                        <CredentialName>Eligibility</CredentialName>
                        <oa:Description>
                            <xsl:if test="count(statements/eligibility/visa) > 0">
                                <xsl:text>Visa :</xsl:text>
                            </xsl:if>
                            <xsl:apply-templates select="statements/eligibility"/>
                        </oa:Description>
                    </SecurityCredential>
                </SecurityCredentials>
            </xsl:if>
            <xsl:if test="count(skills/courses)>0">
                <Licenses>
                    <License>
                        <oa:Description>
                            <xsl:apply-templates select="skills/courses"/>
                        </oa:Description>
                    </License>
                </Licenses>
            </xsl:if>
            <xsl:if test="count(professional/courses)>0">
                <Certifications>
                    <Certification>
                        <oa:Description>
                            <xsl:apply-templates select="professional/courses"/>
                        </oa:Description>
                    </Certification>
                </Certifications>
            </xsl:if>
            <xsl:if test="count(statements/honors)>0">
                <PatentHistory>
                    <Patent>
                        <oa:Description>
                            <xsl:apply-templates select="statements/honors"/>
                        </oa:Description>
                    </Patent>
                </PatentHistory>
            </xsl:if>
            <xsl:if test="count(professional/publications)>0">
                <PublicationHistory>
                    <Publication>
                        <FormattedPublicationDescription>
                            <xsl:apply-templates select="professional/publications"/>
                        </FormattedPublicationDescription>
                    </Publication>
                </PublicationHistory>
            </xsl:if>
            <SpeakingHistory>
                <xsl:if test="count(skills/languages) > 0 or string-length(skills/description) > 0">
                    <UserArea>
                        <Description>
                            <xsl:apply-templates select="skills/languages"/>
                            <xsl:apply-templates select="skills/description"/>
                        </Description>
                    </UserArea>
                </xsl:if>
            </SpeakingHistory>
            <xsl:if test="count(skills/skills|professional/skills)>0">
                <PersonQualifications>
                    <QualificationsSummary>
                        <xsl:apply-templates select="skills/skills"/>
                        <xsl:value-of select="professional/skills"/>
                    </QualificationsSummary>
                </PersonQualifications>
            </xsl:if>
            <xsl:if test="(count(professional/affiliations) > 0)">
                <OrganizationAffiliations>
                    <OrganizationAffiliation>
                        <oa:Description>
                            <xsl:apply-templates select="professional"/>
                        </oa:Description>
                    </OrganizationAffiliation>
                </OrganizationAffiliations>
            </xsl:if>
            <xsl:if test="count(statements/references)>0">
                <EmploymentReferences>
                    <UserArea>
                        <Description>
                            <xsl:apply-templates select="statements/references"/>
                        </Description>
                    </UserArea>
                </EmploymentReferences>
            </xsl:if>
            <!--**************************User Area other Descriptions over here*************-->
            <UserArea>
                <xsl:apply-templates select="../skillrollup"/>
            </UserArea>
        </CandidateProfile>
    </xsl:template>
    <!-- CandidatePerson / PersonName Definition -->
    <xsl:template match="name">
        <FormattedName>
            <xsl:value-of select="honorific"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="givenname[1]"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="surname[1]"/>
        </FormattedName>
        <LegalName>
            <xsl:value-of select="givenname[1]"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="surname[1]"/>
        </LegalName>
        <!--GivenName-->
        <xsl:apply-templates select="givenname[1]" mode="first"/>
        <!--PreferredName-->
        <xsl:apply-templates select="nickname[1]"/>
        <xsl:if test="count(givenname) > 1">
            <MiddleName>
                <xsl:apply-templates select="givenname[position()!=1]" mode="middle"/>
            </MiddleName>
        </xsl:if>
        <!--FamilyName-->
        <xsl:apply-templates select="surname[1]"/>
        <!--Salution Code Mr. Mrs.-->
        <PreferredSalutationCode>
            <xsl:if test="count(honorific) > 0 ">
                <xsl:apply-templates select="honorific[1]"/>
            </xsl:if>
        </PreferredSalutationCode>
        <!--Type of Aristocratic title-->
        <GenerationAffixCode>
            <xsl:if test="count(namesuffix[1]) > 0">
                <xsl:apply-templates select="namesuffix[1]"/>
            </xsl:if>
        </GenerationAffixCode>
        <PersonNameInitials>
            <xsl:value-of select="substring(givenname[1], 1, 1)"/>
        </PersonNameInitials>
    </xsl:template>
    <xsl:template match="givenname" mode="first">
        <oa:GivenName>
            <xsl:if test="count(.) > 0">
                <xsl:value-of select="."/>
            </xsl:if>
        </oa:GivenName>
    </xsl:template>
    <xsl:template match="nickname">
        <PreferredName>
            <xsl:if test="count(.) > 0">
                <xsl:value-of select="."/>
            </xsl:if>
        </PreferredName>
    </xsl:template>
    <xsl:template match="namesuffix">
        <xsl:value-of select="."/>
    </xsl:template>
    <xsl:template match="honorific">
        <xsl:value-of select="."/>
    </xsl:template>
    <xsl:template match="surname[1]">
        <FamilyName>
            <xsl:if test="count(.) > 0">
                <xsl:value-of select="."/>
            </xsl:if>
        </FamilyName>
    </xsl:template>
    <!--**********************************************************************************************************************************************************************************-->
    <!--***************************************************Education History Template Declaration*********************************-->
    <xsl:template match="education">
        <xsl:apply-templates select="school"/>
    </xsl:template>
    <!--********************************************************School Template Declaration*****************************************-->
    <xsl:template match="school">
        <xsl:choose>
            <xsl:when test="count(degree)>0">
                <xsl:apply-templates select="degree"/>
            </xsl:when>
            <xsl:otherwise>
                <EducationOrganizationAttendance>
                    <OrganizationName>
                        <xsl:if test="count(institution) > 0 and string-length(institution) > 0">
                            <xsl:apply-templates select="institution"/>
                        </xsl:if>
                    </OrganizationName>
                    <OrganizationContact>
                        <Communication>
                            <xsl:if test="count(address)>0">
                                <Address>
                                    <xsl:apply-templates select="address[1]" mode="AddressSummary"/>
                                </Address>
                            </xsl:if>
                        </Communication>
                    </OrganizationContact>
                    <xsl:if test="count(degree/@name) > 0">
                        <ProgramName>
                            <xsl:value-of select="degree/@name"/>
                        </ProgramName>
                    </xsl:if>
                    <xsl:choose>
                        <xsl:when test="higheredu">
                            <EducationLevelCode>
                                <xsl:text>highschool</xsl:text>
                            </EducationLevelCode>
                        </xsl:when>
                        <xsl:otherwise>
                            <EducationLevelCode>
                                <xsl:text>trade</xsl:text>
                            </EducationLevelCode>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="count(honors) > 0">
                        <HonorsProgramName>
                            <xsl:value-of select="honors"/>
                        </HonorsProgramName>
                    </xsl:if>
                    <xsl:variable name="NoOfDateRanges" select="count(daterange)"/>
                    <xsl:if test="$NoOfDateRanges>0">
                        <xsl:choose>
                            <xsl:when test="$NoOfDateRanges = 1">
                                <AttendancePeriod>
                                    <xsl:variable name="NoOfStartDates" select="count(daterange/start)"/>
                                    <xsl:choose>
                                        <xsl:when test="$NoOfStartDates>0">
                                            <xsl:apply-templates select="(daterange/start)[1]"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <StartDate>
                                                <DateText>
                                                    <xsl:value-of select="$nkType"/>
                                                </DateText>
                                            </StartDate>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <xsl:apply-templates select="(daterange/end)[1]"/>
                                </AttendancePeriod>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="higheredu and secondaryedu">
                                        <xsl:apply-templates select="daterange[1]" mode="education"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:apply-templates select="daterange" mode="education"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                    <xsl:if test="$NoOfDateRanges=0">
                        <xsl:choose>
                            <xsl:when test="higheredu and secondaryedu and count(completiondate)>0">
                                <xsl:apply-templates select="completiondate[1]" mode="education"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:apply-templates select="completiondate" mode="education"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                    <EducationScore>
                        <xsl:for-each select="gpa">
                            <ScoreText>
                                <xsl:attribute name="scoreTextCode">
                                    <xsl:text>GPA</xsl:text>
                                </xsl:attribute>
                                <xsl:attribute name="maximumScoreText">
                                    <xsl:value-of select="@max"/>
                                </xsl:attribute>
                                <xsl:value-of select="."/>
                            </ScoreText>
                        </xsl:for-each>
                    </EducationScore>
                    <EducationDegree>
                        <xsl:choose>
                            <xsl:when test="degree">
                                <DegreeName>
                                    <xsl:value-of select="degree"/>
                                </DegreeName>
                            </xsl:when>
                            <xsl:when test="higheredu">
                                <DegreeName>
                                    <xsl:value-of select="higheredu"/>
                                </DegreeName>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:for-each select="major">
                            <DegreeMajor>
                                <ProgramName>
                                    <xsl:value-of select="."/>
                                </ProgramName>
                            </DegreeMajor>
                        </xsl:for-each>
                        <xsl:for-each select="minor">
                            <DegreeMinor>
                                <ProgramName>
                                    <xsl:value-of select="."/>
                                </ProgramName>
                            </DegreeMinor>
                        </xsl:for-each>
                    </EducationDegree>
                    <xsl:choose>
                        <xsl:when test="count(description)>0">
                            <UserArea>
                                <Description>
                                    <xsl:apply-templates select="description"/>
                                    <xsl:if test="count(courses)>0">
                                        <xsl:for-each select="courses">
                                            <xsl:text>
                                        
                                            </xsl:text>
                                            <xsl:apply-templates select="."/>
                                        </xsl:for-each>
                                    </xsl:if>
                                </Description>
                            </UserArea>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="count(courses)>0">
                                <UserArea>
                                    <Description>
                                        <xsl:for-each select="courses">
                                            <xsl:text>
                                        
                                            </xsl:text>
                                            <xsl:apply-templates select="."/>
                                        </xsl:for-each>
                                    </Description>
                                </UserArea>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                </EducationOrganizationAttendance>
                <xsl:choose>
                    <xsl:when test="count(secondaryedu)> 0">
                        <EducationOrganizationAttendance>
                            <OrganizationName>
                                <xsl:if test="count(institution) > 0 and string-length(institution) > 0">
                                    <xsl:apply-templates select="institution"/>
                                </xsl:if>
                            </OrganizationName>
                            <EducationLevelCode>
                                <xsl:text>highschool</xsl:text>
                            </EducationLevelCode>
                            <EducationDegree>
                                <DegreeName>
                                    <xsl:value-of select="secondaryedu"/>
                                </DegreeName>
                                <xsl:choose>
                                    <xsl:when test="count(daterange)>1">
                                        <AttendancePeriod>
                                            <xsl:apply-templates select="daterange[2]" mode="education"/>
                                        </AttendancePeriod>
                                    </xsl:when>
                                    <xsl:when test="count(completiondate)>1">
                                        <AttendancePeriod>
                                            <xsl:apply-templates select="completiondate[2]" mode="education"/>
                                        </AttendancePeriod>
                                    </xsl:when>
                                </xsl:choose>
                            </EducationDegree>
                        </EducationOrganizationAttendance>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="count(school)=0">
                            <EducationOrganizationAttendance>
                                <EducationLevelCode>
                                    <xsl:text>trade</xsl:text>
                                </EducationLevelCode>
                            </EducationOrganizationAttendance>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!--**************************************************Experience Template Declaration*******************************************-->
    <xsl:template match="experience">
        <xsl:apply-templates select="job"/>
    </xsl:template>
    <!--******************************************************Job Template Declaration***********************************************-->
    <xsl:template match="job">
        <EmployerHistory>
            <OrganizationName>
                <xsl:if test="count(employer) > 0 ">
                    <xsl:value-of select="employer"/>
                </xsl:if>
            </OrganizationName>
            <OrganizationContact>
                <Communication>
                    <xsl:if test="count(address)>0">
                        <Address>
                            <xsl:apply-templates select="address[1]" mode="AddressSummary"/>
                        </Address>
                    </xsl:if>
                </Communication>
            </OrganizationContact>
            <IndustryCode>
                <xsl:if test="count(@sic) > 0 and string-length(@sic) > 0">
                    <xsl:value-of select="@sic"/>
                </xsl:if>
            </IndustryCode>
            <!--*************************Postion History Starts************************-->
            <PositionHistory>
                <PositionTitle>
                    <xsl:choose>
                        <xsl:when test="count(title) > 0">
                            <xsl:value-of select="title"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>notKnown</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </PositionTitle>
                <OrganizationUnitName>
                    <xsl:choose>
                        <xsl:when test="count(employer) > 0">
                            <xsl:value-of select="employer"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>notKnown</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </OrganizationUnitName>
                <EmploymentPeriod>
                    <xsl:variable name="NoOfDateRanges" select="count(daterange)"/>
                    <xsl:if test="$NoOfDateRanges=0">
                        <StartDate>
                            <DateText>
                                <xsl:value-of select="$nkType"/>
                            </DateText>
                        </StartDate>
                        <EndDate>
                            <DateText>
                                <xsl:value-of select="$nkType"/>
                            </DateText>
                        </EndDate>
                    </xsl:if>
                    <xsl:choose>
                        <xsl:when test="$NoOfDateRanges >= 1">
                            <xsl:variable name="NoOfStartDates" select="count(daterange/start)"/>
                            <xsl:choose>
                                <xsl:when test="$NoOfStartDates>0">
                                    <xsl:apply-templates select="(daterange/start)[1]"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <StartDate>
                                        <DateText>
                                            <xsl:value-of select="$nkType"/>
                                        </DateText>
                                    </StartDate>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:apply-templates select="(daterange/end)[1]"/>
                        </xsl:when>
                    </xsl:choose>
                </EmploymentPeriod>
                <xsl:if test="count(description)=0">
                    <oa:Description/>
                </xsl:if>
                <xsl:if test="count(description)>0">
                    <oa:Description>
                        <xsl:value-of select="description"/>
                        <xsl:text>   </xsl:text>
                        <xsl:if test="count(skills)>0">
                            <xsl:text>SKILLS DESCRIPTION:</xsl:text>
                            <xsl:value-of select="skills"/>
                        </xsl:if>
                        <xsl:if test="count(courses)>0">
                            <xsl:text>COURSES DESCRIPTION:</xsl:text>
                            <xsl:value-of select="courses"/>
                        </xsl:if>
                    </oa:Description>
                </xsl:if>
                <xsl:variable name="jobPos" select="jobtype"/>
                <JobCategoryCode>
                    <xsl:choose>
                        <xsl:when test="$jobPos='temp' or $jobPos='temporary'">
                            <xsl:text>temp</xsl:text>
                        </xsl:when>
                        <xsl:when test="$jobPos='contract' or $jobPos='part time' or $jobPos='free lance' or contains(translate($jobPos,$lowercase,$uppercase), 'CONTRACT')">
                            <xsl:text>contract</xsl:text>
                        </xsl:when>
                        <xsl:when test="$jobPos='permanent' or $jobPos=' full time' or $jobPos='casual'">
                            <xsl:text>directHire</xsl:text>
                        </xsl:when>
                        <xsl:when test="$jobPos='intern'">
                            <xsl:text>internship</xsl:text>
                        </xsl:when>
                        <xsl:when test="$jobPos='volunteer'">
                            <xsl:text>volunteer</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>notReported</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </JobCategoryCode>
                <xsl:if test="count(department)>0">
                    <UserArea>
                        <JobCategoryDescription>
                            <xsl:if test="count(department) > 0 and string-length(department) > 0">
                                <xsl:value-of select="department"/>
                            </xsl:if>
                        </JobCategoryDescription>
                    </UserArea>
                </xsl:if>
            </PositionHistory>
        </EmployerHistory>
    </xsl:template>
    <!--****************************Postal Address Mode = "Address Summary"**************************-->
    <xsl:template match="address" mode="AddressSummary">
        <xsl:choose>
            <xsl:when test="count(street)>0">
                <oa:AddressLine>
                    <xsl:value-of select="street"/>
                </oa:AddressLine>
            </xsl:when>
            <xsl:otherwise>
                <oa:AddressLine>
                    <xsl:value-of select="text()"/>
                </oa:AddressLine>
            </xsl:otherwise>
        </xsl:choose>
        <oa:CityName>
            <xsl:if test="count(city) > 0">
                <xsl:value-of select="city"/>
            </xsl:if>
            <xsl:if test="count(town) > 0">
                <xsl:value-of select="town"/>
            </xsl:if>
        </oa:CityName>
        <xsl:if test="count(state) > 0">
            <oa:CountrySubDivisionCode listID="State">
                <xsl:value-of select="state"/>
            </oa:CountrySubDivisionCode>
        </xsl:if>
        <xsl:if test="count(county) > 0">
            <oa:CountrySubDivisionCode listID="County">
                <xsl:value-of select="county"/>
            </oa:CountrySubDivisionCode>
        </xsl:if>
        <xsl:if test="count(country)=0">
            <CountryCode>
                <xsl:value-of select="$countryCode"/>
            </CountryCode>
        </xsl:if>
        <xsl:if test="count(country)>0">
            <CountryCode>
                <xsl:call-template name="countrycode">
                    <xsl:with-param name="code" select="translate(country,$lowercase,$uppercase)"/>
                </xsl:call-template>
            </CountryCode>
        </xsl:if>
        <xsl:if test="count(postalcode)>0">
            <xsl:apply-templates select="postalcode[1]"/>
        </xsl:if>
    </xsl:template>
    <!--********************************Start Date Template Declaration************************************-->
    <xsl:template match="start">
        <StartDate>
            <xsl:if test="count(@iso8601) > 0 or string-length(@iso8601) > 0">
                <FormattedDateTime>
                    <xsl:value-of select="@iso8601"/>
                </FormattedDateTime>
            </xsl:if>
            <xsl:if test="not(@iso8601)">
                <DateText>
                    <xsl:value-of select="$nkType"/>
                </DateText>
            </xsl:if>
        </StartDate>
    </xsl:template>
    <!--********************************End Date Template Declaration************************************-->
    <xsl:template match="end">
        <EndDate>
            <xsl:choose>
                <xsl:when test="@days='present'">
                    <DateText>current</DateText>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="count(@ios8601) > 0 or string-length(@iso8601) > 0">
                        <FormattedDateTime>
                            <xsl:value-of select="@iso8601"/>
                        </FormattedDateTime>
                    </xsl:if>
                    <xsl:if test="not(@iso8601)">
                        <DateText>
                            <xsl:value-of select="$nkType"/>
                        </DateText>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </EndDate>
    </xsl:template>
    <!--***************************************Postal Code*****************************************************-->
    <xsl:template match="postalcode">
        <xsl:choose>
            <xsl:when test="@inferred-zip">
                <oa:PostalCode>
                    <xsl:value-of select="@inferred-zip"/>
                </oa:PostalCode>
            </xsl:when>
            <xsl:otherwise>
                <oa:PostalCode>
                    <xsl:value-of select="."/>
                </oa:PostalCode>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- ***************************************Country Code Template Definition********************************************************* -->
    <xsl:template name="countrycode">
        <xsl:param name="code"/>
        <xsl:choose>
            <xsl:when test="string-length($code)=2">
                <xsl:choose>
                    <xsl:when test="$code='GB'">
                        <xsl:value-of select="$countryCode"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$code"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$code='AFGHANISTAN'">
                        <xsl:value-of select="'AF'"/>
                    </xsl:when>
                    <xsl:when test="$code='ALAND ISLANDS'">
                        <xsl:value-of select="'AX'"/>
                    </xsl:when>
                    <xsl:when test="$code='ALBANIA'">
                        <xsl:value-of select="'AL'"/>
                    </xsl:when>
                    <xsl:when test="$code='ALGERIA'">
                        <xsl:value-of select="'DZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='AMERICAN SAMOA'">
                        <xsl:value-of select="'AS'"/>
                    </xsl:when>
                    <xsl:when test="$code='ANDORRA'">
                        <xsl:value-of select="'AD'"/>
                    </xsl:when>
                    <xsl:when test="$code='ANGOLA'">
                        <xsl:value-of select="'AO'"/>
                    </xsl:when>
                    <xsl:when test="$code='ANGUILLA'">
                        <xsl:value-of select="'AI'"/>
                    </xsl:when>
                    <xsl:when test="$code='ANTARCTICA'">
                        <xsl:value-of select="'AQ'"/>
                    </xsl:when>
                    <xsl:when test="$code='ANTIGUA AND BARBUDA'">
                        <xsl:value-of select="'AG'"/>
                    </xsl:when>
                    <xsl:when test="$code='ARGENTINA'">
                        <xsl:value-of select="'AR'"/>
                    </xsl:when>
                    <xsl:when test="$code='ARMENIA'">
                        <xsl:value-of select="'AM'"/>
                    </xsl:when>
                    <xsl:when test="$code='ARUBA'">
                        <xsl:value-of select="'AW'"/>
                    </xsl:when>
                    <xsl:when test="$code='AUSTRALIA'">
                        <xsl:value-of select="'AU'"/>
                    </xsl:when>
                    <xsl:when test="$code='AUSTRIA'">
                        <xsl:value-of select="'AT'"/>
                    </xsl:when>
                    <xsl:when test="$code='AZERBAIJAN'">
                        <xsl:value-of select="'AZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='BAHAMAS'">
                        <xsl:value-of select="'BS'"/>
                    </xsl:when>
                    <xsl:when test="$code='BAHRAIN'">
                        <xsl:value-of select="'BH'"/>
                    </xsl:when>
                    <xsl:when test="$code='BANGLADESH'">
                        <xsl:value-of select="'BD'"/>
                    </xsl:when>
                    <xsl:when test="$code='BARBADOS'">
                        <xsl:value-of select="'BB'"/>
                    </xsl:when>
                    <xsl:when test="$code='BELARUS'">
                        <xsl:value-of select="'BY'"/>
                    </xsl:when>
                    <xsl:when test="$code='BELGIUM'">
                        <xsl:value-of select="'BE'"/>
                    </xsl:when>
                    <xsl:when test="$code='BELIZE'">
                        <xsl:value-of select="'BZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='BENIN'">
                        <xsl:value-of select="'BJ'"/>
                    </xsl:when>
                    <xsl:when test="$code='BERMUDA'">
                        <xsl:value-of select="'BM'"/>
                    </xsl:when>
                    <xsl:when test="$code='BHUTAN'">
                        <xsl:value-of select="'BT'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOLIVIA'">
                        <xsl:value-of select="'BO'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOSNIA AND HERZEGOVINA'">
                        <xsl:value-of select="'BA'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOTSWANA'">
                        <xsl:value-of select="'BW'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOUVET ISLAND'">
                        <xsl:value-of select="'BV'"/>
                    </xsl:when>
                    <xsl:when test="$code='BRAZIL'">
                        <xsl:value-of select="'BR'"/>
                    </xsl:when>
                    <xsl:when test="$code='BRITISH INDIAN OCEAN TERRITORY'">
                        <xsl:value-of select="'IO'"/>
                    </xsl:when>
                    <xsl:when test="$code='BRUNEI DARUSSALAM'">
                        <xsl:value-of select="'BN'"/>
                    </xsl:when>
                    <xsl:when test="$code='BULGARIA'">
                        <xsl:value-of select="'BG'"/>
                    </xsl:when>
                    <xsl:when test="$code='BURKINA FASO'">
                        <xsl:value-of select="'BF'"/>
                    </xsl:when>
                    <xsl:when test="$code='BURUNDI'">
                        <xsl:value-of select="'BI'"/>
                    </xsl:when>
                    <xsl:when test="$code='CAMBODIA'">
                        <xsl:value-of select="'KH'"/>
                    </xsl:when>
                    <xsl:when test="$code='CAMEROON'">
                        <xsl:value-of select="'CM'"/>
                    </xsl:when>
                    <xsl:when test="$code='CANADA'">
                        <xsl:value-of select="'CA'"/>
                    </xsl:when>
                    <xsl:when test="$code='CAPE VERDE'">
                        <xsl:value-of select="'CV'"/>
                    </xsl:when>
                    <xsl:when test="$code='CAYMAN ISLANDS'">
                        <xsl:value-of select="'KY'"/>
                    </xsl:when>
                    <xsl:when test="$code='CENTRAL AFRICAN REPUBLIC'">
                        <xsl:value-of select="'CF'"/>
                    </xsl:when>
                    <xsl:when test="$code='CHAD'">
                        <xsl:value-of select="'TD'"/>
                    </xsl:when>
                    <xsl:when test="$code='CHILE'">
                        <xsl:value-of select="'CL'"/>
                    </xsl:when>
                    <xsl:when test="$code='CHINA'">
                        <xsl:value-of select="'CN'"/>
                    </xsl:when>
                    <xsl:when test="$code='CHRISTMAS ISLAND'">
                        <xsl:value-of select="'CX'"/>
                    </xsl:when>
                    <xsl:when test="$code='COCOS (KEELING) ISLANDS'">
                        <xsl:value-of select="'CC'"/>
                    </xsl:when>
                    <xsl:when test="$code='COLOMBIA'">
                        <xsl:value-of select="'CO'"/>
                    </xsl:when>
                    <xsl:when test="$code='COMOROS'">
                        <xsl:value-of select="'KM'"/>
                    </xsl:when>
                    <xsl:when test="$code='CONGO'">
                        <xsl:value-of select="'CG'"/>
                    </xsl:when>
                    <xsl:when test="$code='CONGO, THE DEMOCRATIC REPUBLIC OF THE'">
                        <xsl:value-of select="'CD'"/>
                    </xsl:when>
                    <xsl:when test="$code='COOK ISLANDS'">
                        <xsl:value-of select="'CK'"/>
                    </xsl:when>
                    <xsl:when test="$code='COSTA RICA'">
                        <xsl:value-of select="'CR'"/>
                    </xsl:when>
                    <xsl:when test="$code='COTE DIVOIRE'">
                        <xsl:value-of select="'CI'"/>
                    </xsl:when>
                    <xsl:when test="$code='CROATIA'">
                        <xsl:value-of select="'HR'"/>
                    </xsl:when>
                    <xsl:when test="$code='CUBA'">
                        <xsl:value-of select="'CU'"/>
                    </xsl:when>
                    <xsl:when test="$code='CYPRUS'">
                        <xsl:value-of select="'CY'"/>
                    </xsl:when>
                    <xsl:when test="$code='CZECH REPUBLIC'">
                        <xsl:value-of select="'CZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='DENMARK'">
                        <xsl:value-of select="'DK'"/>
                    </xsl:when>
                    <xsl:when test="$code='DJIBOUTI'">
                        <xsl:value-of select="'DJ'"/>
                    </xsl:when>
                    <xsl:when test="$code='DOMINICA'">
                        <xsl:value-of select="'DM'"/>
                    </xsl:when>
                    <xsl:when test="$code='DOMINICAN REPUBLIC'">
                        <xsl:value-of select="'DO'"/>
                    </xsl:when>
                    <xsl:when test="$code='ECUADOR'">
                        <xsl:value-of select="'EC'"/>
                    </xsl:when>
                    <xsl:when test="$code='EGYPT'">
                        <xsl:value-of select="'EG'"/>
                    </xsl:when>
                    <xsl:when test="$code='EL SALVADOR'">
                        <xsl:value-of select="'SV'"/>
                    </xsl:when>
                    <xsl:when test="$code='EQUATORIAL GUINEA'">
                        <xsl:value-of select="'GQ'"/>
                    </xsl:when>
                    <xsl:when test="$code='ERITREA'">
                        <xsl:value-of select="'ER'"/>
                    </xsl:when>
                    <xsl:when test="$code='ESTONIA'">
                        <xsl:value-of select="'EE'"/>
                    </xsl:when>
                    <xsl:when test="$code='ETHIOPIA'">
                        <xsl:value-of select="'ET'"/>
                    </xsl:when>
                    <xsl:when test="$code='FALKLAND ISLANDS (MALVINAS)'">
                        <xsl:value-of select="'FK'"/>
                    </xsl:when>
                    <xsl:when test="$code='FAROE ISLANDS'">
                        <xsl:value-of select="'FO'"/>
                    </xsl:when>
                    <xsl:when test="$code='FIJI'">
                        <xsl:value-of select="'FJ'"/>
                    </xsl:when>
                    <xsl:when test="$code='FINLAND'">
                        <xsl:value-of select="'FI'"/>
                    </xsl:when>
                    <xsl:when test="$code='FRANCE'">
                        <xsl:value-of select="'FR'"/>
                    </xsl:when>
                    <xsl:when test="$code='FRENCH GUIANA'">
                        <xsl:value-of select="'GF'"/>
                    </xsl:when>
                    <xsl:when test="$code='FRENCH POLYNESIA'">
                        <xsl:value-of select="'PF'"/>
                    </xsl:when>
                    <xsl:when test="$code='FRENCH SOUTHERN TERRITORIES'">
                        <xsl:value-of select="'TF'"/>
                    </xsl:when>
                    <xsl:when test="$code='GABON'">
                        <xsl:value-of select="'GA'"/>
                    </xsl:when>
                    <xsl:when test="$code='GAMBIA'">
                        <xsl:value-of select="'GM'"/>
                    </xsl:when>
                    <xsl:when test="$code='GEORGIA'">
                        <xsl:value-of select="'GE'"/>
                    </xsl:when>
                    <xsl:when test="$code='GERMANY'">
                        <xsl:value-of select="'DE'"/>
                    </xsl:when>
                    <xsl:when test="$code='GHANA'">
                        <xsl:value-of select="'GH'"/>
                    </xsl:when>
                    <xsl:when test="$code='GIBRALTAR'">
                        <xsl:value-of select="'GI'"/>
                    </xsl:when>
                    <xsl:when test="$code='GREECE'">
                        <xsl:value-of select="'GR'"/>
                    </xsl:when>
                    <xsl:when test="$code='GREENLAND'">
                        <xsl:value-of select="'GL'"/>
                    </xsl:when>
                    <xsl:when test="$code='GRENADA'">
                        <xsl:value-of select="'GD'"/>
                    </xsl:when>
                    <xsl:when test="$code='GUADELOUPE'">
                        <xsl:value-of select="'GP'"/>
                    </xsl:when>
                    <xsl:when test="$code='GUAM'">
                        <xsl:value-of select="'GU'"/>
                    </xsl:when>
                    <xsl:when test="$code='GUATEMALA'">
                        <xsl:value-of select="'GT'"/>
                    </xsl:when>
                    <xsl:when test="$code='GUERNSEY'">
                        <xsl:value-of select="'GG'"/>
                    </xsl:when>
                    <xsl:when test="$code='GUINEA'">
                        <xsl:value-of select="'GN'"/>
                    </xsl:when>
                    <xsl:when test="$code='GUINEA-BISSAU'">
                        <xsl:value-of select="'GW'"/>
                    </xsl:when>
                    <xsl:when test="$code='GUYANA'">
                        <xsl:value-of select="'GY'"/>
                    </xsl:when>
                    <xsl:when test="$code='HAITI'">
                        <xsl:value-of select="'HT'"/>
                    </xsl:when>
                    <xsl:when test="$code='HEARD ISLAND AND MCDONALD ISLANDS'">
                        <xsl:value-of select="'HM'"/>
                    </xsl:when>
                    <xsl:when test="$code='HOLY SEE (VATICAN CITY STATE)'">
                        <xsl:value-of select="'VA'"/>
                    </xsl:when>
                    <xsl:when test="$code='HONDURAS'">
                        <xsl:value-of select="'HN'"/>
                    </xsl:when>
                    <xsl:when test="$code='HONG KONG'">
                        <xsl:value-of select="'HK'"/>
                    </xsl:when>
                    <xsl:when test="$code='HUNGARY'">
                        <xsl:value-of select="'HU'"/>
                    </xsl:when>
                    <xsl:when test="$code='ICELAND'">
                        <xsl:value-of select="'IS'"/>
                    </xsl:when>
                    <xsl:when test="$code='INDIA'">
                        <xsl:value-of select="'IN'"/>
                    </xsl:when>
                    <xsl:when test="$code='INDONESIA'">
                        <xsl:value-of select="'ID'"/>
                    </xsl:when>
                    <xsl:when test="$code='IRAN, ISLAMIC REPUBLIC OF'">
                        <xsl:value-of select="'IR'"/>
                    </xsl:when>
                    <xsl:when test="$code='IRAQ'">
                        <xsl:value-of select="'IQ'"/>
                    </xsl:when>
                    <xsl:when test="$code='IRELAND'">
                        <xsl:value-of select="'IE'"/>
                    </xsl:when>
                    <xsl:when test="$code='ISLE OF MAN'">
                        <xsl:value-of select="'IM'"/>
                    </xsl:when>
                    <xsl:when test="$code='ISRAEL'">
                        <xsl:value-of select="'IL'"/>
                    </xsl:when>
                    <xsl:when test="$code='ITALY'">
                        <xsl:value-of select="'IT'"/>
                    </xsl:when>
                    <xsl:when test="$code='JAMAICA'">
                        <xsl:value-of select="'JM'"/>
                    </xsl:when>
                    <xsl:when test="$code='JAPAN'">
                        <xsl:value-of select="'JP'"/>
                    </xsl:when>
                    <xsl:when test="$code='JERSEY'">
                        <xsl:value-of select="'JE'"/>
                    </xsl:when>
                    <xsl:when test="$code='JORDAN'">
                        <xsl:value-of select="'JO'"/>
                    </xsl:when>
                    <xsl:when test="$code='KAZAKHSTAN'">
                        <xsl:value-of select="'KZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='KENYA'">
                        <xsl:value-of select="'KE'"/>
                    </xsl:when>
                    <xsl:when test="$code='KIRIBATI'">
                        <xsl:value-of select="'KI'"/>
                    </xsl:when>
                    <xsl:when test="$code='KOREA, DEMOCRATIC PEOPLES REPUBLIC OF'">
                        <xsl:value-of select="'KP'"/>
                    </xsl:when>
                    <xsl:when test="$code='KOREA, REPUBLIC OF'">
                        <xsl:value-of select="'KR'"/>
                    </xsl:when>
                    <xsl:when test="$code='KUWAIT'">
                        <xsl:value-of select="'KW'"/>
                    </xsl:when>
                    <xsl:when test="$code='KYRGYZSTAN'">
                        <xsl:value-of select="'KG'"/>
                    </xsl:when>
                    <xsl:when test="$code='LAO PEOPLES DEMOCRATIC REPUBLIC'">
                        <xsl:value-of select="'LA'"/>
                    </xsl:when>
                    <xsl:when test="$code='LATVIA'">
                        <xsl:value-of select="'LV'"/>
                    </xsl:when>
                    <xsl:when test="$code='LEBANON'">
                        <xsl:value-of select="'LB'"/>
                    </xsl:when>
                    <xsl:when test="$code='LESOTHO'">
                        <xsl:value-of select="'LS'"/>
                    </xsl:when>
                    <xsl:when test="$code='LIBERIA'">
                        <xsl:value-of select="'LR'"/>
                    </xsl:when>
                    <xsl:when test="$code='LIBYAN ARAB JAMAHIRIYA'">
                        <xsl:value-of select="'LY'"/>
                    </xsl:when>
                    <xsl:when test="$code='LIECHTENSTEIN'">
                        <xsl:value-of select="'LI'"/>
                    </xsl:when>
                    <xsl:when test="$code='LITHUANIA'">
                        <xsl:value-of select="'LT'"/>
                    </xsl:when>
                    <xsl:when test="$code='LUXEMBOURG'">
                        <xsl:value-of select="'LU'"/>
                    </xsl:when>
                    <xsl:when test="$code='MACAO'">
                        <xsl:value-of select="'MO'"/>
                    </xsl:when>
                    <xsl:when test="$code='MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF'">
                        <xsl:value-of select="'MK'"/>
                    </xsl:when>
                    <xsl:when test="$code='MADAGASCAR'">
                        <xsl:value-of select="'MG'"/>
                    </xsl:when>
                    <xsl:when test="$code='MALAWI'">
                        <xsl:value-of select="'MW'"/>
                    </xsl:when>
                    <xsl:when test="$code='MALAYSIA'">
                        <xsl:value-of select="'MY'"/>
                    </xsl:when>
                    <xsl:when test="$code='MALDIVES'">
                        <xsl:value-of select="'MV'"/>
                    </xsl:when>
                    <xsl:when test="$code='MALI'">
                        <xsl:value-of select="'ML'"/>
                    </xsl:when>
                    <xsl:when test="$code='MALTA'">
                        <xsl:value-of select="'MT'"/>
                    </xsl:when>
                    <xsl:when test="$code='MARSHALL ISLANDS'">
                        <xsl:value-of select="'MH'"/>
                    </xsl:when>
                    <xsl:when test="$code='MARTINIQUE'">
                        <xsl:value-of select="'MQ'"/>
                    </xsl:when>
                    <xsl:when test="$code='MAURITANIA'">
                        <xsl:value-of select="'MR'"/>
                    </xsl:when>
                    <xsl:when test="$code='MAURITIUS'">
                        <xsl:value-of select="'MU'"/>
                    </xsl:when>
                    <xsl:when test="$code='MAYOTTE'">
                        <xsl:value-of select="'YT'"/>
                    </xsl:when>
                    <xsl:when test="$code='MEXICO'">
                        <xsl:value-of select="'MX'"/>
                    </xsl:when>
                    <xsl:when test="$code='MICRONESIA, FEDERATED STATES OF'">
                        <xsl:value-of select="'FM'"/>
                    </xsl:when>
                    <xsl:when test="$code='MOLDOVA, REPUBLIC OF'">
                        <xsl:value-of select="'MD'"/>
                    </xsl:when>
                    <xsl:when test="$code='MONACO'">
                        <xsl:value-of select="'MC'"/>
                    </xsl:when>
                    <xsl:when test="$code='MONGOLIA'">
                        <xsl:value-of select="'MN'"/>
                    </xsl:when>
                    <xsl:when test="$code='MONTSERRAT'">
                        <xsl:value-of select="'MS'"/>
                    </xsl:when>
                    <xsl:when test="$code='MOROCCO'">
                        <xsl:value-of select="'MA'"/>
                    </xsl:when>
                    <xsl:when test="$code='MOZAMBIQUE'">
                        <xsl:value-of select="'MZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='MYANMAR'">
                        <xsl:value-of select="'MM'"/>
                    </xsl:when>
                    <xsl:when test="$code='NAMIBIA'">
                        <xsl:value-of select="'NA'"/>
                    </xsl:when>
                    <xsl:when test="$code='NAURU'">
                        <xsl:value-of select="'NR'"/>
                    </xsl:when>
                    <xsl:when test="$code='NEPAL'">
                        <xsl:value-of select="'NP'"/>
                    </xsl:when>
                    <xsl:when test="$code='NETHERLANDS'">
                        <xsl:value-of select="'NL'"/>
                    </xsl:when>
                    <xsl:when test="$code='NETHERLANDS ANTILLES'">
                        <xsl:value-of select="'AN'"/>
                    </xsl:when>
                    <xsl:when test="$code='NEW CALEDONIA'">
                        <xsl:value-of select="'NC'"/>
                    </xsl:when>
                    <xsl:when test="$code='NEW ZEALAND'">
                        <xsl:value-of select="'NZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='NICARAGUA'">
                        <xsl:value-of select="'NI'"/>
                    </xsl:when>
                    <xsl:when test="$code='NIGER'">
                        <xsl:value-of select="'NE'"/>
                    </xsl:when>
                    <xsl:when test="$code='NIGERIA'">
                        <xsl:value-of select="'NG'"/>
                    </xsl:when>
                    <xsl:when test="$code='NIUE'">
                        <xsl:value-of select="'NU'"/>
                    </xsl:when>
                    <xsl:when test="$code='NORFOLK ISLAND'">
                        <xsl:value-of select="'NF'"/>
                    </xsl:when>
                    <xsl:when test="$code='NORTHERN MARIANA ISLANDS'">
                        <xsl:value-of select="'MP'"/>
                    </xsl:when>
                    <xsl:when test="$code='NORWAY'">
                        <xsl:value-of select="'NO'"/>
                    </xsl:when>
                    <xsl:when test="$code='OMAN'">
                        <xsl:value-of select="'OM'"/>
                    </xsl:when>
                    <xsl:when test="$code='PAKISTAN'">
                        <xsl:value-of select="'PK'"/>
                    </xsl:when>
                    <xsl:when test="$code='PALAU'">
                        <xsl:value-of select="'PW'"/>
                    </xsl:when>
                    <xsl:when test="$code='PALESTINIAN TERRITORY, OCCUPIED'">
                        <xsl:value-of select="'PS'"/>
                    </xsl:when>
                    <xsl:when test="$code='PANAMA'">
                        <xsl:value-of select="'PA'"/>
                    </xsl:when>
                    <xsl:when test="$code='PAPUA NEW GUINEA'">
                        <xsl:value-of select="'PG'"/>
                    </xsl:when>
                    <xsl:when test="$code='PARAGUAY'">
                        <xsl:value-of select="'PY'"/>
                    </xsl:when>
                    <xsl:when test="$code='PERU'">
                        <xsl:value-of select="'PE'"/>
                    </xsl:when>
                    <xsl:when test="$code='PHILIPPINES'">
                        <xsl:value-of select="'PH'"/>
                    </xsl:when>
                    <xsl:when test="$code='PITCAIRN'">
                        <xsl:value-of select="'PN'"/>
                    </xsl:when>
                    <xsl:when test="$code='POLAND'">
                        <xsl:value-of select="'PL'"/>
                    </xsl:when>
                    <xsl:when test="$code='PORTUGAL'">
                        <xsl:value-of select="'PT'"/>
                    </xsl:when>
                    <xsl:when test="$code='PUERTO RICO'">
                        <xsl:value-of select="'PR'"/>
                    </xsl:when>
                    <xsl:when test="$code='QATAR'">
                        <xsl:value-of select="'QA'"/>
                    </xsl:when>
                    <xsl:when test="$code='REUNION'">
                        <xsl:value-of select="'RE'"/>
                    </xsl:when>
                    <xsl:when test="$code='ROMANIA'">
                        <xsl:value-of select="'RO'"/>
                    </xsl:when>
                    <xsl:when test="$code='RUSSIAN FEDERATION'">
                        <xsl:value-of select="'RU'"/>
                    </xsl:when>
                    <xsl:when test="$code='RWANDA'">
                        <xsl:value-of select="'RW'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT HELENA'">
                        <xsl:value-of select="'SH'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT KITTS AND NEVIS'">
                        <xsl:value-of select="'KN'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT LUCIA'">
                        <xsl:value-of select="'LC'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT PIERRE AND MIQUELON'">
                        <xsl:value-of select="'PM'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT VINCENT AND THE GRENADINES'">
                        <xsl:value-of select="'VC'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAMOA'">
                        <xsl:value-of select="'WS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAN MARINO'">
                        <xsl:value-of select="'SM'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAO TOME AND PRINCIPE'">
                        <xsl:value-of select="'ST'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAUDI ARABIA'">
                        <xsl:value-of select="'SA'"/>
                    </xsl:when>
                    <xsl:when test="$code='SENEGAL'">
                        <xsl:value-of select="'SN'"/>
                    </xsl:when>
                    <xsl:when test="$code='SERBIA AND MONTENEGRO'">
                        <xsl:value-of select="'CS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SEYCHELLES'">
                        <xsl:value-of select="'SC'"/>
                    </xsl:when>
                    <xsl:when test="$code='SIERRA LEONE'">
                        <xsl:value-of select="'SL'"/>
                    </xsl:when>
                    <xsl:when test="$code='SINGAPORE'">
                        <xsl:value-of select="'SG'"/>
                    </xsl:when>
                    <xsl:when test="$code='SLOVAKIA'">
                        <xsl:value-of select="'SK'"/>
                    </xsl:when>
                    <xsl:when test="$code='SLOVENIA'">
                        <xsl:value-of select="'SI'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOLOMON ISLANDS'">
                        <xsl:value-of select="'SB'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOMALIA'">
                        <xsl:value-of select="'SO'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOUTH AFRICA'">
                        <xsl:value-of select="'ZA'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS'">
                        <xsl:value-of select="'GS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SPAIN'">
                        <xsl:value-of select="'ES'"/>
                    </xsl:when>
                    <xsl:when test="$code='SRI LANKA'">
                        <xsl:value-of select="'LK'"/>
                    </xsl:when>
                    <xsl:when test="$code='SUDAN'">
                        <xsl:value-of select="'SD'"/>
                    </xsl:when>
                    <xsl:when test="$code='SURINAME'">
                        <xsl:value-of select="'SR'"/>
                    </xsl:when>
                    <xsl:when test="$code='SVALBARD AND JAN MAYEN'">
                        <xsl:value-of select="'SJ'"/>
                    </xsl:when>
                    <xsl:when test="$code='SWAZILAND'">
                        <xsl:value-of select="'SZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='SWEDEN'">
                        <xsl:value-of select="'SE'"/>
                    </xsl:when>
                    <xsl:when test="$code='SWITZERLAND'">
                        <xsl:value-of select="'CH'"/>
                    </xsl:when>
                    <xsl:when test="$code='SYRIAN ARAB REPUBLIC'">
                        <xsl:value-of select="'SY'"/>
                    </xsl:when>
                    <xsl:when test="$code='TAIWAN, PROVINCE OF CHINA'">
                        <xsl:value-of select="'TW'"/>
                    </xsl:when>
                    <xsl:when test="$code='TAJIKISTAN'">
                        <xsl:value-of select="'TJ'"/>
                    </xsl:when>
                    <xsl:when test="$code='TANZANIA, UNITED REPUBLIC OF'">
                        <xsl:value-of select="'TZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='THAILAND'">
                        <xsl:value-of select="'TH'"/>
                    </xsl:when>
                    <xsl:when test="$code='TIMOR-LESTE'">
                        <xsl:value-of select="'TL'"/>
                    </xsl:when>
                    <xsl:when test="$code='TOGO'">
                        <xsl:value-of select="'TG'"/>
                    </xsl:when>
                    <xsl:when test="$code='TOKELAU'">
                        <xsl:value-of select="'TK'"/>
                    </xsl:when>
                    <xsl:when test="$code='TONGA'">
                        <xsl:value-of select="'TO'"/>
                    </xsl:when>
                    <xsl:when test="$code='TRINIDAD AND TOBAGO'">
                        <xsl:value-of select="'TT'"/>
                    </xsl:when>
                    <xsl:when test="$code='TUNISIA'">
                        <xsl:value-of select="'TN'"/>
                    </xsl:when>
                    <xsl:when test="$code='TURKEY'">
                        <xsl:value-of select="'TR'"/>
                    </xsl:when>
                    <xsl:when test="$code='TURKMENISTAN'">
                        <xsl:value-of select="'TM'"/>
                    </xsl:when>
                    <xsl:when test="$code='TURKS AND CAICOS ISLANDS'">
                        <xsl:value-of select="'TC'"/>
                    </xsl:when>
                    <xsl:when test="$code='TUVALU'">
                        <xsl:value-of select="'TV'"/>
                    </xsl:when>
                    <xsl:when test="$code='UGANDA'">
                        <xsl:value-of select="'UG'"/>
                    </xsl:when>
                    <xsl:when test="$code='UKRAINE'">
                        <xsl:value-of select="'UA'"/>
                    </xsl:when>
                    <xsl:when test="$code='UNITED ARAB EMIRATES'">
                        <xsl:value-of select="'AE'"/>
                    </xsl:when>
                    <xsl:when test="$code='UNITED KINGDOM'">
                        <xsl:value-of select="'GB'"/>
                    </xsl:when>
                    <xsl:when test="$code='UNITED STATES'">
                        <xsl:value-of select="'US'"/>
                    </xsl:when>
                    <xsl:when test="$code='UNITED STATES MINOR OUTLYING ISLANDS'">
                        <xsl:value-of select="'UM'"/>
                    </xsl:when>
                    <xsl:when test="$code='URUGUAY'">
                        <xsl:value-of select="'UY'"/>
                    </xsl:when>
                    <xsl:when test="$code='UZBEKISTAN'">
                        <xsl:value-of select="'UZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='VANUATU'">
                        <xsl:value-of select="'VU'"/>
                    </xsl:when>
                    <xsl:when test="$code='VENEZUELA'">
                        <xsl:value-of select="'VE'"/>
                    </xsl:when>
                    <xsl:when test="$code='VIET NAM'">
                        <xsl:value-of select="'VN'"/>
                    </xsl:when>
                    <xsl:when test="$code='VIRGIN ISLANDS, BRITISH'">
                        <xsl:value-of select="'VG'"/>
                    </xsl:when>
                    <xsl:when test="$code='VIRGIN ISLANDS, U.S.'">
                        <xsl:value-of select="'VI'"/>
                    </xsl:when>
                    <xsl:when test="$code='WALLIS AND FUTUNA'">
                        <xsl:value-of select="'WF'"/>
                    </xsl:when>
                    <xsl:when test="$code='WESTERN SAHARA'">
                        <xsl:value-of select="'EH'"/>
                    </xsl:when>
                    <xsl:when test="$code='YEMEN'">
                        <xsl:value-of select="'YE'"/>
                    </xsl:when>
                    <xsl:when test="$code='ZAMBIA'">
                        <xsl:value-of select="'ZM'"/>
                    </xsl:when>
                    <xsl:when test="$code='ZIMBABWE'">
                        <xsl:value-of select="'ZW'"/>
                    </xsl:when>
                    <!-- BGT included list -->
                    <xsl:when test="$code='EASTERN AUSTRALIA'">
                        <xsl:value-of select="'AU'"/>
                    </xsl:when>
                    <xsl:when test="$code='WESTERN AUSTRALIA'">
                        <xsl:value-of select="'AU'"/>
                    </xsl:when>
                    <xsl:when test="$code='CENTRAL AUSTRALIA'">
                        <xsl:value-of select="'AU'"/>
                    </xsl:when>
                    <xsl:when test="$code='AMERICA'">
                        <xsl:value-of select="'US'"/>
                    </xsl:when>
                    <xsl:when test="$code='ANTARCTICA'">
                        <xsl:value-of select="'AQ'"/>
                    </xsl:when>
                    <xsl:when test="$code='ANTIGUA &amp; BARBUDA'">
                        <xsl:value-of select="'AG'"/>
                    </xsl:when>
                    <xsl:when test="$code='ANTIGUA AND BARBUDA'">
                        <xsl:value-of select="'AG'"/>
                    </xsl:when>
                    <xsl:when test="$code='ANTIGUA &amp;AMP; BARBUDA'">
                        <xsl:value-of select="'AG'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOSNIA &amp;AMP; HERZEGOVINA'">
                        <xsl:value-of select="'BA'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOSNIA &amp; HERZEGOVINA'">
                        <xsl:value-of select="'BA'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOSNIA AND HERZEGOVINA'">
                        <xsl:value-of select="'BA'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOSNIA AND HERZEGOWINA'">
                        <xsl:value-of select="'BA'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOUVET ISLAND'">
                        <xsl:value-of select="'BV'"/>
                    </xsl:when>
                    <xsl:when test="$code='BOUVETISLAND'">
                        <xsl:value-of select="'BV'"/>
                    </xsl:when>
                    <xsl:when test="$code='BRASIL'">
                        <xsl:value-of select="'BR'"/>
                    </xsl:when>
                    <xsl:when test="$code='BURKINA FASO'">
                        <xsl:value-of select="'BF'"/>
                    </xsl:when>
                    <xsl:when test="$code='BURKINAFASO'">
                        <xsl:value-of select="'BF'"/>
                    </xsl:when>
                    <xsl:when test="$code='BURMA'">
                        <xsl:value-of select="'MM'"/>
                    </xsl:when>
                    <xsl:when test="$code='COCOS (KEELING) ISLANDS'">
                        <xsl:value-of select="'CC'"/>
                    </xsl:when>
                    <xsl:when test="$code='COCOS ISLANDS'">
                        <xsl:value-of select="'CC'"/>
                    </xsl:when>
                    <xsl:when test="$code='COLUMBIA'">
                        <xsl:value-of select="'CO'"/>
                    </xsl:when>
                    <xsl:when test="$code='CONGO, DEMOCRATIC REPUBLIC'">
                        <xsl:value-of select="'CD'"/>
                    </xsl:when>
                    <xsl:when test="$code='COSTARICA'">
                        <xsl:value-of select="'CR'"/>
                    </xsl:when>
                    <xsl:when test="$code='CZECHOSLOVAKIA'">
                        <xsl:value-of select="'CZ'"/>
                    </xsl:when>
                    <xsl:when test="$code='DEMOCRATIC REPUBLIC OF THE CONGO'">
                        <xsl:value-of select="'CD'"/>
                    </xsl:when>
                    <xsl:when test="$code='EAST TIMOR'">
                        <xsl:value-of select="'TL'"/>
                    </xsl:when>
                    <xsl:when test="$code='EASTTIMOR'">
                        <xsl:value-of select="'TL'"/>
                    </xsl:when>
                    <xsl:when test="$code='ENGLAND'">
                        <xsl:value-of select="'GB'"/>
                    </xsl:when>
                    <xsl:when test="$code='FALKLAND ISLANDS'">
                        <xsl:value-of select="'FK'"/>
                    </xsl:when>
                    <xsl:when test="$code='FALKLAND ISLANDS (MALVINAS)'">
                        <xsl:value-of select="'FK'"/>
                    </xsl:when>
                    <xsl:when test="$code='FEDERAL REPUBLIC OF GERMANY'">
                        <xsl:value-of select="'DE'"/>
                    </xsl:when>
                    <xsl:when test="$code='FRG'">
                        <xsl:value-of select="'DE'"/>
                    </xsl:when>
                    <xsl:when test="$code='GREAT BRITAIN'">
                        <xsl:value-of select="'GB'"/>
                    </xsl:when>
                    <xsl:when test="$code='GUINEA - BISSAU'">
                        <xsl:value-of select="'GW'"/>
                    </xsl:when>
                    <xsl:when test="$code='GUINEA BISSAU'">
                        <xsl:value-of select="'GW'"/>
                    </xsl:when>
                    <xsl:when test="$code='HEARD &amp; MCDONALD ISLANDS'">
                        <xsl:value-of select="'HM'"/>
                    </xsl:when>
                    <xsl:when test="$code='HEARD &amp;AMP; MCDONALD ISLANDS'">
                        <xsl:value-of select="'HM'"/>
                    </xsl:when>
                    <xsl:when test="$code='HEARD AND MC DONALD ISLANDS'">
                        <xsl:value-of select="'HM'"/>
                    </xsl:when>
                    <xsl:when test="$code='HEARD AND MCDONALD ISLANDS'">
                        <xsl:value-of select="'HM'"/>
                    </xsl:when>
                    <xsl:when test="$code='HOLYSEE'">
                        <xsl:value-of select="'VA'"/>
                    </xsl:when>
                    <xsl:when test="$code='IVORY COAST'">
                        <xsl:value-of select="'CI'"/>
                    </xsl:when>
                    <xsl:when test="$code='KINGDOM OF SAUDI ARABIA'">
                        <xsl:value-of select="'SA'"/>
                    </xsl:when>
                    <xsl:when test="$code='KOREA'">
                        <xsl:value-of select="'KR'"/>
                    </xsl:when>
                    <xsl:when test="$code='KOREA NORTH'">
                        <xsl:value-of select="'KP'"/>
                    </xsl:when>
                    <xsl:when test="$code='KOREA SOUTH'">
                        <xsl:value-of select="'KR'"/>
                    </xsl:when>
                    <xsl:when test="$code='KOREA, NORTH'">
                        <xsl:value-of select="'KP'"/>
                    </xsl:when>
                    <xsl:when test="$code='KOREA, SOUTH'">
                        <xsl:value-of select="'KR'"/>
                    </xsl:when>
                    <xsl:when test="$code='KSA'">
                        <xsl:value-of select="'SA'"/>
                    </xsl:when>
                    <xsl:when test="$code='MALVINAS'">
                        <xsl:value-of select="'FK'"/>
                    </xsl:when>
                    <xsl:when test="$code='MARSHALL ISLANDS'">
                        <xsl:value-of select="'MH'"/>
                    </xsl:when>
                    <xsl:when test="$code='MICRONESIA'">
                        <xsl:value-of select="'FM'"/>
                    </xsl:when>
                    <xsl:when test="$code='MICRONESIA, FEDERATED STATES OF'">
                        <xsl:value-of select="'FM'"/>
                    </xsl:when>
                    <xsl:when test="$code='MONTENEGRO'">
                        <xsl:value-of select="'CS'"/>
                    </xsl:when>
                    <xsl:when test="$code='P R'">
                        <xsl:value-of select="'PR'"/>
                    </xsl:when>
                    <xsl:when test="$code='P R C'">
                        <xsl:value-of select="'CN'"/>
                    </xsl:when>
                    <xsl:when test="$code='P R CHINA'">
                        <xsl:value-of select="'CN'"/>
                    </xsl:when>
                    <xsl:when test="$code='PALESTINE'">
                        <xsl:value-of select="'PS'"/>
                    </xsl:when>
                    <xsl:when test="$code='PALESTINIAN TERRITORY'">
                        <xsl:value-of select="'PS'"/>
                    </xsl:when>
                    <xsl:when test="$code='PEOPLES REPUBLIC OF CHINA'">
                        <xsl:value-of select="'CN'"/>
                    </xsl:when>
                    <xsl:when test="$code='PHILIPPNES'">
                        <xsl:value-of select="'RP'"/>
                    </xsl:when>
                    <xsl:when test="$code='PITCAIRN'">
                        <xsl:value-of select="'PN'"/>
                    </xsl:when>
                    <xsl:when test="$code='PITCAIRN ISLANDS'">
                        <xsl:value-of select="'PN'"/>
                    </xsl:when>
                    <xsl:when test="$code='PR CHINA'">
                        <xsl:value-of select="'CN'"/>
                    </xsl:when>
                    <xsl:when test="$code='REPUBLIC OF KOREA'">
                        <xsl:value-of select="'KP'"/>
                    </xsl:when>
                    <xsl:when test="$code='REPUBLIC OF PANAMA'">
                        <xsl:value-of select="'PA'"/>
                    </xsl:when>
                    <xsl:when test="$code='ROK'">
                        <xsl:value-of select="'KP'"/>
                    </xsl:when>
                    <xsl:when test="$code='RUSSIA'">
                        <xsl:value-of select="'RU'"/>
                    </xsl:when>
                    <xsl:when test="$code='S A'">
                        <xsl:value-of select="'SA'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT HELENA'">
                        <xsl:value-of select="'SH'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT KITTS &amp; NEVIS'">
                        <xsl:value-of select="'KN'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT KITTS &amp;AMP; NEVIS'">
                        <xsl:value-of select="'KN'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT PIERRE AND MIQUELON'">
                        <xsl:value-of select="'PM'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT VINCENT &amp; THE GRENADINES'">
                        <xsl:value-of select="'VC'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAINT VINCENT &amp; THE GRENADINES'">
                        <xsl:value-of select="'VC'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAO TOME &amp; PRINCIPE'">
                        <xsl:value-of select="'ST'"/>
                    </xsl:when>
                    <xsl:when test="$code='SAO TOME &amp;AMP; PRINCIPE'">
                        <xsl:value-of select="'ST'"/>
                    </xsl:when>
                    <xsl:when test="$code='SCOTLAND'">
                        <xsl:value-of select="'GB'"/>
                    </xsl:when>
                    <xsl:when test="$code='SERBIA'">
                        <xsl:value-of select="'CS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SERBIA &amp; MONTENEGRO'">
                        <xsl:value-of select="'CS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SERBIA &amp;AMP; MONTENEGRO'">
                        <xsl:value-of select="'CS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOUTH GEORGIA &amp; SOUTH SANDWICH ISLANDS'">
                        <xsl:value-of select="'GS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOUTH GEORGIA &amp;AMP; SOUTH SANDWICH ISLANDS'">
                        <xsl:value-of select="'GS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOUTH GEORGIA AND SOUTH SANDWICH ISLANDS'">
                        <xsl:value-of select="'GS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS'">
                        <xsl:value-of select="'GS'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOUTH KOREA'">
                        <xsl:value-of select="'KR'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOUTH VIETNAM'">
                        <xsl:value-of select="'VN'"/>
                    </xsl:when>
                    <xsl:when test="$code='SOUTHERN AFRICA'">
                        <xsl:value-of select="'ZA'"/>
                    </xsl:when>
                    <xsl:when test="$code='ST . HELENA'">
                        <xsl:value-of select="'SH'"/>
                    </xsl:when>
                    <xsl:when test="$code='ST . PIERRE AND MIQUELON'">
                        <xsl:value-of select="'PM'"/>
                    </xsl:when>
                    <xsl:when test="$code='ST HELENA'">
                        <xsl:value-of select="'SH'"/>
                    </xsl:when>
                    <xsl:when test="$code='ST PIERRE &amp; MIQUELON'">
                        <xsl:value-of select="'PM'"/>
                    </xsl:when>
                    <xsl:when test="$code='ST PIERRE &amp;AMP; MIQUELON'">
                        <xsl:value-of select="'PM'"/>
                    </xsl:when>
                    <xsl:when test="$code='ST PIERRE AND MIQUELON'">
                        <xsl:value-of select="'PM'"/>
                    </xsl:when>
                    <xsl:when test="$code='SVALBARD &amp; JANMAYEN ISLANDS'">
                        <xsl:value-of select="'SJ'"/>
                    </xsl:when>
                    <xsl:when test="$code='SVALBARD &amp;AMP; JANMAYEN ISLANDS'">
                        <xsl:value-of select="'SJ'"/>
                    </xsl:when>
                    <xsl:when test="$code='SVALBARD AND JAN MAYEN'">
                        <xsl:value-of select="'SJ'"/>
                    </xsl:when>
                    <xsl:when test="$code='SVALBARD AND JANMAYEN ISLANDS'">
                        <xsl:value-of select="'SJ'"/>
                    </xsl:when>
                    <xsl:when test="$code='THE NETHERLANDS'">
                        <xsl:value-of select="'NL'"/>
                    </xsl:when>
                    <xsl:when test="$code='TIBET'">
                        <xsl:value-of select="'CN'"/>
                    </xsl:when>
                    <xsl:when test="$code='TIMOR LESTE'">
                        <xsl:value-of select="'TL'"/>
                    </xsl:when>
                    <xsl:when test="$code='TRINIDAD &amp; TOBAGO'">
                        <xsl:value-of select="'TT'"/>
                    </xsl:when>
                    <xsl:when test="$code='TRINIDAD &amp;AMP; TOBAGO'">
                        <xsl:value-of select="'TT'"/>
                    </xsl:when>
                    <xsl:when test="$code='TURKS &amp; CAICOS ISLANDS , TUVALU'">
                        <xsl:value-of select="'TC'"/>
                    </xsl:when>
                    <xsl:when test="$code='TURKS &amp;AMP; CAICOS ISLANDS , TUVALU'">
                        <xsl:value-of select="'TC'"/>
                    </xsl:when>
                    <xsl:when test="$code='TURKS AND CAICOS ISLANDS'">
                        <xsl:value-of select="'TC'"/>
                    </xsl:when>
                    <xsl:when test="$code='TURKS AND CAICOS ISLANDS , TUVALU'">
                        <xsl:value-of select="'TC'"/>
                    </xsl:when>
                    <xsl:when test="$code='U K'">
                        <xsl:value-of select="'GB'"/>
                    </xsl:when>
                    <xsl:when test="$code='U S'">
                        <xsl:value-of select="'US'"/>
                    </xsl:when>
                    <xsl:when test="$code='U S A'">
                        <xsl:value-of select="'US'"/>
                    </xsl:when>
                    <xsl:when test="$code='UK'">
                        <xsl:value-of select="'GB'"/>
                    </xsl:when>
                    <xsl:when test="$code='UNITED STATES MINOR OUTLYING ISLANDS'">
                        <xsl:value-of select="'UM'"/>
                    </xsl:when>
                    <xsl:when test="$code='UNITED STATES OF AMERICA'">
                        <xsl:value-of select="'US'"/>
                    </xsl:when>
                    <xsl:when test="$code='US'">
                        <xsl:value-of select="'US'"/>
                    </xsl:when>
                    <xsl:when test="$code='US OF A'">
                        <xsl:value-of select="'US'"/>
                    </xsl:when>
                    <xsl:when test="$code='USA'">
                        <xsl:value-of select="'US'"/>
                    </xsl:when>
                    <xsl:when test="$code='USSR'">
                        <xsl:value-of select="'RU'"/>
                    </xsl:when>
                    <xsl:when test="$code='VATICAN CITY STATE'">
                        <xsl:value-of select="'VA'"/>
                    </xsl:when>
                    <xsl:when test="$code='VATICAN CITY STATE (HOLY SEE)'">
                        <xsl:value-of select="'VA'"/>
                    </xsl:when>
                    <xsl:when test="$code='VIRGIN ISLANDS, BRITISH'">
                        <xsl:value-of select="'VG'"/>
                    </xsl:when>
                    <xsl:when test="$code='VIRGIN ISLANDS, U.S.'">
                        <xsl:value-of select="'VI'"/>
                    </xsl:when>
                    <xsl:when test="$code='W GERMANY'">
                        <xsl:value-of select="'DE'"/>
                    </xsl:when>
                    <xsl:when test="$code='WALES'">
                        <xsl:value-of select="'GB'"/>
                    </xsl:when>
                    <xsl:when test="$code='WALLIS AND FUTUNA'">
                        <xsl:value-of select="'WF'"/>
                    </xsl:when>
                    <xsl:when test="$code='WEST AUSTRALIA'">
                        <xsl:value-of select="'AU'"/>
                    </xsl:when>
                    <xsl:when test="$code='WEST GERMANY'">
                        <xsl:value-of select="'DE'"/>
                    </xsl:when>
                    <xsl:when test="$code='WEST INDIES'">
                        <xsl:value-of select="'WI'"/>
                    </xsl:when>
                    <xsl:when test="$code='TAHITI'">
                        <xsl:value-of select="'PF'"/>
                    </xsl:when>
                    <!-- BGT included, if the country does not match with the above list add the default country code based upon the version of Lens/XRay -->
                    <xsl:otherwise>
                        <xsl:value-of select="$countryCode"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="daterange" mode="education">
        <AttendancePeriod>
            <xsl:apply-templates select="start[1]"/>
            <xsl:apply-templates select="end[1]"/>
        </AttendancePeriod>
    </xsl:template>
    <xsl:template match="daterange">
        <xsl:apply-templates select="start[1]"/>
        <xsl:apply-templates select="end[1]"/>
    </xsl:template>
    <!--**************************************************Completion Date Template Declaration*******************-->
    <xsl:template match="completiondate" mode="education">
        <AttendancePeriod>
            <StartDate>
                <DateText>
                    <xsl:value-of select="$nkType"/>
                </DateText>
            </StartDate>
            <EndDate>
                <xsl:if test="count(@iso8601) > 0">
                    <FormattedDateTime>
                        <xsl:value-of select="@iso8601"/>
                    </FormattedDateTime>
                </xsl:if>
                <xsl:if test="not(@iso8601)">
                    <DateText>
                        <xsl:value-of select="$nkType"/>
                    </DateText>
                </xsl:if>
            </EndDate>
        </AttendancePeriod>
    </xsl:template>
    <!--*****************************************************Institution Template Declaration**************************-->
    <xsl:template match="institution">
        <xsl:value-of select="text()"/>
    </xsl:template>
    <!--******************************************************** Degreee Template Declaration ***********************************************-->
    <xsl:template match="degree">
        <EducationOrganizationAttendance>
            <OrganizationName>
                <xsl:apply-templates select="../institution"/>
            </OrganizationName>
            <OrganizationContact>
                <Communication>
                    <xsl:if test="count(address)>0">
                        <Address>
                            <xsl:apply-templates select="(../institution/address)[1]" mode="AddressSummary"/>
                        </Address>
                    </xsl:if>
                </Communication>
            </OrganizationContact>
            <xsl:if test="count(@name) > 0">
                <ProgramName>
                    <xsl:value-of select="@name"/>
                </ProgramName>
            </xsl:if>
            <!--Uninitialized variable DegreeLevel is invalid as per standard norms..-->
            <xsl:variable name="DegreeLevel">
                <xsl:choose>
                    <xsl:when test="not(@level)">
                        <xsl:value-of select="0"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@level"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <EducationLevelCode>
                <xsl:choose>
                    <xsl:when test="$DegreeLevel=12">
                        <xsl:text>highschool</xsl:text>
                    </xsl:when>
                    <xsl:when test="$DegreeLevel=16">
                        <xsl:text>college</xsl:text>
                    </xsl:when>
                    <xsl:when test="$DegreeLevel=18">
                        <xsl:text>university</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>trade</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </EducationLevelCode>
            <xsl:if test="count(../honors) > 0">
                <HonorsProgramName>
                    <xsl:value-of select="../honors"/>
                </HonorsProgramName>
            </xsl:if>
            <xsl:if test="count(../secondaryedu)>0">
                <EducationDegree>
                    <DegreeName>
                        <xsl:value-of select="../secondaryedu"/>
                    </DegreeName>
                </EducationDegree>
            </xsl:if>
            <xsl:variable name="NoOfDateRanges" select="count(../daterange)"/>
            <xsl:if test="$NoOfDateRanges>0">
                <xsl:choose>
                    <xsl:when test="$NoOfDateRanges = 1">
                        <AttendancePeriod>
                            <xsl:variable name="NoOfStartDates" select="count(../daterange/start)"/>
                            <xsl:choose>
                                <xsl:when test="$NoOfStartDates>0">
                                    <xsl:apply-templates select="../daterange/start[1]"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <StartDate>
                                        <DateText>
                                            <xsl:value-of select="$nkType"/>
                                        </DateText>
                                    </StartDate>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:apply-templates select="../daterange/end[1]"/>
                        </AttendancePeriod>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="../daterange" mode="education"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <xsl:if test="$NoOfDateRanges=0">
                <xsl:if test="count(../completiondate)>0">
                    <xsl:apply-templates select="../completiondate" mode="education"/>
                </xsl:if>
            </xsl:if>
            <EducationScore>
                <xsl:if test="count(../gpa[1])> 0 and string-length(../gpa[1]) > 0">
                    <xsl:for-each select="../gpa[1]">
                        <ScoreText>
                            <xsl:attribute name="scoreTextCode">
                                <xsl:text>GPA</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="maximumScoreText">
                                <xsl:value-of select="@max"/>
                            </xsl:attribute>
                            <xsl:value-of select="."/>
                        </ScoreText>
                    </xsl:for-each>
                </xsl:if>
            </EducationScore>
            <EducationDegree>
                <xsl:choose>
                    <xsl:when test="../degree">
                        <DegreeName>
                            <xsl:value-of select="."/>
                        </DegreeName>
                    </xsl:when>
                    <xsl:when test="../higheredu">
                        <DegreeName>
                            <xsl:value-of select="../higheredu"/>
                        </DegreeName>
                    </xsl:when>
                </xsl:choose>
                <xsl:for-each select="../major">
                    <DegreeMajor>
                        <xsl:if test="count(.)> 0 and string-length(.) > 0">
                            <ProgramName>
                                <xsl:value-of select="."/>
                            </ProgramName>
                        </xsl:if>
                    </DegreeMajor>
                </xsl:for-each>
                <xsl:for-each select="../minor">
                    <DegreeMinor>
                        <xsl:if test="count(.)> 0 and string-length(.) > 0">
                            <ProgramName>
                                <xsl:value-of select="."/>
                            </ProgramName>
                        </xsl:if>
                    </DegreeMinor>
                </xsl:for-each>
            </EducationDegree>
            <xsl:choose>
                <xsl:when test="count(../description)>0">
                    <UserArea>
                        <Description>
                            <xsl:apply-templates select="../description"/>
                            <xsl:text>
                                
                            </xsl:text>
                            <xsl:if test="count(../courses)>0">
                                <xsl:for-each select="../courses">
                                    <xsl:apply-templates select="."/>
                                </xsl:for-each>
                            </xsl:if>
                        </Description>
                    </UserArea>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="count(../courses)>0">
                        <UserArea>
                            <Description>
                                <xsl:for-each select="../courses">
                                    <xsl:if test="count(../courses) > 0 and string-length(../courses) > 0">
                                        <xsl:apply-templates select="."/>
                                    </xsl:if>
                                </xsl:for-each>
                            </Description>
                        </UserArea>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </EducationOrganizationAttendance>
    </xsl:template>
    <!--**********************************************************Skillrollup Template Declaration*******************************-->
    <xsl:template match="skillrollup">
        <xsl:if test="count(canonskill) > 0">
            <xsl:apply-templates select="canonskill"/>
        </xsl:if>
    </xsl:template>
    <!--********************************************************Canon Skill Template Declararion*********************************-->
    <xsl:template match="canonskill">
        <xsl:variable name="end" select="@end"/>
        <xsl:variable name="start" select="@start"/>
        <xsl:variable name="lastused" select="@lastused"/>
        <xsl:variable name="experience" select="@experience"/>
        <Competency>
            <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
            </xsl:attribute>
            <xsl:if test="@skillid">
                <CompetencyId>
                    <xsl:attribute name="id">
                        <xsl:value-of select="@skillid"/>
                    </xsl:attribute>
                </CompetencyId>
            </xsl:if>
            <CompetencyWeight>
                <xsl:choose>
                    <xsl:when test="($end and $start)">
                        <StringValue>
                            <xsl:variable name="exp" select="((($end)-($start)) div 365.25)"/>
                            <xsl:choose>
                                <xsl:when test="$exp &lt; 0.3">&lt; 3mths</xsl:when>
                                <xsl:when test="($exp &gt; 0.3 or $exp = 0.3) and $exp &lt; 0.6">3 - 6 mths</xsl:when>
                                <xsl:when test="($exp &gt; 0.6 or $exp = 0.6) and $exp &lt; 1">6 - 12 mths</xsl:when>
                                <xsl:when test="($exp &gt; 1 or $exp = 1) and $exp &lt; 1.5">1 - 1.5 yrs</xsl:when>
                                <xsl:when test="($exp &gt; 1.5 or $exp = 1.5) and $exp &lt; 2">1.5 - 2 yrs</xsl:when>
                                <xsl:when test="($exp &gt; 2 or $exp = 2) and $exp &lt; 3">2 - 3 yrs</xsl:when>
                                <xsl:when test="($exp &gt; 3 or $exp = 3) and $exp &lt; 4">3 - 4 yrs</xsl:when>
                                <xsl:when test="($exp &gt; 4 or $exp = 4) and $exp &lt; 5">4 - 5 yrs</xsl:when>
                                <xsl:when test="$exp &gt; 5 or $exp = 5">&gt; 5 yrs</xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="round($exp)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </StringValue>
                    </xsl:when>
                    <xsl:otherwise>
                        <StringValue/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="$lastused">
                    <SupportingInformation>
                        <xsl:text>Year last used: </xsl:text>
                        <xsl:value-of select="$lastused"/>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@education">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: education</xsl:text>
                        <xsl:if test="@eduidrefs">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="@eduidrefs"/>
                        </xsl:if>
                        <xsl:if test="not(@eduidrefs) and idrefs">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="@idrefs"/>
                        </xsl:if>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@experience">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: experience</xsl:text>
                        <xsl:if test="@expidrefs">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="@expidrefs"/>
                        </xsl:if>
                        <xsl:if test="not(@expidrefs) and idrefs">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="@idrefs"/>
                        </xsl:if>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@contact">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: contact</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@title">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: title</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@employer">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: employer</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@objective">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: objective</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@references">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: references</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@summary">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: summary</xsl:text>
                    </SupportingInformation>
                </xsl:if>
                <xsl:if test="@statements">
                    <SupportingInformation>
                        <xsl:text>Section of CV found in: statements</xsl:text>
                    </SupportingInformation>
                </xsl:if>
            </CompetencyWeight>
        </Competency>
    </xsl:template>
    <!--******************************************* Phone Template **********************************************-->
    <xsl:template match="phone">
        <xsl:choose>
            <xsl:when test="@type='work'">
                <Communication>
                    <ChannelCode>Telephone</ChannelCode>
                    <UseCode>businessDirect</UseCode>
                    <oa:DialNumber>
                        <xsl:value-of select="."/>
                    </oa:DialNumber>
                </Communication>
            </xsl:when>
            <xsl:when test="@type='home'">
                <Communication>
                    <ChannelCode>Telephone</ChannelCode>
                    <UseCode>personal</UseCode>
                    <oa:DialNumber>
                        <xsl:value-of select="."/>
                    </oa:DialNumber>
                </Communication>
            </xsl:when>
            <xsl:when test="@type='cell'">
                <Communication>
                    <ChannelCode>Mobile</ChannelCode>
                    <UseCode>personal</UseCode>
                    <oa:DialNumber>
                        <xsl:value-of select="."/>
                    </oa:DialNumber>
                </Communication>
            </xsl:when>
            <xsl:when test="@type='fax'">
                <Communication>
                    <ChannelCode>Fax</ChannelCode>
                    <oa:DialNumber>
                        <xsl:value-of select="."/>
                    </oa:DialNumber>
                </Communication>
            </xsl:when>
            <xsl:when test="@type='pager'">
                <Communication>
                    <ChannelCode>Pager</ChannelCode>
                    <oa:DialNumber>
                        <xsl:value-of select="."/>
                    </oa:DialNumber>
                </Communication>
            </xsl:when>
            <xsl:otherwise>
                <Communication>
                    <oa:DialNumber>
                        <xsl:if test="count(.) > 0 and string-length(.) > 0">
                            <xsl:value-of select="."/>
                        </xsl:if>
                    </oa:DialNumber>
                </Communication>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
