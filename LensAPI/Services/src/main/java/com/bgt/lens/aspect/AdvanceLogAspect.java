// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * AdvanceLog Aspect Event
 */
@Aspect
public class AdvanceLogAspect {

    private final Helper _helper = new Helper();

    /**
     * Profile methods execution time
     *
     * @param joinPoint
     * @return
     * @throws Exception
     * @throws Throwable
     */
    @Around("execution(public * com.bgt.lens.services.authentication.impl.AuthenticationServiceImpl.validateApiContext(..))"
            + "|| execution(public * com.bgt.lens.services.logger.impl.LoggerServiceImpl.addAuthenticationLog(..))"
            + "|| execution(public * com.bgt.lens.web.rest.UtilityController.*(..))"
            + "|| execution(* com.bgt.lens.web.soap.SoapEndPoint.*(..)) && (@annotation(com.bgt.lens.helpers.PerformanceLogging)) "
            + "|| execution(public * com.bgt.lens.services.logger.impl.LoggerServiceImpl.updateUsageCounter(..))"
            + "|| execution(public * com.bgt.lens.services.logger.impl.LoggerServiceImpl.addUsageLog(..))"
            + "|| execution(public * com.bgt.lens.services.logger.impl.LoggerServiceImpl.addErrorLog(..))"
            + "|| execution(public * com.bgt.lens.services.license.impl.LicenseManager.updateTransactionCount(..))"
            + "|| execution(public * com.bgt.lens.services.logger.impl.LoggerServiceImpl.addLensDocument(..))"
            + "|| execution(public * com.bgt.lens.repositories.impl.LensCommandBuilder.TagJobPosting(..))"
            + "|| execution(public * com.bgt.lens.repositories.impl.LensCommandBuilder.TagResumeDocument(..))"
            + "|| execution(public * com.bgt.lens.repositories.impl.LensCommandBuilder.buildFacetDistributionCommand(..))"
            + "|| execution(public * com.bgt.lens.repositories.impl.LensCommandBuilder.buildFetchDocumentCommand(..))"
            + "|| execution(public * com.bgt.lens.services.datarepository.impl.DataRepositoryServiceImpl.updateVendor(..))")
    public Object logTime(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        ApiContext apiContext = _helper.getApiContext();

        AdvanceLog advanceLog = apiContext.getAdvanceLog();
        long startTime = System.currentTimeMillis();
        
        if (joinPoint.getSignature().getName().equalsIgnoreCase("TagJobPosting") 
                || joinPoint.getSignature().getName().equalsIgnoreCase("TagResumeDocument")) {
            advanceLog.setSearchCommandbuildUnit(0.001);
            apiContext.setAdvanceLog(advanceLog);
            _helper.setApiContext(apiContext);
        }        

        Object retVal = joinPoint.proceed();
        double elapsedTime = ((System.currentTimeMillis() - startTime) / 1000.0);

        if (joinPoint.getSignature().getName().equalsIgnoreCase("validateApiContext")) {
            advanceLog.setAuthenticationValidation(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("addAuthenticationLog")) {
            advanceLog.setAuthenticationLog(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("getSearchResultResponseFromString")) {
            advanceLog.setSearchResultProcessUnit(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("getJobSearchResultResponseFromString")) {
            advanceLog.setSearchResultProcessUnit(elapsedTime);
        }           
        if (joinPoint.getSignature().getName().equalsIgnoreCase("addSearchCommandDump")) {
            advanceLog.setSearchCommandDumpUnit(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("buildSearchCommand")) {
            advanceLog.setSearchCommandbuildUnit(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("TagJobPosting") 
                || joinPoint.getSignature().getName().equalsIgnoreCase("TagResumeDocument")) {
            advanceLog.setSearchParseDocument(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("buildFacetDistributionCommand")) {
            advanceLog.setFacetDistributionCommandBuildUnit(elapsedTime);
        }
        if (joinPoint.getTarget().getClass().getName().contains("SearchServiceImpl")) {
            advanceLog.setSearchServiceUnit(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("updateVendor")) {
            advanceLog.setUpdateRegisterDocumentCount(elapsedTime);
        }
        if (joinPoint.getTarget().getClass().getName().contains("UtilityController")) {
            advanceLog.setUtilityDataProcessing(elapsedTime);
        }
        if (joinPoint.getTarget().getClass().getName().contains("SearchController")) {
            advanceLog.setSearchControllerUnit(elapsedTime);
        }
        if (joinPoint.getTarget().getClass().getName().contains("SoapEndPoint")) {
            advanceLog.setUtilityDataProcessing(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("updateUsageCounter")) {
            advanceLog.setUsageCounterUpdate(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("addUsageLog")) {
            advanceLog.setUsageLog(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("addErrorLog")) {
            advanceLog.setErrorLog(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("updateTransactionCount")) {
            advanceLog.setUpdateTransactionCount(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("addLensDocument")) {
            advanceLog.setLensDocumentDump(elapsedTime);
        }
        apiContext.setIsAdvanceLog(Boolean.TRUE);
        apiContext.setAdvanceLog(advanceLog);

        _helper.setApiContext(apiContext);
        return retVal;
    }

    /**
     * Profile methods execution time of Lens Communication
     *
     * @param joinPoint
     * @return
     * @throws Exception
     * @throws Throwable
     */
    @Around("execution(* com.bgt.lens.repositories.impl.LensRepository.*(..)) && (@annotation(com.bgt.lens.helpers.PerformanceLogging))")
    public Object logTimeLensCommunication(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        ApiContext apiContext = _helper.getApiContext();

        AdvanceLog advanceLog = apiContext.getAdvanceLog();
        long startTime = System.currentTimeMillis();
        Object retVal = joinPoint.proceed();
        double elapsedTime = ((System.currentTimeMillis() - startTime) / 1000.0);

        if (joinPoint.getSignature().getName().equalsIgnoreCase("tagBinaryData")) {
            if(apiContext.getApi().equalsIgnoreCase(com.bgt.lens.helpers.Enum
                    .ApplicationAPI.SearchService.toString())  && advanceLog.getSearchCommandbuildUnit() == 0){
                advanceLog.setRegisterParseDocument(elapsedTime);
            }
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("registerDocument")) {
            if(advanceLog.getUnregisterDocumentUnit() != 0){
                advanceLog.setOverwriteRegisterDocumentUnit(elapsedTime);
            }
            else{
                advanceLog.setRegisterDocumentWithoutCustomFilterUnit(elapsedTime);
            }
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("unregisterDocument")) {
            advanceLog.setUnregisterDocumentUnit(elapsedTime);
        }
        if (joinPoint.getSignature().getName().equalsIgnoreCase("sendXMLCommand")) {
            if(advanceLog.getSearchCommandbuildUnit() != 0){
                advanceLog.setSearchCommandProcessUnit(elapsedTime);
            }
            if(advanceLog.getFacetDistributionCommandBuildUnit() != 0){
                advanceLog.setFacetDistributionCommandProcessUnit(elapsedTime);
            }
            if(advanceLog.getRegisterParseDocument()!= 0){
                if(advanceLog.getUnregisterDocumentUnit() == 0){
                    advanceLog.setRegisterDocumentWithCustomFilterUnit(elapsedTime);
                }
                else
                {
                    advanceLog.setOverwriteRegisterDocumentUnit(elapsedTime);
                }
            }
            
            if(advanceLog.getSearchCommandbuildUnit() == 0 && advanceLog.getFacetDistributionCommandBuildUnit() == 0 
                && advanceLog.getRegisterParseDocument()== 0){
                if(apiContext.getParseVariant() != null && apiContext.getParseVariant().equalsIgnoreCase(com.bgt.lens.helpers.Enum.VariantType.fetch.toString())){
                    advanceLog.setFetchDocumentProcessUnit(elapsedTime);
                }
                else if(apiContext.getParseVariant() != null && apiContext.getParseVariant().equalsIgnoreCase(com.bgt.lens.helpers.Enum.VariantType.register.toString())){
                    if(advanceLog.getUnregisterDocumentUnit() != 0){
                        advanceLog.setOverwriteRegisterDocumentUnit(elapsedTime);
                    }
                    else{
                        if(advanceLog.getRegisterParseDocument()!= 0){
                            advanceLog.setRegisterDocumentWithCustomFilterUnit(elapsedTime);
                        }
                        else{
                            advanceLog.setRegisterDocumentWithoutCustomFilterUnit(elapsedTime);
                        }
                    }
                }
            }
        }
        if (joinPoint.getTarget().getClass().getName().contains("LensRepository")) {
            advanceLog.setLensCommunication(elapsedTime);
        }
        apiContext.setIsAdvanceLog(Boolean.TRUE);
        apiContext.setAdvanceLog(advanceLog);

        _helper.setApiContext(apiContext);
        return retVal;
    }
    
    /**
     * Profile methods execution time of Lens parsing alone
     *
     * @param joinPoint
     * @return
     * @throws Exception
     * @throws Throwable
     */
    @Around("execution(public * com.bgt.lens.repositories.impl.LensParseData.tagDocument(..)) || execution(* com.bgt.lens.ld.impl.LensLDParseDataImpl.tagDocument(..))")
    public Object logTimeLensParsing(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        ApiContext apiContext = _helper.getApiContext();

        AdvanceLog advanceLog = apiContext.getAdvanceLog();
        long startTime = System.currentTimeMillis();
        Object retVal = joinPoint.proceed();
        double elapsedTime = ((System.currentTimeMillis() - startTime) / 1000.0);

        if (joinPoint.getTarget().getClass().getName().contains("LensParseData") || joinPoint.getTarget().getClass().getName().contains("LensLDParseData")) {
            advanceLog.setParsingDump(elapsedTime);
        }
        apiContext.setIsAdvanceLog(Boolean.TRUE);
        apiContext.setAdvanceLog(advanceLog);

        _helper.setApiContext(apiContext);
        return retVal;
    }
    
    /**
     * Profile methods execution time of locale detection
     *
     * @param joinPoint
     * @return
     * @throws Exception
     * @throws Throwable
     */
    @Around("execution(* com.bgt.lens.ld.impl.LensLDParseDataImpl.processLocale(..)) && (@annotation(com.bgt.lens.helpers.PerformanceLogging)) ")
    public Object logTimeLocaleDetect(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        ApiContext apiContext = _helper.getApiContext();

        AdvanceLog advanceLog = apiContext.getAdvanceLog();
        long startTime = System.currentTimeMillis();
        Object retVal = joinPoint.proceed();
        double elapsedTime = ((System.currentTimeMillis() - startTime) / 1000.0);

        if (joinPoint.getTarget().getClass().getName().contains("LensLDParseData")) {
            advanceLog.setLocaleDetectionDump(elapsedTime);
        }
        apiContext.setIsAdvanceLog(Boolean.TRUE);
        apiContext.setAdvanceLog(advanceLog);

        _helper.setApiContext(apiContext);
        return retVal;
    }
    
    /**
     * Profile methods execution time of HRXML stylesheet update
     *
     * @param joinPoint
     * @return
     * @throws Exception
     * @throws Throwable
     */
    @Around("execution(public * com.bgt.lens.helpers.Helper.*(..)) && (@annotation(com.bgt.lens.helpers.PerformanceLogging)) ")
    public Object logTimeHRXMLStylesheetUpdate(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        ApiContext apiContext = _helper.getApiContext();

        AdvanceLog advanceLog = apiContext.getAdvanceLog();
        long startTime = System.currentTimeMillis();
        Object retVal = joinPoint.proceed();
        double elapsedTime = ((System.currentTimeMillis() - startTime) / 1000.0);

        if (joinPoint.getTarget().getClass().getName().contains("Helper")) {
            advanceLog.setHrxmlStyleSheetUpdate(elapsedTime);
        }
        apiContext.setIsAdvanceLog(Boolean.TRUE);
        apiContext.setAdvanceLog(advanceLog);

        _helper.setApiContext(apiContext);
        return retVal;
    }

}
