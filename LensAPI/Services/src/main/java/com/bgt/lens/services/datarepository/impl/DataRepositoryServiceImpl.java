// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.datarepository.impl;

import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.AggregatedLensDocumentCriteria;
import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.ErrorLogCriteria;
import com.bgt.lens.model.criteria.LensSettingsCriteria;
import com.bgt.lens.model.criteria.ResourceCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.criteria.SearchFilterTypeCriteria;
import com.bgt.lens.model.criteria.SearchLookupCriteria;
import com.bgt.lens.model.criteria.SearchRegisterLogCriteria;
import com.bgt.lens.model.criteria.SearchVendorSettingsCriteria;
import com.bgt.lens.model.criteria.UsageLogCriteria;
import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.dao.ICoreApiDao;
import com.bgt.lens.model.dao.ICoreAuthenticationlogDao;
import com.bgt.lens.model.dao.ICoreClientDao;
import com.bgt.lens.model.dao.ICoreClientapiDao;
import com.bgt.lens.model.dao.ICoreErrorlogDao;
import com.bgt.lens.model.dao.ICoreLensdocumentDao;
import com.bgt.lens.model.dao.ICoreLenssettingsDao;
import com.bgt.lens.model.dao.ICoreResourceDao;
import com.bgt.lens.model.dao.ICoreUsagecounterDao;
import com.bgt.lens.model.dao.ICoreUsagelogDao;
import com.bgt.lens.model.dao.ISearchCommandDumpDao;
import com.bgt.lens.model.dao.ISearchFilterLogDao;
import com.bgt.lens.model.dao.ISearchFilterTypeDao;
import com.bgt.lens.model.dao.ISearchLookupDao;
import com.bgt.lens.model.dao.ISearchRegisterLogDao;
import com.bgt.lens.model.dao.ISearchVendorSettingsDao;
import com.bgt.lens.model.entity.AggregatedCoreLensDocument;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreAuthenticationlog;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreErrorlog;
import com.bgt.lens.model.entity.CoreFailoverlenssettings;
import com.bgt.lens.model.entity.CoreLensdocument;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.CoreResources;
import com.bgt.lens.model.entity.CoreUsagecounter;
import com.bgt.lens.model.entity.CoreUsagelog;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchFilterType;
import com.bgt.lens.model.entity.SearchLookup;
import com.bgt.lens.model.entity.SearchRegisterlog;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;

/**
 *
 * Data repository service Implementation
 */
public class DataRepositoryServiceImpl implements IDataRepositoryService {

    private ICoreApiDao coreApiDao;
    private ICoreAuthenticationlogDao coreAuthenticationlogDao;
    private ICoreClientDao coreClientDao;
    private ICoreClientapiDao coreClientapiDao;
    private ICoreErrorlogDao coreErrorlogDao;
    private ICoreLensdocumentDao coreLensdocumentDao;
    private ICoreLenssettingsDao coreLenssettingsDao;
    private ICoreUsagecounterDao coreUsagecounterDao;
    private ICoreUsagelogDao coreUsagelogDao;
    private ICoreResourceDao coreResourceDao;
    private ISearchVendorSettingsDao searchVendorSettingsDao;
    private ISearchLookupDao searchLookupDao;
    private ISearchCommandDumpDao searchCommandDumpDao;
    private ISearchRegisterLogDao searchRegisterLogDao;
    private ApiConfiguration apiConfig;
    private ISearchFilterTypeDao searchFilterTypeDao;
    private ISearchFilterLogDao searchFilterLogDao;
    
    private final Helper _helper = new Helper();

    /**
     * Set core API dao
     *
     * @param coreApiDao
     */
    public void setCoreApiDao(ICoreApiDao coreApiDao) {
        this.coreApiDao = coreApiDao;
    }

    /**
     * Set authentication log dao
     *
     * @param coreAuthenticationlogDao
     */
    public void setCoreAuthenticationlogDao(ICoreAuthenticationlogDao coreAuthenticationlogDao) {
        this.coreAuthenticationlogDao = coreAuthenticationlogDao;
    }

    /**
     * Set client dao
     *
     * @param coreClientDao
     */
    public void setCoreClientDao(ICoreClientDao coreClientDao) {
        this.coreClientDao = coreClientDao;
    }

    /**
     * Set client API dao
     *
     * @param coreClientapiDao
     */
    public void setCoreClientapiDao(ICoreClientapiDao coreClientapiDao) {
        this.coreClientapiDao = coreClientapiDao;
    }

    /**
     * Set error log dao
     *
     * @param coreErrorlogDao
     */
    public void setCoreErrorlogDao(ICoreErrorlogDao coreErrorlogDao) {
        this.coreErrorlogDao = coreErrorlogDao;
    }

    /**
     * Set Lens document dao
     *
     * @param coreLensdocumentDao
     */
    public void setCoreLensdocumentDao(ICoreLensdocumentDao coreLensdocumentDao) {
        this.coreLensdocumentDao = coreLensdocumentDao;
    }

    /**
     * Set Lens settings dao
     *
     * @param coreLenssettingsDao
     */
    public void setCoreLenssettingsDao(ICoreLenssettingsDao coreLenssettingsDao) {
        this.coreLenssettingsDao = coreLenssettingsDao;
    }

    /**
     * Set usage counter dao
     *
     * @param coreUsagecounterDao
     */
    public void setCoreUsagecounterDao(ICoreUsagecounterDao coreUsagecounterDao) {
        this.coreUsagecounterDao = coreUsagecounterDao;
    }

    /**
     * Set Usage log dao
     *
     * @param coreUsagelogDao
     */
    public void setCoreUsagelogDao(ICoreUsagelogDao coreUsagelogDao) {
        this.coreUsagelogDao = coreUsagelogDao;
    }

    /**
     * Set resource dao
     *
     * @param coreResourceDao
     */
    public void setCoreResourceDao(ICoreResourceDao coreResourceDao) {
        this.coreResourceDao = coreResourceDao;
    }

    /**
     * Set ApiConfig
     *
     * @param apiConfig
     */
    public void setapiConfig(ApiConfiguration apiConfig) {
        this.apiConfig = apiConfig;
    }

    /**
     * Get search vendor settings dao
     *
     * @return
     */
    public ISearchVendorSettingsDao getSearchVendorSettingsDao() {
        return searchVendorSettingsDao;
    }

    /**
     * Set search vendor settings
     *
     * @param searchVendorSettingsDao
     */
    public void setSearchVendorSettingsDao(ISearchVendorSettingsDao searchVendorSettingsDao) {
        this.searchVendorSettingsDao = searchVendorSettingsDao;
    }

    /**
     * Set search command dump
     *
     * @param searchCommandDumpDao
     */
    public void setSearchCommandDumpDao(ISearchCommandDumpDao searchCommandDumpDao) {
        this.searchCommandDumpDao = searchCommandDumpDao;
    }

    /**
     * Set search register log dao
     *
     * @param searchRegisterLogDao
     */
    public void setSearchRegisterLogDao(ISearchRegisterLogDao searchRegisterLogDao) {
        this.searchRegisterLogDao = searchRegisterLogDao;
    }

    /**
     * Get search lookup dao
     *
     * @return
     */
    public ISearchLookupDao getSearchLookupDao() {
        return searchLookupDao;
    }

    /**
     * Set search lookup Dao
     *
     * @param searchLookupDao
     */
    public void setSearchLookupDao(ISearchLookupDao searchLookupDao) {
        this.searchLookupDao = searchLookupDao;
    }

    /**
     * Set Search Filter Log Dao
     *
     * @return
     */
    public ISearchFilterLogDao getSearchFilterLogDao() {
        return searchFilterLogDao;
    }

    /**
     * Get Search Filter Log Dao
     *
     * @param searchFilterLogDao
     */
    public void setSearchFilterLogDao(ISearchFilterLogDao searchFilterLogDao) {
        this.searchFilterLogDao = searchFilterLogDao;
    }

    /**
     * Set search filter Type Dao
     *
     * @param searchFilterTypeDao
     */
    public void setSearchFilterTypeDao(ISearchFilterTypeDao searchFilterTypeDao) {
        this.searchFilterTypeDao = searchFilterTypeDao;
    }

    private static final Logger LOGGER = LogManager.getLogger(DataRepositoryServiceImpl.class);

    // CoreClient
    /**
     * Create client
     *
     * @param coreClient
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse createClient(CoreClient coreClient) throws Exception {
         LOGGER.info("Add consumer data to database. Key - " + coreClient.getClientKey());
        ApiResponse response = new ApiResponse();
        try {
            coreClientDao.save(coreClient);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add consumer. Exception - " + ex.getMessage());

            if (ex.getClass().equals(ConstraintViolationException.class)) {
                String constraintName = ((ConstraintViolationException)ex).getSQLException().getMessage();
                if(constraintName != null){
                    if(constraintName.contains("UQ_Core_Client_ClientKey")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_KEY_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("UQ_Core_Client_Name")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_NAME_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_ClientLensSettings_Core_LensSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENSSETTINGS_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_ClientResources_Core_Resources")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_ClientLensSettings_Core_CustomSkillSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CUSTOMSKILL_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Search_ClientVendorSettings_Search_VendorSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.VENDORSETTINGS_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_AllowedTransactions")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_ALLOWED_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_RemainingTransactions")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_REMAINING_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_ApiId")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_ClientId")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("PK_Core_ClientResources")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_LIST), HttpStatus.BAD_REQUEST);
                    }
                    else{
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }else{
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            else if(ex.getClass().equals(DataException.class)){
                String constraintName = ((DataException)ex).getSQLException().getMessage();
                if(constraintName != null && constraintName.toLowerCase().contains("string or binary data would be truncated")){
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATA_LENGTH), HttpStatus.BAD_REQUEST);
                }
            }
            else if (ex.getClass().equals(DataIntegrityViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_OR_LENS_OR_RESOURCE), HttpStatus.BAD_REQUEST);
            } else if (ex.getClass().equals(DuplicateKeyException.class)) {                
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
            } else if (ex.getClass().equals(NonUniqueObjectException.class)) {
                String expectionMessage = ((NonUniqueObjectException)ex).getMessage();
                if(expectionMessage.contains("CoreClientlenssettings")){
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_LENSSETTINGS), HttpStatus.BAD_REQUEST);
                }
                else{
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_API_KEY_IN_CLIENT_API), HttpStatus.BAD_REQUEST);
                }
            }

            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update client
     *
     * @param coreClient
     * @param updatedCoreClient
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateClient(CoreClient coreClient, CoreClient updatedCoreClient) throws Exception {
        LOGGER.info("Update consumer data to database. Key - " + coreClient.getClientKey());
        ApiResponse response = new ApiResponse();
        try {
            coreClientDao.update(coreClient, updatedCoreClient);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update consumer data. Exception - " + ex.getMessage());
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                String constraintName = ((ConstraintViolationException)ex).getSQLException().getMessage();
                if(constraintName != null){
                    if(constraintName.contains("UQ_Core_Client_ClientKey")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_KEY_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("UQ_Core_Client_Name")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_NAME_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_ClientLensSettings_Core_LensSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENSSETTINGS_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_ClientResources_Core_Resources")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_ClientLensSettings_Core_CustomSkillSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CUSTOMSKILL_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Search_ClientVendorSettings_Search_VendorSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.VENDORSETTINGS_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_AllowedTransactions")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_ALLOWED_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_RemainingTransactions")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_REMAINING_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_ApiId")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID_NUMBER), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_ClientId")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID_NUMBER), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("PK_Core_ClientResources")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_LIST), HttpStatus.BAD_REQUEST);
                    }
                    else{
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }else{
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            else if(ex.getClass().equals(DataException.class)){
                String constraintName = ((DataException)ex).getSQLException().getMessage();
                if(constraintName != null && constraintName.toLowerCase().contains("string or binary data would be truncated")){
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATA_LENGTH), HttpStatus.BAD_REQUEST);
                }
            }
            else if (ex.getClass().equals(DataIntegrityViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_OR_LENS_OR_RESOURCE), HttpStatus.BAD_REQUEST);
            } else if (ex.getClass().equals(DuplicateKeyException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
            }
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update client
     *
     * @param coreClient
     * @param updatedCoreClient
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateClient(Set<BigInteger> id, Map<BigInteger,CoreClient>coreClient,Map<BigInteger,CoreClient> updatedCoreClient) throws Exception {
//        LOGGER.info("Update consumer data to database. Key - " + coreClient.getClientKey());
        ApiResponse response = new ApiResponse();        
        try {
            coreClientDao.update(id,coreClient, updatedCoreClient);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update consumer data. Exception - " + ex.getMessage());
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                String constraintName = ((ConstraintViolationException)ex).getSQLException().getMessage();
                if(constraintName != null){
                    if(constraintName.contains("UQ_Core_Client_ClientKey")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_KEY_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("UQ_Core_Client_Name")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_NAME_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_ClientLensSettings_Core_LensSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENSSETTINGS_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_ClientResources_Core_Resources")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Search_ClientVendorSettings_Search_VendorSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.VENDORSETTINGS_ID_NOT_EXIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_AllowedTransactions")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_ALLOWED_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_RemainingTransactions")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_REMAINING_TRANSACTION_COUNT), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_ApiId")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID_NUMBER), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("CK_Core_ClientApi_ClientId")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID_NUMBER), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("PK_Core_ClientResources")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_LIST), HttpStatus.BAD_REQUEST);
                    }
                    else{
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }else{
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            else if(ex.getClass().equals(DataException.class)){
                String constraintName = ((DataException)ex).getSQLException().getMessage();
                if(constraintName != null && constraintName.toLowerCase().contains("string or binary data would be truncated")){
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATA_LENGTH), HttpStatus.BAD_REQUEST);
                }
            }
            else if (ex.getClass().equals(DataIntegrityViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_OR_LENS_OR_RESOURCE), HttpStatus.BAD_REQUEST);
            } else if (ex.getClass().equals(DuplicateKeyException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
            }
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete client
     *
     * @param coreClient
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse deleteClient(CoreClient coreClient) throws Exception {
        LOGGER.info("Delete consumer from database. ClientKey - " + coreClient.getClientKey());
        ApiResponse response = new ApiResponse();
        try {
            coreClientDao.delete(coreClient);
            response.status = true;

        } catch (Exception ex) {
            LOGGER.error("Delete consumer data. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CONSUMER_DELETE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get client by Id
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public CoreClient getClientById(int clientId) throws Exception {
        try {
            return coreClientDao.getClientById(clientId);
        } catch (Exception ex) {
            LOGGER.error("Get consumer by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_CLIENTS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get client by criteria
     *
     * @param clientCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<CoreClient> getClientByCriteria(ClientCriteria clientCriteria) throws Exception {
        try {
//            clientCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return coreClientDao.getClientByCriteria(clientCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get consumer by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_CLIENTS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CoreApi
    /**
     * Create API
     *
     * @param coreApi
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse createApi(CoreApi coreApi) throws Exception {
        LOGGER.info("Add Api data to database. ApiKey - " + coreApi.getApiKey());
        ApiResponse response = new ApiResponse();
        try {
            coreApiDao.save(coreApi);
            response.status = true;
        } catch (Exception ex) {
            response.status = false;
            LOGGER.error("Add Api. Exception - " + ex.getMessage());
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                String constraintName = ((ConstraintViolationException)ex).getSQLException().getMessage();
                if(constraintName != null){
                    if(constraintName.contains("UQ_Core_Api_APiKey")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_KEY_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("UQ_Core_Api_Name")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_NAME_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else{
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }else{
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
                //throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY_NAME), HttpStatus.BAD_REQUEST);
            }
            else if(ex.getClass().equals(DataException.class)){
                String constraintName = ((DataException)ex).getSQLException().getMessage();
                if(constraintName != null && constraintName.toLowerCase().contains("string or binary data would be truncated")){
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATA_LENGTH), HttpStatus.BAD_REQUEST);
                }
            }
            else if (ex.getClass().equals(DataIntegrityViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY_NAME), HttpStatus.BAD_REQUEST);
            } else if (ex.getClass().equals(DuplicateKeyException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_API_KEY), HttpStatus.BAD_REQUEST);
            }
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update API
     *
     * @param coreApi
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateApi(CoreApi coreApi) throws Exception {
        LOGGER.info("Update Api data to database. ApiKey - " + coreApi.getApiKey());
        ApiResponse response = new ApiResponse();
        try {
            coreApiDao.update(coreApi);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update Api. Exception - " + ex.getMessage());
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                String constraintName = ((ConstraintViolationException)ex).getSQLException().getMessage();
                if(constraintName != null){
                    if(constraintName.contains("UQ_Core_Api_APiKey")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_KEY_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("UQ_Core_Api_Name")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_NAME_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
                    }
                    else{
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }else{
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            else if(ex.getClass().equals(DataException.class)){
                String constraintName = ((DataException)ex).getSQLException().getMessage();
                if(constraintName != null && constraintName.toLowerCase().contains("string or binary data would be truncated")){
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATA_LENGTH), HttpStatus.BAD_REQUEST);
                }
            }
            else if (ex.getClass().equals(DataIntegrityViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY_NAME), HttpStatus.BAD_REQUEST);
            } else if (ex.getClass().equals(DuplicateKeyException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_API_KEY), HttpStatus.BAD_REQUEST);
            }
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete API
     *
     * @param coreApi
     * @return
     */
    @Override
    public ApiResponse deleteApi(CoreApi coreApi) {
        LOGGER.info("Delete Api from database. ApiId - " + coreApi.getId());
        ApiResponse response = new ApiResponse();
        try {
            coreApiDao.delete(coreApi);
            response.status = true;

        } catch (Exception ex) {
            LOGGER.error("Delete Api. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.API_DELETE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get API by Id
     *
     * @param apiId
     * @return
     * @throws Exception
     */
    @Override
    public CoreApi getApiById(int apiId) throws Exception {
        try {
            return coreApiDao.getApiById(apiId);
        } catch (Exception ex) {
            LOGGER.error("Get Api by id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_API_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get API by criteria
     *
     * @param apiCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<CoreApi> getApiByCriteria(ApiCriteria apiCriteria) throws Exception {
        try {
//            apiCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return coreApiDao.getApiByCriteria(apiCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get Api by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_API_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * Get API by criteria
     *
     * @param apiCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<CoreApi> getDuplicateApiByCriteria(ApiCriteria apiCriteria) throws Exception {
        try {
//            apiCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return coreApiDao.findApiDuplicateByCriteria(apiCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get Duplicate Api by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_API_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CoreClientapi
    /**
     * Create client API
     *
     * @param coreClientapi
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse createClientApi(CoreClientapi coreClientapi) throws Exception {
        LOGGER.info("Add ClientApi data to database: " + coreClientapi.getId_1());
        ApiResponse response = new ApiResponse();
        try {
            coreClientapiDao.save(coreClientapi);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add ClientApi. Exception - " + ex.getMessage());
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID), HttpStatus.BAD_REQUEST);
            }
            if (ex.getClass().equals(DataIntegrityViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID), HttpStatus.BAD_REQUEST);
            }
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.ADD_CLIENTAPI_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update Client API
     *
     * @param coreClientapi
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateClientApi(CoreClientapi coreClientapi) throws Exception {
        LOGGER.info("Update ClientApi data to database. ClientApiId" + coreClientapi.getId_1());
        ApiResponse response = new ApiResponse();
        try {
            coreClientapiDao.update(coreClientapi);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Exception class - " + ex.getClass());
            LOGGER.error("Update ClientApi. Exception - " + ex.getMessage());
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                String constraintName = ((ConstraintViolationException)ex).getSQLException().getMessage();
                if(constraintName != null){
                    if(constraintName.contains("Remaining_Transactions_Positive")){
                        Date expiryDate = new LocalDate(coreClientapi.getExpiryDate()).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate();
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LICENSE_EXPIRED)+ " Expiry Date: " + new LocalDate(expiryDate) + ", Remaining Transactions: " + coreClientapi.getRemainingTransactions(), HttpStatus.UNAUTHORIZED);
                    }
                }
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID), HttpStatus.BAD_REQUEST);
            }
            if (ex.getClass().equals(DataIntegrityViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID), HttpStatus.BAD_REQUEST);
            }
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UPDATE_CLIENTAPI_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete Client API
     *
     * @param coreClientapi
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse deleteClientApi(CoreClientapi coreClientapi) throws Exception {
        LOGGER.info("Delete ClientApi data from database. ClientapiId - " + coreClientapi.getId_1());
        ApiResponse response = new ApiResponse();
        try {

            coreClientapiDao.delete(coreClientapi);
            response.status = true;

        } catch (Exception ex) {
            LOGGER.error("Delete ClientApi. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DELETE_CLIENTAPI_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get Client API by id
     *
     * @param clientApiId
     * @return
     */
    @Override
    public CoreClientapi getClientApiById(int clientApiId) {
        try {
            LOGGER.info("Get ClientApi by Id. ClientApiId - " + clientApiId);
            return coreClientapiDao.getClientApiById(clientApiId);
        } catch (Exception ex) {
            LOGGER.error("Get ClientApi by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_CLIENTAPI_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get Client API by criteria
     *
     * @param clientapiCriteria
     * @return
     */
    @Override
    public List<CoreClientapi> getClientApiByCriteria(ClientapiCriteria clientapiCriteria) {
        try {
//            clientapiCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return coreClientapiDao.getClientApiByCriteria(clientapiCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get ClientApi by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_CLIENTAPI_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CoreLenssettings
    /**
     * Create Lens settings
     *
     * @param coreLenssettings
     * @return
     */
    @Override
    public ApiResponse createLensSettings(CoreLenssettings coreLenssettings) {
        ApiResponse response = new ApiResponse();
        try {
            coreLenssettingsDao.save(coreLenssettings);
            response.status = true;
        } catch (Exception ex) {
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                String constraintName = ((ConstraintViolationException)ex).getSQLException().getMessage();
                if(constraintName != null){
                    if(constraintName.contains("UQ_LENS_SETTINGS")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UNIQUE_LENS_SETTINGS_VIOLATION_EXCEPTION), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_LensSettings_Core_FailoverLensSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_FAILOVER_ID_LIST), HttpStatus.BAD_REQUEST);
                    }
                    else{
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
                else{
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            else if(ex.getClass().equals(DataException.class)){
                String constraintName = ((DataException)ex).getSQLException().getMessage();
                if(constraintName != null && constraintName.toLowerCase().contains("string or binary data would be truncated")){
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATA_LENGTH), HttpStatus.BAD_REQUEST);
                }
            }
            LOGGER.error("Add Lens Settings to database. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update Lens Settings
     *
     * @param coreLenssettings
     * @param deleteCoreCustomskillsettings
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateLensSettings(CoreLenssettings coreLenssettings, Set<CoreCustomskillsettings> deleteCoreCustomskillsettings) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            coreLenssettingsDao.update(coreLenssettings, deleteCoreCustomskillsettings);
            response.status = true;
        } catch (Exception ex) {
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                String constraintName = ((ConstraintViolationException)ex).getSQLException().getMessage();
                if(constraintName != null){
                    if(constraintName.contains("UQ_LENS_SETTINGS")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UNIQUE_LENS_SETTINGS_VIOLATION_EXCEPTION), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_LensSettings_Core_FailoverLensSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_FAILOVER_ID_LIST), HttpStatus.BAD_REQUEST);
                    }
                    else if(constraintName.contains("FK_Core_ClientLensSettings_Core_CustomSkillSettings")){
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.CUSTOMSKILL_CODE_USED_BY_CLIENT), HttpStatus.BAD_REQUEST);
                    }
                    else{
                        throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
                else{
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            else if(ex.getClass().equals(DataException.class)){
                String constraintName = ((DataException)ex).getSQLException().getMessage();
                if(constraintName != null && constraintName.toLowerCase().contains("string or binary data would be truncated")){
                    throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATA_LENGTH), HttpStatus.BAD_REQUEST);
                }
            }
            LOGGER.error("Update Lens Settings to database. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete Lens settings
     *
     * @param coreLenssettings
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse deleteLensSettings(CoreLenssettings coreLenssettings) throws Exception {
        ApiResponse response = new ApiResponse();
        try {

            coreLenssettingsDao.delete(coreLenssettings);
            response.status = true;

        } catch (Exception ex) {
            LOGGER.error("Delete LensSettings. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.LENS_DELETE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get Lens Settings by Id
     *
     * @param settingsId
     * @return
     * @throws Exception
     */
    @Override
    public CoreLenssettings getLensSettingsById(int settingsId) throws Exception {
        try {
            return coreLenssettingsDao.getLensSettingsById(settingsId);
        } catch (Exception ex) {
            LOGGER.error("Get Lens Settings by id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LENS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
       /**
     * Get Failover Lens Settings by Id
     *
     * @param settingsId
     * @return
     * @throws Exception
     */
    @Override
    public CoreFailoverlenssettings getFailoverLensSettingsById(int settingsId) throws Exception {
        try {
            return coreLenssettingsDao.getFailoverLensSettingsById(settingsId);
        } catch (Exception ex) {
            LOGGER.error("Get Lens Settings by id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LENS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get Lens Settings by criteria
     *
     * @param lensSettingsCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<CoreLenssettings> getLensSettingsByCriteria(LensSettingsCriteria lensSettingsCriteria) throws Exception {
        try {
            return coreLenssettingsDao.getLensSettingsByCriteria(lensSettingsCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get Lens settings by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LENS_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CoreAuthenticationlog
    /**
     * Create authentication log
     *
     * @param coreAuthenticationlog
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse createAuthenticationlog(CoreAuthenticationlog coreAuthenticationlog) throws Exception {
        LOGGER.info("Add Authentication log data to database. RequestId - " + coreAuthenticationlog.getRequestId());
        ApiResponse response = new ApiResponse();
        try {
            coreAuthenticationlogDao.save(coreAuthenticationlog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add Authentication log. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.ADD_AUTHENTICATIONLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update authentication log
     *
     * @param coreAuthenticationlog
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateAuthenticationlog(CoreAuthenticationlog coreAuthenticationlog) throws Exception {
        LOGGER.info("Update Authentication log data to database. requestid - " + coreAuthenticationlog.getRequestId());
        ApiResponse response = new ApiResponse();
        try {
            coreAuthenticationlogDao.update(coreAuthenticationlog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update Authentication log. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UPDATE_AUTHENTICATIONLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete authentication log
     *
     * @param coreAuthenticationlog
     * @return
     */
    @Override
    public ApiResponse deleteAuthenticationlog(CoreAuthenticationlog coreAuthenticationlog) {
        LOGGER.info("Delete Authentication log data from database. RequestId - " + coreAuthenticationlog.getRequestId());
        ApiResponse response = new ApiResponse();
        try {
            coreAuthenticationlogDao.delete(coreAuthenticationlog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Delete Authentication log. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DELETE_AUTHENTICATIONLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get authentication log by Id
     *
     * @param authenticationlogId
     * @return
     * @throws Exception
     */
    @Override
    public CoreAuthenticationlog getAuthenticationlogById(int authenticationlogId) throws Exception {
        try {
            return coreAuthenticationlogDao.getAuthenticationLogById(authenticationlogId);
        } catch (Exception ex) {
            LOGGER.error("Get authentication log by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_AUTHENTICATIONLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * Get authentication log by request Id
     *
     * @param requestId
     * @return
     * @throws Exception
     */
    @Override
    public CoreAuthenticationlog getAuthenticationlogByRequestId(String requestId) throws Exception {
        try {
            return coreAuthenticationlogDao.getAuthenticationLogByRequestId(requestId);
        } catch (Exception ex) {
            LOGGER.error("Get authentication log by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_AUTHENTICATIONLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create usage counter
     *
     * @param coreUsagecounter
     * @return
     */
    // CoreUsagecounter
    @Override
    public ApiResponse createUsagecounter(CoreUsagecounter coreUsagecounter) {
        LOGGER.info("Add Usagecounter data to database. ClientId - " + coreUsagecounter.getClientId());
        ApiResponse response = new ApiResponse();
        try {
            coreUsagecounterDao.save(coreUsagecounter);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add Usagecounter. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UPDATE_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update usage counter
     *
     * @param coreUsagecounter
     * @return
     */
    @Override
    public ApiResponse updateUsagecounter(CoreUsagecounter coreUsagecounter) {
        LOGGER.info("Update Usagecounter data to database. ClientId - " + coreUsagecounter.getClientId());
        ApiResponse response = new ApiResponse();
        try {
            coreUsagecounterDao.update(coreUsagecounter);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update Usagecounter. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UPDATE_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete usage counter
     *
     * @param coreUsagecounter
     * @return
     */
    @Override
    public ApiResponse deleteUsagecounter(CoreUsagecounter coreUsagecounter) {
        LOGGER.info("Delete Usagecounter data from database. ClientId" + coreUsagecounter.getClientId());
        ApiResponse response = new ApiResponse();
        try {
            coreUsagecounterDao.delete(coreUsagecounter);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Delete Usagecounter. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DELETE_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get usage counter by Id
     *
     * @param usageCounterId
     * @return
     * @throws Exception
     */
    @Override
    public CoreUsagecounter getUsagecounterById(int usageCounterId) throws Exception {
        try {
            return coreUsagecounterDao.getUsageCounterById(usageCounterId);
        } catch (Exception ex) {
            LOGGER.error("Get Usagecounter by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get usage counter by id and client
     *
     * @param usageLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public CoreUsagecounter getUsagecounterById(int usageLogId, int clientId) throws Exception {
        try {
            return coreUsagecounterDao.getUsageCounterById(usageLogId, clientId);
        } catch (Exception ex) {
            LOGGER.error("Get Usagecounter by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get usage counter by criteria
     *
     * @param usagecounterCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<CoreUsagecounter> getUsagecounterByCriteria(UsagecounterCriteria usagecounterCriteria) throws Exception {
        try {
            //usagecounterCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return coreUsagecounterDao.getUsageCounterByCriteria(usagecounterCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get Usagecounter by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGECOUNTER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CoreUsagelog
    /**
     * Create usage log
     *
     * @param coreUsagelog
     * @return
     */
    @Override
    public ApiResponse createUsagelog(CoreUsagelog coreUsagelog) {

        LOGGER.info("Add Usagelog data to database. RequestId - " + coreUsagelog.getRequestId());
        ApiResponse response = new ApiResponse();
        try {
            coreUsagelogDao.save(coreUsagelog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add Usagelog to database. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.ADD_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update usage log
     *
     * @param coreUsagelog
     * @return
     */
    @Override
    public ApiResponse updateUsagelog(CoreUsagelog coreUsagelog) {
        LOGGER.info("Update Usagelog data to database. RequestId - " + coreUsagelog.getRequestId());
        ApiResponse response = new ApiResponse();
        try {
            coreUsagelogDao.update(coreUsagelog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update Usagelog. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UPDATE_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete usage log
     *
     * @param coreUsagelog
     * @return
     */
    @Override
    public ApiResponse deleteUsagelog(CoreUsagelog coreUsagelog) {
        LOGGER.info("Delete Usagelog data to database. RequestId - " + coreUsagelog.getRequestId());
        ApiResponse response = new ApiResponse();
        try {
            coreUsagelogDao.delete(coreUsagelog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Delete Usagelog. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DELETE_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get usage log by Id
     *
     * @param usageLogId
     * @return
     * @throws Exception
     */
    @Override
    public CoreUsagelog getUsagelogById(String usageLogId) throws Exception {
        try {
            return coreUsagelogDao.getUsageLogById(usageLogId);
        } catch (Exception ex) {
            LOGGER.error("Get Usagelog by id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get usage log by Id and client
     *
     * @param usageLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public CoreUsagelog getUsagelogById(String usageLogId, int clientId) throws Exception {
        try {
            return coreUsagelogDao.getUsageLogById(usageLogId, clientId);
        } catch (Exception ex) {
            LOGGER.error("Get Usagelog by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get usage log by criteria
     *
     * @param usageLogCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<CoreUsagelog> getUsagelogByCriteria(UsageLogCriteria usageLogCriteria) throws Exception {
        try {
            usageLogCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return coreUsagelogDao.getUsageLogByCriteria(usageLogCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get Usagelog by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_USAGELOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CoreLensdocument
    /**
     * Create Lens Document
     *
     * @param coreLensdocument
     * @return
     */
    @Override
    public ApiResponse createLensdocument(CoreLensdocument coreLensdocument) {
        LOGGER.info("Add Lensdocument data to database. Filename - " + coreLensdocument.getOriginalFilename());
        ApiResponse response = new ApiResponse();
        try {
            coreLensdocumentDao.save(coreLensdocument);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add Lensdocument. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.ADD_LENSDOCUMENTDUMP_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update lens Document
     *
     * @param coreLensdocument
     * @return
     */
    @Override
    public ApiResponse updateLensdocument(CoreLensdocument coreLensdocument) {
        LOGGER.info("Update Lensdocument data to database. DocumentId - " + coreLensdocument.getId());
        ApiResponse response = new ApiResponse();
        try {
            coreLensdocumentDao.update(coreLensdocument);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update Lensdocument. Exception " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UPDATE_LENSDOCUMENTDUMP_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }
    
    /**
     * Update lens Document dump count
     *
     * @param aggDocumentDumpCriteria
     * @return
     */
    @Override
    public ApiResponse updateDocumentDumpCount(AggregatedCoreLensDocument aggLensDocument){
        LOGGER.info("Update Lensdocument data dump count to database.");
        ApiResponse response = new ApiResponse();
        try {
            coreLensdocumentDao.updateDocumentDumpCount(aggLensDocument);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update Lensdocument dump count to database. Exception " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UPDATE_LENSDOCUMENTDUMP_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete Lens Document
     *
     * @param coreLensdocument
     * @return
     */
    @Override
    public ApiResponse deleteLensdocument(CoreLensdocument coreLensdocument) {
        LOGGER.info("Delete Lensdocument data from database. DocumentId - " + coreLensdocument.getId());
        ApiResponse response = new ApiResponse();
        try {
            coreLensdocumentDao.delete(coreLensdocument);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Delete Lensdocument. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DELETE_LENSDOCUMENTDUMP_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get Lens Document by Id
     *
     * @param lensDocumentId
     * @return
     */
    @Override
    public CoreLensdocument getLensdocumentById(int lensDocumentId) {
        return coreLensdocumentDao.getLensDocumentById(lensDocumentId);
    }

    // CoreErrorlog
    /**
     * Create Error Log
     *
     * @param coreErrorlog
     * @return
     */
    @Override
    public ApiResponse createErrorlog(CoreErrorlog coreErrorlog) {
        LOGGER.info("Add CoreErrorlog data to database. RequestId -  " + coreErrorlog.getRequestId());
        ApiResponse response = new ApiResponse();
        try {
            coreErrorlogDao.save(coreErrorlog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update CoreErrorlog. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.ADD_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update Error Log
     *
     * @param coreErrorlog
     * @return
     */
    @Override
    public ApiResponse updateErrorlog(CoreErrorlog coreErrorlog) {
        LOGGER.info("Update CoreErrorlog data to database. RequestId - " + coreErrorlog.getRequestId());
        ApiResponse response = new ApiResponse();
        try {
            coreErrorlogDao.update(coreErrorlog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update CoreErrorlog. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.UPDATE_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete Error Log
     *
     * @param coreErrorlog
     * @return
     */
    @Override
    public ApiResponse deleteErrorlog(CoreErrorlog coreErrorlog) {
        LOGGER.info("Delete CoreErrorlog data from database. RequestId - " + coreErrorlog.getRequestId());
        ApiResponse response = new ApiResponse();
        try {
            coreErrorlogDao.delete(coreErrorlog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("delete CoreErrorlog. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DELETE_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get Error log by Id
     *
     * @param errorlogId
     * @return
     * @throws Exception
     */
    @Override
    public CoreErrorlog getErrorlogById(String errorlogId) throws Exception {
        try {
            return coreErrorlogDao.getErrorLogById(errorlogId);
        } catch (Exception ex) {
            LOGGER.error("Get Error log by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get error log by Id and client
     *
     * @param errorlogId
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public CoreErrorlog getErrorlogById(String errorlogId, int clientId) throws Exception {
        try {
            return coreErrorlogDao.getErrorLogById(errorlogId, clientId);
        } catch (Exception ex) {
            LOGGER.error("Get Errorlog by id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get error log by criteria
     *
     * @param errorLogCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<CoreErrorlog> getErrorlogByCriteria(ErrorLogCriteria errorLogCriteria) throws Exception {
        try {
            errorLogCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return coreErrorlogDao.getErrorLogByCriteria(errorLogCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get Errorlog by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_ERRORLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create resource
     *
     * @param coreResources
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse createResource(CoreResources coreResources) throws Exception {
        LOGGER.info("Add Resource data to database. ResourceName - " + coreResources.getResource());
        ApiResponse response = new ApiResponse();
        try {
            coreResourceDao.save(coreResources);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add resource. Exception - " + ex.getMessage());
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_RESOURCE + " Or " + ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
            }
            if (ex.getClass().equals(DataIntegrityViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_NAME + " Or " + ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
            }

            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update resource
     *
     * @param coreResources
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateResource(CoreResources coreResources) throws Exception {
        LOGGER.info("Update Resource data to database. Resourcename -  " + coreResources.getResource());
        ApiResponse response = new ApiResponse();
        try {
            coreResourceDao.update(coreResources);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Update resource. Exception - " + ex.getMessage());
            if (ex.getClass().equals(ConstraintViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_NAME + " Or " + ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
            }
            if (ex.getClass().equals(DataIntegrityViolationException.class)) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_NAME + " Or " + ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
            }

            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Delete resource
     *
     * @param coreResources
     * @return
     */
    @Override
    public ApiResponse deleteResource(CoreResources coreResources) {
        LOGGER.info("Delete Resource data from database. ResourceName - " + coreResources.getResource());
        ApiResponse response = new ApiResponse();
        try {

            coreResourceDao.delete(coreResources);
            response.status = true;

        } catch (Exception ex) {
            LOGGER.error("Delete Resource. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_DELETE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Get resource by Id
     *
     * @param resourceid
     * @return
     * @throws Exception
     */
    @Override
    public CoreResources getResourceById(int resourceid) throws Exception {
        try {
            return coreResourceDao.getResourcesById(resourceid);
        } catch (Exception ex) {
            LOGGER.error("Get resource by id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_RESOURCE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get resource by criteria
     *
     * @param resourceCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<CoreResources> getResourceByCriteria(ResourceCriteria resourceCriteria) throws Exception {
        try {
//            resourceCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return coreResourceDao.getResourcesByCriteria(resourceCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get resource by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_RESOURCE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get resource by criteria
     *
     * @param resourceCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<CoreResources> getResourceWithoutApiByCriteria(ResourceCriteria resourceCriteria) throws Exception {
        try {
//            resourceCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return coreResourceDao.getResourcesWithoutApiByCriteria(resourceCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get resource by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_RESOURCE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create vendor
     *
     * @param searchVendorSettings
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse createVendor(SearchVendorsettings searchVendorSettings) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            searchVendorSettingsDao.save(searchVendorSettings);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add Lens Settings to database. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.VENDOR_CREATION_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Update vendor
     *
     * @param searchVendorSettings
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateVendor(SearchVendorsettings searchVendorSettings) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            searchVendorSettingsDao.update(searchVendorSettings);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add Lens Settings to database. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.VENDOR_UPDATE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * Close vendor
     *
     * @param searchVendorSettings
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse deleteVendor(SearchVendorsettings searchVendorSettings) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            searchVendorSettingsDao.delete(searchVendorSettings);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add Lens Settings to database. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.VENDOR_DELETE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @Override
    public List<SearchVendorsettings> getVendorsByCriteria(SearchVendorSettingsCriteria searchVendorSettingsCriteria) throws Exception {
        try {
            return searchVendorSettingsDao.getVendorSettingsByCriteria(searchVendorSettingsCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get vendor by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_VENDOR_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public SearchVendorsettings getVendorById(int vendorId) throws Exception {
        try {
            return searchVendorSettingsDao.getVendorById(vendorId);
        } catch (Exception ex) {
            LOGGER.error("Get vendor by id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_VENDOR_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<SearchLookup> getSearchLookupsByCriteria(SearchLookupCriteria searchLookupCriteria) throws Exception {
        try {
            return searchLookupDao.getLookupsByCriteria(searchLookupCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get lookup by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LOOKUP_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ApiResponse addSearchCommandDump(SearchCommanddump searchCommanddump) throws Exception {
        LOGGER.info("Add search command dump to database.");
        ApiResponse response = new ApiResponse();
        try {
            searchCommandDumpDao.save(searchCommanddump);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add search command dump. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.ADD_SEARCHCOMMANDDUMP_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    /**
     * add search register log to database
     *
     * @param searchRegisterLog
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse addRegisterLog(SearchRegisterlog searchRegisterLog) throws Exception {
        LOGGER.info("Add register log to database.");
        ApiResponse response = new ApiResponse();
        try {
            searchRegisterLogDao.save(searchRegisterLog);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add search command dump. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.ADD_REGISTERLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @Override
    public SearchRegisterlog getRegisterLogById(int registerId, int clientId) throws Exception {
        try {
            return searchRegisterLogDao.getRegisterLogById(registerId, clientId);
        } catch (Exception ex) {
            LOGGER.error("Get Register Log by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_REGISTERLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get register log by criteria
     *
     * @param searchRegisterLogCriteria
     * @return
     * @throws Exception
     */
    @Override
    public List<SearchRegisterlog> getRegisterLogByCriteria(SearchRegisterLogCriteria searchRegisterLogCriteria) throws Exception {
        try {
            //usagecounterCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return searchRegisterLogDao.getRegisterLogByCriteria(searchRegisterLogCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get Registerlog by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_REGISTERLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * Get register log by criteria
     *
     * @param searchRegisterLogCriteria
     * @return
     * @throws Exception
     */
    @Override
    public int getRegisterLogCount() {
        try {
            return searchRegisterLogDao.getRegisterLogCount();
        } catch (Exception ex) {
            LOGGER.error("Get Registerlog count. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_REGISTERLOG_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }    
    }
    
    @Override
    public SearchCommanddump getSearchCommandDumpById(int dumpId, int clientId) throws Exception {
        try {
            return searchCommandDumpDao.getDumpById(dumpId, clientId);
        } catch (Exception ex) {
            LOGGER.error("Get search command dump by Id. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_SEARCHCOMMANDDUMP_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<SearchCommanddump> getSearchCommandDumpByCriteria(SearchCommandDumpCriteria searchCommandDumpCriteria) throws Exception {
        try {
            //usagecounterCriteria.setDefaultResultLimit(apiConfig.getDefaultResultLimit());
            return searchCommandDumpDao.getDumpByCriteria(searchCommandDumpCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get search command dump by criteria. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_SEARCHCOMMANDDUMP_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ApiResponse addLookup(SearchLookup lookup) {
        LOGGER.info("Add search lookup to database.");
        ApiResponse response = new ApiResponse();
        try {
            searchLookupDao.save(lookup);
            response.status = true;
        } catch (Exception ex) {
            LOGGER.error("Add search lookup. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.ADD_SEARCHLOOKUP_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @Override
    public List<SearchFilterType> getFilterIDByName(SearchFilterTypeCriteria searchFilterTypeCriteria) {
        try {
            return searchFilterTypeDao.getFilterIDByName(searchFilterTypeCriteria);
        } catch (Exception ex) {
            LOGGER.error("Get Filter Id by Name. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_SEARCHFILTERTYPE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
}
