// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.logger.ILoggerService;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * *
 * Authentication log aspect
 */
@Aspect
public class AuthenticationLogAspect {

    private static final Logger LOGGER = LogManager.getLogger(AuthenticationLogAspect.class);

    private ILoggerService loggerService = null;

    /**
     * *
     * Set logger service
     *
     * @param loggerService
     */
    public void setLoggerService(ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    /**
     * *
     * Add authentication log
     *
     * @param joinPoint
     * @param result
     */
    @AfterReturning(
            pointcut = "execution(public * com.bgt.lens.services.authentication.impl.AuthenticationServiceImpl.validateApiContext(..))",
            returning = "result")
    public void addAuthenticationLog(JoinPoint joinPoint, Object result) {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");
            ApiResponse apiResponse = (ApiResponse) result;
            if (apiResponse.status) {
                loggerService.addAuthenticationLog(apiContext);
//                if (!apiResponse.status) {
//                    LOGGER.error("Unable to add authentication log");
//                }
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add authentication log. Exception -  " + ex.getMessage());
        }
    }
}
