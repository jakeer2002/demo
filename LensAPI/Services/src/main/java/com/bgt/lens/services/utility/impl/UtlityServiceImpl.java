// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.utility.impl;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.grpcController.GrpcClient;
import com.bgt.lens.helpers.CoreMappers;
import com.bgt.lens.helpers.Enum.InstanceType;
import com.bgt.lens.helpers.Enum.VariantType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.Validator;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.rest.response.DefaultLocaleList;
import com.bgt.lens.model.rest.response.Locale;
import com.bgt.lens.model.criteria.LensSettingsCriteria;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.parserservice.request.CanonRequest;
import com.bgt.lens.model.parserservice.request.ConvertRequest;
import com.bgt.lens.model.parserservice.request.InfoRequest;
import com.bgt.lens.model.parserservice.request.ParseJobRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.parserservice.request.PingRequest;
import com.bgt.lens.model.parserservice.request.ProcessLocaleRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Instance;
import com.bgt.lens.model.rest.response.InstanceList;
import com.bgt.lens.repositories.ILensRepository;
import com.bgt.lens.repositories.impl.LensCommandBuilder;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.license.ILicenseManagementService;
import com.bgt.lens.services.utility.IUtilityService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicInteger;
import org.springframework.http.HttpStatus;

/**
 *
 * Utility service Implementation
 */
public class UtlityServiceImpl implements IUtilityService {

    private static final Logger LOGGER = LogManager.getLogger(UtlityServiceImpl.class);

    private IDataRepositoryService dbService;
    private ILensRepository lensRespository;
    private ApiConfiguration apiConfig;
    private ILicenseManagementService licenseManagementService;
    private LensCommandBuilder lensCommandBuilder;
    private GrpcClient grpcClient;

    private final Helper _helper = new Helper();
    private final Validator _validator = new Validator();
    private final char resumeType = 'R';
    private final char postingType = 'P';
    private final CoreMappers _coreMappers = new CoreMappers();
    //private final GrpcClient grpcClient=new GrpcClient();
      /**
     * Lens Empty error message
     */
    public final String lensEmptyMessage = "error converting: text not generated";
    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate template;

    private static final String hourlyRateLimit = "hourly";
    private static final String dailyRateLimit = "daily";
    private static final String monthlyRateLimit = "monthly";

    /**
     * Set data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dbService = dataRepositoryService;
    }

    public void setGrpcClient(GrpcClient grpcClient) {
        this.grpcClient = grpcClient;
    }
    /**
     * Set ApiConfig
     *
     * @param apiConfig
     */
    public void setapiConfig(ApiConfiguration apiConfig) {
        this.apiConfig = apiConfig;
    }

    /**
     * Set Lens repository
     *
     * @param lensRespository
     */
    public void setLensRepository(ILensRepository lensRespository) {
        this.lensRespository = lensRespository;
    }

    /**
     * Set License management service
     *
     * @param licenseManagementService
     */
    public void setLicenseManagementService(ILicenseManagementService licenseManagementService) {
        this.licenseManagementService = licenseManagementService;
    }

    /**
     * Set lens command builder
     *
     * @param lensCommandBuilder
     */
    public void setLensCommandBuilder(LensCommandBuilder lensCommandBuilder) {
        this.lensCommandBuilder = lensCommandBuilder;
    }
    


    /**
     * Get info
     *
     * @param infoRequest
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse info(InfoRequest infoRequest) throws Exception {
        ApiResponse response = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        try {
            if (_helper.isNull(apiContext.getClient())) {
                // When Authentication part unavailable
                // LensSettings Criteria 
                LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                lensSettingsCriteria.setInstanceType(infoRequest.getInstanceType());
                lensSettingsCriteria.setLocale(_helper.isNotNullOrEmpty(infoRequest.getLocale()) ? infoRequest.getLocale() : apiConfig.getDefaultLocale());
                lensSettingsCriteria.setIsHRXML(false);
                // Fetch LensSettings
                List<CoreLenssettings> coreLenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                // Process LENS Command
                if (coreLenssettingses != null && coreLenssettingses.size() > 0) {
                    if (!coreLenssettingses.get(0).isStatus()) {
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                        return response;
                    }
                    Instance instance = new Instance();
                    instance.status = (String) lensRespository.info(coreLenssettingses.get(0)).responseData;
                    instance.locale = coreLenssettingses.get(0).getLocale();
                    response.status = true;
                    response.responseData = instance;
                    return response;
                } else {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                    // response.responseData = "Invalid Instance Type '" + infoRequest.getInstanceType() + "' or Locale '" + infoRequest.getLocale() + "'";
                    LOGGER.error("Invalid Instance Type '" + infoRequest.getInstanceType() + "' or Locale '" + infoRequest.getLocale() + "'");
                }
            } else {
                CoreClient client = apiContext.getClient();
                InstanceList instanceList = new InstanceList();
                List<Instance> instances = new ArrayList<>();

                if (_helper.isNullOrEmpty(infoRequest.getLocale())) {
                    // Get all lens settings of specific client
                    // Get all lens settings and ignore hrxml content
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> coreLenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (coreLenssettingses != null && coreLenssettingses.size() > 0) {
                        for (CoreLenssettings coreLenssettings : coreLenssettingses) {
                            if (coreLenssettings.getInstanceType().equalsIgnoreCase(infoRequest.getInstanceType())) {
                                if (!coreLenssettings.isStatus()) {
                                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                                    return response;
                                }
                                Instance instance = new Instance();
                                instance.status = (String) lensRespository.info(coreLenssettings).responseData;
                                instance.locale = coreLenssettings.getLocale();
                                instances.add(instance);
                            }
                        }
                        if (instances.size() > 0) {
                            instanceList.instances = instances;
                            response.status = true;
                            response.responseData = instanceList;
                            return response;
                        } else {
                            response.status = false;
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                            // response.responseData = "Invalid Instance Type '" + infoRequest.getInstanceType() + "'";
                            LOGGER.error("Invalid Instance Type: " + infoRequest.getInstanceType());
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + infoRequest.getInstanceType() + "'";
                        LOGGER.error("Invalid Instance Type: " + infoRequest.getInstanceType());
                    }
                } else {
                    // Get only specific client request lens settings and ignore other lens settings details
                    // Get only lens setting and ignore hrxml content
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(infoRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(infoRequest.getLocale());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> coreLenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (coreLenssettingses != null && coreLenssettingses.size() > 0) {
                        if (!coreLenssettingses.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }
                        Instance instance = new Instance();
                        instance.status = (String) lensRespository.info(coreLenssettingses.get(0)).responseData;
                        instance.locale = coreLenssettingses.get(0).getLocale();
                        response.status = true;
                        response.responseData = instance;
                        return response;
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + infoRequest.getInstanceType() + "' or Locale '" + infoRequest.getLocale() + "'";
                        LOGGER.error("Invalid Instance Type '" + infoRequest.getInstanceType() + "' or Locale '" + infoRequest.getLocale() + "'");
                    }
                }
            }
        } catch (Exception ex) {
            response.status = false;
            apiContext.setApiResponse(response);
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Info response : Instance Type '" + infoRequest.getInstanceType() + "' or Locale '" + infoRequest.getLocale() + "', Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Ping Lens
     *
     * @param pingRequest
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse ping(PingRequest pingRequest) throws Exception {
        ApiResponse response = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        try {
            if (!_helper.isNotNull(apiContext.getClient())) {
                // When Authentication part unavailable
                // LensSettings Criteria
                LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                lensSettingsCriteria.setInstanceType(pingRequest.getInstanceType());
                lensSettingsCriteria.setLocale(_helper.isNotNullOrEmpty(pingRequest.getLocale()) ? pingRequest.getLocale() : apiConfig.getDefaultLocale());
                lensSettingsCriteria.setIsHRXML(false);
                // Fetch LensSettings
                List<CoreLenssettings> coreLenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                // Process LENS Command
                if (coreLenssettingses != null && coreLenssettingses.size() > 0) {
                    if (!coreLenssettingses.get(0).isStatus()) {
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                        return response;
                    }
                    Instance instance = new Instance();
                    instance.status = Boolean.toString(lensRespository.ping(coreLenssettingses.get(0)).status);
                    instance.locale = coreLenssettingses.get(0).getLocale();
                    response.status = true;
                    response.responseData = instance;
                    return response;
                } else {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                    // response.responseData = "Invalid Instance Type '" + pingRequest.getInstanceType() + "' or Locale '" + pingRequest.getLocale() + "'";
                    LOGGER.error("Invalid Instance Type '" + pingRequest.getInstanceType() + "' or Locale '" + pingRequest.getLocale() + "'");
                }
            } else {
                CoreClient client = apiContext.getClient();
                InstanceList instanceList = new InstanceList();
                List<Instance> instances = new ArrayList<>();

                if (_helper.isNullOrEmpty(pingRequest.getLocale())) {
                    // Get all lens settings of specific client
                    // Get all lens settings and ignore hrxml content
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> coreLenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (coreLenssettingses != null && coreLenssettingses.size() > 0) {
                        for (CoreLenssettings coreLenssettings : coreLenssettingses) {
                            if (coreLenssettings.getInstanceType().equalsIgnoreCase(pingRequest.getInstanceType())) {
                                if (!coreLenssettings.isStatus()) {
                                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                                    return response;
                                }
                                Instance instance = new Instance();
                                instance.status = Boolean.toString(lensRespository.ping(coreLenssettings).status);
                                instance.locale = coreLenssettings.getLocale();
                                instances.add(instance);
                            }
                        }

                        if (instances.size() > 0) {
                            instanceList.instances = instances;
                            response.status = true;
                            response.responseData = instanceList;
                            return response;
                        } else {
                            response.status = false;
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                            // response.responseData = "Invalid Instance Type '" + pingRequest.getInstanceType() + "'";
                            LOGGER.error("Invalid Instance Type: " + pingRequest.getInstanceType());
                        }

                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + pingRequest.getInstanceType() + "'";
                        LOGGER.error("Invalid Instance Type: " + pingRequest.getInstanceType());
                    }
                } else {
                    // Get only specific client request lens settings and ignore other lens settings details
                    // Get only lens setting and ignore hrxml content
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(pingRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(pingRequest.getLocale());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> coreLenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (coreLenssettingses != null && coreLenssettingses.size() > 0) {
                        if (!coreLenssettingses.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }
                        Instance instance = new Instance();
                        instance.status = Boolean.toString(lensRespository.ping(coreLenssettingses.get(0)).status);
                        instance.locale = coreLenssettingses.get(0).getLocale();
                        response.status = true;
                        response.responseData = instance;
                        return response;
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + pingRequest.getInstanceType() + "' or Locale '" + pingRequest.getLocale() + "'";
                        LOGGER.error("Invalid Instance Type '" + pingRequest.getInstanceType() + "' or Locale '" + pingRequest.getLocale() + "'");
                    }
                }
            }
        } catch (Exception ex) {
            response.status = false;
            apiContext.setApiResponse(response);
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Ping response : Instance Type '" + pingRequest.getInstanceType() + "', Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Convert binary data Aspect to dump the incoming binaryData
     *
     * @param convertRequest
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse convertBinaryData(ConvertRequest convertRequest) throws Exception {
        ApiResponse response = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        try {
            _validator.validateConvertRequest(convertRequest);
            boolean emptyLocale = false;

            convertRequest.setExtension(_helper.isNotNullOrEmpty(convertRequest.getExtension()) ? convertRequest.getExtension() : apiConfig.getDefaultExtension());

            if (_helper.isNullOrEmpty(convertRequest.getLocale())) {
                emptyLocale = true;
            }

            if (!_helper.isNotNull(apiContext.getClient())) {
                // When Authentication part is unavailable
                // LensSettings Criteria
                LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                lensSettingsCriteria.setInstanceType(convertRequest.getInstanceType());
                lensSettingsCriteria.setLocale(convertRequest.getLocale());
                lensSettingsCriteria.setIsHRXML(false);
                // Fetch LensSettings
                List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                // Process LENS Command
                if (lensSettingsList != null && lensSettingsList.size() > 0) {
                    if (!lensSettingsList.get(0).isStatus()) {
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                        return response;
                    }
                    response = lensRespository.convertBinaryData(lensSettingsList.get(0), _helper.decodeBase64String(convertRequest.getBinaryData()), convertRequest.getExtension());
                    response.processedWithLocale = lensSettingsCriteria.getLocale();
                    return response;
                } else {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                    // response.responseData = "Invalid Instance Type '" + convertRequest.getInstanceType() + "' or Locale '" + convertRequest.getLocale() + "'";
                    LOGGER.error("Invalid Instance Type '" + convertRequest.getInstanceType() + "' or Locale '" + convertRequest.getLocale() + "'");
                }

            } else {
                CoreClient client = apiContext.getClient();
                if (emptyLocale) {
                    DefaultLocaleList defaultLocaleList = (DefaultLocaleList) _helper.getObjectFromJson(client.getDefaultLocale(), new DefaultLocaleList());

                    if (defaultLocaleList != null) {
                        List<Locale> localeList = defaultLocaleList.getLocale();

                        if (localeList != null && localeList.size() > 0) {
                            for (Locale locale : localeList) {
                                if (locale.getLanguage().equals("*")) {
                                    convertRequest.setLocale(locale.getCountry());
                                    emptyLocale = false;
                                }
                            }
                            if (emptyLocale) {
                                response.status = false;
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                                return response;
                            }
                        } else {
                            response.status = false;
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                            return response;
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                        return response;
                    }
                }

                // Get only specific client request lens settings and ignore other lens settings details
                // Get only lens setting and ignore hrxml content
                // LensSettings Criteria
                LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                lensSettingsCriteria.setInstanceType(convertRequest.getInstanceType());
                lensSettingsCriteria.setLocale(convertRequest.getLocale());
                lensSettingsCriteria.setCoreClient(client);
                lensSettingsCriteria.setIsHRXML(false);
                // Fetch LensSettings
                List<CoreLenssettings> lenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                // Process LENS Command
                if (lenssettingses != null && lenssettingses.size() > 0) {
                    if (!lenssettingses.get(0).isStatus()) {
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                        return response;
                    }
                    response = lensRespository.convertBinaryData(lenssettingses.get(0), _helper.decodeBase64String(convertRequest.getBinaryData()), convertRequest.getExtension());
                    if (response.status) {
                        if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                            return response;
                        }
                        response.processedWithLocale = lenssettingses.get(0).getLocale();
                        return response;
                    } else {
                        return response;
                    }
                } else {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                    // response.responseData = "Invalid Instance Type '" + convertRequest.getInstanceType() + "' or Locale '" + convertRequest.getLocale() + "'";
                    LOGGER.error("Invalid Instance Type '" + convertRequest.getInstanceType() + "' or Locale '" + convertRequest.getLocale() + "'");
                }
            }
        } catch (Exception ex) {
            response.status = false;
            apiContext.setApiResponse(response);
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Convert response : Instance Type '" + convertRequest.getInstanceType() + "' or Locale '" + convertRequest.getLocale() + "', Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Canon binary data
     *
     * @param canonRequest
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse canonBinaryData(CanonRequest canonRequest) throws Exception {
        ApiResponse response = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        try {
            _validator.validateCanonRequest(canonRequest);
            boolean emptyLocale = false;

            if (_helper.isNullOrEmpty(canonRequest.getLocale())) {
                emptyLocale = true;
            }

            if (_helper.isNull(apiContext.getClient())) {
                // When Authentication part unavailable
                // LensSettings Criteria
                LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                lensSettingsCriteria.setInstanceType(canonRequest.getInstanceType());
                lensSettingsCriteria.setLocale(canonRequest.getLocale());
                lensSettingsCriteria.setIsHRXML(false);
                // Fetch LensSettings
                List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                // Process LENS Command
                if (lensSettingsList != null && lensSettingsList.size() > 0) {
                    if (!lensSettingsList.get(0).isStatus()) {
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                        return response;
                    }
                    String canonCommand = lensCommandBuilder.buildCanonCommand(new String(_helper.decodeBase64String(canonRequest.getBinaryData()), "UTF-8"));
                    canonCommand = "<?xml version='1.0' encoding='" + lensSettingsList.get(0).getCharacterSet() + "'?>" + canonCommand;
                    response = lensRespository.sendXMLCommand(lensSettingsList.get(0), canonCommand);
                    if (response.status) {
                        response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                        response.processedWithLocale = lensSettingsList.get(0).getLocale();
                    }
                } else {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                    // response.responseData = "Invalid Instance Type '" + canonRequest.getInstanceType() + "' or Locale '" + canonRequest.getLocale() + "'";
                    LOGGER.error("Invalid Instance Type '" + canonRequest.getInstanceType() + "' or Locale '" + canonRequest.getLocale() + "'");
                }
            } else {
                CoreClient client = apiContext.getClient();
                if (emptyLocale) {
                    DefaultLocaleList defaultLocaleList = (DefaultLocaleList) _helper.getObjectFromJson(client.getDefaultLocale(), new DefaultLocaleList());

                    if (defaultLocaleList != null) {
                        List<Locale> localeList = defaultLocaleList.getLocale();

                        if (localeList != null && localeList.size() > 0) {
                            for (Locale locale : localeList) {
                                if (locale.getLanguage().equals("*")) {
                                    canonRequest.setLocale(locale.getCountry());
                                    emptyLocale = false;
                                }
                            }
                            if (emptyLocale) {
                                response.status = false;
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                                return response;
                            }
                        } else {
                            response.status = false;
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                            return response;
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                        return response;
                    }
                }
                // Get only specific client request lens settings and ignore other lens settings details
                // Get only lens setting and ignore hrxml content
                // LensSettings Criteria
                LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                lensSettingsCriteria.setInstanceType(canonRequest.getInstanceType());
                lensSettingsCriteria.setLocale(canonRequest.getLocale());
                lensSettingsCriteria.setCoreClient(client);
                lensSettingsCriteria.setIsHRXML(false);
                // Fetch LensSettings
                List<CoreLenssettings> lenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                // Process LENS Command
                if (lenssettingses != null && lenssettingses.size() > 0) {
                    if (!lenssettingses.get(0).isStatus()) {
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                        return response;
                    }
//                    byte[] s = _helper.decodeBase64String(canonRequest.getBinaryData());
//                    String d = new String(s);
//                    String b = _lensCommandBuilder.buildCanonCommand(d);
                    //String f = Arrays.toString(s);
                    // String a =  _lensCommandBuilder.buildCanonCommand(Arrays.toString(s));
                    String canonCommand = lensCommandBuilder.buildCanonCommand(new String(_helper.decodeBase64String(canonRequest.getBinaryData()), "UTF-8"));
                    canonCommand = "<?xml version='1.0' encoding='" + lenssettingses.get(0).getCharacterSet() + "'?>" + canonCommand;
                    response = lensRespository.sendXMLCommand(lenssettingses.get(0), canonCommand);
                    if (response.status) {
                        response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                        if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                            return response;
                        }
                        response.processedWithLocale = lenssettingses.get(0).getLocale();
                        return response;
                    } else {
                        return response;
                    }
                } else {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                    // response.responseData = "Invalid Instance Type '" + canonRequest.getInstanceType() + "' or Locale '" + canonRequest.getLocale() + "'";
                    LOGGER.error("Invalid Instance Type '" + canonRequest.getInstanceType() + "' or Locale '" + canonRequest.getLocale() + "'");
                }
            }
        } catch (Exception ex) {
            response.status = false;
            apiContext.setApiResponse(response);
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Canon response. Instance Type '" + canonRequest.getInstanceType() + "' or Locale '" + canonRequest.getLocale() + "'. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Parse resume and retrieve specific fields
     *
     * @param parseResumeRequestData
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse parseResume(ParseResumeRequest parseResumeRequestData) throws Exception {
        ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
        parseResumeRequest.setInstanceType(parseResumeRequestData.getInstanceType());
        parseResumeRequest.setBinaryData(parseResumeRequestData.getBinaryData());
        parseResumeRequest.setExtension(parseResumeRequestData.getExtension());
        parseResumeRequest.setLocale(parseResumeRequestData.getLocale());
        parseResumeRequest.setFieldList(parseResumeRequestData.getFieldList());
        ApiResponse response = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        try {
            _validator.validateParseResumeRequest(parseResumeRequest);
            boolean emptyLocale = false;

            parseResumeRequest.setExtension(_helper.isNotNullOrEmpty(parseResumeRequest.getExtension()) ? parseResumeRequest.getExtension() : apiConfig.getDefaultExtension());

            if (_helper.isNullOrEmpty(parseResumeRequest.getLocale())) {
                emptyLocale = true;
            }

            if (_helper.isNull(apiContext.getClient())) {
                // When Authentication part unavailable
                // Locale detection based parsing
                if (emptyLocale) {
                    // Get all lens settings of specific instance type
                    // Get all lens settings and ignore hrxml content
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseResumeRequest.getInstanceType());
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Locale LensSettings Criteria
                    lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(InstanceType.LD.name());
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Locale Fetch LensSettings
                    List<CoreLenssettings> localeLensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && localeLensSettingsList != null && localeLensSettingsList.size() > 0) {
                        lensSettingsList.add(localeLensSettingsList.get(0));
                    }

                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        response = lensRespository.ldTagBinaryData(lensSettingsList, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, null);
                        if (response.status) {
                            // Extract resume fields from tag resposnse
                            List<String> fields = _helper.isNull(parseResumeRequest.getFieldList()) ? new ArrayList() : parseResumeRequest.getFieldList().getField();
                            List<String> modifiedFieldList = new ArrayList<>();
                            for (String field : fields) {
                                modifiedFieldList.add(field.toLowerCase());
                            }
                            response = _coreMappers.parseResumeFields(response, modifiedFieldList);
                            response.identifiedLocale = lensSettingsList.get(0).getLocale();

                            // Set identified locale for dump document
                            parseResumeRequest.setLocale(response.identifiedLocale);
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "'");
                    }
                } else {
                    // Specified Locale based parsing
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseResumeRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(parseResumeRequest.getLocale());
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (!lensSettingsList.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }

                        String customSkillCode = null;
                        List<CoreCustomskillsettings> coreCustomskillsettings = new ArrayList<>(lensSettingsList.get(0).getCoreCustomskillsettings());
                            if (_helper.isNotNull(coreCustomskillsettings) && !coreCustomskillsettings.isEmpty()) {
                                customSkillCode = coreCustomskillsettings.get(0).getCustomSkillCode();
                            }

                        response = lensRespository.tagBinaryData(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);

                        if (response.status) {
                            // Extract resume fields from tag resposnse
                            List<String> fields = _helper.isNull(parseResumeRequest.getFieldList()) ? new ArrayList() : parseResumeRequest.getFieldList().getField();
                            List<String> modifiedFieldList = new ArrayList<>();
                            for (String field : fields) {
                                modifiedFieldList.add(field.toLowerCase());
                            }
                            response = _coreMappers.parseResumeFields(response, modifiedFieldList);
                            response.processedWithLocale = lensSettingsList.get(0).getLocale();

                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "'");
                    }
                }
            } else {
                CoreClient client = apiContext.getClient();
                // Locale detection based parsing
                if (emptyLocale && client.isEnableLocaleDetection()) {

                    // Get all lens settings of specific client & instance type
                    // Get all lens settings and ignore hrxml content
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseResumeRequest.getInstanceType());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Locale LensSettings Criteria
                    lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(InstanceType.LD.name());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Locale Fetch LensSettings
                    List<CoreLenssettings> localeLensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && localeLensSettingsList != null && localeLensSettingsList.size() > 0) {
                        lensSettingsList.add(localeLensSettingsList.get(0));
                    }

                    List<CoreLenssettings> updatedLenssettingses = _helper.updateDefaultLocaleList(client.getDefaultLocale(), lensSettingsList, parseResumeRequest.getInstanceType());
                    // Process LocaleDetector Command
                    if (updatedLenssettingses != null && updatedLenssettingses.size() > 0) {
                        response = lensRespository.ldTagBinaryData(updatedLenssettingses, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, client.getDefaultLocale());
                        if (response.status) {
                            // Extract resume fields from tag resposnse
                            List<String> fields = _helper.isNull(parseResumeRequest.getFieldList()) ? new ArrayList() : parseResumeRequest.getFieldList().getField();
                            List<String> modifiedFieldList = new ArrayList<>();
                            for (String field : fields) {
                                modifiedFieldList.add(field.toLowerCase());
                            }
                            // Set identified locale for dump document
                            parseResumeRequest.setLocale(response.identifiedLocale);
                            response = _coreMappers.parseResumeFields(response, modifiedFieldList);
                            // Update Transaction count
                            if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                                return response;
                            }
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                        // response.responseData = "Instance not found";
                    }
                } else if (emptyLocale && _helper.isNotNullOrEmpty(client.getDefaultLocale())) {
                    // Default locale based parsing
                    DefaultLocaleList defaultLocaleList = (DefaultLocaleList) _helper.getObjectFromJson(client.getDefaultLocale(), new DefaultLocaleList());

                    if (defaultLocaleList != null) {
                        List<Locale> localeList = defaultLocaleList.getLocale();

                        if (localeList != null && localeList.size() > 0) {
                            for (Locale locale : localeList) {
                                if (locale.getLanguage().equals("*")) {
                                    parseResumeRequest.setLocale(locale.getCountry());
                                    emptyLocale = false;
                                }
                            }
                            if (emptyLocale) {
                                response.status = false;
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                            }
                        } else {
                            response.status = false;
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                    }

                } else if (emptyLocale) {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);

                }

                if (!emptyLocale) {

                    // Specified Locale based parsing
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseResumeRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(parseResumeRequest.getLocale());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (!lensSettingsList.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }

                        String customSkillCode = null;
                        List<CoreClientlenssettings> coreClientlenssettingses = new ArrayList<>(lensSettingsList.get(0).getCoreClientlenssettingses());
                        CoreCustomskillsettings coreCustomskillsettings = coreClientlenssettingses.get(0).getCoreCustomskillsettings();
                        if (_helper.isNotNull(coreCustomskillsettings)) {
                            customSkillCode = coreCustomskillsettings.getCustomSkillCode();
                        }

                        response = lensRespository.tagBinaryData(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);

                        if (response.status) {
                            // Extract resume fields from tag resposnse
                            List<String> fields = _helper.isNull(parseResumeRequest.getFieldList()) ? new ArrayList() : parseResumeRequest.getFieldList().getField();
                            List<String> modifiedFieldList = new ArrayList<>();
                            for (String field : fields) {
                                modifiedFieldList.add(field.toLowerCase());
                            }

                            response = _coreMappers.parseResumeFields(response, modifiedFieldList);

                            if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                                return response;
                            }
                            response.processedWithLocale = lensSettingsList.get(0).getLocale();
                            parseResumeRequest.setLocale(lensSettingsList.get(0).getLocale());
                            return response;
                        } else {
                            return response;
                        }

                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "'");
                    }
                }
            }
        } catch (Exception ex) {
            response.status = false;
            apiContext.setApiResponse(response);
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Parse resume. Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "', Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Parse resume and retrieve BGXML or HRXML Aspect to dump the incoming
     * binaryData
     *
     * @param parseResumeRequestData
     * @param variant
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse parseResumeVariant(ParseResumeRequest parseResumeRequestData, String variant) throws Exception {
        ParseResumeRequest parseResumeRequest = new ParseResumeRequest();
        parseResumeRequest.setInstanceType(parseResumeRequestData.getInstanceType());
        parseResumeRequest.setBinaryData(parseResumeRequestData.getBinaryData());
        parseResumeRequest.setExtension(parseResumeRequestData.getExtension());
        parseResumeRequest.setLocale(parseResumeRequestData.getLocale());
        parseResumeRequest.setFieldList(parseResumeRequestData.getFieldList());
        ApiResponse response = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        try {
            _validator.validateParseResumeRequest(parseResumeRequest);
            boolean emptyLocale = false;

            parseResumeRequest.setExtension(_helper.isNotNullOrEmpty(parseResumeRequest.getExtension()) ? parseResumeRequest.getExtension() : apiConfig.getDefaultExtension());

            if (_helper.isNullOrEmpty(parseResumeRequest.getLocale())) {
                emptyLocale = true;
            }

            if (!_helper.isNotNull(apiContext.getClient())) {
                // When Authentication part is unavailable
                // Locale detection based parsing
                if (emptyLocale) {

                    // Get all lens settings of specific instance type
                    // Get all lens settings and ignore hrxml content
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseResumeRequest.getInstanceType());
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(variant.equalsIgnoreCase(VariantType.hrxml.toString()));
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Locale LensSettings Criteria
                    lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(InstanceType.LD.name());
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(variant.equalsIgnoreCase(VariantType.hrxml.toString()));
                    // Locale Fetch LensSettings
                    List<CoreLenssettings> localeLensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && localeLensSettingsList != null && localeLensSettingsList.size() > 0) {
                        lensSettingsList.add(localeLensSettingsList.get(0));
                    }

                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                            response = lensRespository.ldTagBinaryData(lensSettingsList, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, null);
                        } else if (variant.equalsIgnoreCase(VariantType.structuredbgtxml.toString())) {
                            response = lensRespository.ldTagBinaryStructuredBGTXMLData(lensSettingsList, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, null);
                        } else if (variant.equalsIgnoreCase(VariantType.hrxml.toString())) {
                            response = lensRespository.ldTagBinaryHRXMLData(lensSettingsList, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, null);
                        } else if (variant.equalsIgnoreCase(VariantType.htm.toString())) {
                            response = lensRespository.ldTagBinaryDataWithHTM(lensSettingsList, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, null);
                        } else if (variant.equalsIgnoreCase(VariantType.rtf.toString())) {
                            response = lensRespository.ldTagBinaryDataWithRTF(lensSettingsList, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, null);
                        }

                        if (response.status) {

                            if (!variant.equalsIgnoreCase(VariantType.hrxml.toString()) && !variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                                response.responseData = _helper.changeResponseDataObjectForStructuredJSON(response.responseData);
                            } else // for bgtxml requests
                            {
                                if (response.responseData.getClass().equals(String.class)) {
                                    response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                                } else {
                                    response.responseData = _helper.changeResponseDataHRXMLObject(response.responseData);
                                }
                            }

                            // response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                            // Set identified locale for dump document
                            parseResumeRequest.setLocale(response.identifiedLocale);
                        }
                        return response;
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "'");
                    }
                } else {
                    // When Authentication part is unavailable & Specified locale based parsing
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseResumeRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(parseResumeRequest.getLocale());
                    lensSettingsCriteria.setIsHRXML(variant.equalsIgnoreCase(VariantType.hrxml.toString()));
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (!lensSettingsList.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }
                        String customSkillCode = null;
                        List<CoreCustomskillsettings> coreCustomskillsettings = new ArrayList<>(lensSettingsList.get(0).getCoreCustomskillsettings());
                        if (_helper.isNotNull(coreCustomskillsettings) && !coreCustomskillsettings.isEmpty()) {
                            customSkillCode = coreCustomskillsettings.get(0).getCustomSkillCode();
                        }

                        if (variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                            response = lensRespository.tagBinaryData(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.structuredbgtxml.toString())) {
                            response = lensRespository.tagBinaryStructuredBGTXMLData(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.hrxml.toString())) {
                            response = lensRespository.tagBinaryHRXMLData(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.htm.toString())) {
                            response = lensRespository.tagBinaryDataWithHTM(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.rtf.toString())) {
                            response = lensRespository.tagBinaryDataWithRTF(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        }
                        if (response.status) {

                            if (!variant.equalsIgnoreCase(VariantType.hrxml.toString()) && !variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                                response.responseData = _helper.changeResponseDataObjectForStructuredJSON(response.responseData);
                            } else // for bgtxml requests
                            {
                                if (response.responseData.getClass().equals(String.class)) {
                                    response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                                } else {
                                    response.responseData = _helper.changeResponseDataHRXMLObject(response.responseData);
                                }
                            }

                            // response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                            // Set identified locale for dump document
                            parseResumeRequest.setLocale(response.identifiedLocale);
                        }
                        return response;
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "'");
                    }
                }
            } else {
                CoreClient client = apiContext.getClient();
                // Locale Detection based parsing
                String lensLocale = "";
                if (emptyLocale && client.isEnableLocaleDetection()) {

                    // Get all lens settings of specific instance type of client
                    // Get all lens settings and ignore hrxml content
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseResumeRequest.getInstanceType());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(variant.equalsIgnoreCase(VariantType.hrxml.toString()));
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Locale LensSettings Criteria
                    lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(InstanceType.LD.name());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(variant.equalsIgnoreCase(VariantType.hrxml.toString()));
                    // Locale Fetch LensSettings
                    List<CoreLenssettings> localeLensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && localeLensSettingsList != null && localeLensSettingsList.size() > 0) {
                        lensSettingsList.add(localeLensSettingsList.get(0));
                    }

                    List<CoreLenssettings> updatedLenssettingses = _helper.updateDefaultLocaleList(client.getDefaultLocale(), lensSettingsList, parseResumeRequest.getInstanceType());

                    if (updatedLenssettingses != null && updatedLenssettingses.size() > 0) {
                        if (variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                            response = lensRespository.ldTagBinaryData(updatedLenssettingses, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, client.getDefaultLocale());
                        } else if (variant.equalsIgnoreCase(VariantType.structuredbgtxml.toString())) {
                            response = lensRespository.ldTagBinaryStructuredBGTXMLData(updatedLenssettingses, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, client.getDefaultLocale());
                        } else if (variant.equalsIgnoreCase(VariantType.hrxml.toString())) {
                            response = lensRespository.ldTagBinaryHRXMLData(updatedLenssettingses, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, client.getDefaultLocale());
                        } else if (variant.equalsIgnoreCase(VariantType.htm.toString())) {
                            response = lensRespository.ldTagBinaryDataWithHTM(updatedLenssettingses, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, client.getDefaultLocale());
                        } else if (variant.equalsIgnoreCase(VariantType.rtf.toString())) {
                            response = lensRespository.ldTagBinaryDataWithRTF(updatedLenssettingses, _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, client.getDefaultLocale());
                        }
                        if (response.status) {

                            if (!variant.equalsIgnoreCase(VariantType.hrxml.toString()) && !variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                                response.responseData = _helper.changeResponseDataObjectForStructuredJSON(response.responseData);
                            } else // for bgtxml requests
                            {
                                if (response.responseData.getClass().equals(String.class)) {
                                    response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                                } else {
                                    response.responseData = _helper.changeResponseDataHRXMLObject(response.responseData);
                                }
                            }
                            // Set identified locale for dump document
                            parseResumeRequest.setLocale(response.identifiedLocale);
                            if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                                return response;
                            }
                        }
                        return response;
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                    }
                } else if (emptyLocale && _helper.isNotNullOrEmpty(client.getDefaultLocale())) {
                    // Default locale based parsing
                    DefaultLocaleList defaultLocaleList = (DefaultLocaleList) _helper.getObjectFromJson(client.getDefaultLocale(), new DefaultLocaleList());

                    if (defaultLocaleList != null) {
                        List<Locale> localeList = defaultLocaleList.getLocale();

                        if (localeList != null && localeList.size() > 0) {
                            for (Locale locale : localeList) {
                                if (locale.getLanguage().equals("*")) {
                                    parseResumeRequest.setLocale(locale.getCountry());
                                    lensLocale = locale.getCountry();
                                    emptyLocale = false;
                                }
                            }
                            if (emptyLocale) {
                                response.status = false;
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                            }
                        } else {
                            response.status = false;
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                    }
                } else if (emptyLocale) {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                }
                response.processedWithLocale = lensLocale;

                _validator.validateParseResumeRequest(parseResumeRequest);

                if (!emptyLocale) {
                    // Specified Locale based parsing
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseResumeRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(parseResumeRequest.getLocale());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setIsHRXML(variant.equalsIgnoreCase(VariantType.hrxml.toString()));
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (!lensSettingsList.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }

                        String customSkillCode = null;
                        List<CoreClientlenssettings> coreClientlenssettingses = new ArrayList<>(lensSettingsList.get(0).getCoreClientlenssettingses());
                        CoreCustomskillsettings coreCustomskillsettings = coreClientlenssettingses.get(0).getCoreCustomskillsettings();
                        if (_helper.isNotNull(coreCustomskillsettings)) {
                            customSkillCode = coreCustomskillsettings.getCustomSkillCode();
                        }

                        if (variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                            response = lensRespository.tagBinaryData(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.structuredbgtxml.toString())) {
                            response = lensRespository.tagBinaryStructuredBGTXMLData(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.hrxml.toString())) {
                            response = lensRespository.tagBinaryHRXMLData(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.htm.toString())) {
                            response = lensRespository.tagBinaryDataWithHTM(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.rtf.toString())) {
                            response = lensRespository.tagBinaryDataWithRTF(lensSettingsList.get(0), _helper.decodeBase64String(parseResumeRequest.getBinaryData()), parseResumeRequest.getExtension(), resumeType, customSkillCode);
                        }
                        if (response.status) {

                            if (!variant.equalsIgnoreCase(VariantType.hrxml.toString()) && !variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                                response.responseData = _helper.changeResponseDataObjectForStructuredJSON(response.responseData);
                            } else // for bgtxml requests
                            {
                                if (response.responseData.getClass().equals(String.class)) {
                                    response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                                } else {
                                    response.responseData = _helper.changeResponseDataHRXMLObject(response.responseData);
                                }
                            }
                            if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                                return response;
                            }

                            response.processedWithLocale = lensSettingsList.get(0).getLocale();
                            return response;
                        } else {
                            return response;
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "' Specified";
                        LOGGER.error("Invalid Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "'");
                    }
                }
            }
        } catch (Exception ex) {
            response.status = false;
            apiContext.setApiResponse(response);
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("TagBinaryDataWithXML response. Instance Type '" + parseResumeRequest.getInstanceType() + "' or Locale '" + parseResumeRequest.getLocale() + "' - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Parse Job and retrieve specific fields
     *
     * @param parseJobRequestData
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse parseJob(ParseJobRequest parseJobRequestData) throws Exception {
        ParseJobRequest parseJobRequest = new ParseJobRequest();
        parseJobRequest.setInstanceType(parseJobRequestData.getInstanceType());
        parseJobRequest.setBinaryData(parseJobRequestData.getBinaryData());
        parseJobRequest.setExtension(parseJobRequestData.getExtension());
        parseJobRequest.setLocale(parseJobRequestData.getLocale());
        parseJobRequest.setFieldList(parseJobRequestData.getFieldList());
        ApiResponse response = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        try {
            _validator.validateParseJobRequest(parseJobRequest);
            boolean emptyLocale = false;

            parseJobRequest.setExtension(_helper.isNotNullOrEmpty(parseJobRequest.getExtension()) ? parseJobRequest.getExtension() : apiConfig.getDefaultExtension());

            if (_helper.isNullOrEmpty(parseJobRequest.getLocale())) {
                emptyLocale = true;
            }

            if (_helper.isNull(apiContext.getClient())) {
                // When Authentication part unavailable
                // Locale detection based parsing
                if (emptyLocale) {
                    // Get all lens settings of specific instance type
                    // Get all lens settings and ignore hrxml content
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseJobRequest.getInstanceType());
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Locale LensSettings Criteria
                    lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(InstanceType.LD.name());
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Locale Fetch LensSettings
                    List<CoreLenssettings> localeLensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && localeLensSettingsList != null && localeLensSettingsList.size() > 0) {
                        lensSettingsList.add(localeLensSettingsList.get(0));
                    }
                    // Process Locale Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        response = lensRespository.ldTagBinaryData(lensSettingsList, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, null);

                        if (response.status) {
                            List<String> fields = _helper.isNull(parseJobRequest.getFieldList()) ? new ArrayList() : parseJobRequest.getFieldList().getField();
                            List<String> modifiedFieldList = new ArrayList<>();
                            for (String field : fields) {
                                modifiedFieldList.add(field.toLowerCase());
                            }
                            // Set identified locale for dump document
                            parseJobRequest.setLocale(response.identifiedLocale);
                            response = _coreMappers.parseJobFields(response, modifiedFieldList);
                        }

                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseJobRequest.getInstanceType() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "'");
                    }
                } else {
                    // Specified Locale based parsing
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseJobRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(parseJobRequest.getLocale());
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (!lensSettingsList.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }

                        String customSkillCode = null;
                        List<CoreCustomskillsettings> coreCustomskillsettings = new ArrayList<>(lensSettingsList.get(0).getCoreCustomskillsettings());
                        if (_helper.isNotNull(coreCustomskillsettings) && !coreCustomskillsettings.isEmpty()) {
                            customSkillCode = coreCustomskillsettings.get(0).getCustomSkillCode();
                        }

                        response = lensRespository.tagBinaryData(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);

                        if (response.status) {
                            List<String> fields = _helper.isNull(parseJobRequest.getFieldList()) ? new ArrayList() : parseJobRequest.getFieldList().getField();
                            List<String> modifiedFieldList = new ArrayList<>();
                            for (String field : fields) {
                                modifiedFieldList.add(field.toLowerCase());
                            }
                            response = _coreMappers.parseJobFields(response, modifiedFieldList);
                            if (_helper.isNullOrEmpty(response.processedWithLocale)) {
                                response.processedWithLocale = lensSettingsList.get(0).getLocale();
                            }
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "'");
                    }
                }
            } else {
                CoreClient client = apiContext.getClient();
                // Locale detection based parsing
                if (emptyLocale && client.isEnableLocaleDetection()) {

                    // Get all lens settings of specific client & instance type
                    // Get all lens settings and ignore hrxml content
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseJobRequest.getInstanceType());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Locale LensSettings Criteria
                    lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(InstanceType.LD.name());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Locale Fetch LensSettings
                    List<CoreLenssettings> localeLensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && localeLensSettingsList != null && localeLensSettingsList.size() > 0) {
                        lensSettingsList.add(localeLensSettingsList.get(0));
                    }

                    List<CoreLenssettings> updatedLenssettingses = _helper.updateDefaultLocaleList(client.getDefaultLocale(), lensSettingsList, parseJobRequest.getInstanceType());
                    // Process LocaleDetector Command
                    if (updatedLenssettingses != null && updatedLenssettingses.size() > 0) {
                        response = lensRespository.ldTagBinaryData(updatedLenssettingses, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, client.getDefaultLocale());

                        if (response.status) {
                            // Extract job fields from tag resposnse
                            List<String> fields = _helper.isNull(parseJobRequest.getFieldList()) ? new ArrayList() : parseJobRequest.getFieldList().getField();
                            List<String> modifiedFieldList = new ArrayList<>();
                            for (String field : fields) {
                                modifiedFieldList.add(field.toLowerCase());
                            }

                            // Set identified locale for dump document
                            parseJobRequest.setLocale(response.identifiedLocale);
                            response = _coreMappers.parseJobFields(response, modifiedFieldList);

                            if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                                return response;
                            }
                            return response;
                        } else {
                            return response;
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                    }
                } else if (emptyLocale && _helper.isNotNullOrEmpty(client.getDefaultLocale())) {
                    // Default locale based parsing
                    DefaultLocaleList defaultLocaleList = (DefaultLocaleList) _helper.getObjectFromJson(client.getDefaultLocale(), new DefaultLocaleList());

                    if (defaultLocaleList != null) {
                        List<Locale> localeList = defaultLocaleList.getLocale();

                        if (localeList != null && localeList.size() > 0) {
                            for (Locale locale : localeList) {
                                if (locale.getLanguage().equals("*")) {
                                    parseJobRequest.setLocale(locale.getCountry());
                                    emptyLocale = false;
                                }
                            }
                            if (emptyLocale) {
                                response.status = false;
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                            }
                        } else {
                            response.status = false;
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                    }
                } else if (emptyLocale) {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                }

                if (!emptyLocale) {
                    // Specified Locale based parsing
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseJobRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(parseJobRequest.getLocale());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (!lensSettingsList.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }
                        String customSkillCode = null;
                        List<CoreClientlenssettings> coreClientlenssettingses = new ArrayList<>(lensSettingsList.get(0).getCoreClientlenssettingses());
                        CoreCustomskillsettings coreCustomskillsettings = coreClientlenssettingses.get(0).getCoreCustomskillsettings();
                        if (_helper.isNotNull(coreCustomskillsettings)) {
                            customSkillCode = coreCustomskillsettings.getCustomSkillCode();
                        }

                        response = lensRespository.tagBinaryData(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);
                        if (response.status) {
                            // Extract job fields from tag resposnse
                            List<String> fields = _helper.isNull(parseJobRequest.getFieldList()) ? new ArrayList() : parseJobRequest.getFieldList().getField();
                            List<String> modifiedFieldList = new ArrayList<>();
                            for (String field : fields) {
                                modifiedFieldList.add(field.toLowerCase());
                            }
                            //String taggedXml = (String) response.responseData;
                            response = _coreMappers.parseJobFields(response, modifiedFieldList);
                            //response.responseData = _coreMappers.parseResumeFields(taggedXml, fields);

                            if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                                return response;
                            }
                            response.processedWithLocale = lensSettingsList.get(0).getLocale();
                            return response;
                        } else {
                            return response;
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "'");
                    }
                }
            }

        } catch (Exception ex) {
            response.status = false;
            apiContext.setApiResponse(response);
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Parse resume. Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "', Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Parse Job and retrieve BGXML or HRXML
     *
     * @param parseJobRequestData
     * @param variant
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse parseJobVariant(ParseJobRequest parseJobRequestData, String variant) throws Exception {
        ParseJobRequest parseJobRequest = new ParseJobRequest();
        parseJobRequest.setInstanceType(parseJobRequestData.getInstanceType());
        parseJobRequest.setBinaryData(parseJobRequestData.getBinaryData());
        parseJobRequest.setExtension(parseJobRequestData.getExtension());
        parseJobRequest.setLocale(parseJobRequestData.getLocale());
        parseJobRequest.setFieldList(parseJobRequestData.getFieldList());
        ApiResponse response = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        try {
            _validator.validateParseJobRequest(parseJobRequest);
            boolean emptyLocale = false;

            parseJobRequest.setExtension(_helper.isNotNullOrEmpty(parseJobRequest.getExtension()) ? parseJobRequest.getExtension() : apiConfig.getDefaultExtension());

            if (_helper.isNullOrEmpty(parseJobRequest.getLocale())) {
                emptyLocale = true;
            }

            if (!_helper.isNotNull(apiContext.getClient())) {
                // When Authentication part is unavailable
                // Locale detection based parsing
                if (emptyLocale) {
                    // Get all lens settings of specific instance type
                    // Get all lens settings and ignore hrxml content
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseJobRequest.getInstanceType());
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Locale LensSettings Criteria
                    lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(InstanceType.LD.name());
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Locale Fetch LensSettings
                    List<CoreLenssettings> localeLensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && localeLensSettingsList != null && localeLensSettingsList.size() > 0) {
                        lensSettingsList.add(localeLensSettingsList.get(0));
                    }

                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                            response = lensRespository.ldTagBinaryData(lensSettingsList, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, null);
                        } else if (variant.equalsIgnoreCase(VariantType.structuredbgtxml.toString())) {
                            response = lensRespository.ldTagBinaryStructuredBGTXMLData(lensSettingsList, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, null);
                        } else if (variant.equalsIgnoreCase(VariantType.htm.toString())) {
                            response = lensRespository.ldTagBinaryDataWithHTM(lensSettingsList, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, null);
                        } else if (variant.equalsIgnoreCase(VariantType.rtf.toString())) {
                            response = lensRespository.ldTagBinaryDataWithRTF(lensSettingsList, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, null);
                        }
                        if (response.status) {
                            if (!variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                                response.responseData = _helper.changeResponseDataObjectForStructuredJSON(response.responseData);
                            } else {
                                response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                                if (_helper.isNullOrEmpty(response.processedWithLocale)) {
                                    response.processedWithLocale = lensSettingsList.get(0).getLocale();
                                }
                            } // Set identified locale for dump document
                            parseJobRequest.setLocale(response.identifiedLocale);
                        }
                        return response;
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseJobRequest.getInstanceType() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "'");
                    }
                } else {
                    // When Authentication part is unavailable & Specified locale based parsing
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseJobRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(parseJobRequest.getLocale());
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (!lensSettingsList.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }
                        String customSkillCode = null;
                        List<CoreCustomskillsettings> coreCustomskillsettings = new ArrayList<>(lensSettingsList.get(0).getCoreCustomskillsettings());
                        if (_helper.isNotNull(coreCustomskillsettings) && !coreCustomskillsettings.isEmpty()) {
                            customSkillCode = coreCustomskillsettings.get(0).getCustomSkillCode();
                        }

                        if (variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                            response = lensRespository.tagBinaryData(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.structuredbgtxml.toString())) {
                            response = lensRespository.tagBinaryStructuredBGTXMLData(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.htm.toString())) {
                            response = lensRespository.tagBinaryDataWithHTM(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.rtf.toString())) {
                            response = lensRespository.tagBinaryDataWithRTF(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);
                        }
                        if (response.status) {
                            if (!variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                                response.responseData = _helper.changeResponseDataObjectForStructuredJSON(response.responseData);
                            } else {
                                response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                                if (_helper.isNullOrEmpty(response.processedWithLocale)) {
                                    response.processedWithLocale = lensSettingsList.get(0).getLocale();
                                }
                            }
                        }
                        return response;
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "'";
                        LOGGER.error("Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "'");
                    }
                }
            } else {
                String lensLocale = "";
                CoreClient client = apiContext.getClient();
                // Locale Detection based parsing
                if (emptyLocale && client.isEnableLocaleDetection()) {
                    // Get all lens settings of specific instance type of client
                    // Get all lens settings and ignore hrxml content
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseJobRequest.getInstanceType());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Locale LensSettings Criteria
                    lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(InstanceType.LD.name());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setStatus(true);
                    lensSettingsCriteria.setIsHRXML(false);
                    // Locale Fetch LensSettings
                    List<CoreLenssettings> localeLensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && localeLensSettingsList != null && localeLensSettingsList.size() > 0) {
                        lensSettingsList.add(localeLensSettingsList.get(0));
                    }

                    List<CoreLenssettings> updatedLenssettingses = _helper.updateDefaultLocaleList(client.getDefaultLocale(), lensSettingsList, parseJobRequest.getInstanceType());

                    if (updatedLenssettingses != null && updatedLenssettingses.size() > 0) {

                        if (variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                            response = lensRespository.ldTagBinaryData(updatedLenssettingses, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, client.getDefaultLocale());
                        } else if (variant.equalsIgnoreCase(VariantType.structuredbgtxml.toString())) {
                            response = lensRespository.ldTagBinaryStructuredBGTXMLData(updatedLenssettingses, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, client.getDefaultLocale());
                        } else if (variant.equalsIgnoreCase(VariantType.htm.toString())) {
                            response = lensRespository.ldTagBinaryDataWithHTM(updatedLenssettingses, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, client.getDefaultLocale());
                        } else if (variant.equalsIgnoreCase(VariantType.rtf.toString())) {
                            response = lensRespository.ldTagBinaryDataWithRTF(updatedLenssettingses, _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, client.getDefaultLocale());
                        }
                        if (response.status) {
                            if (!variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                                response.responseData = _helper.changeResponseDataObjectForStructuredJSON(response.responseData);
                            } else {
                                response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                                if (_helper.isNullOrEmpty(response.processedWithLocale)) {
                                    response.processedWithLocale = lensSettingsList.get(0).getLocale();
                                }
                            }
                            // Set identified locale for dump document
                            parseJobRequest.setLocale(response.identifiedLocale);
                            if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                                return response;
                            }
                            return response;
                        } else {
                            return response;
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                        response.responseData = "Instance not found";
                    }
                } else if (emptyLocale && _helper.isNotNullOrEmpty(client.getDefaultLocale())) {
                    // Default locale based parsing
                    DefaultLocaleList defaultLocaleList = (DefaultLocaleList) _helper.getObjectFromJson(client.getDefaultLocale(), new DefaultLocaleList());

                    if (defaultLocaleList != null) {
                        List<Locale> localeList = defaultLocaleList.getLocale();

                        if (localeList != null && localeList.size() > 0) {
                            for (Locale locale : localeList) {
                                if (locale.getLanguage().equals("*")) {
                                    parseJobRequest.setLocale(locale.getCountry());
                                    lensLocale = locale.getCountry();
                                    emptyLocale = false;
                                }
                            }
                            if (emptyLocale) {
                                response.status = false;
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                            }
                        } else {
                            response.status = false;
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                    }
                } else if (emptyLocale) {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                }

                response.processedWithLocale = lensLocale;

                _validator.validateParseJobRequest(parseJobRequest);
                if (!emptyLocale) {
                    // Specified Locale based parsing
                    // LensSettings Criteria
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseJobRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(parseJobRequest.getLocale());
                    lensSettingsCriteria.setCoreClient(client);
                    lensSettingsCriteria.setIsHRXML(variant.equalsIgnoreCase(VariantType.hrxml.toString()));
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (!lensSettingsList.get(0).isStatus()) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return response;
                        }

                        String customSkillCode = null;
                        List<CoreClientlenssettings> coreClientlenssettingses = new ArrayList<>(lensSettingsList.get(0).getCoreClientlenssettingses());
                        CoreCustomskillsettings coreCustomskillsettings = coreClientlenssettingses.get(0).getCoreCustomskillsettings();
                        if (_helper.isNotNull(coreCustomskillsettings)) {
                            customSkillCode = coreCustomskillsettings.getCustomSkillCode();
                        }

                        if (variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                            response = lensRespository.tagBinaryData(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.structuredbgtxml.toString())) {
                            response = lensRespository.tagBinaryStructuredBGTXMLData(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.htm.toString())) {
                            response = lensRespository.tagBinaryDataWithHTM(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);
                        } else if (variant.equalsIgnoreCase(VariantType.rtf.toString())) {
                            response = lensRespository.tagBinaryDataWithRTF(lensSettingsList.get(0), _helper.decodeBase64String(parseJobRequest.getBinaryData()), parseJobRequest.getExtension(), postingType, customSkillCode);
                        }
                        if (response.status) {
                            if (!variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                                response.responseData = _helper.changeResponseDataObjectForStructuredJSON(response.responseData);
                            } else {
                                response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                                if (_helper.isNullOrEmpty(response.processedWithLocale)) {
                                    response.processedWithLocale = lensSettingsList.get(0).getLocale();
                                }
                            }
                            if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                                return response;
                            }
                            if (_helper.isNullOrEmpty(response.processedWithLocale)) {
                                response.processedWithLocale = lensSettingsList.get(0).getLocale();
                            }
                            return response;
                        } else {
                            return response;
                        }
                    } else {
                        response.status = false;
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE_LOCALE);
                        // response.responseData = "Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "' Specified";
                        LOGGER.error("Invalid Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "'");
                    }
                }
            }
        } catch (Exception ex) {
            response.status = false;
            apiContext.setApiResponse(response);
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("TagBinaryDataWithXML response. Instance Type '" + parseJobRequest.getInstanceType() + "' or Locale '" + parseJobRequest.getLocale() + "' - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Find the locale of given document
     *
     * @param processLocaleRequest
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse findLocale(ProcessLocaleRequest processLocaleRequest) throws Exception {
        ApiResponse response = new ApiResponse();
        ApiContext apiContext = _helper.getApiContext();
        try {
            _validator.validateProcessLocaleRequest(processLocaleRequest);

            if (_helper.isNull(apiContext.getClient())) {
                // When Authentication part unavailable
                // LensSettings Criteria
                LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                lensSettingsCriteria.setInstanceType(InstanceType.LD.toString());
                lensSettingsCriteria.setIsHRXML(false);
                // Fetch LensSettings
                List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                // Process LENS Command
                if (lensSettingsList != null && lensSettingsList.size() > 0) {
                    if (!lensSettingsList.get(0).isStatus()) {
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                        return response;
                    }
                    response = lensRespository.findLocale(lensSettingsList.get(0), _helper.decodeBase64String(processLocaleRequest.getBinaryData()), processLocaleRequest.getExtension(), 'R');
                    if (response.status) {
                        response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                    }
                } else {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.LD_NOT_FOUND);
                    LOGGER.error("Locale detector instance not found");
                }
            } else {
                CoreClient client = apiContext.getClient();
                // Specified Locale based parsing
                // LensSettings Criteria
                LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                lensSettingsCriteria.setInstanceType(InstanceType.LD.toString());
                lensSettingsCriteria.setCoreClient(client);
                lensSettingsCriteria.setIsHRXML(false);
                // Fetch LensSettings
                List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                // Process LENS Command
                if (lensSettingsList != null && lensSettingsList.size() > 0) {

                    if (!lensSettingsList.get(0).isStatus()) {
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                        return response;
                    }
                    response = lensRespository.findLocale(lensSettingsList.get(0), _helper.decodeBase64String(processLocaleRequest.getBinaryData()), processLocaleRequest.getExtension(), 'R');
                    if (response.status) {
                        response.responseData = _helper.changeResponseDataObject((String) response.responseData);
                        if (!licenseManagementService.updateTransactionCount(apiContext.getClientApi())) {
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UPDATE_TRANSACTION_COUNT_ERROR);
                            return response;
                        }
                        return response;
                    } else {
                        return response;
                    }
                } else {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.LD_NOT_FOUND);
                    LOGGER.error("Locale detector instance not found");
                }
            }
        } catch (Exception ex) {
            response.status = false;
            apiContext.setApiResponse(response);
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Locale detector response. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }
    
     /**
     * Get the CoreClient by ClientId
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public CoreClient getCoreClient(int clientId) throws Exception{
        return dbService.getClientById(clientId);
    }
    
     /**
     * Parse resume and retrieve BGXML or HRXML Aspect to dump the incoming
     * binary data with microservice 
     *
     * @param parseResumeRequest
     * @param variant
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse parseResumeVariantMicroservice(ParseResumeRequest parseResumeRequest, String variant) throws Exception{   
        
        ApiResponse canonResponse=new ApiResponse();        
        ApiContext apiContext=_helper.getApiContext();
        CanonRequest canonRequest = new CanonRequest();
        parseResumeRequest.setExtension(_helper.isNotNullOrEmpty(parseResumeRequest.getExtension()) ? parseResumeRequest.getExtension() : "pdf");
        parseResumeRequest.setLocale(_helper.isNotNullOrEmpty(parseResumeRequest.getLocale()) ? parseResumeRequest.getLocale() : "en_us");
        try{
            String requestId= "";
            if (apiContext.getRequestId() != null){
                requestId=apiContext.getRequestId().toString();
            }
            if(variant.equalsIgnoreCase(VariantType.bgtxml.toString())){
                //getting the bold response from Bold Client Api
                long StartTime = System.currentTimeMillis();
                String resDoc= grpcClient.microService(requestId,parseResumeRequest.getBinaryData(),parseResumeRequest.getExtension(),false,false);
                long EndTime = System.currentTimeMillis()-StartTime;
                LOGGER.info("DocStoreService Elapsed Time is "+EndTime);
                if(resDoc!=""){ 
                    canonRequest.setInstanceType(parseResumeRequest.getInstanceType());
                    canonRequest.setLocale(parseResumeRequest.getLocale());
                    canonRequest.setBinaryData(_helper.encodeBase64String(resDoc.getBytes()));
                    if(apiContext.getAccept()!=null){
                    if(apiContext.getAccept().equalsIgnoreCase("application/json")){
                        apiContext.setAccept("application/xml");
                        _helper.setApiContext(apiContext);
                        canonResponse = canonBinaryData(canonRequest);
                        apiContext.setAccept("application/json");
                        _helper.setApiContext(apiContext);
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    if(canonResponse.getResponseData() instanceof String){
                        canonResponse.setResponseData(canonResponse.getResponseData().toString().replace("<bgtres>", "").replace("</bgtres>", "")); 
                    }           
                }else{
                    throw new Exception("Issue in getting response from BOLD API");
                }
            }else if(variant.equalsIgnoreCase(VariantType.structuredbgtxml.toString())){
                //getting the bold response from Bold Client Api
                long StartTime = System.currentTimeMillis();
                String resDoc= grpcClient.microService(requestId,parseResumeRequest.getBinaryData(),parseResumeRequest.getExtension(),false,false);
                long EndTime = System.currentTimeMillis()-StartTime;
                LOGGER.info("DocStoreService Elapsed Time is "+EndTime);
                if(resDoc!=""){
                    canonRequest.setInstanceType(parseResumeRequest.getInstanceType());
                    canonRequest.setLocale(parseResumeRequest.getLocale());
                    canonRequest.setBinaryData(_helper.encodeBase64String(resDoc.getBytes()));
                    if(apiContext.getAccept()!=null){
                    if(apiContext.getAccept().equalsIgnoreCase("application/json")){
                        apiContext.setAccept("application/xml");
                        _helper.setApiContext(apiContext);
                        canonResponse = canonBinaryData(canonRequest);
                        apiContext.setAccept("application/json");
                        _helper.setApiContext(apiContext);
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    if(canonResponse.getResponseData() instanceof String){
                        canonResponse.setResponseData(canonResponse.getResponseData().toString().replace("<bgtres>", "").replace("</bgtres>", "")); 
                    }
                    Object object = _helper.getLENSResponseObject(canonResponse.getResponseData().toString(), parseResumeRequest.getLocale());
                    canonResponse.responseData = _helper.isNotNull(object) ? object : lensEmptyMessage;
                }else{
                    throw new Exception("Issue in getting response from BOLD API");
                }
            }else if(variant.equalsIgnoreCase(VariantType.rtf.toString())){
                //getting the bold response from Bold Client Api
                long StartTime = System.currentTimeMillis();
                String resDoc= grpcClient.microService(requestId,parseResumeRequest.getBinaryData(),parseResumeRequest.getExtension(),false,true);
                long EndTime = System.currentTimeMillis()-StartTime;
                LOGGER.info("DocStoreService Elapsed Time is "+EndTime);
                if(resDoc!=""){
                    //Object object1 = _helper.getLENSResponseObject(resDoc,parseResumeRequest.getLocale());           
                    canonRequest.setInstanceType(parseResumeRequest.getInstanceType());
                    canonRequest.setLocale(parseResumeRequest.getLocale());
                    canonRequest.setBinaryData(_helper.encodeBase64String(resDoc.getBytes()));
                    if(apiContext.getAccept()!=null){
                    if(apiContext.getAccept().equalsIgnoreCase("application/json")){
                        apiContext.setAccept("application/xml");
                        _helper.setApiContext(apiContext);
                        canonResponse = canonBinaryData(canonRequest);
                        apiContext.setAccept("application/json");
                        _helper.setApiContext(apiContext);
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    if(canonResponse.getResponseData() instanceof String){
                        canonResponse.setResponseData(canonResponse.getResponseData().toString().replace("<bgtres>", "").replace("</bgtres>", "")); 
                    }
                    String strResult = canonResponse.getResponseData().toString();
                    String xml10pattern = "[^"
                            + "\u0009\r\n"
                            + "\u0020-\uD7FF"
                            + "\uE000-\uFFFD"
                            + "\ud800\udc00-\udbff\udfff"
                            + "]";
                    strResult = strResult.replaceAll(xml10pattern, "*");
                    Object object = _helper.getLENSResponseObject(strResult,parseResumeRequest.getLocale());
                    canonResponse.responseData = _helper.isNotNull(object) ? object : lensEmptyMessage;
                }else{
                     throw new Exception("Issue in getting response from BOLD API");
                }
            }else if(variant.equalsIgnoreCase(VariantType.htm.toString())){
                //getting the bold response from Bold Client Api
                long StartTime = System.currentTimeMillis();
                String resDoc= grpcClient.microService(requestId,parseResumeRequest.getBinaryData(),parseResumeRequest.getExtension(),true,false);
                long EndTime = System.currentTimeMillis()-StartTime;
                LOGGER.info("DocStoreService Elapsed Time is "+EndTime);
                if(resDoc!=""){
                    canonRequest.setInstanceType(parseResumeRequest.getInstanceType());
                    canonRequest.setLocale(parseResumeRequest.getLocale());
                    canonRequest.setBinaryData(_helper.encodeBase64String(resDoc.getBytes()));
                    if(apiContext.getAccept()!=null){
                    if(apiContext.getAccept().equalsIgnoreCase("application/json")){
                        apiContext.setAccept("application/xml");
                        _helper.setApiContext(apiContext);
                        canonResponse = canonBinaryData(canonRequest);
                        apiContext.setAccept("application/json");
                        _helper.setApiContext(apiContext);
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    if(canonResponse.getResponseData() instanceof String){
                        canonResponse.setResponseData(canonResponse.getResponseData().toString().replace("<bgtres>", "").replace("</bgtres>", "")); 
                    }
                    String strResult = canonResponse.getResponseData().toString();
                    String xml10pattern = "[^"
                            + "\u0009\r\n"
                            + "\u0020-\uD7FF"
                            + "\uE000-\uFFFD"
                            + "\ud800\udc00-\udbff\udfff"
                            + "]";
                    strResult = strResult.replaceAll(xml10pattern, "*");
                    Object object = _helper.getLENSResponseObject(strResult,parseResumeRequest.getLocale());
                    canonResponse.responseData = _helper.isNotNull(object) ? object : lensEmptyMessage;
                }else{
                     throw new Exception("Issue in getting response from BOLD API");
                }
            }else if(variant.equalsIgnoreCase(VariantType.hrxml.toString())){
                //getting the bold response from Bold Client Api
                long StartTime = System.currentTimeMillis();
                String resDoc= grpcClient.microService(requestId,parseResumeRequest.getBinaryData(),parseResumeRequest.getExtension(),false,false);
                long EndTime = System.currentTimeMillis()-StartTime;
                LOGGER.info("DocStoreService Elapsed Time is "+EndTime);
                if(resDoc!=""){
                    canonRequest.setInstanceType(parseResumeRequest.getInstanceType());
                    canonRequest.setLocale(parseResumeRequest.getLocale());
                    canonRequest.setBinaryData(_helper.encodeBase64String(resDoc.getBytes()));
                    if(apiContext.getAccept()!=null){
                    if(apiContext.getAccept().equalsIgnoreCase("application/json")){
                        apiContext.setAccept("application/xml");
                        _helper.setApiContext(apiContext);
                        canonResponse = canonBinaryData(canonRequest);
                        apiContext.setAccept("application/json");
                        _helper.setApiContext(apiContext);
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    if(canonResponse.getResponseData() instanceof String){
                        canonResponse.setResponseData(canonResponse.getResponseData().toString().replace("<bgtres>", "").replace("</bgtres>", "")); 
                    }
                    String strResult=canonResponse.getResponseData().toString();
                    LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                    lensSettingsCriteria.setInstanceType(parseResumeRequest.getInstanceType());
                    lensSettingsCriteria.setLocale(parseResumeRequest.getLocale());
                    lensSettingsCriteria.setCoreClient(apiContext.getClient());
                    lensSettingsCriteria.setIsHRXML(variant.equalsIgnoreCase(VariantType.hrxml.toString()));
                    // Fetch LensSettings
                    List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);
                    // Process LENS Command
                    if (lensSettingsList != null && lensSettingsList.size() > 0) {
                        if (!lensSettingsList.get(0).isStatus()) {
                            canonResponse.responseData = _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_LENS);
                            return canonResponse;
                        }
                    }
                    canonResponse=lensRespository.tagBinaryHRXMLDataBold(lensSettingsList.get(0),canonResponse);
                }else{
                     throw new Exception("Issue in getting response from BOLD API");
                } 
            }else {
                //getting the bold response from Bold Client Api
                long StartTime = System.currentTimeMillis();
                String resDoc= grpcClient.microService(requestId,parseResumeRequest.getBinaryData(),parseResumeRequest.getExtension(),false,false);
                long EndTime = System.currentTimeMillis()-StartTime;
                LOGGER.info("DocStoreService Elapsed Time is "+EndTime);
                if(resDoc!=""){
                    canonRequest.setInstanceType(parseResumeRequest.getInstanceType());
                    canonRequest.setLocale(parseResumeRequest.getLocale());
                    canonRequest.setBinaryData(_helper.encodeBase64String(resDoc.getBytes()));
                    if(apiContext.getAccept()!=null){
                    if(apiContext.getAccept().equalsIgnoreCase("application/json")){
                        apiContext.setAccept("application/xml");
                        _helper.setApiContext(apiContext);
                        canonResponse = canonBinaryData(canonRequest);
                        apiContext.setAccept("application/json");
                        _helper.setApiContext(apiContext);
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    }else{canonResponse = canonBinaryData(canonRequest);}
                    if(canonResponse.getResponseData() instanceof String){
                        canonResponse.setResponseData(canonResponse.getResponseData().toString().replace("<bgtres>", "").replace("</bgtres>", "")); 
                    }
                     if (canonResponse.status) {
                                    // Extract resume fields from tag resposnse
                                    List<String> fields = _helper.isNull(parseResumeRequest.getFieldList()) ? new ArrayList() : parseResumeRequest.getFieldList().getField();
                                    List<String> modifiedFieldList = new ArrayList<>();
                                    for (String field : fields) {
                                        modifiedFieldList.add(field.toLowerCase());
                                    }
                                    // Set identified locale for dump document
                                    parseResumeRequest.setLocale(canonResponse.identifiedLocale);
                                    canonResponse = _coreMappers.parseResumeFields(canonResponse, modifiedFieldList);

                    }
                }
                else{
                         throw new Exception("Issue in getting response from BOLD API");
                }
            }
            if (canonResponse.status) {
                if (!variant.equalsIgnoreCase(VariantType.hrxml.toString()) && !variant.equalsIgnoreCase(VariantType.bgtxml.toString())) {
                    canonResponse.responseData = _helper.changeResponseDataObjectForStructuredJSON(canonResponse.responseData);
                } 
                else // for bgtxml requests
                {
                    if (canonResponse.responseData.getClass().equals(String.class)) {
                        canonResponse.responseData = _helper.changeResponseDataObject((String) canonResponse.responseData);
                    } else {
                        canonResponse.responseData = _helper.changeResponseDataHRXMLObject(canonResponse.responseData);
                    }
                }
            }
            return canonResponse ;
          }catch(Exception e){
            LOGGER.error("DocStoreService Exception - " + e.getMessage());
            throw new Exception("Issue in getting response from DocStoreService");
          } 
           
    }
    
    
    public CoreClient getClientDetails(String consumerKey) throws Exception {
        try {
            ClientCriteria clientCriteria = new ClientCriteria();
            clientCriteria.setClientKey(consumerKey);
            List<CoreClient> coreClientList = dbService.getClientByCriteria(clientCriteria);
            if (coreClientList == null || coreClientList.size() < 0) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.UNAUTHORIZED_RESOURCE_ACCESS), HttpStatus.BAD_REQUEST);
            }
            return coreClientList.get(0);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public long getInitialTimeStamp(List<String> keysList) {
        List<Long> timeStampList = new ArrayList<>();
        for (String key : keysList) {
            String timeStamp = (key.split("_"))[3];
            timeStampList.add(Long.parseLong(timeStamp));
        }
        Collections.sort(timeStampList);
        return timeStampList.get(0);
    }

    public int getCounterSum(List<String> keysList) {
        int counterSum = 0;
        for (String key : keysList) {
            RedisAtomicInteger counter = new RedisAtomicInteger(key, template.getConnectionFactory());
            int count = counter.get();
            counterSum += count;
        }
        return counterSum;
    }

    public boolean isRateLimitExceeded(List<String> keysList) {
        int counterSum = 0;
        boolean isLimitExceeded = false;
        for (String key : keysList) {
            RedisAtomicInteger counter = new RedisAtomicInteger(key, template.getConnectionFactory());
            int count = counter.get();
            counterSum += count;
        }
        String[] keyParams = keysList.get(0).split("_");
        String clientRateLimit = keyParams[2];

        if (counterSum >= Integer.parseInt(clientRateLimit)) {
            isLimitExceeded = true;
        }
        return isLimitExceeded;
    }


}
