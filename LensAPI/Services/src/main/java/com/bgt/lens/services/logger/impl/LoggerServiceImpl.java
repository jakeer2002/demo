// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.logger.impl;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.CoreMappers;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.ResolverMappers;
import com.bgt.lens.helpers.Validator;
import com.bgt.lens.model.adminservice.request.GetErrorLogsRequest;
import com.bgt.lens.model.adminservice.request.GetUsageCounterRequest;
import com.bgt.lens.model.adminservice.request.GetUsageLogsRequest;
import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.criteria.ErrorLogCriteria;
import com.bgt.lens.model.criteria.SearchFilterTypeCriteria;
import com.bgt.lens.model.criteria.UsageLogCriteria;
import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreErrorlog;
import com.bgt.lens.model.entity.CoreUsagecounter;
import com.bgt.lens.model.entity.CoreUsagelog;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchFilterLog;
import com.bgt.lens.model.entity.SearchFilterType;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.datarepository.impl.DataRepositoryServiceImpl;
import com.bgt.lens.services.logger.ILoggerService;
import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.AsyncResult;

/**
 *
 * Logger service implementation
 */
public class LoggerServiceImpl implements ILoggerService {

    private static final Logger LOGGER = LogManager.getLogger(DataRepositoryServiceImpl.class);
    private final Helper _helper = new Helper();
    private final Validator _validator = new Validator();
    private final CoreMappers _coreMappers = new CoreMappers();
    private final ResolverMappers _resolverMappers = new ResolverMappers();
    private ApiConfiguration apiConfig;

    private IDataRepositoryService dataRepositoryService;
    
    @Autowired
    private AsyncLogStorageService asyncLogStorageService;
    
    public LoggerServiceImpl(ElasticSearchTransportClient esTransportClient){
    }

    /**
     * Set data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dataRepositoryService = dataRepositoryService;
    }

    /**
     * set ApiConfig
     *
     * @param apiConfig
     */
    public void setapiConfig(ApiConfiguration apiConfig) {
        this.apiConfig = apiConfig;
    }

    /**
     * Add authentication log
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addAuthenticationLog(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addAuthenticationLogAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_AUTHENTICATIONLOG_ERROR));
        }
    }

    /**
     * Add Lens document
     *
     * @param context
     * @param binaryData
     * @param docType
     * @param instanceType
     * @param locale
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addLensDocument(ApiContext context, String binaryData, String docType, String instanceType, String locale) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addLensDocumentAsync(context, binaryData, docType, instanceType, locale);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_LENSDOCUMENTDUMP_ERROR));
        }
    }

    @Override
    public ApiResponse addSearchCommandDump(ApiContext context) throws Exception {

        if (_helper.isNotNull(context)) {
            SearchFilterTypeCriteria searchFilterTypeCriteria = new SearchFilterTypeCriteria();
            SearchCommanddump searchCommandDump = new SearchCommanddump();
            List<SearchFilterType> searchFilterTypeList = null;
            Set<SearchFilterLog> searchFilterLogList = new HashSet<SearchFilterLog>();
            SearchFilterLog searchFilterLog;

            searchCommandDump.setRequestId(context.getRequestId().toString());

            if (_helper.isNotNull(context.getClient()) && _helper.isNotNull(context.getClient().getId())) {
                searchCommandDump.setClientId(context.getClient().getId());
            } else {
                searchCommandDump.setClientId(0);
            }
            if (_helper.isNotNull(context.getLensSettingsId())) {
                searchCommandDump.setLensSettingsId(context.getLensSettingsId());
            } else {
                searchCommandDump.setLensSettingsId(0);
            }
            if (_helper.isNotNull(context.getVendorId())) {
                searchCommandDump.setVendorId(context.getVendorId());
            } else {
                searchCommandDump.setVendorId(0);
            }
            if (_helper.isNotNull(context.getSearchCommand())) {
                searchCommandDump.setSearchCommand(context.getSearchCommand());
            } else {
                searchCommandDump.setSearchCommand("");
            }
            if (_helper.isNotNull(context.getLocale())) {
                searchCommandDump.setLocale(context.getLocale());
            } else {
                searchCommandDump.setLocale("");
            }
            if (_helper.isNotNull(context.getInstanceType())) {
                searchCommandDump.setInstanceType(context.getInstanceType());
            } else {
                searchCommandDump.setInstanceType("");
            }
            if (_helper.isNotNull(context.getDocType())) {
                searchCommandDump.setDocumentType(context.getDocType());
            } else {
                searchCommandDump.setDocumentType("");
            }

            searchCommandDump.setCreatedOn(Date.from(Instant.now()));

            if (_helper.isNotNull(context.getFilterType())) {
                //get FilterId from Search Filter Type table
                searchFilterTypeCriteria.setFilterType(context.getFilterType());

                searchFilterTypeList = dataRepositoryService.getFilterIDByName(searchFilterTypeCriteria);
                if (_helper.isNotNull(searchFilterTypeList)) {
                    for (int index = 0; index < searchFilterTypeList.size(); index++) {
                        SearchFilterType searchFilterTypeObj = new SearchFilterType();
                        Object obj = searchFilterTypeList.get(index);
                        searchFilterTypeObj.setId((Integer) obj);
                        searchFilterLogList.add(new SearchFilterLog(searchCommandDump, searchFilterTypeObj));
                    }
                    searchCommandDump.setSearchFilterLogs(searchFilterLogList);
                }
            } else {
                searchCommandDump.setSearchFilterLogs(null);
            }

            ApiResponse response = dataRepositoryService.addSearchCommandDump(searchCommandDump);
            response.responseData = searchCommandDump;
            return response;
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_SEARCHCOMMANDDUMP_ERROR));
        }
    }

    @Override
    public Future<Boolean> addRegisterLog(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addSearchRegisterLogAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_REGISTERLOG_ERROR));
        }
    }

    /**
     * Add usage log
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addUsageLog(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addUsageLogAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_USAGELOG_ERROR));
        }
    }

    /**
     * Add error log
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addErrorLog(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addErrorLogAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_ERRORLOG_ERROR));
        }
    }
    
    /**
     * Add advanced log
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addAdvancedLog(ApiContext context) throws Exception {
        return new AsyncResult<>(true);
//        if (_helper.isNotNull(context)) {
//            asyncLogStorageService.addErrorLogAsync(context);
//            return new AsyncResult<>(true);
//        } else {
//            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_ERRORLOG_ERROR));
//        }
    }

    /**
     * Update usage counter
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> updateUsageCounter(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.updateUsageCounterAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.UPDATE_USAGECOUNTER_ERROR));
        }
    }

    /**
     * Get usage logs
     *
     * @param usageLog
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getUsageLogs(GetUsageLogsRequest usageLog, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
        // TODO : move it to validator
        try {

            if (_helper.isNotNullOrEmpty(usageLog.getFrom())) {
                usageLogCriteria.setFrom(_helper.parseDate(usageLog.getFrom()));
            }

            if (_helper.isNotNullOrEmpty(usageLog.getTo())) {
                usageLogCriteria.setTo(new LocalDate(_helper.parseDate(usageLog.getTo())).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
            }
            
            //Check consumer key
            String consumerKey = "";
            if (!_helper.isNull(clientId)) {
                CoreClient client = dataRepositoryService.getClientById(clientId);
                if (client != null) {
                    consumerKey = client.getClientKey();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID), HttpStatus.BAD_REQUEST);
                }
            }

            Integer apiId = null;
            //Check api Id
            if (!_helper.isNullOrEmpty(usageLog.getApiKey())) {
                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(usageLog.getApiKey());

                List<CoreApi> apiList = dataRepositoryService.getApiByCriteria(criteria);

                if (apiList != null && apiList.size() > 0) {
                    apiId = apiList.get(0).getId();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }
            }

            // validate http method
            if (!_helper.isNullOrEmpty(usageLog.getMethod())) {
                if (!_helper.isValidHttpMethod(usageLog.getMethod())) {
                    LOGGER.error("Invalid http method : " + usageLog.getMethod());
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_HTTP_METHOD), HttpStatus.BAD_REQUEST);
                }
            }

            // validate http method
            if (!_helper.isNullOrEmpty(usageLog.getResource())) {
                if (!_helper.isValidAPIResource(usageLog.getResource())) {
                    LOGGER.error("Invalid resource name : " + usageLog.getResource());
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_NAME), HttpStatus.BAD_REQUEST);
                }
            }

            usageLogCriteria = _coreMappers.createUsageLogsCriteria(usageLogCriteria, usageLog, clientId, null, apiId, null);

            List<CoreUsagelog> coreUsageLogList = dataRepositoryService.getUsagelogByCriteria(usageLogCriteria);

            if (coreUsageLogList == null) {
                LOGGER.info("Usage log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_LOG_NOT_EXIST);
                return response;
            } else {
                List<CoreApi> apiList = new ArrayList<>();
                Map apiMap = new HashMap<>();
                if(_helper.isNullOrEmpty(usageLog.getApiKey())){
                    apiList = dataRepositoryService.getApiByCriteria(new ApiCriteria());
                    for(CoreApi api : apiList){
                        apiMap.put(api.getId(), api.getApiKey());
                    }
                }
                
                for(CoreUsagelog log : coreUsageLogList){
                    if(_helper.isNullOrEmpty(usageLog.getApiKey())){
                        log.setApi((String)apiMap.get(log.getApiId()));
                    }
                    else{
                        log.setApi(usageLog.getApiKey());
                    }
                    log.setConsumerKey(consumerKey);
                }
                
                response.status = true;
                response.responseData = _coreMappers.createUsageLogResponse(coreUsageLogList);
            }
        } catch (ParseException ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage log. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Get usage logs by Id
     *
     * @param usageLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getUsageLogById(String usageLogId, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            //Check consumer key
            String consumerKey = "";
            if (!_helper.isNull(clientId)) {
                CoreClient client = dataRepositoryService.getClientById(clientId);
                if (client != null) {
                    consumerKey = client.getClientKey();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID), HttpStatus.BAD_REQUEST);
                }
            }
            
            CoreUsagelog coreUsageLog = dataRepositoryService.getUsagelogById(usageLogId, clientId);

            if (coreUsageLog == null) {
                LOGGER.info("Usage log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_LOG_NOT_EXIST);
                return response;
            } else {
                List<CoreApi> apiList = new ArrayList<>();
                Map apiMap = new HashMap<>();
                apiList = dataRepositoryService.getApiByCriteria(new ApiCriteria());
                for(CoreApi api : apiList){
                    apiMap.put(api.getId(), api.getApiKey());
                }
                
                coreUsageLog.setApi((String)apiMap.get(coreUsageLog.getApiId()));
                coreUsageLog.setConsumerKey(consumerKey);
                
                response.status = true;
                response.responseData = _coreMappers.createUsageLogResponse(coreUsageLog);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage counter. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get error log by criteria
     *
     * @param errorLogs
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getErrorLogs(GetErrorLogsRequest errorLogs, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            Integer apiId = null;
            //Check consumer key
            String consumerKey = "";
            if (!_helper.isNull(clientId)) {
                CoreClient client = dataRepositoryService.getClientById(clientId);
                if (client != null) {
                    consumerKey = client.getClientKey();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID), HttpStatus.BAD_REQUEST);
                }
            }
            
            //check api Id
            if (!_helper.isNullOrEmpty(errorLogs.getApiKey())) {
                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(errorLogs.getApiKey());

                List<CoreApi> apiList = dataRepositoryService.getApiByCriteria(criteria);

                if (apiList != null && apiList.size() > 0) {
                    apiId = apiList.get(0).getId();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }
            }

            ErrorLogCriteria errorLogCriteria = _coreMappers.createErrorLogsCriteria(errorLogs, clientId, null, apiId, null);

            List<CoreErrorlog> coreErrorLogList = dataRepositoryService.getErrorlogByCriteria(errorLogCriteria);

            if (coreErrorLogList == null) {
                LOGGER.info("Error log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
                return response;
            } else {
                List<CoreApi> apiList = new ArrayList<>();
                Map apiMap = new HashMap<>();
                if(_helper.isNullOrEmpty(errorLogs.getApiKey())){
                    apiList = dataRepositoryService.getApiByCriteria(new ApiCriteria());
                    for(CoreApi api : apiList){
                        apiMap.put(api.getId(), api.getApiKey());
                    }
                }
                
                for(CoreErrorlog log : coreErrorLogList){
                    if(_helper.isNullOrEmpty(errorLogs.getApiKey())){
                        log.setApi((String)apiMap.get(log.getApiId()));
                    }
                    else{
                        log.setApi(errorLogs.getApiKey());
                    }
                    log.setConsumerKey(consumerKey);
                }
                response.status = true;
                response.responseData = _coreMappers.createErrorLogResponse(coreErrorLogList);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get error log. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get error log by Id
     *
     * @param errorLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getErrorLogById(String errorLogId, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            //Check consumer key
            String consumerKey = "";
            if (!_helper.isNull(clientId)) {
                CoreClient client = dataRepositoryService.getClientById(clientId);
                if (client != null) {
                    consumerKey = client.getClientKey();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID), HttpStatus.BAD_REQUEST);
                }
            }
            
            CoreErrorlog coreErrorLog = dataRepositoryService.getErrorlogById(errorLogId, clientId);

            if (coreErrorLog == null) {
                LOGGER.info("Error log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
                return response;
            } else {
                List<CoreApi> apiList = new ArrayList<>();
                Map apiMap = new HashMap<>();
                apiList = dataRepositoryService.getApiByCriteria(new ApiCriteria());
                for(CoreApi api : apiList){
                    apiMap.put(api.getId(), api.getApiKey());
                }
                
                coreErrorLog.setApi((String)apiMap.get(coreErrorLog.getApiId()));
                coreErrorLog.setConsumerKey(consumerKey);
                    
                response.status = true;
                response.responseData = _coreMappers.createErrorLogResponse(coreErrorLog);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage counter. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get usage counter
     *
     * @param usageCounter
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getUsageCounter(GetUsageCounterRequest usageCounter, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            Integer apiId = null;
            //check api Id
            if (!_helper.isNullOrEmpty(usageCounter.getApiKey())) {
                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(usageCounter.getApiKey());

                List<CoreApi> apiList = dataRepositoryService.getApiByCriteria(criteria);

                if (apiList != null && apiList.size() > 0) {
                    apiId = apiList.get(0).getId();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }
            }

            UsagecounterCriteria usageCounterCriteria = _coreMappers.createUsageCounterCriteria(usageCounter, clientId, apiId);

            List<CoreUsagecounter> coreUsageCounterList = dataRepositoryService.getUsagecounterByCriteria(usageCounterCriteria);

            if (coreUsageCounterList == null) {
                LOGGER.info("Usage counter does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_COUNTER_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createUsageCounterResponse(coreUsageCounterList);
            }
        } catch (ParseException ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage counter. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Get usage counter by id
     *
     * @param usageCounterId
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getUsageCounterById(Integer usageCounterId, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            CoreUsagecounter coreUsageCounterList = dataRepositoryService.getUsagecounterById(usageCounterId, clientId);

            if (coreUsageCounterList == null) {
                LOGGER.info("Usage counter does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_COUNTER_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createUSageCounterResponse(coreUsageCounterList);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage counter. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

}
