// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import org.springframework.ws.context.MessageContext;

/**
 *
 * SOAP message context identifier
 */
public final class MessageContextHolder {

    private static final ThreadLocal<MessageContext> threadLocal
            = new ThreadLocal() {
                @Override
                protected MessageContext initialValue() {
                    return null;
                }
            };

    /**
     * Initialize message context
     */
    private MessageContextHolder() {
    }

    /**
     * Get message context
     *
     * @return
     */
    public static MessageContext getMessageContext() {
        return threadLocal.get();
    }

    /**
     * Set message context
     *
     * @param context
     */
    public static void setMessageContext(MessageContext context) {
        threadLocal.set(context);
    }

    /**
     * Remove message context
     */
    public static void removeMessageContext() {
        threadLocal.remove();
    }
}