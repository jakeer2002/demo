// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.authentication.IAuthenticationService;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Authentication aspect
 */
@Aspect
public class AuthenticationAspect {

    private IAuthenticationService authenticationService = null;

    /**
     * Set authentication service
     *
     * @param authenticationService
     */
    public void setAuthenticationService(IAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    /**
     * Validate authentication REST
     *
     * @param joinPoint
     * @return
     * @throws Exception
     * @throws Throwable
     */
    @Around("execution(public * com.bgt.lens.web.rest.CoreController.*(..))||execution(public * com.bgt.lens.web.rest.UtilityController.*(..))")
    public ResponseEntity<ApiResponse> validateAuthentication(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        ApiResponse response = authenticationService.validateApiContext(request);

        if (response.status) {
            return (ResponseEntity<ApiResponse>) joinPoint.proceed();
        } else {
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * Validate authentication SOAP
     *
     * @param joinPoint
     * @return
     * @throws Exception
     * @throws Throwable
     */
    @Around("execution(* com.bgt.lens.web.soap.SoapEndPoint.*(..))")
    public Object validateSoapAuthentication(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        ApiResponse response = authenticationService.validateApiContext(request);
        if (response.status) {
            return joinPoint.proceed();
        } else {
            return response;
        }
    }
}
