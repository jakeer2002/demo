// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.license;

import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.rest.response.ApiResponse;

/**
 *
 * License management Interface
 */
public interface ILicenseManagementService {

    /**
     * Verify client license activation
     *
     * @param clientId
     * @param apiId
     * @return License data
     */
    public boolean checkLicenseActivationDate(int clientId, int apiId);

    /**
     * Verify client license expiry date
     *
     * @param clientId
     * @param apiId
     * @return License data
     */
    public boolean checkLicenseExpiryDate(int clientId, int apiId);

    /**
     * Verify the remaining transaction count of a user
     *
     * @param clientId
     * @param apiId
     * @return License data
     */
    public boolean checkRemainingTransactionCount(int clientId, int apiId);

    /**
     * Update the remaining transaction count
     *
     * @param clientId
     * @param apiId
     * @return License data
     */
    public ApiResponse updateRemainingTransactionCount(int clientId, int apiId);

    /**
     * Get License details by client
     *
     * @param clientId
     * @return License data
     * @throws Exception
     */
    public ApiResponse getLicenseDetailsById(int clientId) throws Exception;

    /**
     * Get License details of all existing client
     *
     * @return List of License data of all the clients
     * @throws Exception
     */
    public ApiResponse getLicenseDetailsAll() throws Exception;

    /**
     * Update Transaction Count for each successFul request.
     *
     * @param coreClientapi
     * @return
     * @throws Exception
     */
    public boolean updateTransactionCount(CoreClientapi coreClientapi) throws Exception;
}
