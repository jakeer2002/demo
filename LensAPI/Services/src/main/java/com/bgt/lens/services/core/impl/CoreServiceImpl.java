// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.core.impl;

import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.CoreMappers;
import com.bgt.lens.helpers.Enum.InstanceType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.Validator;
import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.adminservice.request.ClientApi;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.CustomSkillsList;
import com.bgt.lens.model.adminservice.request.GetApiRequest;
import com.bgt.lens.model.adminservice.request.GetClientsRequest;
import com.bgt.lens.model.adminservice.request.GetLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.GetResourceRequest;
import com.bgt.lens.model.adminservice.request.InstanceList;
import com.bgt.lens.model.adminservice.request.SearchSettings;
import com.bgt.lens.model.adminservice.request.UpdateApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest.CustomSkillsSettings;
import com.bgt.lens.model.adminservice.request.UpdateResourceRequest;
import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.LensSettingsCriteria;
import com.bgt.lens.model.criteria.ResourceCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.CoreResources;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.Client;
import com.bgt.lens.model.rest.response.ClientLensSettings;
import com.bgt.lens.model.rest.response.FilterSettings;
import com.bgt.lens.model.rest.response.LensSettings;
import com.bgt.lens.services.core.ICoreService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.http.HttpStatus;

/**
 *
 * Core service implementation
 */
public class CoreServiceImpl implements ICoreService {

    private static final Logger LOGGER = LogManager.getLogger(CoreServiceImpl.class);
    private final Helper _helper = new Helper();
    private final CoreMappers _coreMappers = new CoreMappers();
    private final Validator _validator = new Validator();
    private final int internal = 1;
    private final int external = 2;

    private IDataRepositoryService dbService;

    private ApiConfiguration apiConfig;

    /**
     * Set data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dbService = dataRepositoryService;
    }

    /**
     * set ApiConfig
     *
     * @param apiConfig
     */
    public void setapiConfig(ApiConfiguration apiConfig) {
        this.apiConfig = apiConfig;
    }

    /**
     * Creates new API
     *
     * @param api
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse createApi(CreateApiRequest api) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Validate Api. ApiKey -  " + api.getKey());
            if (_validator.validateApi(api)) {

//                ApiCriteria criteria = new ApiCriteria();
//                criteria.setApiKey(api.getKey());
//                criteria.setName(api.getName());
//                List<CoreApi> apiList = dbService.getDuplicateApiByCriteria(criteria);
//
//                if (_helper.isNull(apiList) || apiList.size() <= 0) {
                LOGGER.info("Create Api entity. ApiKey - " + api.getKey());
                CoreApi coreApi = _coreMappers.createApiEntity(api);

                ApiResponse dbResponse = dbService.createApi(coreApi);

                if (dbResponse.status) {
                    response.status = true;
                    response.responseData = _coreMappers.createApiResponse(coreApi);
                } else {
                    response.responseData = dbResponse.responseData;
                }
//                } else {
//                    LOGGER.error("Create Api. ApiKey - " + api.getKey() + ", Exception -  " + ApiErrors.DUPLICATE_API_KEY_NAME);
//                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_API_KEY_NAME, HttpStatus.BAD_REQUEST);
//                }
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Create Api. ApiKey - " + api.getKey() + ", Exception -  " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Update API details
     *
     * @param apiId
     * @param api
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateApi(int apiId, UpdateApiRequest api) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Validate Api. ApiKey -  " + api.getKey());
            if (_validator.validateApi(api)) {

                LOGGER.info("Check Api exists. ApiId - " + apiId);
                CoreApi coreApi = dbService.getApiById(apiId);

                if (coreApi == null) {
                    LOGGER.error("Api does not exist");
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.API_NOT_EXIST);
                    return response;
                }

                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(api.getKey());
                criteria.setName(api.getName());
                List<CoreApi> apiList = dbService.getDuplicateApiByCriteria(criteria);

                if (_helper.isNull(apiList) || apiList.size() <= 0 || (apiList.size() == 1 && apiList.get(0).getId().equals(apiId))) {
                    LOGGER.info("Update Api entity. ApiId -  " + apiId);
                    CoreApi updatedApi = _coreMappers.updateApiEntity(api);

                    updatedApi.setId(apiId);
                    updatedApi.setApiKey(_helper.isNotNullOrEmpty(api.getKey()) ? updatedApi.getApiKey() : coreApi.getApiKey());
                    updatedApi.setName(_helper.isNotNullOrEmpty(api.getName()) ? updatedApi.getName() : coreApi.getName());
                    updatedApi.setCreatedOn(coreApi.getCreatedOn());

                    ApiResponse dbResponse = dbService.updateApi(updatedApi);

                    if (dbResponse.status) {
                        response.status = true;
                        response.responseData = _coreMappers.createApiResponse(updatedApi);
                    } else {
                        response.responseData = dbResponse.responseData;
                    }
                } else {

                    LOGGER.error("Update Api. ApiKey - " + api.getKey() + ", Exception -  " + ApiErrors.DUPLICATE_API_KEY_NAME);
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.DUPLICATE_API_KEY_NAME), HttpStatus.BAD_REQUEST);
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Update Api. ApiKey - " + api.getKey() + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get API details by criteria
     *
     * @param api
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getApi(GetApiRequest api) throws Exception {
        ApiResponse response = new ApiResponse();
        try {

            ApiCriteria apiCriteria = _coreMappers.createApiCriteria(api);

            List<CoreApi> coreApiList = dbService.getApiByCriteria(apiCriteria);

            if (coreApiList == null) {
                LOGGER.info("Api does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.API_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createApiResponse(coreApiList);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get Api. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get API by Id
     *
     * @param apiId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getApiById(int apiId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            CoreApi coreApi = dbService.getApiById(apiId);

            if (coreApi == null) {
                LOGGER.info("Api does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.API_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createApiResponse(coreApi);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get Api. ApiId - " + apiId + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Delete API
     *
     * @param apiId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse deleteApi(int apiId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Check Api exists. ApiId - " + apiId);
            CoreApi coreApi = dbService.getApiById(apiId);

            if (coreApi == null) {
                LOGGER.error("Api does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.API_NOT_EXIST);
                return response;
            }
            //Send delete request to db service
            ApiResponse dbResponse = dbService.deleteApi(coreApi);

            if (dbResponse.status) {
                response.status = true;
                response.responseData = _coreMappers.deleteApiResponse(apiId);
            } else {
                response.responseData = dbResponse.responseData;
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Delete Api. apiId - " + apiId + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Add resource
     *
     * @param resource
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse addResource(AddResourceRequest resource) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Validate resource. ResourceName - " + resource.getResourceName() + ", ApiKey - " + resource.getApiKey());
            if (_validator.validateResource(resource)) {

                //Get api data
                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(resource.getApiKey());

                List<CoreApi> apiList = dbService.getApiByCriteria(criteria);

                CoreApi api = null;

                if (apiList != null && apiList.size() > 0) {
                    api = apiList.get(0);
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }

                CoreResources coreResources = _coreMappers.createResourceEntity(resource, api);

                ApiResponse dbResponse = dbService.createResource(coreResources);

                if (dbResponse.status) {
                    response.status = true;
                    response.responseData = _coreMappers.createResourceResponse(coreResources);
                } else {
                    response.responseData = dbResponse.responseData;
                }

            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add resource. ResourceName - " + resource.getResourceName() + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Update resource details
     *
     * @param ResourceId
     * @param resource
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateResource(int ResourceId, UpdateResourceRequest resource) throws Exception {
        ApiResponse response = new ApiResponse();

        try {
            LOGGER.info("Update Resource request. ResourceName - " + resource.getResourceName() + ", apiKey - " + resource.getApiKey());
            if (_validator.validateResource(resource)) {

                LOGGER.info("Check Resource exists. ResourceID - " + ResourceId);
                CoreResources resources = dbService.getResourceById(ResourceId);

                if (resources == null) {
                    LOGGER.error("Resource does not exist");
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_NOT_EXIST);
                    return response;
                }

                LOGGER.info("Update Resource entity. ResourceID - " + ResourceId);
                if (_helper.isNullOrEmpty(resource.getApiKey())) {
                    resource.setApiKey(resources.getCoreApi().getApiKey());
                }

                if (_helper.isNullOrEmpty(resource.getResourceName())) {
                    resource.setResourceName(resources.getResource());
                }

                //Get api data
                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(resource.getApiKey());

                List<CoreApi> apiList = dbService.getApiByCriteria(criteria);

                CoreApi api = null;

                if (apiList != null && apiList.size() > 0) {
                    api = apiList.get(0);
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }

                CoreResources coreResources = _coreMappers.updateResourceEntity(resource, api, ResourceId);
                coreResources.setCoreClients(resources.getCoreClients());

                ApiResponse dbResponse = dbService.updateResource(coreResources);

                if (dbResponse.status) {
                    response.status = true;
                    response.responseData = _coreMappers.createResourceResponse(coreResources);
                } else {
                    response.responseData = dbResponse.responseData;
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Update Resource. ResourceName - " + resource.getResourceName() + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get resources by criteria
     *
     * @param request
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getResource(GetResourceRequest request) throws Exception {
        ApiResponse response = new ApiResponse();

        try {
            LOGGER.info("Get Resource. ResourceName - " + request.getResourceName() + ", ApiKey - " + request.getApiKey());

            //Get api data
            CoreApi api = null;
            if (!_helper.isNullOrEmpty(request.getApiKey())) {
                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(request.getApiKey());

                List<CoreApi> apiList = dbService.getApiByCriteria(criteria);
                if (apiList != null && apiList.size() > 0) {
                    api = apiList.get(0);
                }

                if (api == null) {
                    LOGGER.info("Resource does not exist");
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_NOT_EXIST);
                    return response;
                }
            }

            ResourceCriteria criteria = new ResourceCriteria();
            criteria.setResourceName(request.getResourceName());
            criteria.setApi(api);

            List<CoreResources> coreResourceList = dbService.getResourceByCriteria(criteria);

            if (coreResourceList == null) {
                LOGGER.info("Resource does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createResourceResponse(coreResourceList);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Get Resource. ResourceName - " + request.getResourceName() + ", ApiKey - " + request.getApiKey() + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get resource by id
     *
     * @param resourceId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getResourceById(int resourceId) throws Exception {
        ApiResponse response = new ApiResponse();

        try {
            LOGGER.info("Check resource exists. ResourceId - " + resourceId);

            CoreResources resources = dbService.getResourceById(resourceId);

            if (resources == null) {
                LOGGER.error("Resource does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createResourceResponse(resources);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Get Resource. ResourceId - " + resourceId + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Delete resource
     *
     * @param resourceId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse deleteResource(int resourceId) throws Exception {
        ApiResponse response = new ApiResponse();

        try {
            LOGGER.info("Check Resource exists. ResourceId - " + resourceId);

            CoreResources resources = dbService.getResourceById(resourceId);

            if (resources == null) {
                LOGGER.error("Resource does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.RESOURCE_NOT_EXIST);
                return response;
            }

            ApiResponse dbResponse = dbService.deleteResource(resources);

            if (dbResponse.status) {
                response.status = true;
                LOGGER.info("Delete Resource: " + resourceId);
                response.responseData = _coreMappers.deleteResourceResponse(resources);
            } else {
                LOGGER.error("Delete Resource: " + resourceId);
                //LOGGER.info("Create Consumer response: "+ consumer.getKey()+" - " + dbResponse.ResponseData);
                response.responseData = dbResponse.responseData;
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Delete Resource. ResourceId - " + resourceId + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Add Lens settings
     *
     * @param settings
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse addLensSettings(AddLensSettingsRequest settings) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Validate Lens settings. InstanceType - " + settings.getInstanceType());
            if (_validator.validateLensSettings(settings)) {

                // Validate failover instance type
                if (_helper.isNotNull(settings.getFailoverLensSettings()) && _helper.isNotNull(settings.getFailoverLensSettings().getFailoverLensSettingsIdList())
                        && settings.getFailoverLensSettings().getFailoverLensSettingsIdList().size() > 0) {
                    for (BigInteger settingsId : settings.getFailoverLensSettings().getFailoverLensSettingsIdList()) {
                        CoreLenssettings lensSettings = dbService.getLensSettingsById(settingsId.intValue());
                        if (_helper.isNull(lensSettings)) {
                            LOGGER.error("Failover lens settings not found " + settingsId.intValue());
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.FAILOVER_INSTANCE_NOT_FOUND);
                            return response;
                        }
                        if (!lensSettings.getInstanceType().equalsIgnoreCase(settings.getInstanceType())) {
                            LOGGER.error("Type of failover instance " + settingsId.intValue() + " (" + lensSettings.getInstanceType() + ") mismatches with the actual instance to be updated");
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_TYPE_MISMATCH_ERROR);
                            return response;
                        }
                    }
                }

                CoreLenssettings coreLensSettings = _coreMappers.createLensSettingsEntity(settings, apiConfig);

                // check setting exists already
                // TODO : check for the move to controller
                LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
                lensSettingsCriteria.setHost(settings.getHost());
                lensSettingsCriteria.setPort(settings.getPort().intValue());
                lensSettingsCriteria.setCharacterSet(settings.getCharSet());
                lensSettingsCriteria.setLocale(settings.getLocale());
                lensSettingsCriteria.setLanguage(settings.getLanguage());
                //addded by sic
                lensSettingsCriteria.setDocServerResume(settings.getDocServerResume());
                lensSettingsCriteria.setDocServerPostings(settings.getDocServerPostings());
                lensSettingsCriteria.setHypercubeServerResume(settings.getHypercubeServerResume());
                lensSettingsCriteria.setHypercubeServerPostings(settings.getHypercubeServerPostings());

                lensSettingsCriteria.setCountry(settings.getCountry());
                lensSettingsCriteria.setHrxmlVersion(settings.getHRXMLVersion());
                lensSettingsCriteria.setHrxmlFileName(settings.getHRXMLFileName());
                lensSettingsCriteria.setLensVersion(settings.getVersion());
                lensSettingsCriteria.setInstanceType(settings.getInstanceType());

                List<CoreLenssettings> coreLenssettingses = dbService.getLensSettingsByCriteria(lensSettingsCriteria);

                // Check failover settings id - PIG
                //dbService.getFailoverLensSettingsById(settings.getFailoverLensSettings().)
                if (coreLenssettingses != null && coreLenssettingses.size() > 0) {
                    response.status = false;
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.SETTINGS_ALREADY_EXISTS_ERROR);
                } else {
                    ApiResponse dbResponse = dbService.createLensSettings(coreLensSettings);
                    if (dbResponse.status) {
                        response.status = true;
                        // Added ordering of failover ids
                        List<Integer> integers = new ArrayList<>();
                        if (_helper.isNotNull(settings.getFailoverLensSettings())
                                && _helper.isNotNull(settings.getFailoverLensSettings().getFailoverLensSettingsIdList())) {
                            for (BigInteger bigInteger : settings.getFailoverLensSettings().getFailoverLensSettingsIdList()) {
                                integers.add(bigInteger.intValue());
                            }
                            LensSettings lensSettings = _coreMappers.createLensSettingsResponse(coreLensSettings);
                            lensSettings.setFailoverLensSettings(integers);
                            response.responseData = lensSettings;
                            // 
                        } else {
                            response.responseData = _coreMappers.createLensSettingsResponse(coreLensSettings);
                        }
                    } else {
                        response.responseData = dbResponse.responseData;
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add Lens settings. InstanceType - " + settings.getInstanceType() + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Update Lens settings
     *
     * @param settingsId
     * @param settings
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateLensSettings(int settingsId, UpdateLensSettingsRequest settings) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Check Lens settings exists. SettingsId - " + settingsId);
            CoreLenssettings coreLenssettings = dbService.getLensSettingsById(settingsId);
           
            if (!_helper.isValidInstanceType(settings.getInstanceType())) {
                LOGGER.error("LENS instance not found");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_NOT_FOUND);
                return response;
            }
            if (coreLenssettings == null) {
                LOGGER.error("Lens settings does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.LENS_SETTINGS_NOT_EXIST);
                return response;
            }
            
            Set<CoreCustomskillsettings> olderCoreCustomskillsettings = coreLenssettings.getCoreCustomskillsettings();

            Set<CoreCustomskillsettings> deleteCoreCustomskillsettings = new HashSet<>();

            Set<CoreClientlenssettings> coreClientlenssettingses = coreLenssettings.getCoreClientlenssettingses();

            // Validate failover instance type
            if (_helper.isNotNull(settings.getFailoverLensSettings()) && _helper.isNotNull(settings.getFailoverLensSettings().getFailoverLensSettingsIdList())
                    && settings.getFailoverLensSettings().getFailoverLensSettingsIdList().size() > 0) {
                for (BigInteger failoverId : settings.getFailoverLensSettings().getFailoverLensSettingsIdList()) {
                    CoreLenssettings lensSettings = dbService.getLensSettingsById(failoverId.intValue());
                    if (!lensSettings.getInstanceType().equalsIgnoreCase(settings.getInstanceType())) {
                        LOGGER.error("Type of failover instance " + failoverId.intValue() + " (" + lensSettings.getInstanceType() + ") mismatches with the actual instance to be updated");
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.INSTANCE_TYPE_MISMATCH_ERROR);
                        // response.responseData = "Type of failover instance " + failoverId.intValue() + " (" + lensSettings.getInstanceType() + ") mismatches with the actual instance to be updated";
                        return response;
                    }
                }
            }

            LOGGER.info("Create Lens settings entity. SettingsId - " + settingsId);
            CoreLenssettings updatedLenssettings = _coreMappers.updateLensSettingsEntity(coreLenssettings.getId(), settings, apiConfig);

            updatedLenssettings.setId(coreLenssettings.getId());
            updatedLenssettings.setCharacterSet(_helper.isNotNull(updatedLenssettings.getCharacterSet()) && _helper.isNotEmpty(updatedLenssettings.getCharacterSet()) ? updatedLenssettings.getCharacterSet() : coreLenssettings.getCharacterSet());

            if (!_helper.isValidEncoding(updatedLenssettings.getCharacterSet())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_ENCODING_FORMAT), HttpStatus.BAD_REQUEST);
            }

            updatedLenssettings.setCreatedOn(coreLenssettings.getCreatedOn());
            updatedLenssettings.setHost(_helper.isNotNull(updatedLenssettings.getHost()) && _helper.isNotEmpty(updatedLenssettings.getHost()) ? updatedLenssettings.getHost() : coreLenssettings.getHost());
            //updatedLenssettings.setInstanceType(null);

            if (_helper.isNotNull(updatedLenssettings.getInstanceType()) || _helper.isNotEmpty(updatedLenssettings.getInstanceType())) {
                String instanceType = updatedLenssettings.getInstanceType();
                if (!_helper.isValidInstanceType(instanceType)) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_TYPE), HttpStatus.BAD_REQUEST);
                } else {
                    updatedLenssettings.setInstanceType(instanceType);
                }
            } else {
                updatedLenssettings.setInstanceType(coreLenssettings.getInstanceType());
            }

            updatedLenssettings.setLensVersion(_helper.isNotNull(updatedLenssettings.getLensVersion()) && _helper.isNotEmpty(updatedLenssettings.getLensVersion()) ? updatedLenssettings.getLensVersion() : coreLenssettings.getLensVersion());

            updatedLenssettings.setLanguage(_helper.isNotNullOrEmpty(updatedLenssettings.getLanguage()) ? updatedLenssettings.getLanguage() : coreLenssettings.getLanguage());
            updatedLenssettings.setCountry(_helper.isNotNullOrEmpty(updatedLenssettings.getCountry()) ? updatedLenssettings.getCountry() : coreLenssettings.getCountry());
            updatedLenssettings.setHrxmlcontent(_helper.isNotNullOrEmpty(updatedLenssettings.getHrxmlcontent()) ? updatedLenssettings.getHrxmlcontent() : coreLenssettings.getHrxmlcontent());
            // Added HRXML version 
            updatedLenssettings.setHrxmlversion(_helper.isNotNullOrEmpty(updatedLenssettings.getHrxmlversion()) ? updatedLenssettings.getHrxmlversion() : coreLenssettings.getHrxmlversion());
            updatedLenssettings.setHrxmlfilename(_helper.isNotNullOrEmpty(updatedLenssettings.getHrxmlfilename()) ? updatedLenssettings.getHrxmlfilename() : coreLenssettings.getHrxmlfilename());
            updatedLenssettings.setLocale(_helper.isNotNull(updatedLenssettings.getLocale()) && _helper.isNotEmpty(updatedLenssettings.getLocale()) ? updatedLenssettings.getLocale() : coreLenssettings.getLocale());
            updatedLenssettings.setPort(_helper.isNotNull(updatedLenssettings.getPort()) && updatedLenssettings.getPort() > 0 ? updatedLenssettings.getPort() : coreLenssettings.getPort());
            updatedLenssettings.setTimeout(_helper.isNotNull(updatedLenssettings.getTimeout()) && updatedLenssettings.getTimeout() > 0 ? updatedLenssettings.getTimeout() : coreLenssettings.getTimeout());
            //updatedLenssettings.setCoreClients(coreLenssettings.getCoreClients());
//added by sic

            updatedLenssettings.setDocServerResume(_helper.isNotNullOrEmpty(updatedLenssettings.getDocServerResume()) ? updatedLenssettings.getDocServerResume() : coreLenssettings.getDocServerResume());
            updatedLenssettings.setDocServerPostings(_helper.isNotNullOrEmpty(updatedLenssettings.getDocServerPostings()) ? updatedLenssettings.getDocServerPostings() : coreLenssettings.getDocServerPostings());

            updatedLenssettings.setHypercubeServerResume(_helper.isNotNullOrEmpty(updatedLenssettings.getHypercubeServerResume()) ? updatedLenssettings.getHypercubeServerResume() : coreLenssettings.getHypercubeServerResume());
            updatedLenssettings.setHypercubeServerPostings(_helper.isNotNullOrEmpty(updatedLenssettings.getHypercubeServerPostings()) ? updatedLenssettings.getHypercubeServerPostings() : coreLenssettings.getHypercubeServerPostings());

            if (_helper.isNotNull(updatedLenssettings.getCoreLenssettingsesForFailoverInstanceId()) && updatedLenssettings.getCoreLenssettingsesForFailoverInstanceId().size() > 0) {
                updatedLenssettings.setCoreLenssettingsesForFailoverInstanceId(updatedLenssettings.getCoreLenssettingsesForFailoverInstanceId());
                updatedLenssettings.setCoreLenssettingsesForLensSettingsId(coreLenssettings.getCoreLenssettingsesForLensSettingsId());
            } else {
                updatedLenssettings.setCoreLenssettingsesForFailoverInstanceId(coreLenssettings.getCoreLenssettingsesForFailoverInstanceId());
                updatedLenssettings.setCoreLenssettingsesForLensSettingsId(coreLenssettings.getCoreLenssettingsesForLensSettingsId());
            }

            FilterSettings filterSettings = null;

            if (_helper.isNotNullOrEmpty(updatedLenssettings.getCustomFilterSettings())) {
                filterSettings = (FilterSettings) _helper.getObjectFromJson(updatedLenssettings.getCustomFilterSettings(), new FilterSettings());
                if (_helper.isNotNull(filterSettings)) {
                    if (_helper.isNotNull(filterSettings.getCustomFilters()) && (_helper.isNotNull(filterSettings.getCustomFilters().getResumeFilters()))) {
                        if (filterSettings.getCustomFilters().getResumeFilters().size() <= 0) {
                            filterSettings.getCustomFilters().setResumeFilters(null);
                        }
                    }
                    if (_helper.isNotNull(filterSettings.getCustomFilters()) && (_helper.isNotNull(filterSettings.getCustomFilters().getPostingFilters()))) {
                        if (filterSettings.getCustomFilters().getPostingFilters().size() <= 0) {
                            filterSettings.getCustomFilters().setPostingFilters(null);
                        }
                    }

                    if (_helper.isNotNull(filterSettings.getCustomFilters()) && _helper.isNull(filterSettings.getCustomFilters().getResumeFilters()) && _helper.isNull(filterSettings.getCustomFilters().getPostingFilters())) {
                        filterSettings.setCustomFilters(null);
                    }

                    if (_helper.isNotNull(filterSettings.getFacetFilters()) && (_helper.isNotNull(filterSettings.getFacetFilters().getResumeFilters()))) {
                        if (filterSettings.getFacetFilters().getResumeFilters().size() <= 0) {
                            filterSettings.getFacetFilters().setResumeFilters(null);
                        }
                    }
                    if (_helper.isNotNull(filterSettings.getFacetFilters()) && (_helper.isNotNull(filterSettings.getFacetFilters().getPostingFilters()))) {
                        if (filterSettings.getFacetFilters().getPostingFilters().size() <= 0) {
                            filterSettings.getFacetFilters().setPostingFilters(null);
                        }
                    }

                    if (_helper.isNotNull(filterSettings.getFacetFilters()) && _helper.isNull(filterSettings.getFacetFilters().getResumeFilters()) && _helper.isNull(filterSettings.getFacetFilters().getPostingFilters())) {
                        filterSettings.setFacetFilters(null);
                    }

                    if (_helper.isNull(filterSettings.getCustomFilters()) && _helper.isNull(filterSettings.getFacetFilters())) {
                        filterSettings = null;
                    }
                }
            }

            if (_helper.isNotNull(olderCoreCustomskillsettings) && olderCoreCustomskillsettings.size() > 0
                    && _helper.isNotNull(updatedLenssettings.getCoreCustomskillsettings())) {

                //updates the existing Custom skill settings Id
                CopyOnWriteArrayList<CoreCustomskillsettings> coreCustomskillsettings = new CopyOnWriteArrayList<>(updatedLenssettings.getCoreCustomskillsettings());
                for (CoreCustomskillsettings olderCoreCustomskillsetting : olderCoreCustomskillsettings) {
                    boolean deleteOlderSkill = true;
                    int index = -1;
                    for (int i = 0; i < coreCustomskillsettings.size(); i++) {
                        if (olderCoreCustomskillsetting.getCustomSkillCode().equalsIgnoreCase(coreCustomskillsettings.get(i).getCustomSkillCode())) {

                            index = i;
                            deleteOlderSkill = false;
                            break;
                        }
                    }
                    if (deleteOlderSkill) {
                       
                        CoreCustomskillsettings coreCustomskillsetting = new CoreCustomskillsettings();
                        coreCustomskillsetting.setId(olderCoreCustomskillsetting.getId());
                        deleteCoreCustomskillsettings.add(coreCustomskillsetting);

                    }
                    if (index > -1) {
                        coreCustomskillsettings.get(index).setId(olderCoreCustomskillsetting.getId());
                    }
                }
                updatedLenssettings.setCoreCustomskillsettings(new HashSet<>(coreCustomskillsettings));
            }

            //updatedLenssettings.setCustomFilterSettings(_helper.isNotNull(filterSettings) ? _helper.getJsonStringFromObject(filterSettings) : coreLenssettings.getCustomFilterSettings());
            ApiResponse dbResponse = dbService.updateLensSettings(updatedLenssettings, deleteCoreCustomskillsettings);

            if (dbResponse.status) {
                response.status = true;
                LensSettings lensSettings = _coreMappers.createLensSettingsResponse(updatedLenssettings);
                if (_helper.isNotNull(settings.getFailoverLensSettings())
                        && _helper.isNotNull(settings.getFailoverLensSettings().getFailoverLensSettingsIdList())) {
                    // Added ordering of failover ids
                    List<Integer> integers = new ArrayList<>();
                    for (BigInteger bigInteger : settings.getFailoverLensSettings().getFailoverLensSettingsIdList()) {
                        integers.add(bigInteger.intValue());
                    }
                    lensSettings.setFailoverLensSettings(integers);
                }
                // 
                response.responseData = lensSettings;
            } else {
                response.responseData = dbResponse.responseData;
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Update Lens settings. SettingsId - " + settingsId + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get Lens settings by criteria
     *
     * @param settings
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getLensSettings(GetLensSettingsRequest settings) throws Exception {
        ApiResponse response = new ApiResponse();
        try {

            LensSettingsCriteria settingsCriteria = _coreMappers.createLensSettingsCriteria(settings);

            List<CoreLenssettings> coreLensSettingsList = dbService.getLensSettingsByCriteria(settingsCriteria);

            if (coreLensSettingsList == null) {
                LOGGER.info("Lens settings does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.LENS_SETTINGS_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createLensSettingsResponse(coreLensSettingsList);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get Lens settings. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get Lens settings by Id
     *
     * @param settingsId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getLensSettingsById(int settingsId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            CoreLenssettings coreLenssettings = dbService.getLensSettingsById(settingsId);

            if (coreLenssettings == null) {
                LOGGER.info("Lens settings does not exist: " + settingsId);
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.LENS_SETTINGS_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createLensSettingsResponse(coreLenssettings);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get Lens settings. SettingsId - " + settingsId + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Delete Lens settings
     *
     * @param instanceId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse deleteLensSettings(int instanceId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Check Lens settings exists. SettingsId - " + instanceId);
            CoreLenssettings coreLensSettings = dbService.getLensSettingsById(instanceId);

            if (coreLensSettings == null) {
                LOGGER.error("Lens settings does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.LENS_SETTINGS_NOT_EXIST);
                return response;
            }

            CoreLenssettings coreLenssettings = dbService.getLensSettingsById(instanceId);

            if (coreLenssettings.getCoreClientlenssettingses().isEmpty()) {
                //Send delete request to db service
                ApiResponse dbResponse = dbService.deleteLensSettings(coreLensSettings);

                if (dbResponse.status) {
                    response.status = true;
                    response.responseData = _coreMappers.deleteLensSettingsResponse(instanceId);
                } else {
                    response.responseData = dbResponse.responseData;
                }
            } else {
                LOGGER.error("Lens setting is assigned to user, it cannot be deleted");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.LENS_SETTINGS_USED_BY_CLIENT);
                return response;
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Delete Lens settings. settingsId - " + instanceId + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Create new client
     *
     * @param consumer
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse createConsumer(CreateConsumerRequest consumer) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Validate consumer details. Key - " + consumer.getKey());
            if (consumer.getClientApiList() != null && consumer.getClientApiList().getClientApi() != null) {

                // Update ApiId
                for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
                    ClientApi clientApi = consumer.getClientApiList().getClientApi().get(i);
                    if (!_helper.isNullOrEmpty(clientApi.getApiKey())) {
                        ApiCriteria criteria = new ApiCriteria();
                        criteria.setApiKey(clientApi.getApiKey());

                        List<CoreApi> apiList = dbService.getApiByCriteria(criteria);

                        if (apiList != null && apiList.size() > 0) {
                            consumer.getClientApiList().getClientApi().get(i).setApiId(new BigInteger(Integer.toString(apiList.get(0).getId())));
                        } else {
                            LOGGER.error("Api key is invalid");
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                        }
                    } else {
                        LOGGER.error("Api key is invalid");
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY), HttpStatus.BAD_REQUEST);
                    }
                }
            }

            if (_validator.validateConsumer(consumer, apiConfig)) {

                LOGGER.info("Create Consumer Entity. Key - " + consumer.getKey());
                CoreClient client = _coreMappers.createConsumerAsEntity(consumer, apiConfig);
                ApiResponse dbResponse = dbService.createClient(client);

                if (dbResponse.status) {
                    response.status = true;
                    response.responseData = _coreMappers.createClientResponse(client);
                } else {
                    response.responseData = dbResponse.responseData;
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Create Consumer response. Key - " + consumer.getKey() + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Update existing consumer
     *
     * @param clientId
     * @param consumer
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateConsumer(int clientId, UpdateConsumerRequest consumer) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            if (_helper.isNotNull(consumer.getTier()) && consumer.getTier().intValue() != internal && consumer.getTier().intValue() != external) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TIER), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNull(consumer)) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
            }
            if (_helper.isNull(consumer.getAuthenticationType())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_AUTHENTICATION_TYPE), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNotNullOrEmpty(consumer.getKey()) && _helper.isNotNullOrEmpty(consumer.getSecret())) {

                if (!(_helper.isAlphaNumeric(consumer.getKey()))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
                }
                if (!(_helper.isValidSecret(consumer.getSecret()))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CONSUMER_SECRET), HttpStatus.BAD_REQUEST);
                }

                if (!_helper.isAlphaNumericWithSomeSpecialCharacter(consumer.getName())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CONSUMER_NAME), HttpStatus.BAD_REQUEST);
                }
            }

            if (_helper.isNotNull(consumer.getKey()) && _helper.isEmpty(consumer.getKey())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNotNull(consumer.getName()) && _helper.isEmpty(consumer.getName())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_NAME), HttpStatus.BAD_REQUEST);
            }

            if (_helper.isNotNull(consumer.getSecret()) && _helper.isEmpty(consumer.getSecret())) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_SECRET), HttpStatus.BAD_REQUEST);
            }

            if ((_helper.isNotNull(consumer.getLensSettings()) && _helper.isNotNull(consumer.getLensSettings().getInstanceList()) && consumer.getLensSettings().getInstanceList().size() > 0)) {
                for (InstanceList instance : consumer.getLensSettings().getInstanceList()) {
                    if (!_helper.isNotNull(instance.getInstanceId())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_SETTINGS), HttpStatus.BAD_REQUEST);
                    }
                    if (consumer.isRateLimitEnabled()) {
                        if (!_helper.isNotNull(consumer.getRateLimitTimePeriod())) {
                            // Throw error
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_USER_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                        } else if (!_helper.isValidInteger(consumer.getRateLimitTimePeriod().longValue())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_USER_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                        }

                        if (!_helper.isNotNull(consumer.getRateLimitCount())) {
                            // Throw error
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_USER_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);
                        } else if (!_helper.isValidInteger(consumer.getRateLimitCount().longValue())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_USER_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);

                        }

                    }
                    if (instance.isRateLimitEnabled()) {
                        if (!_helper.isNotNull(instance.getRateLimitTimePeriod())) {
                            // Throw error
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                        } else if (!_helper.isValidInteger(instance.getRateLimitTimePeriod().longValue())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                        }

                        if (!_helper.isNotNull(instance.getRateLimitCount())) {
                            // Throw error
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);
                        } else if (!_helper.isValidInteger(instance.getRateLimitCount().longValue())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);

                        }
                    }
                }
            }

            // Check if client exists
            LOGGER.info("Check Client exists. ClientId -  " + clientId);
            CoreClient client = dbService.getClientById(clientId);
            CoreClient updatedClient = new CoreClient();
            List<CoreClientapi> oldApiDetails = new ArrayList<>();
            if (client == null) {
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.CLIENT_NOT_EXIST);
                return response;
            } else {

                for (CoreClientapi oldApiList : client.getCoreClientapis()) {
                    if (oldApiList.getId().getClientId() == clientId) {
                        oldApiDetails.add(oldApiList);
                    }
                }

            }

            LOGGER.info("Update consumer entity. ClientId - " + clientId);
            if (consumer.getClientApiList() != null && consumer.getClientApiList().getClientApi() != null) {

                //Update ApiId
                for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
                    ClientApi clientApi = consumer.getClientApiList().getClientApi().get(i);
                    if (!_helper.isNullOrEmpty(clientApi.getApiKey())) {
                        ApiCriteria criteria = new ApiCriteria();
                        criteria.setApiKey(clientApi.getApiKey());

                        List<CoreApi> apiList = dbService.getApiByCriteria(criteria);

                        if (apiList != null && apiList.size() > 0) {
                            /*if (_helper.isNotNull(clientApi.getApiId())) {
                             if (clientApi.getApiId().intValue() != apiList.get(0).getId()) {
                             throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID, HttpStatus.BAD_REQUEST);
                             }
                             }*/

                            consumer.getClientApiList().getClientApi().get(i).setApiId(new BigInteger(Integer.toString(apiList.get(0).getId())));
                        } else {
                            LOGGER.info("Api key is invalid");
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                        }
                    } else {
                        LOGGER.info("Api key is invalid");
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY), HttpStatus.BAD_REQUEST);
                    }
                }
                if (_validator.validateUpdateClientApi(consumer.getClientApiList().getClientApi(), apiConfig, oldApiDetails)) {
                    //Map request to entity
                    updatedClient = _coreMappers.updateConsumerAsEntity(consumer);
                }
            } else {
                //Map request to entity
                updatedClient = _coreMappers.updateConsumerAsEntity(consumer);
                // updatedClient.setCoreClientapis(client.getCoreClientapis());
            }

            updatedClient.setAuthenticationType(consumer.getAuthenticationType());

            updatedClient.setTier(_helper.isNull(updatedClient.getTier()) ? client.getTier() : updatedClient.getTier());

            //Update entity
            LOGGER.info("Update entity details(new and existing API's). ClientId - " + clientId);
            updatedClient.setId(clientId);
            updatedClient.setClientKey(_helper.isNullOrEmpty(updatedClient.getClientKey()) ? client.getClientKey() : updatedClient.getClientKey());
            updatedClient.setName(_helper.isNullOrEmpty(updatedClient.getName()) ? client.getName() : updatedClient.getName());
            updatedClient.setSecret(_helper.isNullOrEmpty(updatedClient.getSecret()) ? client.getSecret() : updatedClient.getSecret());
            updatedClient.setCreatedOn(client.getCreatedOn());
            updatedClient.setUpdatedOn(Date.from(Instant.now()));
            updatedClient.setEnableLocaleDetection(_helper.isNull(consumer.isEnableLocaleDetection()) ? updatedClient.isEnableLocaleDetection() : consumer.isEnableLocaleDetection());

            if (_helper.isNotNull(consumer.getDefaultLocaleList())) {
                for (com.bgt.lens.model.adminservice.request.Locale locale : consumer.getDefaultLocaleList().getLocale()) {
                    if (_helper.isNullOrEmpty(locale.getLanguage())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LANGUAGE), HttpStatus.BAD_REQUEST);
                    } else if (_helper.isNullOrEmpty(locale.getCountry())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_COUNTRY), HttpStatus.BAD_REQUEST);
                    }
                }
            }

            String defaultLocaleList = _helper.isNotNull(consumer.getDefaultLocaleList()) ? _helper.getJsonStringFromObject(consumer.getDefaultLocaleList()) : client.getDefaultLocale();
            updatedClient.setDefaultLocale(defaultLocaleList);

            if (updatedClient.getCoreResourceses() != null && updatedClient.getCoreResourceses().size() > 0) {
                updatedClient.setCoreResourceses(updatedClient.getCoreResourceses());
            } else {
                updatedClient.setCoreResourceses(client.getCoreResourceses());
            }

            if (updatedClient.getCoreClientlenssettingses() != null && updatedClient.getCoreClientlenssettingses().size() > 0) {
                updatedClient.setCoreClientlenssettingses(updatedClient.getCoreClientlenssettingses());
            } else {
                updatedClient.setCoreClientlenssettingses(client.getCoreClientlenssettingses());
            }

            if (updatedClient.getSearchVendorsettingses() != null && updatedClient.getSearchVendorsettingses().size() > 0) {
                updatedClient.setSearchVendorsettingses(updatedClient.getSearchVendorsettingses());
            }
//            else {
//                updatedClient.setSearchVendorsettingses(client.getSearchVendorsettingses());
//            }
            String resumeVendorId = "";
            String postingVendorId = "";
            // Added validation for max search count - PIG
            if (_helper.isNotNull(consumer.getSearchSettings())) {
                if (_helper.isNotNull(consumer.getSearchSettings().getMaxSearchCount()) && (!_helper.isValidInteger(consumer.getSearchSettings().getMaxSearchCount()))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_MAX_SEARCHCOUNT), HttpStatus.BAD_REQUEST);
                }
                if (_helper.isNotNull(consumer.getSearchSettings().getMaxSearchCount()) && ((consumer.getSearchSettings().getMaxSearchCount().intValue()) <= 0)) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_MAX_SEARCHCOUNT), HttpStatus.BAD_REQUEST);
                }
                if (_helper.isNotNull(consumer.getSearchSettings().getMaxSearchCount()) && consumer.getSearchSettings().getMaxSearchCount() < 0 && consumer.getSearchSettings().getMaxSearchCount() > Integer.MAX_VALUE) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_MAX_SEARCHCOUNT), HttpStatus.BAD_REQUEST);

                }

                if (_helper.isNotNull(consumer.getSearchSettings().getResume()) && consumer.getSearchSettings().getResume().size() > 0) {
                    for (SearchSettings.Resume resume : consumer.getSearchSettings().getResume()) {
                        if (_helper.isNull(resume.getInstanceId())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                        }
                        resumeVendorId = resume.getVendorId().toString();
                        int vendoId = resume.getVendorId().intValue();
                        if (dbService.getVendorById(vendoId) == null) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                        }

                        if (_helper.isNull(resume.getVendorId())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_OR_INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                        }
                        if (!_helper.isNotNull(consumer.getLensSettings()) && !_helper.isNotNull(consumer.getLensSettings().getInstanceList())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                        }

                        if (_helper.isNotNull(consumer.getLensSettings().getInstanceList()) && consumer.getLensSettings().getInstanceList().size() > 0) {
                            boolean isAvailable = false;
                            for (InstanceList instance : consumer.getLensSettings().getInstanceList()) {
                                if (instance.getInstanceId().equals(resume.getInstanceId())) {
                                    isAvailable = true;
                                }
                            }
                            if (!isAvailable) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                            }
                        }

                        if (_helper.isNotNull(consumer.getVendorSettings()) && _helper.isNotNull(consumer.getVendorSettings().getVendorIdList()) && (!consumer.getVendorSettings().getVendorIdList().contains(resume.getVendorId()))) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                        }
                    }
                }
                if (_helper.isNotNull(consumer.getSearchSettings().getPosting()) && consumer.getSearchSettings().getPosting().size() > 0) {
                    for (SearchSettings.Posting posting : consumer.getSearchSettings().getPosting()) {
                        if (_helper.isNull(posting.getInstanceId())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                        }
                        postingVendorId = posting.getVendorId().toString();

                        if (_helper.isNull(posting.getVendorId())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_OR_INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                        }
                        if (_helper.isNotNull(consumer.getLensSettings().getInstanceList())) {

                            boolean isAvailable = false;
                            for (InstanceList instance : consumer.getLensSettings().getInstanceList()) {
                                if (instance.getInstanceId().equals(posting.getInstanceId())) {
                                    isAvailable = true;
                                }
                            }
                            if (!isAvailable) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                            }
                        }
                        if (!consumer.getVendorSettings().getVendorIdList().contains(posting.getVendorId())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                        }
                    }
                }

                if (!resumeVendorId.equals("") && !postingVendorId.equals("")) {
                    if (resumeVendorId.equals(postingVendorId)) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.SAME_VENDOR_ID_ERROR), HttpStatus.BAD_REQUEST);

                    }
                }
            }

            // Set default max search count
            if (_helper.isNotNull(consumer.getSearchSettings()) && _helper.isNull(consumer.getSearchSettings().getMaxSearchCount())) {
                if (_helper.isNotNullOrEmpty(client.getSearchSettings())) {
                    SearchSettings searchSettings = (SearchSettings) _helper.getObjectFromJson(client.getSearchSettings(), new SearchSettings());
                    if (_helper.isNotNull(searchSettings.getMaxSearchCount())) {
                        consumer.getSearchSettings().setMaxSearchCount(searchSettings.getMaxSearchCount());
                    } else {
                        consumer.getSearchSettings().setMaxSearchCount((long) apiConfig.getDefaultMaxSearchCount());
                    }
                } else {
                    consumer.getSearchSettings().setMaxSearchCount((long) apiConfig.getDefaultMaxSearchCount());
                }
            }

            String searchSettings = _helper.isNotNull(consumer.getSearchSettings()) ? _helper.getJsonStringFromObject(consumer.getSearchSettings()) : null;
            updatedClient.setSearchSettings(searchSettings);

            List<CoreClientapi> updatedApiList = new ArrayList<>();

            updatedApiList.addAll(updatedClient.getCoreClientapis());

            //Update already added API's
            for (int i = 0; i < updatedApiList.size(); i++) {
                if (updatedApiList.get(i).getCoreApi() != null && updatedApiList.get(i).getCoreApi().getId() != 0) {
                    int id = updatedApiList.get(i).getCoreApi().getId();

                    boolean addApi = false;
                    CoreClientapi oldApi = new CoreClientapi();
                    for (CoreClientapi oldApiList : client.getCoreClientapis()) {
                        if (oldApiList.getCoreApi().getId() == id) {
                            addApi = true;
                            oldApi = oldApiList;
                        }
                    }

                    //for (CoreClientapi oldApi : client.getCoreClientapis()) {
                    if (addApi) {
                        CoreClientapi api = updatedApiList.get(i);
                        //  Increase the allowed transaction count while updating user transaction count
                        if (api.getRemainingTransactions() != 0) {
                            if (_helper.validateTransactionCount((api.getRemainingTransactions() + oldApi.getAllowedTransactions()))) {
                                api.setAllowedTransactions((api.getRemainingTransactions() + oldApi.getAllowedTransactions()));
                            } else {
                                api.setAllowedTransactions(apiConfig.getMaxTransactionCount());
                            }
                        } else {
                            api.setAllowedTransactions(oldApi.getAllowedTransactions());
                        }

                        // Allow Remaining Count
                        if (api.getRemainingTransactions() != 0) {
                            if (_helper.validateTransactionCount((api.getRemainingTransactions() + oldApi.getRemainingTransactions()))) {

                                api.setRemainingTransactions((api.getRemainingTransactions() + oldApi.getRemainingTransactions()));

                            } else {
                                api.setRemainingTransactions(apiConfig.getMaxTransactionCount());
                            }
                        } else {
                            api.setRemainingTransactions(oldApi.getRemainingTransactions());
                        }

                        // ActivationDate,ExpiryDate,Secret,Enabled
                        api.setActivationDate(api.getActivationDate() != null ? api.getActivationDate() : oldApi.getActivationDate());
                        api.setExpiryDate(api.getExpiryDate() != null ? api.getExpiryDate() : oldApi.getExpiryDate());
                        api.setSecret(_helper.isNotNullOrEmpty(api.getSecret()) ? api.getSecret() : oldApi.getSecret());
                        api.setEnabled(api.isEnabled());

                        if (api.getActivationDate().after(api.getExpiryDate())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                        }

                        api.getCoreApi().setApiKey(oldApi.getCoreApi().getApiKey());

                        updatedApiList.set(i, api);
                    } else {
                        CoreClientapi api = updatedApiList.get(i);
                        api.setSecret(api.getSecret());

                        //If both activation date and allowed transaction not specified
                        if (_helper.isNull(api.getActivationDate()) && _helper.isNull(api.getExpiryDate()) && _helper.isNull(api.getAllowedTransactions())) {
                            LocalDate today = new LocalDate();
                            LocalDate today30 = today.plusDays(apiConfig.getDefaultExpiryDateInterval());

                            api.setActivationDate(today.toDateTimeAtStartOfDay().toDate());
                            api.setExpiryDate(today30.toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
                            api.setAllowedTransactions(apiConfig.getDefaultTransactionCount());
                        } else if (_helper.isNotNull(api.getActivationDate()) && _helper.isNotNull(api.getExpiryDate())) {
                            api.setActivationDate(api.getActivationDate());
                            api.setExpiryDate(api.getExpiryDate());
                        } else if (_helper.isNotNull(api.getActivationDate())) {
                            Calendar cal = new GregorianCalendar();
                            cal.setTime(api.getActivationDate());
                            cal.add(Calendar.DAY_OF_MONTH, apiConfig.getDefaultExpiryDateInterval());
                            Date today30 = cal.getTime();
                            api.setActivationDate(api.getActivationDate());
                            api.setExpiryDate(new LocalDate(today30).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
                        } else if (_helper.isNotNull(api.getExpiryDate())) {
                            LocalDate today = new LocalDate();
                            api.setActivationDate(today.toDateTimeAtStartOfDay().toDate());
                            api.setExpiryDate(new LocalDate(api.getExpiryDate()).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());

                        } else {
                            LocalDate today = new LocalDate();
                            LocalDate today30 = today.plusDays(apiConfig.getDefaultExpiryDateInterval());

                            api.setActivationDate(today.toDateTimeAtStartOfDay().toDate());
                            api.setExpiryDate(today30.toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
                        }

                        if (api.getActivationDate().after(api.getExpiryDate())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                        }

                        api.setAllowedTransactions(api.getAllowedTransactions() <= 0 ? apiConfig.getDefaultTransactionCount() : api.getAllowedTransactions());
                        api.setEnabled(api.isEnabled());
                        api.setRemainingTransactions(api.getAllowedTransactions());
                        CoreApi coreApi = new CoreApi();
                        coreApi.setId(api.getCoreApi().getId());
                        api.setCoreApi(coreApi);
                        updatedApiList.set(i, api);
                    }
                    //}
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID), HttpStatus.BAD_REQUEST);
                }
            }

            // Add new API's
            for (CoreClientapi api : client.getCoreClientapis()) {
                boolean contains = false;

                for (CoreClientapi updatedApi : updatedClient.getCoreClientapis()) {
                    if (Objects.equals(api.getCoreApi().getId(), updatedApi.getCoreApi().getId())) {
                        contains = true;
                    }
                }

                if (!contains) {
                    api.setEnabled(false);
                    updatedApiList.add(api);
                }
            }

            Set<CoreClientapi> coreClientApiSet = new HashSet<>();

            coreClientApiSet.addAll(updatedApiList);

            updatedClient.setCoreClientapis(coreClientApiSet);

            updatedClient.setRateLimitEnabled(consumer.isRateLimitEnabled());

            if (consumer.isRateLimitEnabled()) {
                if (_helper.isNotNull(consumer.getRateLimitTimePeriod())) {
                    updatedClient.setRateLimitTimePeriod(Integer.valueOf(consumer.getRateLimitTimePeriod().toString()));
                }
                if (_helper.isNotNull(consumer.getRateLimitCount())) {
                    updatedClient.setRateLimitCount(Integer.valueOf(consumer.getRateLimitCount().toString()));
                }
            } else {
                updatedClient.setRateLimitEnabled(Boolean.FALSE);
            }

            LOGGER.info("Update Consumer Entity. Key - " + consumer.getKey());
            // Send update request to db service
            ApiResponse dbResponse = dbService.updateClient(client, updatedClient);

            if (dbResponse.status) {
                response.status = true;
                response.responseData = _coreMappers.createClientResponse(updatedClient);
            } else {
                response.responseData = dbResponse.responseData;
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Update Consumer response. Key - " + consumer.getKey() + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Update existing consumer
     *
     * @param clientId
     * @param consumer
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse updateConsumer(int clientId, List<UpdateConsumerRequest> consumerList) throws Exception {
        ApiResponse response = new ApiResponse();
        HashMap<BigInteger, CoreClient> clientHashMap = new HashMap<BigInteger, CoreClient>();
        HashMap<BigInteger, CoreClient> updateClientHashMap = new HashMap<BigInteger, CoreClient>();
        Set<BigInteger> _id = new HashSet<>();
        CoreClient resellerClient = new CoreClient();
        String consumerKey = "";

        for (UpdateConsumerRequest consumer : consumerList) {
            try {
                if (_helper.isNotNull(consumer.getTier()) && consumer.getTier().intValue() != internal && consumer.getTier().intValue() != external) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_TIER), HttpStatus.BAD_REQUEST);
                }

                if (_helper.isNull(consumer)) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
                }
                if (_helper.isNull(consumer.getAuthenticationType())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_AUTHENTICATION_TYPE), HttpStatus.BAD_REQUEST);
                }

                if (_helper.isNotNullOrEmpty(consumer.getKey()) && _helper.isNotNullOrEmpty(consumer.getSecret())) {

                    if (!(_helper.isAlphaNumeric(consumer.getKey()))) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
                    }
                    if (!(_helper.isValidSecret(consumer.getSecret()))) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CONSUMER_SECRET), HttpStatus.BAD_REQUEST);
                    }

                    if (!_helper.isAlphaNumericWithSomeSpecialCharacter(consumer.getName())) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CONSUMER_NAME), HttpStatus.BAD_REQUEST);
                    }
                }

                if (_helper.isNotNull(consumer.getKey()) && _helper.isEmpty(consumer.getKey())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
                }

                if (_helper.isNotNull(consumer.getName()) && _helper.isEmpty(consumer.getName())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_NAME), HttpStatus.BAD_REQUEST);
                }

                if (_helper.isNotNull(consumer.getSecret()) && _helper.isEmpty(consumer.getSecret())) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_SECRET), HttpStatus.BAD_REQUEST);
                }

                if ((_helper.isNotNull(consumer.getLensSettings()) && _helper.isNotNull(consumer.getLensSettings().getInstanceList()) && consumer.getLensSettings().getInstanceList().size() > 0)) {
                    for (InstanceList instance : consumer.getLensSettings().getInstanceList()) {
                        if (!_helper.isNotNull(instance.getInstanceId())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_SETTINGS), HttpStatus.BAD_REQUEST);
                        }
                        if (instance.isRateLimitEnabled()) {
                            if (!_helper.isNotNull(instance.getRateLimitTimePeriod())) {
                                // Throw error
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                            } else if (!_helper.isValidInteger(instance.getRateLimitTimePeriod().longValue())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_PERIOD), HttpStatus.BAD_REQUEST);
                            }
                            if (!_helper.isNotNull(instance.getRateLimitCount())) {
                                // Throw error
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);
                            } else if (!_helper.isValidInteger(instance.getRateLimitTimePeriod().longValue())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LENS_RATE_LIMIT_COUNT), HttpStatus.BAD_REQUEST);

                            }
                        }
                    }
                }

                // Check if client exists
                LOGGER.info("Check Client exists. ClientId -  " + consumer.getClientId().intValue());
                CoreClient client = dbService.getClientById(consumer.getClientId().intValue());
                CoreClient updatedClient = new CoreClient();
                List<CoreClientapi> oldApiDetails = new ArrayList<>();
                if (client == null) {
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.CLIENT_NOT_EXIST);
                    return response;
                } else {

                    for (CoreClientapi oldApiList : client.getCoreClientapis()) {
                        if (oldApiList.getId().getClientId() == consumer.getClientId().intValue()) {
                            oldApiDetails.add(oldApiList);
                        }
                    }

                }

                LOGGER.info("Update consumer entity. ClientId - " + consumer.getClientId().intValue());
                if (consumer.getClientApiList() != null && consumer.getClientApiList().getClientApi() != null) {

                    //Update ApiId
                    for (int i = 0; i < consumer.getClientApiList().getClientApi().size(); i++) {
                        ClientApi clientApi = consumer.getClientApiList().getClientApi().get(i);
                        if (!_helper.isNullOrEmpty(clientApi.getApiKey())) {
                            ApiCriteria criteria = new ApiCriteria();
                            criteria.setApiKey(clientApi.getApiKey());

                            List<CoreApi> apiList = dbService.getApiByCriteria(criteria);

                            if (apiList != null && apiList.size() > 0) {
                                /*if (_helper.isNotNull(clientApi.getApiId())) {
                                 if (clientApi.getApiId().intValue() != apiList.get(0).getId()) {
                                 throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID, HttpStatus.BAD_REQUEST);
                                 }
                                 }*/

                                consumer.getClientApiList().getClientApi().get(i).setApiId(new BigInteger(Integer.toString(apiList.get(0).getId())));
                            } else {
                                LOGGER.info("Api key is invalid");
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                            }
                        } else {
                            LOGGER.info("Api key is invalid");
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_API_KEY), HttpStatus.BAD_REQUEST);
                        }
                    }
                    if(consumer.isResellerUserEnabled()== true){
                        if (_validator.validateUpdateClientApi(consumer.getClientApiList().getClientApi(), apiConfig, oldApiDetails)) {
                        //Map request to entity
                              updatedClient = _coreMappers.updateConsumerAsEntity(consumer);
                        }
                    }
                    else{
                        updatedClient = _coreMappers.updateConsumerAsEntity(consumer);
                    }
                } else {
                    //Map request to entity
                    updatedClient = _coreMappers.updateConsumerAsEntity(consumer);
                    // updatedClient.setCoreClientapis(client.getCoreClientapis());
                }

                updatedClient.setAuthenticationType(consumer.getAuthenticationType());

                updatedClient.setTier(_helper.isNull(updatedClient.getTier()) ? client.getTier() : updatedClient.getTier());

                //Update entity
                LOGGER.info("Update entity details(new and existing API's). ClientId - " + consumer.getClientId().intValue());
                updatedClient.setId(consumer.getClientId().intValue());
                updatedClient.setClientKey(_helper.isNullOrEmpty(updatedClient.getClientKey()) ? client.getClientKey() : updatedClient.getClientKey());
                updatedClient.setName(_helper.isNullOrEmpty(updatedClient.getName()) ? client.getName() : updatedClient.getName());
                updatedClient.setSecret(_helper.isNullOrEmpty(updatedClient.getSecret()) ? client.getSecret() : updatedClient.getSecret());
                updatedClient.setCreatedOn(client.getCreatedOn());
                updatedClient.setUpdatedOn(new Date());
                updatedClient.setEnableLocaleDetection(_helper.isNull(consumer.isEnableLocaleDetection()) ? updatedClient.isEnableLocaleDetection() : consumer.isEnableLocaleDetection());

                if (_helper.isNotNull(consumer.getDefaultLocaleList())) {
                    for (com.bgt.lens.model.adminservice.request.Locale locale : consumer.getDefaultLocaleList().getLocale()) {
                        if (_helper.isNullOrEmpty(locale.getLanguage())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_LANGUAGE), HttpStatus.BAD_REQUEST);
                        } else if (_helper.isNullOrEmpty(locale.getCountry())) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_COUNTRY), HttpStatus.BAD_REQUEST);
                        }
                    }
                }

                String defaultLocaleList = _helper.isNotNull(consumer.getDefaultLocaleList()) ? _helper.getJsonStringFromObject(consumer.getDefaultLocaleList()) : client.getDefaultLocale();
                updatedClient.setDefaultLocale(defaultLocaleList);

                if (updatedClient.getCoreResourceses() != null && updatedClient.getCoreResourceses().size() > 0) {
                    updatedClient.setCoreResourceses(updatedClient.getCoreResourceses());
                } else {
                    updatedClient.setCoreResourceses(client.getCoreResourceses());
                }

                if (updatedClient.getCoreClientlenssettingses() != null && updatedClient.getCoreClientlenssettingses().size() > 0) {
                    updatedClient.setCoreClientlenssettingses(updatedClient.getCoreClientlenssettingses());
                } else {
                    updatedClient.setCoreClientlenssettingses(client.getCoreClientlenssettingses());
                }

                if (updatedClient.getSearchVendorsettingses() != null && updatedClient.getSearchVendorsettingses().size() > 0) {
                    updatedClient.setSearchVendorsettingses(updatedClient.getSearchVendorsettingses());
                }
//            else {
//                updatedClient.setSearchVendorsettingses(client.getSearchVendorsettingses());
//            }
                String resumeVendorId = "";
                String postingVendorId = "";
                // Added validation for max search count - PIG
                if (_helper.isNotNull(consumer.getSearchSettings())) {
                    if (_helper.isNotNull(consumer.getSearchSettings().getMaxSearchCount()) && (!_helper.isValidInteger(consumer.getSearchSettings().getMaxSearchCount()))) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_MAX_SEARCHCOUNT), HttpStatus.BAD_REQUEST);
                    }
                    if (_helper.isNotNull(consumer.getSearchSettings().getMaxSearchCount()) && ((consumer.getSearchSettings().getMaxSearchCount().intValue()) <= 0)) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_MAX_SEARCHCOUNT), HttpStatus.BAD_REQUEST);
                    }
                    if (_helper.isNotNull(consumer.getSearchSettings().getMaxSearchCount()) && consumer.getSearchSettings().getMaxSearchCount() < 0 && consumer.getSearchSettings().getMaxSearchCount() > Integer.MAX_VALUE) {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_MAX_SEARCHCOUNT), HttpStatus.BAD_REQUEST);

                    }
                    if (_helper.isNotNull(consumer.getSearchSettings().getResume()) && consumer.getSearchSettings().getResume().size() > 0
                            && _helper.isNotNull(consumer.getLensSettings()) && _helper.isNotNull(consumer.getVendorSettings())) {
                        for (SearchSettings.Resume resume : consumer.getSearchSettings().getResume()) {
                            if (_helper.isNull(resume.getInstanceId())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                            }
                            resumeVendorId = resume.getVendorId().toString();
                            int vendoId = resume.getVendorId().intValue();
                            if (dbService.getVendorById(vendoId) == null) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                            }

                            if (_helper.isNull(resume.getVendorId())) {

                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_OR_INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                            }
                            if (!_helper.isNotNull(consumer.getLensSettings()) && !_helper.isNotNull(consumer.getLensSettings().getInstanceList())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                            }

                            if (_helper.isNotNull(consumer.getLensSettings().getInstanceList()) && consumer.getLensSettings().getInstanceList().size() > 0) {
                                boolean isAvailable = false;
                                for (InstanceList instance : consumer.getLensSettings().getInstanceList()) {
                                    if (instance.getInstanceId().equals(resume.getInstanceId())) {
                                        isAvailable = true;
                                    }
                                }
                                if (!isAvailable) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                                }
                            }

                            if (_helper.isNotNull(consumer.getVendorSettings()) && _helper.isNotNull(consumer.getVendorSettings().getVendorIdList()) && (!consumer.getVendorSettings().getVendorIdList().contains(resume.getVendorId()))) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                            }
                        }
                    }
                    if (_helper.isNotNull(consumer.getSearchSettings().getPosting()) && consumer.getSearchSettings().getPosting().size() > 0
                            && _helper.isNotNull(consumer.getLensSettings()) && _helper.isNotNull(consumer.getVendorSettings())) {
                        for (SearchSettings.Posting posting : consumer.getSearchSettings().getPosting()) {
                            if (_helper.isNull(posting.getInstanceId())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                            }
                            postingVendorId = posting.getVendorId().toString();

                            if (_helper.isNull(posting.getVendorId())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_OR_INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                            }
                            if (_helper.isNotNull(consumer.getLensSettings().getInstanceList())) {

                                boolean isAvailable = false;
                                for (InstanceList instance : consumer.getLensSettings().getInstanceList()) {
                                    if (instance.getInstanceId().equals(posting.getInstanceId())) {
                                        isAvailable = true;
                                    }
                                }
                                if (!isAvailable) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_INSTANCE_ID), HttpStatus.BAD_REQUEST);
                                }
                            }
                            if (!consumer.getVendorSettings().getVendorIdList().contains(posting.getVendorId())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_VENDOR_ID), HttpStatus.BAD_REQUEST);
                            }
                        }
                    }

                    if (!resumeVendorId.equals("") && !postingVendorId.equals("")) {
                        if (resumeVendorId.equals(postingVendorId)) {
                            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.SAME_VENDOR_ID_ERROR), HttpStatus.BAD_REQUEST);

                        }
                    }
                }

                // Set default max search count
                if (_helper.isNotNull(consumer.getSearchSettings()) && _helper.isNull(consumer.getSearchSettings().getMaxSearchCount())) {
                    if (_helper.isNotNullOrEmpty(client.getSearchSettings())) {
                        SearchSettings searchSettings = (SearchSettings) _helper.getObjectFromJson(client.getSearchSettings(), new SearchSettings());
                        if (_helper.isNotNull(searchSettings.getMaxSearchCount())) {
                            consumer.getSearchSettings().setMaxSearchCount(searchSettings.getMaxSearchCount());
                        } else {
                            consumer.getSearchSettings().setMaxSearchCount((long) apiConfig.getDefaultMaxSearchCount());
                        }
                    } else {
                        consumer.getSearchSettings().setMaxSearchCount((long) apiConfig.getDefaultMaxSearchCount());
                    }
                }

                String searchSettings = _helper.isNotNull(consumer.getSearchSettings()) ? _helper.getJsonStringFromObject(consumer.getSearchSettings()) : null;
                updatedClient.setSearchSettings(searchSettings);

                List<CoreClientapi> updatedApiList = new ArrayList<>();

                updatedApiList.addAll(updatedClient.getCoreClientapis());

                //Update already added API's
                for (int i = 0; i < updatedApiList.size(); i++) {
                    if (updatedApiList.get(i).getCoreApi() != null && updatedApiList.get(i).getCoreApi().getId() != 0) {
                        int id = updatedApiList.get(i).getCoreApi().getId();

                        boolean addApi = false;
                        CoreClientapi oldApi = new CoreClientapi();
                        for (CoreClientapi oldApiList : client.getCoreClientapis()) {
                            if (oldApiList.getCoreApi().getId() == id) {
                                addApi = true;
                                oldApi = oldApiList;
                            }
                        }

                        //for (CoreClientapi oldApi : client.getCoreClientapis()) {
                        if (addApi) {
                            CoreClientapi api = updatedApiList.get(i);
                            //  Increase the allowed transaction count while updating user transaction count
                            if (api.getRemainingTransactions() != 0) {
                                if (_helper.validateTransactionCount((api.getRemainingTransactions() + oldApi.getAllowedTransactions()))) {
                                    api.setAllowedTransactions((api.getRemainingTransactions() + oldApi.getAllowedTransactions()));
                                } else {
                                    api.setAllowedTransactions(apiConfig.getMaxTransactionCount());
                                }
                            } else {
                                api.setAllowedTransactions(oldApi.getAllowedTransactions());
                            }

                            // Allow Remaining Count
                            if (api.getRemainingTransactions() != 0) {
                                if (_helper.validateTransactionCount((api.getRemainingTransactions() + oldApi.getRemainingTransactions()))) {

                                    api.setRemainingTransactions((api.getRemainingTransactions() + oldApi.getRemainingTransactions()));

                                } else {
                                    api.setRemainingTransactions(apiConfig.getMaxTransactionCount());
                                }
                            } else {
                                api.setRemainingTransactions(oldApi.getRemainingTransactions());
                            }

                            // ActivationDate,ExpiryDate,Secret,Enabled
                            api.setActivationDate(api.getActivationDate() != null ? api.getActivationDate() : oldApi.getActivationDate());
                            api.setExpiryDate(api.getExpiryDate() != null ? api.getExpiryDate() : oldApi.getExpiryDate());
                            api.setSecret(_helper.isNotNullOrEmpty(api.getSecret()) ? api.getSecret() : oldApi.getSecret());
                            api.setEnabled(api.isEnabled());
                            if( consumer.isResellerUserEnabled()==true){
                                if (api.getActivationDate().after(api.getExpiryDate())) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                                }
                            }

                            api.getCoreApi().setApiKey(oldApi.getCoreApi().getApiKey());

                            updatedApiList.set(i, api);
                        } else {
                            CoreClientapi api = updatedApiList.get(i);
                            api.setSecret(api.getSecret());

                            //If both activation date and allowed transaction not specified
                            if (_helper.isNull(api.getActivationDate()) && _helper.isNull(api.getExpiryDate()) && _helper.isNull(api.getAllowedTransactions())) {
                                LocalDate today = new LocalDate();
                                LocalDate today30 = today.plusDays(apiConfig.getDefaultExpiryDateInterval());

                                api.setActivationDate(today.toDateTimeAtStartOfDay().toDate());
                                api.setExpiryDate(today30.toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
                                api.setAllowedTransactions(apiConfig.getDefaultTransactionCount());
                            } else if (_helper.isNotNull(api.getActivationDate()) && _helper.isNotNull(api.getExpiryDate())) {
                                api.setActivationDate(api.getActivationDate());
                                api.setExpiryDate(api.getExpiryDate());
                            } else if (_helper.isNotNull(api.getActivationDate())) {
                                Calendar cal = new GregorianCalendar();
                                cal.setTime(api.getActivationDate());
                                cal.add(Calendar.DAY_OF_MONTH, apiConfig.getDefaultExpiryDateInterval());
                                Date today30 = cal.getTime();
                                api.setActivationDate(api.getActivationDate());
                                api.setExpiryDate(new LocalDate(today30).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
                            } else if (_helper.isNotNull(api.getExpiryDate())) {
                                LocalDate today = new LocalDate();
                                api.setActivationDate(today.toDateTimeAtStartOfDay().toDate());
                                api.setExpiryDate(new LocalDate(api.getExpiryDate()).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());

                            } else {
                                LocalDate today = new LocalDate();
                                LocalDate today30 = today.plusDays(apiConfig.getDefaultExpiryDateInterval());

                                api.setActivationDate(today.toDateTimeAtStartOfDay().toDate());
                                api.setExpiryDate(today30.toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
                            }

                            if (api.getActivationDate().after(api.getExpiryDate())) {
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_EXPIRY_DATE), HttpStatus.BAD_REQUEST);
                            }

                            api.setAllowedTransactions(api.getAllowedTransactions() <= 0 ? apiConfig.getDefaultTransactionCount() : api.getAllowedTransactions());
                            api.setEnabled(api.isEnabled());
                            api.setRemainingTransactions(api.getAllowedTransactions());
                            CoreApi coreApi = new CoreApi();
                            coreApi.setId(api.getCoreApi().getId());
                            api.setCoreApi(coreApi);
                            updatedApiList.set(i, api);
                        }
                        //}
                    } else {
                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_ID), HttpStatus.BAD_REQUEST);
                    }
                }

                // Add new API's
                for (CoreClientapi api : client.getCoreClientapis()) {
                    boolean contains = false;

                    for (CoreClientapi updatedApi : updatedClient.getCoreClientapis()) {
                        if (Objects.equals(api.getCoreApi().getId(), updatedApi.getCoreApi().getId())) {
                            contains = true;
                        }
                    }

                    if (!contains) {
                        api.setEnabled(false);
                        updatedApiList.add(api);
                    }
                }

                Set<CoreClientapi> coreClientApiSet = new HashSet<>();

                coreClientApiSet.addAll(updatedApiList);

                updatedClient.setCoreClientapis(coreClientApiSet);

                updatedClient.setRateLimitEnabled(consumer.isRateLimitEnabled());

                if (consumer.isRateLimitEnabled()) {
                    updatedClient.setRateLimitTimePeriod(Integer.valueOf(consumer.getRateLimitTimePeriod().toString()));
                    updatedClient.setRateLimitCount(Integer.valueOf(consumer.getRateLimitCount().toString()));
//                    if (consumer.getRemainingRateLimitCount() != null) {
//                        updatedClient.setRemainingRateLimitCount(Integer.valueOf(consumer.getRemainingRateLimitCount().toString()));
//                    }
                } else {
                    updatedClient.setRateLimitEnabled(Boolean.FALSE);
                }

                LOGGER.info("Update Consumer Entity. Key - " + consumer.getKey());
                clientHashMap.put(consumer.getClientId(), client);
                updateClientHashMap.put(consumer.getClientId(), updatedClient);
                _id.add(consumer.getClientId());
                if (consumer.getClientId().intValue() == clientId) {
                    resellerClient = updatedClient;
                    consumerKey = consumer.getKey();
                }
            } catch (Exception ex) {
                LOGGER.error(ExceptionUtils.getStackTrace(ex));
                LOGGER.error("Update Consumer response. Key - " + consumer.getKey() + ", Exception - " + ex.getMessage());
                throw ex;
            }
        }
        try {
            // Send update request to db service
            ApiResponse dbResponse = dbService.updateClient(_id, clientHashMap, updateClientHashMap);

            if (dbResponse.status) {
                response.status = true;
                response.responseData = _coreMappers.createClientResponse(resellerClient);
            } else {
                response.responseData = dbResponse.responseData;
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Update Consumer response. Key - " + consumerKey + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get client by criteria
     *
     * @param clients
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getClients(GetClientsRequest clients) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            ClientCriteria clientCriteria = _coreMappers.createClientCriteria(clients);

            List<CoreClient> coreClientList = dbService.getClientByCriteria(clientCriteria);

            if (coreClientList == null) {
                LOGGER.info("Client does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.CLIENT_NOT_EXIST);
                return response;
            } else {
                response.status = true;

                response.responseData = createClientResponse(coreClientList);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get client. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get client by Id
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getClientById(int clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            CoreClient coreClient = dbService.getClientById(clientId);

            if (coreClient == null) {
                LOGGER.info("Client does not exist: " + clientId);
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.CLIENT_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createClientResponse(coreClient);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get client. ClientId - " + clientId + ", Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Delete client
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse deleteEntireConsumerDetails(int clientId) throws Exception {

        ApiResponse response = new ApiResponse();
        try {
            //Send delete request to db service
            LOGGER.info("Check client exists. ClientId -  " + clientId);
            CoreClient coreClient = dbService.getClientById(clientId);

            if (coreClient == null) {
                LOGGER.info("Client does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.CLIENT_NOT_EXIST);
                return response;
            }

            ApiResponse dbResponse = dbService.deleteClient(coreClient);

            if (dbResponse.status) {
                response.status = true;
                response.responseData = _coreMappers.deleteClientResponse(clientId);
            } else {
                response.responseData = dbResponse.responseData;
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to delete client. ClientId - " + clientId + ", Exception -  " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Delete client
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse deleteConsumer(int clientId) throws Exception {

        ApiResponse response = new ApiResponse();
        try {
            //Send delete request to db service
            LOGGER.info("Check client exists. ClientId -  " + clientId);
            CoreClient coreClient = dbService.getClientById(clientId);

            if (coreClient == null) {
                LOGGER.info("Client does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.CLIENT_NOT_EXIST);
                return response;
            }

            // Update deleted on column
            coreClient.setDeletedOn(Date.from(Instant.now()));
            ApiResponse dbResponse = dbService.updateClient(coreClient, coreClient);

            // TODO : Disable the following
            // dbService.deleteClient(coreClient);
            if (dbResponse.status) {
                response.status = true;
                response.responseData = _coreMappers.deleteClientResponse(clientId);
            } else {
                response.responseData = dbResponse.responseData;
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to delete client. ClientId - " + clientId + ", Exception -  " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Create client response
     *
     * @param coreClientList
     * @return
     * @throws java.lang.Exception
     */
    public List<Client> createClientResponse(List<CoreClient> coreClientList) throws Exception {
        List<Client> clientList = new ArrayList<>();
        for (CoreClient coreClient : coreClientList) {
            Client client = new Client();

            client.setId(coreClient.getId());
            client.setKey(coreClient.getClientKey());
            client.setSecret(coreClient.getSecret());
            client.setDumpStatus(coreClient.isDumpStatus());

            //List<CoreLenssettings> lensSettingsList = new ArrayList<>();
            //lensSettingsList.addAll(coreClient.getCoreLenssettingses());
            // LensSettings Criteria
            LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
            lensSettingsCriteria.setCoreClient(coreClient);
            lensSettingsCriteria.setIsHRXML(false);
            // Fetch LensSettings
            List<CoreLenssettings> lensSettingsList = dbService.getLensSettingsByCriteria(lensSettingsCriteria);

            ResourceCriteria resourceCriteria = new ResourceCriteria();
            resourceCriteria.setCoreClient(coreClient);
            // Fetch LensSettings
            List<CoreResources> resourceList = dbService.getResourceWithoutApiByCriteria(resourceCriteria);

            //List<CoreResources> resourceList = new ArrayList<>();
            //resourceList.addAll(coreClient.getCoreResourceses());
            List<ClientLensSettings> clientLensSettingsList = new ArrayList<>();
            if (_helper.isNotNull(clientLensSettingsList)) {
                for (CoreLenssettings settings : lensSettingsList) {
                    ClientLensSettings cls = new ClientLensSettings();
                    cls.setInstanceId(settings.getId());
                    Set<CoreClientlenssettings> ccs = settings.getCoreClientlenssettingses();

                    for (CoreClientlenssettings clientlenssettings : ccs) {
                        cls.setRateLimitEnabled(clientlenssettings.isRateLimitEnabled());
                        if (clientlenssettings.isRateLimitEnabled()) {
                            cls.setRateLimitTimePeriod(clientlenssettings.getRateLimitTimePeriod());
                            cls.setRateLimitCount(clientlenssettings.getRateLimitCount());
                        }
                    }
                }
            }

            client.setClientLensSettings(clientLensSettingsList);

            List<Integer> resourcesList = new ArrayList<>();

            if (_helper.isNotNull(resourceList)) {
                for (CoreResources resources : resourceList) {
                    resourcesList.add(resources.getId());
                }
            }

            client.setLicensedResources(resourcesList);

            List<CoreClientapi> clientApiList = new ArrayList<>();
            clientApiList.addAll(coreClient.getCoreClientapis());

            List<com.bgt.lens.model.rest.response.ClientApi> clientApisList = new ArrayList<>();
            for (CoreClientapi clientApi : clientApiList) {
                com.bgt.lens.model.rest.response.ClientApi api = new com.bgt.lens.model.rest.response.ClientApi();
                //api.Id = clientApi.getId_1();
                api.setId(null);
                api.setApiId(clientApi.getCoreApi().getId());
                api.setApiKey(clientApi.getCoreApi().getApiKey());
                api.setEnabled(clientApi.isEnabled());
                api.setSecret(clientApi.getSecret());
                api.setActivationDate(clientApi.getActivationDate());
                api.setExpiryDate(clientApi.getExpiryDate());
                api.setAllowedTransactions(clientApi.getAllowedTransactions());
                api.setRemainingTransactions(clientApi.getRemainingTransactions());
                clientApisList.add(api);
            }
            client.setClientApi(clientApisList);
            clientList.add(client);
        }
        return clientList;
    }
}
