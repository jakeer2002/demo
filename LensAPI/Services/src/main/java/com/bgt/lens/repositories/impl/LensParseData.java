// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.repositories.impl;

import com.bgt.lens.DocKey;
import com.bgt.lens.JLens;
import com.bgt.lens.LensException;
import com.bgt.lens.LensMessage;
import com.bgt.lens.LensSession;
import com.bgt.lens.ServerBusyException;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.ld.JLensLD;
import com.bgt.lens.model.entity.CoreLenssettings;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LensParseData {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(LensParseData.class);

    /**
     * JLensLD
     */
    private JLensLD jLensLD = null;

    private Helper helper = new Helper();

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * Get JLensLD
     *
     * @return
     */
    public JLensLD getjLensLD() {
        return jLensLD;
    }

    /**
     * Set JLensLD
     *
     * @param jLensLD
     */
    public void setjLensLD(JLensLD jLensLD) {
        this.jLensLD = jLensLD;
    }

    /**
     * Create socket connection to lens server
     *
     * @param coreLenssettings
     * @return
     * @throws LensException
     * @throws ServerBusyException
     */
    public LensSession createLensSession(CoreLenssettings coreLenssettings) throws LensException, ServerBusyException {
        LOGGER.info("Host:" + coreLenssettings.getHost() + "  Port: " + coreLenssettings.getPort() + "CharacterSet:" + coreLenssettings.getCharacterSet() + "TimeOut" + coreLenssettings.getTimeout());
        LensSession session = JLens.createSession(coreLenssettings.getHost(), coreLenssettings.getPort(), coreLenssettings.getCharacterSet());
        session.open();
        LOGGER.info("Lens Session opened");
        session.setEnableTransactionTimeout(true);
        session.setTransactionTimeout((long) 1000 * (long) coreLenssettings.getTimeout());
        return session;
    }

    /**
     * Create socket connection to lens server
     *
     * @param lenssettingses
     * @return
     * @throws LensException
     * @throws ServerBusyException
     */
    public JLensLD createLensSession(List<CoreLenssettings> lenssettingses) throws LensException, ServerBusyException {
//        LensSession session = jLensLD.createSession(lenssettingses, jLensLD);
//        session.open();
        LOGGER.info("Lens Session opened");
        return jLensLD;
    }

    /**
     * Close socket connection
     *
     * @param session
     */
    public void closeLensSession(LensSession session) {
        if (session != null && session.isOpen()) {
            try {
                session.close();
                LOGGER.info("Lens Session Closed");
            } catch (LensException lex) {
                LOGGER.error("Session Close Lens Exception : " + lex.getMessage());
            }
        }
    }

    /**
     * Tag Binary Data
     *
     * @param data
     * @param docType
     * @param type
     * @param locale
     * @param variantType
     * @param coreLenssettings
     * @param customSkillCode
     * @return
     * @throws LensException
     * @throws ServerBusyException
     */
    public String tagDocument(byte[] data, String docType, char type, String locale, com.bgt.lens.helpers.Enum.VariantType variantType, CoreLenssettings coreLenssettings, String customSkillCode) throws LensException, ServerBusyException {
        LensMessage outMessage = null;
        LensSession session = null;
        String messageData = null;
        try {
            session = createLensSession(coreLenssettings);

            //locale = lensInstanceManager.getDefaultLocaleByLocale(locale);
            LensMessage.MessageContentType jsonMessageType=  LensMessage.MessageContentType.JSON;
            switch (variantType) {
                case bgtxml:
                    if (!helper.isNotNullOrEmpty(customSkillCode)) {
                        outMessage = session.tagBinaryData(data, docType, type);
                    	//outMessage = session.tagBinaryData(data, docType, type, jsonMessageType);
                    } else {
                        outMessage = session.tagBinaryData(data, docType, type, customSkillCode.trim().toCharArray());
                    }
                    break;
                case rtf:
                    if (!helper.isNotNullOrEmpty(customSkillCode)) {
                        outMessage = session.tagBinaryDataWithRTF(data, docType, type);
                        //outMessage = session.tagBinaryData(data, docType, type, jsonMessageType);
                    } else {
                        outMessage = session.tagBinaryDataWithRTF(data, docType, type, customSkillCode.trim().toCharArray());
                    }
                    break;
                case htm:
                    if (!helper.isNotNullOrEmpty(customSkillCode)) {
                        outMessage = session.tagBinaryDataWithHTM(data, docType, type);
                        //outMessage = session.tagBinaryData(data, docType, type, jsonMessageType);
                    } else {
                        outMessage = session.tagBinaryDataWithHTM(data, docType, type, customSkillCode.trim().toCharArray());
                    }
                    break;
                default:
                    break;
            }

            if (outMessage != null) {
                messageData = outMessage.getMessageData();
            }
            return messageData;
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            throw lex;
        } finally {
            closeLensSession(session);
        }
    }

    /**
     * Register Document
     *
     * @param data
     * @param docType
     * @param vendor
     * @param docId
     * @param type
     * @param customSkillCode
     * @param coreLenssettings
     * @return
     * @throws LensException
     * @throws ServerBusyException
     */
    public String registerDocument(byte[] data, String docType, String vendor, String docId, char type, CoreLenssettings coreLenssettings, String customSkillCode) throws LensException, ServerBusyException {
        LensMessage outMessage = null;
        LensSession session = null;
        String messageData = null;
        try {
            session = createLensSession(coreLenssettings);
            if (!helper.isNotNullOrEmpty(customSkillCode)) {               
                outMessage = session.registerBinaryData(data, docType, vendor, docId, new DocKey(), type);
            } else {
                outMessage = session.registerBinaryData(data, docType, vendor, docId, new DocKey(), type, customSkillCode.trim().toCharArray());
            }
            if (outMessage != null) {
                messageData = outMessage.getMessageData();
            }
            return messageData;
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            throw lex;
        } finally {
            closeLensSession(session);
        }
    }

    /**
     * Register Document
     *
     * @param vendor
     * @param docID
     * @param docKey
     * @param type
     * @param coreLenssettings
     * @return
     * @throws LensException
     * @throws ServerBusyException
     */
    public String unregisterDocument(String vendor, String docID, DocKey docKey, char type, CoreLenssettings coreLenssettings) throws LensException, ServerBusyException {
        LensMessage outMessage = null;
        LensSession session = null;
        String messageData = null;
        try {
            session = createLensSession(coreLenssettings);
            outMessage = session.unregister(vendor, docID, docKey, type);
            if (outMessage != null) {
                messageData = outMessage.getMessageData();
            }
            return messageData;
        } catch (LensException | ServerBusyException lex) {
            LOGGER.error("Lens Exception : " + lex);
            throw lex;
        } finally {
            closeLensSession(session);
        }
    }
}
