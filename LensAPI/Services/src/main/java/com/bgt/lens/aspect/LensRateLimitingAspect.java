// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.LensSettingsCriteria;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicInteger;
import org.springframework.http.HttpStatus;

/**
 * Lens RateLimiting aspect
 */
@Aspect
public class LensRateLimitingAspect {

    private static final Logger LOGGER = LogManager.getLogger(LensRateLimitingAspect.class);
    private static final Helper _helper = new Helper();
    private IDataRepositoryService dataRepositoryService;
    private final Object lock = new Object();
    private final Object _lock = new Object();

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate template;

    /**
     * Initialize data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dataRepositoryService = dataRepositoryService;
    }

    // <editor-fold defaultstate="collapsed" desc="REDIS BASED LENS RATE LIMITING">
    /**
     * Validate Request
     *
     * @param joinPoint This will not consider info, ping and convert requests
     * Only Parse/Search(SendXML) requests are being taken
     * @return
     * @throws Exception
     */
    @Around("execution(public * com.bgt.lens.repositories.impl.LensRepository.tagBinaryData(..)) "
            + "|| execution(public * com.bgt.lens.repositories.impl.LensRepository.tagBinaryStructuredBGTXMLData(..)) "
            + "|| execution(public * com.bgt.lens.repositories.impl.LensRepository.tagBinaryHRXMLData(..))"
            + "|| execution(public * com.bgt.lens.repositories.impl.LensRepository.tagBinaryDataWithRTF(..))"
            + "|| execution(public * com.bgt.lens.repositories.impl.LensRepository.tagBinaryDataWithHTM(..))"
            + "|| execution(public * com.bgt.lens.repositories.impl.LensRepository.sendXMLCommand(..))"
            + "|| execution(public * com.bgt.lens.repositories.impl.LensRepository.findLocale(..))"
            + "|| execution(public * com.bgt.lens.instancemanager.impl.LensInstanceManager.ldParsingCheckRateLimit(..))")
    public Object validateLensRequestLimit(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        ApiContext apiContext = _helper.getApiContext();
        AdvanceLog advanceLog = apiContext.getAdvanceLog();
        long startTime = System.currentTimeMillis();
        Object[] obj = joinPoint.getArgs();
        if (obj[0] instanceof CoreLenssettings) {
            CoreLenssettings coreLenssettings = (CoreLenssettings) obj[0];
            Set<CoreClientlenssettings> clientlenssettingses = coreLenssettings.getCoreClientlenssettingses();
            if (clientlenssettingses != null) {
                for (CoreClientlenssettings clientlenssettings : clientlenssettingses) {
                    if (clientlenssettings.isRateLimitEnabled()) {
                        // Get keys based on cosumerId_locale_product_limit
                        // We set keys with customer name in first - since getting client id involves some time
                        int rateLimitCount = clientlenssettings.getRateLimitCount();
                        synchronized (lock) {
                            try {
                                boolean isKeyAvailable = template.opsForValue().getOperations().hasKey(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                        + coreLenssettings.getLocale().replaceAll("_", ""));
                                RedisAtomicInteger counter = null;
                                // if the key is not there
                                if (!isKeyAvailable) {
                                    int timePeriodInMintes = clientlenssettings.getRateLimitTimePeriod();
                                    counter = new RedisAtomicInteger(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                            + coreLenssettings.getLocale().replaceAll("_", ""), template.getConnectionFactory());
                                    counter.incrementAndGet();
                                    template.opsForValue().getOperations().expire(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                            + coreLenssettings.getLocale().replaceAll("_", ""), timePeriodInMintes, TimeUnit.MINUTES);
                                    break;
                                } else {
                                    counter = new RedisAtomicInteger(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                            + coreLenssettings.getLocale().replaceAll("_", ""), template.getConnectionFactory());
                                    int count = counter.incrementAndGet() - 1;
                                    if (count == 0) {
                                        int timePeriodInMintes = clientlenssettings.getRateLimitTimePeriod();
                                        template.opsForValue().getOperations().expire(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                                + coreLenssettings.getLocale().replaceAll("_", ""), timePeriodInMintes, TimeUnit.MINUTES);
                                    }
                                    // If count exceeds throw error
                                    if (count != 0 && count >= rateLimitCount) {
                                        throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.PRODUCT_LOCALE_RATELIMIT_ERROR), HttpStatus.TOO_MANY_REQUESTS);
                                    }  
                                }
                            } catch (RedisConnectionFailureException ex) {
                                LOGGER.error("Error occurred while connecting Redis :" + ex);
                                LOGGER.info("Skipping Product Rate Limiting");
                                return joinPoint.proceed();
                            }
                        }
                    }

                }
            }
        }
        // Set Advanced log time
        double elapsedTime = ((System.currentTimeMillis() - startTime) / 1000.0);
        // In LD based parsing - Rate Limiting will be called 2 times for a request
        // So consolidate the transaction time of both
        if (_helper.isNotNull(advanceLog.getProductRateLimit())) {
            advanceLog.setProductRateLimit(advanceLog.getProductRateLimit() + elapsedTime);
        } else {
            advanceLog.setProductRateLimit(elapsedTime);
        }
        return joinPoint.proceed();

    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="REDIS BASED LENS RATE LIMITING-LD">
    /**
     * Limit product requests in LD based parsing
     *
     * @param joinPoint
     * @return
     * @throws Exception
     * @throws Throwable
     */
    @Around("execution(public * com.bgt.lens.ld.impl.LensLDParseDataImpl.ldParsingCheckRateLimit(..))")
    public Object validateLensRequestLimitLDParsing(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        ApiContext apiContext = _helper.getApiContext();
        AdvanceLog advanceLog = apiContext.getAdvanceLog();
        long startTime = System.currentTimeMillis();
        Object[] obj = joinPoint.getArgs();
        String hostName = (String) obj[1];
        int port = (Integer) obj[2];
        String locale = (String) obj[3];
        String instanceType = (String) obj[4];
        CoreClient client = apiContext.getClient();
        LensSettingsCriteria lensSettingsCriteria = new LensSettingsCriteria();
        lensSettingsCriteria.setCoreClient(client);
        lensSettingsCriteria.setHost(hostName);
        lensSettingsCriteria.setPort(port);
        if (!"LD".equalsIgnoreCase(locale)) {
            lensSettingsCriteria.setLocale(locale);
        }
        lensSettingsCriteria.setInstanceType(instanceType);

        List<CoreLenssettings> coreLenssettingses = dataRepositoryService.getLensSettingsByCriteria(lensSettingsCriteria);
        CoreLenssettings coreLenssettings = coreLenssettingses.get(0);

        Set<CoreClientlenssettings> clientlenssettingses = coreLenssettings.getCoreClientlenssettingses();
        if (clientlenssettingses != null) {
            for (CoreClientlenssettings clientlenssettings : clientlenssettingses) {
                if (clientlenssettings.isRateLimitEnabled()) {
                    // Get keys based on cosumerId_locale_product_limit
                    // We set keys with customer name in first - since getting client id involves some time
                    int rateLimitCount = clientlenssettings.getRateLimitCount();
                    synchronized (lock) {
                        try {
                            boolean isKeyAvailable = template.opsForValue().getOperations().hasKey(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                    + coreLenssettings.getLocale().replaceAll("_", ""));
                            RedisAtomicInteger counter = null;
                            // if the key is not there
                            if (!isKeyAvailable) {
                                int timePeriodInMintes = clientlenssettings.getRateLimitTimePeriod();
                                counter = new RedisAtomicInteger(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                        + coreLenssettings.getLocale().replaceAll("_", ""), template.getConnectionFactory());
                                counter.incrementAndGet();

                                template.opsForValue().getOperations().expire(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                        + coreLenssettings.getLocale().replaceAll("_", ""), timePeriodInMintes, TimeUnit.MINUTES);
                                break;
                            } else {

                                counter = new RedisAtomicInteger(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                        + coreLenssettings.getLocale().replaceAll("_", ""), template.getConnectionFactory());
                                int count = counter.incrementAndGet() - 1;
                                if (count == 0) {
                                    int timePeriodInMintes = clientlenssettings.getRateLimitTimePeriod();
                                    template.opsForValue().getOperations().expire(apiContext.getClient().getId() + "_" + coreLenssettings.getInstanceType().toLowerCase() + "_"
                                            + coreLenssettings.getLocale().replaceAll("_", ""), timePeriodInMintes, TimeUnit.MINUTES);
                                }
                                // If count exceeds throw error
                                if (count != 0 && count >= rateLimitCount) {
                                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.PRODUCT_LOCALE_RATELIMIT_ERROR), HttpStatus.TOO_MANY_REQUESTS);
                                }  
                            }
                        } catch (RedisConnectionFailureException ex) {
                            LOGGER.error("Error occurred while connecting Redis :" + ex);
                            LOGGER.info("Skipping Product Rate Limiting - LD");
                            return joinPoint.proceed();
                        }
                    }
                }
            }
        }
        // Set Advanced log time
        double elapsedTime = ((System.currentTimeMillis() - startTime) / 1000.0);
        // In LD based parsing - Rate Limiting will be called 2 times for a request
        // So consolidate the transaction time of both
        if (_helper.isNotNull(advanceLog.getProductRateLimit())) {
            advanceLog.setProductRateLimit(advanceLog.getProductRateLimit() + elapsedTime);
        } else {
            advanceLog.setProductRateLimit(elapsedTime);
        }

        return joinPoint.proceed();
        
    }
    // </editor-fold>
}
