// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.license.impl;

import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.CoreMappers;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.license.ILicenseManagementService;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.http.HttpStatus;

/**
 *
 * License manage Implementation
 */
public class LicenseManager implements ILicenseManagementService {

    private static final Logger LOGGER = LogManager.getLogger(LicenseManager.class);
    private final CoreMappers _coreMappers = new CoreMappers();

    private IDataRepositoryService dbService;
    
    private final Helper _helper = new Helper();

    /**
     * Set data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dbService = dataRepositoryService;
    }

    /**
     * Get license details by Id
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getLicenseDetailsById(int clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Check client exists. ClientId - " + clientId);
            CoreClient coreClient = dbService.getClientById(clientId);

            if (coreClient == null) {
                LOGGER.info("Client does not exist");
                _helper.getErrorMessageWithURL(ApiErrors.CLIENT_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createLicenseResponse(coreClient);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get License details. Exception - " + ex.getMessage());
            if (ex instanceof ApiException) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LICENSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
            throw ex;
        }
        return response;
    }

    /**
     * Get License details
     *
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getLicenseDetailsAll() throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            List<CoreClient> coreClientList = dbService.getClientByCriteria(new ClientCriteria());

            if (coreClientList == null) {
                LOGGER.info("Client list is empty");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CLIENT_LIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createLicenseResponse(coreClientList);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get License details. Exception - " + ex.getMessage());
            if (ex instanceof ApiException) {
                throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.GET_LICENSE_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
            }
            throw ex;
        }
        return response;
    }

    /**
     * Check License activation date
     *
     * @param clientId
     * @param apiId
     * @return
     */
    @Override
    public boolean checkLicenseActivationDate(int clientId, int apiId) {
        try {
            LOGGER.info("Check clientexists. Clientid - " + clientId);
            CoreClient coreClient = dbService.getClientById(clientId);

            if (coreClient == null) {
                LOGGER.info("Client does not exist");
                return false;
            } else {
                for (CoreClientapi api : coreClient.getCoreClientapis()) {
                    if (api.getId().getApiId() == apiId) {
                        Date today = Date.from(Instant.now());
                        if (!api.getActivationDate().after(today)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception ex) {

            LOGGER.error("Unable to get client details. Exception - " + ex.getMessage());
            return false;
        }
        return false;
    }

    /**
     * Check License expiry date
     *
     * @param clientId
     * @param apiId
     * @return
     */
    @Override
    public boolean checkLicenseExpiryDate(int clientId, int apiId) {
        try {
            LOGGER.info("Check clientexists. Clientid - " + clientId);
            CoreClient coreClient = dbService.getClientById(clientId);

            if (coreClient == null) {
                LOGGER.info("Client does not exist");
                return false;
            } else {
                for (CoreClientapi api : coreClient.getCoreClientapis()) {
                    if (api.getId().getApiId() == apiId) {
                        Date today = Date.from(Instant.now());
                        if (!api.getExpiryDate().before(today)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception ex) {

            LOGGER.error("Unable to get client details. Exception - " + ex.getMessage());
            return false;
        }
        return false;
    }

    /**
     * Check remaining transaction count
     *
     * @param clientId
     * @param apiId
     * @return
     */
    @Override
    public boolean checkRemainingTransactionCount(int clientId, int apiId) {
        try {
            LOGGER.info("Check clientexists. Clientid - " + clientId);
            CoreClient coreClient = dbService.getClientById(clientId);

            if (coreClient == null) {
                LOGGER.info("Client does not exist");
                return false;
            } else {
                for (CoreClientapi api : coreClient.getCoreClientapis()) {
                    if (api.getId().getApiId() == apiId) {
                        if (api.getRemainingTransactions() > 0) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception ex) {

            LOGGER.error("Unable to get client details. Exception - " + ex.getMessage());
            return false;
        }
        return false;
    }

    /**
     * Update remaining transaction count
     *
     * @param clientId
     * @param apiId
     * @return
     */
    @Override
    public ApiResponse updateRemainingTransactionCount(int clientId, int apiId) {
        ApiResponse response = new ApiResponse();
        try {
            LOGGER.info("Check client exists. Clientid - " + clientId);
            CoreClient coreClient = dbService.getClientById(clientId);

            if (coreClient == null) {
                LOGGER.info("Client does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.CLIENT_NOT_EXIST);
                return response;
            } else {
                boolean contains = false;
                List<CoreClientapi> apiList = new ArrayList<>();
                Set<CoreClientapi> apiSet = new HashSet<>();
                apiList.addAll(coreClient.getCoreClientapis());

                int i = 0;
                for (CoreClientapi api : apiList) {
                    if (api.getId().getApiId() == apiId && api.getAllowedTransactions() != 0) {
                        apiList.get(i).setRemainingTransactions(apiList.get(i).getRemainingTransactions() - 1);
                        contains = true;
                    }
                    i++;
                }

                if (contains) {
                    apiSet.addAll(apiList);
                    coreClient.setCoreClientapis(apiSet);

                    ApiResponse dbResponse = dbService.updateClient(coreClient, coreClient);

                    if (dbResponse.status) {
                        response.status = true;
                        response.responseData = _coreMappers.createLicenseResponse(coreClient);
                    } else {
                        LOGGER.error("Unable to update transaction count: " + apiId + " - " + response.responseData);
                        response.responseData = dbResponse.responseData;
                    }
                } else {
                    LOGGER.info("Client Api does not exist");
                    response.responseData = _helper.getErrorMessageWithURL(ApiErrors.CLIENTAPI_NOT_EXIST);
                    return response;
                }
            }
        } catch (Exception ex) {
            response.status = false;
            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.CLIENTAPI_NOT_EXIST);
            LOGGER.error("Unable to get License details. Exception - " + ex.getMessage());
        }
        return response;
    }

    /**
     * Update remaining transaction count by client API
     *
     * @param coreClientapi
     * @return
     * @throws Exception
     */
    @Override
    public boolean updateTransactionCount(CoreClientapi coreClientapi) throws Exception {
        try {
            LOGGER.info("Update transaction count. ClientId - " + coreClientapi.getCoreClient().getId() + ", ApiId - " + coreClientapi.getCoreApi().getId());
            ApiResponse response = dbService.updateClientApi(coreClientapi);
            LOGGER.info("Updated transaction count");
            return response.status;
//            CoreClient client = dbService.getClientById(coreClientapi.getCoreClient().getId());
//            for(CoreClientapi clientApi : client.getCoreClientapis()){
//                if(clientApi.getId().equals(coreClientapi.getId())){
//                    LOGGER.info("Remaining transaction count - " + (clientApi.getRemainingTransactions() - 1));
//                    if (clientApi.getAllowedTransactions() != 0 && clientApi.getRemainingTransactions() != 0) {
//                        clientApi.setRemainingTransactions(clientApi.getRemainingTransactions() - 1);
//                    }
//                    ApiResponse response = dbService.updateClientApi(clientApi);
//                    LOGGER.info("Updated transaction count");
//                    return response.status;
//                }
//            }
//            return false;
            
        } catch (Exception ex) {
            throw ex;
        }
    }

}
