// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.parserservice.request.CanonRequest;
import com.bgt.lens.model.parserservice.request.ConvertRequest;
import com.bgt.lens.model.parserservice.request.ParseJobRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.services.logger.ILoggerService;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Lens document dump aspect
 */
@Aspect
public class LensDocumentDumpAspect {

    private static final Logger LOGGER = LogManager.getLogger(AuthenticationLogAspect.class);

    private final Helper _helper = new Helper();

    private ILoggerService loggerService = null;

    /**
     * Set logger service
     *
     * @param loggerService
     */
    public void setLoggerService(ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    /**
     * Input document dump
     *
     * @param joinPoint
     * @throws Exception
     * @throws Throwable
     */
    @AfterReturning("execution(public * com.bgt.lens.services.utility.impl.UtlityServiceImpl.convertBinaryData(..))"
            + "|| execution(public * com.bgt.lens.services.utility.impl.UtlityServiceImpl.parseResumeVariant(..))"
            + "|| execution(public * com.bgt.lens.services.utility.impl.UtlityServiceImpl.parseResume(..))"
            + "|| execution(public * com.bgt.lens.services.utility.impl.UtlityServiceImpl.parseJobVariant(..))"
            + "|| execution(public * com.bgt.lens.services.utility.impl.UtlityServiceImpl.parseJob(..))"
            + "|| execution(public * com.bgt.lens.services.utility.impl.UtlityServiceImpl.canonBinaryData(..))")
    public void lensDocumentDump(JoinPoint joinPoint) throws Exception, Throwable {
        try {
            LOGGER.info("Add LENS document dump to database");
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");

            Object[] obj = joinPoint.getArgs();
            if (obj[0] instanceof ConvertRequest) {
                ConvertRequest convertRequest = (ConvertRequest) obj[0];
                if (apiContext.getClient().isDumpStatus()) {
                    if (_helper.isNullOrEmpty(convertRequest.getLocale())) {
                       convertRequest.setLocale(null);
                    }
                    loggerService.addLensDocument(apiContext, convertRequest.getBinaryData(), convertRequest.getExtension(), convertRequest.getInstanceType(), convertRequest.getLocale());
                }
            } else if (obj[0] instanceof ParseResumeRequest) {
                ParseResumeRequest parseResumeRequest = (ParseResumeRequest) obj[0];
                if (apiContext.getClient().isDumpStatus()) {
                    if (_helper.isNullOrEmpty(parseResumeRequest.getLocale())) {
                        parseResumeRequest.setLocale(null);
                    }
                    loggerService.addLensDocument(apiContext, parseResumeRequest.getBinaryData(), parseResumeRequest.getExtension(), parseResumeRequest.getInstanceType(), parseResumeRequest.getLocale());
                }
            } else if (obj[0] instanceof ParseJobRequest) {
                ParseJobRequest parseJobRequest = (ParseJobRequest) obj[0];
                if (apiContext.getClient().isDumpStatus()) {
                    if (_helper.isNullOrEmpty(parseJobRequest.getLocale())) {
                        parseJobRequest.setLocale(null);
                    }
                    loggerService.addLensDocument(apiContext, parseJobRequest.getBinaryData(), parseJobRequest.getExtension(), parseJobRequest.getInstanceType(), parseJobRequest.getLocale());
                }
            } else if (obj[0] instanceof CanonRequest) {
                CanonRequest canonRequest = (CanonRequest) obj[0];
                if (apiContext.getClient().isDumpStatus()) {
                    if (_helper.isNullOrEmpty(canonRequest.getLocale())) {
                        canonRequest.setLocale(null);
                    }
                    loggerService.addLensDocument(apiContext, canonRequest.getBinaryData(), "xml", canonRequest.getInstanceType(), canonRequest.getLocale());
                }
            }
            
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add Lens Document Exception. Exception - " + ex.getMessage());
        }
    }
}
