// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.logger.impl;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.CoreMappers;
import com.bgt.lens.helpers.Enum.Headers;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.HttpRestRequest;
import com.bgt.lens.helpers.ResolverMappers;
import com.bgt.lens.helpers.Validator;
import com.bgt.lens.helpers.WebServiceClient;
import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.documentexporter.ExporterApiRequest;
import com.bgt.lens.model.entity.AggregatedCoreLensDocument;
import com.bgt.lens.model.entity.CoreAuthenticationlog;
import com.bgt.lens.model.entity.CoreErrorlog;
import com.bgt.lens.model.entity.CoreLensdocument;
import com.bgt.lens.model.entity.CoreUsagecounter;
import com.bgt.lens.model.entity.CoreUsagelog;
import com.bgt.lens.model.entity.CoreUsagelogdata;
import com.bgt.lens.model.entity.SearchRegisterlog;
import com.bgt.lens.model.es.entity.AdvancedLog;
import com.bgt.lens.model.es.entity.CoreAuthenticationLog;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeWithHTMRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.datarepository.impl.DataRepositoryServiceImpl;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Future;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

/**
 * Asynchronous log storage service
 */
public class AsyncLogStorageService {

    private static final Logger LOGGER = LogManager.getLogger(DataRepositoryServiceImpl.class);
    private final Helper _helper = new Helper();
    private final Validator _validator = new Validator();
    private final CoreMappers _coreMappers = new CoreMappers();
    private ParseResumeWithHTMRequest s = new ParseResumeWithHTMRequest();
    private final ResolverMappers _resolverMappers = new ResolverMappers();
    private ApiConfiguration apiConfig;

    @Value("${ElasticSearchNodes}")
    private String elasticSearchNodeList;

    @Value("${ElasticSearchUsageLogIndex}")
    private String elasticSearchUsageLogIndex;

    @Value("${ElasticSearchUsageLogType}")
    private String elasticSearchUsageLogType;

    @Value("${ElasticSearchUsageLogDataIndex}")
    private String elasticSearchUsageLogDataIndex;

    @Value("${ElasticSearchUsageLogDataType}")
    private String elasticSearchUsageLogDataType;

    @Value("${ElasticSearchAuthenticationLogIndex}")
    private String elasticSearchAuthenticationLogIndex;

    @Value("${ElasticSearchAuthenticationLogType}")
    private String elasticSearchAuthenticationLogType;

    @Value("${ElasticSearchErrorLogIndex}")
    private String elasticSearchErrorLogIndex;

    @Value("${ElasticSearchErrorLogType}")
    private String elasticSearchErrorLogType;

    @Value("${ElasticSearchAdvancedLogIndex}")
    public String elasticSearchAdvancedLogIndex;

    @Value("${ElasticSearchAdvancedLogType}")
    public String elasticSearchAdvancedLogType;

    @Value("${EnableAutoDateBasedUsageLog}")
    private Boolean enableAutoDateBasedUsageLog;

    @Value("${EnableAutoDateBasedUsageLogData}")
    private Boolean enableAutoDateBasedUsageLogData;

    @Value("${ElasticSearchTimeout}")
    public Integer elasticSearchTimeout;

    @Value("${exporter.talentpipeline.url}")
    private String talentPipelineURL;

    @Value("${exporter.lensapi.url}")
    private String lensapiURL;

    @Value("${exporter.runTime}")
    private String runTime;

    @Value("${exporter.contentType}")
    private String exportContentType;

    @Value("${exporter.acceptType}")
    private String exportAcceptType;

    @Value("${ExportToTalentPipeline}")
    private Boolean exportToTalentPipeline;
    
    @Value("${ExportToPostingPipeline}")
    private Boolean exportToPostingPipeline;

    @Value("${TalentPipelineTimeout}")
    private Integer talentPipelineTimeout;

    @Value("${MaxDocumentSizeInMB}")
    private Integer maxDocumentSize;

    @Value("${EnableUsageLogData}")
    private Boolean enableUsageLogData;
    
    @Value("${DisableDocumentDump}")
    private Boolean disableDocumentDump;

    private IDataRepositoryService dataRepositoryService;

    Client client = null;

    /**
     * Set data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dataRepositoryService = dataRepositoryService;
    }

    /**
     * set ApiConfig
     *
     * @param apiConfig
     */
    public void setapiConfig(ApiConfiguration apiConfig) {
        this.apiConfig = apiConfig;
    }

    public AsyncLogStorageService(ElasticSearchTransportClient esTransportClient) {
        if (_helper.isNotNull(esTransportClient) && _helper.isNotNull(esTransportClient.getClient())) {
            client = esTransportClient.getClient();
        } else {
            throw new ApiException("", "Unable to initialize elasticsearch client", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Async
    public Future<Boolean> addLensDocumentAsync(ApiContext context, String binaryData, String docType, String instanceType, String locale) throws Exception {
        LOGGER.info("Add LENS document dump for request - " + context.getRequestId());
        if (_helper.isNotNull(context)) {

            int actualDocSizeinMB = Math.round(binaryData.length() / (1024 * 1024));

            boolean isSizeExists = false;

            if (actualDocSizeinMB < maxDocumentSize) {
                isSizeExists = true;
            } else {
                LOGGER.error("Document size is exceeded its limit - " + context.getRequestId().toString());
            }

            boolean checkStatus = false;
            HttpRestRequest httpRestRequest = new HttpRestRequest();
            if ((exportToTalentPipeline||exportToPostingPipeline) && isSizeExists) {
                ExporterApiRequest exporterApiRequest = new ExporterApiRequest();
                exporterApiRequest.setClient(context.getClient().getClientKey());
                exporterApiRequest.setRunTime(runTime);
                exporterApiRequest.setLocale(locale);
                exporterApiRequest.setPayLoad(binaryData);
                exporterApiRequest.setInstanceType(instanceType);
                exporterApiRequest.setResource(context.getResource());
                exporterApiRequest.setRequestId(context.getRequestId().toString());
                SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dformat.setLenient(true);
                exporterApiRequest.setAcquiredDate(dformat.format(context.getInTime()));
//                exporterApiRequest.setAcquiredDate(context.getInTime().toString());
                exporterApiRequest.setIsParsed(context.getApiResponse().status);
                exporterApiRequest.setFileExtension(docType);

                String postData = null;
                String response = null;
                if (exportToTalentPipeline && exporterApiRequest.getResource().trim().equalsIgnoreCase("resume")) {
                    httpRestRequest.setWebServiceURL(new URL(talentPipelineURL));
                    exporterApiRequest.setIsParsed(!context.getApiResponse().status);
                    String expStatus;
                    if (Headers.ApplicationJSON.toString().equalsIgnoreCase(exportContentType)) {
                        postData = _helper.getJsonStringFromObject(exporterApiRequest);
                    } else {
                        postData = _helper.getXMLStringFromObject(exporterApiRequest);
                    }

                    if (Headers.ApplicationJSON.toString().equalsIgnoreCase(exportAcceptType)) {
                        expStatus = "\"success\":true";
                    } else {
                        expStatus = "<success>true</success>";
                    }
                    response = new WebServiceClient().sendHTTPRequest(httpRestRequest.webServiceURL, "POST",
                            exportContentType, exportAcceptType, postData, talentPipelineTimeout);
                    checkStatus = response.contains(expStatus);


                } 
                else if (exportToPostingPipeline){
                    httpRestRequest.setWebServiceURL(new URL(lensapiURL));
                    String expStatus;
                    if (Headers.ApplicationJSON.toString().equalsIgnoreCase(exportContentType)) {
                        postData = _helper.getJsonStringFromObject(exporterApiRequest);
                    } else {
                        postData = _helper.getXMLStringFromObject(exporterApiRequest);
                    }

                    if (Headers.ApplicationJSON.toString().equalsIgnoreCase(exportAcceptType)) {
                        expStatus = "\"success\":true";
                    } else {
                        expStatus = "<success>true</success>";
                    }
                    response = new WebServiceClient().sendHTTPRequest(httpRestRequest.webServiceURL, "POST",
                            exportContentType, exportAcceptType, postData, talentPipelineTimeout);
                    checkStatus = response.contains(expStatus);

                }

                if (!checkStatus) {
                    LOGGER.error("Unable to dump LENS document. URL - " + httpRestRequest.getWebServiceURL());
                } else {
                    if(disableDocumentDump){
                        LOGGER.info("Document dumped successfully");
                    }
                    else{
                        AggregatedCoreLensDocument aggLensDocument = new AggregatedCoreLensDocument();
                        aggLensDocument.setClientId(context.getClient().getId());
                        if (context.getClientApi() != null && context.getClientApi().getCoreApi() != null) {
                            aggLensDocument.setApiId(context.getClientApi().getCoreApi().getId());
                        }
                        aggLensDocument.setInstanceType(instanceType);
                        aggLensDocument.setLocale(locale);
                        aggLensDocument.setResource(context.getResource());
                        aggLensDocument.setMethod(context.getMethod());
                        aggLensDocument.setHour(new LocalDateTime().getHourOfDay());
                        aggLensDocument.setMinFileSize(binaryData.length());
                        aggLensDocument.setMaxFileSize(binaryData.length());
                        aggLensDocument.setAvgFileSize(binaryData.length());
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        df.setLenient(true);
                        aggLensDocument.setDate(df.format(new LocalDate().toDate()));
                        ApiResponse apiResponse = dataRepositoryService.updateDocumentDumpCount(aggLensDocument);
                        if (!apiResponse.status) {
                            LOGGER.error("Unable to update LENS document dump count in db");
                        }
                    }
                }
            }

            if (!(exportToTalentPipeline||exportToPostingPipeline) && isSizeExists) {
                LOGGER.info("Add LENS document dump to DB with request id - " + context.getRequestId());
                CoreLensdocument coreLensdocument = new CoreLensdocument();
                coreLensdocument.setRequestId(context.getRequestId().toString());
                coreLensdocument.setClientId(context.getClient().getId());
                coreLensdocument.setApiId(context.getClientApi().getCoreApi().getId());
                coreLensdocument.setMethod(context.getMethod());
                coreLensdocument.setResource(context.getResource());
                coreLensdocument.setInstanceType(instanceType);
                coreLensdocument.setLocale(locale);
                // TODO : need to check the use of type
                // Original file name 
                // Mime type            
                coreLensdocument.setType(true); // True - Resume ; False- Resume
                coreLensdocument.setByteSize(binaryData.length());
                coreLensdocument.setFileExtension(docType);
                coreLensdocument.setBinaryContent(binaryData);
                coreLensdocument.setCreatedOn(Date.from(Instant.now()));

                if (!context.getApiResponse().status) {
                    coreLensdocument.setRaisedError(true);
                }

                ApiResponse apiResponse = dataRepositoryService.createLensdocument(coreLensdocument);
                apiResponse.responseData = coreLensdocument;
                if (!apiResponse.status) {
                    LOGGER.error("Unable to dump LENS document in DB");
                } else {
                    if(disableDocumentDump){
                        LOGGER.info("Document dumped successfully");
                     }
                    else{
                        LOGGER.info("Added LENS document dump in DB for request - " + context.getRequestId());
                        AggregatedCoreLensDocument aggLensDocument = new AggregatedCoreLensDocument();
                        aggLensDocument.setClientId(context.getClient().getId());
                        if (context.getClientApi() != null && context.getClientApi().getCoreApi() != null) {
                            aggLensDocument.setApiId(context.getClientApi().getCoreApi().getId());
                        }
                        aggLensDocument.setInstanceType(instanceType);
                        aggLensDocument.setLocale(locale);
                        aggLensDocument.setResource(context.getResource());
                        aggLensDocument.setMethod(context.getMethod());
                        aggLensDocument.setHour(new LocalDateTime().getHourOfDay());
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        df.setLenient(true);
                        aggLensDocument.setDate(df.format(new LocalDate().toDate()));
                        apiResponse = dataRepositoryService.updateDocumentDumpCount(aggLensDocument);
                        if (!apiResponse.status) {
                            LOGGER.error("Unable to update LENS document dump count in db");
                        }
                    }
                    
                            
                }

                return new AsyncResult<>(apiResponse.status);
            } else {
                LOGGER.info("Added LENS document dump for request - " + context.getRequestId());
            }

            return new AsyncResult<>(checkStatus);//need to check
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_LENSDOCUMENTDUMP_ERROR));
        }
    }

    @Async
    public Future<Boolean> addAuthenticationLogAsync(ApiContext context) throws Exception {
        LOGGER.info("Add authentication log for request - " + context.getRequestId());
        if (_helper.isNotNull(context)) {
            CoreAuthenticationlog coreAuthenticationlog = new CoreAuthenticationlog();
            coreAuthenticationlog.setRequestId(context.getRequestId().toString());
            coreAuthenticationlog.setClientId(context.getClient().getId());
            coreAuthenticationlog.setApiId(context.getClientApi().getCoreApi().getId());
            coreAuthenticationlog.setSignature(context.getoAuthHeader().getSignature());
            coreAuthenticationlog.setTimestamp(context.getoAuthHeader().getTimeStamp());
            coreAuthenticationlog.setNonce(context.getoAuthHeader().getNonce());
            coreAuthenticationlog.setCreatedOn(Date.from(Instant.now()));
            ApiResponse response = dataRepositoryService.createAuthenticationlog(coreAuthenticationlog);
            response.responseData = coreAuthenticationlog;
            if (!response.status) {
                LOGGER.error("Unable to add authentication log");
            } else {
                LOGGER.info("Added authentication log for request - " + context.getRequestId());
            }

            return new AsyncResult<>(response.status);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_AUTHENTICATIONLOG_ERROR));
        }
    }

    @Async
    public Future<Boolean> updateUsageCounterAsync(ApiContext context) throws Exception {
        LOGGER.info("Update usage counter for request - " + context.getRequestId());
        if (_helper.isNotNull(context)) {
            int apiId = _helper.isNotNull(context.getClientApi()) && _helper.isNotNull(context.getClientApi().getCoreApi()) ? context.getClientApi().getCoreApi().getId() : 0;
            int clientId = _helper.isNotNull(context.getClient()) ? context.getClient().getId() : 0;
            String method = _helper.isNullOrEmpty(context.getMethod()) ? null : context.getMethod();
            String resource = _helper.isNullOrEmpty(context.getResource()) ? null : context.getResource();

            if (apiId != 0 && clientId != 0) {
                UsagecounterCriteria criteria = new UsagecounterCriteria();
                criteria.setApiId(apiId);
                criteria.setClientId(clientId);
                criteria.setMethod(method);
                criteria.setResource(resource);

                List<CoreUsagecounter> usageCounterList = dataRepositoryService.getUsagecounterByCriteria(criteria);

                if (usageCounterList != null && usageCounterList.size() > 0) {
                    CoreUsagecounter usageCounter = new CoreUsagecounter();
                    usageCounter.setApiId(apiId);
                    usageCounter.setClientId(clientId);
                    usageCounter.setMethod(method);
                    usageCounter.setResource(resource);
                    usageCounter.setUpdatedOn(Date.from(Instant.now()));
                    usageCounter.setUsageCount(usageCounterList.get(0).getUsageCount() + 1);
                    usageCounter.setCreatedOn(usageCounterList.get(0).getCreatedOn());
                    usageCounter.setId(usageCounterList.get(0).getId());

                    ApiResponse response = dataRepositoryService.updateUsagecounter(usageCounter);
                    response.responseData = usageCounter;
                    if (!response.status) {
                        LOGGER.error("Unable to update usage counter");
                    } else {
                        LOGGER.info("Updated usage counter for request - " + context.getRequestId());
                    }

                    return new AsyncResult<>(response.status);
                } else {
                    CoreUsagecounter usageCounter = new CoreUsagecounter();
                    Date now = Date.from(Instant.now());
                    usageCounter.setApiId(apiId);
                    usageCounter.setClientId(clientId);
                    usageCounter.setMethod(method);
                    usageCounter.setResource(resource);
                    usageCounter.setUpdatedOn(now);
                    usageCounter.setUsageCount(1);
                    usageCounter.setCreatedOn(now);

                    ApiResponse response = dataRepositoryService.createUsagecounter(usageCounter);
                    response.responseData = usageCounter;
                    if (!response.status) {
                        LOGGER.error("Unable to update usage counter");
                    } else {
                        LOGGER.info("Added LENS document dump for request - " + context.getRequestId());
                    }

                    return new AsyncResult<>(response.status);
                }
            } else {
                throw new Exception("Unable to update usage counter.");
            }
        } else {
            throw new Exception("Unable to update usage counter.");
        }
    }

    @Async
    public Future<Boolean> addUsageLogAsync(ApiContext context) throws Exception {
        LOGGER.info("Add usage log for request - " + context.getRequestId());
        if (_helper.isNotNull(context)) {
            LOGGER.info("Usage log - Api Context - Start time : " + Date.from(Instant.now()));
            CoreUsagelog usagelog = new CoreUsagelog();
            usagelog.setApiId(_helper.isNotNull(context.getClientApi()) && _helper.isNotNull(context.getClientApi().getCoreApi()) ? context.getClientApi().getCoreApi().getId() : null);
            usagelog.setClientId(_helper.isNotNull(context.getClient()) ? context.getClient().getId() : null);
            usagelog.setInTime(_helper.isNotNull(context.getInTime()) ? context.getInTime() : null);
            usagelog.setMethod(_helper.isNotNull(context.getMethod()) ? context.getMethod() : null);
            usagelog.setRequestId(context.getRequestId().toString());
            usagelog.setResource(_helper.isNotNull(context.getResource()) ? context.getResource() : null);
            usagelog.setInstanceType(_helper.isNotNull(context.getInstanceType()) ? context.getInstanceType() : null);
            usagelog.setLocale(_helper.isNotNull(context.getLocale()) ? context.getLocale() : null);
            usagelog.setParseVariant(_helper.isNotNull(context.getParseVariant()) ? context.getParseVariant() : null);
            usagelog.setUri(_helper.isNotNull(context.getRequestUri()) ? context.getRequestUri() : null);
            usagelog.setServerAddress(_helper.isNotNull(context.getServerAddress()) ? context.getServerAddress() : null);
            usagelog.setClientAddress(_helper.isNotNull(context.getClientAddress()) ? context.getClientAddress() : null);

            LOGGER.info("Usage log - Api Context - Serialization - Request - Start time : " + Date.from(Instant.now()));

            if (enableUsageLogData != null && enableUsageLogData) {
                usagelog.setCoreUsagelogdata(new CoreUsagelogdata());
                // set request Header
                // usagelog.setHeaders(_helper.isNotNull(context.getRequestHeader()) ? context.getRequestHeader() : null);
                usagelog.getCoreUsagelogdata().setHeaders(_helper.isNotNull(context.getRequestHeader()) ? context.getRequestHeader() : null);

                // set request content
                if (_helper.isNotNull(context.getRequestContent())) {
                    if (context.getMethod().equalsIgnoreCase(com.bgt.lens.helpers.Enum.HttpMethod.GET.toString()) || context.getMethod().equalsIgnoreCase(com.bgt.lens.helpers.Enum.HttpMethod.OPTIONS.toString()) || context.getMethod().equalsIgnoreCase(com.bgt.lens.helpers.Enum.HttpMethod.DELETE.toString())) {
//                    usagelog.setRequestContent(context.getRequestContent().toString());
                        usagelog.getCoreUsagelogdata().setRequestContent(context.getRequestContent().toString());
                    } else if (context.getContentType() != null && context.getContentType().toLowerCase(Locale.ENGLISH).contains(com.bgt.lens.helpers.Enum.Headers.ApplicationJSON.toString())) {
//                    usagelog.setRequestContent(_helper.getJsonStringFromObject(context.getRequestContent()));
                        usagelog.getCoreUsagelogdata().setRequestContent(_helper.getJsonStringFromObject(context.getRequestContent()));
                    } else {
                        if (!context.getRequestContent().getClass().equals(String.class)) {
                            usagelog.getCoreUsagelogdata().setRequestContent(_helper.getXMLStringFromObject(context.getRequestContent()));
//                        usagelog.setRequestContent(_helper.getXMLStringFromObject(context.getRequestContent()));
                        } else {
                            usagelog.getCoreUsagelogdata().setRequestContent(context.getRequestContent().toString());
//                        usagelog.setRequestContent(context.getRequestContent().toString());
                        }
                    }

                } else {
                    usagelog.getCoreUsagelogdata().setRequestContent(null);
//                usagelog.setRequestContent(null);
                }

                LOGGER.info("Usage log - Api Context - Serialization - Request - End time : " + Date.from(Instant.now()));
                LOGGER.info("Usage log - Api Context - Serialization - Response - Start time : " + Date.from(Instant.now()));

                //set response content and raised error
                if (_helper.isNotNull(context.getApiResponse())) {
                    if (context.getAccept() != null && context.getAccept().toLowerCase(Locale.ENGLISH).contains(com.bgt.lens.helpers.Enum.Headers.ApplicationJSON.toString())) {
                        usagelog.getCoreUsagelogdata().setResponseContent(_helper.getJsonStringFromObject(context.getApiResponse()));
//                    usagelog.setResponseContent(_helper.getJsonStringFromObject(context.getApiResponse()));
                    } else if (_helper.isNotNullOrEmpty(context.getReturnType())) {
                        Object obj = null;
                        switch (context.getReturnType()) {
                            case "InfoResponse":
                                obj = _resolverMappers.getinfo(context.getApiResponse());
                                break;
                            case "PingResponse":
                                obj = _resolverMappers.getPing(context.getApiResponse());
                                break;
                            case "ConvertResponse":
                                obj = _resolverMappers.convertBinaryData(context.getApiResponse());
                                break;
                            case "CanonResponse":
                                obj = _resolverMappers.canonBinaryData(context.getApiResponse());
                                break;
                            case "ParseResumeResponse":
                                obj = _resolverMappers.parseResume(context.getApiResponse());
                                break;
                            case "ParseResumeBGXMLResponse":
                                obj = _resolverMappers.parseResumeBGXML(context.getApiResponse());
                                break;
                            case "ParseResumeStructuredBGXMLResponse":
                                obj = _resolverMappers.parseResumeStructuredBGXML(context.getApiResponse());
                                break;
                            case "ParseResumeHRXMLResponse":
                                obj = _resolverMappers.parseResumeHRXML(context.getApiResponse());
                                break;
                            case "ParseResumeWithRTFResponse":
                                obj = _resolverMappers.parseResumeWithRTF(context.getApiResponse());
                                break;
                            case "ParseResumeWithHTMResponse":
                                obj = _resolverMappers.parseResumeWithHTM(context.getApiResponse());
                                break;
                            case "ParseJobResponse":
                                obj = _resolverMappers.parseJob(context.getApiResponse());
                                break;
                            case "ParseJobBGXMLResponse":
                                obj = _resolverMappers.parseJobBGXML(context.getApiResponse());
                                break;
                            case "ParseJobStructuredBGXMLResponse":
                                obj = _resolverMappers.parseJobStructuredBGXML(context.getApiResponse());
                                break;
                            case "ParseJobWithRTFResponse":
                                obj = _resolverMappers.parseJobWithRFT(context.getApiResponse());
                                break;
                            case "ParseJobWithHTMResponse":
                                obj = _resolverMappers.parseJobWithHTM(context.getApiResponse());
                                break;
                            case "ProcessLocaleResponse":
                                obj = _resolverMappers.findLocale(context.getApiResponse());
                                break;
                            case "CreateApiResponse":
                                obj = _resolverMappers.createApi(context.getApiResponse());
                                break;
                            case "UpdateApiResponse":
                                obj = _resolverMappers.updateApi(context.getApiResponse());
                                break;
                            case "DeleteApiResponse":
                                obj = _resolverMappers.deleteApi(context.getApiResponse());
                                break;
                            case "GetApiResponse":
                                obj = _resolverMappers.getApi(context.getApiResponse());
                                break;
                            case "GetApiByIdResponse":
                                obj = _resolverMappers.getApiById(context.getApiResponse());
                                break;
                            case "AddResourceResponse":
                                obj = _resolverMappers.addResource(context.getApiResponse());
                                break;
                            case "UpdateResourceResponse":
                                obj = _resolverMappers.updateResource(context.getApiResponse());
                                break;
                            case "DeleteResourceResponse":
                                obj = _resolverMappers.deleteResource(context.getApiResponse());
                                break;
                            case "GetResourceResponse":
                                obj = _resolverMappers.getResource(context.getApiResponse());
                                break;
                            case "GetResourceByIdResponse":
                                obj = _resolverMappers.getResourcebyId(context.getApiResponse());
                                break;
                            case "AddLensSettingsResponse":
                                obj = _resolverMappers.addLensSettings(context.getApiResponse());
                                break;
                            case "UpdateLensSettingsResponse":
                                obj = _resolverMappers.updateLensSettings(context.getApiResponse());
                                break;
                            case "DeleteLensSettingsResponse":
                                obj = _resolverMappers.deleteLensSettings(context.getApiResponse());
                                break;
                            case "GetLensSettingsResponse":
                                obj = _resolverMappers.getLensSettings(context.getApiResponse());
                                break;
                            case "GetLensSettingsByIdResponse":
                                obj = _resolverMappers.getLensSettingsById(context.getApiResponse());
                                break;
                            case "CreateConsumerResponse":
                                obj = _resolverMappers.createConsumer(context.getApiResponse());
                                break;
                            case "UpdateConsumerResponse":
                                obj = _resolverMappers.updateConsumer(context.getApiResponse());
                                break;
                            case "DeleteConsumerResponse":
                                obj = _resolverMappers.deleteConsumer(context.getApiResponse());
                                break;
                            case "GetClientsResponse":
                                obj = _resolverMappers.getClients(context.getApiResponse());
                                break;
                            case "GetClientByIdResponse":
                                obj = _resolverMappers.getClientById(context.getApiResponse());
                                break;
                            case "GetAllLicenseDetailsResponse":
                                obj = _resolverMappers.getLicenseDetails(context.getApiResponse());
                                break;
                            case "GetLicenseResponse":
                                obj = _resolverMappers.getLicenseDetailsById(context.getApiResponse());
                                break;
                            case "GetUsageLogsResponse":
                                obj = _resolverMappers.getUsageLogs(context.getApiResponse());
                                break;
                            case "GetUsageLogByIdResponse":
                                obj = _resolverMappers.getUsageLogsById(context.getApiResponse());
                                break;
                            case "GetErrorLogsResponse":
                                obj = _resolverMappers.getErrorLogs(context.getApiResponse());
                                break;
                            case "GetErrorLogByIdResponse":
                                obj = _resolverMappers.getErrorLogsById(context.getApiResponse());
                                break;
                            case "GetUsageCounterResponse":
                                obj = _resolverMappers.getUsageCounter(context.getApiResponse());
                                break;
                            case "GetUsageCounterByIdResponse":
                                obj = _resolverMappers.getUsageCounterById(context.getApiResponse());
                                break;
                        }

                        usagelog.getCoreUsagelogdata().setResponseContent(_helper.getXMLStringFromObject(obj));
//                    usagelog.setResponseContent(_helper.getXMLStringFromObjectForSOAPResponse(context.getReturnType(), context.getApiResponse()));
                    } else {
                        usagelog.getCoreUsagelogdata().setResponseContent(_helper.getXMLStringFromObject(context.getApiResponse()));
//                    usagelog.setResponseContent(_helper.getXMLStringFromObject(context.getApiResponse()));
                    }

                    if (!context.getApiResponse().status) {
                        usagelog.setRaisedError(true);
                    }
                } else {
                    usagelog.getCoreUsagelogdata().setResponseContent(null);
//                usagelog.setResponseContent(null);
                    usagelog.setRaisedError(true);
                }

                LOGGER.info("Usage log - Api Context - Serialization - Response -  End time : " + Date.from(Instant.now()));
            } else {
                if (_helper.isNotNull(context.getApiResponse())) {
                    if (!context.getApiResponse().status) {
                        usagelog.setRaisedError(true);
                    }
                } else {
                    usagelog.getCoreUsagelogdata().setResponseContent(null);
                    usagelog.setRaisedError(true);
                }
            }

            Date now = Date.from(Instant.now());
            usagelog.setOutTime(now);

            //Set milliseconds
            if (_helper.isNotNull(context.getInTime())) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                long milliseconds1 = cal.getTimeInMillis();
                cal.setTime(context.getInTime());
                long milliseconds2 = cal.getTimeInMillis();
                long diff = milliseconds1 - milliseconds2;
                usagelog.setMilliseconds(diff);
            } else {
                usagelog.setMilliseconds((long) 0);
            }

            LOGGER.info("Usage log - Api Context - End time : " + Date.from(Instant.now()));

            ApiResponse response = dataRepositoryService.createUsagelog(usagelog);
            response.responseData = usagelog;

            if (!response.status) {
                LOGGER.error("Unable to add usage log");
            } else {
                LOGGER.info("Added usage log for request - " + context.getRequestId());
            }

            return new AsyncResult<>(response.status);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_USAGELOG_ERROR));
        }
    }

    @Async
    public Future<Boolean> addErrorLogAsync(ApiContext context) throws Exception {
        LOGGER.info("Add error log for request - " + context.getRequestId());
        if (_helper.isNotNull(context)) {
            CoreErrorlog errorLog = new CoreErrorlog();
            errorLog.setApiId(_helper.isNotNull(context.getClientApi()) && _helper.isNotNull(context.getClientApi().getCoreApi()) ? context.getClientApi().getCoreApi().getId() : null);
            errorLog.setClientId(_helper.isNotNull(context.getClient()) ? context.getClient().getId() : null);
            errorLog.setCreatedOn(Date.from(Instant.now()));
            errorLog.setException(_helper.isNotNull(context.getException()) ? context.getException().getMessage() : null);
            errorLog.setRequestId(_helper.isNotNull(context.getRequestId()) ? context.getRequestId().toString() : null);
            errorLog.setStatusCode(_helper.isNotNull(context.getApiResponse()) ? context.getApiResponse().statusCode.value() : 500); // ToDo
            errorLog.setUserMessage(_helper.isNotNull(context.getException()) ? context.getException().getMessage() : null);
            errorLog.setTraceLog(_helper.isNotNull(context.getTraceLog()) ? context.getTraceLog() : null);

            ApiResponse response = dataRepositoryService.createErrorlog(errorLog);
            response.responseData = errorLog;
            if (!response.status) {
                LOGGER.error("Unable to add error log");
            } else {
                LOGGER.info("Added error log for request - " + context.getRequestId());
            }

            return new AsyncResult<>(response.status);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_ERRORLOG_ERROR));
        }
    }

    @Async
    public Future<Boolean> addSearchRegisterLogAsync(ApiContext context) throws Exception {
        LOGGER.info("Add register log for request - " + context.getRequestId());
        if (_helper.isNotNull(context)) {
            SearchRegisterlog searchRegisterlog = context.getSearchRegisterLog();

            if (searchRegisterlog != null) {
                searchRegisterlog.setRequestId(context.getRequestId().toString());
                ApiResponse response = dataRepositoryService.addRegisterLog(searchRegisterlog);
                response.responseData = searchRegisterlog;
                if (!response.status) {
                    LOGGER.error("Unable to add register log");
                } else {
                    LOGGER.info("Added register log for request - " + context.getRequestId());
                }

                return new AsyncResult<>(response.status);
            } else {
                throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_REGISTERLOG_ERROR));
            }
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_REGISTERLOG_ERROR));
        }
    }

    // ELK Logging 
    @Async
    public Future<Boolean> addErrorLogToAMQPAsync(ApiContext context) throws Exception {
        LOGGER.info("Add error log for request - " + context.getRequestId());
        if (_helper.isNotNull(context)) {
            com.bgt.lens.model.es.entity.CoreErrorlog errLog = new com.bgt.lens.model.es.entity.CoreErrorlog();
            errLog.setApi(_helper.isNotNull(context.getClientApi()) && _helper.isNotNull(context.getClientApi().getCoreApi()) ? context.getClientApi().getCoreApi().getApiKey() : null);
            errLog.setConsumerKey(_helper.isNotNull(context.getClient()) ? context.getClient().getClientKey() : null);
            errLog.setCreatedOn(Date.from(Instant.now()));
            errLog.setException(_helper.isNotNull(context.getException()) ? context.getException().getMessage() : null);
            errLog.setStatusCode(_helper.isNotNull(context.getApiResponse()) ? context.getApiResponse().statusCode.value() : 500); // ToDo
            errLog.setUserMessage(_helper.isNotNull(context.getException()) ? context.getException().getMessage() : null);
            errLog.setTraceLog(_helper.isNotNull(context.getTraceLog()) ? context.getTraceLog() : null);

            CoreErrorlog errorLog = new CoreErrorlog();
            errorLog.setApiId(_helper.isNotNull(context.getClientApi()) && _helper.isNotNull(context.getClientApi().getCoreApi()) ? context.getClientApi().getCoreApi().getId() : null);
            errorLog.setClientId(_helper.isNotNull(context.getClient()) ? context.getClient().getId() : null);
            errorLog.setCreatedOn(Date.from(Instant.now()));
            errorLog.setException(_helper.isNotNull(context.getException()) ? context.getException().getMessage() : null);
            errorLog.setRequestId(_helper.isNotNull(context.getRequestId()) ? context.getRequestId().toString() : null);
            errorLog.setStatusCode(_helper.isNotNull(context.getApiResponse()) ? context.getApiResponse().statusCode.value() : 500); // ToDo
            errorLog.setUserMessage(_helper.isNotNull(context.getException()) ? context.getException().getMessage() : null);
            errorLog.setTraceLog(_helper.isNotNull(context.getTraceLog()) ? context.getTraceLog() : null);

            try {
//                
                String responseMessage = _helper.getJsonStringFromObjectWithoutNull(errLog);

                if (_helper.isNotNullOrEmpty(responseMessage)) {
                    responseMessage = responseMessage.replaceAll("\r\n", "");
                }

                try {
                    IndexResponse errorLogStatus = client.prepareIndex(elasticSearchErrorLogIndex, elasticSearchErrorLogType, context.getRequestId().toString())
                            .setSource(responseMessage, XContentType.JSON)
                            .get(TimeValue.timeValueMillis((long) elasticSearchTimeout));

                    if ((!errorLogStatus.status().equals(RestStatus.CREATED))) {
                        LOGGER.error("Unable to index error log into elasticsearch instance - " + errorLog.getRequestId());
                        LOGGER.info("Starting store error log to database. RequestId - " + errorLog.getRequestId());
                        ApiResponse apiResponse = dataRepositoryService.createErrorlog(errorLog);
                        if (!apiResponse.status) {
                            LOGGER.error("Unable to store error log. Request Id - " + errorLog.getRequestId());
                        }
                    }
                } catch (Exception ex) {
                    LOGGER.error("Unable to index error log into elasticsearch instance - " + errorLog.getRequestId());
                    LOGGER.info("Starting store error log to database. RequestId - " + errorLog.getRequestId());
                    ApiResponse apiResponse = dataRepositoryService.createErrorlog(errorLog);
                    if (!apiResponse.status) {
                        LOGGER.error("Unable to store error log. Request Id - " + errorLog.getRequestId());
                    }
                }

                return new AsyncResult<>(true);
            } catch (Exception ex) {
                LOGGER.error("Unable to index error log data into elasticsearch instance - " + errorLog.getRequestId() + " - Exception - " + ex.getMessage());
            }
            return new AsyncResult<>(false);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_ERRORLOG_ERROR));
        }
    }

    @Async
    public Future<Boolean> addAdvancedLogToAMQPAsync(ApiContext context) throws Exception {
        LOGGER.info("Add advanced log for request - " + context.getRequestId());
        if (_helper.isNotNull(context)) {
            AdvancedLog advancedLog = new AdvancedLog();
            AdvanceLog advanceLog = context.getAdvanceLog();
            advancedLog.setApi(context.getApi());
            advancedLog.setMethod(context.getMethod());
            advancedLog.setVersion("2.0");
            advancedLog.setApiRateLimit(advanceLog.getApiRateLimit() != 0 ? advanceLog.getApiRateLimit() : null);
            advancedLog.setAuthLog(advanceLog.getAuthenticationLog() != 0 ? advanceLog.getAuthenticationLog() : null);
            advancedLog.setAuthValidation(advanceLog.getAuthenticationValidation() != 0 ? advanceLog.getAuthenticationValidation() : null);
            if (_helper.isNotNull(context.getClient())) {
                advancedLog.setConsumerKey(context.getClient().getClientKey());
            }
            advancedLog.setDocumentDump(advanceLog.getLensDocumentDump() != 0 ? advanceLog.getLensDocumentDump() : null);
            advancedLog.setElapsedTime(Double.valueOf(context.getSeconds()));
            advancedLog.setEndTime(context.getOutTime());
            advancedLog.setErrorLogUpdate(advanceLog.getErrorLog() != 0 ? advanceLog.getErrorLog() : null);
            advancedLog.setFacetDistCommandProcess(advanceLog.getFacetDistributionCommandProcessUnit() != 0 ? advanceLog.getFacetDistributionCommandProcessUnit() : null);
            advancedLog.setFacetDistcommandBuild(advanceLog.getFacetDistributionCommandBuildUnit() != 0 ? advanceLog.getFacetDistributionCommandBuildUnit() : null);
            advancedLog.setFetchCommandProcess(advanceLog.getFetchDocumentProcessUnit() != 0 ? advanceLog.getFetchDocumentProcessUnit() : null);
            advancedLog.setHrxmlUpdate(advanceLog.getHrxmlStyleSheetUpdate() != 0 ? advanceLog.getHrxmlStyleSheetUpdate() : null);
            advancedLog.setLocaleDetection(advanceLog.getLocaleDetectionDump() != 0 ? advanceLog.getLocaleDetectionDump() : null);
            advancedLog.setOverWriteRegisterDocument(advanceLog.getOverwriteRegisterDocumentUnit() != 0 ? advanceLog.getOverwriteRegisterDocumentUnit() : null);
            advancedLog.setOverallLensCommunication(advanceLog.getLensCommunication() != 0 ? advanceLog.getLensCommunication() : null);
            advancedLog.setProductRateLimit(advanceLog.getProductRateLimit() != 0 ? advanceLog.getProductRateLimit() : null);
            advancedLog.setRegisterTagDocument(advanceLog.getRegisterParseDocument() != 0 ? advanceLog.getRegisterParseDocument() : null);
            advancedLog.setRegisterWithCustomFilters(advanceLog.getRegisterDocumentWithCustomFilterUnit() != 0 ? advanceLog.getRegisterDocumentWithCustomFilterUnit() : null);
            advancedLog.setRegisterWithoutCustomFilters(advanceLog.getRegisterDocumentWithoutCustomFilterUnit() != 0 ? advanceLog.getRegisterDocumentWithoutCustomFilterUnit() : null);
//            advancedLog.setRequestId(context.getRequestId().toString());
            advancedLog.setResourceName(context.getResource());
            advancedLog.setSearchCommandBuild(advanceLog.getSearchCommandbuildUnit() != 0 ? advanceLog.getSearchCommandbuildUnit() : null);
            advancedLog.setSearchCommandDump(advanceLog.getSearchCommandDumpUnit() != 0 ? advanceLog.getSearchCommandDumpUnit() : null);
            advancedLog.setSearchCommandProcess(advanceLog.getSearchCommandProcessUnit() != 0 ? advanceLog.getSearchCommandProcessUnit() : null);
            advancedLog.setSearchTagDocument(advanceLog.getSearchParseDocument() != 0 ? advanceLog.getSearchParseDocument() : null);
            advancedLog.setStartTime(context.getInTime());
            if (_helper.isNotNull(context.getApiResponse())) {
                advancedLog.setStatus(!context.getApiResponse().status);
            }
            advancedLog.setTagDocument(advanceLog.getParsingDump() != 0 ? advanceLog.getParsingDump() : null);
            advancedLog.setUnregister(advanceLog.getUnregisterDocumentUnit() != 0 ? advanceLog.getUnregisterDocumentUnit() : null);
            advancedLog.setUpdateRegisterDocumentCount(advanceLog.getUpdateRegisterDocumentCount() != 0 ? advanceLog.getUpdateRegisterDocumentCount() : null);
            advancedLog.setUpdateTransactionCount(advanceLog.getUpdateTransactionCount() != 0 ? advanceLog.getUpdateTransactionCount() : null);
            advancedLog.setUrl(context.getRequestUri());
            advancedLog.setUsageCounterUpdate(advanceLog.getUsageCounterUpdate() != 0 ? advanceLog.getUsageCounterUpdate() : null);
            advancedLog.setUsageLogUpdate(advanceLog.getUsageLog() != 0 ? advanceLog.getUsageLog() : null);

            try {
//                
                String responseMessage = _helper.getJsonStringFromObjectWithoutNull(advancedLog);

                if (_helper.isNotNullOrEmpty(responseMessage)) {
                    responseMessage = responseMessage.replaceAll("\r\n", "");
                }

                try {
                    IndexResponse advancedLogStatus = client.prepareIndex(elasticSearchAdvancedLogIndex, elasticSearchAdvancedLogType, context.getRequestId().toString())
                            .setSource(responseMessage, XContentType.JSON)
                            .get(TimeValue.timeValueMillis((long) elasticSearchTimeout));

                    if ((!advancedLogStatus.status().equals(RestStatus.CREATED))) {
                        LOGGER.error("Unable to index advanced log into elasticsearch instance - " + advancedLog.getRequestId());
                    }
                } catch (Exception ex) {
                    LOGGER.error("Unable to index advanced log into elasticsearch instance - " + advancedLog.getRequestId());
                }

                return new AsyncResult<>(true);
            } catch (Exception ex) {
                LOGGER.error("Unable to index advanced log data into elasticsearch instance - " + advancedLog.getRequestId() + " - Exception - " + ex.getMessage());
            }
            return new AsyncResult<>(false);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_ADVANCEDLOG_ERROR));
        }
    }

    @Async
    public Future<Boolean> addAuthenticationLogToAMQPAsync(ApiContext context) throws Exception {
        LOGGER.info("Add authentication log for request - " + context.getRequestId());
        if (_helper.isNotNull(context)) {
            CoreAuthenticationlog authLog = new CoreAuthenticationlog();
            authLog.setApi(context.getApi());
            if (context.getClient() != null) {
                authLog.setConsumerKey(context.getClient().getClientKey());
            }
//            coreAuthenticationlog.setApiId(context.getClientApi().getCoreApi().getId());
            authLog.setRequestId(context.getRequestId().toString());
            authLog.setSignature(context.getoAuthHeader().getSignature());
            authLog.setTimestamp(context.getoAuthHeader().getTimeStamp());
            authLog.setNonce(context.getoAuthHeader().getNonce());
            authLog.setCreatedOn(Date.from(Instant.now()));

            CoreAuthenticationLog coreAuthenticationlog = new CoreAuthenticationLog();
            coreAuthenticationlog.setApi(context.getApi());
            if (context.getClient() != null) {
                coreAuthenticationlog.setConsumerKey(context.getClient().getClientKey());
            }
//            coreAuthenticationlog.setApiId(context.getClientApi().getCoreApi().getId());
            coreAuthenticationlog.setSignature(context.getoAuthHeader().getSignature());
            coreAuthenticationlog.setTimestamp(context.getoAuthHeader().getTimeStamp());
            coreAuthenticationlog.setNonce(context.getoAuthHeader().getNonce());
            coreAuthenticationlog.setCreatedOn(Date.from(Instant.now()));

            try {
//                
                String responseMessage = _helper.getJsonStringFromObjectWithoutNull(coreAuthenticationlog);

                if (_helper.isNotNullOrEmpty(responseMessage)) {
                    responseMessage = responseMessage.replaceAll("\r\n", "");
                }

                try {
                    IndexResponse authenticationLogStatus = client.prepareIndex(elasticSearchAuthenticationLogIndex, elasticSearchAuthenticationLogType, context.getRequestId().toString())
                            .setSource(responseMessage, XContentType.JSON)
                            .get(TimeValue.timeValueMillis((long) elasticSearchTimeout));

                    if ((!authenticationLogStatus.status().equals(RestStatus.CREATED))) {
                        LOGGER.error("Unable to index authentication log into elasticsearch instance - " + coreAuthenticationlog.getRequestId());
                        LOGGER.info("Starting store authentication log to database. RequestId - " + coreAuthenticationlog.getRequestId());
                        authLog.setApiId(context.getClientApi().getCoreApi().getId());
                        authLog.setClientId(context.getClient().getId());
                        ApiResponse apiResponse = dataRepositoryService.createAuthenticationlog(authLog);
                        if (!apiResponse.status) {
                            LOGGER.error("Unable to store authentication log. Request Id - " + coreAuthenticationlog.getRequestId());
                        }
                    }
                } catch (Exception ex) {
                    LOGGER.error("Unable to index authentication log into elasticsearch instance - " + coreAuthenticationlog.getRequestId());
                    LOGGER.info("Starting store authentication log to database. RequestId - " + coreAuthenticationlog.getRequestId());
                    authLog.setApiId(context.getClientApi().getCoreApi().getId());
                    authLog.setClientId(context.getClient().getId());
                    authLog.setRequestId(context.getRequestId().toString());
                    ApiResponse apiResponse = dataRepositoryService.createAuthenticationlog(authLog);
                    if (!apiResponse.status) {
                        LOGGER.error("Unable to store authentication log. Request Id - " + coreAuthenticationlog.getRequestId());
                    }
                }

                return new AsyncResult<>(true);
            } catch (Exception ex) {
                LOGGER.error("Unable to index authentication log data into elasticsearch instance - " + coreAuthenticationlog.getRequestId() + " - Exception - " + ex.getMessage());
            }
            return new AsyncResult<>(false);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_AUTHENTICATIONLOG_ERROR));
        }
    }

    @Async
    public Future<Boolean> addUsageLogToAMQPAsync(ApiContext context) throws Exception {
        LOGGER.info("Add usage log for request - " + context.getRequestId());
        try {
            if (_helper.isNotNull(context)) {
                LOGGER.info("Usage log - Api Context - Start time : " + Date.from(Instant.now()));
                com.bgt.lens.model.es.entity.CoreUsagelog esCoreUsageLog = new com.bgt.lens.model.es.entity.CoreUsagelog();
                com.bgt.lens.model.es.entity.CoreUsagelogdata esUsageLogResData = new com.bgt.lens.model.es.entity.CoreUsagelogdata();

                esCoreUsageLog.setApi(context.getApi());
                if (_helper.isNotNull(context.getClient())) {
                    esCoreUsageLog.setConsumerKey(context.getClient().getClientKey());
                }
                esCoreUsageLog.setInTime(_helper.isNotNull(context.getInTime()) ? context.getInTime() : null);
                esCoreUsageLog.setMethod(_helper.isNotNull(context.getMethod()) ? context.getMethod() : null);
//            coreUsageLog.setRequestId(context.getRequestId().toString());
                esCoreUsageLog.setResource(_helper.isNotNull(context.getResource()) ? context.getResource() : null);
                esCoreUsageLog.setInstanceType(_helper.isNotNull(context.getInstanceType()) ? context.getInstanceType() : null);
                esCoreUsageLog.setLocale(_helper.isNotNull(context.getLocale()) ? context.getLocale() : null);
                esCoreUsageLog.setParseVariant(_helper.isNotNull(context.getParseVariant()) ? context.getParseVariant() : null);
                esCoreUsageLog.setUri(_helper.isNotNull(context.getRequestUri()) ? context.getRequestUri() : null);
                esCoreUsageLog.setServerAddress(_helper.isNotNull(context.getServerAddress()) ? context.getServerAddress() : null);
                esCoreUsageLog.setClientAddress(_helper.isNotNull(context.getClientAddress()) ? context.getClientAddress() : null);

                CoreUsagelog dbUsagelog = new CoreUsagelog();
                CoreUsagelog dbUsageLogResponse = new CoreUsagelog();
                dbUsagelog.setCoreUsagelogdata(new CoreUsagelogdata());
                dbUsagelog.setApiId(_helper.isNotNull(context.getClientApi()) && _helper.isNotNull(context.getClientApi().getCoreApi()) ? context.getClientApi().getCoreApi().getId() : null);
                dbUsagelog.setClientId(_helper.isNotNull(context.getClient()) ? context.getClient().getId() : null);
                dbUsagelog.setConsumerKey(_helper.isNotNull(context.getClient()) ? context.getClient().getClientKey() : null);
                dbUsagelog.setHeaders(_helper.isNotNull(context.getRequestHeader()) ? context.getRequestHeader() : null);
//            usagelog.getCoreUsagelogdata().setHeaders(_helper.isNotNull(context.getRequestHeader()) ? context.getRequestHeader() : null);
                dbUsagelog.setInTime(_helper.isNotNull(context.getInTime()) ? context.getInTime() : null);
                dbUsagelog.setMethod(_helper.isNotNull(context.getMethod()) ? context.getMethod() : null);
                dbUsagelog.setRequestId(context.getRequestId().toString());
                dbUsagelog.setResource(_helper.isNotNull(context.getResource()) ? context.getResource() : null);
                dbUsagelog.setInstanceType(_helper.isNotNull(context.getInstanceType()) ? context.getInstanceType() : null);
                dbUsagelog.setLocale(_helper.isNotNull(context.getLocale()) ? context.getLocale() : null);
                dbUsagelog.setParseVariant(_helper.isNotNull(context.getParseVariant()) ? context.getParseVariant() : null);
                dbUsagelog.setUri(_helper.isNotNull(context.getRequestUri()) ? context.getRequestUri() : null);
                dbUsagelog.setServerAddress(_helper.isNotNull(context.getServerAddress()) ? context.getServerAddress() : null);
                dbUsagelog.setClientAddress(_helper.isNotNull(context.getClientAddress()) ? context.getClientAddress() : null);

                if (enableUsageLogData != null && enableUsageLogData) {

                    LOGGER.info("Usage log - Api Context - Serialization - Request - Start time : " + Date.from(Instant.now()));

                    Object requestObj = null;
                    if (context.getParseVariant() != null && context.getParseVariant().contains("htm") && context.getResource().contains("resume")) {
                        requestObj = _resolverMappers.parseResumeWithHTMRequest((ParseResumeRequest) context.getRequestContent());
                    } else if (context.getParseVariant() != null && context.getParseVariant().contains("bgtxml") && context.getParseVariant().length() == 6 && context.getResource().contains("resume")) {
                        requestObj = _resolverMappers.parseResumeBGXMLRequest((ParseResumeRequest) context.getRequestContent());
                    } else if (context.getParseVariant() != null && context.getParseVariant().contains("structuredbgtxml") && context.getResource().contains("resume")) {
                        requestObj = _resolverMappers.parseResumeStructuredBGXMLRequest((ParseResumeRequest) context.getRequestContent());
                    } else if (context.getParseVariant() != null && context.getParseVariant().contains("hrxml") && context.getResource().contains("resume")) {
                        requestObj = _resolverMappers.parseResumeHRXMLRequest((ParseResumeRequest) context.getRequestContent());
                    } else if (context.getParseVariant() != null && context.getParseVariant().contains("rtf") && context.getResource().contains("resume")) {
                        requestObj = _resolverMappers.parseResumeWithRTFRequest((ParseResumeRequest) context.getRequestContent());
                    } else if (context.getResource() != null && context.getResource().contains("resume")) {
                        requestObj = context.getRequestContent();
                    }
                    //set request content
                    if (_helper.isNotNull(context.getRequestContent())) {
                        if (context.getMethod().equalsIgnoreCase(com.bgt.lens.helpers.Enum.HttpMethod.GET.toString()) || context.getMethod().equalsIgnoreCase(com.bgt.lens.helpers.Enum.HttpMethod.OPTIONS.toString()) || context.getMethod().equalsIgnoreCase(com.bgt.lens.helpers.Enum.HttpMethod.DELETE.toString())) {
                            dbUsagelog.setRequestContent(context.getRequestContent().toString());
//                    usagelog.getCoreUsagelogdata().setRequestContent(context.getRequestContent().toString());
                        } else if (context.getContentType() != null && context.getContentType().toLowerCase(Locale.ENGLISH).contains(com.bgt.lens.helpers.Enum.Headers.ApplicationJSON.toString())) {
                            dbUsagelog.setRequestContent(_helper.getJsonStringFromObject(context.getRequestContent()));
//                    usagelog.getCoreUsagelogdata().setRequestContent(_helper.getJsonStringFromObject(context.getRequestContent()));
                        } else {
                            if (!context.getRequestContent().getClass().equals(String.class)) {
                                if (context.getResource().contains("resume")) {
                                    dbUsagelog.setRequestContent(_helper.getXMLStringFromObject(requestObj));
                                } else {
                                    dbUsagelog.setRequestContent(_helper.getXMLStringFromObject(context.getRequestContent()));
                                }
                                //  usagelog.setRequestContent(requestcontent);
                            } else {
//                        usagelog.getCoreUsagelogdata().setRequestContent(context.getRequestContent().toString());
                                dbUsagelog.setRequestContent(context.getRequestContent().toString());
                            }
                        }
                    } else {
//                usagelog.getCoreUsagelogdata().setRequestContent(null);
                        dbUsagelog.setRequestContent(null);
                    }

                    LOGGER.info("Usage log - Api Context - Serialization - Request - End time : " + Date.from(Instant.now()));
                    LOGGER.info("Usage log - Api Context - Serialization - Response - Start time : " + Date.from(Instant.now()));

                    //set response content and raised error
                    if (_helper.isNotNull(context.getApiResponse())) {
                        if (context.getAccept() != null && context.getAccept().toLowerCase(Locale.ENGLISH).contains(com.bgt.lens.helpers.Enum.Headers.ApplicationJSON.toString())) {
//                    usagelog.getCoreUsagelogdata().setResponseContent(_helper.getJsonStringFromObject(context.getApiResponse()));
                            dbUsagelog.setResponseContent(_helper.getJsonStringFromObject(context.getApiResponse()));
                        } else if (_helper.isNotNullOrEmpty(context.getReturnType())) {
                            Object obj = null;
                            switch (context.getReturnType()) {
                                case "InfoResponse":
                                    obj = _resolverMappers.getinfo(context.getApiResponse());
                                    break;
                                case "PingResponse":
                                    obj = _resolverMappers.getPing(context.getApiResponse());
                                    break;
                                case "ConvertResponse":
                                    obj = _resolverMappers.convertBinaryData(context.getApiResponse());
                                    break;
                                case "CanonResponse":
                                    obj = _resolverMappers.canonBinaryData(context.getApiResponse());
                                    break;
                                case "ParseResumeResponse":
                                    obj = _resolverMappers.parseResume(context.getApiResponse());
                                    break;
                                case "ParseResumeBGXMLResponse":
                                    obj = _resolverMappers.parseResumeBGXML(context.getApiResponse());
                                    break;
                                case "ParseResumeStructuredBGXMLResponse":
                                    obj = _resolverMappers.parseResumeStructuredBGXML(context.getApiResponse());
                                    break;
                                case "ParseResumeHRXMLResponse":
                                    obj = _resolverMappers.parseResumeHRXML(context.getApiResponse());
                                    break;
                                case "ParseResumeWithRTFResponse":
                                    obj = _resolverMappers.parseResumeWithRTF(context.getApiResponse());
                                    break;
                                case "ParseResumeWithHTMResponse":
                                    obj = _resolverMappers.parseResumeWithHTM(context.getApiResponse());
                                    break;
                                case "ParseJobResponse":
                                    obj = _resolverMappers.parseJob(context.getApiResponse());
                                    break;
                                case "ParseJobBGXMLResponse":
                                    obj = _resolverMappers.parseJobBGXML(context.getApiResponse());
                                    break;
                                case "ParseJobStructuredBGXMLResponse":
                                    obj = _resolverMappers.parseJobStructuredBGXML(context.getApiResponse());
                                    break;
                                case "ParseJobWithRTFResponse":
                                    obj = _resolverMappers.parseJobWithRFT(context.getApiResponse());
                                    break;
                                case "ParseJobWithHTMResponse":
                                    obj = _resolverMappers.parseJobWithHTM(context.getApiResponse());
                                    break;
                                case "ProcessLocaleResponse":
                                    obj = _resolverMappers.findLocale(context.getApiResponse());
                                    break;
                                case "CreateApiResponse":
                                    obj = _resolverMappers.createApi(context.getApiResponse());
                                    break;
                                case "UpdateApiResponse":
                                    obj = _resolverMappers.updateApi(context.getApiResponse());
                                    break;
                                case "DeleteApiResponse":
                                    obj = _resolverMappers.deleteApi(context.getApiResponse());
                                    break;
                                case "GetApiResponse":
                                    obj = _resolverMappers.getApi(context.getApiResponse());
                                    break;
                                case "GetApiByIdResponse":
                                    obj = _resolverMappers.getApiById(context.getApiResponse());
                                    break;
                                case "AddResourceResponse":
                                    obj = _resolverMappers.addResource(context.getApiResponse());
                                    break;
                                case "UpdateResourceResponse":
                                    obj = _resolverMappers.updateResource(context.getApiResponse());
                                    break;
                                case "DeleteResourceResponse":
                                    obj = _resolverMappers.deleteResource(context.getApiResponse());
                                    break;
                                case "GetResourceResponse":
                                    obj = _resolverMappers.getResource(context.getApiResponse());
                                    break;
                                case "GetResourceByIdResponse":
                                    obj = _resolverMappers.getResourcebyId(context.getApiResponse());
                                    break;
                                case "AddLensSettingsResponse":
                                    obj = _resolverMappers.addLensSettings(context.getApiResponse());
                                    break;
                                case "UpdateLensSettingsResponse":
                                    obj = _resolverMappers.updateLensSettings(context.getApiResponse());
                                    break;
                                case "DeleteLensSettingsResponse":
                                    obj = _resolverMappers.deleteLensSettings(context.getApiResponse());
                                    break;
                                case "GetLensSettingsResponse":
                                    obj = _resolverMappers.getLensSettings(context.getApiResponse());
                                    break;
                                case "GetLensSettingsByIdResponse":
                                    obj = _resolverMappers.getLensSettingsById(context.getApiResponse());
                                    break;
                                case "CreateConsumerResponse":
                                    obj = _resolverMappers.createConsumer(context.getApiResponse());
                                    break;
                                case "UpdateConsumerResponse":
                                    obj = _resolverMappers.updateConsumer(context.getApiResponse());
                                    break;
                                case "DeleteConsumerResponse":
                                    obj = _resolverMappers.deleteConsumer(context.getApiResponse());
                                    break;
                                case "GetClientsResponse":
                                    obj = _resolverMappers.getClients(context.getApiResponse());
                                    break;
                                case "GetClientByIdResponse":
                                    obj = _resolverMappers.getClientById(context.getApiResponse());
                                    break;
                                case "GetAllLicenseDetailsResponse":
                                    obj = _resolverMappers.getLicenseDetails(context.getApiResponse());
                                    break;
                                case "GetLicenseResponse":
                                    obj = _resolverMappers.getLicenseDetailsById(context.getApiResponse());
                                    break;
                                case "GetUsageLogsResponse":
                                    obj = _resolverMappers.getUsageLogs(context.getApiResponse());
                                    break;
                                case "GetUsageLogByIdResponse":
                                    obj = _resolverMappers.getUsageLogsById(context.getApiResponse());
                                    break;
                                case "GetErrorLogsResponse":
                                    obj = _resolverMappers.getErrorLogs(context.getApiResponse());
                                    break;
                                case "GetErrorLogByIdResponse":
                                    obj = _resolverMappers.getErrorLogsById(context.getApiResponse());
                                    break;
                                case "GetUsageCounterResponse":
                                    obj = _resolverMappers.getUsageCounter(context.getApiResponse());
                                    break;
                                case "GetUsageCounterByIdResponse":
                                    obj = _resolverMappers.getUsageCounterById(context.getApiResponse());
                                    break;
                            }

                            dbUsagelog.setResponseContent(_helper.getXMLStringFromObject(obj));
//                    usagelog.setResponseContent(_helper.getXMLStringFromObjectForSOAPResponse(context.getReturnType(), context.getApiResponse()));
                        } else {
                            dbUsagelog.setResponseContent(_helper.getXMLStringFromObject(context.getApiResponse()));
//                    usagelog.setResponseContent(_helper.getXMLStringFromObject(context.getApiResponse()));
                        }

                        if (!context.getApiResponse().status) {
                            esCoreUsageLog.setRaisedError(true);
                            dbUsagelog.setRaisedError(true);
                        }
                    } else {
//                usagelog.getCoreUsagelogdata().setResponseContent(null);
                        dbUsagelog.setResponseContent(null);
                        esCoreUsageLog.setRaisedError(true);
                        dbUsagelog.setRaisedError(true);
                    }

//            usagelog.setRequestContent(null);
//            usagelog.setResponseContent(null);
//            usagelog.setHeaders(null);
                    dbUsagelog.setCoreUsagelogdata(null);

                    esUsageLogResData.setApi(context.getApi());
                    if (_helper.isNotNull(context.getClient())) {
                        esUsageLogResData.setConsumerKey(context.getClient().getClientKey());
                    }
                    esUsageLogResData.setRequestContent(dbUsagelog.getRequestContent());
                    esUsageLogResData.setResponseContent(dbUsagelog.getResponseContent());
                    esUsageLogResData.setHeaders(dbUsagelog.getHeaders());
                    esUsageLogResData.setInTime(esCoreUsageLog.getInTime());

                    dbUsageLogResponse.setRequestContent(dbUsagelog.getRequestContent());
                    dbUsageLogResponse.setResponseContent(dbUsagelog.getResponseContent());
                    dbUsageLogResponse.setHeaders(dbUsagelog.getHeaders());
                    dbUsageLogResponse.setRequestId(dbUsagelog.getRequestId());
//            usageLogResponse.setInTime(usagelog.getInTime());

                    LOGGER.info("Usage log - Api Context - Serialization - Response -  End time : " + Date.from(Instant.now()));
                } else {
                    // Diabled UsageLogData. So only set the RaisedError field.
                    if (_helper.isNotNull(context.getApiResponse())) {
                        if (!context.getApiResponse().status) {
                            esCoreUsageLog.setRaisedError(true);
                            dbUsagelog.setRaisedError(true);
                        }
                    } else {
                        dbUsagelog.setResponseContent(null);
                        esCoreUsageLog.setRaisedError(true);
                        dbUsagelog.setRaisedError(true);
                    }

                    dbUsagelog.setCoreUsagelogdata(null);
                }

//                Date now = Date.from(Instant.now());
//                coreUsageLog.setOutTime(now);
//                usagelog.setOutTime(now);
                esCoreUsageLog.setOutTime(context.getOutTime());
                dbUsagelog.setOutTime(context.getOutTime());

//            usageLogResponse.setOutTime(usagelog.getOutTime());
                dbUsagelog.setRequestContent(null);
                dbUsagelog.setResponseContent(null);
                dbUsagelog.setHeaders(null);
                esUsageLogResData.setOutTime(esCoreUsageLog.getOutTime());

                // Set milliseconds
                if (_helper.isNotNull(context.getInTime())) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(context.getOutTime());
                    long milliseconds1 = cal.getTimeInMillis();
                    cal.setTime(context.getInTime());
                    long milliseconds2 = cal.getTimeInMillis();
                    long diff = milliseconds1 - milliseconds2;
                    dbUsagelog.setMilliseconds(diff);
                    esCoreUsageLog.setElapsedTime(diff);
                } else {
                    dbUsagelog.setMilliseconds((long) 0);
                    esCoreUsageLog.setElapsedTime((long) 0);
                }

                LOGGER.info("Usage log - Api Context - End time : " + Date.from(Instant.now()));

                try {
                    String responseMessage = _helper.getJsonStringFromObjectWithoutNull(esUsageLogResData);
                    String message = _helper.getJsonStringFromObjectWithoutNull(esCoreUsageLog);

                    if (_helper.isNotNullOrEmpty(message)) {
                        message = message.replaceAll("\r\n", "");
                    }

                    if (_helper.isNotNullOrEmpty(responseMessage)) {
                        responseMessage = responseMessage.replaceAll("\r\n", "");
                    }

                    try {
                        if (enableUsageLogData != null && enableUsageLogData) {
                            // Store both UsageLog Metric and UsageLog Data
                            String usageLogIndex = elasticSearchUsageLogIndex;
                            String usageLogDataIndex = elasticSearchUsageLogDataIndex;
                            if (enableAutoDateBasedUsageLog) {
                                DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                                usageLogIndex = usageLogIndex + "_" + formatter.format(Date.from(Instant.now()));
                            }
                            if (enableAutoDateBasedUsageLogData) {
                                DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                                usageLogDataIndex = usageLogDataIndex + "_" + formatter.format(Date.from(Instant.now()));
                            }

                            IndexResponse usageLogStatus = client.prepareIndex(usageLogIndex, elasticSearchUsageLogType, context.getRequestId().toString())
                                    .setSource(message, XContentType.JSON)
                                    .get(TimeValue.timeValueMillis((long) elasticSearchTimeout));

                            IndexResponse usageLogDataStatus = client.prepareIndex(usageLogDataIndex, elasticSearchUsageLogDataType, context.getRequestId().toString())
                                    .setSource(responseMessage, XContentType.JSON)
                                    .get(TimeValue.timeValueMillis((long) elasticSearchTimeout));

                            if ((!usageLogStatus.status().equals(RestStatus.CREATED)) || (!usageLogDataStatus.status().equals(RestStatus.CREATED))) {
                                LOGGER.error("Unable to index usage log or data into elasticsearch instance - " + dbUsagelog.getRequestId());

                                DeleteResponse responseUsageLog = client.prepareDelete(usageLogIndex, elasticSearchUsageLogType, dbUsagelog.getRequestId())
                                        .get(TimeValue.timeValueMillis((long) elasticSearchTimeout));
                                DeleteResponse responseUsageLogData = client.prepareDelete(usageLogDataIndex, elasticSearchUsageLogDataType, dbUsagelog.getRequestId())
                                        .get(TimeValue.timeValueMillis((long) elasticSearchTimeout));

                                if (!responseUsageLog.status().equals(RestStatus.OK) && !responseUsageLogData.status().equals(RestStatus.OK)) {
                                    LOGGER.error("Unable to delete usage log or data from elasticsearch instance - " + dbUsagelog.getRequestId());
                                } else {
                                    LOGGER.info("Starting store usagelog to database. RequestId - " + dbUsagelog.getRequestId());
                                    if (enableUsageLogData != null && enableUsageLogData) {
                                        CoreUsagelogdata usageLogData = new CoreUsagelogdata();
                                        usageLogData.setRequestContent(dbUsageLogResponse.getRequestContent());
                                        usageLogData.setResponseContent(dbUsageLogResponse.getResponseContent());
                                        usageLogData.setHeaders(dbUsageLogResponse.getHeaders());
                                        dbUsagelog.setCoreUsagelogdata(usageLogData);
                                    }
                                    ApiResponse apiResponse = dataRepositoryService.createUsagelog(dbUsagelog);
                                    if (!apiResponse.status) {
                                        LOGGER.error("Unable to store usage log. Request Id - " + dbUsagelog.getRequestId());
                                    }
                                }
                            }
                        } else {
                            // Only Store UsageLog Metrics
                            String usageLogIndex = elasticSearchUsageLogIndex;
                            if (enableAutoDateBasedUsageLog) {
                                DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                                usageLogIndex = usageLogIndex + "_" + formatter.format(Date.from(Instant.now()));
                            }

                            IndexResponse usageLogStatus = client.prepareIndex(usageLogIndex, elasticSearchUsageLogType, context.getRequestId().toString())
                                    .setSource(message, XContentType.JSON)
                                    .get(TimeValue.timeValueMillis((long) elasticSearchTimeout));

                            if (!usageLogStatus.status().equals(RestStatus.CREATED)) {
                                LOGGER.error("Unable to index usage log into elasticsearch instance - " + dbUsagelog.getRequestId());

                                DeleteResponse responseUsageLog = client.prepareDelete(usageLogIndex, elasticSearchUsageLogType, dbUsagelog.getRequestId()).get(TimeValue.timeValueMillis((long) elasticSearchTimeout));

                                if (!responseUsageLog.status().equals(RestStatus.OK)) {
                                    LOGGER.error("Unable to delete usage log from elasticsearch instance - " + dbUsagelog.getRequestId());
                                } else {
                                    LOGGER.info("Starting store usagelog to database. RequestId - " + dbUsagelog.getRequestId());
                                    if (enableUsageLogData != null && enableUsageLogData) {
                                        CoreUsagelogdata usageLogData = new CoreUsagelogdata();
                                        usageLogData.setRequestContent(dbUsageLogResponse.getRequestContent());
                                        usageLogData.setResponseContent(dbUsageLogResponse.getResponseContent());
                                        usageLogData.setHeaders(dbUsageLogResponse.getHeaders());
                                        dbUsagelog.setCoreUsagelogdata(usageLogData);
                                    }
                                    ApiResponse apiResponse = dataRepositoryService.createUsagelog(dbUsagelog);
                                    if (!apiResponse.status) {
                                        LOGGER.error("Unable to store usage log. Request Id - " + dbUsagelog.getRequestId());
                                    }
                                }
                            }
                        }
                    } catch (Exception ex) {
                        LOGGER.error("Unable to index usage log data into elasticsearch instance - " + dbUsagelog.getRequestId() + " - Exception - " + ex.getMessage());
                        LOGGER.info("Starting store usagelog to database. RequestId - " + dbUsagelog.getRequestId());
                        if (enableUsageLogData != null && enableUsageLogData) {
                            CoreUsagelogdata usageLogData = new CoreUsagelogdata();
                            usageLogData.setRequestContent(dbUsageLogResponse.getRequestContent());
                            usageLogData.setResponseContent(dbUsageLogResponse.getResponseContent());
                            usageLogData.setHeaders(dbUsageLogResponse.getHeaders());
                            dbUsagelog.setCoreUsagelogdata(usageLogData);
                        }
                        ApiResponse response = dataRepositoryService.createUsagelog(dbUsagelog);
                        if (!response.status) {
                            LOGGER.error("Unable to store usage log. Request Id - " + dbUsagelog.getRequestId());
                        }
                    }
                    return new AsyncResult<>(true);
                } catch (Exception ex) {
                    LOGGER.error("Unable to index usage log data into elasticsearch instance - " + dbUsagelog.getRequestId() + " - Exception - " + ex.getMessage());
                }
                return new AsyncResult<>(false);
            } else {
                throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_USAGELOG_ERROR));
            }
        } catch (Exception ex) {
            LOGGER.error("Unable to index usage log data" + " - Exception - " + ex.getMessage());
        }
        return new AsyncResult<>(false);
    }
}
