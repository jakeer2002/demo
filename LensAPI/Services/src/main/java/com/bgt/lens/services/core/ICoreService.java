// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.core;

import com.bgt.lens.model.adminservice.request.AddLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.CreateApiRequest;
import com.bgt.lens.model.adminservice.request.CreateConsumerRequest;
import com.bgt.lens.model.adminservice.request.GetApiRequest;
import com.bgt.lens.model.adminservice.request.GetClientsRequest;
import com.bgt.lens.model.adminservice.request.GetLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.GetResourceRequest;
import com.bgt.lens.model.adminservice.request.UpdateApiRequest;
import com.bgt.lens.model.adminservice.request.UpdateConsumerRequest;
import com.bgt.lens.model.adminservice.request.UpdateLensSettingsRequest;
import com.bgt.lens.model.adminservice.request.UpdateResourceRequest;
import com.bgt.lens.model.adminservice.request.AddResourceRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import java.util.List;

/**
 * *
 * Core service Interface
 */
public interface ICoreService {

    /**
     * Get the list of clients
     *
     * @param clients
     * @return List of Users
     * @throws Exception
     */
    public ApiResponse getClients(GetClientsRequest clients) throws Exception;

    /**
     * Get the list of Apis
     *
     * @param api
     * @return List of API's
     * @throws Exception
     */
    public ApiResponse getApi(GetApiRequest api) throws Exception;

    /**
     * Get the list of clients
     *
     * @param clientId
     * @return List of Users
     * @throws Exception
     */
    public ApiResponse getClientById(int clientId) throws Exception;

    /**
     * Get the list of Apis
     *
     * @param apiId
     * @return API
     * @throws Exception
     */
    public ApiResponse getApiById(int apiId) throws Exception;

    /**
     * Create new client
     *
     * @param consumer
     * @return Client
     * @throws Exception
     */
    public ApiResponse createConsumer(CreateConsumerRequest consumer) throws Exception;

    /**
     * Update user
     *
     * @param clientId
     * @param consumer
     * @return Client
     * @throws Exception
     */
    public ApiResponse updateConsumer(int clientId, UpdateConsumerRequest consumer) throws Exception;
    
    /**
     * Update user
     *
     * @param clientId
     * @param consumer
     * @return Client
     * @throws Exception
     */
    public ApiResponse updateConsumer(int clientId, List<UpdateConsumerRequest> consumer) throws Exception;
    
    /**
     * Delete a client
     *
     * @param clientId
     * @return Client Id
     * @throws Exception
     */
    public ApiResponse deleteEntireConsumerDetails(int clientId) throws Exception;

    /**
     * Delete a client
     *
     * @param clientId
     * @return Client Id
     * @throws Exception
     */
    public ApiResponse deleteConsumer(int clientId) throws Exception;

    /**
     * Add new Lens instance
     *
     * @param settings
     * @return Lens settings
     * @throws Exception
     */
    public ApiResponse addLensSettings(AddLensSettingsRequest settings) throws Exception;

    /**
     * Update existing Lens instance
     *
     * @param SettingsId
     * @param settings
     * @return Lens settings
     * @throws Exception
     */
    public ApiResponse updateLensSettings(int SettingsId, UpdateLensSettingsRequest settings) throws Exception;

    /**
     * Delete Lens instance
     *
     * @param instanceId
     * @return Lens settings id
     * @throws Exception
     */
    public ApiResponse deleteLensSettings(int instanceId) throws Exception;

    /**
     * Get the list of Apis
     *
     * @param request
     * @return List of Lens settings
     * @throws Exception
     */
    public ApiResponse getLensSettings(GetLensSettingsRequest request) throws Exception;

    /**
     * Get the list of clients
     *
     * @param settingsId
     * @return Lens settings
     * @throws Exception
     */
    public ApiResponse getLensSettingsById(int settingsId) throws Exception;

    /**
     * Creates new API
     *
     * @param api
     * @return API
     * @throws Exception
     */
    public ApiResponse createApi(CreateApiRequest api) throws Exception;

    /**
     * Update and API
     *
     * @param apiId
     * @param api
     * @return API
     * @throws Exception
     */
    public ApiResponse updateApi(int apiId, UpdateApiRequest api) throws Exception;

    /**
     * Delete an API
     *
     * @param apiId
     * @return API Id
     * @throws Exception
     */
    public ApiResponse deleteApi(int apiId) throws Exception;

    /**
     * Add new resource
     *
     * @param resource
     * @return resource
     * @throws Exception
     */
    public ApiResponse addResource(AddResourceRequest resource) throws Exception;

    /**
     * Update existing resource
     *
     * @param ResourceId
     * @param resource
     * @return resource
     * @throws Exception
     */
    public ApiResponse updateResource(int ResourceId, UpdateResourceRequest resource) throws Exception;

    /**
     * Delete resource
     *
     * @param resourceId
     * @return resource
     * @throws Exception
     */
    public ApiResponse deleteResource(int resourceId) throws Exception;

    /**
     * Get the list of Apis
     *
     * @param request
     * @return List of Resources
     * @throws Exception
     */
    public ApiResponse getResource(GetResourceRequest request) throws Exception;

    /**
     * Get the list of resources
     *
     * @param resourceId
     * @return Resources
     * @throws Exception
     */
    public ApiResponse getResourceById(int resourceId) throws Exception;
}
