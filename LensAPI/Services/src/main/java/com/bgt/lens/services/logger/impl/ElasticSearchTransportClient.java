// <editor-fold defaultstate="collapsed" desc="Copyright © 2016 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.logger.impl;

import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Helper;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.http.HttpStatus;

/**
 * Elasticsearch transport client
 */
public class ElasticSearchTransportClient {

    private final Helper _helper = new Helper();

    /**
     * Elasticsearch client
     */
    Client client = null;

    public ElasticSearchTransportClient(String elasticSearchNodeList,String elasticSearchPingTimeout,String elasticSearchClusterName) throws UnknownHostException {
        Settings settings = Settings.builder()
                .put("client.transport.ping_timeout", elasticSearchPingTimeout)
                .put("client.transport.sniff", true)
                .put("cluster.name", elasticSearchClusterName).build();
        TransportClient transportClient = new PreBuiltTransportClient(settings);
        if (_helper.isNotNullOrEmpty(elasticSearchNodeList)) {
            List<String> nodeList = new ArrayList<>();
            nodeList = Arrays.asList(elasticSearchNodeList.split(","));

            if (nodeList != null && nodeList.size() > 0) {
                for (String node : nodeList) {
                    List<String> hostPort = new ArrayList<>();
                    hostPort = Arrays.asList(node.split(":"));
                    if (hostPort != null && hostPort.size() >= 2) {
                        transportClient.addTransportAddresses(new TransportAddress(InetAddress.getByName(hostPort.get(0).trim()), Integer.valueOf(hostPort.get(1).trim())));
                    } else {
                        throw new ApiException("", "Invalid elasticsearch node list specified. Elastic search nodes should be specified like host:port,host:port", HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
            }
        } else {
            throw new ApiException("", "Elasticsearch node list is empty", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        client = transportClient;
    }

    /**
     * Get Elasticsearch Client
     *
     * @return
     */
    public Client getClient() {
        return client;
    }

    /**
     * Set Elasticsearch Client
     *
     * @param client
     */
    public void setClient(Client client) {
        this.client = client;
    }
}
