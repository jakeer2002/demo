// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.logger.ILoggerService;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * *
 * Usage counter aspect
 */
@Aspect
@Order(3)
public class UsageCounterAspect {

    /**
     * *
     * Logger service
     */
    private ILoggerService loggerService = null;

    /**
     * *
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(UsageCounterAspect.class);

    /**
     * *
     * Set logger service
     *
     * @param loggerService
     */
    public void setLoggerService(ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    /**
     * *
     * Update usage counter
     *
     * @param joinPoint
     * @param result
     * @throws Exception
     */
    @AfterReturning(
            pointcut = "execution(* com.bgt.lens.web.rest.CoreController.*(..)) || execution(* com.bgt.lens.web.rest.UtilityController.*(..))",
            returning = "result")
    public void updateUsageCounter(JoinPoint joinPoint, Object result) throws Exception {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");

            loggerService.updateUsageCounter(apiContext);
//            if (!response.status) {
//                LOGGER.error("Unable to update usage counter");
//            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Update usage counter. Exception - " + ex.getMessage());
        }
    }

    /**
     * *
     * Update usage counter
     *
     * @param joinPoint
     * @param result
     * @throws Exception
     */
    @AfterReturning(
            pointcut = "execution(* com.bgt.lens.web.soap.SoapEndPoint.*(..))",
            returning = "result")
    public void updateUsageCounterSoap(JoinPoint joinPoint, Object result) throws Exception {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");
            loggerService.updateUsageCounter(apiContext);
//            if (!response.status) {
//                LOGGER.error("Unable to update usage counter");
//            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Update usage counter. Exception - " + ex.getMessage());
        }
    }
}
