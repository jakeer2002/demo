// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.repositories;

import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import java.util.List;
import java.util.Set;

/**
 *
 * Lens repository interface
 */
public interface ILensRepository {

    /**
     * Get Info
     *
     * @param coreLenssettings
     * @return
     * @throws Exception
     */
    ApiResponse info(CoreLenssettings coreLenssettings) throws Exception;

    /**
     * Ping Lens
     *
     * @param coreLenssettings
     * @return
     * @throws Exception
     */
    ApiResponse ping(CoreLenssettings coreLenssettings) throws Exception;

    /**
     * Convert binary data
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @return
     * @throws Exception
     */
    ApiResponse convertBinaryData(CoreLenssettings coreLenssettings, byte[] binaryData, String extension) throws Exception;

    /**
     * Tag binary data
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @param customSkillCode
     * @return
     * @throws Exception
     */
    ApiResponse tagBinaryData(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) throws Exception;

    
    /**
     * Tag binary data and get HRXML
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @param customSkillCode
     * @return
     * @throws Exception
     */
    ApiResponse tagBinaryStructuredBGTXMLData(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) throws Exception;

    /**
     * Tag binary data and get HRXML
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @param customSkillCode
     * @return
     * @throws Exception
     */
    ApiResponse tagBinaryHRXMLData(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) throws Exception;

    ApiResponse tagBinaryHRXMLDataBold(CoreLenssettings coreLenssettings, ApiResponse response) throws Exception;

    /**
     * Tag binary data with RTF
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @param customSkillCode
     * @return
     * @throws Exception
     */
    ApiResponse tagBinaryDataWithRTF(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) throws Exception;

    /**
     * Tag binary data with HTM
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @param customSkillCode
     * @return
     * @throws Exception
     */
    ApiResponse tagBinaryDataWithHTM(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) throws Exception;

    /**
     * Send XML command
     *
     * @param coreLenssettings
     * @param xmlCommand
     * @return
     * @throws Exception
     */
    ApiResponse sendXMLCommand(CoreLenssettings coreLenssettings, String xmlCommand) throws Exception;

    /**
     * To find the locale of the given document
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @return
     * @throws Exception
     */
    ApiResponse findLocale(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType) throws Exception;

    /**
     * Locale Detector - Tag binary data
     *
     * @param lenssettingses
     * @param binaryData
     * @param extension
     * @param docType
     * @param defaultLocale
     * @return
     * @throws Exception
     */
    ApiResponse ldTagBinaryData(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception;

    /**
     * Locale Detector - Tag binary data
     *
     * @param lenssettingses
     * @param binaryData
     * @param extension
     * @param docType
     * @param defaultLocale
     * @return
     * @throws Exception
     */
    ApiResponse ldTagBinaryStructuredBGTXMLData(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception;

    
    /**
     * Locale Detector - Tag binary data and get HRXML
     *
     * @param lenssettingses
     * @param binaryData
     * @param extension
     * @param docType
     * @param defaultLocale
     * @return
     * @throws Exception
     */
    ApiResponse ldTagBinaryHRXMLData(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception;

    /**
     * Locale Detector - Tag binary data with RTF
     *
     * @param lenssettingses
     * @param binaryData
     * @param extension
     * @param docType
     * @param defaultLocale
     * @return
     * @throws Exception
     */
    ApiResponse ldTagBinaryDataWithRTF(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception;

    /**
     * Locale Detector - Tag binary data with HTM
     *
     * @param lenssettingses
     * @param binaryData
     * @param extension
     * @param docType
     * @param defaultLocale
     * @return
     * @throws Exception
     */
    ApiResponse ldTagBinaryDataWithHTM(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception;
    
//    /**
//     * Create vendor
//     * @param coreLenssettings
//     * @param vendorName
//     * @param docType
//     * @param docServer
//     * @param hypercubeServer
//     * @return
//     * @throws Exception 
//     */
//    public ApiResponse createVendor(CoreLenssettings coreLenssettings, String vendorName, char docType, String docServer, String hypercubeServer) throws Exception;
//
//    /**
//     * Open vendor
//     * @param coreLenssettings
//     * @param vendorName
//     * @param docType
//     * @return
//     * @throws Exception 
//     */
//    public ApiResponse openVendor(CoreLenssettings coreLenssettings, String vendorName, char docType) throws Exception;
//
//    /**
//     * Close vendor
//     * @param coreLenssettings
//     * @param vendorName
//     * @param docType
//     * @return
//     * @throws Exception 
//     */
//    public ApiResponse closeVendor(CoreLenssettings coreLenssettings, String vendorName, char docType) throws Exception;
        
    /**
     * Get the details of active doc server and hypercube server list
     * @param coreLenssettings
     * @param docType
     * @return
     * @throws Exception 
     */
    ApiResponse getLensServiceList(CoreLenssettings coreLenssettings, String docType) throws Exception;
    
    /**
     * Register resume
     * @param lensSettings
     * @param binaryData
     * @param extension
     * @param type
     * @param docType
     * @param vendorName
     * @param docId
     * @return
     * @throws Exception 
     */
    ApiResponse registerDocument(CoreLenssettings lensSettings, byte[] binaryData, String extension, String vendorName, String docType, char type, String docId, String customSkillCode) throws Exception;
    
    /**
     * Unregister document
     * @param lensSettings
     * @param vendorName
     * @param docType
     * @param type
     * @param docId
     * @return
     * @throws Exception 
     */
    ApiResponse unregisterDocument(CoreLenssettings lensSettings, String vendorName, String docType, char type, String docId) throws Exception;
}
