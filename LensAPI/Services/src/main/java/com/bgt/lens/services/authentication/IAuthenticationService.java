// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.authentication;

import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.rest.response.ApiResponse;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * *
 * Authentication service interface
 */
public interface IAuthenticationService {

    /**
     * *
     * Validate user
     *
     * @param request
     * @return
     * @throws Exception
     */
    public ApiResponse validateApiContext(HttpServletRequest request) throws Exception;

    /**
     * *
     * Get client details
     *
     * @param client
     * @return
     * @throws Exception
     */
    public CoreClient getClientDetails(String client) throws Exception;

    /**
     * *
     * Get client Api details by client
     *
     * @param oAuthtoken
     * @param coreClient
     * @return
     */
    public CoreClientapi getClientApiDetails(String oAuthtoken, CoreClient coreClient);

    /**
     * *
     * Validate authentication related data from HTTP request
     *
     * @param authHeader
     * @throws java.lang.Exception
     */
    public void validateOAuthHeader(Map<String, String> authHeader) throws Exception;
    /**
     * *
     * Validate authentication related data from HTTP request
     *
     * @param authHeader
     * @throws java.lang.Exception
     */
    public void validateBasicAuthHeader(Map<String, String> authHeader) throws Exception;

}
