// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.authentication.impl;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.authentication.AuthorizationHeaderParser;
import com.bgt.lens.authentication.BasicAuthHeader;
import com.bgt.lens.authentication.OAuth;
import com.bgt.lens.authentication.OAuthHeader;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ResourceCriteria;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreResources;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.authentication.IAuthenticationService;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.http.HttpStatus;

/**
 *
 * Implementation of Authentication service
 */
public class AuthenticationServiceImpl implements IAuthenticationService {

    private static final Logger LOGGER = LogManager.getLogger(AuthenticationServiceImpl.class);
    private final Helper _helper = new Helper();

    private IDataRepositoryService dataRepositoryService;
    private ApiConfiguration apiConfig;

    /**
     * Initialize data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dataRepositoryService = dataRepositoryService;
    }

    /**
     * *
     * Set ApiConfig
     *
     * @param apiConfig
     */
    public void setapiConfig(ApiConfiguration apiConfig) {
        this.apiConfig = apiConfig;
    }

    /**
     * Validate client
     *
     * @param request
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("empty-statement")
    public ApiResponse validateApiContext(HttpServletRequest request) throws Exception {
        LOGGER.info("Validate user");
        ApiContext apiContext = _helper.getApiContext();
        ApiResponse apiResponse = new ApiResponse();
        String consumer_key;

        // Check authentication type
        try {
            LOGGER.info("API Resource :" + apiContext.getResource());
            // Validate Request Authentication

            if (_helper.isNotNull(request)) {

                String authorizationHeader = request.getHeader("authorization");
                
                if (_helper.isNullOrEmpty(authorizationHeader)) {
                    LOGGER.error("Authentication header not found - "+ apiContext.getRequestId());
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.AUTHENTICATION_HEADER_NOT_FOUND), HttpStatus.BAD_REQUEST);                    
                }
                
                AuthorizationHeaderParser authorizationHeaderParser = new AuthorizationHeaderParser();
	            consumer_key = authorizationHeaderParser.parse(authorizationHeader);

            } else {
                LOGGER.error("Invalid HTTP request - "+ apiContext.getRequestId());
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_HTTP_REQUEST), HttpStatus.BAD_REQUEST);
            }
            

                // Get Client details from Database service
                LOGGER.info("Get user details from database");
				CoreClient coreClient = getClientDetails(consumer_key);
                
                apiContext.setClient(coreClient);
                apiContext.setRequestHeader(getHeadersInfo(request));
                _helper.setApiContext(apiContext);

                // Get CoreClientapi resource
                LOGGER.info("Get client API details");
                CoreClientapi coreClientapi = getClientApiDetails("Utility", coreClient);

                // Check that the client is allowed to use the api	
                LOGGER.info("Validate resource");
                LOGGER.info("Resource - " + apiContext.getResource());

                // LensSettings Criteria
                ResourceCriteria resourceCriteria = new ResourceCriteria();
                resourceCriteria.setApi(coreClientapi.getCoreApi());
                resourceCriteria.setCoreClient(coreClient);
                resourceCriteria.setResourceName(apiContext.getResource());
                // Fetch LensSettings
                List<CoreResources> coreResourceList = dataRepositoryService.getResourceWithoutApiByCriteria(resourceCriteria);
                
                if (_helper.isNull(coreResourceList) || coreResourceList.size() <= 0) {
                    LOGGER.error("Not authorized to use API - "+ apiContext.getRequestId());
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.UNAUTHORIZED_RESOURCE_ACCESS), HttpStatus.UNAUTHORIZED);
                }

                // Check that the client enabled status
                LOGGER.info("Validate API access");
                if (!coreClientapi.isEnabled()) {
                    LOGGER.error("Client account has been disabled - "+ apiContext.getRequestId());
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INACTIVE_CLIENT), HttpStatus.UNAUTHORIZED);
                }

                // Check that the client activationdate
                Date today = new LocalDate().toDateTimeAtStartOfDay().toDate();
                Date activationDate = new LocalDate(coreClientapi.getActivationDate()).toDateTimeAtStartOfDay().toDate();
                if (activationDate.after(today)) {
                    DateTimeFormatter df = _helper.getDateFormat();
                    String activationDateMessage = "License will be activated on " + new LocalDate(coreClientapi.getActivationDate()).toString(df);
                    LOGGER.error("License not activated - "+ apiContext.getRequestId());
                    throw new ApiException("", activationDateMessage, HttpStatus.UNAUTHORIZED);
                }

                // Check that the client remainingTransactions,expiryDate
                Date expiryDate = new LocalDate(coreClientapi.getExpiryDate()).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate();
                apiContext.setClientApi(coreClientapi);
                _helper.setApiContext(apiContext);
                if (!((coreClientapi.getRemainingTransactions() > 0) && expiryDate.after(today))) {
                    LOGGER.error("License expired - "+ apiContext.getRequestId());
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.LICENSE_EXPIRED) + ". Expiry Date: " + new LocalDate(expiryDate).toString()+ ", Remaining Transactions: " + coreClientapi.getRemainingTransactions(), HttpStatus.UNAUTHORIZED);
                }

                String httpMethod = request.getMethod();

                apiContext.setClient(coreClient);
                apiContext.setClientApi(coreClientapi);
                apiContext.setRequestHeader(getHeadersInfo(request));
                apiResponse.status = true;
            // </editor-fold>

        } catch (ApiException | NoSuchElementException ex) {
            if (ex instanceof NoSuchElementException) {
                LOGGER.error("Invalid Authentication Header: "+request.getHeader("authorization"));
                apiContext.setApiResponse(apiResponse);
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_AUTHENTICATION_HEADER), HttpStatus.BAD_REQUEST);
            } else {
                apiContext.setApiResponse(apiResponse);
                request.setAttribute("ApiContext", apiContext);
            }
            throw ex;
        }
        // </editor-fold>

        apiContext.setApiResponse(apiResponse);
        request.setAttribute("ApiContext", apiContext);
        return apiResponse;
    }

    /**
     * Get request data
     *
     * @param authHeaderMap
     * @throws Exception
     */
    @Override
    public void validateOAuthHeader(Map<String, String> authHeaderMap) throws Exception {

        try {
            if (_helper.isNotNull(authHeaderMap)) {

                // check Authorization Scheme
                if (_helper.isNullOrEmpty(authHeaderMap.get("scheme"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.AUTHENTICATION_SCHEME_NOT_FOUND), HttpStatus.BAD_REQUEST);
                }

                // check Authorization Scheme is OAuth
                if (!"OAuth".equalsIgnoreCase(authHeaderMap.get("scheme"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_AUTHENTICATION_SCHEME), HttpStatus.BAD_REQUEST);
                }

                // check Authorization Method with implemented methods
                if (!"PLAINTEXT".equals(authHeaderMap.get("oauth_signature_method")) && !"HMAC-SHA1".equals(authHeaderMap.get("oauth_signature_method"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_SIGNATURE_METHOD), HttpStatus.BAD_REQUEST);
                }

                // check OAuthSignatureKey
                if (_helper.isNullOrEmpty(authHeaderMap.get("oauth_signature"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_SIGNATURE), HttpStatus.BAD_REQUEST);
                }

                // check OAuthConsumerKey
                if (_helper.isNullOrEmpty(authHeaderMap.get("oauth_consumer_key"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
                }

                // check OAuthTokenKey
                if (_helper.isNullOrEmpty(authHeaderMap.get("oauth_token"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_ACCESS_TOKEN), HttpStatus.BAD_REQUEST);
                }

                // check OAuthTimestamp
                if (_helper.isNullOrEmpty(authHeaderMap.get("oauth_timestamp"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_AUTHENTICATION_TIMESTAMP), HttpStatus.BAD_REQUEST);
                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_AUTHENTICATION_HEADER), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            if (ex instanceof NoSuchElementException) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_AUTHENTICATION_HEADER), HttpStatus.BAD_REQUEST);
            }
            throw ex;
        }
    }

    /**
     * Get request data
     *
     * @param authHeaderMap
     * @throws Exception
     */
    @Override
    public void validateBasicAuthHeader(Map<String, String> authHeaderMap) throws Exception {

        try {
            if (_helper.isNotNull(authHeaderMap)) {

                // check consumer key
                if (_helper.isNullOrEmpty(authHeaderMap.get("consumer_key"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_KEY), HttpStatus.BAD_REQUEST);
                }

                // check consumer secret
                if (_helper.isNullOrEmpty(authHeaderMap.get("consumer_secret"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_CONSUMER_SECRET), HttpStatus.BAD_REQUEST);
                }

                // check access token
                if (_helper.isNullOrEmpty(authHeaderMap.get("access_token"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_ACCESS_TOKEN), HttpStatus.BAD_REQUEST);
                }

                // check access token secret
                if (_helper.isNullOrEmpty(authHeaderMap.get("access_token_secret"))) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.EMPTY_ACCESS_TOKEN_SECRET), HttpStatus.BAD_REQUEST);
                }

            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_AUTHENTICATION_HEADER), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            if (ex instanceof NoSuchElementException) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_AUTHENTICATION_HEADER), HttpStatus.BAD_REQUEST);
            }
            throw ex;
        }
    }

    /**
     * Get client details from database
     *
     * @param consumerKey
     * @return
     * @throws Exception
     */
    @Override
    public CoreClient getClientDetails(String consumerKey) throws Exception {
        try {
            ClientCriteria clientCriteria = new ClientCriteria();
            clientCriteria.setClientKey(consumerKey);

            List<CoreClient> coreClientList = dataRepositoryService.getClientByCriteria(clientCriteria);

            if (coreClientList == null || coreClientList.size() < 0) {
            	LOGGER.error("Client Not Found");
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.CLIENT_NOT_EXIST), HttpStatus.NOT_FOUND);
            }

            return coreClientList.get(0);
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Get API details of the client
     *
     * @param oAuthtoken
     * @param coreClient
     * @return
     * @throws ApiException
     */
    @Override
    public CoreClientapi getClientApiDetails(String oAuthtoken, CoreClient coreClient) throws ApiException {
        boolean isValidApi = false;
        CoreClientapi coreClientapi = new CoreClientapi();
        for (CoreClientapi clientapi : coreClient.getCoreClientapis()) {

            if (oAuthtoken.equalsIgnoreCase(clientapi.getCoreApi().getApiKey())) {
                isValidApi = true;
                coreClientapi = clientapi;
            }
        }

        if (!isValidApi) {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.UNAUTHORIZED_RESOURCE_ACCESS), HttpStatus.UNAUTHORIZED);
        }
        return coreClientapi;

    }

    /**
     * Get request headers
     *
     * @param request
     * @return
     * @throws Exception
     */
    public String getHeadersInfo(HttpServletRequest request) throws Exception {

        try {
            Map<String, String> map = new HashMap<>();

            Enumeration headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String key = (String) headerNames.nextElement();
                String value = request.getHeader(key);
                map.put(key, value);
            }

            return _helper.getJsonStringFromObject(map);
        } catch (JsonProcessingException ex) {
            throw ex;
        }
    }

}
