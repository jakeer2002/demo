// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.utility;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.parserservice.request.CanonRequest;
import com.bgt.lens.model.parserservice.request.ConvertRequest;
import com.bgt.lens.model.parserservice.request.InfoRequest;
import com.bgt.lens.model.parserservice.request.ParseJobRequest;
import com.bgt.lens.model.parserservice.request.ParseResumeRequest;
import com.bgt.lens.model.parserservice.request.PingRequest;
import com.bgt.lens.model.parserservice.request.ProcessLocaleRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.parserservice.ParseResumeBGXMLResponse;
import org.springframework.http.ResponseEntity;
/**
 * *
 * Utility service Interface
 */
public interface IUtilityService {

    /**
     *
     * Check Lens availability
     *
     * @param infoRequest
     * @return Status of all the instances
     * @throws Exception
     */
    ApiResponse info(InfoRequest infoRequest) throws Exception;

    /**
     * *
     * Check Lens availability
     *
     * @param pingRequest
     * @return
     * @throws Exception
     */
    ApiResponse ping(PingRequest pingRequest) throws Exception;

    /**
     * *
     * Convert the given binary data into plain text
     *
     * @param convertRequest
     * @return
     * @throws Exception
     */
    ApiResponse convertBinaryData(ConvertRequest convertRequest) throws Exception;

    /**
     * *
     * Canon binary XML data
     *
     * @param canonRequest
     * @return
     * @throws Exception
     */
    ApiResponse canonBinaryData(CanonRequest canonRequest) throws Exception;

    /**
     * *
     * Parses the resume
     *
     * @param ParseResumeRequest
     * @return
     * @throws Exception
     */
    ApiResponse parseResume(ParseResumeRequest ParseResumeRequest) throws Exception;

    /**
     * *
     * Parses the resume with BGTXML,HRXML HTM,RTF
     *
     * @param parseResumeRequest
     * @param variant
     * @return
     * @throws Exception
     */
    ApiResponse parseResumeVariant(ParseResumeRequest parseResumeRequest, String variant) throws Exception;

    /**
     * Find locale
     *
     * @param processLocaleRequest
     * @return
     * @throws java.lang.Exception
     */
    ApiResponse findLocale(ProcessLocaleRequest processLocaleRequest) throws Exception;

    /**
     * *
     * Parses the resume
     *
     * @param parseJobRequest
     * @return
     * @throws Exception
     */
    ApiResponse parseJob(ParseJobRequest parseJobRequest) throws Exception;

    /**
     * *
     * Parses the resume with BGTXML,HRXML HTM,RTF
     *
     * @param parseJobRequest
     * @param variant
     * @return
     * @throws Exception
     */
    ApiResponse parseJobVariant(ParseJobRequest parseJobRequest, String variant) throws Exception;

      /**
     * *
     * Get the CoreClient by Id
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    CoreClient getCoreClient(int clientId) throws Exception;
    
        /**
     * *
     * Parses the resume with BGTXML,HRXML HTM,RTF by using microservice
     *
     * @param parseResumeRequest
     * @param variant
     * @return
     * @throws Exception
     */
    ApiResponse parseResumeVariantMicroservice(ParseResumeRequest parseResumeRequest, String variant) throws Exception;
  
}
