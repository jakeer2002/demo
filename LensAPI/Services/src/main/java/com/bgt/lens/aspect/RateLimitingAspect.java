// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.authentication.AuthorizationHeaderParser;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.AdvanceLog;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicInteger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * RateLimiting aspect
 */
@Aspect
@Controller
public class RateLimitingAspect {

    private static final Logger LOGGER = LogManager.getLogger(RateLimitingAspect.class);
    private static final Helper _helper = new Helper();
    private IDataRepositoryService dataRepositoryService;

    private final Object lock = new Object();

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate template;

    /**
     * Initialize data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dataRepositoryService = dataRepositoryService;
    }

    // <editor-fold defaultstate="collapsed" desc="REDIS BASED API RATE LIMITING">
    /**
     * Validate Request For Rate Limit
     *
     * @param joinPoint
     * @return
     * @throws Exception  
     */
    @Around("execution(public * com.bgt.lens.services.authentication.impl.AuthenticationServiceImpl.validateApiContext(..))")
    public Object validateRequestLimit(ProceedingJoinPoint joinPoint) throws Exception, Throwable {
        ApiContext apiContext = _helper.getApiContext();
        AdvanceLog advanceLog = apiContext.getAdvanceLog();
        long startTime = System.currentTimeMillis();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if (!apiContext.getApi().equalsIgnoreCase("core")) {
        	String authHeaderMap;
            if (_helper.isNotNull(request)) {

                String authorizationHeader = request.getHeader("authorization");
                if (_helper.isNullOrEmpty(authorizationHeader)) {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.AUTHENTICATION_HEADER_NOT_FOUND), HttpStatus.BAD_REQUEST);
                }

                AuthorizationHeaderParser authorizationHeaderParser = new AuthorizationHeaderParser();
                authHeaderMap = authorizationHeaderParser.parse(authorizationHeader);

                LOGGER.info("Get user details from database for API Rate Limit Check");
                CoreClient coreClient = getClientDetails(authHeaderMap);
                if (coreClient.isRateLimitEnabled()) {
                    synchronized (lock) {
                        try {
                        boolean isKeyAvailable = template.opsForValue().getOperations().hasKey(authHeaderMap);

                        RedisAtomicInteger counter = null;
                        // if key is not available - create a key
                        if (!isKeyAvailable) {
                            
                            ///////////////////////////////////////////////////
                            int rateLimitCount = coreClient.getRateLimitTimePeriod();
                            counter = new RedisAtomicInteger(coreClient.getClientKey(), template.getConnectionFactory());
                            int count = counter.incrementAndGet();
                            template.opsForValue().getOperations().expire(coreClient.getClientKey(), rateLimitCount, TimeUnit.MINUTES);
                            long timeToLive = template.getExpire(coreClient.getClientKey());
                            setRateLimitResponseHeaders(String.valueOf(count), String.valueOf(coreClient.getRateLimitCount() - count), timeToLive);
                            return joinPoint.proceed();
                            ///////////////////////////////////////////////////
                            
                        } else {

                            ///////////////////////////////////////////////////
                            counter = new RedisAtomicInteger(authHeaderMap, template.getConnectionFactory());
                            int count = counter.incrementAndGet() - 1;
                            if (count == 0) {
                                int rateLimitCount = coreClient.getRateLimitTimePeriod();
                                template.opsForValue().getOperations().expire(coreClient.getClientKey(), rateLimitCount, TimeUnit.MINUTES);
                            }
                            long timeToLive = template.getExpire(authHeaderMap);
                            // If count exceeds throw error
                            if (count != 0 && count >= coreClient.getRateLimitCount()) {
                                setRateLimitResponseHeaders(String.valueOf(coreClient.getRateLimitCount()), String.valueOf(0), timeToLive);
                                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.RATELIMIT_ERROR), HttpStatus.TOO_MANY_REQUESTS);
                            } 
                            setRateLimitResponseHeaders(String.valueOf(count), String.valueOf(coreClient.getRateLimitCount() - count), timeToLive);
                            ///////////////////////////////////////////////////
                        }
                      } catch(RedisConnectionFailureException ex) {
                          LOGGER.error("Error occurred while connecting Redis :"+ex);
                          LOGGER.info("Skipping API Rate Limiting");
                          return joinPoint.proceed();
                      }
                    }
                }
            } else {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_HTTP_REQUEST), HttpStatus.BAD_REQUEST);
            }
        }
        // Set Advanced log time
        double elapsedTime = ((System.currentTimeMillis() - startTime) / 1000.0);
        advanceLog.setApiRateLimit(elapsedTime);
        
        return joinPoint.proceed();

    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HELPER FUNCTIONS">
    public CoreClient getClientDetails(String consumerKey) throws Exception {
        try {
            ClientCriteria clientCriteria = new ClientCriteria();
            clientCriteria.setClientKey(consumerKey);
            List<CoreClient> coreClientList = dataRepositoryService.getClientByCriteria(clientCriteria);
            if (coreClientList == null || coreClientList.size() < 0) {
                throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.UNAUTHORIZED_RESOURCE_ACCESS), HttpStatus.BAD_REQUEST);
            }
            return coreClientList.get(0);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public long getInitialTimeStamp(List<String> keysList) {
        List<Long> timeStampList = new ArrayList<>();
        for (String key : keysList) {
            String timeStamp = (key.split("_"))[3];
            timeStampList.add(Long.parseLong(timeStamp));
        }
        Collections.sort(timeStampList);
        return timeStampList.get(0);
    }

    public int getCounterSum(List<String> keysList) {
        int counterSum = 0;
        for (String key : keysList) {
            RedisAtomicInteger counter = new RedisAtomicInteger(key, template.getConnectionFactory());
            int count = counter.get();
            counterSum += count;
        }
        return counterSum;
    }

    public boolean isRateLimitExceeded(List<String> keysList) {
        int counterSum = 0;
        boolean isLimitExceeded = false;
        for (String key : keysList) {
            RedisAtomicInteger counter = new RedisAtomicInteger(key, template.getConnectionFactory());
            int count = counter.get();
            counterSum += count;
        }
        String[] keyParams = keysList.get(0).split("_");
        String clientRateLimit = keyParams[2];

        if (counterSum >= Integer.parseInt(clientRateLimit)) {
            isLimitExceeded = true;
        }
        return isLimitExceeded;
    }

    public void setRateLimitResponseHeaders(String limit, String remaining, long timeStamp) {
        ApiContext apiContext = _helper.getApiContext();
        apiContext.setRequestCount(limit);
        apiContext.setRemainingRequestCount(remaining);
        apiContext.setRetryTimeStamp(timeStamp);
    }
    // </editor-fold>
}
