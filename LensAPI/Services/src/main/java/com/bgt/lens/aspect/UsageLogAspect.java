// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.logger.ILoggerService;
import java.time.Instant;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Usage log aspect
 */
@Aspect
@Order(2)
public class UsageLogAspect {

    private ILoggerService loggerService;

    /**
     * Set logger service
     *
     * @param loggerService
     */
    public void setLoggerService(ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    private static final Logger LOGGER = LogManager.getLogger(UsageLogAspect.class);
    
    private static final Helper _helper = new Helper();

    /**
     * Add usage log if any exception from controller
     *
     * @param joinPoint
     * @param exception
     */
    @AfterThrowing(pointcut = "execution(* com.bgt.lens.web.rest.CoreController.*(..)) "
            + "|| execution(* com.bgt.lens.web.rest.UtilityController.*(..))"
            + "|| execution(* com.bgt.lens.web.soap.SoapEndPoint.*(..)) ", throwing = "exception")
    public void addUsageLog(JoinPoint joinPoint, Exception exception) {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");
            apiContext.setException(exception);

            ApiResponse response = apiContext.getApiResponse();
            response.status = false;
            if (exception instanceof ApiException) {
                ApiException ex = new ApiException("", "", HttpStatus.INTERNAL_SERVER_ERROR);

                Throwable cause = apiContext.getException();
                if (cause instanceof ApiException) {
                    ex = (ApiException) cause;
                }
                response.statusCode = ex.getStatusCode();
                response.requestId = apiContext.getRequestId().toString();
                response.timeStamp = Date.from(Instant.now());
                response.responseData = ex.getMessage();
            } else {
                
                apiContext.setStatusCode(500);
                response.requestId = apiContext.getRequestId().toString();
                response.timeStamp = Date.from(Instant.now());
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UNKNOWN_EXCEPTION);
            }
            apiContext.setApiResponse(response);
            apiContext.setLocale(response.getProcessedWithLocale());
            
            Date endDateTime = Date.from(Instant.now());
            apiContext.setOutTime(endDateTime);
            
            loggerService.addUsageLog(apiContext);
//            if (!response.status) {
//                LOGGER.error("Unable to add usage log");
//            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add usage log. Exception -  " + ex.getMessage());
        }
    }

    /**
     * Add usage log after response returned from controller
     *
     * @param joinPoint
     * @param result
     * @throws Exception
     */
    @AfterReturning(
            pointcut = "execution(* com.bgt.lens.web.rest.CoreController.*(..)) "
            + "|| execution(* com.bgt.lens.web.rest.UtilityController.*(..)) "
            + "|| execution(* com.bgt.lens.web.soap.SoapEndPoint.*(..)) ",
            returning = "result")
    public void addUsageLog(JoinPoint joinPoint, Object result) throws Exception {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");
            LOGGER.info("Usage log Start time : " + Date.from(Instant.now()));
             
            Date endDateTime = Date.from(Instant.now());
            apiContext.setOutTime(endDateTime);
            
            loggerService.addUsageLog(apiContext);
            LOGGER.info("Usage log End time : " + Date.from(Instant.now()));
//            if (!response.status) {
//                LOGGER.error("Unable to add usage log");
//            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add usage log. Exception - " + ex.getMessage());
        }
    }
}
