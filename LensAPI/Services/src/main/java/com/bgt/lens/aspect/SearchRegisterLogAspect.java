// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.logger.ILoggerService;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Search Register Log aspect
 */
@Aspect
public class SearchRegisterLogAspect {
    private static final Logger LOGGER = LogManager.getLogger(SearchCommandDumpAspect.class);

    private final Helper _helper = new Helper();

    private ILoggerService loggerService = null;

    /**
     * Set logger service
     *
     * @param loggerService
     */
    public void setLoggerService(ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    /**
     * Register log
     *
     * @param joinPoint
     * @param result
     * @throws Exception
     * @throws Throwable
     */
    @AfterReturning(pointcut = "execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.registerResume(..)) || "
            + "execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.unregisterResume(..)) ||"
            + "execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.registerJob(..)) ||"
            + "execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.unregisterJob(..))",
            returning = "result")
    public void addRegisterLog(JoinPoint joinPoint, Object result) throws Exception, Throwable {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");
            ApiResponse apiResponse = (ApiResponse) result;
            if (apiResponse.status && _helper.isNotNull(apiContext.getSearchRegisterLog())) {
                loggerService.addRegisterLog(apiContext);
            }
//            apiContext.setSearchRegisterLog(null);
//            _helper.setApiContext(apiContext);
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add Lens Document Exception. Exception - " + ex.getMessage());
        }
    }
}
