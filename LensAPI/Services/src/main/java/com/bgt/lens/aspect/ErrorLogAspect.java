// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.logger.ILoggerService;
import java.time.Instant;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Error log aspect
 */
@Aspect
@Order(1)
public class ErrorLogAspect {

    private ILoggerService loggerService;
    private final Helper _helper = new Helper();

    /**
     * Set logger service
     *
     * @param loggerService
     */
    public void setLoggerService(ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    private static final Logger LOGGER = LogManager.getLogger(ErrorLogAspect.class);

    /**
     * Add error log
     *
     * @param joinPoint
     * @param exception
     */
    @AfterThrowing(pointcut = "execution(* com.bgt.lens.web.rest.CoreController.*(..)) || execution(* com.bgt.lens.web.rest.UtilityController.*(..)) || execution(* com.bgt.lens.web.soap.SoapEndPoint.*(..))", throwing = "exception")
    public void addErrorLog(JoinPoint joinPoint, Exception exception) {
        try {

            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");
            apiContext.setException(exception);
            apiContext.setTraceLog(ExceptionUtils.getStackTrace(exception));

            ApiResponse response = apiContext.getApiResponse();
            if (exception instanceof ApiException) {
                apiContext.setApiResponse(new ApiResponse());
                ApiException ex = new ApiException("", "", HttpStatus.INTERNAL_SERVER_ERROR);

                Throwable cause = apiContext.getException();
                if (cause instanceof ApiException) {
                    ex = (ApiException) cause;
                }

                response.requestId = apiContext.getRequestId().toString();
                response.timeStamp = Date.from(Instant.now());
                response.responseData = ex.getMessage();
                response.statusCode = ex.getStatusCode();
            } else {
                apiContext.setStatusCode(500);
                response.requestId = apiContext.getRequestId().toString();
                response.timeStamp = Date.from(Instant.now());
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.UNKNOWN_EXCEPTION);
            }
            apiContext.setApiResponse(response);

            loggerService.addErrorLog(apiContext);
//            if (!response.status) {
//                LOGGER.error("Unable to add error log");
//            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add error log . Exception - " + ex.getMessage());
        }
    }   
       
    /**
     * Add error log after response returned from controller
     *
     * @param joinPoint
     * @param result
     * @throws Exception
     */
    @AfterReturning(
            pointcut = "execution(* com.bgt.lens.web.rest.CoreController.*(..)) "
            + "|| execution(* com.bgt.lens.web.rest.UtilityController.*(..)) "
            + "|| execution(* com.bgt.lens.web.soap.SoapEndPoint.*(..)) ",
            returning = "result")
    public void addErrorLog(JoinPoint joinPoint, Object result) throws Exception {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                    .getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");
            ApiResponse response = apiContext.getApiResponse();
            if(!response.status){
                if(!(_helper.isNotNull(apiContext.getException()) 
                    && _helper.isNotNullOrEmpty(apiContext.getException().getMessage()))){
                    apiContext.setException(new Exception((String)response.responseData));
                }
                loggerService.addErrorLog(apiContext);
//                if (!response.status) {
//                    LOGGER.error("Unable to add error log");
//                }
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add error log. Exception - " + ex.getMessage());
        }
    }  
}
