/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bgt.lens.grpcController;

import com.bgt.lens.grpc.DocumentStoreServiceGrpc;
import com.bgt.lens.grpc.DocumentStoreServiceGrpc.DocumentStoreServiceBlockingStub;
import com.bgt.lens.grpc.ServiceRequest;
import com.bgt.lens.grpc.ServiceResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author sivareddy
 */
public class GrpcClient {
    
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(GrpcClient.class);
    
    private String host;
    private int port;
    
    public GrpcClient() {
    }
    
    public GrpcClient(String hostName, int portNumber) {
        host = hostName;
        port = portNumber;
    }
        
    
    public String microService(String requestId, String base64, String extension, boolean htm,boolean rtf ){
         ManagedChannel managedChannel = ManagedChannelBuilder
        .forAddress(host, port).usePlaintext().build();   
        DocumentStoreServiceBlockingStub stub= DocumentStoreServiceGrpc.newBlockingStub(managedChannel);
        ServiceRequest request = ServiceRequest.newBuilder().setBase64(base64).setExtension(extension).setGetHtm(htm).setGetRtf(rtf).setLensApiRequestId(requestId).build();
         ServiceResponse response = stub.serviceMain(request);
         managedChannel.shutdown();
         return response.getCanonCommand();
    }
}
