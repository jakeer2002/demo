// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.repositories.impl;

import com.bgt.lens.DocKey;
import com.bgt.lens.LensException;
import com.bgt.lens.LensMessage;
import com.bgt.lens.LensSession;
import com.bgt.lens.ServerBusyException;
import com.bgt.lens.config.LensServiceConfig;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.Enum.VariantType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.PerformanceLogging;
import com.bgt.lens.ld.JLensLD;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.repositories.ILensRepository;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.xml.bind.UnmarshalException;
import org.apache.commons.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

/**
 *
 * Lens Repository
 */
public class LensRepository implements ILensRepository {

    /**
     * Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(LensRepository.class);

    /**
     * Helper object
     */
    private Helper helper = null;

    private ApiErrors apiErrors = new ApiErrors();

    /**
     * LENS parse data
     */
    private LensParseData lensParseData = null;

    /**
     * Lens default error message
     */
    public final String lensErrorMessage = "Could not connect to server. Server may be temporarily down, Please contact your Burning Glass representative";

    /**
     * Lens Empty error message
     */
    public final String lensEmptyMessage = "error converting: text not generated";

    /**
     * Get helper
     *
     * @return
     */
    public Helper getHelper() {
        return helper;
    }

    /**
     * Set Helper
     *
     * @param helper
     */
    public void setHelper(Helper helper) {
        this.helper = helper;
    }

    /**
     * Get LENS parse data
     *
     * @return
     */
    public LensParseData getLensParseData() {
        return lensParseData;
    }

    /**
     * Set LENS parse data
     *
     * @param lensParseData
     */
    public void setLensParseData(LensParseData lensParseData) {
        this.lensParseData = lensParseData;
    }

    /**
     * Get Lens status
     *
     * @param coreLenssettings
     * @return
     */
    @Override
    @PerformanceLogging
    public ApiResponse info(CoreLenssettings coreLenssettings) {
        LOGGER.info("<Info>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            session = lensParseData.createLensSession(coreLenssettings);
            LensMessage outMessage = session.getInfo();
            String lensInfo = outMessage.getMessageData();
            // ToDo: Need to check posting tagger also
            if (lensInfo.contains("rtgr") || lensInfo.contains("ptgr") && lensInfo.contains("name='logger'") && !lensInfo.contains("status='not found'") || lensInfo.toLowerCase().contains("message bus is available now")) {
                lensResponse.status = true;
                lensResponse.responseData = "Services are available";
            } else {
                lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<Info> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<info> - Response Sent");
        return lensResponse;
    }

    /**
     * Ping Lens
     *
     * @param coreLenssettings
     * @return
     */
    @Override
    @PerformanceLogging
    public ApiResponse ping(CoreLenssettings coreLenssettings) {
        LOGGER.info("<Ping>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            session = lensParseData.createLensSession(coreLenssettings);
            lensResponse.status = session.ping();
        } catch (LensException | ServerBusyException | UnsupportedEncodingException ex) {
            LOGGER.error("<Ping> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<Ping> - Response Sent");
        return lensResponse;
    }

    /**
     * Convert binary data
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @return
     */
    @Override
    @PerformanceLogging
    public ApiResponse convertBinaryData(CoreLenssettings coreLenssettings, byte[] binaryData, String extension) {
        LOGGER.info("<Convert>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            session = lensParseData.createLensSession(coreLenssettings);
            LensMessage outMessage = session.convertBinaryData(binaryData, extension);
            String strResult = outMessage.getMessageData();
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? helper.getErrorMessageWithURL(ApiErrors.LENS_CONVERSION_ERROR) : strResult;
            lensResponse.status = helper.checkLensResultForFailure((String) lensResponse.responseData);
            if (!lensResponse.status) {
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<Convert> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<Convert> - Response Sent");
        return lensResponse;
    }

    /**
     * Tag binary data
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @param customSkillCode
     * @return
     */
    @Override
    @PerformanceLogging
    public ApiResponse tagBinaryData(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) {
        LOGGER.info("<TagBinaryData>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            String strResult = lensParseData.tagDocument(binaryData, extension, docType, extension, VariantType.bgtxml, coreLenssettings, customSkillCode);

            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;

            lensResponse.status = helper.checkLensResultForFailure(strResult);
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<TagBinaryData> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } catch (Exception ex) {
            LOGGER.error("<TagBinaryData> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<TagBinaryData> - Response Sent");
        return lensResponse;
    }

    /**
     * Tag binary data
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @return
     */
    @Override
    @PerformanceLogging
    public ApiResponse tagBinaryStructuredBGTXMLData(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) {
        LOGGER.info("<TagBinaryData>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            String strResult = lensParseData.tagDocument(binaryData, extension, docType, extension, VariantType.bgtxml, coreLenssettings, customSkillCode);
            Object object = helper.getLENSResponseObject(strResult, coreLenssettings.getLocale());
            lensResponse.responseData = helper.isNotNull(object) ? object : lensEmptyMessage;
            lensResponse.status = helper.checkLensResultForFailure(strResult);
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }

                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<TagBinaryData> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } catch (Exception ex) {
            LOGGER.error("<TagBinaryData> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<TagBinaryData> - Response Sent");
        return lensResponse;
    }

    /**
     * Tag binary data and get HRXML
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @return
     */
    @Override
    @PerformanceLogging
    public ApiResponse tagBinaryHRXMLData(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) {
        LOGGER.info("<TagBinaryDataHRXML>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            String strResult = lensParseData.tagDocument(binaryData, extension, docType, extension, VariantType.bgtxml, coreLenssettings, customSkillCode);
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
            Boolean isError = helper.checkLensResultForFailure((String) lensResponse.responseData);
            if (isError) {
                if (helper.isNullOrEmpty((String) lensResponse.responseData)) {
                    throw new ApiException("", helper.getErrorMessageWithURL(ApiErrors.EMPTY_PARSED_DATA), HttpStatus.INTERNAL_SERVER_ERROR);
                }

                if (helper.isNullOrEmpty(coreLenssettings.getHrxmlcontent())) {
                    throw new ApiException("", helper.getErrorMessageWithURL(ApiErrors.EMPTY_HRXML_DATA), HttpStatus.METHOD_NOT_ALLOWED);
                }

                String transformedHRXML = helper.applyStyleSheet((String) lensResponse.responseData, coreLenssettings.getHrxmlcontent());
                Object object = helper.getHRXMLResponseObject(coreLenssettings.getHrxmlversion(), transformedHRXML);
                lensResponse.responseData = object;
                lensResponse.status = true;
            }
            if (!isError) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    //lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<TagBinaryDataHRXML> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } catch (UnmarshalException ex) {
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.HRXML_VERSION_ERROR);
            LOGGER.error("Invalid HRXML version assigned to the HRXML stylesheet" + ex.getMessage());
            return lensResponse;
        } catch (ApiException ex) {
            throw ex;
        } catch (Exception ex) {
            LOGGER.error("<TagBinaryDataHRXML> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<TagBinaryDataHRXML> - Response Sent");
        return lensResponse;
    }
    @Override
    @PerformanceLogging
     public ApiResponse tagBinaryHRXMLDataBold(CoreLenssettings coreLenssettings, ApiResponse lensResponse) {
       String strResult = lensResponse.getResponseData().toString();
         try {
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
            Boolean isError = helper.checkLensResultForFailure((String) lensResponse.responseData);
            if (isError) {
                if (helper.isNullOrEmpty((String) lensResponse.responseData)) {
                    throw new ApiException("", helper.getErrorMessageWithURL(ApiErrors.EMPTY_PARSED_DATA), HttpStatus.INTERNAL_SERVER_ERROR);
                }

                if (helper.isNullOrEmpty(coreLenssettings.getHrxmlcontent())) {
                    throw new ApiException("", helper.getErrorMessageWithURL(ApiErrors.EMPTY_HRXML_DATA), HttpStatus.METHOD_NOT_ALLOWED);
                }

                String transformedHRXML = helper.applyStyleSheet((String) lensResponse.responseData, coreLenssettings.getHrxmlcontent());
                Object object = helper.getHRXMLResponseObject(coreLenssettings.getHrxmlversion(), transformedHRXML);
                lensResponse.responseData = object;
                lensResponse.status = true;
            }
            if (!isError) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    //lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<TagBinaryDataHRXML> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } catch (UnmarshalException ex) {
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.HRXML_VERSION_ERROR);
            LOGGER.error("Invalid HRXML version assigned to the HRXML stylesheet" + ex.getMessage());
            return lensResponse;
        } catch (ApiException ex) {
            throw ex;
        } catch (Exception ex) {
            LOGGER.error("<TagBinaryDataHRXML> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } 
        LOGGER.info("<TagBinaryDataHRXML> - Response Sent");
        return lensResponse;
    }

    /**
     * Tag binary data with RTF
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @return
     */
    @Override
    @PerformanceLogging
    public ApiResponse tagBinaryDataWithRTF(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) {
        LOGGER.info("<TagBinaryDataWithRTF>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            String strResult = lensParseData.tagDocument(binaryData, extension, docType, extension, VariantType.rtf, coreLenssettings, customSkillCode);
            String xml10pattern = "[^"
                    + "\u0009\r\n"
                    + "\u0020-\uD7FF"
                    + "\uE000-\uFFFD"
                    + "\ud800\udc00-\udbff\udfff"
                    + "]";
            strResult = strResult.replaceAll(xml10pattern, "*");
            Object object = helper.getLENSResponseObject(strResult, coreLenssettings.getLocale());
            lensResponse.responseData = helper.isNotNull(object) ? object : lensEmptyMessage;
            lensResponse.status = helper.checkLensResultForFailure(strResult);
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    //lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<TagBinaryDataWithRTF> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } catch (Exception ex) {
            LOGGER.error("<TagBinaryDataWithRTF> Parsing Exception : " + ex.getMessage());
            // lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<TagBinaryDataWithRTF> - Response Sent");
        return lensResponse;
    }

    /**
     * Tag binary data with HTM
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @return
     */
    @Override
    @PerformanceLogging
    public ApiResponse tagBinaryDataWithHTM(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType, String customSkillCode) {
        LOGGER.info("<TagBinaryDataWithHTM>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            String strResult = lensParseData.tagDocument(binaryData, extension, docType, extension, VariantType.htm, coreLenssettings, customSkillCode);
            String xml10pattern = "[^"
                    + "\u0009\r\n"
                    + "\u0020-\uD7FF"
                    + "\uE000-\uFFFD"
                    + "\ud800\udc00-\udbff\udfff"
                    + "]";
            strResult = strResult.replaceAll(xml10pattern, "*");
            Object object = helper.getLENSResponseObject(strResult, coreLenssettings.getLocale());
            lensResponse.responseData = helper.isNotNull(object) ? object : lensEmptyMessage;
            lensResponse.status = helper.checkLensResultForFailure(strResult);
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<TagBinaryDataWithHTM> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } catch (Exception ex) {
            LOGGER.error("<TagBinaryDataWithHTM> Parsing Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<TagBinaryDataWithHTM> - Response Sent");
        return lensResponse;
    }

    /**
     * Send XML command No object conversion, Since the request is a string data
     * to LENS
     *
     * @param coreLenssettings
     * @param xmlCommand
     * @return
     */
    @Override
    @PerformanceLogging
    public ApiResponse sendXMLCommand(CoreLenssettings coreLenssettings, String xmlCommand) throws Exception {
        LOGGER.info("<SendXMLCommand>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            session = lensParseData.createLensSession(coreLenssettings);
            //LensMessage inMessage = LensMessage.create(xmlCommand, LensMessage.XML_TYPE);
            LensMessage.MessageContentType jsonMessageType=  LensMessage.MessageContentType.JSON;
            LensMessage inMessage = LensMessage.create(xmlCommand, LensMessage.XML_TYPE,jsonMessageType);
            LensMessage outMessage = session.sendMessage(inMessage);
            String strResult = "";
            lensResponse.status = helper.checkLensResultForFailure((String) outMessage.getMessageData());
            strResult = (String) outMessage.getMessageData();
            String xml10pattern = "[^"
                    + "\u0009\r\n"
                    + "\u0020-\uD7FF"
                    + "\uE000-\uFFFD"
                    + "\ud800\udc00-\udbff\udfff"
                    + "]";
            strResult = strResult.replaceAll(xml10pattern, "*");
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult((String) outMessage.getMessageData());
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                if (helper.isCanonInvalidDocument(strResult)) {
                    strResult = helper.getErrorMessageWithURL(ApiErrors.INVALID_CANON_DATA);
                } else {
                    strResult = apiErrors.getLensErrorMessage(strResult);
                }
            }
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
        } catch (LensException | ServerBusyException | UnsupportedEncodingException ex) {
            LOGGER.error("<SendXMLCommand> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<SendXMLCommand> - Response Sent");
        return lensResponse;
    }

    /**
     * Find Locale Command
     *
     * @param coreLenssettings
     * @param binaryData
     * @param extension
     * @param docType
     * @return
     * @throws Exception
     */
    @Override
    @PerformanceLogging
    public ApiResponse findLocale(CoreLenssettings coreLenssettings, byte[] binaryData, String extension, char docType) throws Exception {
        LOGGER.info("<ProcessLocale>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            session = lensParseData.createLensSession(coreLenssettings);
            LensMessage outMessage = session.getLocale(binaryData, extension, docType);
            String strResult = outMessage.getMessageData();
            boolean error = helper.checkLensResultForFailure(strResult);
            if (!error) {
                strResult = apiErrors.getLensErrorMessage(outMessage.getMessageData());
            } else {
                strResult = outMessage.getMessageData();
            }
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
            lensResponse.status = helper.checkLensResultForFailure((String) lensResponse.responseData);
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<ProcessLocale> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<ProcessLocale> - Response Sent");
        return lensResponse;
    }

    @Override
    @PerformanceLogging
    public ApiResponse ldTagBinaryData(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception {
        LOGGER.info("<LD_TagBinaryData>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            JLensLD jLensLD = lensParseData.createLensSession(lenssettingses);
            LensMessage outMessage = jLensLD.tagBinaryData(binaryData, extension, docType, lenssettingses);
            String strResult = outMessage.getMessageData();
            boolean error = helper.checkLensResultForFailure(strResult);
            boolean invalidParseData = !(strResult.contains("<ResDoc>") || strResult.contains("<JobDoc>"));

            if (invalidParseData) {
                error = false;
            }
            if (!error) {
                strResult = apiErrors.getLensErrorMessage(outMessage.getMessageData());
            } else {
                strResult = outMessage.getMessageData();
            }
            if (error) {
                lensResponse.identifiedLocale = helper.getLocale(strResult);
            }
            // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
            if (error) {
                strResult = error == true ? helper.updateLocale(strResult, defaultLocale, lensResponse.identifiedLocale, lenssettingses) : strResult;
                lensResponse.processedWithLocale = helper.getLocale(strResult);
            }
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
            lensResponse.status = error;
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<LD_TagBinaryData> Lens Exception : " + ex.getMessage());
            if (ex.getLocalizedMessage().contains("Error during locale identification ")
                    || ex.getLocalizedMessage().contains("Lens instance details are missing :")
                    || ex.getLocalizedMessage().contains("The given BGT XML command is missing the <special><locale> tag")
                    || ex.getLocalizedMessage().contains("LanguageDetector does not support this XML command")
                    || ex.getLocalizedMessage().contains("<locale> missing code attribute : ")) {
                lensResponse.responseData = ex.getLocalizedMessage();
            } else {
                lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            }
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<LD_TagBinaryData> - Response Sent");
        return lensResponse;
    }

    @Override
    @PerformanceLogging
    public ApiResponse ldTagBinaryStructuredBGTXMLData(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception {
        LOGGER.info("<LD_TagBinaryData>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            JLensLD jLensLD = lensParseData.createLensSession(lenssettingses);
            LensMessage outMessage = jLensLD.tagBinaryData(binaryData, extension, docType, lenssettingses);
            String strResult = outMessage.getMessageData();
            boolean error = helper.checkLensResultForFailure(strResult);
            boolean invalidParseData = !(strResult.contains("<ResDoc>") || strResult.contains("<JobDoc>"));

            if (invalidParseData) {
                error = false;
            }
            if (!error) {
                strResult = apiErrors.getLensErrorMessage(outMessage.getMessageData());
            } else {
                strResult = outMessage.getMessageData();
            }
            if (error) {
                lensResponse.identifiedLocale = helper.getLocale(strResult);
            }
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
            if (error) {
                strResult = error == true ? helper.updateLocale(strResult, defaultLocale, lensResponse.identifiedLocale, lenssettingses) : strResult;
                lensResponse.processedWithLocale = helper.getLocale(strResult);
                Object object = helper.getLENSResponseObject(strResult, helper.getLocale(strResult));
                lensResponse.responseData = helper.isNotNull(object) ? object : lensEmptyMessage;
            }

            lensResponse.status = error;
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<LD_TagBinaryData> Lens Exception : " + ex.getMessage());
            if (ex.getLocalizedMessage().contains("Error during locale identification ")
                    || ex.getLocalizedMessage().contains("Lens instance details are missing :")
                    || ex.getLocalizedMessage().contains("The given BGT XML command is missing the <special><locale> tag")
                    || ex.getLocalizedMessage().contains("LanguageDetector does not support this XML command")
                    || ex.getLocalizedMessage().contains("<locale> missing code attribute : ")) {
                lensResponse.responseData = ex.getLocalizedMessage();
            } else {
                lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            }
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<LD_TagBinaryData> - Response Sent");
        return lensResponse;
    }

    @Override
    @PerformanceLogging
    public ApiResponse ldTagBinaryHRXMLData(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception {
        LOGGER.info("<LD_TagBinaryDataHRXML>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            JLensLD jLensLD = lensParseData.createLensSession(lenssettingses);
            LensMessage outMessage = jLensLD.tagBinaryData(binaryData, extension, docType, lenssettingses);
            String strResult = outMessage.getMessageData();
            boolean error = helper.checkLensResultForFailure(strResult);
            boolean invalidParseData = !(strResult.contains("<ResDoc>") || strResult.contains("<JobDoc>"));

            if (invalidParseData) {
                error = false;
            }
            if (!error) {
                strResult = apiErrors.getLensErrorMessage(outMessage.getMessageData());
            } else {
                strResult = outMessage.getMessageData();
            }
            if (error) {
                lensResponse.identifiedLocale = helper.getLocale(strResult);
            }
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
            if (error) {
                strResult = error == true ? helper.updateLocale(strResult, defaultLocale, lensResponse.identifiedLocale, lenssettingses) : strResult;
                lensResponse.processedWithLocale = helper.getLocale(strResult);
            }
            Boolean isError = helper.checkLensResultForFailure((String) lensResponse.responseData);
            if (isError) {
                if (helper.isNullOrEmpty((String) lensResponse.responseData)) {
                    throw new ApiException("", helper.getErrorMessageWithURL(ApiErrors.EMPTY_PARSED_DATA), HttpStatus.INTERNAL_SERVER_ERROR);
                }
                CoreLenssettings coreLenssettings = helper.getLensSettingsByLocale(lenssettingses, strResult);

                if (helper.isNullOrEmpty(coreLenssettings.getHrxmlcontent())) {
                    throw new ApiException("", helper.getErrorMessageWithURL(ApiErrors.EMPTY_HRXML_DATA), HttpStatus.METHOD_NOT_ALLOWED);
                }
                String transformedHRXML = helper.applyStyleSheet((String) lensResponse.responseData, coreLenssettings.getHrxmlcontent());
                Object object = helper.getHRXMLResponseObject(coreLenssettings.getHrxmlversion(), transformedHRXML);
                lensResponse.responseData = object;
                lensResponse.status = true;
            }
            if (!isError) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<LD_TagBinaryDataHRXML> Lens Exception : " + ex.getMessage());
            if (ex.getLocalizedMessage().contains("Error during locale identification ")
                    || ex.getLocalizedMessage().contains("Lens instance details are missing :")
                    || ex.getLocalizedMessage().contains("The given BGT XML command is missing the <special><locale> tag")
                    || ex.getLocalizedMessage().contains("LanguageDetector does not support this XML command")
                    || ex.getLocalizedMessage().contains("<locale> missing code attribute : ")) {
                lensResponse.responseData = ex.getLocalizedMessage();
            } else {
                lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            }
            return lensResponse;
        } catch (ApiException ex) {
            throw ex;
        } catch (Exception ex) {
            LOGGER.error("<LD_TagBinaryDataHRXML> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<LD_TagBinaryDataHRXML> - Response Sent");
        return lensResponse;
    }

    @Override
    @PerformanceLogging
    public ApiResponse ldTagBinaryDataWithRTF(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception {
        LOGGER.info("<LD_TagBinaryDataWithRTF>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            JLensLD jLensLD = lensParseData.createLensSession(lenssettingses);
            LensMessage outMessage = jLensLD.tagBinaryDataWithRTF(binaryData, extension, docType, lenssettingses);
            String strResult = outMessage.getMessageData();
            String xml10pattern = "[^"
                    + "\u0009\r\n"
                    + "\u0020-\uD7FF"
                    + "\uE000-\uFFFD"
                    + "\ud800\udc00-\udbff\udfff"
                    + "]";
            strResult = strResult.replaceAll(xml10pattern, "*");
            boolean error = helper.checkLensResultForFailure(strResult);
            boolean invalidParseData = !(strResult.contains("<ResDoc>") || strResult.contains("<JobDoc>"));

            if (invalidParseData) {
                error = false;
            }
            if (!error) {
                strResult = apiErrors.getLensErrorMessage(outMessage.getMessageData());
            } else {
                strResult = outMessage.getMessageData();
            }
            if (error) {
                lensResponse.identifiedLocale = helper.getLocale(strResult);
            }
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
            if (error) {
                strResult = error == true ? helper.updateLocale(strResult, defaultLocale, lensResponse.identifiedLocale, lenssettingses) : strResult;
                lensResponse.processedWithLocale = helper.getLocale(strResult);
                Object object = helper.getLENSResponseObject(strResult, helper.getLocale(strResult));
                lensResponse.responseData = helper.isNotNull(object) ? object : lensEmptyMessage;
            }
            lensResponse.status = error;
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<LD_TagBinaryDataWithRTF> Lens Exception : " + ex.getMessage());
            if (ex.getLocalizedMessage().contains("Error during locale identification ")
                    || ex.getLocalizedMessage().contains("Lens instance details are missing :")
                    || ex.getLocalizedMessage().contains("The given BGT XML command is missing the <special><locale> tag")
                    || ex.getLocalizedMessage().contains("LanguageDetector does not support this XML command")
                    || ex.getLocalizedMessage().contains("<locale> missing code attribute : ")) {
                lensResponse.responseData = ex.getLocalizedMessage();
            } else {
                lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            }
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<LD_TagBinaryDataWithRTF> - Response Sent");
        return lensResponse;
    }

    @Override
    @PerformanceLogging
    public ApiResponse ldTagBinaryDataWithHTM(List<CoreLenssettings> lenssettingses, byte[] binaryData, String extension, char docType, String defaultLocale) throws Exception {
        LOGGER.info("<LD_TagBinaryDataWithHTM>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            JLensLD jLensLD = lensParseData.createLensSession(lenssettingses);
            LensMessage outMessage = jLensLD.tagBinaryDataWithHTM(binaryData, extension, docType, lenssettingses);
            String strResult = outMessage.getMessageData();
            String xml10pattern = "[^"
                    + "\u0009\r\n"
                    + "\u0020-\uD7FF"
                    + "\uE000-\uFFFD"
                    + "\ud800\udc00-\udbff\udfff"
                    + "]";
            strResult = strResult.replaceAll(xml10pattern, "*");
            boolean error = helper.checkLensResultForFailure(strResult);
            boolean invalidParseData = !(strResult.contains("<ResDoc>") || strResult.contains("<JobDoc>"));

            if (invalidParseData) {
                error = false;
            }
            if (!error) {
                strResult = apiErrors.getLensErrorMessage(outMessage.getMessageData());
            } else {
                strResult = outMessage.getMessageData();
            }
            if (error) {
                lensResponse.identifiedLocale = helper.getLocale(strResult);
            }
            lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
            if (error) {
                strResult = error == true ? helper.updateLocale(strResult, defaultLocale, lensResponse.identifiedLocale, lenssettingses) : strResult;
                lensResponse.processedWithLocale = helper.getLocale(strResult);
                Object object = helper.getLENSResponseObject(strResult, helper.getLocale(strResult));
                lensResponse.responseData = helper.isNotNull(object) ? object : lensEmptyMessage;
            }

            lensResponse.status = error;
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<LD_TagBinaryDataWithHTM> Lens Exception : " + ex.getMessage());
            if (ex.getLocalizedMessage().contains("Error during locale identification ")
                    || ex.getLocalizedMessage().contains("Lens instance details are missing :")
                    || ex.getLocalizedMessage().contains("The given BGT XML command is missing the <special><locale> tag")
                    || ex.getLocalizedMessage().contains("LanguageDetector does not support this XML command")
                    || ex.getLocalizedMessage().contains("<locale> missing code attribute : ")) {
                lensResponse.responseData = ex.getLocalizedMessage();
            } else {
                lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            }
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<LD_TagBinaryDataWithHTM> - Response Sent");
        return lensResponse;
    }

    @Override
    public ApiResponse getLensServiceList(CoreLenssettings coreLenssettings, String docType) throws Exception {
        LOGGER.info("<Info>");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            session = lensParseData.createLensSession(coreLenssettings);
            LensMessage outMessage = session.getInfo();
            String xrayInfo = outMessage.getMessageData();
            // ToDo: Need to check posting tagger also
            if (xrayInfo.contains("name='auditor'") && xrayInfo.contains("rtgr") && xrayInfo.contains("name='logger'") && !xrayInfo.contains("status='not found'")) {
                lensResponse.status = true;
                List<LensServiceConfig> serviceConfigList = helper.getLensServiceConfigFromInfo(xrayInfo, docType);
                lensResponse.responseData = serviceConfigList;
            } else {
                lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<Info> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<info> - Response Sent");
        return lensResponse;
    }

    /**
     * Register document
     *
     * @param lensSettings
     * @param binaryData
     * @param extension
     * @param vendorName
     * @param docType
     * @param type
     * @param docId
     * @param customSkillCode
     * @return
     * @throws Exception
     */
    @Override
    @PerformanceLogging
    public ApiResponse registerDocument(CoreLenssettings lensSettings, byte[] binaryData, String extension, String vendorName, String docType, char type, String docId, String customSkillCode) throws Exception {
        LOGGER.info("<Register" + WordUtils.capitalize(docType) + ">");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            String strResult = lensParseData.registerDocument(binaryData, extension, vendorName, docId, type, lensSettings, customSkillCode);
//            lensResponse.responseData = helper.isNotNull(strResult) ? strResult : lensEmptyMessage;
            lensResponse.status = helper.checkLensResultForFailure(strResult);
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<Register" + WordUtils.capitalize(docType) + "> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<Register" + WordUtils.capitalize(docType) + "> - Response Sent");
        return lensResponse;
    }

    /**
     * Unregister Document
     *
     * @param lensSettings
     * @param vendorName
     * @param docType
     * @param type
     * @param docId
     * @return
     * @throws Exception
     */
    @Override
    @PerformanceLogging
    public ApiResponse unregisterDocument(CoreLenssettings lensSettings, String vendorName, String docType, char type, String docId) throws Exception {
        LOGGER.info("<Unregister" + WordUtils.capitalize(docType) + ">");
        ApiResponse lensResponse = new ApiResponse();
        LensSession session = null;
        try {
            String strResult = lensParseData.unregisterDocument(vendorName, docId, new DocKey(), type, lensSettings);
            lensResponse.responseData = helper.isNotNull(strResult) ? strResult : lensEmptyMessage;
            lensResponse.status = helper.checkLensResultForFailure(strResult);
            if (!lensResponse.status) {
                if (strResult.contains("<bgtres>")) {
                    strResult = helper.getErrorResponseFromLensResult(strResult);
                    strResult = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                    // lensResponse.responseData = helper.isNullOrEmpty(strResult) ? lensEmptyMessage : strResult;
                }
                String lensErrorMessage = apiErrors.getLensErrorMessage(strResult);
                lensResponse.responseData = helper.isNullOrEmpty(lensErrorMessage) ? strResult : lensErrorMessage;
            }
        } catch (LensException | ServerBusyException ex) {
            LOGGER.error("<Unregister" + WordUtils.capitalize(docType) + "> Lens Exception : " + ex.getMessage());
            lensResponse.responseData = helper.getErrorMessageWithURL(ApiErrors.LENS_SERVICE_UNAVAILABLE);
            return lensResponse;
        } finally {
            lensParseData.closeLensSession(session);
        }
        LOGGER.info("<Unregister" + WordUtils.capitalize(docType) + "> - Response Sent");
        return lensResponse;
    }
}
