// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.repositories.impl;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.CoreMappers;
import com.bgt.lens.helpers.Enum.distanceUnits;
import com.bgt.lens.helpers.Enum.docType;
import com.bgt.lens.helpers.Enum.keywordContext;
import com.bgt.lens.helpers.Enum.keywordOperator;
import com.bgt.lens.helpers.Enum.keywordSearchTypes;
import com.bgt.lens.helpers.Enum.lookupType;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.Validator;
import com.bgt.lens.model.criteria.SearchLookupCriteria;
import com.bgt.lens.model.entity.CoreClientlenssettings;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.SearchLookup;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.model.rest.response.FilterSettings;
import com.bgt.lens.repositories.ILensRepository;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * Lens Command builder
 */
public class LensCommandBuilder implements ApplicationContextAware, BeanNameAware {

    private static final Logger LOGGER = LogManager.getLogger(LensCommandBuilder.class);

    /**
     *
     * @param ac
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException {
        applicationContext = ac;
    }

    /**
     *
     * @param beanName
     */
    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
    private ApplicationContext applicationContext;
    private String beanName;

    /**
     * Data repository service
     */
    private IDataRepositoryService dbService;

    /**
     * Lens repository
     */
    private ILensRepository lensRepository;

    /**
     * Api Configuration
     */
    private ApiConfiguration apiConfig;

    /**
     * Helper object
     */
    private final Helper _helper = new Helper();

    /**
     * Core mapper object
     */
    private final CoreMappers _coreMapper = new CoreMappers();

    /**
     * Core mapper object
     */
    private final Validator _validator = new Validator();
    /**
     * DocType Resume
     */
    private final char resumeType = 'R';
    /**
     * DocType Posting
     */
    private final char postingType = 'P';

    /**
     * Set data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dbService = dataRepositoryService;
    }

    /**
     * Set Lens repository
     *
     * @param lensRepository
     */
    public void setLensRepository(ILensRepository lensRepository) {
        this.lensRepository = lensRepository;
    }

    /**
     * Set ApiConfig
     *
     * @param apiConfig
     */
    public void setapiConfig(ApiConfiguration apiConfig) {
        this.apiConfig = apiConfig;
    }

    /**
     * Build canon command
     *
     * @param tagggedXml
     * @return
     */
    public String buildCanonCommand(String tagggedXml) {
        tagggedXml = tagggedXml.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
        return "<bgtcmd><canon>" + tagggedXml + "</canon></bgtcmd>";
    }

    /**
     * Create RegisterResume request with custom filter command
     *
     * @param <T>
     * @param registerRequest
     * @param vendor
     * @param id
     * @param taggedXml
     * @param docType
     * @return
     * @throws java.lang.Exception
     */
    public <T> String buildRegisterCommand(T registerRequest, String vendor, String id, String taggedXml, char docType) throws Exception {

        int spos, epos;
        String cmd = "";
        try {
            if (docType == 'R') {

//                try {
//                    spos = taggedXml.indexOf("<resume");
//                    epos = taggedXml.lastIndexOf("</resume>") + 9;
//                    taggedXml = taggedXml.substring(spos, epos);
//                } catch (Exception ex) {
//                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_PARSED_RESUME), HttpStatus.BAD_REQUEST);
//                }
                taggedXml = taggedXml.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                cmd = "<bgtcmd><register type='resume' vendor='";
                cmd += vendor;
                cmd += "' id='";
                cmd += id;
                cmd += "'>";
                cmd += taggedXml;
                cmd += "</register></bgtcmd>";
            } else {
//                try {
//                    spos = taggedXml.indexOf("<posting");
//                    epos = taggedXml.lastIndexOf("</posting>") + 10;
//                    taggedXml = taggedXml.substring(spos, epos);
//                } catch (Exception ex) {
//                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_PARSED_JOB), HttpStatus.BAD_REQUEST);
//                }
                taggedXml = taggedXml.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                cmd = "<bgtcmd><register type='posting' vendor='";
                cmd += vendor;
                cmd += "' id='";
                cmd += id;
                cmd += "'>";
                cmd += taggedXml;
                cmd += "</register></bgtcmd>";
            }
        } catch (ApiException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.CREATE_REGISTER_PARSED_RESUME_ERROR), HttpStatus.BAD_REQUEST);
        }
        return cmd;
    }

    /**
     * Build vendor create command
     *
     * @param type
     * @param vendor
     * @param docServerLabel
     * @param hypercuberServerLabel
     * @return
     */
    public String buildCreateVendorCommand(String type, String vendor, String docServerLabel, String hypercuberServerLabel) {
        String command = "<bgtcmd>";
        command += "<vendor type='" + type + "' name='" + vendor + "' command='vendorcreate' docserver='" + docServerLabel + "' ";
        if (_helper.isNotNullOrEmpty(hypercuberServerLabel)) {
            command += "hypercube='" + hypercuberServerLabel + "' ";
        }
        command += "/>";
        command += "</bgtcmd>";
        return command;
    }

    /**
     * Build vendor close command
     *
     * @param type
     * @param vendor
     * @param docServerLabel
     * @param hypercuberServerLabel
     * @return
     */
    public String buildCloseVendorCommand(String type, String vendor, String docServerLabel, String hypercuberServerLabel) {
        String command = "<bgtcmd>";
        command += "<vendor type='" + type + "' name='" + vendor + "' command='vendorclose' docserver='" + docServerLabel + "' ";
        if (_helper.isNotNullOrEmpty(hypercuberServerLabel)) {
            command += "hypercube='" + hypercuberServerLabel + "' ";
        }
        command += "/>";
        command += "</bgtcmd>";
        return command;
    }

    /**
     * Build vendor open command
     *
     * @param type
     * @param vendor
     * @param docServerLabel
     * @param hypercuberServerLabel
     * @return
     */
    public String buildOpenVendorCommand(String type, String vendor, String docServerLabel, String hypercuberServerLabel) {
        String command = "<bgtcmd>";
        command += "<vendor type='" + type + "' name='" + vendor + "' command='vendoropen' docserver='" + docServerLabel + "' ";
        if (_helper.isNotNullOrEmpty(hypercuberServerLabel)) {
            command += "hypercube='" + hypercuberServerLabel + "' ";
        }
        command += "/>";
        command += "</bgtcmd>";
        return command;
    }

    /**
     * Build fetch document command
     *
     * @param type
     * @param vendor
     * @param id
     * @return
     */
    public String buildFetchDocumentCommand(String type, String vendor, String id) {
        String command = "<bgtcmd>";
        command += "<fetch type='" + type + "' vendor='" + vendor + "' id='" + id + "' ";
        command += "/>";
        command += "</bgtcmd>";
        return command;
    }

    /**
     * Build Get document ids from a vendor command
     *
     * @param type
     * @param vendor
     * @param count
     * @param lastDocId
     * @return
     */
    public String buildGetDocumentIdsCommand(String type, String vendor, Long count, String lastDocId) {
        String command = null;        
           command = "<bgtcmd>";
           if (lastDocId != null) {
                command += "<vendor type='" + type + "' name='" + vendor + "' count='" + count + "' lastdocid='" + lastDocId + "' command='getdocids'";
           }else{
               command += "<vendor type='" + type + "' name='" + vendor + "' count='" + count + "' command='getdocids'";
           }
           command += "/>";
           command += "</bgtcmd>";           
        return command;
    }   
    
    /**
     * Build clarify command
     *
     * @param text
     * @return
     */
    public String buildClarifyCommand(String text) {
        String command = "<bgtcmd>";
        command += "<clarify>" + text;
        command += "</clarify>";
        command += "</bgtcmd>";
        return command;
    }

    /**
     * Frame the BGTXML TagCommand
     *
     * @param dataToTag
     * @param documentType
     * @param xmlHeader
     * @return
     * @throws Exception
     */
    public String BuildTagCommand(String dataToTag, docType documentType, String xmlHeader) throws Exception {
        String type = (documentType == docType.posting) ? "posting" : "resume";
        String command = xmlHeader + String.format("<bgtcmd><tag type='%s'></tag></bgtcmd>", type);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(command));
        Document doc = builder.parse(is);
        NodeList filterNode = doc.getElementsByTagName("tag");
        CDATASection cdata = doc.createCDATASection(dataToTag);
        filterNode.item(0).appendChild(cdata);
        return xmlHeader + _coreMapper.getOuterXml(doc);
    }

    /**
     * Custom boolean search command
     *
     * @param customBooleanCriteria
     * @return
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     * @throws javax.xml.transform.TransformerException
     */
    public String getCustomBooleanSearchCommand(String customBooleanCriteria)
            throws ParserConfigurationException, SAXException, IOException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader("<keyword></keyword>"));
        Document doc = builder.parse(is);
        NodeList filterNode = doc.getElementsByTagName("keyword");
        CDATASection cdata = doc.createCDATASection(customBooleanCriteria);
        filterNode.item(0).appendChild(cdata);
        String keywordXml = _coreMapper.getOuterXml(filterNode);
        return keywordXml;
    }
}
