// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.datarepository;

import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.criteria.ClientCriteria;
import com.bgt.lens.model.criteria.ClientapiCriteria;
import com.bgt.lens.model.criteria.ErrorLogCriteria;
import com.bgt.lens.model.criteria.LensSettingsCriteria;
import com.bgt.lens.model.criteria.ResourceCriteria;
import com.bgt.lens.model.criteria.SearchCommandDumpCriteria;
import com.bgt.lens.model.criteria.SearchFilterTypeCriteria;
import com.bgt.lens.model.criteria.SearchLookupCriteria;
import com.bgt.lens.model.criteria.SearchRegisterLogCriteria;
import com.bgt.lens.model.criteria.SearchVendorSettingsCriteria;
import com.bgt.lens.model.criteria.UsageLogCriteria;
import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.entity.AggregatedCoreLensDocument;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreAuthenticationlog;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreClientapi;
import com.bgt.lens.model.entity.CoreCustomskillsettings;
import com.bgt.lens.model.entity.CoreErrorlog;
import com.bgt.lens.model.entity.CoreFailoverlenssettings;
import com.bgt.lens.model.entity.CoreLensdocument;
import com.bgt.lens.model.entity.CoreLenssettings;
import com.bgt.lens.model.entity.CoreResources;
import com.bgt.lens.model.entity.CoreUsagecounter;
import com.bgt.lens.model.entity.CoreUsagelog;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchFilterType;
import com.bgt.lens.model.entity.SearchLookup;
import com.bgt.lens.model.entity.SearchRegisterlog;
import com.bgt.lens.model.entity.SearchVendorsettings;
import com.bgt.lens.model.rest.response.ApiResponse;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * Data repository service Interface
 */
public interface IDataRepositoryService {

    // CoreClient
    /**
     * Create client
     *
     * @param coreClient
     * @return
     * @throws Exception
     */
    ApiResponse createClient(CoreClient coreClient) throws Exception;

    /**
     * Update client
     *
     * @param coreClient
     * @param updatedCoreClient
     * @return
     * @throws Exception
     */
    ApiResponse updateClient(CoreClient coreClient, CoreClient updatedCoreClient) throws Exception;
    
     /**
     * Update client
     *
     * @param id
     * @param coreClient
     * @param updatedCoreClient
     * @return
     * @throws Exception
     */
    ApiResponse updateClient(Set<BigInteger> id, Map<BigInteger,CoreClient>coreClient,Map<BigInteger,CoreClient> updatedCoreClient) throws Exception;

    /**
     * Get client
     *
     * @param clientId
     * @return
     * @throws Exception
     */
    CoreClient getClientById(int clientId) throws Exception;

    /**
     * Delete client
     *
     * @param coreClient
     * @return
     * @throws Exception
     */
    ApiResponse deleteClient(CoreClient coreClient) throws Exception;

    /**
     * Get client by criteria
     *
     * @param clientCriteria
     * @return
     * @throws Exception
     */
    List<CoreClient> getClientByCriteria(ClientCriteria clientCriteria) throws Exception;

    // CoreApi
    /**
     * Create API
     *
     * @param coreApi
     * @return
     * @throws Exception
     */
    ApiResponse createApi(CoreApi coreApi) throws Exception;

    /**
     * Update API
     *
     * @param coreApi
     * @return
     * @throws Exception
     */
    ApiResponse updateApi(CoreApi coreApi) throws Exception;

    /**
     * Delete API
     *
     * @param coreApi
     * @return
     * @throws Exception
     */
    ApiResponse deleteApi(CoreApi coreApi) throws Exception;

    /**
     * Get API by Id
     *
     * @param apiId
     * @return
     * @throws Exception
     */
    CoreApi getApiById(int apiId) throws Exception;

    /**
     * Get API by criteria
     *
     * @param apiCriteria
     * @return
     * @throws Exception
     */
    List<CoreApi> getApiByCriteria(ApiCriteria apiCriteria) throws Exception;
    
    /**
     * Get API by criteria
     *
     * @param apiCriteria
     * @return
     * @throws Exception
     */
    List<CoreApi> getDuplicateApiByCriteria(ApiCriteria apiCriteria) throws Exception;

    // CoreClientapi
    /**
     * Create API to client
     *
     * @param coreClientapi
     * @return
     * @throws Exception
     */
    ApiResponse createClientApi(CoreClientapi coreClientapi) throws Exception;

    /**
     * Update client's API
     *
     * @param coreClientapi
     * @return
     * @throws Exception
     */
    ApiResponse updateClientApi(CoreClientapi coreClientapi) throws Exception;

    /**
     * Delete client's API
     *
     * @param coreClientapi
     * @return
     * @throws Exception
     */
    ApiResponse deleteClientApi(CoreClientapi coreClientapi) throws Exception;

    /**
     * Get Client's API by Id
     *
     * @param clientApiId
     * @return
     * @throws Exception
     */
    CoreClientapi getClientApiById(int clientApiId) throws Exception;

    /**
     * Get client's API by criteria
     *
     * @param clientapiCriteria
     * @return
     * @throws Exception
     */
    List<CoreClientapi> getClientApiByCriteria(ClientapiCriteria clientapiCriteria) throws Exception;

    // CoreLenssettings
    /**
     * Create new Lens Settings
     *
     * @param coreLenssettings
     * @return
     * @throws Exception
     */
    ApiResponse createLensSettings(CoreLenssettings coreLenssettings) throws Exception;

    /**
     * Update Lens settings
     *
     * @param coreLenssettings
     * @param coreCustomskillsettings
     * @return
     * @throws Exception
     */
    ApiResponse updateLensSettings(CoreLenssettings coreLenssettings, Set<CoreCustomskillsettings> coreCustomskillsettings) throws Exception;

    /**
     * Delete Lens settings
     *
     * @param coreLenssettings
     * @return
     * @throws Exception
     */
    ApiResponse deleteLensSettings(CoreLenssettings coreLenssettings) throws Exception;

    /**
     * Get Lens settings by Id
     *
     * @param settingsId
     * @return
     * @throws Exception
     */
    CoreLenssettings getLensSettingsById(int settingsId) throws Exception;
    
    /**
     * Get Failover Lens settings by Id
     *
     * @param settingsId
     * @return
     * @throws Exception
     */
    CoreFailoverlenssettings getFailoverLensSettingsById(int settingsId) throws Exception;

    /**
     * Get Lens settings by criteria
     *
     * @param lensSettingsCriteria
     * @return
     * @throws Exception
     */
    List<CoreLenssettings> getLensSettingsByCriteria(LensSettingsCriteria lensSettingsCriteria) throws Exception;

    // CoreAuthenticationlog
    /**
     * Create authentication log
     *
     * @param coreAuthenticationlog
     * @return
     * @throws Exception
     */
    ApiResponse createAuthenticationlog(CoreAuthenticationlog coreAuthenticationlog) throws Exception;

    /**
     * Update authentication log
     *
     * @param coreAuthenticationlog
     * @return
     * @throws Exception
     */
    ApiResponse updateAuthenticationlog(CoreAuthenticationlog coreAuthenticationlog) throws Exception;

    /**
     * Delete authentication log
     *
     * @param coreAuthenticationlog
     * @return
     * @throws Exception
     */
    ApiResponse deleteAuthenticationlog(CoreAuthenticationlog coreAuthenticationlog) throws Exception;

    /**
     * Get authentication log by Id
     *
     * @param authenticationlogId
     * @return
     * @throws Exception
     */
    CoreAuthenticationlog getAuthenticationlogById(int authenticationlogId) throws Exception;
    
    /**
     * Get authentication log by request Id
     *
     * @param requestId
     * @return
     * @throws Exception
     */
    CoreAuthenticationlog getAuthenticationlogByRequestId(String requestId) throws Exception;

    // CoreUsagecounter
    /**
     * Create usage counter
     *
     * @param coreUsagecounter
     * @return
     * @throws Exception
     */
    ApiResponse createUsagecounter(CoreUsagecounter coreUsagecounter) throws Exception;

    /**
     * Update usage counter
     *
     * @param coreUsagecounter
     * @return
     * @throws Exception
     */
    ApiResponse updateUsagecounter(CoreUsagecounter coreUsagecounter) throws Exception;

    /**
     * Delete usage counter
     *
     * @param coreUsagecounter
     * @return
     * @throws Exception
     */
    ApiResponse deleteUsagecounter(CoreUsagecounter coreUsagecounter) throws Exception;

    /**
     * Get usage counter by Id
     *
     * @param usageLogId
     * @return
     * @throws Exception
     */
    CoreUsagecounter getUsagecounterById(int usageLogId) throws Exception;

    /**
     * Get usage counter by id and client
     *
     * @param usageLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    CoreUsagecounter getUsagecounterById(int usageLogId, int clientId) throws Exception;

    /**
     * Get usage counter by criteria
     *
     * @param usagecounterCriteria
     * @return
     * @throws Exception
     */
    List<CoreUsagecounter> getUsagecounterByCriteria(UsagecounterCriteria usagecounterCriteria) throws Exception;

    // CoreUsagelog
    /**
     * Create usage log
     *
     * @param coreUsagelog
     * @return
     * @throws Exception
     */
    ApiResponse createUsagelog(CoreUsagelog coreUsagelog) throws Exception;

    /**
     * Update usage log
     *
     * @param coreUsagelog
     * @return
     * @throws Exception
     */
    ApiResponse updateUsagelog(CoreUsagelog coreUsagelog) throws Exception;

    /**
     * Delete usage log
     *
     * @param coreUsagelog
     * @return
     * @throws Exception
     */
    ApiResponse deleteUsagelog(CoreUsagelog coreUsagelog) throws Exception;

    /**
     * Get usage log by Id
     *
     * @param usageLogId
     * @return
     * @throws Exception
     */
    CoreUsagelog getUsagelogById(String usageLogId) throws Exception;

    /**
     * Get usage log by Id and client
     *
     * @param usageLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    CoreUsagelog getUsagelogById(String usageLogId, int clientId) throws Exception;

    /**
     * Get usage log by criteria
     *
     * @param usageLogCriteria
     * @return
     * @throws Exception
     */
    List<CoreUsagelog> getUsagelogByCriteria(UsageLogCriteria usageLogCriteria) throws Exception;

    // CoreLensdocument
    /**
     * Create Lens document
     *
     * @param coreLensdocument
     * @return
     * @throws Exception
     */
    ApiResponse createLensdocument(CoreLensdocument coreLensdocument) throws Exception;

    /**
     * Update Lens document
     *
     * @param coreLensdocument
     * @return
     * @throws Exception
     */
    ApiResponse updateLensdocument(CoreLensdocument coreLensdocument) throws Exception;
    
    /**
     * Update document dump count
     *
     * @param aggDocumentDumpCriteria
     * @return
     * @throws Exception
     */
    ApiResponse updateDocumentDumpCount(AggregatedCoreLensDocument aggLensDocument) throws Exception;

    /**
     * Delete Lens document
     *
     * @param coreLensdocument
     * @return
     * @throws Exception
     */
    ApiResponse deleteLensdocument(CoreLensdocument coreLensdocument) throws Exception;

    /**
     * Get Lens document by Id
     *
     * @param lensDocumentId
     * @return
     */
    CoreLensdocument getLensdocumentById(int lensDocumentId);

    // CoreErrorlog
    /**
     * Create error log
     *
     * @param coreErrorlog
     * @return
     * @throws Exception
     */
    ApiResponse createErrorlog(CoreErrorlog coreErrorlog) throws Exception;

    /**
     * Update error log
     *
     * @param coreErrorlog
     * @return
     * @throws Exception
     */
    ApiResponse updateErrorlog(CoreErrorlog coreErrorlog) throws Exception;

    /**
     * Delete error log
     *
     * @param coreErrorlog
     * @return
     * @throws Exception
     */
    ApiResponse deleteErrorlog(CoreErrorlog coreErrorlog) throws Exception;

    /**
     * Get error log by Id
     *
     * @param errorlogId
     * @return
     * @throws Exception
     */
    CoreErrorlog getErrorlogById(String errorlogId) throws Exception;

    /**
     * Get error log by id and client
     *
     * @param errorlogId
     * @param clientId
     * @return
     * @throws Exception
     */
    CoreErrorlog getErrorlogById(String errorlogId, int clientId) throws Exception;

    /**
     * Get error log by criteria
     *
     * @param usageLogCriteria
     * @return
     * @throws Exception
     */
    List<CoreErrorlog> getErrorlogByCriteria(ErrorLogCriteria usageLogCriteria) throws Exception;

    // CoreResources
    /**
     * Create resource
     *
     * @param coreResources
     * @return
     * @throws Exception
     */
    ApiResponse createResource(CoreResources coreResources) throws Exception;

    /**
     * Update resource
     *
     * @param coreResources
     * @return
     * @throws Exception
     */
    ApiResponse updateResource(CoreResources coreResources) throws Exception;

    /**
     * Delete resource
     *
     * @param coreResources
     * @return
     * @throws Exception
     */
    ApiResponse deleteResource(CoreResources coreResources) throws Exception;

    /**
     * Get resource by Id
     *
     * @param resourceid
     * @return
     * @throws Exception
     */
    CoreResources getResourceById(int resourceid) throws Exception;

    /**
     * Get resource by criteria
     *
     * @param resourceCriteria
     * @return
     * @throws Exception
     */
    List<CoreResources> getResourceByCriteria(ResourceCriteria resourceCriteria) throws Exception;
    
    /**
     * Get resource by criteria
     *
     * @param resourceCriteria
     * @return
     * @throws Exception
     */
    List<CoreResources> getResourceWithoutApiByCriteria(ResourceCriteria resourceCriteria) throws Exception;
    
    /**
     * Create new Lens Settings
     *
     * @param searchVendorSettings
     * @return
     * @throws Exception
     */
    ApiResponse createVendor(SearchVendorsettings searchVendorSettings) throws Exception;

    /**
     * Update Lens settings
     *
     * @param searchVendorSettings
     * @return
     * @throws Exception
     */
    ApiResponse updateVendor(SearchVendorsettings searchVendorSettings) throws Exception;

    /**
     * Delete Lens settings
     *
     * @param searchVendorSettings
     * @return
     * @throws Exception
     */
    ApiResponse deleteVendor(SearchVendorsettings searchVendorSettings) throws Exception;
    
    /**
     * Get vendor by Id
     * @param vendorId
     * @return
     * @throws Exception 
     */
    SearchVendorsettings getVendorById(int vendorId) throws Exception;
    
    /**
     * Get vendor list by criteria
     *
     * @param searchVendorSettingsCriteria
     * @return
     * @throws Exception
     */
    List<SearchVendorsettings> getVendorsByCriteria(SearchVendorSettingsCriteria searchVendorSettingsCriteria) throws Exception;

    /**
     * Get search lookup by Criteria
     * @param searchLookupCriteria
     * @return
     * @throws Exception 
     */
    List<SearchLookup> getSearchLookupsByCriteria(SearchLookupCriteria searchLookupCriteria) throws Exception;
    
    /**
     * Add search command dump to database
     * @param searchCommanddump
     * @return
     * @throws Exception 
     */
    ApiResponse addSearchCommandDump(SearchCommanddump searchCommanddump) throws Exception;
    
    /**
     * Add register log to database
     * @param searchRegisterLog
     * @return
     * @throws Exception 
     */
    ApiResponse addRegisterLog(SearchRegisterlog searchRegisterLog) throws Exception;
    
    /**
     * Add register log to database
     * @param registerId
     * @param clientId
     * @return
     * @throws Exception 
     */
    SearchRegisterlog getRegisterLogById(int registerId, int clientId) throws Exception;
    
    /**
     * Get register log by criteria
     *
     * @param searchRegisterLogCriteria
     * @return
     * @throws Exception
     */
    List<SearchRegisterlog> getRegisterLogByCriteria(SearchRegisterLogCriteria searchRegisterLogCriteria) throws Exception;
    
    /**
     * Get register log count
     *
     * @return
     * @throws Exception
     */
    int getRegisterLogCount() throws Exception;

    /**
     * Add register log to database
     * @param dumpId
     * @param clientId
     * @return
     * @throws Exception 
     */
    SearchCommanddump getSearchCommandDumpById(int dumpId, int clientId) throws Exception;
    
    /**
     * Get search command dump by criteria
     *
     * @param searchCommandDumpCriteria
     * @return
     * @throws Exception
     */
    List<SearchCommanddump> getSearchCommandDumpByCriteria(SearchCommandDumpCriteria searchCommandDumpCriteria) throws Exception;
    
    /**
     * Add search lookup
     * @param lookup
     * @return 
     */
    ApiResponse addLookup(SearchLookup lookup);
    
    /**
     * Get Filter ID By Name
     * @param searchFilterTypeCriteria
     * @return
     */
    List<SearchFilterType> getFilterIDByName(SearchFilterTypeCriteria searchFilterTypeCriteria); 
    
}
