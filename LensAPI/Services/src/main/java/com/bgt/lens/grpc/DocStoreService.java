// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: docStoreService.proto

package com.bgt.lens.grpc;

public final class DocStoreService {
  private DocStoreService() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ServiceRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ServiceRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ServiceResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ServiceResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\025docStoreService.proto\"m\n\016ServiceReques" +
      "t\022\030\n\020lensApiRequestId\030\001 \001(\t\022\016\n\006base64\030\002 " +
      "\001(\t\022\021\n\textension\030\003 \001(\t\022\016\n\006getHtm\030\004 \001(\010\022\016" +
      "\n\006getRtf\030\005 \001(\010\"\'\n\017ServiceResponse\022\024\n\014can" +
      "onCommand\030\001 \001(\t2H\n\024DocumentStoreService\022" +
      "0\n\013serviceMain\022\017.ServiceRequest\032\020.Servic" +
      "eResponseB\020\n\014com.bgt.grpcP\001b\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_ServiceRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_ServiceRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ServiceRequest_descriptor,
        new java.lang.String[] { "LensApiRequestId", "Base64", "Extension", "GetHtm", "GetRtf", });
    internal_static_ServiceResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_ServiceResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ServiceResponse_descriptor,
        new java.lang.String[] { "CanonCommand", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
