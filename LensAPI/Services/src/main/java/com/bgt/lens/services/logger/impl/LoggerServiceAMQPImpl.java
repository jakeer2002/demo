// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.logger.impl;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.config.ApiConfiguration;
import com.bgt.lens.exception.ApiErrors;
import com.bgt.lens.exception.ApiException;
import com.bgt.lens.helpers.CoreMappers;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.helpers.ResolverMappers;
import com.bgt.lens.helpers.Validator;
import com.bgt.lens.model.adminservice.request.GetErrorLogsRequest;
import com.bgt.lens.model.adminservice.request.GetUsageCounterRequest;
import com.bgt.lens.model.adminservice.request.GetUsageLogsRequest;
import com.bgt.lens.model.criteria.ApiCriteria;
import com.bgt.lens.model.criteria.ErrorLogCriteria;
import com.bgt.lens.model.criteria.SearchFilterTypeCriteria;
import com.bgt.lens.model.criteria.UsageLogCriteria;
import com.bgt.lens.model.criteria.UsagecounterCriteria;
import com.bgt.lens.model.entity.CoreApi;
import com.bgt.lens.model.entity.CoreClient;
import com.bgt.lens.model.entity.CoreErrorlog;
import com.bgt.lens.model.entity.CoreUsagecounter;
import com.bgt.lens.model.entity.CoreUsagelog;
import com.bgt.lens.model.entity.SearchCommanddump;
import com.bgt.lens.model.entity.SearchFilterLog;
import com.bgt.lens.model.entity.SearchFilterType;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.datarepository.IDataRepositoryService;
import com.bgt.lens.services.datarepository.impl.DataRepositoryServiceImpl;
import com.bgt.lens.services.logger.ILoggerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Future;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.AsyncResult;

/**
 *
 * Logger service implementation
 */
public class LoggerServiceAMQPImpl implements ILoggerService {

    private static final Logger LOGGER = LogManager.getLogger(DataRepositoryServiceImpl.class);
    private final Helper _helper = new Helper();
    private final Validator _validator = new Validator();
    private final CoreMappers _coreMappers = new CoreMappers();
    private final ResolverMappers _resolverMappers = new ResolverMappers();
    private ApiConfiguration apiConfig;
    
    @Value("${ElasticSearchNodes}")
    private String elasticSearchNodeList;
    
    @Value("${ElasticSearchUsageLogIndex}")
    private String elasticSearchUsageLogIndex;
    
    @Value("${ElasticSearchUsageLogType}")
    private String elasticSearchUsageLogType;
    
    @Value("${ElasticSearchErrorLogIndex}")
    private String elasticSearchErrorLogIndex;
    
    @Value("${ElasticSearchErrorLogType}")
    private String elasticSearchErrorLogType;
    
    @Value("${ElasticSearchUsageLogDataIndex}")
    private String elasticSearchUsageLogDataIndex;
    
    @Value("${ElasticSearchUsageLogDataType}")
    private String elasticSearchUsageLogDataType;
    
    @Value("${ElasticSearchAdvancedLogIndex}")
    private String elasticSearchAdvancedLogIndex;
    
    @Value("${ElasticSearchAdvancedLogType}")
    private String elasticSearchAdvancedLogType;
    
    @Value("${ElasticSearchAuthenticationLogType}")
    private String elasticSearchAuthenticationLogType;
    
    @Value("${ElasticSearchUsageLogRecentIndexAlias}")
    private String elasticSearchUsageLogRecentIndexAlias;
    
    @Value("${ElasticSearchUsageLogRecentIndexType}")
    private String elasticSearchUsageLogRecentIndexType;
    
    @Value("${EnableAutoDateBasedUsageLog}")
    private Boolean enableAutoDateBasedUsageLog;
    
    private IDataRepositoryService dataRepositoryService;
    
    @Autowired
    private AsyncLogStorageService asyncLogStorageService;
    
    Client client = null;
    
    public LoggerServiceAMQPImpl(ElasticSearchTransportClient esTransportClient){
        if(_helper.isNotNull(esTransportClient) &&  _helper.isNotNull(esTransportClient.getClient())){
            client = esTransportClient.getClient();
        }
        else{
            throw new ApiException("", "Unable to initialize elasticsearch client", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Set data repository service
     *
     * @param dataRepositoryService
     */
    public void setDataRepositoryService(IDataRepositoryService dataRepositoryService) {
        this.dataRepositoryService = dataRepositoryService;
    }

    /**
     * set ApiConfig
     *
     * @param apiConfig
     */
    public void setapiConfig(ApiConfiguration apiConfig) {
        this.apiConfig = apiConfig;
    }

    /**
     * Add authentication log
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addAuthenticationLog(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addAuthenticationLogToAMQPAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_AUTHENTICATIONLOG_ERROR));
        }
    }

    /**
     * Add Lens document
     *
     * @param context
     * @param binaryData
     * @param docType
     * @param instanceType
     * @param locale
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addLensDocument(ApiContext context, String binaryData, String docType, String instanceType, String locale) throws Exception {
        if (_helper.isNotNull(context)) {
            LOGGER.info("Add LENS document dump to database");
            asyncLogStorageService.addLensDocumentAsync(context, binaryData, docType, instanceType, locale);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_LENSDOCUMENTDUMP_ERROR));
        }
    }

    @Override
    public ApiResponse addSearchCommandDump(ApiContext context) throws Exception {

        if (_helper.isNotNull(context)) {
            SearchFilterTypeCriteria searchFilterTypeCriteria = new SearchFilterTypeCriteria();
            SearchCommanddump searchCommandDump = new SearchCommanddump();
            List<SearchFilterType> searchFilterTypeList = null;
            Set<SearchFilterLog> searchFilterLogList = new HashSet<SearchFilterLog>();
            SearchFilterLog searchFilterLog;

            searchCommandDump.setRequestId(context.getRequestId().toString());

            if (_helper.isNotNull(context.getClient()) && _helper.isNotNull(context.getClient().getId())) {
                searchCommandDump.setClientId(context.getClient().getId());
            } else {
                searchCommandDump.setClientId(0);
            }
            if (_helper.isNotNull(context.getLensSettingsId())) {
                searchCommandDump.setLensSettingsId(context.getLensSettingsId());
            } else {
                searchCommandDump.setLensSettingsId(0);
            }
            if (_helper.isNotNull(context.getVendorId())) {
                searchCommandDump.setVendorId(context.getVendorId());
            } else {
                searchCommandDump.setVendorId(0);
            }
            if (_helper.isNotNull(context.getSearchCommand())) {
                searchCommandDump.setSearchCommand(context.getSearchCommand());
            } else {
                searchCommandDump.setSearchCommand("");
            }
            if (_helper.isNotNull(context.getLocale())) {
                searchCommandDump.setLocale(context.getLocale());
            } else {
                searchCommandDump.setLocale("");
            }
            if (_helper.isNotNull(context.getInstanceType())) {
                searchCommandDump.setInstanceType(context.getInstanceType());
            } else {
                searchCommandDump.setInstanceType("");
            }
            if (_helper.isNotNull(context.getDocType())) {
                searchCommandDump.setDocumentType(context.getDocType());
            } else {
                searchCommandDump.setDocumentType("");
            }

            searchCommandDump.setCreatedOn(Date.from(Instant.now()));

            if (_helper.isNotNull(context.getFilterType())) {
                //get FilterId from Search Filter Type table
                searchFilterTypeCriteria.setFilterType(context.getFilterType());

                searchFilterTypeList = dataRepositoryService.getFilterIDByName(searchFilterTypeCriteria);
                if (_helper.isNotNull(searchFilterTypeList)) {
                    for (int index = 0; index < searchFilterTypeList.size(); index++) {
                        SearchFilterType searchFilterTypeObj = new SearchFilterType();
                        Object obj = searchFilterTypeList.get(index);
                        searchFilterTypeObj.setId((Integer) obj);
                        searchFilterLogList.add(new SearchFilterLog(searchCommandDump, searchFilterTypeObj));
                    }
                    searchCommandDump.setSearchFilterLogs(searchFilterLogList);
                }
            } else {
                searchCommandDump.setSearchFilterLogs(null);
            }

            ApiResponse response = dataRepositoryService.addSearchCommandDump(searchCommandDump);
            response.responseData = searchCommandDump;
            return response;
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_SEARCHCOMMANDDUMP_ERROR));
        }
    }

    @Override
    public Future<Boolean> addRegisterLog(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addSearchRegisterLogAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_REGISTERLOG_ERROR));
        }
    }

    /**
     * Add usage log
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addUsageLog(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addUsageLogToAMQPAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_USAGELOG_ERROR));
        }
    }

    /**
     * Add usage log
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addErrorLog(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addErrorLogToAMQPAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_ERRORLOG_ERROR));
        }
    }

    /**
     * Add usage log
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> addAdvancedLog(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.addAdvancedLogToAMQPAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.ADD_ADVANCEDLOG_ERROR));
        }
    }
    
    /**
     * Update usage counter
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public Future<Boolean> updateUsageCounter(ApiContext context) throws Exception {
        if (_helper.isNotNull(context)) {
            asyncLogStorageService.updateUsageCounterAsync(context);
            return new AsyncResult<>(true);
        } else {
            throw new Exception(_helper.getErrorMessageWithURL(ApiErrors.UPDATE_USAGECOUNTER_ERROR));
        }
    }

    /**
     * Get usage logs
     *
     * @param usageLog
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getUsageLogs(GetUsageLogsRequest usageLog, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        UsageLogCriteria usageLogCriteria = new UsageLogCriteria();
        String consumerKey = "";
        // TODO : move it to validator
        try {

            if (_helper.isNotNullOrEmpty(usageLog.getFrom())) {
                usageLogCriteria.setFrom(_helper.parseDate(usageLog.getFrom()));
            }

            if (_helper.isNotNullOrEmpty(usageLog.getTo())) {
                usageLogCriteria.setTo(new LocalDate(_helper.parseDate(usageLog.getTo())).toLocalDateTime(LocalTime.MIDNIGHT).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
            }
            
            //check client Id
            if (!_helper.isNull(clientId)) {
                CoreClient client = dataRepositoryService.getClientById(clientId);
                if (client != null) {
                    consumerKey = client.getClientKey();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID), HttpStatus.BAD_REQUEST);
                }
            }

            Integer apiId = null;
            //check api Id
            if (!_helper.isNullOrEmpty(usageLog.getApiKey())) {
                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(usageLog.getApiKey());

                List<CoreApi> apiList = dataRepositoryService.getApiByCriteria(criteria);

                if (apiList != null && apiList.size() > 0) {
                    apiId = apiList.get(0).getId();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }
            }

            // validate http method
            if (!_helper.isNullOrEmpty(usageLog.getMethod())) {
                if (!_helper.isValidHttpMethod(usageLog.getMethod())) {
                    LOGGER.error("Invalid http method : " + usageLog.getMethod());
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_HTTP_METHOD), HttpStatus.BAD_REQUEST);
                }
            }

            // validate http method
            if (!_helper.isNullOrEmpty(usageLog.getResource())) {
                if (!_helper.isValidAPIResource(usageLog.getResource())) {
                    LOGGER.error("Invalid resource name : " + usageLog.getResource());
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_RESOURCE_NAME), HttpStatus.BAD_REQUEST);
                }
            }
            
            usageLogCriteria = _coreMappers.createUsageLogsCriteria(usageLogCriteria, usageLog, null, consumerKey, null, usageLog.getApiKey());
            
            SearchRequestBuilder rb = client.prepareSearch(elasticSearchUsageLogRecentIndexAlias)
                .setTypes(elasticSearchUsageLogRecentIndexType)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
            
            BoolQueryBuilder boolQuery = new BoolQueryBuilder();
            
//            String query = "{\"query\":{";
            
            if(_helper.isNotNull(usageLogCriteria.getFrom())){
                DateFormat advformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                boolQuery.filter(QueryBuilders.rangeQuery("inTime").format("yyyy-MM-dd HH:mm:ss.SSS")
                    .gte(advformatter.format(usageLogCriteria.getFrom())));
//                query += "\"bool\":{\"filter\":{\"range\":{\"inTime\":{\"from\":\"" 
//                        + advformatter.format(usageLogCriteria.getFrom()) + "\", \"format\":\"\"yyyy-MM-dd HH:mm:ss.SSS\"\"";
            }
            if(_helper.isNotNull(usageLogCriteria.getTo())){
                DateFormat advformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                boolQuery.filter(QueryBuilders.rangeQuery("outTime").format("yyyy-MM-dd HH:mm:ss.SSS")
                    .lte(advformatter.format(usageLogCriteria.getTo())));
            } 
            
            if (_helper.isNotNullOrEmpty(consumerKey)) {
                boolQuery.filter(QueryBuilders.matchQuery("consumerKey", usageLogCriteria.getConsumerKey()));
            }

            if (_helper.isNotNullOrEmpty(usageLogCriteria.getApi())) {
                boolQuery.filter(QueryBuilders.matchQuery("api", usageLogCriteria.getApi()));
            }

            if (!_helper.isNullOrEmpty(usageLogCriteria.getMethod())) {
                boolQuery.filter(QueryBuilders.matchQuery("method", usageLogCriteria.getMethod().toLowerCase()));
            }

            if (!_helper.isNullOrEmpty(usageLogCriteria.getResource())) {
                boolQuery.filter(QueryBuilders.matchQuery("resource", usageLogCriteria.getResource().toLowerCase()));
            }

            if (_helper.isNotNull(usageLogCriteria.isRaisedError())) {
                boolQuery.filter(QueryBuilders.termQuery("raisedError", usageLogCriteria.isRaisedError()));
            }
            
            rb.addSort("inTime", SortOrder.DESC);
            
            rb.setQuery(boolQuery);
            
            rb.setSize(apiConfig.getDefaultResultLimit());
                        
            SearchResponse searchResponse = rb
                .execute()
                .actionGet();
            
            List<CoreUsagelog> coreUsageLogList = new ArrayList<>();
            
            if(searchResponse.getHits() != null && searchResponse.getHits().getHits() != null && searchResponse.getHits().getHits().length >0){
                for(SearchHit searchHit : searchResponse.getHits()){
                    if(searchResponse.getHits().getHits().length <= 0) {
                        LOGGER.info("Usage log does not exist");
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_LOG_NOT_EXIST);
                        return response;
                    }
                    String sourceAsString = searchHit.getSourceAsString();
                    if(_helper.isNotNullOrEmpty(sourceAsString)){
                        com.bgt.lens.model.es.entity.CoreUsagelog usageLogList = (com.bgt.lens.model.es.entity.CoreUsagelog)new ObjectMapper().readValue(sourceAsString, com.bgt.lens.model.es.entity.CoreUsagelog.class);
                        if(usageLogList != null){
                            CoreUsagelog coreUsageLog = new CoreUsagelog();
                            coreUsageLog.setRequestId(searchHit.getId());
                            coreUsageLog.setApi(usageLogList.getApi());
                            coreUsageLog.setConsumerKey(usageLogList.getConsumerKey());
                            coreUsageLog.setClientAddress(usageLogList.getClientAddress());
                            DateFormat advformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                            advformatter.setTimeZone(TimeZone.getDefault());
//                            String inTime = advformatter.format(new Date(usageLogList.getInTime()));
//                            String outTime = advformatter.format(new Date(usageLogList.getOutTime()));
                            String inTime = advformatter.format(usageLogList.getInTime());
                            String outTime = advformatter.format(usageLogList.getOutTime());
                            coreUsageLog.setInTime(advformatter.parse(inTime));
                            coreUsageLog.setOutTime(advformatter.parse(outTime));
                            coreUsageLog.setInstanceType(usageLogList.getInstanceType());
                            coreUsageLog.setLocale(usageLogList.getLocale());
                            coreUsageLog.setMethod(usageLogList.getMethod());
                            coreUsageLog.setMilliseconds(usageLogList.getElapsedTime());
                            coreUsageLog.setParseVariant(usageLogList.getParseVariant());
                            coreUsageLog.setRaisedError(usageLogList.isRaisedError());
                            coreUsageLog.setResource(usageLogList.getResource());
                            coreUsageLog.setServerAddress(usageLogList.getServerAddress());
                            coreUsageLog.setUri(usageLogList.getUri());
                            
                            coreUsageLogList.add(coreUsageLog);
                        }
                        else{
                            LOGGER.info("Usage log does not exist");
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_LOG_NOT_EXIST);
                            return response;
                        }
                    }
                }
            }
            else{
                LOGGER.info("Usage log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_LOG_NOT_EXIST);
                return response;
            }
            

//            usageLogCriteria = _coreMappers.createUsageLogsCriteria(usageLogCriteria, usageLog, clientId, apiId);

//            List<CoreUsagelog> coreUsageLogList = dataRepositoryService.getUsagelogByCriteria(usageLogCriteria);

            if (coreUsageLogList == null || coreUsageLogList.isEmpty()) {
                LOGGER.info("Usage log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_LOG_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createUsageLogResponse(coreUsageLogList);
            }
        } catch (ParseException ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage log. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Get usage logs by Id
     *
     * @param usageLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getUsageLogById(String usageLogId, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {         
            String consumerKey = "";
            
            //check api Id
            if (!_helper.isNull(clientId)) {
                CoreClient client = dataRepositoryService.getClientById(clientId);
                if (client != null) {
                    consumerKey = client.getClientKey();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID), HttpStatus.BAD_REQUEST);
                }
            }
            
            BoolQueryBuilder boolQuery = new BoolQueryBuilder();
            if (_helper.isNotNullOrEmpty(consumerKey)) {
                boolQuery.filter(QueryBuilders.matchQuery("consumerKey", consumerKey));
            }
            
            boolQuery.filter(QueryBuilders.termQuery("_id", usageLogId));
            
            SearchResponse searchResponse = client.prepareSearch(elasticSearchUsageLogRecentIndexAlias)
                .setTypes(elasticSearchUsageLogRecentIndexType)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(boolQuery)
                .execute()
                .actionGet();
            
            if(searchResponse.getHits() != null && searchResponse.getHits().getHits() != null && searchResponse.getHits().getHits().length > 0){
                for(SearchHit searchHit : searchResponse.getHits()){
                    if(searchResponse.getHits().getHits().length <= 0) {
                        LOGGER.info("Usage log does not exist");
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_LOG_NOT_EXIST);
                        return response;
                    }
                    String sourceAsString = searchHit.getSourceAsString();
                    if(_helper.isNotNullOrEmpty(sourceAsString)){
                        com.bgt.lens.model.es.entity.CoreUsagelog usageLog = (com.bgt.lens.model.es.entity.CoreUsagelog)new ObjectMapper().readValue(sourceAsString, com.bgt.lens.model.es.entity.CoreUsagelog.class);
                        if(usageLog != null){
                            CoreUsagelog coreUsageLog = new CoreUsagelog();
                            coreUsageLog.setApi(usageLog.getApi());
                            coreUsageLog.setConsumerKey(usageLog.getConsumerKey());
                            coreUsageLog.setClientAddress(usageLog.getClientAddress());
                            DateFormat advformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                            advformatter.setTimeZone(TimeZone.getDefault());
//                            String inTime = advformatter.format(new Date(usageLog.getInTime()));
//                            String outTime = advformatter.format(new Date(usageLog.getOutTime()));
                            String inTime = advformatter.format(usageLog.getInTime());
                            String outTime = advformatter.format(usageLog.getOutTime());
                            coreUsageLog.setInTime(advformatter.parse(inTime));
                            coreUsageLog.setOutTime(advformatter.parse(outTime));
                            coreUsageLog.setInstanceType(usageLog.getInstanceType());
                            coreUsageLog.setLocale(usageLog.getLocale());
                            coreUsageLog.setMethod(usageLog.getMethod());
                            coreUsageLog.setMilliseconds(usageLog.getElapsedTime());
                            coreUsageLog.setParseVariant(usageLog.getParseVariant());
                            coreUsageLog.setRaisedError(usageLog.isRaisedError());
                            coreUsageLog.setRequestId(searchHit.getId());
                            coreUsageLog.setResource(usageLog.getResource());
                            coreUsageLog.setServerAddress(usageLog.getServerAddress());
                            coreUsageLog.setUri(usageLog.getUri());
                            
                            response.status = true;
                            response.responseData = _coreMappers.createUsageLogResponse(coreUsageLog);
                        }
                        else{
                            LOGGER.info("Usage log does not exist");
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_LOG_NOT_EXIST);
                            return response;
                        }
                    }
                }
            }
            else{
                LOGGER.info("Usage log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_LOG_NOT_EXIST);
                return response;
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage log by id. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get error log by criteria
     *
     * @param errorLogs
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getErrorLogs(GetErrorLogsRequest errorLogs, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            Integer apiId = null;
            String consumerKey = "";
            //check client Id
            if (!_helper.isNull(clientId)) {
                CoreClient client = dataRepositoryService.getClientById(clientId);
                if (client != null) {
                    consumerKey = client.getClientKey();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID), HttpStatus.BAD_REQUEST);
                }
            }
            
            //check api Id
            if (!_helper.isNullOrEmpty(errorLogs.getApiKey())) {
                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(errorLogs.getApiKey());

                List<CoreApi> apiList = dataRepositoryService.getApiByCriteria(criteria);

                if (apiList != null && apiList.size() > 0) {
                    apiId = apiList.get(0).getId();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }
            }

            ErrorLogCriteria errorLogCriteria = _coreMappers.createErrorLogsCriteria(errorLogs, null, consumerKey, null, errorLogs.getApiKey());
            
            SearchRequestBuilder rb = client.prepareSearch(elasticSearchErrorLogIndex)
                .setTypes(elasticSearchErrorLogType)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
            
            BoolQueryBuilder boolQuery = new BoolQueryBuilder();
            
//            String query = "{\"query\":{";
            
            if (_helper.isNotNullOrEmpty(errorLogCriteria.getRequestId())) {
                boolQuery.filter(QueryBuilders.termQuery("_id", errorLogCriteria.getRequestId()));
            }
            
            if (_helper.isNotNullOrEmpty(errorLogCriteria.getConsumerKey())) {
                boolQuery.filter(QueryBuilders.matchQuery("consumerKey", errorLogCriteria.getConsumerKey()));
            }
            
            if (_helper.isNotNullOrEmpty(errorLogCriteria.getApi())) {
                boolQuery.filter(QueryBuilders.termQuery("api", errorLogCriteria.getApi()));
            }
            
            if (_helper.isNotNull(errorLogCriteria.getStatusCode()) && errorLogCriteria.getStatusCode()> 0) {
                boolQuery.filter(QueryBuilders.termQuery("statusCode", errorLogCriteria.getStatusCode()));
            }
            
            rb.setQuery(boolQuery);
            
            rb.setSize(apiConfig.getDefaultResultLimit());
                        
            SearchResponse searchResponse = rb
                .execute()
                .actionGet();
            
            List<CoreErrorlog> coreErrorLogList = new ArrayList<>();
            
            if(searchResponse.getHits() != null && searchResponse.getHits().getHits() != null && searchResponse.getHits().getHits().length >0){
                for(SearchHit searchHit : searchResponse.getHits()){
                    if(searchResponse.getHits().getHits().length <= 0) {
                        LOGGER.info("Error log does not exist");
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
                        return response;
                    }
                    String sourceAsString = searchHit.getSourceAsString();
                    if(_helper.isNotNullOrEmpty(sourceAsString)){
                        com.bgt.lens.model.es.entity.CoreErrorlog errorLogList = (com.bgt.lens.model.es.entity.CoreErrorlog)new ObjectMapper().readValue(sourceAsString, com.bgt.lens.model.es.entity.CoreErrorlog.class);
                        if(errorLogList != null){
                            CoreErrorlog coreErrorLog = new CoreErrorlog();
                            coreErrorLog.setApi(errorLogList.getApi());
                            coreErrorLog.setConsumerKey(errorLogList.getConsumerKey());
                            coreErrorLog.setException(errorLogList.getException());
                            DateFormat advformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                            advformatter.setTimeZone(TimeZone.getDefault());
//                            String createdOn = advformatter.format(new Date(errorLogList.getCreatedOn()));
//                            coreErrorLog.setCreatedOn(advformatter.parse(errorLogList.getCreatedOn()));
                            coreErrorLog.setCreatedOn(errorLogList.getCreatedOn());
                            coreErrorLog.setRequestId(searchHit.getId());
                            coreErrorLog.setStatusCode(errorLogList.getStatusCode());
                            coreErrorLog.setTraceLog(errorLogList.getTraceLog());
                            coreErrorLog.setUserMessage(errorLogList.getTraceLog());
                            coreErrorLogList.add(coreErrorLog);
                        }
                        else{
                            LOGGER.info("Error log does not exist");
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
                            return response;
                        }
                    }
                }
            }
            else{
                LOGGER.info("Error log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
                return response;
            }

            if (coreErrorLogList == null || coreErrorLogList.isEmpty()) {
                LOGGER.info("Error log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createErrorLogResponse(coreErrorLogList);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get error log. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get error log by Id
     *
     * @param errorLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getErrorLogById(String errorLogId, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            String consumerKey = "";
            //check api Id
            if (!_helper.isNull(clientId)) {
                CoreClient client = dataRepositoryService.getClientById(clientId);
                if (client != null) {
                    consumerKey = client.getClientKey();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_CLIENT_ID), HttpStatus.BAD_REQUEST);
                }
            }
            
            BoolQueryBuilder boolQuery = new BoolQueryBuilder();
            if (_helper.isNotNullOrEmpty(consumerKey)) {
                boolQuery.filter(QueryBuilders.matchQuery("consumerKey", consumerKey));
            }
            
            boolQuery.filter(QueryBuilders.termQuery("_id", errorLogId));
            SearchResponse searchResponse = client.prepareSearch(elasticSearchErrorLogIndex)
                .setTypes(elasticSearchErrorLogType)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(boolQuery)
                .execute()
                .actionGet();
            
            if(searchResponse.getHits() != null && searchResponse.getHits().getHits() != null && searchResponse.getHits().getHits().length > 0){
                for(SearchHit searchHit : searchResponse.getHits()){
                    if(searchResponse.getHits().getHits().length <= 0) {
                        LOGGER.info("Error log does not exist");
                        response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
                        return response;
                    }
                    String sourceAsString = searchHit.getSourceAsString();
                    if(_helper.isNotNullOrEmpty(sourceAsString)){
                        CoreErrorlog errorLog = (CoreErrorlog)new ObjectMapper().readValue(sourceAsString, CoreErrorlog.class);
                        errorLog.setRequestId(searchHit.getId());
                        if (errorLog == null) {
                            LOGGER.info("Error log does not exist");
                            response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
                            return response;
                        } else {
                            response.status = true;
                            response.responseData = _coreMappers.createErrorLogResponse(errorLog);
                        }
                    }
                }
            }
            else{
                LOGGER.info("Error log does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
                return response;
            }
            
//            CoreErrorlog coreErrorLog = dataRepositoryService.getErrorlogById(errorLogId, clientId);
//
//            if (coreErrorLog == null) {
//                LOGGER.info("Error log does not exist");
//                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.ERROR_LOG_NOT_EXIST);
//                return response;
//            } else {
//                response.status = true;
//                response.responseData = _coreMappers.createErrorLogResponse(coreErrorLog);
//            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage counter. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }

    /**
     * Get usage counter
     *
     * @param usageCounter
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getUsageCounter(GetUsageCounterRequest usageCounter, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            Integer apiId = null;
            //check api Id
            if (!_helper.isNullOrEmpty(usageCounter.getApiKey())) {
                ApiCriteria criteria = new ApiCriteria();
                criteria.setApiKey(usageCounter.getApiKey());

                List<CoreApi> apiList = dataRepositoryService.getApiByCriteria(criteria);

                if (apiList != null && apiList.size() > 0) {
                    apiId = apiList.get(0).getId();
                } else {
                    throw new ApiException("", _helper.getErrorMessageWithURL(ApiErrors.INVALID_API_KEY), HttpStatus.BAD_REQUEST);
                }
            }

            UsagecounterCriteria usageCounterCriteria = _coreMappers.createUsageCounterCriteria(usageCounter, clientId, apiId);

            List<CoreUsagecounter> coreUsageCounterList = dataRepositoryService.getUsagecounterByCriteria(usageCounterCriteria);

            if (coreUsageCounterList == null) {
                LOGGER.info("Usage counter does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_COUNTER_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createUsageCounterResponse(coreUsageCounterList);
            }
        } catch (ParseException ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage counter. Exception - " + ex.getMessage());
            throw new ApiException(ExceptionUtils.getStackTrace(ex), _helper.getErrorMessageWithURL(ApiErrors.INVALID_DATE_FORMAT), HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Get usage counter by id
     *
     * @param usageCounterId
     * @param clientId
     * @return
     * @throws Exception
     */
    @Override
    public ApiResponse getUsageCounterById(Integer usageCounterId, Integer clientId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            CoreUsagecounter coreUsageCounterList = dataRepositoryService.getUsagecounterById(usageCounterId, clientId);

            if (coreUsageCounterList == null) {
                LOGGER.info("Usage counter does not exist");
                response.responseData = _helper.getErrorMessageWithURL(ApiErrors.USAGE_COUNTER_NOT_EXIST);
                return response;
            } else {
                response.status = true;
                response.responseData = _coreMappers.createUSageCounterResponse(coreUsageCounterList);
            }
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Unable to get usage counter. Exception - " + ex.getMessage());
            throw ex;
        }
        return response;
    }
}
