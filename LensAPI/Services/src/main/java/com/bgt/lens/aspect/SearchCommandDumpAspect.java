// <editor-fold defaultstate="collapsed" desc="Copyright © 2015 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.aspect;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.helpers.Helper;
import com.bgt.lens.model.rest.response.ApiResponse;
import com.bgt.lens.services.logger.ILoggerService;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * Search Command dump aspect
 */
@Aspect
public class SearchCommandDumpAspect {

    private static final Logger LOGGER = LogManager.getLogger(SearchCommandDumpAspect.class);

    private final Helper _helper = new Helper();

    private ILoggerService loggerService = null;

    /**
     * Set logger service
     *
     * @param loggerService
     */
    public void setLoggerService(ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    /**
     * Search command dump
     *
     * @param joinPoint
     * @param result
     * @throws Exception
     * @throws Throwable
     */
    @AfterReturning(pointcut = "execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.searchResume(..)) "
            + "||execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.getFacetDistribution(..))"
            + "||execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.searchJob(..))" 
            + "||execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.getJobDocumentIds(..))"
            + "||execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.getResumeDocumentIds(..))",
            returning = "result")
    public void dumpSearchCommand(JoinPoint joinPoint, Object result) throws Exception, Throwable {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");
            // We have to log all the search command even if we are getting error in search response.
            // For example, fetch,resume like this search some time document will not be avaliable in vendor then search response will be error tag with message.
            ApiResponse apiResponse = loggerService.addSearchCommandDump(apiContext);
            
            /*
             ApiResponse apiResponse = (ApiResponse) result;
             if (apiResponse.status) {
             loggerService.addSearchCommandDump(apiContext);
             }
             */
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add Lens Document Exception. Exception - " + ex.getMessage());
        }
    }
    
    /**
     * Search command dump
     *
     * @param joinPoint
     * @param result
     * @throws Exception
     * @throws Throwable
     */
    @AfterThrowing(pointcut = "execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.searchResume(..)) "
            + "||execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.getFacetDistribution(..))"
            + "||execution(public * com.bgt.lens.services.search.impl.SearchServiceImpl.searchJob(..))" , throwing = "exception")
    public void dumpSearchCommandOnException(JoinPoint joinPoint, Exception exception) throws Exception, Throwable {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            ApiContext apiContext = (ApiContext) request.getAttribute("ApiContext");
            // We have to log all the search command even if we are getting error in search response.
            // For example, fetch,resume like this search some time document will not be avaliable in vendor then search response will be error tag with message.
            if(_helper.isNotNullOrEmpty(apiContext.getSearchCommand())){
                ApiResponse apiResponse = loggerService.addSearchCommandDump(apiContext);
            }
            
            /*
             ApiResponse apiResponse = (ApiResponse) result;
             if (apiResponse.status) {
             loggerService.addSearchCommandDump(apiContext);
             }
             */
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            LOGGER.error("Add Lens Document Exception. Exception - " + ex.getMessage());
        }
    }
}
