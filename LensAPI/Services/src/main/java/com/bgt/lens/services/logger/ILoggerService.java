// <editor-fold defaultstate="collapsed" desc="Copyright © 2014 Burning Glass International Inc.">
/*
 *******************************************************************************
 * Proprietary and Confidential
 *
 * All rights reserved. Burning
 * Glass Technologies DISCLAIMS ANY OTHER WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * *****************************************************************************
 */
// </editor-fold>
package com.bgt.lens.services.logger;

import com.bgt.lens.api.ApiContext;
import com.bgt.lens.model.adminservice.request.GetErrorLogsRequest;
import com.bgt.lens.model.adminservice.request.GetUsageCounterRequest;
import com.bgt.lens.model.adminservice.request.GetUsageLogsRequest;
import com.bgt.lens.model.rest.response.ApiResponse;
import java.util.concurrent.Future;

/**
 *
 * Logger service Interface
 */
public interface ILoggerService {

    /**
     * Add authentication log
     *
     * @param context
     * @return Authentication log
     * @throws Exception
     */
    public Future<Boolean> addAuthenticationLog(ApiContext context) throws Exception;
    
    /**
     * Add advanced log
     *
     * @param context
     * @return Authentication log
     * @throws Exception
     */
    public Future<Boolean> addAdvancedLog(ApiContext context) throws Exception;

    /**
     * Add Lens document
     *
     * @param context
     * @param binaryData
     * @param docType
     * @return
     * @throws Exception
     */
    public Future<Boolean> addLensDocument(ApiContext context, String binaryData, String docType, String InstanceType, String Locale) throws Exception;

    /**
     * Add Usage log
     *
     * @param context
     * @return Authentication log
     * @throws Exception
     */
    public Future<Boolean> addUsageLog(ApiContext context) throws Exception;

    /**
     * Add error log
     *
     * @param context
     * @return
     * @throws Exception
     */
    public Future<Boolean> addErrorLog(ApiContext context) throws Exception;

    /**
     * Update Usage Counter
     *
     * @param context
     * @return
     * @throws Exception
     */
    public Future<Boolean> updateUsageCounter(ApiContext context) throws Exception;

    /**
     * Get usage logs by criteria
     *
     * @param getUsageLogs
     * @param clientId
     * @return
     * @throws Exception
     */
    public ApiResponse getUsageLogs(GetUsageLogsRequest getUsageLogs, Integer clientId) throws Exception;

    /**
     * Get usage logs by id
     *
     * @param usageLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    public ApiResponse getUsageLogById(String usageLogId, Integer clientId) throws Exception;

    /**
     * Get error logs by criteria
     *
     * @param errorLogs
     * @param clientId
     * @return
     * @throws Exception
     */
    public ApiResponse getErrorLogs(GetErrorLogsRequest errorLogs, Integer clientId) throws Exception;

    /**
     * Get error log by id
     *
     * @param errorLogId
     * @param clientId
     * @return
     * @throws Exception
     */
    public ApiResponse getErrorLogById(String errorLogId, Integer clientId) throws Exception;

    /**
     * Get usage counter by criteria
     *
     * @param usageCounter
     * @param clientId
     * @return
     * @throws Exception
     */
    public ApiResponse getUsageCounter(GetUsageCounterRequest usageCounter, Integer clientId) throws Exception;

    /**
     * Get usage counter by id
     *
     * @param usageCounterId
     * @param clientId
     * @return
     * @throws Exception
     */
    public ApiResponse getUsageCounterById(Integer usageCounterId, Integer clientId) throws Exception;
    
    /**
     * Add search command dump
     * @param context
     * @return
     * @throws Exception 
     */
    public ApiResponse addSearchCommandDump(ApiContext context) throws Exception;
    
    /**
     * Add register log
     * @param context
     * @return
     * @throws Exception 
     */
    public Future<Boolean> addRegisterLog(ApiContext context) throws Exception;
}
